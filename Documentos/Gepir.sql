USE [master]
GO
/****** Object:  Database [Gepir]    Script Date: 11/11/2015 17:51:24 ******/
CREATE DATABASE [Gepir]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Gepir', FILENAME = N'C:\WKM\data\MSSQL12.SQL2K14\MSSQL\DATA\Gepir.mdf' , SIZE = 5312KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Gepir_log', FILENAME = N'C:\WKM\data\MSSQL12.SQL2K14\MSSQL\DATA\Gepir_log.ldf' , SIZE = 1344KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Gepir] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Gepir].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Gepir] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Gepir] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Gepir] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Gepir] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Gepir] SET ARITHABORT OFF 
GO
ALTER DATABASE [Gepir] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Gepir] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Gepir] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Gepir] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Gepir] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Gepir] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Gepir] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Gepir] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Gepir] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Gepir] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Gepir] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Gepir] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Gepir] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Gepir] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Gepir] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Gepir] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [Gepir] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Gepir] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Gepir] SET  MULTI_USER 
GO
ALTER DATABASE [Gepir] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Gepir] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Gepir] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Gepir] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Gepir] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Gepir]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Address]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Cep] [nvarchar](max) NOT NULL,
	[Street] [nvarchar](max) NOT NULL,
	[Number] [nvarchar](max) NOT NULL,
	[Complement] [nvarchar](max) NULL,
	[Burgh] [nvarchar](max) NOT NULL,
	[City] [nvarchar](max) NOT NULL,
	[State] [nvarchar](max) NOT NULL,
	[Country] [nvarchar](max) NOT NULL,
	[StreetTypeId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Address] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Company]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[ProfileId] [int] NOT NULL DEFAULT ((0)),
	[Overdue] [bit] NOT NULL DEFAULT ((0)),
	[CacheExpires] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
 CONSTRAINT [PK_dbo.Company] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompanyActivity]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyActivity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.CompanyActivity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConfigurationProfile]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConfigurationProfile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DailyQueries] [int] NOT NULL,
	[WeeklyQueries] [int] NOT NULL,
	[MonthlyQueries] [int] NOT NULL,
	[AutomaticAuthorization] [bit] NOT NULL,
	[SharedQueries] [bit] NOT NULL,
	[UnlimitedQueries] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.ConfigurationProfile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogEntry]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogEntry](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogType] [nvarchar](max) NULL,
	[Timestamp] [datetime] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[Executor] [nvarchar](max) NULL,
	[CompanyKey] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.LogEntry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MapRouterLocation]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapRouterLocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Country] [nvarchar](max) NULL,
	[Ip] [nvarchar](max) NULL,
	[IsOnline] [bit] NOT NULL,
	[LastChecked] [datetime] NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[WSDLUrl] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.MapRouterLocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Method]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Method](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Method] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MobileUser]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MobileUser](
	[Id] [int] NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[Age] [int] NOT NULL,
	[Region] [nvarchar](max) NOT NULL,
	[Sex] [nvarchar](max) NOT NULL,
	[ProfileId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.MobileUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Password]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Password](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Password] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PortalUser]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PortalUser](
	[Id] [int] NOT NULL,
	[CPF] [nvarchar](max) NOT NULL,
	[External] [bit] NOT NULL DEFAULT ((0)),
	[CompanyId] [int] NOT NULL DEFAULT ((0)),
	[SolicitationRequestId] [int] NULL,
 CONSTRAINT [PK_dbo.PortalUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProfessionalArea]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProfessionalArea](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.ProfessionalArea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RequestLogEntry]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestLogEntry](
	[Id] [int] NOT NULL,
	[AccessChannel] [nvarchar](max) NOT NULL,
	[MethodName] [nvarchar](max) NOT NULL,
	[RequestParam] [nvarchar](max) NOT NULL,
	[CompanyName] [nvarchar](max) NULL,
	[ReturnCode] [int] NOT NULL,
	[ItemDescription] [nvarchar](max) NULL,
	[ResponderGLN] [nvarchar](max) NOT NULL,
	[ItemOwnerName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.RequestLogEntry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReturnCodeMessage]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReturnCodeMessage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [int] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[TextPt] [nvarchar](max) NOT NULL,
	[DescriptionPt] [nvarchar](max) NOT NULL,
	[DisplayDescriptionWithError] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.ReturnCodeMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReturnField]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReturnField](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Category] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.ReturnField] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReturnFieldConfigurationProfile]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReturnFieldConfigurationProfile](
	[ReturnField_Id] [int] NOT NULL,
	[ConfigurationProfile_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ReturnFieldConfigurationProfile] PRIMARY KEY CLUSTERED 
(
	[ReturnField_Id] ASC,
	[ConfigurationProfile_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleLevel]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleLevel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.RoleLevel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleUser]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleUser](
	[User_Id] [int] NOT NULL,
	[Role_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RoleUser] PRIMARY KEY CLUSTERED 
(
	[User_Id] ASC,
	[Role_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SearchChannel]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SearchChannel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Ip] [nvarchar](max) NULL,
	[GLN] [nvarchar](max) NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
	[CompanyId] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.SearchChannel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SearchChannelMethod]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SearchChannelMethod](
	[SearchChannel_Id] [int] NOT NULL,
	[Method_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.SearchChannelMethod] PRIMARY KEY CLUSTERED 
(
	[SearchChannel_Id] ASC,
	[Method_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Settings]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Settings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SolicitationRequest]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolicitationRequest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CPF] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[CompanyKey] [nvarchar](max) NULL,
	[CompanyName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NOT NULL,
	[SolicitationDate] [datetime] NOT NULL,
	[ApprovalDate] [datetime] NULL,
	[Email] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[TelephoneNumber] [nvarchar](max) NOT NULL,
	[CellphoneNumber] [nvarchar](max) NOT NULL,
	[CompanyActivityId] [int] NOT NULL,
	[RoleLevelId] [int] NOT NULL DEFAULT ((0)),
	[ProfessionalAreaId] [int] NOT NULL DEFAULT ((0)),
	[IsApproved] [bit] NOT NULL DEFAULT ((0)),
	[AddressId] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.SolicitationRequest] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StreetType]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StreetType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.StreetType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 11/11/2015 17:51:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Login] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DailyQueries] [int] NOT NULL,
	[ExclusiveQueries] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[UnlimitedQueries] [bit] NOT NULL DEFAULT ((0)),
	[Provider] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [IX_StreetTypeId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_StreetTypeId] ON [dbo].[Address]
(
	[StreetTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Profile_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_Profile_Id] ON [dbo].[Company]
(
	[ProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[MobileUser]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProfileId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_ProfileId] ON [dbo].[MobileUser]
(
	[ProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[Password]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CompanyId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_CompanyId] ON [dbo].[PortalUser]
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[PortalUser]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SolicitationRequestId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_SolicitationRequestId] ON [dbo].[PortalUser]
(
	[SolicitationRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[RequestLogEntry]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ConfigurationProfile_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_ConfigurationProfile_Id] ON [dbo].[ReturnFieldConfigurationProfile]
(
	[ConfigurationProfile_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ReturnField_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_ReturnField_Id] ON [dbo].[ReturnFieldConfigurationProfile]
(
	[ReturnField_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Role_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_Role_Id] ON [dbo].[RoleUser]
(
	[Role_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_User_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_User_Id] ON [dbo].[RoleUser]
(
	[User_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CompanyId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_CompanyId] ON [dbo].[SearchChannel]
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Method_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_Method_Id] ON [dbo].[SearchChannelMethod]
(
	[Method_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SearchChannel_Id]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_SearchChannel_Id] ON [dbo].[SearchChannelMethod]
(
	[SearchChannel_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AddressId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_AddressId] ON [dbo].[SolicitationRequest]
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CompanyActivityId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_CompanyActivityId] ON [dbo].[SolicitationRequest]
(
	[CompanyActivityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProfessionalAreaId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_ProfessionalAreaId] ON [dbo].[SolicitationRequest]
(
	[ProfessionalAreaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_RoleLevelId]    Script Date: 11/11/2015 17:51:24 ******/
CREATE NONCLUSTERED INDEX [IX_RoleLevelId] ON [dbo].[SolicitationRequest]
(
	[RoleLevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MobileUser] ADD  DEFAULT ((0)) FOR [ProfileId]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Address_dbo.StreetType_StreetTypeId] FOREIGN KEY([StreetTypeId])
REFERENCES [dbo].[StreetType] ([Id])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_dbo.Address_dbo.StreetType_StreetTypeId]
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Company_dbo.ConfigurationProfile_Profile_Id] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[ConfigurationProfile] ([Id])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_dbo.Company_dbo.ConfigurationProfile_Profile_Id]
GO
ALTER TABLE [dbo].[MobileUser]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MobileUser_dbo.ConfigurationProfile_ProfileId] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[ConfigurationProfile] ([Id])
GO
ALTER TABLE [dbo].[MobileUser] CHECK CONSTRAINT [FK_dbo.MobileUser_dbo.ConfigurationProfile_ProfileId]
GO
ALTER TABLE [dbo].[MobileUser]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MobileUser_dbo.User_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[MobileUser] CHECK CONSTRAINT [FK_dbo.MobileUser_dbo.User_Id]
GO
ALTER TABLE [dbo].[Password]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Password_dbo.User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Password] CHECK CONSTRAINT [FK_dbo.Password_dbo.User_UserId]
GO
ALTER TABLE [dbo].[PortalUser]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PortalUser_dbo.Company_CompanyId] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PortalUser] CHECK CONSTRAINT [FK_dbo.PortalUser_dbo.Company_CompanyId]
GO
ALTER TABLE [dbo].[PortalUser]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PortalUser_dbo.SolicitationRequest_SolicitationRequestId] FOREIGN KEY([SolicitationRequestId])
REFERENCES [dbo].[SolicitationRequest] ([Id])
GO
ALTER TABLE [dbo].[PortalUser] CHECK CONSTRAINT [FK_dbo.PortalUser_dbo.SolicitationRequest_SolicitationRequestId]
GO
ALTER TABLE [dbo].[PortalUser]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PortalUser_dbo.User_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[PortalUser] CHECK CONSTRAINT [FK_dbo.PortalUser_dbo.User_Id]
GO
ALTER TABLE [dbo].[RequestLogEntry]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RequestLogEntry_dbo.LogEntry_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[LogEntry] ([Id])
GO
ALTER TABLE [dbo].[RequestLogEntry] CHECK CONSTRAINT [FK_dbo.RequestLogEntry_dbo.LogEntry_Id]
GO
ALTER TABLE [dbo].[ReturnFieldConfigurationProfile]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReturnFieldConfigurationProfile_dbo.ConfigurationProfile_ConfigurationProfile_Id] FOREIGN KEY([ConfigurationProfile_Id])
REFERENCES [dbo].[ConfigurationProfile] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReturnFieldConfigurationProfile] CHECK CONSTRAINT [FK_dbo.ReturnFieldConfigurationProfile_dbo.ConfigurationProfile_ConfigurationProfile_Id]
GO
ALTER TABLE [dbo].[ReturnFieldConfigurationProfile]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReturnFieldConfigurationProfile_dbo.ReturnField_ReturnField_Id] FOREIGN KEY([ReturnField_Id])
REFERENCES [dbo].[ReturnField] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReturnFieldConfigurationProfile] CHECK CONSTRAINT [FK_dbo.ReturnFieldConfigurationProfile_dbo.ReturnField_ReturnField_Id]
GO
ALTER TABLE [dbo].[RoleUser]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRole_dbo.Role_Role_Id] FOREIGN KEY([Role_Id])
REFERENCES [dbo].[Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleUser] CHECK CONSTRAINT [FK_dbo.UserRole_dbo.Role_Role_Id]
GO
ALTER TABLE [dbo].[RoleUser]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRole_dbo.User_User_Id] FOREIGN KEY([User_Id])
REFERENCES [dbo].[User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleUser] CHECK CONSTRAINT [FK_dbo.UserRole_dbo.User_User_Id]
GO
ALTER TABLE [dbo].[SearchChannel]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SearchChannel_dbo.Company_CompanyId] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SearchChannel] CHECK CONSTRAINT [FK_dbo.SearchChannel_dbo.Company_CompanyId]
GO
ALTER TABLE [dbo].[SearchChannelMethod]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SearchChannelMethod_dbo.Method_Method_Id] FOREIGN KEY([Method_Id])
REFERENCES [dbo].[Method] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SearchChannelMethod] CHECK CONSTRAINT [FK_dbo.SearchChannelMethod_dbo.Method_Method_Id]
GO
ALTER TABLE [dbo].[SearchChannelMethod]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SearchChannelMethod_dbo.SearchChannel_SearchChannel_Id] FOREIGN KEY([SearchChannel_Id])
REFERENCES [dbo].[SearchChannel] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SearchChannelMethod] CHECK CONSTRAINT [FK_dbo.SearchChannelMethod_dbo.SearchChannel_SearchChannel_Id]
GO
ALTER TABLE [dbo].[SolicitationRequest]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SolicitationRequest_dbo.Address_AddressId] FOREIGN KEY([AddressId])
REFERENCES [dbo].[Address] ([Id])
GO
ALTER TABLE [dbo].[SolicitationRequest] CHECK CONSTRAINT [FK_dbo.SolicitationRequest_dbo.Address_AddressId]
GO
ALTER TABLE [dbo].[SolicitationRequest]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SolicitationRequest_dbo.CompanyActivity_CompanyActivityId] FOREIGN KEY([CompanyActivityId])
REFERENCES [dbo].[CompanyActivity] ([Id])
GO
ALTER TABLE [dbo].[SolicitationRequest] CHECK CONSTRAINT [FK_dbo.SolicitationRequest_dbo.CompanyActivity_CompanyActivityId]
GO
ALTER TABLE [dbo].[SolicitationRequest]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SolicitationRequest_dbo.ProfessionalArea_ProfessionalAreaId] FOREIGN KEY([ProfessionalAreaId])
REFERENCES [dbo].[ProfessionalArea] ([Id])
GO
ALTER TABLE [dbo].[SolicitationRequest] CHECK CONSTRAINT [FK_dbo.SolicitationRequest_dbo.ProfessionalArea_ProfessionalAreaId]
GO
ALTER TABLE [dbo].[SolicitationRequest]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SolicitationRequest_dbo.RoleLevel_RoleLevelId] FOREIGN KEY([RoleLevelId])
REFERENCES [dbo].[RoleLevel] ([Id])
GO
ALTER TABLE [dbo].[SolicitationRequest] CHECK CONSTRAINT [FK_dbo.SolicitationRequest_dbo.RoleLevel_RoleLevelId]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de histórico de Migrations, gerada automaticamente pelo EntityFramework. É através dessa tabela que o framework controla o versionamento e atualizações dos schemas de banco de dados' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'__MigrationHistory'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do logradouro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address', @level2type=N'COLUMN',@level2name=N'Street'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número (complemento 1) do endereço' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address', @level2type=N'COLUMN',@level2name=N'Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Complemento (2) do endereço' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address', @level2type=N'COLUMN',@level2name=N'Complement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bairro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address', @level2type=N'COLUMN',@level2name=N'Burgh'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cidade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address', @level2type=N'COLUMN',@level2name=N'City'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado (para cadastros no Brasil)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address', @level2type=N'COLUMN',@level2name=N'State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'País' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address', @level2type=N'COLUMN',@level2name=N'Country'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira que vincula o tipo do logradouro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address', @level2type=N'COLUMN',@level2name=N'StreetTypeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de endereços' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Address'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave da empresa (pode ser CPF ou CNPJ válidos)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da empresa ou da pessoa física cadastrada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira para perfl de configuração da emrpesa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'ProfileId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag indicando se empresa encontra-se em débito com a GS1. É atualizado mediante consulta ao RGC durante o login sempre que o valor CacheExpires obriga a atualização' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Overdue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expiração de cache de informações de empresa cadastrada a partir de um sistema externo. Indica a data/hora a partir da qual o sistema executará uma verificação dos dados de acesso da empresa junto ao RGC quando o usuário fizer login' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'CacheExpires'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cadastro de empresas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do ramo de atividade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CompanyActivity', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cadastro de ramos de atividades das empresas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CompanyActivity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do perfil de configuração' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ConfigurationProfile', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Limite de consultas diárias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ConfigurationProfile', @level2type=N'COLUMN',@level2name=N'DailyQueries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Limite de consultas semanais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ConfigurationProfile', @level2type=N'COLUMN',@level2name=N'WeeklyQueries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Limite de consultas mensais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ConfigurationProfile', @level2type=N'COLUMN',@level2name=N'MonthlyQueries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag indicando autorização automática para solicitações de acesso à empresas em determinado perfil. Este campo também indica autorização de acesso automático para novos usuários que se cadastram vinculando-se às empresas cadastradas em perfis que permitem a autorização automática' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ConfigurationProfile', @level2type=N'COLUMN',@level2name=N'AutomaticAuthorization'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag que indica se usuários poderão utilizar consultas compartilhadas ou não. Caso não haja a possiobilidade de consultas compartilhadas, o número de consultas para cada usuário é limitado pela configuração do próprio usuário. Consultas compartilhadas permitem que consultas não-exclusivas possam ser consumidas pelos usuários da empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ConfigurationProfile', @level2type=N'COLUMN',@level2name=N'SharedQueries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag que indica se empresa tem consultas ilimitadas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ConfigurationProfile', @level2type=N'COLUMN',@level2name=N'UnlimitedQueries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Configurações de perfil de empresas e usários mobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ConfigurationProfile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo da entrada de log (todos os tipos possíveis estão na classe Model.Domain.Log.LogType)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntry', @level2type=N'COLUMN',@level2name=N'LogType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora da geração do log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntry', @level2type=N'COLUMN',@level2name=N'Timestamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição da operação realizada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntry', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuário executor da ação' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntry', @level2type=N'COLUMN',@level2name=N'Executor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave da empresa a qual o usuário pertence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntry', @level2type=N'COLUMN',@level2name=N'CompanyKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entradas de logs gerais do sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código ISO para o país do roteador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MapRouterLocation', @level2type=N'COLUMN',@level2name=N'Country'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP do roteador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MapRouterLocation', @level2type=N'COLUMN',@level2name=N'Ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag que indica se na última verificação o roteador estava online' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MapRouterLocation', @level2type=N'COLUMN',@level2name=N'IsOnline'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora da última verificação do roteador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MapRouterLocation', @level2type=N'COLUMN',@level2name=N'LastChecked'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Latitude do local do roteador (indicado no xml de configuração)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MapRouterLocation', @level2type=N'COLUMN',@level2name=N'Latitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longitude do local do roteador (indicado no xml de configuração)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MapRouterLocation', @level2type=N'COLUMN',@level2name=N'Longitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'URL do WSDL do roteador' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MapRouterLocation', @level2type=N'COLUMN',@level2name=N'WSDLUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dados de configuração dos roteadores (proveniente do xml de configuração)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MapRouterLocation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do método' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Method', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Métodos de consulta disponíveis para execução' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Method'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sobrenome' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUser', @level2type=N'COLUMN',@level2name=N'LastName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Idade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUser', @level2type=N'COLUMN',@level2name=N'Age'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Região' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUser', @level2type=N'COLUMN',@level2name=N'Region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gênero do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUser', @level2type=N'COLUMN',@level2name=N'Sex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira indicando o perfil do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUser', @level2type=N'COLUMN',@level2name=N'ProfileId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cadastro de usuários mobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUser'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hash da senha do histórico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Password', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora de criação da senha' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Password', @level2type=N'COLUMN',@level2name=N'CreationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Password', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Histórico de senhas do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag que indica quando usuário é cadastrado externamente (ex: importado do CNP)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PortalUser', @level2type=N'COLUMN',@level2name=N'External'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira indicando o id da empresa a qual o usuário está vinculado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PortalUser', @level2type=N'COLUMN',@level2name=N'CompanyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira indicando o id da Solicitação de acesso do usuário (caso exista)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PortalUser', @level2type=N'COLUMN',@level2name=N'SolicitationRequestId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cadastro de usuários de portal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PortalUser'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cadastro de Área profissional' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProfessionalArea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canal de acesso utilizado para consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry', @level2type=N'COLUMN',@level2name=N'AccessChannel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do método de consulta executado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry', @level2type=N'COLUMN',@level2name=N'MethodName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parâmetro enviado ao método de consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry', @level2type=N'COLUMN',@level2name=N'RequestParam'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da emrpesa do usuário executor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry', @level2type=N'COLUMN',@level2name=N'CompanyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código de retorno da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry', @level2type=N'COLUMN',@level2name=N'ReturnCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do item de retorno da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry', @level2type=N'COLUMN',@level2name=N'ItemDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GLN de resposta da consulta (indicar o roteador que retornou os valores da consulta)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry', @level2type=N'COLUMN',@level2name=N'ResponderGLN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do proprietário do item de retorno da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry', @level2type=N'COLUMN',@level2name=N'ItemOwnerName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entrada de logs de consultas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RequestLogEntry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do código de retorno para mapeamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnCodeMessage', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mensagem de retorno para o código em inglês' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnCodeMessage', @level2type=N'COLUMN',@level2name=N'Text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição completa da mensagem em inglês' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnCodeMessage', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mensagem de retorno para o código em português' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnCodeMessage', @level2type=N'COLUMN',@level2name=N'TextPt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição completa da mensagem em português' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnCodeMessage', @level2type=N'COLUMN',@level2name=N'DescriptionPt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag para exibição ou não da descrição ao imprimir a mensagem personalizada para o código' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnCodeMessage', @level2type=N'COLUMN',@level2name=N'DisplayDescriptionWithError'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Configurações das mensagens de retorno de códigos das consultas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnCodeMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do campo a ser exibido em consultas de determinado perfil' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnField', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Indica a categoria da informação provida por determinado campo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnField', @level2type=N'COLUMN',@level2name=N'Category'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de configuração de campos a serem retornados nas consultas. Campos não cadastrados para um perfil, são exibidos sem dados quando usuário executa uma consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnField'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relacionamento entre tabela de Perfis com tabela de campos de retorno de consultas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnFieldConfigurationProfile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da regra de acesso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de cadastro de regras de acesso (permissão)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cadastro de cargos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RoleLevel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de relacionamento entre usuários e regras de acesso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RoleUser'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP utilizado para consultas via WebService na empresa. Será implementada possibilidade de mais de um IP por empresa, separado por ponto e vírgula. Asterisco é um wildcard' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchChannel', @level2type=N'COLUMN',@level2name=N'Ip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GLN autorizado para executar a consulta em caso de consultas via WebService' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchChannel', @level2type=N'COLUMN',@level2name=N'GLN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de acesso válido para configuração. Pode ser WebService, Mobile ou Portal e cada empresa terá as três configurações' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchChannel', @level2type=N'COLUMN',@level2name=N'Discriminator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id da empresa vinculada à configuração' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchChannel', @level2type=N'COLUMN',@level2name=N'CompanyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Canais de acesso de consultas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchChannel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relacionamento entre Canais de Acesso e Métodos de consulta permitidos. Desse modo, cada empresa terá permissão em métodos de consulta específicos para cada canal de acesso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchChannelMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave da configuração (key-value)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Settings', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor da configuração (key-value)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Settings', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Configurações da aplicação em pares de chave/valor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Settings'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primeiro nome do solicitante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave da empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'CompanyKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da emrpesa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'CompanyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Senha inicial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora da solicitação de acesso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'SolicitationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora da aprovação de acesso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'ApprovalDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Email do solicitante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Último nome do solicitante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'LastName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do telefone 1 do solicitante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'TelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número do telefone 2 (ou celular) do solicitante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'CellphoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira para o ramo de atividade da empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'CompanyActivityId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira indicando o cargo do usuário solicitante em sua empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'RoleLevelId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira indicando a área de atuação do solicitante em sua empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'ProfessionalAreaId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag de status de aprovação da solicitação' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'IsApproved'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira vinculando o endereço da solicitação' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest', @level2type=N'COLUMN',@level2name=N'AddressId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Solicitações de acesso ao sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SolicitationRequest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de logradouro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StreetType', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela que armazena os tipos de logradouro cadastrados no sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StreetType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Login do usuário (email)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Login'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome completo para exibição do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de consultas diárias disponíveis para o usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'DailyQueries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de consultas diárias exclusivas na empresa para o usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'ExclusiveQueries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status do cadastro do usuário (Model.UserStatuses)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag indicando se usuário tem direito a consultas ilimitadas (configuração sobrescrita pelo perfil da empresa)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'UnlimitedQueries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag que indica o provedor das informações do usuário (Model.UserProvider)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Provider'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cadastro de usuários' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User'
GO
USE [master]
GO
ALTER DATABASE [Gepir] SET  READ_WRITE 
GO
