USE [master]
GO
/****** Object:  Database [inbarHMG]    Script Date: 12/11/2015 20:56:48 ******/
CREATE DATABASE [inbarHMG]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'inbarHMG_Data', FILENAME = N'C:\WKM\data\MSSQL12.SQL2K14\MSSQL\DATA\inbarHMG.mdf' , SIZE = 6336KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'inbarHMG_Log', FILENAME = N'C:\WKM\data\MSSQL12.SQL2K14\MSSQL\DATA\inbarHMG.ldf' , SIZE = 1536KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [inbarHMG] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [inbarHMG].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [inbarHMG] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [inbarHMG] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [inbarHMG] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [inbarHMG] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [inbarHMG] SET ARITHABORT OFF 
GO
ALTER DATABASE [inbarHMG] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [inbarHMG] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [inbarHMG] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [inbarHMG] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [inbarHMG] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [inbarHMG] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [inbarHMG] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [inbarHMG] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [inbarHMG] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [inbarHMG] SET  DISABLE_BROKER 
GO
ALTER DATABASE [inbarHMG] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [inbarHMG] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [inbarHMG] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [inbarHMG] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [inbarHMG] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [inbarHMG] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [inbarHMG] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [inbarHMG] SET RECOVERY FULL 
GO
ALTER DATABASE [inbarHMG] SET  MULTI_USER 
GO
ALTER DATABASE [inbarHMG] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [inbarHMG] SET DB_CHAINING OFF 
GO
ALTER DATABASE [inbarHMG] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [inbarHMG] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [inbarHMG] SET DELAYED_DURABILITY = DISABLED 
GO
USE [inbarHMG]
GO
/****** Object:  User [Working.Minds]    Script Date: 12/11/2015 20:56:48 ******/
CREATE USER [Working.Minds] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [Working.Minds]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AIs]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AIs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[NameEN] [nvarchar](255) NOT NULL,
	[NameES] [nvarchar](255) NOT NULL,
	[Identifier] [nvarchar](5) NOT NULL,
	[Format] [nvarchar](50) NOT NULL,
	[Label] [nvarchar](30) NOT NULL,
	[LabelEN] [nvarchar](30) NOT NULL,
	[LabelES] [nvarchar](30) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_dbo.AIs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Devices]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Devices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_dbo.Devices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeviceSearchLogs]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceSearchLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[LogId] [int] NOT NULL,
	[DeviceId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.DeviceSearchLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FullItemSearchLog]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FullItemSearchLog](
	[Id] [int] NOT NULL,
	[CpfCnpj] [nvarchar](14) NULL,
	[RazaoSocial] [nvarchar](150) NULL,
	[Gtin] [nvarchar](14) NULL,
	[NrPrefixo] [nvarchar](12) NULL,
	[DescTipoGtin] [nvarchar](7) NULL,
	[MarcaProduto] [nvarchar](50) NULL,
	[CodigoInterno] [nvarchar](100) NULL,
	[Descricao] [nvarchar](300) NULL,
	[DescricaoImpressao] [nvarchar](40) NULL,
	[DtInclusao] [datetime] NULL,
	[DtSuspensao] [datetime] NULL,
	[DtReativacao] [datetime] NULL,
	[DtCancelamento] [datetime] NULL,
	[DtReutilizacao] [datetime] NULL,
	[Pais] [nvarchar](3) NULL,
	[NomeTipoProduto] [nvarchar](50) NULL,
	[Lingua] [nvarchar](5) NULL,
	[Estado] [nvarchar](10) NULL,
	[TIPI] [decimal](18, 2) NULL,
	[CodigoRegistro] [nvarchar](30) NULL,
	[NomeAgencia] [nvarchar](20) NULL,
	[Largura] [decimal](18, 2) NULL,
	[Profundidade] [decimal](18, 2) NULL,
	[Altura] [decimal](18, 2) NULL,
	[PesoLiquido] [decimal](18, 2) NULL,
	[PesoBruto] [decimal](18, 2) NULL,
	[UrlMobilecom] [nvarchar](100) NULL,
	[UrlFoto1] [nvarchar](100) NULL,
	[UrlFoto2] [nvarchar](100) NULL,
	[UrlFoto3] [nvarchar](100) NULL,
	[CompartilhaDado] [nvarchar](max) NULL,
	[GtinOrigem] [nvarchar](14) NULL,
	[Status] [nvarchar](max) NULL,
	[DescStatus] [nvarchar](18) NULL,
	[Observacoes] [nvarchar](max) NULL,
	[NCM] [nvarchar](100) NULL,
	[GtinInferior] [nvarchar](14) NULL,
	[QtdeItens] [int] NULL,
	[DescricaoInferior] [nvarchar](300) NULL,
	[CodeSegment] [nvarchar](10) NULL,
	[CodeFamily] [nvarchar](10) NULL,
	[CodeClass] [nvarchar](10) NULL,
	[CodeBrick] [nvarchar](10) NULL,
	[CodeType] [nvarchar](10) NULL,
	[CodeValue] [nvarchar](10) NULL,
	[StatusPrefixo] [nvarchar](18) NULL,
	[Imagem1] [nvarchar](max) NULL,
	[Imagem2] [nvarchar](max) NULL,
	[Imagem3] [nvarchar](max) NULL,
	[Contato] [nvarchar](100) NULL,
 CONSTRAINT [PK_dbo.FullItemSearchLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ItemSearchLogs]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemSearchLogs](
	[Id] [int] NOT NULL,
	[GTIN] [nvarchar](18) NULL,
	[ManufacturerGLN] [nvarchar](18) NULL,
	[ItemName] [nvarchar](400) NULL,
	[BrandName] [nvarchar](400) NULL,
	[TradeItemUnitDescriptor] [nvarchar](400) NULL,
	[DescriptiveSize] [nvarchar](400) NULL,
	[NetContent] [nvarchar](200) NULL,
 CONSTRAINT [PK_dbo.ItemSearchLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogEntries]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogEntries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Executor] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](4000) NOT NULL,
 CONSTRAINT [PK_dbo.LogEntries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MobileUserBaseDevices]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MobileUserBaseDevices](
	[MobileUserBase_Id] [int] NOT NULL,
	[Device_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.MobileUserBaseDevices] PRIMARY KEY CLUSTERED 
(
	[MobileUserBase_Id] ASC,
	[Device_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MobileUsers]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MobileUsers](
	[Id] [int] NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Gender] [nvarchar](1) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](2) NULL,
	[Country] [nvarchar](50) NULL,
	[ProfileId] [int] NOT NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
	[ProviderId] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
 CONSTRAINT [PK_dbo.MobileUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartySearchLogs]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartySearchLogs](
	[Id] [int] NOT NULL,
	[GLN] [nvarchar](18) NULL,
	[GCP] [nvarchar](50) NULL,
	[AdditionalPartyId] [nvarchar](200) NULL,
	[PartyName] [nvarchar](400) NULL,
	[StreetAddress] [nvarchar](400) NULL,
	[POBoxNumber] [nvarchar](200) NULL,
	[Subdivision] [nvarchar](200) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[City] [nvarchar](200) NULL,
	[CountryISOCode] [nvarchar](50) NULL,
	[PartyRole] [nvarchar](100) NULL,
 CONSTRAINT [PK_dbo.PartySearchLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Passwords]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Passwords](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](2000) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[LocalMobileUserId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Passwords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PortalUsers]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PortalUsers](
	[Id] [int] NOT NULL,
	[Identification] [nvarchar](20) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_dbo.PortalUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProblemReports]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProblemReports](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Reporter] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[CodeType] [nvarchar](5) NOT NULL,
	[BarcodeType] [nvarchar](50) NULL,
	[Reason] [nvarchar](200) NOT NULL,
	[Text] [nvarchar](500) NULL,
	[ImageLink] [nvarchar](300) NOT NULL,
	[Latitude] [nvarchar](40) NULL,
	[Longitude] [nvarchar](40) NULL,
 CONSTRAINT [PK_dbo.ProblemReports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Registrations]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registrations](
	[Id] [int] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](2000) NOT NULL,
	[Gender] [nvarchar](1) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](2) NULL,
	[Country] [nvarchar](50) NULL,
	[DeviceId] [nvarchar](100) NULL,
	[UserId] [int] NULL,
	[BirthDate] [datetime] NULL,
 CONSTRAINT [PK_dbo.Registrations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReturnFields]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReturnFields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Category] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_dbo.ReturnFields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReturnFieldUserProfiles]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReturnFieldUserProfiles](
	[ReturnField_Id] [int] NOT NULL,
	[UserProfile_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ReturnFieldUserProfiles] PRIMARY KEY CLUSTERED 
(
	[ReturnField_Id] ASC,
	[UserProfile_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RolePortalUsers]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePortalUsers](
	[Role_Id] [int] NOT NULL,
	[PortalUser_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.RolePortalUsers] PRIMARY KEY CLUSTERED 
(
	[Role_Id] ASC,
	[PortalUser_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SearchLogs]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SearchLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Device] [nvarchar](100) NOT NULL,
	[Executor] [nvarchar](100) NULL,
	[Method] [nvarchar](30) NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[BarcodeType] [nvarchar](50) NULL,
	[OwnerKey] [nvarchar](30) NULL,
	[NumberOfHits] [int] NOT NULL,
	[ResponderGLN] [nvarchar](20) NOT NULL,
	[ReturnCode] [int] NOT NULL,
	[Latitude] [nvarchar](40) NULL,
	[Longitude] [nvarchar](40) NULL,
 CONSTRAINT [PK_dbo.SearchLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sections]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sections](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[Language] [nvarchar](5) NULL,
	[ParentNodeId] [int] NULL,
 CONSTRAINT [PK_dbo.Sections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Settings]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Collection] [nvarchar](50) NOT NULL,
	[Key] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_dbo.Settings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tokens]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tokens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](60) NOT NULL,
	[ValidationDate] [datetime] NOT NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_dbo.Tokens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfiles]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfiles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DailyLimit] [int] NOT NULL,
	[WeekLimit] [int] NOT NULL,
	[MonthLimit] [int] NOT NULL,
 CONSTRAINT [PK_dbo.UserProfiles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/11/2015 20:56:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[Provider] [nvarchar](50) NULL,
	[Type] [nvarchar](20) NULL,
	[FirstName] [nvarchar](50) NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Identifier]    Script Date: 12/11/2015 20:56:48 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Identifier] ON [dbo].[AIs]
(
	[Identifier] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Value]    Script Date: 12/11/2015 20:56:48 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Value] ON [dbo].[Devices]
(
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DeviceId]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_DeviceId] ON [dbo].[DeviceSearchLogs]
(
	[DeviceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LogId]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_LogId] ON [dbo].[DeviceSearchLogs]
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[FullItemSearchLog]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ItemSearchLogs]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Device_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Device_Id] ON [dbo].[MobileUserBaseDevices]
(
	[Device_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MobileUserBase_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_MobileUserBase_Id] ON [dbo].[MobileUserBaseDevices]
(
	[MobileUserBase_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[MobileUsers]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProfileId]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_ProfileId] ON [dbo].[MobileUsers]
(
	[ProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[PartySearchLogs]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LocalMobileUserId]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_LocalMobileUserId] ON [dbo].[Passwords]
(
	[LocalMobileUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Value]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Value] ON [dbo].[Passwords]
(
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[PortalUsers]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Identification]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Identification] ON [dbo].[PortalUsers]
(
	[Identification] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[Registrations]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Name]    Script Date: 12/11/2015 20:56:48 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Name] ON [dbo].[ReturnFields]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ReturnField_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_ReturnField_Id] ON [dbo].[ReturnFieldUserProfiles]
(
	[ReturnField_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserProfile_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_UserProfile_Id] ON [dbo].[ReturnFieldUserProfiles]
(
	[UserProfile_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_PortalUser_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_PortalUser_Id] ON [dbo].[RolePortalUsers]
(
	[PortalUser_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Role_Id]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Role_Id] ON [dbo].[RolePortalUsers]
(
	[Role_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Name]    Script Date: 12/11/2015 20:56:48 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Name] ON [dbo].[Roles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Key]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Key] ON [dbo].[Sections]
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ParentNodeId]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_ParentNodeId] ON [dbo].[Sections]
(
	[ParentNodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Key]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Key] ON [dbo].[Settings]
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[Tokens]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Value]    Script Date: 12/11/2015 20:56:48 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Value] ON [dbo].[Tokens]
(
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Name]    Script Date: 12/11/2015 20:56:48 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Name] ON [dbo].[UserProfiles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Email]    Script Date: 12/11/2015 20:56:48 ******/
CREATE NONCLUSTERED INDEX [IX_Email] ON [dbo].[Users]
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DeviceSearchLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.DeviceSearchLogs_dbo.Devices_DeviceId] FOREIGN KEY([DeviceId])
REFERENCES [dbo].[Devices] ([Id])
GO
ALTER TABLE [dbo].[DeviceSearchLogs] CHECK CONSTRAINT [FK_dbo.DeviceSearchLogs_dbo.Devices_DeviceId]
GO
ALTER TABLE [dbo].[DeviceSearchLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.DeviceSearchLogs_dbo.SearchLogs_LogId] FOREIGN KEY([LogId])
REFERENCES [dbo].[SearchLogs] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DeviceSearchLogs] CHECK CONSTRAINT [FK_dbo.DeviceSearchLogs_dbo.SearchLogs_LogId]
GO
ALTER TABLE [dbo].[FullItemSearchLog]  WITH CHECK ADD  CONSTRAINT [FK_dbo.FullItemSearchLog_dbo.SearchLogs_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[SearchLogs] ([Id])
GO
ALTER TABLE [dbo].[FullItemSearchLog] CHECK CONSTRAINT [FK_dbo.FullItemSearchLog_dbo.SearchLogs_Id]
GO
ALTER TABLE [dbo].[ItemSearchLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ItemSearchLogs_dbo.SearchLogs_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[SearchLogs] ([Id])
GO
ALTER TABLE [dbo].[ItemSearchLogs] CHECK CONSTRAINT [FK_dbo.ItemSearchLogs_dbo.SearchLogs_Id]
GO
ALTER TABLE [dbo].[MobileUserBaseDevices]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MobileUserBaseDevices_dbo.Devices_Device_Id] FOREIGN KEY([Device_Id])
REFERENCES [dbo].[Devices] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MobileUserBaseDevices] CHECK CONSTRAINT [FK_dbo.MobileUserBaseDevices_dbo.Devices_Device_Id]
GO
ALTER TABLE [dbo].[MobileUserBaseDevices]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MobileUserBaseDevices_dbo.MobileUsers_MobileUserBase_Id] FOREIGN KEY([MobileUserBase_Id])
REFERENCES [dbo].[MobileUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MobileUserBaseDevices] CHECK CONSTRAINT [FK_dbo.MobileUserBaseDevices_dbo.MobileUsers_MobileUserBase_Id]
GO
ALTER TABLE [dbo].[MobileUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MobileUsers_dbo.UserProfiles_ProfileId] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[MobileUsers] CHECK CONSTRAINT [FK_dbo.MobileUsers_dbo.UserProfiles_ProfileId]
GO
ALTER TABLE [dbo].[MobileUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MobileUsers_dbo.Users_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[MobileUsers] CHECK CONSTRAINT [FK_dbo.MobileUsers_dbo.Users_Id]
GO
ALTER TABLE [dbo].[PartySearchLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PartySearchLogs_dbo.SearchLogs_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[SearchLogs] ([Id])
GO
ALTER TABLE [dbo].[PartySearchLogs] CHECK CONSTRAINT [FK_dbo.PartySearchLogs_dbo.SearchLogs_Id]
GO
ALTER TABLE [dbo].[Passwords]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Passwords_dbo.MobileUsers_LocalMobileUserId] FOREIGN KEY([LocalMobileUserId])
REFERENCES [dbo].[MobileUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Passwords] CHECK CONSTRAINT [FK_dbo.Passwords_dbo.MobileUsers_LocalMobileUserId]
GO
ALTER TABLE [dbo].[PortalUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PortalUsers_dbo.Users_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[PortalUsers] CHECK CONSTRAINT [FK_dbo.PortalUsers_dbo.Users_Id]
GO
ALTER TABLE [dbo].[Registrations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Registrations_dbo.MobileUsers_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[MobileUsers] ([Id])
GO
ALTER TABLE [dbo].[Registrations] CHECK CONSTRAINT [FK_dbo.Registrations_dbo.MobileUsers_Id]
GO
ALTER TABLE [dbo].[Registrations]  WITH CHECK ADD  CONSTRAINT [FK_Registrations_MobileUsers_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[MobileUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Registrations] CHECK CONSTRAINT [FK_Registrations_MobileUsers_Id]
GO
ALTER TABLE [dbo].[ReturnFieldUserProfiles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReturnFieldUserProfiles_dbo.ReturnFields_ReturnField_Id] FOREIGN KEY([ReturnField_Id])
REFERENCES [dbo].[ReturnFields] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReturnFieldUserProfiles] CHECK CONSTRAINT [FK_dbo.ReturnFieldUserProfiles_dbo.ReturnFields_ReturnField_Id]
GO
ALTER TABLE [dbo].[ReturnFieldUserProfiles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ReturnFieldUserProfiles_dbo.UserProfiles_UserProfile_Id] FOREIGN KEY([UserProfile_Id])
REFERENCES [dbo].[UserProfiles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ReturnFieldUserProfiles] CHECK CONSTRAINT [FK_dbo.ReturnFieldUserProfiles_dbo.UserProfiles_UserProfile_Id]
GO
ALTER TABLE [dbo].[RolePortalUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RolePortalUsers_dbo.PortalUsers_PortalUser_Id] FOREIGN KEY([PortalUser_Id])
REFERENCES [dbo].[PortalUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RolePortalUsers] CHECK CONSTRAINT [FK_dbo.RolePortalUsers_dbo.PortalUsers_PortalUser_Id]
GO
ALTER TABLE [dbo].[RolePortalUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RolePortalUsers_dbo.Roles_Role_Id] FOREIGN KEY([Role_Id])
REFERENCES [dbo].[Roles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RolePortalUsers] CHECK CONSTRAINT [FK_dbo.RolePortalUsers_dbo.Roles_Role_Id]
GO
ALTER TABLE [dbo].[Sections]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Sections_dbo.Sections_ParentNodeId] FOREIGN KEY([ParentNodeId])
REFERENCES [dbo].[Sections] ([Id])
GO
ALTER TABLE [dbo].[Sections] CHECK CONSTRAINT [FK_dbo.Sections_dbo.Sections_ParentNodeId]
GO
ALTER TABLE [dbo].[Tokens]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tokens_dbo.MobileUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[MobileUsers] ([Id])
GO
ALTER TABLE [dbo].[Tokens] CHECK CONSTRAINT [FK_dbo.Tokens_dbo.MobileUsers_UserId]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela com informações de migração de banco de dados, gerada pelo Framework' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'__MigrationHistory'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome completo em português' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome completo em Inglês' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'NameEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome completo em espanhol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'NameES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador da AI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'Identifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Padrão do format da AI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'Format'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome abreviado da AI em português' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'Label'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome abreviado da AI em Inglês' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'LabelEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome abreviado da AI em Espanhol' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'LabelES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código do status da AI específica (GepirDomain.Gepir.AI.AIStatus)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela que armazena as definições de AI do sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AIs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador do dispositivo utilizado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Devices', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela com identificação de dispositivos utilizados pelos usuários' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Devices'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora da realização da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeviceSearchLogs', @level2type=N'COLUMN',@level2name=N'Timestamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira relacionando com tabela de log de consultas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeviceSearchLogs', @level2type=N'COLUMN',@level2name=N'LogId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira indicando o dispositivo com o qual a consulta foi realizada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeviceSearchLogs', @level2type=N'COLUMN',@level2name=N'DeviceId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela com log de consultas em dispositivos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DeviceSearchLogs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CPF/CNPJ do proprietário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'CpfCnpj'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Razão social do proprietário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'RazaoSocial'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GTIN do item pesquisado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Gtin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Prefixo do item pesquisado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'NrPrefixo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do tipo de GTIN pesquisado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DescTipoGtin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Marca do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'MarcaProduto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código interno do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'CodigoInterno'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do produto pesquisado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Descricao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição a ser impressa no resultado da consulta ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DescricaoImpressao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora da inclusão da entrada de log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DtInclusao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da suspenção ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DtSuspensao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da reativação' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DtReativacao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data do cancelamento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DtCancelamento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data da reutilização' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DtReutilizacao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'País do proprietário do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Pais'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do tipo de produto (caixa, item)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'NomeTipoProduto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Linguagem na qual o produto esá descrito' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Lingua'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado/Porvíncia' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Estado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tarifa IPI' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'TIPI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código de registro do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'CodigoRegistro'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Largura do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Largura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comprimento do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Profundidade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Altura do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Altura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Peso do produto sem embalagem' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'PesoLiquido'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Peso do produto com embalagem' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'PesoBruto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag que indica se dados de produtos podem ser exibidos para usuários ou não' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'CompartilhaDado'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do status do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DescStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Observações da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'Observacoes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Campo NCM do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'NCM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GTIN inferior do produto' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'GtinInferior'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Quantidade de itens por caixa ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'QtdeItens'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descrição do produto especificado no GTIN inferior' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'DescricaoInferior'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status do prefixo do GTIN' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog', @level2type=N'COLUMN',@level2name=N'StatusPrefixo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de pesquisa por itens completos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FullItemSearchLog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GTIN pesquisado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItemSearchLogs', @level2type=N'COLUMN',@level2name=N'GTIN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GLN do fabricante' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItemSearchLogs', @level2type=N'COLUMN',@level2name=N'ManufacturerGLN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItemSearchLogs', @level2type=N'COLUMN',@level2name=N'ItemName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da marca' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItemSearchLogs', @level2type=N'COLUMN',@level2name=N'BrandName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItemSearchLogs', @level2type=N'COLUMN',@level2name=N'TradeItemUnitDescriptor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log de busca por informações dos itens' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItemSearchLogs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora do evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntries', @level2type=N'COLUMN',@level2name=N'Timestamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo do evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntries', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuário executor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntries', @level2type=N'COLUMN',@level2name=N'Executor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resultado do evento logado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntries', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela com entradas de log para todas as ações das aplicações' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogEntries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de relacionamento entre os dispositivos utilizados e usuários' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUserBaseDevices'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sobrenome de cadastro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'LastName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gênero' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'Gender'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cidade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'City'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado / Província' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'País' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'Country'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira indicando a qual perfil usuário pertence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'ProfileId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de cadastro do usuário (mobile/oauth)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'Discriminator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID do proveddor da identificação do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'ProviderId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de nascimento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers', @level2type=N'COLUMN',@level2name=N'BirthDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de identificação de todos os usuários mobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MobileUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GLN da emresa consultada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'GLN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'GCP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome da empresa pesquisada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'PartyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Logradouro da empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'StreetAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Caixa postal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'POBoxNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CEP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'PostalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cidade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'City'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código do país no formato ISO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'CountryISOCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Perfil da empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs', @level2type=N'COLUMN',@level2name=N'PartyRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Log de busca por informações da empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PartySearchLogs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A senha da app' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Passwords', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de criação da senha' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Passwords', @level2type=N'COLUMN',@level2name=N'CreationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira indicando o usuáro possuidor de determinado histórico de senhas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Passwords', @level2type=N'COLUMN',@level2name=N'LocalMobileUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela com histórico de senhas de usuários da aplicação' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Passwords'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PortalUsers', @level2type=N'COLUMN',@level2name=N'Identification'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status do cadastro do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PortalUsers', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela que identifica os usuários de portal do sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PortalUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora do evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'Timestamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuário executor da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'Reporter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código consultado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de código consultado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'CodeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de código de barras consultado, quando aplicável' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'BarcodeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Motivo do evento logado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'Reason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comentários do usuário a respeito do problema com código' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'Text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link para a foto do código consultado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'ImageLink'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Latitude da localização do usuário executor, quando disponível' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'Latitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longitude da localização do usuário executor, quando disponível' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports', @level2type=N'COLUMN',@level2name=N'Longitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de log de erros de consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ProblemReports'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primeiro nome do usuário registrado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'FirstName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sobrenome do usuário registrado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'LastName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Email do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Senha criptografada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gênero' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'Gender'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cidade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'City'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado/Província' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'País' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'Country'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador do dispositivo utilizado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'DeviceId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave estrangeira que liga a tabela de usuários mobile com registros dos usuários' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data de nascimento do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations', @level2type=N'COLUMN',@level2name=N'BirthDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela com registros de usuários nas apps' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Registrations'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do campo de retorno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnFields', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Categoria do campo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnFields', @level2type=N'COLUMN',@level2name=N'Category'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de cadastro de campos de retorno das consultas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnFields'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de relacionamento que define os campos de retorno para consultas dos perfis' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReturnFieldUserProfiles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de relacionamento entre níveis de acesso e usuários de portal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePortalUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Níveis de acesso de usuários do portal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Roles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data/Hora do evento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'Timestamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dispositivo com o qual a consulta foi realizada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'Device'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuário executor da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'Executor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Método de consulta disparado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'Method'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código consultado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de código de barras identificado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'BarcodeType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave do proprietário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'OwnerKey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de respostas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'NumberOfHits'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GLN do servidor responsável por resposta da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'ResponderGLN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código de retorno da consulta' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'ReturnCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Latitude da localização do dispositivo no momento da consulta (quando aplicável)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'Latitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Longitude da localização do dispositivo no momento da consulta (quando aplicável)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs', @level2type=N'COLUMN',@level2name=N'Longitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela com logs de consultas' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SearchLogs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave da seção' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sections', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor a ser exibido para a seção (Texto amigável)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sections', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Linguagem da seção configurada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sections', @level2type=N'COLUMN',@level2name=N'Language'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID do nó superior à seção' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sections', @level2type=N'COLUMN',@level2name=N'ParentNodeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de mapeamento de seções das aplicações' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sections'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''Schema'' ou ''Namespace'' da configuração. Especifica o domínio da configuração específica' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Settings', @level2type=N'COLUMN',@level2name=N'Collection'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Chave de identificação da configuração' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Settings', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor da configuração específica' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Settings', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de chaves-valores com informações da configuração do sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Settings'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Valor do token (GUID)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Tokens', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data em que o token foi utilizado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Tokens', @level2type=N'COLUMN',@level2name=N'ValidationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuário logado no momento da autenticação' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Tokens', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resgistra tokens GUID gerado pela aplicação' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Tokens'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nome do perfil' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserProfiles', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Limite de consultas diárias' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserProfiles', @level2type=N'COLUMN',@level2name=N'DailyLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Limite de consultas semanais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserProfiles', @level2type=N'COLUMN',@level2name=N'WeekLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Limite de consultas mensais' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserProfiles', @level2type=N'COLUMN',@level2name=N'MonthLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tabela de cadastro de perfis de usuários' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserProfiles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Email' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Provedor do usuário (local, facebook, etc)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Provider'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de autenticação do usuários (portal, moibile, etc)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primeiro nome do usuário' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'FirstName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Informações do usuário do sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users'
GO
USE [master]
GO
ALTER DATABASE [inbarHMG] SET  READ_WRITE 
GO
