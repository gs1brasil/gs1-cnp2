﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GS1.CNP.BLL.CRM
{
    public class Busca
    {
        // Busca Estado pela UF
        public static Entity buscarEstado(IOrganizationService service, String nome)
        {
            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                             <entity name='smart_estado'>
                             <attribute name='smart_estadoid'/>
                             <filter>
                                 <condition attribute='smart_id' operator='eq' value='" + nome + @"'/>
                             </filter>
                             </entity>
                             </fetch>";


            EntityCollection _result = service.RetrieveMultiple(new FetchExpression(fetch));


            Entity retorno = new Entity();

            foreach (Entity
                i in _result.Entities)
            {
                retorno = i;
            }

            return retorno;
        }
        // Busca Área Profissional Pelo Nome
        public static Entity buscarAreaProfissional(IOrganizationService service, String nome)
        {
            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                             <entity name='smart_areaprofissional'>
                             <attribute name='smart_areaprofissionalid'/>
                             <filter>
                                 <condition attribute='smart_name' operator='eq' value='" + nome + @"'/>
                             </filter>
                             </entity>
                             </fetch>";


            EntityCollection _result = service.RetrieveMultiple(new FetchExpression(fetch));


            Entity retorno = new Entity();

            foreach (Entity
                i in _result.Entities)
            {
                retorno = i;
            }

            return retorno;
        }
        // Busca Ramo de Atividade Pelo nome
        public static Entity buscarRamoAtividade(IOrganizationService service, String nome)
        {
            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                             <entity name='smart_ramodeatividade'>
                             <attribute name='smart_ramodeatividadeid'/>
                             <filter>
                                 <condition attribute='smart_name' operator='eq' value='" + nome + @"'/>
                             </filter>
                             </entity>
                             </fetch>";


            EntityCollection _result = service.RetrieveMultiple(new FetchExpression(fetch));


            Entity retorno = new Entity();

            foreach (Entity
                i in _result.Entities)
            {
                retorno = i;
            }

            return retorno;
        }

        public static Entity buscarPais(IOrganizationService service, String nome)
        {
            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                             <entity name='smart_pais'>
                             <attribute name='smart_paisid'/>
                             <filter>
                                 <condition attribute='smart_name' operator='eq' value='" + nome + @"'/>
                             </filter>
                             </entity>
                             </fetch>";


            EntityCollection _result = service.RetrieveMultiple(new FetchExpression(fetch));


            Entity retorno = new Entity();

            foreach (Entity
                i in _result.Entities)
            {
                retorno = i;
            }

            return retorno;
        }

        public static string GetOptionsSetTextOnValue(IOrganizationService service, string entitySchemaName, string attributeSchemaName, int optionsetValue)
        {
            RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entitySchemaName,
                LogicalName = attributeSchemaName,
                RetrieveAsIfPublished = true
            };
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string metadata = string.Empty;
            if (optionList.Length > 0)
            {
                metadata = (from a in optionList
                            where a.Value == optionsetValue
                            select a.Label.UserLocalizedLabel.Label).First();
            }
            return metadata;
        }

        public static int BuscarValorPicklistPeloNome(string nome, string atributo = null, string entidade = null, IOrganizationService service = null)
        {
            try
            {
                //Retorna todos os valores do PickList a partir de sua entidade e campo
                OptionMetadataCollection opcoes = BuscarPicklist(atributo, entidade, service);

                //Busca entre suas opções o nome informado
                foreach (var opcao in opcoes)
                {
                    if (opcao.Label.LocalizedLabels.FirstOrDefault().Label.ToString() == nome)
                    {
                        //Retorna o valor no PickList referente ao nome buscado
                        return opcao.Value.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível buscar o Tipo de Logradouro\n" + ex.Message);
            }

            return -1;
        }

        //Retorna todos os itens de um PickList, a partir de sua entidade e campo referentes
        public static OptionMetadataCollection BuscarPicklist(string atributo = null, string entidade = null, IOrganizationService service = null)
        {

            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entidade,
                LogicalName = atributo,
                RetrieveAsIfPublished = true,
            };

            RetrieveAttributeResponse attributeResponse = (RetrieveAttributeResponse)service.Execute(attributeRequest);
            AttributeMetadata attrMetadata = (AttributeMetadata)attributeResponse.AttributeMetadata;
            PicklistAttributeMetadata picklistMetadata = new PicklistAttributeMetadata();
            StatusAttributeMetadata statusMetadata = new StatusAttributeMetadata();
            StateAttributeMetadata stateMetadata = new StateAttributeMetadata();

            if (atributo != "statuscode" && atributo != "statecode")
            {
                picklistMetadata = (PicklistAttributeMetadata)attrMetadata;
                return picklistMetadata.OptionSet.Options;
            }
            return null;
        }

    }
}
