﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ConexaoCRM;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;
using System.IO;
using GS1.CNP.BLL.Backoffice;


namespace GS1.CNP.BLL.CRM
{
    public class CRMTRADE
    {
        private Conexao _conexao;

        public CRMTRADE()
        {
            // Instancia classe de conexão do CRM
            _conexao = new Conexao();

            // Chama método de conexão
            _conexao.ConectaCRM();

            // Retorna objeto service, necessário para realizar operações no Dynamics CRM, como buscar, criar, atualizar e deletar registros.
            IOrganizationService service = _conexao.service;
        }

        public UsuarioCRM ConsultarUsuarioCRM(string CPFCNPJ)
        {
            UsuarioCRM usuarioCRM = null;

            Entity retornoUsuario = new Entity();
            retornoUsuario = buscarEmpresabyCPFCNPJ(CPFCNPJ);

            if (retornoUsuario != null)
            {
                if (retornoUsuario.Attributes != null && retornoUsuario.Attributes.Count > 0)
                {
                    usuarioCRM = new UsuarioCRM();
                    usuarioCRM.cpfcnpj = CPFCNPJ;
                    usuarioCRM.id = retornoUsuario.GetAttributeValue<Guid>("accountid");

                    if (buscarAcessoTrade(usuarioCRM.id))
                    {                        
                        usuarioCRM.nome = retornoUsuario.GetAttributeValue<string>("name");
                        usuarioCRM.bl_adimplente = buscarStatusAdimplente(usuarioCRM.id);
                        usuarioCRM.bl_premium = buscarStatusPremium(usuarioCRM.id);
                        usuarioCRM.bl_acessoTrade = true;
                    }
                    else
                    {
                        usuarioCRM.bl_acessoTrade = false;
                    }

                    return usuarioCRM;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        // Buscar Cliente pelo CPF/CNPJ
        public Entity buscarEmpresabyCPFCNPJ(String CPFCNPJ)
        {
            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                <entity name='account'>
                                <attribute name='name' />
                                <attribute name='accountid' />
                                <attribute name='smart_condicaoderelacionamento' />
                                <attribute name='accountnumber' />
                                <order attribute='name' descending='false' />
                                <filter type='and'>
                                    <condition attribute='smart_cnpjcpf' operator='eq' value='" + CPFCNPJ + @"' />
                                    <condition attribute='smart_condicaoderelacionamento' operator='not-in'>
                                        <value>3</value>
                                        <value>4</value>
                                        <value>1</value>
                                    </condition>
                                </filter>
                                </entity>
                            </fetch>";

            EntityCollection _result = this._conexao.service.RetrieveMultiple(new FetchExpression(fetch));

            if (_result.Entities != null && _result.Entities.Count > 0)
            {
                Entity retorno = new Entity();

                foreach (Entity i in _result.Entities)
                {
                    retorno = i;
                }

                return retorno;
            }
            else
                return null;
        }

        // Buscar Cliente pelo CPF/CNPJ
        public Entity buscarEmpresabyEmail(String email)
        {
            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                <entity name='account'>
                                <attribute name='name' />
                                <attribute name='accountid' />
                                <attribute name='smart_condicaoderelacionamento' />
                                <attribute name='accountnumber' />
                                <order attribute='name' descending='false' />
                                <filter type='and'>
                                    <condition attribute='emailaddress1' operator='eq' value='" + email + @"' />
                                    <condition attribute='smart_condicaoderelacionamento' operator='not-in'>
                                        <value>3</value>
                                        <value>4</value>
                                        <value>1</value>
                                    </condition>
                                </filter>
                                </entity>
                            </fetch>";

            EntityCollection _result = this._conexao.service.RetrieveMultiple(new FetchExpression(fetch));

            if (_result.Entities != null && _result.Entities.Count > 0)
            {
                Entity retorno = new Entity();

                foreach (Entity i in _result.Entities)
                {
                    retorno = i;
                }

                return retorno;
            }
            else
                return null;
        }

        // Buscar Status Financeiro Cliente
        public Boolean buscarStatusAdimplente(Guid CodigoUsuario)
        {
            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                  <entity name='smart_gestaodelicencaseproduto'>
                                    <attribute name='smart_gestaodelicencaseprodutoid' />
                                    <attribute name='smart_name' />
                                    <attribute name='smart_financeirostatus' />
                                    <attribute name='smart_associado' />
                                    <order attribute='smart_name' descending='false' />
                                    <filter type='and'>
                                      <filter type='and'>
                                        <condition attribute='smart_associado' operator='eq' uitype='account' value='{" + CodigoUsuario.ToString() + @"}' />
                                        <condition attribute='smart_financeirostatus' operator='not-in'>
                                          <value>1</value>
                                          <value>3</value>
                                        </condition>
                                      </filter>
                                    </filter>
                                  </entity>
                                </fetch>";

            EntityCollection _result = this._conexao.service.RetrieveMultiple(new FetchExpression(fetch));

            if (_result.Entities != null && _result.Entities.Count > 0)
            {
                return true;
            }

            return false;
        }

        // Buscar Status Financeiro Cliente
        public Boolean buscarStatusPremium(Guid CodigoUsuario)
        {
            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                  <entity name='smart_licenca'>
                                    <attribute name='smart_licencaid' />
                                    <filter type='and'>
                                      <filter type='and'>
                                        <condition attribute='smart_negocioname' operator='like' value='%EPC%' />
                                        <condition attribute='smart_pap_status' operator='eq' value='2' />
                                      </filter>
                                      <condition attribute='smart_empresa' operator='eq' uitype='account' value='{" + CodigoUsuario.ToString() + @"}' />
                                    </filter>
                                  </entity>
                                </fetch>";

            EntityCollection _result = this._conexao.service.RetrieveMultiple(new FetchExpression(fetch));

            if (_result.Entities != null && _result.Entities.Count > 0)
            {
                return true;
            }

            return false;
        }

        // Buscar Status Financeiro Cliente
        public Boolean buscarAcessoTrade(Guid CodigoUsuario)
        {


            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                  <entity name='smart_licenca'>
                                    <attribute name='smart_licencaid' />
                                    <filter type='and'>                                      
                                        <condition attribute='smart_negocioname' operator='like' value='%GUIA DE PARCEIROS%' />                                        
                                        <condition attribute='smart_pap_status' operator='eq' value='2' />
                                        <condition attribute='smart_empresa' operator='eq' uitype='account' value='{" + CodigoUsuario.ToString() + @"}' />
                                    </filter>
                                  </entity>
                                </fetch>";

            EntityCollection _result = this._conexao.service.RetrieveMultiple(new FetchExpression(fetch));

            if (_result.Entities != null && _result.Entities.Count > 0)
            {
                return true;
            }

            return false;
        }

        // Buscar Clientes
        public Boolean buscarStatusUsuarios(List<IntegracaoCrmBO.CpfCnpjFornecedor> fornecedores)
        {
            StringBuilder stringFornecedores = new StringBuilder();

            foreach (IntegracaoCrmBO.CpfCnpjFornecedor fornecedor in fornecedores)
            {
                stringFornecedores.Append(" <condition attribute='smart_cnpjcpf' operator='eq' value='"+ fornecedor.cpfcnpj +"' />");

            }

//            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true' >
//                                <entity name='account' >
//                                    <attribute name='accountid' />
//                                    <attribute name='smart_cnpjcpf' />
//                                    <order attribute='name' descending='false' />
//                                    <filter type='and' >
//                                        <filter type='or' >"
//                                        + stringFornecedores +
//                                        @"</filter>
//                                    </filter>
//                                    <link-entity name='smart_licenca' from='smart_empresa' to='accountid' alias='ag'>
//                                        <attribute name='smart_empresa' />
//                                        <attribute name='smart_negocioname' />
//                                        <attribute name='smart_negocio' />
//                                        <attribute name='smart_pap_status' />
//                                    </link-entity>
//                                    <link-entity name='smart_gestaodelicencaseproduto' from='smart_associadotransferencia' to='accountid' alias='ah'>
//                                        <attribute name='smart_associado' />
//                                        <attribute name='smart_financeirostatus' />
//                                    </link-entity>
//                                </entity>                                
//                            </fetch>";


            String fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true' >
                                <entity name='account' >
                                    <attribute name='accountid' />
                                    <attribute name='smart_cnpjcpf' />
                                    <order attribute='name' descending='false' />
                                    <filter type='and' >
                                        <filter type='or' >"
                                     + stringFornecedores +
                                     @"</filter>
                                    </filter>
                                    <link-entity name='smart_licenca' from='smart_empresa' to='accountid' alias='ag'>
                                        <attribute name='smart_empresa' />
                                        <attribute name='smart_negocioname' />
                                        <attribute name='smart_negocio' />
                                        <attribute name='smart_pap_status' />
                                    </link-entity>
                                    <link-entity name='smart_gestaodelicencaseproduto' from='smart_associado' to='accountid' alias='ah'>
                                        <attribute name='smart_associado' />
                                        <attribute name='smart_financeirostatus' />
                                    </link-entity>
                                </entity>                                
                            </fetch>";


            EntityCollection _result = this._conexao.service.RetrieveMultiple(new FetchExpression(fetch));



        
            if (_result.Entities != null && _result.Entities.Count > 0)
            {
                List<FornecedorCRM> fornecedoresCRM = new List<FornecedorCRM>();
                foreach (Entity entity in _result.Entities)
                {
                    FornecedorCRM fornecedor = new FornecedorCRM();
                    if (entity.Attributes.Contains("accountid"))
                        fornecedor.id = (System.Guid)entity.Attributes["accountid"];
                    if (entity.Attributes.Contains("smart_cnpjcpf"))
                        fornecedor.CPFCNPJ = entity.Attributes["smart_cnpjcpf"].ToString();
                    if (entity.Attributes.Contains("ag.smart_negocio"))
                        fornecedor.negocioname = ((EntityReference)((AliasedValue)((entity.Attributes["ag.smart_negocio"]))).Value).Name.ToString();                    
                    if(entity.Attributes.Contains("ag.smart_pap_status"))
                        fornecedor.papstatus = ((OptionSetValue)((AliasedValue)(entity.Attributes["ag.smart_pap_status"])).Value).Value.ToString();
                    if (entity.Attributes.Contains("ah.smart_financeirostatus"))
                        fornecedor.financeirostatus = ((OptionSetValue)(((AliasedValue)(entity.Attributes["ah.smart_financeirostatus"])).Value)).Value.ToString();

                    fornecedoresCRM.Add(fornecedor);
                }

                IntegracaoCrmBO integracaoCrmBO = new IntegracaoCrmBO();
                integracaoCrmBO.UpdateUsuariosCRM(Serialize(fornecedoresCRM));

                return true;
            }

            return false;
        }

        private string Serialize(List<FornecedorCRM> records)
        {
            string retVal = null;
            using (var tw = new StringWriter())
            using (var xw = new XmlTextWriter(tw))
            {
                var ser = new DataContractSerializer(typeof(List<FornecedorCRM>));
                ser.WriteObject(xw, records);
                retVal = tw.ToString();
            }
            return retVal;
        }
       
    }

    public class FornecedorCRM
    {
        public Guid id;
        public string CPFCNPJ;
        public string negocioname;
        public string papstatus;
        public string financeirostatus;
    }
}
