﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ConexaoCRM;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel;
using GS1.CNP.BLL.Backoffice;


namespace GS1.CNP.BLL.CRM
{
    public class ClientePotencial
    {
        public void criaClientePotencial(List<IntegracaoCrmBO.UsuarioProspect> usuarios)
        {

            List<Entity> entities = new List<Entity>();

            Conexao conexao = new Conexao();

            conexao.ConectaCRM();

            IOrganizationService service = conexao.service;

            foreach (IntegracaoCrmBO.UsuarioProspect usuario in usuarios)
            {

                Entity entClientePotencial = new Entity("lead");

                //CNPJ/CPF
                if (!String.IsNullOrEmpty(usuario.cpfcnpj) && !String.IsNullOrWhiteSpace(usuario.cpfcnpj))
                    entClientePotencial.Attributes["smart_cnpjcpf"] = usuario.cpfcnpj;

                if (usuario.tipopessoa == "1") { 
                // Nome do Contato
                if (!String.IsNullOrWhiteSpace(usuario.nome) && !String.IsNullOrWhiteSpace(usuario.nome))
                    entClientePotencial.Attributes["firstname"] = usuario.nome;
                }
                else if (usuario.tipopessoa == "2")
                {
                    if (!String.IsNullOrWhiteSpace(usuario.nome) && !String.IsNullOrWhiteSpace(usuario.nome))
                        entClientePotencial.Attributes["companyname"] = usuario.nome;
                }

                // Email
                if (!String.IsNullOrWhiteSpace(usuario.email) && !String.IsNullOrWhiteSpace(usuario.email))
                    entClientePotencial.Attributes["emailaddress1"] = usuario.email;

                //// Estado
                if (!String.IsNullOrWhiteSpace(usuario.nomeestado) && !String.IsNullOrWhiteSpace(usuario.nomeestado))
                    entClientePotencial.Attributes["address1_stateorprovince"] = usuario.nomeestado;

                // CEP
                if (!String.IsNullOrWhiteSpace(usuario.cep) && !String.IsNullOrWhiteSpace(usuario.cep))
                    entClientePotencial.Attributes["address1_postalcode"] = usuario.cep;

                // Endereco
                if (!String.IsNullOrWhiteSpace(usuario.endereco) && !String.IsNullOrWhiteSpace(usuario.endereco))
                    entClientePotencial.Attributes["address1_line1"] = usuario.endereco;
                // Tipo de Logradouro

                // Cidade
                if (!String.IsNullOrWhiteSpace(usuario.nomecidade) && !String.IsNullOrWhiteSpace(usuario.nomecidade))
                    entClientePotencial.Attributes["address1_city"] = usuario.nomecidade;
  
                // Telefone 1
                if (!String.IsNullOrWhiteSpace(usuario.telefone) && !String.IsNullOrWhiteSpace(usuario.telefone))
                    entClientePotencial.Attributes["telephone1"] = usuario.telefone;

                entClientePotencial["industrycode"] = new OptionSetValue(Convert.ToInt32(usuario.setor)); //Setor - Agricultura
                entClientePotencial["statuscode"] = new OptionSetValue(100000000);

                entities.Add(entClientePotencial);
            }
            BulkCreate(service, entities);
        }
        /// <summary>
        /// Call this method for bulk Create
        /// </summary>
        /// <param name="service">Org Service</param>
        /// <param name="entities">Collection of entities to Create</param>
        public static void BulkCreate(IOrganizationService service, List<Entity> entities)
        {
            // Create an ExecuteMultipleRequest object.
            var multipleRequest = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            // Add a CreateRequest for each entity to the request collection.
            foreach (var entity in entities)
            {
                CreateRequest createRequest = new CreateRequest { Target = entity };
                multipleRequest.Requests.Add(createRequest);
            }

            // Execute all the requests in the request collection using a single web method call.
            ExecuteMultipleResponse multipleResponse = (ExecuteMultipleResponse)service.Execute(multipleRequest);

        }
    }
}
