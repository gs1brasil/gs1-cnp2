﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.ServiceModel.Description;

// Microsoft Dynamics CRM
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Services.Utility;

namespace ConexaoCRM
{
    public class Conexao
    {
        // CRM
        public OrganizationServiceProxy serviceProxy { get; set; }
        public IOrganizationService service { get; set; }


        /// <summary>
        /// Método de conexão com o CRM
        /// </summary>
        public void ConectaCRM()
        {
            ServerConnection serverConnect = new ServerConnection();
            ServerConnection.Configuration serverConfig = serverConnect.GetServerConfiguration(ConfigurationManager.AppSettings["CrmURL"],
                                                                                            Convert.ToBoolean(ConfigurationManager.AppSettings["CrmSSL"]),
                                                                                            ConfigurationManager.AppSettings["CrmUser"],
                                                                                            ConfigurationManager.AppSettings["CrmPassword"],
                                                                                            ConfigurationManager.AppSettings["CrmOrganization"],
                                                                                            ConfigurationManager.AppSettings["CrmDomain"]);
            //ServerConnection.Configuration serverConfig = serverConnect.GetServerConfiguration("192.168.70.183:5555",
            //                                                                                Convert.ToBoolean("false"),
            //                                                                                "SVC.CRM",
            //                                                                                "Gs1!27072013!",
            //                                                                                "GS1 Brasil",
            //                                                                                "GS1BR");
            using (serviceProxy = new OrganizationServiceProxy(serverConfig.OrganizationUri,
                                                                serverConfig.HomeRealmUri,
                                                                serverConfig.Credentials,
                                                                serverConfig.DeviceCredentials))
            {
                serviceProxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());
                serviceProxy.EnableProxyTypes();
                service = (IOrganizationService)serviceProxy;
            }
        }
    }
}
