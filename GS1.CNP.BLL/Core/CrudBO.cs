﻿// -----------------------------------------------------------------------
// <copyright file="CrudBO.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using GS1.CNP.DAL;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace GS1.CNP.BLL.Core
{
    using System.Collections.Generic;
    using System.Data;
    using Newtonsoft.Json;
    using System.Web;
    using System.Web.SessionState;
    using GS1.CNP.BLL.Model;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public abstract class CrudBO : IRequiresSessionState
    {
        /// <summary>
        /// Nome da tabela referente ao BO, deve ser informado na classe herdada
        /// </summary>
        public abstract string nomeTabela { get; }

        /// <summary>
        /// Nome do campo que é chave primária, deve ser informado na classe herdada
        /// </summary>
        public abstract string nomePK { get; }

        /// <summary>
        /// Nome da Chave do banco de dados. Para aplicações normais, pode ser setado aqui. Caso seja multiBanco pode ser setado no BO herdado
        /// </summary>
        public virtual string chaveDB
        {
            //TODO: Mudar para chave do banco SQL
            get { return "BD"; }
        }

        /// <summary>
        /// Define se a chave primária da tabela é autoincremento ou não
        /// </summary>
        public virtual bool isIdentity
        {
            get { return true; }
        }

        public Login usuario
        {
            get { return this.getSession("USUARIO_LOGADO") as Login; }
        }

        private ConfigCRUD _config { get; set; }

        public CrudBO()
        {
            _config = new ConfigCRUD() { isIdentity = this.isIdentity, nomeTabela = this.nomeTabela, nomePK = this.nomePK };
        }

        protected void setSession(string key, object value)
        {
            //TODO tratar session
            if (HttpContext.Current.Server != null)
            {
                HttpContext.Current.Session.Add(key, value);
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }

        protected object getSession(string key)
        {
            //TODO tratar session
            if (HttpContext.Current.Server != null)
            {
                object sessionKey = HttpContext.Current.Session[key];
                return sessionKey;
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }

        public virtual string Insert(dynamic parametro)
        {
            //Cria a conexão com o banco de Dados
            Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase(chaveDB);

            dynamic obj = new DynamicJsonObject(new Dictionary<string, object>());
            obj = parametro;
            string str = JsonConvert.SerializeObject(obj);

            DataAccessParse parse = new DataAccessParse(str, this._config);

            IDataReader reader = db.ExecuteReader(System.Data.CommandType.Text, parse.GeraComando(EnumComandos.INSERT));
            return new Util().ConvertReaderTOJson(reader);
        }

        public virtual void Delete(dynamic parametro)
        {
            //Cria a conexão com o banco de Dados
            Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase(chaveDB);

            dynamic obj = new DynamicJsonObject(new Dictionary<string, object>());
            obj = parametro;
            string str = JsonConvert.SerializeObject(obj);

            DataAccessParse parse = new DataAccessParse(str, this._config);

            db.ExecuteNonQuery(System.Data.CommandType.Text, parse.GeraComando(EnumComandos.DELETE));
        }

        public virtual string SelectAll(dynamic parametro)
        {
            //Cria a conexão com o banco de Dados
            Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase(chaveDB);

            DataAccessParse parse = new DataAccessParse("", this._config);

            IDataReader reader = db.ExecuteReader(System.Data.CommandType.Text, parse.GeraComando(EnumComandos.SELECTALL));
            return new Util().ConvertReaderTOJson(reader);
        }

        public virtual string SelectFiltro(dynamic parametro)
        {
            //Cria a conexão com o banco de Dados
            Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase(chaveDB);

            dynamic obj = new DynamicJsonObject(new Dictionary<string, object>());
            obj = parametro;
            string str = JsonConvert.SerializeObject(obj);

            DataAccessParse parse = new DataAccessParse(str, this._config);

            IDataReader reader = db.ExecuteReader(System.Data.CommandType.Text, parse.GeraComando(EnumComandos.SELECTFILTRO));

            return new Util().ConvertReaderTOJson(reader);
        }

        public virtual void Update(dynamic parametro)
        {
            //Cria a conexão com o banco de Dados
            Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase(chaveDB);

            dynamic obj = new DynamicJsonObject(new Dictionary<string, object>());
            obj = parametro;
            string str = JsonConvert.SerializeObject(obj);

            DataAccessParse parse = new DataAccessParse(str, this._config);

            db.ExecuteNonQuery(System.Data.CommandType.Text, parse.GeraComando(EnumComandos.UPDATE));
        }

        public virtual bool validaPermissaoBO(GS1CNPAttribute attrMethod)
        {
            List<string> permissoes = HttpContext.Current.Session["PERMISSOES_USUARIO_LOGADO"] as List<string>;

            if (!attrMethod.GS1CNPValidaPermissao(permissoes))
            {
                throw new GS1TradeException("Sem permissão para acessar esta funcionalidade.");
            }

            return true;
        }
    }
}
