﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using Microsoft.Office.Interop.Word;
using Novacode;
using System.Drawing;

namespace GS1.CNP.BLL.Core
{
    public class DocxHelper
    {
        private static readonly Novacode.Border Border = new Novacode.Border()
        {
            Color = Color.Black,
            Size = BorderSize.one,
            Tcbs = Novacode.BorderStyle.Tcbs_single
        };

        private static void SetHeaderStyle(Novacode.Cell cell, string text)
        {
            cell.FillColor = Color.Black;
            cell.VerticalAlignment = VerticalAlignment.Center;

            var paragraph = cell.Paragraphs[0].InsertParagraphAfterSelf(text, false);
            cell.Paragraphs[0].Remove(false);
            paragraph.Alignment = Alignment.center;
            paragraph.Color(Color.White);
        }

        private static Novacode.Table GetTable(DocX file, string tableName)
        {
            return file.Tables.FirstOrDefault(x => x.Rows[0].Paragraphs[0].Text.Contains(tableName));
        }

        public static string Extenso(string numero, string separator = "")
        {
            var arrayUnidade = new List<string> { "Zero", "Um", "Dois", "Três", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove" };

            var result = new List<string>();

            foreach (var unidade in numero)
            {
                result.Add(arrayUnidade[int.Parse(unidade.ToString())]);
            }

            return string.Join(separator, result);
        }

        public DocX CreateGTINTables(string pathdoc, string prefixo, string descricao, string nomelicenca)
        {
            var file = DocX.Load(HttpContext.Current.Server.MapPath(pathdoc));

            var phPrefxo = GetTable(file, "{Prefixos}");
            if(phPrefxo == null) GetTable(file, "{Prefixo}");

            var phgtin = GetTable(file, "{GTIN13}");

            var prefixoTable = file.AddTable(1, 2);
            prefixoTable.SetBorder(TableBorderType.Bottom, Border);
            prefixoTable.SetBorder(TableBorderType.Left, Border);
            prefixoTable.SetBorder(TableBorderType.Right, Border);
            prefixoTable.SetBorder(TableBorderType.Top, Border);
            prefixoTable.SetBorder(TableBorderType.InsideH, Border);
            prefixoTable.SetBorder(TableBorderType.InsideV, Border);
            prefixoTable.Rows[0].Height = 18;

            var gtinTable = file.AddTable(2, 3);
            gtinTable.SetBorder(TableBorderType.Bottom, Border);
            gtinTable.SetBorder(TableBorderType.Left, Border);
            gtinTable.SetBorder(TableBorderType.Right, Border);
            gtinTable.SetBorder(TableBorderType.Top, Border);
            gtinTable.SetBorder(TableBorderType.InsideH, Border);
            gtinTable.SetBorder(TableBorderType.InsideV, Border);
            gtinTable.Alignment = Alignment.center;
            SetHeaderStyle(gtinTable.Rows[0].Cells[0], "Prefixos GS1 de Empresa");
            SetHeaderStyle(gtinTable.Rows[0].Cells[1], "Produtos ou Locais");
            SetHeaderStyle(gtinTable.Rows[0].Cells[2], "Dígito Verificador");
            gtinTable.Rows[0].Cells[0].Width = file.PageWidth - 360;
            gtinTable.Rows[0].Cells[1].Width = 180;
            gtinTable.Rows[0].Cells[2].Width = 180;
            gtinTable.Rows[0].Height = 18;
            gtinTable.Rows[1].Height = 18;

            //Prefixo Detalhado
            prefixoTable.Rows[0].Cells[0].Paragraphs[0].InsertText(prefixo.ToString());

            if (nomelicenca != "GTIN-8")
            {
                prefixoTable.Rows[0].Cells[1].Paragraphs[0].InsertText("(" + Extenso(prefixo.ToString(), ", ").ToUpper() + ")");
                if(prefixo.ToString().Length > 8)
                    prefixoTable.Rows[0].Height = 36;                       
            }
            else
            {
                //TODO: Buscar a descrição do GTIN-8 e apresentar na tela
                prefixoTable.Rows[0].Cells[1].Paragraphs[0].InsertText(descricao);
            }

            prefixoTable.Rows[0].Cells[0].Paragraphs[0].Alignment = Alignment.center;
            prefixoTable.Rows[0].Cells[1].Paragraphs[0].Alignment = Alignment.center;
            prefixoTable.Rows[0].Cells[0].Paragraphs[0].Bold();
            prefixoTable.Rows[0].Cells[1].Paragraphs[0].Bold();
            prefixoTable.Rows[0].Cells[0].Width = 180;
            prefixoTable.Rows[0].Cells[1].Width = file.PageWidth - 180;

            //GTIN-13
            var produtos = 13 - (prefixo.ToString().Length + 1);
            if (produtos > 0)
            {
                gtinTable.Rows[1].Cells[0].Paragraphs[0].InsertText(prefixo.ToString());
                gtinTable.Rows[1].Cells[1].Paragraphs[0].InsertText(new string('X', produtos));
                gtinTable.Rows[1].Cells[2].Paragraphs[0].InsertText("X");
                gtinTable.Rows[1].Cells[0].Paragraphs[0].Alignment = Alignment.center;
                gtinTable.Rows[1].Cells[1].Paragraphs[0].Alignment = Alignment.center;
                gtinTable.Rows[1].Cells[2].Paragraphs[0].Alignment = Alignment.center;
                gtinTable.Rows[1].Cells[0].Paragraphs[0].Bold();
                gtinTable.Rows[1].Cells[1].Paragraphs[0].Bold();
                gtinTable.Rows[1].Cells[2].Paragraphs[0].Bold();
            }

            //Substituir tabelas
            if (phPrefxo != null)
            {
                phPrefxo.InsertTableAfterSelf(prefixoTable);
                phPrefxo.Remove();
            }

            if (phgtin != null)
            {
                phgtin.InsertTableAfterSelf(gtinTable);
                phgtin.Remove();
            }

            return file;
        }

        public DocX CreateProdutosTables(string pathdoc, dynamic produtos)
        {
            var file = DocX.Load(HttpContext.Current.Server.MapPath(pathdoc));

            var phProduto = GetTable(file, "{Produtos}");
            if (phProduto == null) GetTable(file, "{Produto}");

            var produtoTable = file.AddTable(Convert.ToInt32(produtos.Count) + 1, 2);
            produtoTable.Alignment = Alignment.center;
            produtoTable.SetBorder(TableBorderType.Bottom, Border);
            produtoTable.SetBorder(TableBorderType.Left, Border);
            produtoTable.SetBorder(TableBorderType.Right, Border);
            produtoTable.SetBorder(TableBorderType.Top, Border);
            produtoTable.SetBorder(TableBorderType.InsideH, Border);
            produtoTable.SetBorder(TableBorderType.InsideV, Border);
            produtoTable.Rows[0].Height = 18;
            
            SetHeaderStyle(produtoTable.Rows[0].Cells[0], "GTIN");
            SetHeaderStyle(produtoTable.Rows[0].Cells[1], "Descrição do Produto");

            for (int i = 0; i < produtos.Count; i++)
            {
                produtoTable.Rows[i+1].Cells[0].Paragraphs[0].InsertText(Convert.ToString(produtos[i].gtin));
                produtoTable.Rows[i+1].Cells[1].Paragraphs[0].InsertText(Convert.ToString(produtos[i].descricao));
            }

            if (phProduto != null)
            {
                phProduto.InsertTableAfterSelf(produtoTable);
                phProduto.Remove();
            }

            return file;

        }

        public List<string> ValidateWordDocument(string caminho)
        {

            if (caminho != null)
            {

                if (File.Exists(caminho))
                {
                    Object oMissing = System.Reflection.Missing.Value;
                    Application wordApp = new Application();
                    Document document = wordApp.Documents.Open(caminho, ref oMissing, true, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing);
                    bool isTrue = false;
                    object doNotSaveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;

                    List<string> words = new List<string>();
                    List<string> problemas = new List<string>();
                    List<string> problemasSecundario = new List<string>();
                    words.Add("{Data Extenso}");
                    words.Add("{Data}");
                    words.Add("{Razao Social}");
                    words.Add("{Contato}");
                    words.Add("{Prefixo}");
                    words.Add("{Prefixos}");

                    for (int i = 0; i < words.Count(); i++)
                    {
                        Microsoft.Office.Interop.Word.Range range = document.Content;
                        Microsoft.Office.Interop.Word.Find find = range.Find;
                        isTrue = find.Execute(words[i], null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                        if (!isTrue)
                        {
                            problemasSecundario.Add(words[i]);
                        }
                    }

                    bool hasErrorPrefixo = false, hasErrorPrefixos = false, hasErrorData = false, hasErrorDataExtenso = false;

                    for (var i = 0; i < problemasSecundario.Count; i++)
                    {
                        if (problemasSecundario[i] == "{Prefixo}")
                        {
                            hasErrorPrefixo = true;
                        }

                        if (problemasSecundario[i] == "{Prefixos}")
                        {
                            hasErrorPrefixos = true;
                        }

                        if (problemasSecundario[i] == "{Data Extenso}")
                        {
                            hasErrorDataExtenso = true;
                        }

                        if (problemasSecundario[i] == "{Data}")
                        {
                            hasErrorData = true;
                        }

                    }

                    for (var i = 0; i < problemasSecundario.Count; i++)
                    {
                        if ((hasErrorPrefixos && hasErrorPrefixo && (problemasSecundario[i] == "{Prefixos}" || problemasSecundario[i] == "{Prefixo}")) ||
                            (hasErrorData && hasErrorDataExtenso && (problemasSecundario[i] == "{Data Extenso}" || problemasSecundario[i] == "{Data}")))
                        {
                            problemas.Add(problemasSecundario[i]);
                        }
                    }

                    document.Close(ref doNotSaveChanges);
                    wordApp.Quit();

                    //return isTrue;
                    return problemas;
                }
                else
                {
                    //return false;
                    throw new GS1TradeException("Os campos do template estão fora do padrão. Por favor selecione um template válido.");
                }
            }
            else
            {
                //return false;
                throw new GS1TradeException("Os campos do template estão fora do padrão. Por favor selecione um template válido.");
            }
        }

        public static void FindAndReplace(Document document, string findText, string replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;

            Microsoft.Office.Interop.Word.Range range = document.Content;
            Microsoft.Office.Interop.Word.Find find = range.Find;

            find.Execute(findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        public FileStream ConvertPdf(DocX file)
        {
            var docFileName = Path.Combine(HttpContext.Current.Server.MapPath("~/temp"), Path.GetRandomFileName());
            var pdfFileName = Path.Combine(HttpContext.Current.Server.MapPath("~/temp"), Path.GetRandomFileName());

            file.SaveAs(docFileName);

            var appWord = new Microsoft.Office.Interop.Word.Application();

            try
            {
                var doc = appWord.Documents.Open(docFileName);

                try
                {
                    if (doc == null)
                        throw new Exception(string.Format("Não foi possível abrir o documento {0}", docFileName));

                    doc.ExportAsFixedFormat(pdfFileName, WdExportFormat.wdExportFormatPDF);
                }
                finally
                {
                    if (doc != null)
                        doc.Close();
                }
            }
            finally
            {
                appWord.Quit();
            }

            var stream = new FileStream(pdfFileName, FileMode.Open, FileAccess.Read);

            return stream;
        }
    }
}
