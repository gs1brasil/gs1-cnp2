﻿using System;
using System.Collections.Generic;

namespace GS1.CNP.BLL.Core
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple=false)]
    public class GS1CNPAttribute : Attribute
    {
        public string ChavePermissao { get; set; }
        public bool ValidarPermissao { get; set; }
        public bool LogarMetodo { get; set; }
        public string[] ListaChavePermissao { get; set; }

        public GS1CNPAttribute(string chavePermissao, bool validarPermissao = true, bool logarMetodo = false)
        {
            this.ChavePermissao = chavePermissao;
            this.ValidarPermissao = validarPermissao;
            this.LogarMetodo = logarMetodo;
            this.ListaChavePermissao = null;
        }

        public GS1CNPAttribute(bool validarPermissao = true, bool logarMetodo = false)
        {
            this.ChavePermissao = string.Empty;
            this.ValidarPermissao = validarPermissao;
            this.LogarMetodo = logarMetodo;
            this.ListaChavePermissao = null;
        }

        public GS1CNPAttribute(bool validarPermissao = true, bool logarMetodo = false, params string[] chavePermissao)
        {
            this.ChavePermissao = null;
            this.ListaChavePermissao = chavePermissao;
            this.ValidarPermissao = validarPermissao;
            this.LogarMetodo = logarMetodo;
        }

        public bool GS1CNPValidaPermissao(List<string> permissao)
        {
            if (ListaChavePermissao != null)
            {
                foreach (var item in permissao)
                {
                    foreach (var item2 in this.ListaChavePermissao)
                    {
                        if (item.Equals(item2.ToString()))
                        {
                            return true;
                        }
                    }
                }
            }
            else
            {
                foreach (var item2 in permissao)
                {
                    if (this.ChavePermissao.Equals(item2.ToString()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
