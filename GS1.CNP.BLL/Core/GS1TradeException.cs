﻿using System;
using System.Security.Permissions;
using System.Runtime.Serialization;

namespace GS1.CNP.BLL.Core
{
    [Serializable()]
    public class GS1TradeException : System.Exception
    {
        public DateTime DateOccur { get { return DateTime.Now; } }

        public GS1TradeException() { }
        public GS1TradeException(string message) : base(message) { }
        public GS1TradeException(string message, System.Exception inner) : base(message, inner) { }

        protected GS1TradeException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");

            //TODO - validar retorno correto dos erros - Criado por Eduardo Pendencia para Rafael
            info.AddValue("DateOccurrence", this.DateOccur);
            info.AddValue("Data", (this.Data == null && this.InnerException != null ? this.InnerException.Data : this.Data));
            info.AddValue("HelpLink", (this.HelpLink == null && this.InnerException != null ? this.InnerException.HelpLink : this.HelpLink));
            info.AddValue("HResult", this.HResult);
            //info.AddValue("InnerException", this.InnerException);
            info.AddValue("Message", (string.IsNullOrEmpty(this.Message) && this.InnerException != null ? this.InnerException.Message : this.Message));
            //info.AddValue("Source", (this.Source == null && this.InnerException != null ? this.InnerException.Source : this.Source));
            //info.AddValue("StackTrace", (this.StackTrace == null && this.InnerException != null ? this.InnerException.StackTrace : this.StackTrace));
            //info.AddValue("TargetSite", (this.TargetSite == null && this.InnerException != null ? this.InnerException.TargetSite : this.TargetSite));

            //base.GetObjectData(info, context);
        }
    }

    [Serializable()]
    public class GS1TradeSessionException : GS1TradeException
    {
    }
}
