﻿using System;
using System.Collections.Generic;
using System.Web;
using log4net;
using System.IO;
using System.Web.SessionState;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL.Core
{
    public class Log : IRequiresSessionState
    {
        public static void WriteError(string message, Exception exception = null)
        {
            ILog log = LogManager.GetLogger("LogError");

            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;
            HttpBrowserCapabilities browser = request.Browser != null ? request.Browser : (HttpBrowserCapabilities)context.Session["Browser"];

            string parameterRequest = string.Empty,
                email = string.Empty;
            long? id_user = null;

            if (context.Request.InputStream.CanSeek && context.Request.InputStream.Position > 0)
            {
                context.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            }

            if (context.Request.InputStream.Length > 0)
            {
                parameterRequest = (new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd();

                string metodo = string.Empty,
                    bo = string.Empty;

                GetMethodInfo(context, out bo, out metodo);

                if (!string.IsNullOrEmpty(metodo) && metodo.ToLower().Equals("logar"))
                {
                    JObject jObj = JObject.Parse(parameterRequest);

                    dynamic parametro = jObj.ToObject<dynamic>();

                    dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro);

                    if (param.where != null && param.where.senha != null)
                    {
                        ((IDictionary<string, object>)param.where).Remove("senha");
                    }

                    parameterRequest = JsonConvert.SerializeObject(param);
                }
            }

            if (context.Session != null && context.Session["USUARIO_LOGADO"] != null)
            {
                Login user = (Login)context.Session["USUARIO_LOGADO"];

                id_user = user.id;
                email = user.email;

                log4net.ThreadContext.Properties["EMAIL"] = email;
            }
            else
            {
                dynamic parameter = JsonConvert.DeserializeObject(parameterRequest);

                if (parameter != null && parameter.where != null)
                {
                    log4net.ThreadContext.Properties["EMAIL"] = parameter.where.login != null ? parameter.where.login.Value : "";
                }
            }

            log4net.ThreadContext.Properties["CODIGOUSUARIO"] = id_user;
            log4net.ThreadContext.Properties["URL"] = !string.IsNullOrEmpty(context.Request.PathInfo) ? context.Request.PathInfo : request.Url.LocalPath;
            log4net.ThreadContext.Properties["IP"] = request.ServerVariables["REMOTE_ADDR"];
            log4net.ThreadContext.Properties["NAVEGADOR"] = browser != null ? browser.Browser : string.Empty;
            log4net.ThreadContext.Properties["VERSAONAVEGADOR"] = browser != null ? browser.Version : string.Empty;
            log4net.ThreadContext.Properties["REQUISICAO"] = parameterRequest;
            //log4net.ThreadContext.Properties["SERVIDOR"] = request.ServerVariables["SERVER_NAME"];
            log4net.ThreadContext.Properties["SERVIDOR"] = request.ServerVariables["LOCAL_ADDR"];
            

            log.Error(message, exception);
        }

        public static void WriteInfo(string classe, string metodo, long? entidade = null)
        {
            ILog log = LogManager.GetLogger("LogInfo");

            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;
            HttpBrowserCapabilities browser = request.Browser != null ? request.Browser : (HttpBrowserCapabilities)context.Session["Browser"];
            
            string parameterRequest = string.Empty,
                email = string.Empty;
            long? id_user = null,
                codigoassociado = null;

            if (context.Request.InputStream.CanSeek && context.Request.InputStream.Position > 0)
            {
                context.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            }

            if (context.Request.InputStream.Length > 0)
            {
                parameterRequest = (new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd();

                if (!string.IsNullOrEmpty(metodo) && (metodo.ToLower().Equals("logar") || metodo.ToLower().Equals("login")) )
                {
                    JObject jObj = JObject.Parse(parameterRequest);

                    dynamic parametro = jObj.ToObject<dynamic>();

                    dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro);

                    if (param.where != null && param.where.senha != null)
                    {
                        ((IDictionary<string, object>)param.where).Remove("senha");
                    }

                    parameterRequest = JsonConvert.SerializeObject(param);
                }
            }

            if (context.Session != null && context.Session["USUARIO_LOGADO"] != null)
            {
                Login user = (Login)context.Session["USUARIO_LOGADO"];

                id_user = user.id;
                email = user.email;
            }

            if (context.Session != null && context.Session["ASSOCIADO_SELECIONADO"] != null)
            {
                Associado associado = context.Session["ASSOCIADO_SELECIONADO"] as Associado;

                codigoassociado = associado.codigo;
            }

            log4net.ThreadContext.Properties["CODIGOUSUARIO"] = id_user;
            log4net.ThreadContext.Properties["CLASSE"] = classe;
            log4net.ThreadContext.Properties["METODO"] = metodo;
            log4net.ThreadContext.Properties["IP"] = request.ServerVariables["REMOTE_ADDR"];
            log4net.ThreadContext.Properties["NAVEGADOR"] = browser != null ? browser.Browser : string.Empty;
            log4net.ThreadContext.Properties["VERSAONAVEGADOR"] = browser != null ? browser.Version : string.Empty;
            log4net.ThreadContext.Properties["REQUISICAO"] = parameterRequest;
            log4net.ThreadContext.Properties["CODIGOENTIDADE"] = entidade;
            log4net.ThreadContext.Properties["URL"] = !string.IsNullOrEmpty(context.Request.PathInfo) ? context.Request.PathInfo : request.Url.LocalPath;
            log4net.ThreadContext.Properties["CODIGOASSOCIADO"] = codigoassociado;

            log.Info("");
        }

        private static void GetMethodInfo(HttpContext context, out string boName, out string methodName)
        {
            boName = string.Empty;
            methodName = string.Empty;

            if (context != null)
            {
                string pathInfo = string.Empty;

                pathInfo = context.Request.PathInfo;

                if (!string.IsNullOrEmpty(pathInfo))
                {

                    if (pathInfo.StartsWith("/"))
                        pathInfo = pathInfo.Substring(1);

                    string[] path = pathInfo.Split('/');

                    boName = (path.Length > 0 ? path[0] : string.Empty);
                    methodName = (path.Length > 1 ? path[1] : string.Empty);
                }
            }
        }

        public static void WriteJsnLog()
        {
            ILog log = LogManager.GetLogger("LogError");

            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;
            HttpBrowserCapabilities browser = request.Browser != null ? request.Browser : (HttpBrowserCapabilities)context.Session["Browser"];

            string parameterRequest = string.Empty,
                email = string.Empty;
            long? id_user = null;
            dynamic param1 = null;

            if (context.Request.InputStream.CanSeek && context.Request.InputStream.Position > 0)
            {
                context.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            }

            if (context.Request.InputStream.Length > 0)
            {
                parameterRequest = (new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd();

                string metodo = string.Empty,
                    bo = string.Empty;

                GetMethodInfo(context, out bo, out metodo);               
                
                JObject jObj = JObject.Parse(parameterRequest);
                dynamic parametro = jObj.ToObject<dynamic>();
                dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.lg);
                JObject jObj2 = JObject.Parse(param[0].m);
                dynamic parametro2 = jObj2.ToObject<dynamic>();
                param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro2);

                parameterRequest = JsonConvert.SerializeObject(param1);
                
            }

            if (context.Session != null && context.Session["USUARIO_LOGADO"] != null)
            {
                Login user = (Login)context.Session["USUARIO_LOGADO"];

                id_user = user.id;
                email = user.email;
            }

            log4net.ThreadContext.Properties["CODIGOUSUARIO"] = id_user;
            log4net.ThreadContext.Properties["URL"] = !string.IsNullOrEmpty(context.Request.PathInfo) ? context.Request.PathInfo : request.Url.LocalPath;
            log4net.ThreadContext.Properties["IP"] = request.ServerVariables["REMOTE_ADDR"];
            log4net.ThreadContext.Properties["NAVEGADOR"] = browser != null ? browser.Browser : string.Empty;
            log4net.ThreadContext.Properties["VERSAONAVEGADOR"] = browser != null ? browser.Version : string.Empty;
            log4net.ThreadContext.Properties["EMAIL"] = email;
            log4net.ThreadContext.Properties["REQUISICAO"] = parameterRequest;
            //log4net.ThreadContext.Properties["SERVIDOR"] = request.ServerVariables["SERVER_NAME"];
            log4net.ThreadContext.Properties["SERVIDOR"] = request.ServerVariables["LOCAL_ADDR"];

            if (param1 != null)
            {
                log.Error(param1.message, new Exception(param1.stack));
            }
        
        }

        public static void WriteErrorWS(string message, Exception exception = null)
        {
            ILog log = LogManager.GetLogger("LogErrorWsProdutoService");

            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            string parameterRequest = string.Empty;
            if (context.Request.InputStream.CanSeek && context.Request.InputStream.Position > 0)
            {
                context.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            }

            if (context.Request.InputStream.Length > 0)
            {
                parameterRequest = (new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd();
            }

            log4net.ThreadContext.Properties["IP"] = request.ServerVariables["REMOTE_ADDR"];
            log4net.ThreadContext.Properties["REQUISICAO"] = parameterRequest;
            log4net.ThreadContext.Properties["URL"] = !string.IsNullOrEmpty(context.Request.PathInfo) ? context.Request.PathInfo : request.Url.LocalPath;
            log4net.ThreadContext.Properties["SERVIDOR"] = request.ServerVariables["LOCAL_ADDR"];



            log.Error(exception.Message, exception);
        }

        public static void WriteInfoWS(string resposta)
        {
            ILog log = LogManager.GetLogger("LogInfoWsProdutoService");

            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            
            string parameterRequest = string.Empty;
            if (context.Request.InputStream.CanSeek && context.Request.InputStream.Position > 0)
            {
                context.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            }

            if (context.Request.InputStream.Length > 0)
            {
                parameterRequest = (new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd();                
            }
            
            log4net.ThreadContext.Properties["IP"] = request.ServerVariables["REMOTE_ADDR"];
            log4net.ThreadContext.Properties["REQUISICAO"] = parameterRequest;
            log4net.ThreadContext.Properties["URL"] = !string.IsNullOrEmpty(context.Request.PathInfo) ? context.Request.PathInfo : request.Url.LocalPath;            
            log4net.ThreadContext.Properties["SERVIDOR"] = request.ServerVariables["LOCAL_ADDR"];
            log4net.ThreadContext.Properties["RESPOSTA"] = resposta;

            log.Info("");
        }
    }



    internal class ParameterHandler
    {
        public string nomeBO { get; set; }
        public string nomeMetodo { get; set; }
        public dynamic campos { get; set; }
        public dynamic where { get; set; }
        public string sql { get; set; }
        public string tipoCampo { get; set; }
        public int registroPorPagina { get; set; }
        public int paginaAtual { get; set; }
        public string ordenacao { get; set; }
    }

}