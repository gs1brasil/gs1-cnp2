﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Net.Mime;
using System.Diagnostics;
using System.Globalization;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Reflection;
using System.Web;
using GS1.CNP.DAL;
using Microsoft.Office.Interop.Word;
using System.Xml.Linq;
using Novacode;
using System.Web.Security;

namespace GS1.CNP.BLL.Core
{
    public class Util
    {
        /// <summary>
        /// Convert um datareader em string JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public string ConvertReaderTOJson(IDataReader reader, int pageNumber = 0)
        {
            if (reader != null || !reader.IsClosed)
            {
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                int rowcount = 0,
                    currentPage = 0;

                using (JsonWriter jsonWriter = new JsonTextWriter(sw))
                {
                    jsonWriter.WriteStartArray();

                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();

                        int fields = reader.FieldCount;

                        for (int i = 0; i < fields; i++)
                        {
                            if (!reader.GetName(i).ToUpper().Equals("ROWNUM") && !reader.GetName(i).ToUpper().Equals("ROWCOUNT") && !reader.GetName(i).ToUpper().Equals("CURRENTPAGE"))
                            {
                                jsonWriter.WritePropertyName(reader.GetName(i).ToLower());
                                jsonWriter.WriteValue(reader[i]);
                            }
                            else if (rowcount == 0 && reader.GetName(i).ToUpper().Equals("ROWCOUNT") && reader[i] != null)
                            {
                                rowcount = Int32.Parse(reader[i].ToString());
                            }
                            else if (currentPage == 0 && reader.GetName(i).ToUpper().Equals("CURRENTPAGE") && reader[i] != null)
                            {
                                currentPage = Int32.Parse(reader[i].ToString());
                            }
                        }

                        jsonWriter.WriteEndObject();
                    }

                    jsonWriter.WriteEndArray();
                }

                if (rowcount > 0)
                {
                    return "{\"RowCount\": " + rowcount.ToString() + ", \"CurrentPage\": " + currentPage.ToString() + ", \"data\": " + sw.ToString() + "}";
                }
                else
                {
                    return sw.ToString();
                }
            }
            else
            {
                throw new Exception("Reader não tem informação para geração do JSON");
            }
        }

        public object ConvertReaderTOExpandoObject(IDataReader reader)
        {
            if (reader != null || !reader.IsClosed)
            {
                List<ExpandoObject> retorno = new List<ExpandoObject>();

                while (reader.Read())
                {
                    dynamic linha = new ExpandoObject();
                    int fields = reader.FieldCount;

                    for (int i = 0; i < fields; i++)
                    {
                        if (!reader.GetName(i).ToUpper().Equals("ROWNUM") && !reader.GetName(i).ToUpper().Equals("ROWCOUNT") && !reader.GetName(i).ToUpper().Equals("CURRENTPAGE"))
                        {
                            (linha as IDictionary<string, object>).Add(reader.GetName(i), reader.GetValue(i));
                        }
                    }

                    retorno.Add(linha);
                }

                return retorno;
            }
            else
            {
                throw new Exception("Reader não tem informação para geração do JSON");
            }
        }

        public static object ConvertJTokenToObject(JToken token)
        {
            if (token is JValue)
            {
                return ((JValue)token).Value;
            }
            if (token is JObject)
            {
                ExpandoObject expando = new ExpandoObject();

                (from childToken in ((JToken)token) where childToken is JProperty select childToken as JProperty).ToList().ForEach(property =>
                {
                    ((IDictionary<string, object>)expando).Add(property.Name, ConvertJTokenToObject(property.Value));
                });

                return expando;
            }
            if (token is JArray)
            {
                object[] array = new object[((JArray)token).Count];
                int index = 0;

                foreach (JToken arrayItem in ((JArray)token))
                {
                    array[index] = ConvertJTokenToObject(arrayItem);
                    index++;
                }

                return array;
            }

            throw new ArgumentException(string.Format("Unknown token type '{0}'", token.GetType()), "token");
        }

        public IEnumerable<object> ToObject(IDataReader reader, Func<IDataRecord, object> BuildObject)
        {
            try
            {
                while (reader.Read())
                {
                    yield return BuildObject(reader);
                }
            }
            finally
            {
                reader.Dispose();
            }
        }

        /*public dynamic ConvertReaderTODynamic(IDataReader reader)
        {
            if (reader != null || !reader.IsClosed)
            {
                var jsonArray = new JArray();
                var jsonObject = new JObject();

                while (reader.Read())
                {
                    int fields = reader.FieldCount;

                    for (int i = 0; i < fields; i++)
                    {
                        if (!reader.GetName(i).ToUpper().Equals("ROWNUM") && !reader.GetName(i).ToUpper().Equals("ROWCOUNT") && !reader.GetName(i).ToUpper().Equals("CURRENTPAGE"))
                        {
                            jsonObject.Add(reader.GetName(i), reader.GetValue(i));
                            (linha as IDictionary<string, object>).Add(reader.GetName(i), reader.GetValue(i));
                        }
                    }

                    retorno.Add(linha);
                }

                return retorno;
            }
            else
            {
                throw new Exception("Reader não tem informação para geração do JSON");
            }
        }*/

        public static bool IsBoolean(string value)
        {
            try
            {
                Boolean.Parse(value);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsDate(string value)
        {
            try
            {
                DateTime dt = DateTime.Parse(value);

                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                {
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsDateBR(string value)
        {
            try
            {
                DateTime dt = DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                {
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsInteger(string value)
        {
            try
            {
                Int64.Parse(value);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsDecimal(string value)
        {
            try
            {
                Decimal.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string GeraSenha()
        {
            string guid = Guid.NewGuid().ToString().Replace("-", "");

            Random clsRan = new Random();
            Int32 tamanhoSenha = clsRan.Next(7, 10);

            string senha = "";
            for (Int32 i = 0; i <= tamanhoSenha; i++)
            {
                senha += guid.Substring(clsRan.Next(1, guid.Length), 1);
            }

            return senha;
        }

        public string GeraSHA1(string valor)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(valor, "SHA1");
        }

        public string GerarMD5(string valor)
        {
            // Cria uma nova intância do objeto que implementa o algoritmo para criptografia MD5
            MD5 md5Hasher = MD5.Create();

            // Criptografa o valor passado
            byte[] valorCriptografado = md5Hasher.ComputeHash(Encoding.Default.GetBytes(valor));

            // Cria um StringBuilder para passarmos os bytes gerados para ele
            StringBuilder strBuilder = new StringBuilder();

            // Converte cada byte em um valor hexadecimal e adiciona ao string builder and format each one as a hexadecimal string.
            for (int i = 0; i < valorCriptografado.Length; i++)
            {
                strBuilder.Append(valorCriptografado[i].ToString("x2"));
            }

            // retorna o valor criptografado como string
            return strBuilder.ToString();
        }

        public static void EnviarEmail(string email, string assunto, string mensagem, bool idfHtml, Dictionary<Stream, ContentType> anexos = null)
        {
            try
            {

                //Configurações de envio de email
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(ConfigurationManager.AppSettings["remetente"], email);
                SmtpClient client = new SmtpClient();

                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["portSMTP"]);
                client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["enableSSL"]);

                if (ConfigurationManager.AppSettings["pass"] != "")
                {
                    NetworkCredential basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["pass"]);
                    client.UseDefaultCredentials = false;
                    client.Credentials = basicCredential;
                }
                else //Relay
                {
                    client.UseDefaultCredentials = true;
                }

                client.Host = ConfigurationManager.AppSettings["hostSMTP"];

                //Envio de email
                mail.Subject = assunto;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;



                if (idfHtml)
                {
                    AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + mensagem + "</body></html>", null, MediaTypeNames.Text.Html);
                    mail.AlternateViews.Add(av1);
                    mail.IsBodyHtml = true;
                }
                else
                {
                    mail.Body = mensagem;
                }

                //Verfica se existe arquivos para serem anexados
                if (anexos != null)
                {
                    foreach (var anexo in anexos)
                    {
                        Attachment anexado = new Attachment(anexo.Key, anexo.Value);
                        mail.Attachments.Add(anexado);
                    }
                    
                }

                client.Send(mail);

            }
            catch (SmtpException smtpex)
            {
                Log.WriteError(smtpex.Message, smtpex);
                throw new GS1TradeException("Erro ao enviar email. Falha de comunicação com o servidor.");

            }
            catch (Exception ex)
            {
                Log.WriteError(ex.Message, ex);
                throw new GS1TradeException("Falha ao enviar email. Consulte o Administrador.");
            }

        }

        //Apenas gerar a carta, não realiza o download
        public static byte[] GerarCarta(dynamic parametro)
        {
            object user = parametro.campos.user;
            byte[] bytes = null;
            Application wordApp = new Application();
            Document document = new Document();
            object doNotSaveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;

            if (parametro != null)
            {
                //if (File.Exists(HttpContext.Current.Server.MapPath(Convert.ToString(parametro.campos.caminhotemplate.Value))))
                //{

                    try
                    {
                        //document = wordApp.Documents.Open(HttpContext.Current.Server.MapPath(Convert.ToString(parametro.campos.caminhotemplate.Value)));
                        document = wordApp.Documents.Open(parametro.campos.caminhotemplate.Value);

                        if (user != null)
                        {
                            if (parametro.campos != null)
                            {

                                FindAndReplace(document, "{Data Extenso}", Convert.ToString(parametro.campos.currentDate.Value));
                                FindAndReplace(document, "{Razao Social}", Convert.ToString(parametro.campos.razaosocial.Value));
                                FindAndReplace(document, "{Contato}", Convert.ToString(parametro.campos.nomecontato.Value));
                                FindAndReplace(document, "{Data}", Convert.ToString(parametro.campos.currentDate.Value));
                                //FindAndReplace(document, "{Prefixo}", Convert.ToString(parametro.campos.prefixo.Value));

                                //if (parametro.campos.prefixos != null)
                                //{
                                //    FindAndReplace(document, "{Prefixos}", Convert.ToString(parametro.campos.prefixos.Value));
                                //}

                                if (parametro.campos.chave != null)
                                {
                                    FindAndReplace(document, "{Chave}", Convert.ToString(parametro.campos.chave.Value));
                                }

                                if (parametro.campos.validade != null)
                                {
                                    FindAndReplace(document, "{Validade}", Convert.ToString(parametro.campos.validade.Value));
                                }
                            }

                            //string extension = Path.GetExtension(HttpContext.Current.Server.MapPath(Convert.ToString(parametro.campos.caminhotemplate.Value)));
                            string extension = Path.GetExtension(parametro.campos.caminhotemplate.Value);
                            string virtualName = string.Format("/upload/cartas/temp_{0}{1}", System.Guid.NewGuid().ToString(), extension);
                            string fullName = HttpContext.Current.Server.MapPath(virtualName);
                            string Name = document.Name;
                            document.SaveAs2(fullName);
                            document.Close(ref doNotSaveChanges);

                            string prefixos = parametro.campos.prefixos != null ? parametro.campos.prefixos.ToString() : string.Empty,
                                descricao = parametro.campos.descricao != null ? parametro.campos.descricao.ToString() : string.Empty,
                                nomelicenca = parametro.campos.nomelicenca != null ? parametro.campos.nomelicenca.ToString() : string.Empty;

                            DocxHelper doc = new DocxHelper();
                            using (DocX file = doc.CreateGTINTables(virtualName, prefixos, descricao, nomelicenca))
                            {
                                file.Save();
                                file.Dispose();
                            }

                            document = wordApp.Documents.Open(HttpContext.Current.Server.MapPath(virtualName));

                            Object oMissing = System.Reflection.Missing.Value;
                            document.ExportAsFixedFormat(fullName.Replace(".docx", ".pdf"), WdExportFormat.wdExportFormatPDF, false, WdExportOptimizeFor.wdExportOptimizeForOnScreen,
                            WdExportRange.wdExportAllDocument, 1, 1, WdExportItem.wdExportDocumentContent, true, true,
                            WdExportCreateBookmarks.wdExportCreateHeadingBookmarks, true, true, false, ref oMissing);

                            document.Close(ref doNotSaveChanges);
                            wordApp.Quit();

                            bytes = System.IO.File.ReadAllBytes(fullName.Replace(".docx", ".pdf"));

                            if (File.Exists(fullName))
                                File.Delete(fullName);
                            if (File.Exists(fullName.Replace(".docx", ".pdf")))
                                File.Delete(fullName.Replace(".docx", ".pdf"));

                            return bytes;
                        }
                    }
                    catch (Exception e)
                    {
                        wordApp.Quit();
                        throw new GS1TradeException("Não foi possível gerar a Carta Certificada.", e);
                    }
                //}
            }
            return null;

        }

        //gera e faz o download da carta
        public static void GerarBaixarCarta(dynamic parametro)
        {
            if (parametro != null)
            {

                byte[] bytes = GerarCarta(parametro);

                if (bytes != null)
                {
                    HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.AddHeader("Content-Type", "application/pdf");
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + string.Format("CartaFiliacao_{0}_{1}{2}", parametro.campos.nomelicenca.Value.ToString(), DateTime.Now.ToString("ddMMyyyy_HHmmss"), ".pdf"));
                    HttpContext.Current.Response.AddHeader("Content-Length", bytes.Length.ToString());
                    HttpContext.Current.Response.AddHeader("Cache-Control", "max-age=0");
                    HttpContext.Current.Response.AddHeader("Accept-Ranges", "none");
                    HttpContext.Current.Response.BinaryWrite(bytes);

                    HttpContext.Current.Response.Flush();

                    //Insere Data de Geração na tabela TemplateCartaUsuario
                    try
                    {
                        InsereDataGeracao(parametro);
                    }
                    catch (Exception e)
                    {
                        //throw new GS1TradeException("Não foi possível gerar a Carta Certificada.", e);
                    }
                }
                else
                {
                    throw new GS1TradeException("Não foi possível gerar a Carta Certificada.");
                }
            }
        }

        public List<string> ValidateWordDocument(string caminho)
        {

            if (caminho != null)
            {

                //if (File.Exists(caminho))
                //{
                    Object oMissing = System.Reflection.Missing.Value;
                    Application wordApp = new Application();
                    Document document = wordApp.Documents.Open(caminho, ref oMissing, true, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing);
                    bool isTrue = false;
                    object doNotSaveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;

                    List<string> words = new List<string>();
                    List<string> problemas = new List<string>();
                    List<string> problemasSecundario = new List<string>();
                    words.Add("{Data Extenso}");
                    words.Add("{Data}");
                    words.Add("{Razao Social}");
                    words.Add("{Contato}");
                    words.Add("{Prefixo}");
                    words.Add("{Prefixos}");

                    for (int i = 0; i < words.Count(); i++)
                    {
                        Microsoft.Office.Interop.Word.Range range = document.Content;
                        Microsoft.Office.Interop.Word.Find find = range.Find;
                        isTrue = find.Execute(words[i], null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                        if (!isTrue)
                        {
                            problemasSecundario.Add(words[i]);
                        }
                    }

                    bool hasErrorPrefixo = false, hasErrorPrefixos = false, hasErrorData = false, hasErrorDataExtenso = false;

                    for (var i = 0; i < problemasSecundario.Count; i++)
                    {
                        if (problemasSecundario[i] == "{Prefixo}")
                        {
                            hasErrorPrefixo = true;
                        }

                        if (problemasSecundario[i] == "{Prefixos}")
                        {
                            hasErrorPrefixos = true;
                        }

                        if (problemasSecundario[i] == "{Data Extenso}")
                        {
                            hasErrorDataExtenso = true;
                        }

                        if (problemasSecundario[i] == "{Data}")
                        {
                            hasErrorData = true;
                        }

                    }

                    for (var i = 0; i < problemasSecundario.Count; i++)
                    {
                        if ((hasErrorPrefixos && hasErrorPrefixo && (problemasSecundario[i] == "{Prefixos}" || problemasSecundario[i] == "{Prefixo}")) ||
                            (hasErrorData && hasErrorDataExtenso && (problemasSecundario[i] == "{Data Extenso}" || problemasSecundario[i] == "{Data}")))
                        {
                            problemas.Add(problemasSecundario[i]);
                        }
                    }

                    document.Close(ref doNotSaveChanges);
                    wordApp.Quit();

                    //return isTrue;
                    return problemas;
                //}
                //else
                //{
                //    //return false;
                //    throw new GS1TradeException("Os campos do template estão fora do padrão. Por favor selecione um template válido.");
                //}
            }
            else
            {
                //return false;
                throw new GS1TradeException("Os campos do template estão fora do padrão. Por favor selecione um template válido.");
            }
        }

        public static object InsereDataGeracao(dynamic parametro)
        {
            dynamic param = null;

            dynamic user = HttpContext.Current.Session["USUARIO_LOGADO"];
            dynamic associado = HttpContext.Current.Session["ASSOCIADO_SELECIONADO"];

            if (user != null && associado != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();

                    try
                    {

                        parametro.campos.codigoassociado = associado.codigo.ToString();
                        parametro.campos.codigousuario = user.id.ToString();

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object retorno = null;

                        //foreach (dynamic pref in prefixo)
                        //{
                        string sql_template = @"INSERT INTO TEMPLATECARTAUSUARIO VALUES (@codigotemplatecarta, @caminhotemplate, @codigousuario, getDate(), @codigoassociado, @prefixos)";
                        retorno = db.Select(sql_template, param);
                        //}

                        db.Commit();
                        return JsonConvert.SerializeObject(retorno);
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        return "Não foi possível buscar os dados do associado.";
                    }
                }
            }
            else
            {
                return "Não foi possível buscar os dados do associado.";
            }
        }

        public static string TratarNomeArquivo(string texto)
        {
            if (string.IsNullOrEmpty(texto))
                return string.Empty;
            else
            {
                byte[] bytes = System.Text.Encoding.GetEncoding("iso-8859-8").GetBytes(texto);
                string nomeArquivo = System.Text.Encoding.UTF8.GetString(bytes);

                nomeArquivo = nomeArquivo.Replace(" de ", string.Empty)
                    .Replace(" da ", string.Empty)
                    .Replace(" do ", string.Empty)
                    .Replace(" por ", string.Empty)
                    .Replace(" e ", string.Empty)
                    .Replace(" em ", string.Empty)
                    .Replace(" com ", string.Empty)
                    .Replace("/", string.Empty)
                    .Replace(" ", string.Empty);

                return nomeArquivo;
            }
        }

        public static string ChangeDateFormat(string dateValue)
        {
            if (!string.IsNullOrEmpty(dateValue))
            {
                if (IsDate(dateValue))
                {
                    DateTime date = DateTime.ParseExact(dateValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    return date.ToString("yyyy-MM-dd");
                }
            }

            return string.Empty;
        }

        public static List<byte> uploadBlobDatabase(HttpContext context, HttpFileCollection SelectedFiles)
        {
            byte[] fileData = null;
            List<byte> listBlob = new List<byte>();

            using (var binaryReader = new BinaryReader(context.Request.Files[0].InputStream))
            {
                fileData = binaryReader.ReadBytes(context.Request.Files[0].ContentLength);
            }

            foreach (var x in fileData)
            {
                listBlob.Add(fileData[x]);
            }

            //TODO: chamar método de update do BO informado
            Type BOClassType = null;
            object BOObject = null, retorno = null;
            string BO_NAMESPACE = "GS1.CNP.BLL";

            Assembly assembly = Assembly.Load(BO_NAMESPACE);

            BOClassType = assembly.GetType(String.Format("{0}.{1}", BO_NAMESPACE, context.Request.QueryString["BO"]));
            BOObject = Activator.CreateInstance(BOClassType);

            Object[] parameter = new Object[] { SelectedFiles };
            retorno = BOClassType.InvokeMember("Update", BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public, null, BOObject, parameter);

            return listBlob;
        }

        public static string uploadBlobAzure(HttpPostedFile SelectedFile)
        {
            string list;

            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"]);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference("imagesazurecontainer");
                container.CreateIfNotExists();
                container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
 
                string novoFilename = System.Guid.NewGuid().ToString() + "." + SelectedFile.FileName.Split('.').Last();
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(novoFilename);

                using (var fileStream = SelectedFile.InputStream)
                {
                    blockBlob.UploadFromStream(fileStream);
                    list = System.Configuration.ConfigurationManager.AppSettings["URLBlobAzure"] + "/" + container.Name + "/" + novoFilename;
                }
            }
            catch (Exception)
            {
                throw new GS1TradeException("Não foi possível fazer o upload da imagem no Windows Azure.");
            }

            return list;
        }

        public static void deleteBlobAzure(string nomeimagem)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"]);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference("imagesazurecontainer");
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(nomeimagem.Split('/').Last());
                blockBlob.Delete();
            }
            catch (Exception)
            {
                throw new GS1TradeException("Não foi possível remover a imagem no Windows Azure.");
            }
        }

        public static void DeletarImagem(string codigo, string nomeimagem, string table)
        {

            if (nomeimagem.Contains(System.Configuration.ConfigurationManager.AppSettings["URLBlobAzure"]))
            {
                GS1.CNP.BLL.Core.Util.deleteBlobAzure(nomeimagem);
            }
            else if (nomeimagem.Contains("/upload"))
            {
                //TODO: Remover imagem física
            }

            string sql = String.Format("UPDATE {0} SET NOMEIMAGEM = NULL WHERE CODIGO = {1}", table, codigo);

            using (Database db = new Database("BD"))
            {
                db.Open();
                db.Select(sql, null);
            }

        }

        public static void FindAndReplace(Document document, string findText, string replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;

            Microsoft.Office.Interop.Word.Range range = document.Content;
            Microsoft.Office.Interop.Word.Find find = range.Find;

            find.Execute(findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }
    }

    public static class JObjectExtensions
    {
        public static object ToExpando(this JToken token)
        {
            if (token is JValue)
            {
                return ((JValue)token).Value;
            }
            if (token is JObject)
            {
                ExpandoObject expando = new ExpandoObject();

                (from childToken in ((JToken)token) where childToken is JProperty select childToken as JProperty).ToList().ForEach(property =>
                {
                    ((IDictionary<string, object>)expando).Add(property.Name, ToExpando(property.Value));
                });

                return expando;
            }
            if (token is JArray)
            {
                object[] array = new object[((JArray)token).Count];
                int index = 0;

                foreach (JToken arrayItem in ((JArray)token))
                {
                    array[index] = ToExpando(arrayItem);
                    index++;
                }

                return array;
            }

            throw new ArgumentException(string.Format("Unknown token type '{0}'", token.GetType()), "token");
        }
    }

    public static class LinqToObjectExtensions
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
    }

    public static class EnumerableDebugger
    {
        public static ConsoleColor DefaultColor = ConsoleColor.Yellow;
        public static bool ShowWhiteSpace { get; set; }

        public static IEnumerable<T> Dump<T>(this IEnumerable<T> input, string title)
        {
            return Dump(input, title,
               item => item != null ? JsonConvert.SerializeObject(item) : "(null)", DefaultColor);
        }

        public static IEnumerable<T> Dump<T>(
            this IEnumerable<T> input,
            string title,
            ConsoleColor consoleColor)
        {
            return Dump(input, title,
                item => item != null ? JsonConvert.SerializeObject(item) : "(null)", consoleColor);
        }

        public static IEnumerable<T> Dump<T>(
            this IEnumerable<T> input,
            string title,
            Func<T, string> toString,
            ConsoleColor consoleColor)
        {
            foreach (var item in input)
            {
                Console.ForegroundColor = consoleColor;
                Debug.WriteLine(ShowWhiteSpace ? '[' + title + " : " + toString(item) + ']' : title + " : " + toString(item));
                Console.ResetColor();
                yield return item;
            }
        }
    }

    public class DynamicXml : DynamicObject
    {
        XElement _root;
        private DynamicXml(XElement root)
        {
            _root = root;
        }

        public static DynamicXml Parse(string xmlString)
        {
            return new DynamicXml(XDocument.Parse(xmlString).Root);
        }

        public static DynamicXml Load(string filename)
        {
            return new DynamicXml(XDocument.Load(filename).Root);
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;

            var att = _root.Attribute(binder.Name);
            if (att != null)
            {
                result = att.Value;
                return true;
            }

            var nodes = _root.Elements(binder.Name);
            if (nodes.Count() > 1)
            {
                result = nodes.Select(n => new DynamicXml(n)).ToList();
                return true;
            }

            var node = _root.Element(binder.Name);
            if (node != null)
            {
                if (node.HasElements)
                {
                    result = new DynamicXml(node);
                }
                else
                {
                    result = node.Value;
                }
                return true;
            }

            return true;
        }
    }
}
