﻿namespace GS1.CNP.BLL.Enum
{
    public enum EnumOperation
    {
        INSERIR = 1,
        ALTERAR = 2,
        PESQUISAR = 3,
        EXCLUIR = 4
    }

    public enum EnumUpdateImages
    {
        BLOB,
        BLOBAZURE,
        FISICO
    }
}
