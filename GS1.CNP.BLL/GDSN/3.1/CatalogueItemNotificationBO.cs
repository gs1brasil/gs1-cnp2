﻿using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;

namespace GS1.CNP.BLL
{

    [GS1CNPAttribute("CatalogueItemNotificationBO")]
    public class CatalogueItemNotificationBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CatalogueItemNotification"; }
        }

        public override string nomePK
        {
            get { return "CODIGOPRODUTO"; }
        }

        [GS1CNPAttribute(false)]
        public object GetCatalogueItemNotification(dynamic parametro)
        {

            string sql = @"SELECT * FROM CatalogueItemNotification WITH (NOLOCK) WHERE CodigoProduto = @codigoproduto";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;
                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> consulta = db.Select(sql, param);

                return JsonConvert.SerializeObject(consulta);
            }
        }

        [GS1CNPAttribute(false)]
        public object InsertCatalogueItemNotification(long codigoproduto, string catalogueitemnotificationtype)
        {

            dynamic parametro = new ExpandoObject();
            parametro.codigoproduto = codigoproduto;
            parametro.catalogueitemnotificationtype = catalogueitemnotificationtype;

            string sql = @"INSERT INTO CatalogueItemNotification (CodigoProduto, CatalogueItemNotificationType,DataAtualizacao) 
                            VALUES (@codigoproduto,@catalogueitemnotificationtype,GETDATE())";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> consulta = db.Select(sql, parametro);
                return JsonConvert.SerializeObject(consulta);
            }
        }

        [GS1CNPAttribute(false)]
        public object UpdateCatalogueItemNotification(long codigoproduto, string catalogueitemnotificationtype)
        {
            dynamic parametro = new ExpandoObject();
            parametro.codigoproduto = codigoproduto;
            parametro.catalogueitemnotificationtype = catalogueitemnotificationtype;

            string sql = @"UPDATE CatalogueItemNotification SET CatalogueItemNotificationType = @catalogueitemnotificationtype, DataAtualizacao = GETDATE() WHERE CodigoProduto = @codigoproduto";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> consulta = db.Select(sql, parametro);
                return JsonConvert.SerializeObject(consulta);
            }
        }

    }
}