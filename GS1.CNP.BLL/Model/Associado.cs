﻿using System;

namespace GS1.CNP.BLL.Model
{
    [Serializable]
    public class Associado
    {
        public long codigo { get; set; }
        public string nome { get; set; }
        public string cpfcnpj { get; set; }
        public string cad { get; set; }
        public string indicadorcnaerestritivo { get; set; }
        public bool ativado { get; set; }
        public bool aprovacao { get; set; }
        public long codigoperfilgepir { get; set; }
    }
}
