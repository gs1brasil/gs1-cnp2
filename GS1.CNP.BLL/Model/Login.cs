﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GS1.CNP.BLL.Model
{
    [Serializable]
    public class Login
    {
        public long id { get; set; }
        public int id_perfil { get; set; }
        public int id_tipousuario { get; set; }
        public string nome { get; set; }
        public int quantidade_tentativa { get; set; }
        [JsonIgnore]
        public string senha { get; set; }
        public long id_usuariofornecedor { get; set; }
        public int id_statususuario { get; set; }
        public int id_tipoassociado { get; set; }
        public int avisosenha { get; set; }
        public int diasvencimentosenha { get; set; }
        public int id_statususuariopai { get; set; }
        public string email { get; set; }
        public string CPFCNPJ { get; set; }
        public string qtdediasbloqsenha { get; set; }
        public int qtdetentativasbloqueio { get; set; }
        public int toleranciainadimplente { get; set; }
        public int id_tipopatrocinador { get; set; }
        public int termoadesaopendente { get; set; }
        public int qtdetentativasrecaptcha { get; set; }
        public int qtdeassociadosativos { get; set; }
        public int indicadorprincipal { get; set; }
        public List<string> permissoes { get; set; }
    }
}
