﻿namespace GS1.CNP.BLL.Model
{
    public class LoginResult
    {
        public bool status_login { get; set; }
        public int quantidade_tentativa { get; set; }
        public string mensagem { get; set; }
        public bool primeiro_acesso { get; set; }
    }
}
