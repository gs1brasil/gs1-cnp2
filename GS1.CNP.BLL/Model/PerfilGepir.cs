﻿using System;

namespace GS1.CNP.BLL.Model
{
    [Serializable]
    public class PerfilGepir
    {
        public long id { get; set; }
        public string name { get; set; }
        public int daily_queries { get; set; }
        public int weekly_queries { get; set; }
        public int monthly_queries { get; set; }
        public bool automatic_authorization { get; set; }
        public bool shared_queries { get; set; }
        public bool unlimited_queries { get; set; }

    }
}
