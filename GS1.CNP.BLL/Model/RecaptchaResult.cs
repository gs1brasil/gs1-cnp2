﻿namespace GS1.CNP.BLL.Model
{
    public class RecaptchaResult
    {
        public bool status_login { get; set; }
        public int quantidade_tentativa { get; set; }
    }
}
