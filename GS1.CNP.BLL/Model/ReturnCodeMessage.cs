﻿using System;

namespace GS1.CNP.BLL.Model
{
    [Serializable]
    public class ReturnCodeMessage
    {
        public int   value { get; set; }
        public string text { get; set; }
        public string textpt { get; set; }
    }
}
