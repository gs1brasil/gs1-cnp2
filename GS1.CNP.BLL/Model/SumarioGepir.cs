﻿using System;

namespace GS1.CNP.BLL.Model
{
    [Serializable]
    public class SumarioGepir
    {
        public int totaldiario { get; set; }
        public int totalsemanal { get; set; }
        public int totalmensal { get; set; }
        public DateTime dataSumario { get; set; }
    }
}
