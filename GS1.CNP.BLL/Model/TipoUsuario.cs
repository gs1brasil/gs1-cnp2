﻿namespace GS1.CNP.BLL.Model
{
    public class TipoUsuario
    {
        public int codigo { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public int status { get; set; }
    }

}
