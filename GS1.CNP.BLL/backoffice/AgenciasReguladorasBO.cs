using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("AgenciasReguladorasBO")]
    class AgenciasReguladorasBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "AGENCIAREGULADORA"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute("PesquisarAgencia")]
        public string BuscarAgenciasReguladoras(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            string sql = "SELECT CODIGO, NOME, DESCRICAO, STATUS FROM AGENCIAREGULADORA WITH (NOLOCK)";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("PesquisarAgencia")]
        public string BuscarAgenciaReguladora(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            string sql = "SELECT CODIGO, NOME, DESCRICAO, STATUS FROM AGENCIAREGULADORA WITH (NOLOCK)";

            if (parametro.campos != null)
            {
                sql += " WHERE 1=1";
                
                if (parametro.campos.nome != null)
                {
                    sql += "AND NOME LIKE '%' + @nome + '%'";
                }

                if (parametro.campos.descricao != null)
                {
                    sql += "AND DESCRICAO LIKE '%' + @descricao + '%'";
                }

                if (parametro.campos.status != null)
                {
                    sql += " AND STATUS = @status";
                }
            }

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> agencia = db.Select(sql, param);

                return JsonConvert.SerializeObject(agencia);
            }
        }

        [GS1CNPAttribute("CadastrarAgencia",true,true)]
        public object CadastrarAgencia(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    //Verifica se o nome da Ag�ncia j� foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM AGENCIAREGULADORA A WHERE A.NOME = @nome";

                    db.Open();
                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {

                        string sql = "INSERT INTO AGENCIAREGULADORA VALUES (@nome, @descricao, @status, GETDATE(), @CodigoUsuarioAlteracao)";

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        long result = db.Insert(sql, param);

                        if (result > 0)
                        {
                            Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, result);
                        }
                    }
                    else
                    {
                        throw new GS1TradeException("Ag�ncia Reguladora j� cadastrada.");
                    }
                }
                return "";
            }
            else
            {
                return "N�o foi poss�vel realizar o cadastro.";
            }
        }

        [GS1CNPAttribute(true, true, "EditarAgencia", "VisualizarAgencia")]
        public object AtualizarAgencia(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    
                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    db.Open(true);
                    db.BeginTransaction();

                    //Inser��o na tabela AGENCIAREGULADORAHISTORICO
                    string sql_historico = @"INSERT INTO AGENCIAREGULADORAHISTORICO (CODIGO, NOME, DESCRICAO, STATUS, DATAALTERACAO, CODIGOUSUARIOALTERACAO, DATAHISTORICO)
	                                        SELECT CODIGO, NOME, DESCRICAO, STATUS, DATAALTERACAO, CODIGOUSUARIOALTERACAO, GETDATE()
				                                            FROM AGENCIAREGULADORA WITH (NOLOCK)
				                                            WHERE AGENCIAREGULADORA.CODIGO = @codigo";

                    dynamic param1 = null;

                    if (parametro.campos != null)
                        param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql_historico, param1);

                    //Verifica se Ag�ncia Reguladora j� foi cadastrada
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM AGENCIAREGULADORA A WHERE A.NOME = @nome AND A.CODIGO <> @codigo";

                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {
                        //Atualiza Ag�ncias Reguladoras
                        string sql = "UPDATE AGENCIAREGULADORA SET NOME = @nome, DESCRICAO = @descricao, STATUS = @status, CODIGOUSUARIOALTERACAO = @CodigoUsuarioAlteracao, DATAALTERACAO = GETDATE() WHERE CODIGO = @codigo";

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);
                        db.Commit();

                        return result;
                    }
                    else
                    {
                        db.Rollback();
                        throw new GS1TradeException("Ag�ncia Reguladora j� cadastrada.");
                    }
                }
            }
            else
            {
                return "N�o foi poss�vel realizar a atualiza��o.";
            }
        }

        [GS1CNPAttribute("ExcluirAgencia", true, true)]
        public object RemoverAgencia(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                try
                {
                    string sql = "DELETE FROM AGENCIAREGULADORA WHERE CODIGO = @codigo";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Exclus�o n�o � permitida. A Ag�ncia Reguladora possui dados vinculados.", e);
                }
            }
            else
            {
                return "N�o foi poss�vel remover o item selecionado.";
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarAgenciasReguladorasAtivas(dynamic parametro)
        {
            string sql = @"SELECT CODIGO, NOME, STATUS 
                           FROM AGENCIAREGULADORA WITH (NOLOCK) 
                           WHERE STATUS = 1 
                           ORDER BY NOME ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }
}
