﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("AjudaPassoAPassoBO")]
    public class AjudaPassoAPassoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "FAQ"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarAjudasPassoAPasso(dynamic parametro)
        {

            string sql = @"SELECT FA.CODIGO, FA.NOME, FA.URLVIDEO AS VIDEO, FA.AJUDA
	                            ,TOTALREGISTROS = COUNT(*) OVER()
                                FROM FORMULARIOAJUDAPASSOAPASSO FA WITH (NOLOCK)
                                WHERE FA.CODIGOSTATUSPUBLICACAO = 1
	                            AND FA.AJUDA IS NOT NULL
                                AND FA.CODIGOIDIOMA = @codigoidioma 
	                            AND FA.CODIGOFORMULARIO = @codigoformulario
                                ORDER BY ORDEM ASC, DATAALTERACAO DESC ";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro != null && parametro.campos != null)
                {
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                }

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarFormularios(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                if (parametro != null && parametro.campos != null)
                {
                    if (parametro.campos.registroporpagina == null)
                        parametro.campos.registroporpagina = 10;
                }

                string sql = @"SELECT F1.CODIGOFORMULARIO, F1.NOME, F1.DESCRICAO, QUANTIDADETOTAL = COUNT(*) OVER()
		                            FROM PERFILFUNCIONALIDADE PF WITH (NOLOCK) 
		                            INNER JOIN FUNCIONALIDADE F WITH (NOLOCK) ON F.CODIGO = PF.CODIGOFUNCIONALIDADE
		                            INNER JOIN FORMULARIOIDIOMA F1 WITH (NOLOCK) ON F1.CODIGOFORMULARIO = F.CODIGOFORMULARIO
		                            INNER JOIN FORMULARIOAJUDAPASSOAPASSO FA WITH (NOLOCK) ON FA.CODIGOFORMULARIO = F1.CODIGOFORMULARIO
		                            INNER JOIN PERFIL P WITH (NOLOCK) ON P.CODIGO = PF.CODIGOPERFIL
		                            AND FA.AJUDA IS NOT NULL
		                            AND F.STATUS = 1 
                                    AND FA.CODIGOSTATUSPUBLICACAO = 1 
                                    AND FA.CODIGOIDIOMA = @codigoidioma 
                                    AND F1.CODIGOIDIOMA = @codigoidioma ";
                                    
                                    if(user.id_tipousuario == 2)
                                        sql += " AND P.CODIGOTIPOUSUARIO = 2 ";

                                sql += @"GROUP BY F1.CODIGOFORMULARIO, F1.NOME, F1.DESCRICAO
                                ORDER BY F1.CODIGOFORMULARIO ASC  
                                OFFSET " + parametro.campos.registroporpagina + " * (@paginaatual - 1) ROWS FETCH NEXT " + parametro.campos.registroporpagina + " ROWS ONLY";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível gerar o registro.");
            }
        }
    }

}
