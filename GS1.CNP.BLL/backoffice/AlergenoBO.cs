﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("AlergenoBO")]
    public class AlergenoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "AllergenTypeCode"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarAtivos(dynamic parametro)
        {
            string sql = @"SELECT CODIGO, SIGLA, DESCRICAO
                            FROM AllergenTypeCode WITH (NOLOCK) 
                            ORDER BY SIGLA ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

//        public AllergenInformationModuleType extract(dynamic alergenos)
//        {
//            AllergenInformationModuleType result = new AllergenInformationModuleType();
//            result.allergenRelatedInformation = new AllergenRelatedInformationType[1];

//            IList<AllergenRelatedInformationType> listaAlergenos = new List<AllergenRelatedInformationType>();
//            if (alergenos != null)
//            {
//                foreach (var item in alergenos)
//                {
//                    AllergenRelatedInformationType info = new AllergenRelatedInformationType();
//                    info.
//                    dynamic paramInsert = new ExpandoObject();
//                    paramInsert.CodigoAllergenTypeCode = item.allergenRelatedInformation.allergen.codigo.ToString();
//                    paramInsert.allergenSpecificationAgency = item.allergenSpecificationAgency != null && item.allergenSpecificationAgency != "" ? item.allergenSpecificationAgency : null;
//                    paramInsert.allergenSpecificationName = item.allergenSpecificationName != null && item.allergenSpecificationName != "" ? item.allergenSpecificationName : null;
//                    paramInsert.allergenStatement = item.allergenStatement != null && item.allergenStatement != "" ? item.allergenStatement : null;

//                    int idInserido = db.Insert("AllergenRelatedInformation", "CODIGO", paramInsert, true);

//                    string insert = @"INSERT INTO ProdutoAllergen (CodigoProduto, CodigoAllergenRelatedInformation, levelOfContainmentCode)
//                                    VALUES (" + codigoproduto + ", " + idInserido + ", '" + item.levelOfContainmentCode.codigo.ToString() + "')";
//                    db.Select(insert, null);
//                }
//            }

//            return result;
//        }
    }

}
