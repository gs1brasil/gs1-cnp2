﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using System.Web;
using System.Dynamic;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("AprovarImportacaoProdutosBO")]
    public class AprovarImportacaoProdutosBO : CrudBO, IDisposable
    {
        public override string nomeTabela
        {
            get { return string.Empty; }
        }

        public override string nomePK
        {
            get { return string.Empty; }
        }

        [GS1CNPAttribute(false, true)]
        public object BuscarAprovacao(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                using (ImportarProdutosBO bo = new ImportarProdutosBO())
                {
                    dynamic dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml"));

                    List<dynamic> dicionario = null;
                    List<dynamic> lista = null;
                    int tipoArquivo = -1,
                        template = 0,
                        padraoGDSN = 0;

                    dynamic retorno = new ExpandoObject(),
                        param = new ExpandoObject();
                    Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

                    bool existeErro = false;

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        param.associado = associado.codigo;

                        var aprovaProduto = db.Find("AprovacaoProduto", "CodigoAssociado = @associado", param);

                        if (aprovaProduto != null)
                        {
                            param.codigoaprovacao = aprovaProduto.codigo;

                            if (aprovaProduto.codigotipoarquivo != null && !string.IsNullOrEmpty(aprovaProduto.codigotipoarquivo.ToString()))
                            {
                                retorno.tipoArquivo = bo.RecuperarTipoArquivoPorID(aprovaProduto.codigotipoarquivo);
                                retorno.tipoitem = bo.RecuperarTipoItem(retorno.tipoArquivo);

                                tipoArquivo = retorno.tipoArquivo;
                            }

                            padraoGDSN = aprovaProduto.gdsn ? 1 : 2;
                            retorno.gdsn = (padraoGDSN == 1 ? "Sim" : "Não");
                            retorno.padraoGDSN = padraoGDSN;

                            if (tipoArquivo != 0 && tipoArquivo != 12 && tipoArquivo != 13 && tipoArquivo != 14)
                                throw new GS1TradeException("Template do arquivo não identificado.");

                            if (tipoArquivo > 0)
                                template = (tipoArquivo * 10) + padraoGDSN;
                            else
                                template = tipoArquivo;

                            dicionario = bo.RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                            if (tipoArquivo > 0)
                                lista = db.Select("SELECT * FROM PRODUTOTEMP WHERE CODIGOAPROVACAO=@codigoaprovacao", param);
                            else
                                lista = db.Select("SELECT * FROM LOCALIZACAOFISICATEMP WHERE CODIGOAPROVACAO=@codigoaprovacao", param);

                            for (var i = 0; i < lista.Count; i++)
                            {
                                lista[i] = TratarItem(db, bo, dicionario, lista[i]);

                                if (!bo.ValidarRegistro(db, dicionario, lista[i], associado, tipoArquivo, template))
                                    existeErro = true;
                            }

                            if (lista != null && lista.Count > 0)
                                retorno.lista = lista.OrderByDescending(x => x != null && (x as IDictionary<string, object>).ContainsKey("errors") && (x.errors as Dictionary<string, string>).Count > 0);
                            else
                                throw new GS1TradeException("Arquivo vazio. Favor inserir registros antes de importar, ou utilize a Importação Manual.");

                            retorno.colunas = (from dynamic d in dicionario
                                               select new
                                               {
                                                   field = d.nome.ToLower(),
                                                   title = d.coluna,
                                                   table = d.tabela.ToLower(),
                                                   column = d.campo.ToLower(),
                                                   type = d.tipo.ToLower(),
                                                   required = (d.obrigatorio as string).Split(',').Contains(template.ToString()),//((retorno.tipoArquivo == 14 && (d.nome.ToLower().Equals("gtinorigem") || d.nome.ToLower().Equals("quantidadeitensporcaixa"))) ? true : d.obrigatorio == "s"),
                                                   listname = d.nomeitemlista.ToLower(),
                                                   tablereference = d.tabelareferencia.ToLower(),
                                                   fieldreference = d.camporeferencia.ToLower(),
                                                   descriptionreference = d.campodescricao.ToLower(),
                                                   condition = d.condicao.ToLower(),
                                                   show = true
                                               });
                            retorno.existeErro = existeErro;
                        }
                    }

                    return retorno;
                }
            }
            else
                throw new GS1TradeSessionException();
        }

        [GS1CNPAttribute("aprovaImportacaoProdutos")]
        public object ImportarAprovacao(dynamic parametro)
        {
            using (ImportarProdutosBO bo = new ImportarProdutosBO())
            {
                object result = bo.Importar(parametro);

                if (result != null)
                {
                    Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        dynamic param = new ExpandoObject();
                        param.codigoassociado = associado.codigo;
                        db.Delete("AprovacaoProduto", "codigoassociado", param);
                    }
                }

                return result;
            }
        }

        private dynamic TratarItem(Database db, ImportarProdutosBO bo, List<dynamic> dicionario, dynamic item)
        {
            IDictionary<string, object> dicItem = item as IDictionary<string, object>;
            dynamic result = new ExpandoObject();

            if (dicItem.Count > 0)
            {
                foreach (KeyValuePair<string, object> dItem in dicItem)
                {
                    string columName = dItem.Key.Trim().ToLower();

                    dynamic col = (from dynamic d in dicionario
                                   where d.nome.ToLower().Equals(columName)
                                   select d).FirstOrDefault();

                    if (col != null)
                    {
                        if (bo.isDropdown(col.nome.ToLower()) && !col.nome.ToLower().Equals("statusgln"))
                        {
                            if (dItem.Value.ToString().Equals("0"))
                                ((IDictionary<string, object>)result).Add(col.nome, "Não");
                            else if (dItem.Value.ToString().Equals("1"))
                                ((IDictionary<string, object>)result).Add(col.nome, "Sim");
                            else
                                ((IDictionary<string, object>)result).Add(col.nome, dItem.Value.ToString().ToLower().Equals("sim") ? "Sim" : "Não");
                        }
                        else if (bo.isDropdown(col.nome.ToLower()) && col.nome.ToLower().Equals("statusgln"))
                        {
                            if (dItem.Value.ToString().Equals("0"))
                                ((IDictionary<string, object>)result).Add(col.nome, "Inativo");
                            else if (dItem.Value.ToString().Equals("1"))
                                ((IDictionary<string, object>)result).Add(col.nome, "Ativo");
                            else
                                ((IDictionary<string, object>)result).Add(col.nome, dItem.Value.ToString().ToLower().Equals("ativo") ? "Ativo" : "Inativo");
                        }
                        else if (!string.IsNullOrEmpty(col.tabelareferencia) && !string.IsNullOrEmpty(dItem.Value.ToString()))
                        {
                            dynamic registro = bo.RecuperarRegistroPorDescricao(db, col.tabelareferencia, col.camporeferencia, col.campodescricao, dItem.Value.ToString());
                            ((IDictionary<string, object>)result).Add(col.nome, (registro != null ? registro.descricao : dItem.Value.ToString()));
                        }
                        else if (col.tipo.ToLower().Equals("int") && !string.IsNullOrEmpty(dItem.Value.ToString()) && GS1.CNP.BLL.Core.Util.IsInteger(dItem.Value.ToString()))
                            ((IDictionary<string, object>)result).Add(col.nome, Convert.ToInt32(dItem.Value.ToString()));
                        else if (col.tipo.ToLower().Equals("datetime") && !string.IsNullOrEmpty(dItem.Value.ToString()) && !GS1.CNP.BLL.Core.Util.IsDate(dItem.Value.ToString()))
                        {
                            long date = long.Parse(dItem.Value.ToString());
                            DateTime dt = DateTime.FromOADate(date);
                            ((IDictionary<string, object>)result).Add(col.nome, dt.ToString("dd/MM/yyyy"));
                        }
                        else
                            ((IDictionary<string, object>)result).Add(col.nome, dItem.Value.ToString());
                    }
                }
            }

            return result;
        }

        public void Dispose() { }
    }
}
