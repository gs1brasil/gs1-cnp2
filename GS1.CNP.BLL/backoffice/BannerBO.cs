﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("BannerBO")]
    class BannerBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "Banner"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        public class Banner
        {
            public int id_banner { get; set; }
        }

        [GS1CNPAttribute(true, true, "EditarBanner", "VisualizarBanner")]
        public override void Update(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            if (parametro != null)
            {
                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, long.Parse(parametro.where.ToString()));
            }

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            try
            {
                if (user != null)
                {
                    parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    parametro.campos.DataInicioPublicacao = parametro.campos.DataInicioPublicacao.ToString("MM/dd/yyyy HH:mm:ss.fff");
                    parametro.campos.DataFimPublicacao = parametro.campos.DataFimPublicacao.ToString("MM/dd/yyyy HH:mm:ss.fff");
                    base.Update((object)parametro);
                }
            }
            catch (Exception e)
            {
                throw new GS1TradeException("Não foi possível salvar as alterações.", e);
            }
        }

        [GS1CNPAttribute("PesquisarBanner")]
        public object CarregaGridBanner(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            string sql = @"SELECT DISTINCT B.*, BE.ORDEM, BE.CODIGO AS CODIGOBANNERESPACOBANNER 
                FROM BANNER B 
                LEFT JOIN BANNERESPACOBANNER BE WITH (NOLOCK) ON BE.CODIGOBANNER = B.CODIGO
				LEFT JOIN ESPACOBANNER E WITH (NOLOCK) ON E.CODIGO = BE.CODIGOESPACOBANNER
				AND BE.CODIGOBANNER = B.CODIGO
                WHERE be.CodigoEspacoBanner = '" + parametro.where.CodigoEspacoBanner + "' ORDER BY be.ordem";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> banner = db.Select(sql, param);

                return JsonConvert.SerializeObject(banner);
            }
        }

        [GS1CNPAttribute("CadastrarBanner")]
        public override string Insert(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            try
            {
                parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                dynamic objRetorno = JsonConvert.DeserializeObject(base.Insert((object)parametro));

                if (objRetorno != null)
                {
                    string sql = "INSERT INTO BANNERESPACOBANNER (CODIGOESPACOBANNER, CODIGOBANNER, ORDEM) VALUES (" + parametro.where.codigoespacobanner + ", " + objRetorno[0].codigo + ", " + parametro.where.ordem + ")";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        dynamic param = null;

                        var result = db.Insert(sql, param);

                        if (result != null && result > 0)
                        {
                            Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, result);
                        }

                        return result.ToString();
                    }
                }
                else
                {
                    throw new GS1TradeException("Não foi possível salvar as alterações.");
                }
            }
            catch (Exception e)
            {
                throw new GS1TradeException("Não foi possível salvar.", e);
            }
        }

        [GS1CNPAttribute("ExcluirBanner")]
        public override void Delete(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            if (parametro != null)
            {
                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, long.Parse(parametro.where.ToString()));
            }

            string sql = "DELETE FROM BANNERESPACOBANNER WHERE CODIGOBANNER = " + parametro.where;

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> banner = db.Select(sql, param);

                base.Delete((object)parametro);
            }
        }

        [GS1CNPAttribute(false)]
        public object UpdateBannerEspacoBanner(dynamic parametro)
        {
            if (parametro != null)
            {
                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, long.Parse(parametro.where.codigobannerespacobanner.ToString()));
            }

            string sql = @"UPDATE BANNERESPACOBANNER SET CODIGOESPACOBANNER = @codigoespacobanner, ORDEM = @ordem WHERE CODIGO = " + parametro.where.codigobannerespacobanner;

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> banner = db.Select(sql, param);

                return JsonConvert.SerializeObject(banner);
            }
        }
    }
}
