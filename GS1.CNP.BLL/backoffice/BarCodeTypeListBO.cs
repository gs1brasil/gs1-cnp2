﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("BarCodeTypeListBO")]
    public class BarCodeTypeListBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "BarCodeTypeList"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string buscarTipoCodigoBarras(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            if (user != null && parametro.where != null && parametro.where.codigotipogtin != null)
            {

                string sql = @"SELECT B.CODIGO
		                            , B.NAME 
		                            ,C.MAGNIFICATIONFACTOR
		                            ,C.IDEALMODULEWIDTH
		                            ,C.WIDTH
		                            ,C.HEIGHT
		                            ,C.LEFTGUARD
		                            ,C.RIGHTGUARD
                            FROM TIPOGTINBARCODETYPE A  (Nolock) 
                            INNER JOIN BARCODETYPELIST B   (Nolock)  ON B.CODIGO = A.CODIGOBARCODETYPELIST
                            LEFT JOIN BARCODETYPELISTMAGNIFICATION C  (Nolock)  ON C.CODIGOBARCODETYPELIST = B.CODIGO
                            WHERE A.CODIGOTIPOGTIN = " + parametro.where.codigotipogtin.Value + @" AND [MAGNIFICATIONFACTOR] = 1";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();
                        object retorno = db.Select(sql, null);
                        return JsonConvert.SerializeObject(retorno);
                    }                
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }

        }

        [GS1CNPAttribute(false)]
        public string BuscarCodigoBarras(dynamic parametro)
        {

            string sql = @"SELECT B.CODIGO, B.NAME 
                            FROM TIPOGTINBARCODETYPE A  (Nolock) 
                            INNER JOIN BARCODETYPELIST B  (Nolock)  ON B.CODIGO = A.CODIGOBARCODETYPELIST";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
            
        }

    }
}
