﻿using System;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("CrudBO")]
    public class CEPBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string consultarPaises(dynamic parametro)
        {

            string sql = "SELECT DISTINCT COUNTRYNAME AS PAIS FROM [DBO].[TBCOUNTRY]  (Nolock) ";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string consultarUF(dynamic parametro)
        {
            string sql = @"SELECT STATENAME AS UF FROM [DBO].[TBSTATECOUNTRY] A
                            INNER JOIN TBCOUNTRY B ON B.COUNTRYCODE = A.COUNTRYCODE
                            WHERE B.COUNTRYNAME = @pais";

            dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string consultarUFByCode(dynamic parametro)
        {

            string sql = @"SELECT STATENAME AS UF FROM [DBO].[TBSTATECOUNTRY] A  (Nolock)
                            INNER JOIN TBCOUNTRY B (Nolock)  ON B.COUNTRYCODE = A.COUNTRYCODE
                            INNER JOIN COUNTRYOFORIGINISO3166 C  (Nolock) ON C.SIGLA3 = B.COUNTRYCODE
                            WHERE C.CODIGOPAIS = @pais";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if(parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string consultarCidadesUF(dynamic parametro)
        {

            string sql = "SELECT DISTINCT CIDADE FROM [dbo].[MAPDBM_LOGRADOUROS]  (Nolock) WHERE UF = @uf";
            dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

            using (Database db = new Database("BDCEP"))
            {
                db.Open();
                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }


        [GS1CNPAttribute(false)]
        public object consultaEnderecoPorCEP(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            if (user != null)
            {

                try
                {
                    using (Database db = new Database("BDCEP"))
                    {
                        db.Open();

                        dynamic param = new ExpandoObject();

                        dynamic parametrosOut = new ExpandoObject();

                        if (parametro.campos != null)
                        {
                            parametro.campos.In_CEP_LogradouroP = parametro.campos.cep;
                            parametro.campos.Remove("cep");

                            parametrosOut.Out_Tipo_LogradouroP = new ExpandoObject();
                            parametrosOut.Out_Tipo_LogradouroP.type = System.Data.DbType.String;
                            parametrosOut.Out_Tipo_LogradouroP.size = 10;

                            parametrosOut.Out_LogradouroP = new ExpandoObject();
                            parametrosOut.Out_LogradouroP.type = System.Data.DbType.String;
                            parametrosOut.Out_LogradouroP.size = 255;

                            parametrosOut.Out_BairroP = new ExpandoObject();
                            parametrosOut.Out_BairroP.type = System.Data.DbType.String;
                            parametrosOut.Out_BairroP.size = 255;

                            parametrosOut.Out_LocalidadeP = new ExpandoObject();
                            parametrosOut.Out_LocalidadeP.type = System.Data.DbType.String;
                            parametrosOut.Out_LocalidadeP.size = 255;

                            parametrosOut.Out_UFP = new ExpandoObject();
                            parametrosOut.Out_UFP.type = System.Data.DbType.String;
                            parametrosOut.Out_UFP.size = 2;

                            parametrosOut.Out_Logradouro_30P = new ExpandoObject();
                            parametrosOut.Out_Logradouro_30P.type = System.Data.DbType.String;
                            parametrosOut.Out_Logradouro_30P.size = 30;

                            parametrosOut.Out_Bairro_15P = new ExpandoObject();
                            parametrosOut.Out_Bairro_15P.type = System.Data.DbType.String;
                            parametrosOut.Out_Bairro_15P.size = 15;

                            parametrosOut.Out_Localidade_20P = new ExpandoObject();
                            parametrosOut.Out_Localidade_20P.type = System.Data.DbType.String;
                            parametrosOut.Out_Localidade_20P.size = 20;

                            parametrosOut.Out_RetornoP = new ExpandoObject();
                            parametrosOut.Out_RetornoP.type = System.Data.DbType.String;
                            parametrosOut.Out_RetornoP.size = 2;

                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        }
                        db.CommandTimeout = 300;
                        dynamic logradouro = db.ExecuteProcedure("Stp_Dbm_Recuperacao_Logradouro", param, parametrosOut);


                        return JsonConvert.SerializeObject(logradouro);
                    }
                }
                catch (Exception ex)
                {
                    
                    throw new Exception("Erro ao Consultar CEP.");
                }
                
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }

    public class Logradouro
    {
        public string nome { get; set; }
        public string tipologradourop { get; set; }
		public string logradourop { get; set; }
		public string bairrop { get; set; }
		public string localidadep { get; set; }
		public string ufp { get; set; }
		public string logradouro30p { get; set; }
		public string bairro15p { get; set; }
		public string localidade20p { get; set; }
        public string retornop { get; set; }
    }

}
