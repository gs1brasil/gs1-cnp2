﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNP("CESTBO")]
    public class CESTBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CEST"; }
        }

        public override string nomePK
        {
            get { return "CEST"; }
        }

        [GS1CNPAttribute(false)]
        public string PesquisaCEST(dynamic parametro)
        {
            string sql = @"SELECT CODIGOCEST, CEST, DESCRICAO_CEST
                            FROM CEST WITH (NOLOCK)";

            if (parametro.campos != null)
            {
                if (!string.IsNullOrEmpty((string)parametro.campos.cest))
                {
                    sql += " WHERE REPLACE(CEST, '.', '')  = REPLACE(@cest, '.', '') ";
                }

                if (!string.IsNullOrEmpty((string)parametro.campos.descricao_cest))
                {
                    sql += string.IsNullOrEmpty((string)parametro.campos.cest) ? " WHERE" : " AND";

                    sql += " DESCRICAO_CEST LIKE '%' + @descricao_cest + '%' ";
                }
            }

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }

        }
    }

}
