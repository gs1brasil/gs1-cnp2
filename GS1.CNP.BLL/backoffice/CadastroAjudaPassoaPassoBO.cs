﻿using System;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("CadastroAjudaPassoaPassoBO")]
    public class CadastroAjudaPassoaPassoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "FORMULARIOAJUDAPASSOAPASSO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarFormulariosAjuda(dynamic parametro)
        {

            string sql = @"SELECT F.CODIGO AS CODIGOFORMULARIO, F.NOME AS NOMEFORMULARIO, M.CODIGO AS CODIGOMODULO, M.NOME AS NOMEMODULO  
                            FROM FORMULARIO F WITH (NOLOCK)
		                    INNER JOIN MODULO M WITH (NOLOCK) ON M.CODIGO = F.CODIGOMODULO";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string PesquisarFormulariosAjuda(dynamic parametro)
        {

            string sql = @"SELECT FA.CODIGO, F.CODIGO AS CODIGOFORMULARIO, F.NOME AS NOMEFORMULARIO, M.CODIGO AS CODIGOMODULO, M.NOME AS NOMEMODULO, FA.AJUDA, U.NOME AS NOMEUSUARIO, FA.DATAALTERACAO, FA.STATUS 
                            FROM FORMULARIOAJUDAPASSOAPASSO FA WITH (NOLOCK)
                            INNER JOIN USUARIO U WITH (NOLOCK) ON U.CODIGO = FA.CODIGOUSUARIOALTERACAO
		                    INNER JOIN FORMULARIO F WITH (NOLOCK) ON F.CODIGO = FA.CODIGOFORMULARIO
		                    INNER JOIN MODULO M WITH (NOLOCK) ON M.CODIGO = F.CODIGOMODULO
                            WHERE 1 = 1 ";

            if (parametro.campos.codigomodulo != null)
            {
                sql += " AND M.CODIGO = @codigomodulo";
            }

            if (parametro.campos.codigoformulario != null)
            {
                sql += " AND F.CODIGO = @codigoformulario";
            }

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarCampos(dynamic parametro)
        {

            if (parametro != null && parametro.campos != null)
            {
                string sql = @"SELECT TA.CODIGO AS CODIGOTIPOAJUDA, TA.NOME, TA.DESCRICAO, FA.AJUDA, FA.CODIGOFORMULARIO, FA.CODIGO AS CODIGOFORMULARIOAJUDA, 
	                            F.NOME AS NOMEFORMULARIO, M.NOME AS NOMEMODULO, FA.NOME AS TITULO, FA.URLVIDEO AS VIDEO, SA.CODIGO AS CODIGOSTATUS, SA.NOME AS NOMESTATUS,
                                I.CODIGO AS CODIGOIDIOMA, I.NOME AS NOMEIDIOMA, FA.DATAALTERACAO, FA.ORDEM  
	                            FROM TIPOAJUDA TA WITH (NOLOCK)
	                            LEFT JOIN FORMULARIOAJUDAPASSOAPASSO FA WITH (NOLOCK) ON TA.CODIGO = FA.CODIGOTIPOAJUDA
	                            LEFT JOIN FORMULARIO F WITH (NOLOCK) ON F.CODIGO = FA.CODIGOFORMULARIO
	                            LEFT JOIN MODULO M WITH (NOLOCK) ON M.CODIGO = F.CODIGOMODULO
                                INNER JOIN STATUSPUBLICACAO SA WITH (NOLOCK) ON SA.CODIGO = FA.CODIGOSTATUSPUBLICACAO
                                INNER JOIN IDIOMA I WITH (NOLOCK) ON I.CODIGO = FA.CODIGOIDIOMA
							    WHERE TA.STATUS = 1";

                if (parametro.campos.codigomodulo != null)
                {
                    sql += "AND M.CODIGO = @codigomodulo ";
                }

                if(parametro.campos.codigoformulario != null) 
                {
                    sql += "AND FA.CODIGOFORMULARIO = @codigoformulario ";
                }

                if (parametro.campos.codigoidioma != null) 
                {
                    sql += "AND FA.CODIGOIDIOMA = @codigoidioma ";
                }

                if (parametro.campos.dataalteracao != null && parametro.campos.dataalteracao != "")
                {
                    parametro.campos.dataalteracao = Core.Util.ChangeDateFormat(parametro.campos.dataalteracao.ToString());
                    sql += "AND CONVERT(VARCHAR(10),FA.DATAALTERACAO,120) = @dataalteracao ";
                }

                if (parametro.campos.codigostatus != null)
                {
                    sql += "AND FA.CODIGOSTATUSPUBLICACAO = @codigostatus ";
                }

                if (parametro.campos.ordem != null)
                {
                    sql += "AND FA.ORDEM = @ordem ";
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return null;
            }
        }

        [GS1CNPAttribute("CadastrarAjudaPassoaPasso")]
        public string AlteraFormularioAjudaPassoAPasso(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();

                    //Inserção na tabela FORMULARIOAJUDAPASSOAPASSOHISTORICO
                    string sql_historico = @"INSERT INTO FORMULARIOAJUDAPASSOAPASSOHISTORICO (CODIGOFORMULARIOAJUDAPASSOAPASSO, CODIGOFORMULARIO, CODIGOTIPOAJUDA, AJUDA, NOME, CODIGOUSUARIOALTERACAO, DATAALTERACAO,
                                                        DATAHISTORICO, CODIGOSTATUSPUBLICACAO, URLVIDEO, CODIGOIDIOMA, ORDEM)
	                                            SELECT CODIGO, CODIGOFORMULARIO, CODIGOTIPOAJUDA, AJUDA, NOME, CODIGOUSUARIOALTERACAO, DATAALTERACAO, GETDATE(), CODIGOSTATUSPUBLICACAO, URLVIDEO, CODIGOIDIOMA, ORDEM 
				                                                FROM FORMULARIOAJUDAPASSOAPASSO FA WITH (NOLOCK)
				                                            WHERE FA.CODIGOTIPOAJUDA = @codigotipoajuda AND FA.CODIGOFORMULARIO = @codigoformulario";
                    try
                    {
                        dynamic param1 = null;

                        if (parametro.campos != null)
                            param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        db.Select(sql_historico, param1);

                        if (parametro != null && parametro.campos != null)
                            parametro.campos.codigousuarioalteracao = user.id.ToString();

                        string sql = @"UPDATE FORMULARIOAJUDAPASSOAPASSO SET AJUDA = @ajuda, CODIGOUSUARIOALTERACAO = @codigousuarioalteracao, DATAALTERACAO = GETDATE(), 
                                            NOME = @titulo, CODIGOSTATUSPUBLICACAO = @codigostatus, URLVIDEO = @video, ORDEM = @ordem 
                                            WHERE CODIGOTIPOAJUDA = @codigotipoajuda 
                                            AND CODIGOFORMULARIO = @codigoformulario";

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object retorno = db.Select(sql, param);
                        db.Commit();
                        return JsonConvert.SerializeObject(retorno);
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível alterar a Ajuda Passo a Passo informada.", e);
                    }
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível alterar o registro.");
            }
        }

    }

}
