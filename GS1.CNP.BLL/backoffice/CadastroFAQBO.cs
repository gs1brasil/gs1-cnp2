﻿using System;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("CadastroFAQBO")]
    public class CadastroFAQBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CadastroFAQ"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarFAQs(dynamic parametro)
        {
            string sql = @"SELECT F.CODIGO, F.NOME AS PERGUNTA, F.DESCRICAO AS RESPOSTA, F.URL AS VIDEO, F.ORDEM, F.DATACADASTRO, S.NOME AS STATUSPUBLICACAO, U.NOME AS NOMEUSUARIO, 
                                I.CODIGO AS CODIGOIDIOMA, I.NOME AS NOMEIDIOMA, F.CODIGOSTATUSPUBLICACAO AS CODIGOSTATUS, SV.CODIGO AS CODIGOSTATUSVISUALIZACAOFAQ, SV.NOME AS NOMESTATUSVISUALIZACAOFAQ
                            FROM FAQ F WITH (NOLOCK) 
		                    INNER JOIN USUARIO U   (NOLOCK)  ON U.CODIGO = F.CODIGOUSUARIOALTERACAO
                            INNER JOIN STATUSPUBLICACAO S   (NOLOCK)   ON S.CODIGO = F.CODIGOSTATUSPUBLICACAO
                            INNER JOIN IDIOMA I (NOLOCK)  ON I.CODIGO = F.CODIGOIDIOMA
                            INNER JOIN STATUSVISUALIZACAOFAQ SV   (NOLOCK)   ON SV.CODIGO = F.CODIGOSTATUSVISUALIZACAOFAQ";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarFAQsPesquisa(dynamic parametro)
        {

            if (parametro != null && parametro.campos != null)
            {
                string sql = @"SELECT F.CODIGO, F.NOME AS PERGUNTA, F.DESCRICAO AS RESPOSTA, F.URL AS VIDEO, F.ORDEM, F.DATACADASTRO, S.NOME AS STATUSPUBLICACAO, U.NOME AS NOMEUSUARIO, 
                                I.CODIGO AS CODIGOIDIOMA, I.NOME AS NOMEIDIOMA, F.CODIGOSTATUSPUBLICACAO AS CODIGOSTATUS, SV.CODIGO AS CODIGOSTATUSVISUALIZACAOFAQ, SV.NOME AS NOMESTATUSVISUALIZACAOFAQ
                            FROM FAQ F WITH (NOLOCK) 
		                    INNER JOIN USUARIO U  (NOLOCK)  ON U.CODIGO = F.CODIGOUSUARIOALTERACAO
                            INNER JOIN STATUSPUBLICACAO S  (NOLOCK)  ON S.CODIGO = F.CODIGOSTATUSPUBLICACAO
                            INNER JOIN IDIOMA I (NOLOCK)  ON I.CODIGO = F.CODIGOIDIOMA
                            INNER JOIN STATUSVISUALIZACAOFAQ SV  (NOLOCK)  ON SV.CODIGO = F.CODIGOSTATUSVISUALIZACAOFAQ
                            WHERE 1 = 1 ";

                if (parametro.campos.codigoidioma != null) 
                {
                    sql += " AND F.CODIGOIDIOMA = @codigoidioma ";
                }

                if (parametro.campos.datacadastro != null && parametro.campos.datacadastro != "")
                {
                    parametro.campos.datacadastro = Core.Util.ChangeDateFormat(parametro.campos.datacadastro.ToString());
                    sql += " AND CONVERT(VARCHAR(10),F.DATACADASTRO,120) = @datacadastro ";
                }

                if (parametro.campos.codigostatus != null)
                {
                    sql += " AND F.CODIGOSTATUSPUBLICACAO = @codigostatus ";
                }

                if (parametro.campos.ordem != null)
                {
                    sql += " AND F.ORDEM = @ordem ";
                }

                if (parametro.campos.pergunta != null && parametro.campos.pergunta != "")
                {
                    sql += " AND F.NOME like '%' + @pergunta + '%' ";
                }

                if (parametro.campos.codigostatusvisualizacaofaq != null)
                {
                    sql += " AND F.CODIGOSTATUSVISUALIZACAOFAQ = @codigostatusvisualizacaofaq ";
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return null;
            }
        }

        [GS1CNPAttribute(true, true, "EditarFAQ", "VisualizarFAQ")]
        public string AlteraFAQ(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();

                    //Inserção na tabela FAQHISTORICO
                    string sql_historico = @"INSERT INTO FAQHISTORICO (CODIGO, NOME, DESCRICAO, URL, ORDEM, DATACADASTRO, CODIGOUSUARIOALTERACAO,
                                                        CODIGOSTATUSPUBLICACAO, DATAHISTORICO, CODIGOIDIOMA, CODIGOSTATUSVISUALIZACAOFAQ)
	                                            SELECT CODIGO, NOME, DESCRICAO, URL, ORDEM, DATACADASTRO, CODIGOUSUARIOALTERACAO, CODIGOSTATUSPUBLICACAO, GETDATE(), CODIGOIDIOMA, CODIGOSTATUSVISUALIZACAOFAQ 
				                                                FROM FAQ FA WITH (NOLOCK)
				                                            WHERE FA.CODIGO = @codigo";
                    try
                    {
                        dynamic param1 = null;

                        if (parametro.campos != null)
                            param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        db.Select(sql_historico, param1);

                        if (parametro != null && parametro.campos != null)
                            parametro.campos.codigousuarioalteracao = user.id.ToString();

                        string sql = @"UPDATE FAQ SET NOME = @pergunta, DESCRICAO = @resposta, ORDEM = @ordem, CODIGOUSUARIOALTERACAO = @codigousuarioalteracao, DATACADASTRO = GETDATE(), 
                                            CODIGOSTATUSPUBLICACAO = @codigostatus, URL = @video, CODIGOIDIOMA = @codigoidioma, CODIGOSTATUSVISUALIZACAOFAQ = @codigostatusvisualizacaofaq
                                            WHERE CODIGO = @codigo";

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object retorno = db.Select(sql, param);
                        db.Commit();
                        return JsonConvert.SerializeObject(retorno);
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível alterar a Ajuda informada.", e);
                    }
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível alterar o registro.");
            }
        }

        [GS1CNPAttribute("CadastrarFAQ")]
        public string CadastraFAQ(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    if (parametro != null && parametro.campos != null)
                    {
                        parametro.campos.codigousuarioalteracao = user.id.ToString();

                        if (parametro.campos.video == null)
                            parametro.campos.video = "";
                    }

                    string sql = @"INSERT INTO FAQ (NOME, DESCRICAO, URL, ORDEM, DATACADASTRO, CODIGOUSUARIOALTERACAO, CODIGOSTATUSPUBLICACAO, CODIGOIDIOMA, CODIGOSTATUSVISUALIZACAOFAQ)
                                            VALUES (@pergunta, @resposta, @video, @ordem, GETDATE(), @codigousuarioalteracao, @codigostatus, @codigoidioma, @codigostatusvisualizacaofaq)";

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    db.Commit();
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível alterar o registro.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarStatusPublicacao(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME FROM STATUSPUBLICACAO WITH (NOLOCK) WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTiposDisponibilidade(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME FROM STATUSVISUALIZACAOFAQ WITH (NOLOCK) WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
