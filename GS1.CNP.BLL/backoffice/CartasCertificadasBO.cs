﻿    using System;
using System.Collections.Generic;
using System.Linq;
    using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Office.Interop.Word;
using System.IO;
using System.Web;
using Novacode;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("CartasCertificadasBO")]
    public class CartasCertificadasBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CARTASCERTIFICADAS"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutos(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null)
            {
                parametro.campos.codigoassociado = associado.codigo.ToString();
                dynamic param = null;

                string sql = @"SELECT p.globalTradeItemNumber AS gtin,
	                                p.productDescription AS descricao,
	                                tg.Nome AS tipoDeGtin,
	                                p.CodigoStatusGTIN,
									c.NumeroPrefixo,
                                    c.chave,
									(select 0) AS checked 
                                FROM produto AS p (Nolock)
                                INNER JOIN TipoGTIN AS  (Nolock) tg ON tg.Codigo = p.CodigoTipoGTIN
								INNER JOIN PrefixoLicencaAssociado AS  (Nolock) pla on pla.NumeroPrefixo = p.NrPrefixo
								INNER JOIN Chave as c   (Nolock) on c.NumeroPrefixo = p.NrPrefixo
                                WHERE p.CodigoStatusGTIN IN (1,3,6)
								AND p.globalTradeItemNumber is not null
								AND pla.CodigoAssociado = @codigoassociado ";

                if (user.id_tipousuario != 1)
                {
                    sql += "AND c.DataExpiracao IS NULL ";
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    
                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return "Não foi possível buscar os dados do associado.";
            }
            
        }

        [GS1CNPAttribute(false)]
        public string BuscarProduto(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && parametro.campos != null)
            {
                string sql = @"SELECT p.globalTradeItemNumber AS gtin,
	                                p.productDescription AS descricao,
	                                tg.Nome AS tipoDeGtin,
	                                p.CodigoStatusGTIN,
									c.NumeroPrefixo,
                                    c.chave,
									(select 0) AS checked 
                                FROM produto AS p  (Nolock)
                                INNER JOIN TipoGTIN AS tg  (Nolock) ON tg.Codigo = p.CodigoTipoGTIN
								INNER JOIN PrefixoLicencaAssociado AS  (Nolock) pla on pla.NumeroPrefixo = p.NrPrefixo
								INNER JOIN Chave as c  (Nolock) on c.NumeroPrefixo = p.NrPrefixo
                                WHERE p.CodigoStatusGTIN IN (1,3,6)
								AND p.globalTradeItemNumber is not null
								AND pla.CodigoAssociado = @codigoassociado ";

                if (user.id_tipousuario != 1)
                {
                    sql += "AND c.DataExpiracao IS NULL ";
                }

                if (parametro.campos.gtin != null && parametro.campos.gtin.Value != null && Convert.ToString(parametro.campos.gtin) != String.Empty)
                {
                    sql += "AND p.globalTradeItemNumber = @gtin ";
                }

                if (parametro.campos.codigotipogtin != null && parametro.campos.codigotipogtin.Value != null && Convert.ToString(parametro.campos.codigotipogtin) != String.Empty)
                {
                    sql += "AND p.CodigoTipoGTIN = @codigotipogtin ";
                }

                if (parametro.campos.descricao != null && parametro.campos.descricao.Value != null && Convert.ToString(parametro.campos.descricao) != String.Empty)
                {
                    sql += "AND p.productDescription like '%'+ @descricao +'%' ";
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    parametro.campos.codigoassociado = associado.codigo.ToString();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> produtos = db.Select(sql, param);

                    return JsonConvert.SerializeObject(produtos);
                }
            }
            else
            {
                return "Não foi possível buscar os dados do associado.";
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTiposGtin(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                object retorno = db.All("TIPOGTIN");
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public void GerarCarta(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");
            string caminhoTemplate = BuscarCaminhoTemplate();
            string contato = BuscarContatoAssociado();
            string chave = FormatarChave(parametro.campos.chaves.ToObject<List<string>>());

            if (caminhoTemplate == String.Empty)
            {
                throw new GS1TradeException("Não existe modelo para esta carta.");
            }

            if (contato == String.Empty)
            {
                throw new GS1TradeException("Não existe contato para este associado.");
            }

            if (chave == String.Empty)
            {
                throw new GS1TradeException("Não existe licença para um ou mais produtos selecionados.");
            }

            byte[] bytes = null;
            Application wordApp = new Application();

            try
            {
                Document document = new Document();
                object doNotSaveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;

                if (parametro != null)
                {
                    //if (File.Exists(HttpContext.Current.Server.MapPath(Convert.ToString(caminhoTemplate))))
                    //{
                    try                        
                    {
                        //Log.WriteError("CaminhoTemplate: " + caminhoTemplate, null);
                        //document = wordApp.Documents.Open(HttpContext.Current.Server.MapPath(Convert.ToString(caminhoTemplate)));
                        document = wordApp.Documents.Open(caminhoTemplate);

                        if (user != null)
                        {
                            if (parametro.campos != null)
                            {
                                GS1.CNP.BLL.Core.Util.FindAndReplace(document, "{Data_Extenso}", Convert.ToString(parametro.campos.currentDate.Value));
                                GS1.CNP.BLL.Core.Util.FindAndReplace(document, "{Razao_Social}", Convert.ToString(associado.nome));
                                GS1.CNP.BLL.Core.Util.FindAndReplace(document, "{contato}", Convert.ToString(contato));
                                GS1.CNP.BLL.Core.Util.FindAndReplace(document, "{chave}", Convert.ToString(chave));
                            }

                            string extension = Path.GetExtension(caminhoTemplate);
                            string virtualName = string.Format("/upload/cartas/temp_{0}{1}", System.Guid.NewGuid().ToString(), extension);
                            string fullName = HttpContext.Current.Server.MapPath(virtualName);
                            string Name = document.Name;
                            document.SaveAs2(fullName);
                            document.Close(ref doNotSaveChanges);

                            DocxHelper doc = new DocxHelper();
                            using (DocX file = doc.CreateProdutosTables(virtualName, parametro.campos.produtos))
                            {
                                file.Save();
                                file.Dispose();
                            }

                            document = wordApp.Documents.Open(HttpContext.Current.Server.MapPath(virtualName));
                            //document = wordApp.Documents.Open(virtualName);

                            Object oMissing = System.Reflection.Missing.Value;
                            document.ExportAsFixedFormat(fullName.Replace(".docx", ".pdf"), WdExportFormat.wdExportFormatPDF, false, WdExportOptimizeFor.wdExportOptimizeForOnScreen,
                            WdExportRange.wdExportAllDocument, 1, 1, WdExportItem.wdExportDocumentContent, true, true,
                            WdExportCreateBookmarks.wdExportCreateHeadingBookmarks, true, true, false, ref oMissing);

                            document.Close(ref doNotSaveChanges);
                            wordApp.Quit();

                            bytes = System.IO.File.ReadAllBytes(fullName.Replace(".docx", ".pdf"));

                            if (File.Exists(fullName))
                                File.Delete(fullName);
                            if (File.Exists(fullName.Replace(".docx", ".pdf")))
                                File.Delete(fullName.Replace(".docx", ".pdf"));

                            if (bytes != null)
                            {
                                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });

                                HttpContext.Current.Response.Clear();
                                HttpContext.Current.Response.AddHeader("Content-Type", "application/pdf");
                                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + string.Format("CartaUnificada_{0}_{1}", DateTime.Now.ToString("ddMMyyyy_HHmmss"), ".pdf"));
                                HttpContext.Current.Response.AddHeader("Content-Length", bytes.Length.ToString());
                                HttpContext.Current.Response.AddHeader("Cache-Control", "max-age=0");
                                HttpContext.Current.Response.AddHeader("Accept-Ranges", "none");
                                HttpContext.Current.Response.BinaryWrite(bytes);

                                HttpContext.Current.Response.Flush();

                                //Insere Data de Geração na tabela TemplateCartaUsuario
                                //try
                                //{
                                //    InsereDataGeracao(parametro);
                                //}
                                //catch (Exception e)
                                //{
                                //    //throw new GS1TradeException("Não foi possível gerar a Carta Certificada.", e);
                                //}
                            }
                            else
                            {
                                throw new GS1TradeException("Não foi possível gerar a Carta Certificada.");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        //wordApp.Quit();
                        throw new GS1TradeException("Não foi possível gerar a Carta Certificada.", e);
                    }
                    finally
                    {
                        //wordApp.Quit();
                    }

                    //}
                }
            }
            catch (Exception e)
            {
                //wordApp.Quit();
                throw new GS1TradeException("Não foi possível gerar a Carta Certificada.", e);
            } finally {
                //wordApp.Quit();
            }
        }
        
        public string BuscarCaminhoTemplate()
        {
            using (Database db = new Database("BD"))
            {
                db.Open();
                string sql = @"SELECT t.CaminhoTemplate FROM TemplateCarta t  (Nolock) WHERE t.CodigoLicenca = 8";
                //dynamic retorno = db.Select(sql);

                dynamic retorno = ((List<dynamic>)db.Select(sql)).FirstOrDefault();

                if (retorno != null)
                {
                    return retorno.caminhotemplate;
                }
                else
                {
                    return "";
                }
            }
        }

        public string BuscarContatoAssociado()
        {
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (associado != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    string sql = @"SELECT TOP(1)nome FROM contatoassociado  (Nolock) WHERE CodigoAssociado = " + associado.codigo.ToString() +" AND Status = 1 ORDER BY Codigo";
                    dynamic retorno = ((List<dynamic>)db.Select(sql)).FirstOrDefault();

                    if (retorno != null)
                    {
                        return retorno.nome;
                    }
                    else
                    {
                        return "";
                    }
                    
                }
            }
            else
            {
                return "Não foi possível buscar os dados do associado.";
            }

            
        }

        public string FormatarChave(List<string> chaves) 
        {
            string strChave = String.Empty;

            if (chaves != null)
            {
                for (int i = 0; i < chaves.Count; i++)
                {
                    if (strChave == String.Empty)
                    {
                        strChave = chaves[i];
                    }
                    else
                    {
                        strChave += ", " + chaves[i];
                    }
                    
                }
            }


            return strChave;
        }
        
        [GS1CNPAttribute(false)]
        public string VerificarChaves(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null)
            {
                parametro.campos.codigoassociado = associado.codigo.ToString();
                dynamic param = null;

                string sql = @"SELECT * FROM CHAVE (Nolock) where CodigoAssociado = @codigoassociado ";

                if (user.id_tipousuario != 1)
                {
                    sql += "AND DataExpiracao IS NULL";
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    
                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return "Não foi possível realizar a operação.";
            }
        }
    }
}

