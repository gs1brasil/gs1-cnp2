﻿using System;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using Microsoft.Office.Interop.Word;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("CartasCertificadasGs1BO")]
    public class CartasCertificadasGs1BO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CARTASCERTIFICADAS"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscaInformacoesAssociado(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null)
            {
                parametro.campos.codigousuariologado = user.id.ToString();
                parametro.campos.codigoassociado = associado.codigo.ToString();
                dynamic param = null;

                string sql = @"SELECT DISTINCT D.NOME AS RAZAOSOCIAL, B.NOME AS NOMELICENCA
                                , COALESCE((SELECT TOP 1 NOME FROM CONTATOASSOCIADO C WHERE C.INDICADORPRINCIPAL = 1 AND C.CODIGOASSOCIADO = D.CODIGO AND C.STATUS = 1), D.NOME) AS NOMECONTATO
                                , A.NUMEROPREFIXO AS PREFIXOS
                                , C.CAMINHOTEMPLATE
                                , C.CODIGO AS CODIGOTEMPLATECARTA
                                , (SELECT MAX(DATAGERACAO) FROM TEMPLATECARTAUSUARIO TC  (Nolock)
	                                    WHERE CODIGOTEMPLATECARTA = C.CODIGO AND TC.CODIGOASSOCIADO = D.CODIGO AND A.NUMEROPREFIXO = TC.NUMEROPREFIXO
                                    ) AS DATAGERACAO
                                ,(
		                            SELECT TOP 1 CONVERT(VARCHAR(10),CONVERT(DATE, Z.DATAANIVERSARIO,106),103)
		                            FROM LICENCAASSOCIADO Z  (Nolock)
		                            WHERE Z.CODIGOLICENCA = A.CODIGOLICENCA 
		                            AND Z.CODIGOASSOCIADO = A.CODIGOASSOCIADO
	                            ) AS VALIDADE
                                ,(
			                        SELECT TOP 1 CHAVE FROM CHAVE Z   (Nolock) WHERE Z.NUMEROPREFIXO = A.NUMEROPREFIXO
		                        ) AS CHAVE
                                ,(SELECT TOP 1 PRODUCTDESCRIPTION FROM PRODUTO P   (Nolock) WHERE P.GLOBALTRADEITEMNUMBER =  A.NUMEROPREFIXO AND P.CODIGOASSOCIADO = D.CODIGO) AS DESCRICAO
                                FROM PREFIXOLICENCAASSOCIADO A  (Nolock)
                                INNER JOIN LICENCA B  (Nolock) ON B.CODIGO = A.CODIGOLICENCA
                                INNER JOIN TEMPLATECARTA C  (Nolock) ON C.CODIGOLICENCA = B.CODIGO
                                INNER JOIN ASSOCIADO D  (Nolock) ON D.CODIGO = A.CODIGOASSOCIADO
                                LEFT JOIN ASSOCIADOUSUARIO E  (Nolock) ON E.CODIGOASSOCIADO = D.CODIGO
                                WHERE A.CODIGOASSOCIADO = @codigoassociado
                                AND (E.CODIGOUSUARIO = @codigousuariologado OR
	                                1 = (SELECT Z.CODIGOTIPOUSUARIO FROM USUARIO Z WHERE CODIGO = @codigousuariologado)
	                                )
                                AND C.CAMINHOTEMPLATE IS NOT NULL
                                ORDER BY B.NOME,  A.NUMEROPREFIXO";


//                string sql = @";WITH DADOS AS (	SELECT DISTINCT D.NOME AS RAZAOSOCIAL, B.NOME AS NOMELICENCA, B.CODIGO AS CODIGOLICENCA
//					                    , COALESCE((SELECT TOP 1 NOME FROM CONTATOASSOCIADO C WHERE C.INDICADORPRINCIPAL = 1 AND C.CODIGOASSOCIADO = D.CODIGO AND C.STATUS = 1), D.NOME) AS NOMECONTATO
//					                    , A.NUMEROPREFIXO AS PREFIXO
//					                    , C.CAMINHOTEMPLATE AS CAMINHOTEMPLATE
//					                    , C.CODIGO AS CODIGOTEMPLATECARTA
//					                    , D.CODIGO AS CODIGOASSOCIADO
//					                    FROM PREFIXOLICENCAASSOCIADO A
//					                    INNER JOIN LICENCA B ON B.CODIGO = A.CODIGOLICENCA
//					                    INNER JOIN TEMPLATECARTA C ON C.CODIGOLICENCA = B.CODIGO
//					                    INNER JOIN ASSOCIADO D ON D.CODIGO = A.CODIGOASSOCIADO
//					                    LEFT JOIN ASSOCIADOUSUARIO E ON E.CODIGOASSOCIADO = D.CODIGO
//					                    WHERE A.CODIGOASSOCIADO = 1
//					                    AND (E.CODIGOUSUARIO = 1 OR
//						                    1 = (SELECT Z.CODIGOTIPOUSUARIO FROM USUARIO Z WHERE CODIGO = 1)
//					                    )
//                                        AND B.STATUS = 1
//				                    )
//				
//		                    SELECT RAZAOSOCIAL, NOMELICENCA, NOMECONTATO 
//			                      ,CAMINHOTEMPLATE, CODIGOTEMPLATECARTA	  
//			                      ,SUBSTRING(COALESCE(PREFIXOS,''),0,LEN(PREFIXOS)-1) AS PREFIXOS
//			                      ,TOTALREGISTROS			  			  
//			                      ,(SELECT MAX(TC.DATAGERACAO) FROM TEMPLATECARTAUSUARIO TC 
//				                    INNER JOIN TEMPLATECARTA T ON T.CODIGO = TC.CODIGOTEMPLATECARTA
//				                    WHERE TC.CODIGOTEMPLATECARTA = DADOS2.CODIGOTEMPLATECARTA 
//				                    AND TC.CODIGOASSOCIADO = DADOS2.CODIGOASSOCIADO
//				                    AND T.CODIGOLICENCA = DADOS2.CODIGOLICENCA
//				                    ) AS DATAGERACAO		
//		                    FROM (						
//				                    SELECT DISTINCT RAZAOSOCIAL, NOMELICENCA, CODIGOLICENCA, NOMECONTATO, CAMINHOTEMPLATE, CODIGOTEMPLATECARTA,CODIGOASSOCIADO
//				                    ,STUFF( (SELECT CONVERT(VARCHAR(50), PREFIXO) + ', '
//								                    FROM DADOS X WITH (NOLOCK)
//								                    WHERE X.NOMELICENCA = A.NOMELICENCA
//								                    FOR XML PATH('')		
//						                    ),1,0,'') AS PREFIXOS
//				                    ,(SELECT COUNT(*) FROM DADOS B WHERE B.NOMELICENCA = A.NOMELICENCA ) TOTALREGISTROS 
//				                    FROM DADOS A
//		                    ) AS DADOS2
//                            ORDER BY NOMELICENCA";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return "Não foi possível buscar os dados do associado.";
            }

        }
        
        [GS1CNPAttribute(true, true, "GerarCartasCertificadasGs1", "BaixarChave")]
        public void GerarCarta(dynamic parametro)
        {
            #region OLD
            
            //Login user = (Login)this.getSession("USUARIO_LOGADO");
            //Application wordApp = new Application();
            //Document document = new Document();
            //object doNotSaveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;

            //if (parametro != null)
            //{
            //    if (File.Exists(HttpContext.Current.Server.MapPath(Convert.ToString(parametro.campos.caminhotemplate.Value))))
            //    {
            //        try
            //        {
            //            document = wordApp.Documents.Open(HttpContext.Current.Server.MapPath(Convert.ToString(parametro.campos.caminhotemplate.Value)));

            //            if (user != null)
            //            {
            //                if (parametro.campos != null)
            //                {
            //                    FindAndReplace(document, "{Data Extenso}", Convert.ToString(parametro.campos.currentDate.Value));
            //                    FindAndReplace(document, "{Razao Social}", Convert.ToString(parametro.campos.razaosocial.Value));
            //                    FindAndReplace(document, "{Contato}", Convert.ToString(parametro.campos.nomecontato.Value));
            //                    FindAndReplace(document, "{Data}", Convert.ToString(parametro.campos.currentDate.Value));
            //                    FindAndReplace(document, "{Prefixo}", Convert.ToString(parametro.campos.prefixos.Value));

            //                    if (parametro.campos.prefixocompleto != null)
            //                    {
            //                        FindAndReplace(document, "{Prefixocompleto}", Convert.ToString(parametro.campos.prefixocompleto.Value));
            //                    }

            //                    if (parametro.campos.chave != null)
            //                    {
            //                        FindAndReplace(document, "{Chave}", Convert.ToString(parametro.campos.chave.Value));
            //                    }

            //                    if (parametro.campos.nomelicenca == "GTIN-13")
            //                    {

            //                        if(Convert.ToString(parametro.campos.prefixos.Value).Length == 7)
            //                        {
            //                            FindAndReplace(document, "{quantidadeprodutos}", "XXXXX");
            //                        }
            //                        else if(Convert.ToString(parametro.campos.prefixos.Value).Length == 8)
            //                        {
            //                            FindAndReplace(document, "{quantidadeprodutos}", "XXXX");
            //                        }
            //                        else if(Convert.ToString(parametro.campos.prefixos.Value).Length == 9)
            //                        {
            //                            FindAndReplace(document, "{quantidadeprodutos}", "XXX");
            //                        }
            //                        else if(Convert.ToString(parametro.campos.prefixos.Value).Length == 10)
            //                        {
            //                            FindAndReplace(document, "{quantidadeprodutos}", "XX");
            //                        }
            //                        else if(Convert.ToString(parametro.campos.prefixos.Value).Length == 11)
            //                        {
            //                            FindAndReplace(document, "{quantidadeprodutos}", "X");
            //                        }
            //                    }

            //                    //parametro.campos.prefixos.Value = parametro.campos.prefixos.Value.Replace("/ ", "\x0B");
            //                }

            //                string extension = Path.GetExtension(HttpContext.Current.Server.MapPath(Convert.ToString(parametro.campos.caminhotemplate.Value)));
            //                string fullName = HttpContext.Current.Server.MapPath(string.Format("/upload/cartas/temp_{0}{1}", System.Guid.NewGuid().ToString(), extension));
            //                string Name = document.Name;
            //                document.SaveAs2(fullName);

            //                //TODO - mudar download para pdf
            //                Object oMissing = System.Reflection.Missing.Value;
            //                document.ExportAsFixedFormat(fullName.Replace(".docx",".pdf"), WdExportFormat.wdExportFormatPDF, false, WdExportOptimizeFor.wdExportOptimizeForOnScreen,
            //                WdExportRange.wdExportAllDocument,1,1,WdExportItem.wdExportDocumentContent,true,true,
            //                WdExportCreateBookmarks.wdExportCreateHeadingBookmarks,true,true,false,ref oMissing);

            //                document.Close(ref doNotSaveChanges);
            //                wordApp.Quit();

            //                byte[] bytes = System.IO.File.ReadAllBytes(fullName.Replace(".docx", ".pdf"));

            //                if (File.Exists(fullName))
            //                    File.Delete(fullName);

            //                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });

            //                HttpContext.Current.Response.Clear();
            //                HttpContext.Current.Response.AddHeader("Content-Type", "application/pdf");
            //                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + string.Format("CartaCertificada_{0}_{1}{2}", parametro.campos.nomelicenca.Value.ToString(), DateTime.Now.ToString("ddMMyyyy_HHmmss"), ".pdf"));
            //                HttpContext.Current.Response.AddHeader("Content-Length", bytes.Length.ToString());
            //                HttpContext.Current.Response.AddHeader("Cache-Control", "max-age=0");
            //                HttpContext.Current.Response.AddHeader("Accept-Ranges", "none");
            //                HttpContext.Current.Response.BinaryWrite(bytes);

            //                HttpContext.Current.Response.Flush();

            //                //Insere Data de Geração na tabela TemplateCartaUsuario
            //                try
            //                {
            //                    InsereDataGeracao(parametro);
            //                }
            //                catch (Exception)
            //                {
            //                    //throw new GS1TradeException("Não foi possível gerar a Carta Certificada.");
            //                }
            //            }
            //            else
            //            {
            //                document.Close(ref doNotSaveChanges);
            //                throw new GS1TradeException("Não foi possível gerar a Carta Certificada.");
            //            }
            //        }
            //        catch(Exception)
            //        {
            //            throw new GS1TradeException("Não foi possível gerar a Carta Certificada.");
            //        }
            //        finally
            //        {
            //            wordApp.Quit();
            //        }
            //    }
            //    else
            //    {
            //        wordApp.Quit();
            //        throw new GS1TradeException("Não foi possível encontrar o template para geração.");
            //    }
            //}
            //else
            //{
            //    wordApp.Quit();
            //    throw new GS1TradeException("Não foi possível gerar a Carta Certificada.");
            //}
            #endregion

            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            parametro.campos.user = JsonConvert.SerializeObject(this.getSession("USUARIO_LOGADO"));
            parametro.campos.associado = JsonConvert.SerializeObject(this.getSession("ASSOCIADO_SELECIONADO"));

            if (parametro != null)
            {
                try
                {
                    GS1.CNP.BLL.Core.Util.GerarBaixarCarta(parametro);
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Não foi possível gerar a Carta Certificada.", e);
                }
            }
        }

//        public object InsereDataGeracao(dynamic parametro)
//        {
//            dynamic param = null;

//            Login user = (Login)this.getSession("USUARIO_LOGADO");
//            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

//            if (user != null)
//            {
//                using (Database db = new Database("BD"))
//                {
//                    db.Open(true);
//                    db.BeginTransaction();

//                    try
//                    {

////                    string sql_prefixos = @"SELECT DISTINCT A.NUMEROPREFIXO AS PREFIXO
////                                
////                                FROM PREFIXOLICENCAASSOCIADO A
////                                INNER JOIN LICENCA B ON B.CODIGO = A.CODIGOLICENCA
////                                INNER JOIN TEMPLATECARTA C ON C.CODIGOLICENCA = B.CODIGO
////                                INNER JOIN ASSOCIADO D ON D.CODIGO = A.CODIGOASSOCIADO
////                                LEFT JOIN ASSOCIADOUSUARIO E ON E.CODIGOASSOCIADO = D.CODIGO
////                                WHERE A.CODIGOASSOCIADO = 1
////                                AND (E.CODIGOUSUARIO = 1 OR
////	                                1 = (SELECT Z.CODIGOTIPOUSUARIO FROM USUARIO Z WHERE CODIGO = 1)
////	                            )";

////                                if(parametro != null && parametro.campos != null) {
////                                    sql_prefixos += " AND C.CODIGO = " + parametro.campos.codigotemplatecarta;
////                                }

////                        List<dynamic> prefixo = db.Select(sql_prefixos, null);


//                        if (parametro.campos != null)
//                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

//                        object retorno = null;

//                        //foreach (dynamic pref in prefixo)
//                        //{
//                            string sql_template = @"INSERT INTO TEMPLATECARTAUSUARIO VALUES (@codigotemplatecarta, @caminhotemplate, @codigousuario, getDate(), @codigoassociado, @prefixos)";
//                            retorno = db.Select(sql_template, param);
//                        //}

//                        db.Commit();
//                        return JsonConvert.SerializeObject(retorno);
//                    }
//                    catch(Exception e) {
//                        db.Rollback();
//                        return "Não foi possível buscar os dados do associado.";
//                    }
//                }
//            }
//            else
//            {
//                return "Não foi possível buscar os dados do associado.";
//            }
//        }

        public static void FindAndReplace(Document document, string findText, string replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;

            Microsoft.Office.Interop.Word.Range range = document.Content;
            Microsoft.Office.Interop.Word.Find find = range.Find;

            find.Execute(findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

    }

}
