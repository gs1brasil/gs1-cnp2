﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("CertificacaoProdutosBO")]
    public class CertificacaoProdutosBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CertificacaoProdutos"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public object BuscarCertificacoes(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT PRODUTO, [DATA DE LIBERAÇÃO] as DATALIBERACAO, 
                                CONVERT(VARCHAR(10),CONVERT(DATE, DATEADD(YEAR, 1, CONVERT(DATETIME, [DATA DE LIBERAÇÃO], 103)),106),103) AS DATAVALIDADE, 
                                SMART_SALDOAMOSTRAS AS QUANTIDADECERTIFICACOESDISPONIVEIS, TOTALQUANTIDADECERTIFICACOESDISPONIVEIS = SUM(SMART_SALDOAMOSTRAS) OVER(), STATUS
                                FROM VW_SALDOCERTIFICACAO  (Nolock)
                                WHERE ACCOUNTNUMBER = '" + associado.cad.ToString() + "'";

                using (Database db = new Database("BDBASEUNICACNP"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisarCertificacoes(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT PRODUTO, [DATA DE LIBERAÇÃO] as DATALIBERACAO, 
                                CONVERT(VARCHAR(10),CONVERT(DATE, DATEADD(YEAR, 1, CONVERT(DATETIME, [DATA DE LIBERAÇÃO], 103)),106),103) AS DATAVALIDADE, 
                                SMART_SALDOAMOSTRAS AS QUANTIDADECERTIFICACOESDISPONIVEIS, TOTALQUANTIDADECERTIFICACOESDISPONIVEIS = SUM(SMART_SALDOAMOSTRAS) OVER(), STATUS
                                FROM VW_SALDOCERTIFICACAO C  (Nolock)
                                WHERE ACCOUNTNUMBER = '" + associado.cad.ToString() + "'";

                if (parametro != null && parametro.campos != null)
                {

                    if (parametro.campos.dataliberacao != null)
                    {
                        //parametro.campos.dataliberacao = DateTime.ParseExact(Convert.ToString(parametro.campos.dataliberacao.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        sql += @" AND @dataliberacao = C.[DATA DE LIBERAÇÃO] ";
                    }

                    if (parametro.campos.datavalidade != null)
                    {
                        //parametro.campos.datavalidade = DateTime.ParseExact(Convert.ToString(parametro.campos.datavalidade.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        sql += @" AND @datavalidade = CONVERT(VARCHAR(10),CONVERT(DATE, DATEADD(YEAR, 1, CONVERT(DATETIME, [DATA DE LIBERAÇÃO], 103)),106),103) ";
                    }

                    if (parametro.campos.status != null)
                    {
                        sql += " AND C.STATUS = @status";
                    }

                    if (parametro.campos.produto != null)
                    {
                        sql += " AND C.PRODUTO LIKE '%' + @produto + '%'";
                    }
                }

                using (Database db = new Database("BDBASEUNICACNP"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }
    }

}
