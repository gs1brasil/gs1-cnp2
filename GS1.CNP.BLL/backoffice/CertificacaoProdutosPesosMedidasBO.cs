﻿using System;
using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("CertificacaoProdutosPesosMedidasBO")]
    public class CertificacaoProdutosPesosMedidasBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CertificacaoProdutosPesosMedidas"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public object BuscarCertificacoesPesosMedidas(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT C.CODIGO, C.DATAALTERACAO, P.PRODUCTDESCRIPTION, P.GLOBALTRADEITEMNUMBER, C.CODIGOPRODUTO, P.PRODUCTDESCRIPTION AS DESCRICAOPRODUTO, 
                                    U.NOME AS USUARIO, P.CODITEM, P.NRPREFIXO, C.STATUS, CONVERT(VARCHAR(10),CONVERT(DATE,C.DATAVALIDADE,106),103) AS DATAVALIDADE, 
                                    CONVERT(VARCHAR(10),CONVERT(DATE,C.DATACERTIFICADO,106),103) AS DATACERTIFICADO, T.CODIGO AS CODIGOTIPOGTIN, T.NOME AS NOMETIPOGTIN
                                    FROM CERTIFICACAOPRODUTOSPESOSMEDIDAS C WITH (NOLOCK)
                                    LEFT JOIN PRODUTO P WITH (NOLOCK) ON (P.CODIGOPRODUTO = C.CODIGOPRODUTO) AND (P.GLOBALTRADEITEMNUMBER IS NOT NULL)
                                    INNER JOIN USUARIO U WITH (NOLOCK) ON (U.CODIGO = C.CODIGOUSUARIOALTERACAO)
									INNER JOIN TIPOGTIN T WITH (NOLOCK) ON (T.CODIGO = P.CODIGOTIPOGTIN)
                                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutosAutoComplete(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, PRODUCTDESCRIPTION 
                                FROM PRODUTO P WITH (NOLOCK)
                                WHERE (P.GLOBALTRADEITEMNUMBER LIKE '%' + @nome + '%' 
                                OR P.PRODUCTDESCRIPTION LIKE '%' + @nome + '%')
                                AND P.GLOBALTRADEITEMNUMBER IS NOT NULL 
                                AND P.CODIGOASSOCIADO = " + associado.codigo.ToString();


                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    List<dynamic> produtos = db.Select(sql, param);

                    return JsonConvert.SerializeObject(produtos);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisaProdutos(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT P.CODIGOPRODUTO
                                    , P.PRODUCTDESCRIPTION
                                    , P.GLOBALTRADEITEMNUMBER
                                    , TG.NOME AS NOMETIPOGTIN
                                    , P.NRPREFIXO 
                                    , P.CODITEM 
                                FROM PRODUTO P WITH (NOLOCK)
                                    INNER JOIN TIPOGTIN TG WITH (NOLOCK) ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                WHERE P.GLOBALTRADEITEMNUMBER IS NOT NULL 
                                    AND P.CODIGOASSOCIADO = " + associado.codigo.ToString() + @"
                                    AND CAST(P.GLOBALTRADEITEMNUMBER AS VARCHAR(MAX)) LIKE ('%' + ISNULL(@gtin, '') + '%')
                                    AND P.PRODUCTDESCRIPTION LIKE ('%' + ISNULL(@descricao, '') + '%')
                                    AND (P.CODIGOSTATUSGTIN = @codigostatusgtin OR @codigostatusgtin IS NULL OR @codigostatusgtin = '')
                                    AND (P.CODIGOTIPOGTIN = @codigotipogtin OR @codigotipogtin IS NULL OR @codigotipogtin = '')
                                    AND P.CODIGOSTATUSGTIN IN (1,3,6) ";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute("CadastrarCertificacaoPesosMedidas", true, true)]
        public object InsereCertificacaoPesosMedidas(dynamic parametro)
        {
            dynamic param = null;

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();

                    try
                    {
                        parametro.campos.codigoassociado = associado.codigo.ToString();
                        parametro.campos.codigousuarioalteracao = user.id.ToString();

                        //Converte campos data
                        if (parametro.campos.datavalidade != null)
                            parametro.campos.datavalidade = parametro.campos.datavalidade.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        if (parametro.campos.datacertificado != null)
                            parametro.campos.datacertificado = parametro.campos.datacertificado.ToString("MM/dd/yyyy HH:mm:ss.fff");

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object retorno = null;
                        string sql = String.Empty;

                        sql = @"INSERT INTO CERTIFICACAOPRODUTOSPESOSMEDIDAS (Status, DataAlteracao, DataCertificado, DataValidade, CodigoProduto, CodigoUsuarioAlteracao, CodigoAssociado)
                                                VALUES (@status, getDate(), @datacertificado, @datavalidade, @codigoproduto, @codigousuarioalteracao, @codigoassociado)";

                        retorno = db.Select(sql, param);

                        db.Commit();
                        return JsonConvert.SerializeObject(retorno);
                    }
                    catch (GS1TradeException e)
                    {
                        throw new GS1TradeException(e.Message.ToString());
                    }
                }
            }
            else
            {
                return "Não foi possível buscar os dados do associado.";
            }
        }

        [GS1CNPAttribute("EditarCertificacaoPesosMedidas", true, true)]
        public void EditarCertificacaoPesosMedidas(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                using (Database db = new Database("BD"))
                {

                    db.Open(true);
                    db.BeginTransaction();

                    //Inserção na tabela CERTIFICACAOPRODUTOSPESOSMEDIDASHISTORICO
                    string sql_historico = @"INSERT INTO CERTIFICACAOPRODUTOSPESOSMEDIDASHISTORICO (CODIGO, STATUS, DATAALTERACAO, DATACERTIFICADO, DATAVALIDADE, CODIGOPRODUTO, CODIGOUSUARIOALTERACAO,
                                                    CODIGOASSOCIADO, DATAHISTORICO)
	                                        SELECT CODIGO, STATUS, DATAALTERACAO, DATACERTIFICADO, DATAVALIDADE, CODIGOPRODUTO, CODIGOUSUARIOALTERACAO,
                                                    CODIGOASSOCIADO, GETDATE()
				                                    FROM CERTIFICACAOPRODUTOSPESOSMEDIDAS WITH (NOLOCK)
				                                    WHERE CERTIFICACAOPRODUTOSPESOSMEDIDAS.CODIGO = @codigo";

                    dynamic param1 = null;

                    if (parametro.campos != null)
                        param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql_historico, param1);

                    try
                    {
                        //Converte campos data
                        if (parametro.campos.datavalidade != null)
                            parametro.campos.datavalidade = parametro.campos.datavalidade.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        if (parametro.campos.datacertificado != null)
                            parametro.campos.datacertificado = parametro.campos.datacertificado.ToString("MM/dd/yyyy HH:mm:ss.fff");

                        parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        parametro.campos.codigousuarioalteracao = user.id.ToString();
                        parametro.campos.codigoassociado = associado.codigo.ToString();
                        parametro.campos.Remove("codigo");
                        base.Update((object)parametro);

                        db.Commit();
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível cadastrar a Certificação de Produtos informada.", e);
                    }

                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a atualização.");
            }

        }

        [GS1CNPAttribute(false)]
        public object PesquisarCertificacoesPesosMedidas(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT C.CODIGO, C.DATAALTERACAO, P.PRODUCTDESCRIPTION, P.GLOBALTRADEITEMNUMBER, C.CODIGOPRODUTO, P.PRODUCTDESCRIPTION AS DESCRICAOPRODUTO, 
                                    U.NOME AS USUARIO, P.CODITEM, P.NRPREFIXO, C.STATUS, CONVERT(VARCHAR(10),CONVERT(DATE,C.DATAVALIDADE,106),103) AS DATAVALIDADE, 
                                    CONVERT(VARCHAR(10),CONVERT(DATE,C.DATACERTIFICADO,106),103) AS DATACERTIFICADO, T.CODIGO AS CODIGOTIPOGTIN, T.NOME AS NOMETIPOGTIN
                                    FROM CERTIFICACAOPRODUTOSPESOSMEDIDAS C WITH (NOLOCK)
                                    LEFT JOIN PRODUTO P WITH (NOLOCK) ON (P.CODIGOPRODUTO = C.CODIGOPRODUTO) AND (P.GLOBALTRADEITEMNUMBER IS NOT NULL)
                                    INNER JOIN USUARIO U WITH (NOLOCK) ON (U.CODIGO = C.CODIGOUSUARIOALTERACAO)
									INNER JOIN TIPOGTIN T WITH (NOLOCK) ON (T.CODIGO = P.CODIGOTIPOGTIN)
                                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString();

                if (parametro != null && parametro.campos != null)
                {

                    if (parametro.campos.datacertificado != null)
                    {
                        parametro.campos.datacertificado = DateTime.ParseExact(Convert.ToString(parametro.campos.datacertificado.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        sql += @" AND @datacertificado = C.DATACERTIFICADO ";
                    }

                    if (parametro.campos.datavalidade != null)
                    {
                        parametro.campos.datavalidade = DateTime.ParseExact(Convert.ToString(parametro.campos.datavalidade.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        sql += @" AND @datavalidade = C.DATAVALIDADE ";
                    }

                    if (parametro.campos.status != null)
                    {
                        sql += " AND C.STATUS = @status";
                    }

                    if (parametro.campos.produto != null)
                    {
                        sql += " AND P.PRODUCTDESCRIPTION LIKE '%' + @produto + '%'";
                    }

                    if (parametro.campos.codigotipogtin != null)
                    {
                        sql += " AND P.CODIGOTIPOGTIN = @codigotipogtin";
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }
    }

}
