﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using Newtonsoft.Json;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using System.Configuration;
using System.Net.Mime;
using System.IO;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("ChaveBO")]
    class ChaveBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CHAVE"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarChaves(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT DISTINCT C.CODIGOCHAVE,
	                            A.NOME AS RAZAOSOCIAL,
	                            A.CPFCNPJ,
	                            PLA.NUMEROPREFIXO AS PREFIXOS,
	                            C.DATACRIACAO,
	                            C.DATAEXPIRACAO,
	                            C.CHAVE,
                                TC.CODIGO AS CODIGOTEMPLATECARTA,
								TC.CAMINHOTEMPLATE,
								L.NOME AS NOMELICENCA,
								(SELECT TOP(1) NOME FROM CONTATOASSOCIADO WITH (NOLOCK) WHERE CODIGOASSOCIADO = A.CODIGO AND STATUS = 1 AND INDICADORPRINCIPAL = 1 ORDER BY DATAATUALIZACAOCRM DESC) AS NOMECONTATO, 
								(SELECT TOP(1) EMAIL FROM CONTATOASSOCIADO WITH (NOLOCK) WHERE CODIGOASSOCIADO = A.CODIGO AND STATUS = 1 AND INDICADORPRINCIPAL = 1 ORDER BY DATAATUALIZACAOCRM DESC) AS EMAILCONTATO,
                                (
		                            SELECT TOP 1 CONVERT(VARCHAR(10),CONVERT(DATE, Z.DATAANIVERSARIO,106),103)
		                            FROM LICENCAASSOCIADO Z (Nolock)
		                            WHERE Z.CODIGOLICENCA = PLA.CODIGOLICENCA 
		                            AND Z.CODIGOASSOCIADO = PLA.CODIGOASSOCIADO
	                            ) AS VALIDADE,
	                            CASE WHEN C.DATAEXPIRACAO IS NULL 
		                            THEN 'ATIVO'
		                            ELSE 'EXPIRADA'
	                            END AS STATUS
                            FROM CHAVE C
                            INNER JOIN ASSOCIADO AS A WITH (NOLOCK) ON A.CODIGO = C.CODIGOASSOCIADO
                            INNER JOIN PREFIXOLICENCAASSOCIADO AS PLA WITH (NOLOCK) ON PLA.NUMEROPREFIXO = C.NUMEROPREFIXO
							INNER JOIN LICENCA AS L WITH (NOLOCK) ON L.CODIGO = C.CODIGOLICENCA
							LEFT  JOIN TEMPLATECARTA AS TC WITH (NOLOCK) ON TC.CODIGOLICENCA = PLA.CODIGOLICENCA
                            WHERE C.CODIGOASSOCIADO = @codigoAssociado
                            ORDER BY C.DATAEXPIRACAO";

                parametro.campos.codigoAssociado = associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Open();

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarPrefixosSemChave(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user.id_tipousuario == 1)
            {
                if (associado != null)
                {
                    string sql = @"SELECT PLA.CODIGOASSOCIADO,
	                                    PLA.CODIGOLICENCA, 
	                                    A.NOME AS RAZAO,
	                                    A.CPFCNPJ,
	                                    PLA.NUMEROPREFIXO,
	                                    PLA.CODIGOSTATUSPREFIXO
                                    FROM PREFIXOLICENCAASSOCIADO AS PLA WITH (NOLOCK)
                                    INNER JOIN ASSOCIADO AS A WITH (NOLOCK) ON A.CODIGO = PLA.CODIGOASSOCIADO
                                    WHERE PLA.NUMEROPREFIXO NOT IN (
								                                    SELECT C.NUMEROPREFIXO 
									                                    FROM CHAVE C INNER JOIN PREFIXOLICENCAASSOCIADO PLA ON PLA.CODIGOASSOCIADO = C.CODIGOASSOCIADO 
									                                    WHERE PLA.CODIGOASSOCIADO = @codigoAssociado
								                                    )
                                        AND PLA.CODIGOASSOCIADO = @codigoAssociado
	                                    AND PLA.CODIGOSTATUSPREFIXO IN (1, 8)";

                    parametro.campos.codigoAssociado = associado.codigo.ToString();

                    using (Database db = new Database("BD"))
                    {
                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        db.Open();

                        List<dynamic> retorno = db.Select(sql, param);

                        return JsonConvert.SerializeObject(retorno);
                    }
                }
                else
                {
                    throw new GS1TradeException("Não foi possível realizar a consulta");
                }
            }
            else
            {
                return "";
            }
        }

        [GS1CNPAttribute("GerarChave")]
        public object GerarChave(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            
            //Verifica se o usuário possui perfil que permita executar esta operação
            if (user.id_tipousuario == 1)
            {
                using (Database db = new Database("BD"))
                {
                    //Verifica se ja existe uma chave para o prefixo.
                    string sqlCheckPrefixo = @"SELECT COUNT(*) AS quantidade FROM CHAVE (NOlock) WHERE NumeroPrefixo = @numeroprefixo AND CodigoAssociado = @codigoassociado";
                    dynamic paramCheckPrefixo = null;

                    if (parametro.campos != null)
                    {
                        paramCheckPrefixo = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    db.Open();
                    dynamic resultCheckPrefixo = ((List<dynamic>)db.Select(sqlCheckPrefixo, paramCheckPrefixo)).FirstOrDefault();

                    //Se não existir uma chave para o prefixo,uma nova chave será gerada
                    if (resultCheckPrefixo.quantidade == 0)
                    {
                        parametro.campos.chave = NovaChave();
                        parametro.campos.codigousuariocriacao = user.id.ToString();

                        string sql = @"INSERT INTO Chave (CodigoLicenca, CodigoAssociado, NumeroPrefixo, Chave, DataCriacao, DataExpiracaoOriginal, CodigoUsuarioCriacao)
                                        VALUES (@codigolicenca,@codigoassociado,@numeroprefixo, @chave, CONVERT(DATETIME,CONVERT(VARCHAR(10),GETDATE(),101)), DATEADD(YEAR, 1, CONVERT(DATETIME,CONVERT(VARCHAR(10),GETDATE(),101))), @codigousuariocriacao)";

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object retorno = db.Select(sql, param);
                        return JsonConvert.SerializeObject(retorno);
                    }
                    else
                    {
                        throw new GS1TradeException("Já existe uma Chave para este prefixo!");
                    }
                }
            }
            else
            {
                throw new GS1TradeException("Sem permissão para acessar esta funcionalidade.");
            }
        }

        [GS1CNPAttribute("ExpirarChave")]
        public object ExpirarChave(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user.id_tipousuario == 1)
            {
                string sql = @"UPDATE Chave SET DataExpiracao = CONVERT(DATETIME,CONVERT(VARCHAR(10),GETDATE(),101)), CodigoUsuarioExpiracao = @CodigoUsuarioExpiracao WHERE CodigoChave = @codigochave";
                parametro.campos.CodigoUsuarioExpiracao = user.id.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Sem permissão para acessar esta funcionalidade.");
            }

        }

        [GS1CNPAttribute("EnviarChave")]
        public void EnviarChave(dynamic parametro)
        {
            
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            parametro.campos.user = JsonConvert.SerializeObject(this.getSession("USUARIO_LOGADO"));

            string nomecontato = parametro.campos.nomecontato;
            string destinatario = parametro.campos.emailcontato;
            
            if (nomecontato == String.Empty || destinatario == String.Empty)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    string sql = @"select valor from parametro (Nolock) where chave = 'bo.nu.telefone'";

                    dynamic telefone = ((List<dynamic>)db.Select(sql)).FirstOrDefault();
                    throw new Exception("Não foi possível enviar a carta, pois não existe um contato responsável da empresa. Por favor, entre em contato com a GS1 Brasil: Tel. "+ telefone.valor.ToString() +" ou através do nosso chat no portal www.gs1br.org." );
                }
            }

            string empresa = parametro.campos.razaosocial;
            string assunto = "[CNP] - Geração de Chave";
            string url = ConfigurationManager.AppSettings["URLCaminhoImagemEmail"];
            string urlFacebook = ConfigurationManager.AppSettings["URLCaminhoFacebookEmail"];
            string urlLinkedin = ConfigurationManager.AppSettings["URLCaminhoLinkedinEmail"];
            string urlTwitter = ConfigurationManager.AppSettings["URLCaminhoTwitterEmail"];
            string urlYoutube = ConfigurationManager.AppSettings["URLCaminhoYoutubeEmail"];

            string mensagem = @"<div id='principal' style='text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; color:#000000; background-color:#FFFFFF; font-family: verdana, arial, helvetica; '>
		                            <table align='center' width='600' border='0' cellpadding='0' cellspacing='0' style='border-width: 0px; background-color: #ffffff;'>
			                            <tr valign='top'>
				                            <td><p style=' text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;'>
                                                <img width='600' height='134' alt='' hspace=1 vspace=1 src='" + url + @"/header_email.png'></p>
                                            </td>
			                            </tr>
			                            <tr>
				                            <td>
                                                <p style='padding:10px 10px 0px 10px;'> 
                                                    <span style='font-size: 11pt; font-family: verdana, arial, helvetica;'>
                                                        Caro " + nomecontato + @",<br/><br/>
                                                        Este é um e-mail automático. Por favor não responda.<br/><br/>
                                                        Segue, em anexo, a chave gerada pelo sistema CNP para a empresa " + empresa + @".<br/><br/>
                                                        Equipe GS1 Brasil
                                                    </span>
                                                    <div style='font-size: 8pt; text-align:left; font-family: verdana, arial, helvetica;'>
                                                        Acompanhe: <a href='" + urlFacebook + @"'><img src='" + url + @"/fb.png'></a> <a href='" + urlLinkedin + @"'><img src='" + url + @"/linkedin.png'></a> <a href='" + urlTwitter + @"'><img src='" + url + @"/twitter.png'></a> <a href='" + urlYoutube + @"'><img src='" + url + @"/youtube.png'></a> 
                                                    </div>
                                                </p>
                                            </td>
			                            </tr>
			                            <tr valign='top'>
				                            <td><p style='text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;'><img width='600' height='134' alt='' hspace=1 vspace=1 src='" + url + @"/footer_email.png'></p></td>
			                            </tr>
		                            </table>
	                            </div>";

            Dictionary<Stream, ContentType> anexo = new Dictionary<Stream, ContentType>();

            //Gera a carta
            byte[] bytes = GS1.CNP.BLL.Core.Util.GerarCarta(parametro);

            //Valida se a carta foi gerada
            if (bytes != null)
            {

                Stream s = new MemoryStream(bytes);
                ContentType ct = new ContentType(MediaTypeNames.Application.Pdf);
                ct.Name = string.Format("CartaFiliacao_{0}_{1}{2}", parametro.campos.nomelicenca.Value.ToString(), DateTime.Now.ToString("ddMMyyyy_HHmmss"), ".pdf");

                anexo.Add(s, ct);

                //Verifica se existe todos os itens necessários para o envio do email
                if (!String.IsNullOrEmpty(destinatario) && !String.IsNullOrEmpty(assunto) && !String.IsNullOrEmpty(mensagem) && anexo.Count > 0)
                {
                    GS1.CNP.BLL.Core.Util.EnviarEmail(destinatario, assunto, mensagem, true, anexo);
                }
                else
                {
                    throw new Exception("Não foi possível Enviar a Carta.");
                }
            }
            else
            {
                throw new Exception("Não foi possível enviar a carta pois a mesma não foi encontrada.");
            }
        }

        //Gera uma nova chave
        public string NovaChave()
        {
            string chave = Guid.NewGuid().ToString();

            string sql = @"SELECT COUNT(Chave) AS quantidade FROM CHAVE  (nolock)  WHERE Chave = '" + chave + "'";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic retorno = ((List<dynamic>)db.Select(sql)).FirstOrDefault();

                if (retorno.quantidade == 0)
                {
                    return chave;
                }
                else
                {
                    return NovaChave();
                }
            }
        }

    }
}
