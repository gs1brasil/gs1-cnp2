﻿using GS1.CNP.BLL.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GS1.CNP.DAL;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("ConfiguracaoGepirBO")]
    public class ConfiguracaoGepirBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "PARAMETRO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }


        [GS1CNPAttribute("PesquisarConfiguracoesGepir")]
        public string ConsultarEmpresas(dynamic parametro)
        {

            string sql = @"SELECT B.CODIGO, B.CAD, B.NOME, A.CODIGOPERFILGEPIR 
                            FROM [ASSOCIADOPERFILGEPIR] A (Nolock)
                            INNER JOIN [ASSOCIADO] B (Nolock) ON B.CODIGO = A.CODIGOASSOCIADO";


            if (parametro.where != null)
            {
                sql += " WHERE 1 = 1 ";

                if (parametro.where.codigoassociado != null)
                {
                    sql += " AND a.codigoassociado = @codigoassociado";
                }

                if (parametro.where.empresaassociada != null && !parametro.where.empresaassociada.Equals(string.Empty) && parametro.where.empresaassociada.Value != string.Empty)
                {                  
                    parametro.where.codigoassociado = parametro.where.empresaassociada.codigo;
                    sql += " AND a.codigoassociado = @codigoassociado";                  
                }
               
                if (parametro.where.perfilpadrao != null && parametro.where.perfilpadrao > 0)
                {
                    sql += " AND a.codigoperfilgepir = @perfilpadrao";
                }

            }

            string registroPorPagina = "10";
            if (parametro.registroPorPagina != null && parametro.registroPorPagina > 0)
                registroPorPagina = parametro.registroPorPagina.ToString();

            string numeropagina = "1";
            if (parametro.paginaAtual > 0)
                numeropagina = parametro.paginaAtual.ToString();

            string ordenacao = "Codigo ASC";
            if (parametro.ordenacao != null && parametro.ordenacao != string.Empty)
                ordenacao = parametro.ordenacao;


            sql = @";WITH DADOS AS ( " + sql;
            sql = sql + @")
                        SELECT * 
                        ,TOTALREGISTROS = COUNT(*) OVER()
                        FROM DADOS WITH (NOLOCK)
                        ORDER BY " + ordenacao + @"
                        OFFSET " + registroPorPagina + @" * (" + numeropagina + @"-1) ROWS FETCH NEXT " + registroPorPagina + @" ROWS ONLY";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }


        [GS1CNPAttribute("EditarConfiguracoesGepir")]
        public string SalvarEmpresa(dynamic parametro)
        {

            string sql = @" INSERT INTO [dbo].[AssociadoPerfilGepirHistorico] ([CodigoAssociado],[CodigoPerfilGepir],[DataAlteracao],[CodigoUsuarioAlteracao],[DataHistorico])
                            SELECT [CodigoAssociado],[CodigoPerfilGepir],[DataAlteracao],[CodigoUsuarioAlteracao], GETDATE() FROM AssociadoPerfilGepir (Nolock) WHERE CODIGOASSOCIADO = @codigoassociado
                            UPDATE [ASSOCIADOPERFILGEPIR] SET CODIGOPERFILGEPIR = @codigoperfilgepir WHERE CODIGOASSOCIADO = @codigoassociado";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        
    }
}
