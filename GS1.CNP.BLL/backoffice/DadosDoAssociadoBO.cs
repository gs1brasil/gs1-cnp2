﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;
using System.Dynamic;
using System.Data;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("DadosAssociadoBO")]
    public class DadosDoAssociadoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "ASSOCIADO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscaDadosDoAssociado(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                string registroPorPagina = "10";
                if (parametro.registroPorPagina > 0)
                    registroPorPagina = parametro.registroPorPagina.ToString();

                string numeropagina = "1";
                if (parametro.paginaAtual > 0)
                    numeropagina = parametro.paginaAtual.ToString();

                string ordenacao = "Nome ASC";
                if (parametro.ordenacao != string.Empty)
                    ordenacao = parametro.ordenacao;


                string sql = @";WITH DADOS AS (SELECT DISTINCT a.CODIGO,
						                                A.NOME,
						                                A.CPFCNPJ,
                                                        A.CAD,
                                                        A.EMAIL,
						                                SA.NOME AS STATUSASSOCIADO,
						                                CASE WHEN A.CODIGOSITUACAOFINANCEIRA = 1 THEN
							                                'Sim'
						                                ELSE
							                                'Não' END AS SITUACAOFINANCEIRA,
						                                TC.CODIGO AS TIPOCOMPARTILHAMENTOCODIGO, 
						                                TC.NOME AS TIPOCOMPARTILHAMENTONOME,
						                                A.ENDERECO,
						                                A.NUMERO,
						                                A.COMPLEMENTO,
						                                A.BAIRRO,
						                                A.CIDADE,
						                                A.UF,
                                                        A.PAIS,
                                                        --A.ATIVADO,
                                                        --A.APROVACAO

						                                CONVERT(VARCHAR(10), CONVERT(DATE, A.DATAATUALIZACAOCRM, 106),103) + ' '  + convert(VARCHAR(8), A.DATAATUALIZACAOCRM, 14) AS DATAATUALIZACAOCRM,
														CONVERT(VARCHAR(10), CONVERT(DATE, A.DATAMIGRACAO, 106), 103) + ' '  + convert(VARCHAR(8), A.DATAMIGRACAO, 14) AS DATAMIGRACAO,
                                                        (SELECT COUNT(*) FROM PRODUTO X (NOlock) WHERE X.CODIGOASSOCIADO = A.CODIGO AND X.CODIGOSTATUSGTIN <> 7) as QUANTIDADEPRODUTOS

					                                FROM ASSOCIADO AS A WITH (NOLOCK)
					                                LEFT JOIN ASSOCIADOUSUARIO AS AU WITH (NOLOCK) ON AU.CODIGOASSOCIADO = A.CODIGO 
					                                INNER JOIN STATUSASSOCIADO AS SA WITH (NOLOCK) ON SA.CODIGO = A.CODIGOSTATUSASSOCIADO
					                                INNER JOIN TIPOCOMPARTILHAMENTO AS TC WITH (NOLOCK) ON TC.CODIGO = A.CODIGOTIPOCOMPARTILHAMENTO";



                if (user.id_tipousuario != 1)
                {
                    sql += " WHERE au.CodigoUsuario = @codigoUsuario ";
                }

                sql += @")
                                SELECT * 
                                ,TOTALREGISTROS = COUNT(*) OVER()
                                FROM DADOS WITH (NOLOCK)
                                 ORDER BY " + ordenacao + @"
                                OFFSET " + registroPorPagina + @" * (" + numeropagina + @"-1) ROWS FETCH NEXT " + registroPorPagina + @" ROWS ONLY";

                using (Database db = new Database("BD"))
                {
                    parametro.campos.codigoUsuario = user.id.ToString();

                    db.Open();
                    dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscaRepresentantesLegais(dynamic parametro)
        {
            //if (ValidarAssociado(Convert.ToInt32(parametro.campos.codigoAssociado)))
            if (parametro.campos.codigoAssociado != null)
            {
                string sql = @"SELECT Codigo,
	                        Nome,
	                        Email,
	                        CodigoAssociado
                        FROM ContatoAssociado WITH (NOLOCK)
                        WHERE CodigoAssociado = @codigoAssociado
                            and status = 1";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscaLicencas(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (parametro != null && parametro.campos != null && parametro.campos.codigoAssociado != null)
            {
                string sql = @"SELECT DISTINCT LA.CODIGOLICENCA,
	                            L.NOME,
	                            SP.NOME AS STATUS,
	                            PLA.NUMEROPREFIXO,
	                            LA.DATAANIVERSARIO,
	                            LA.CODIGOASSOCIADO
				                    FROM ASSOCIADO A (Nolock) 
				                    INNER JOIN PREFIXOLICENCAASSOCIADO PLA (Nolock)  ON (PLA.CODIGOASSOCIADO = A.CODIGO)
				                    INNER JOIN LICENCAASSOCIADO LA (Nolock)  ON (LA.CODIGOASSOCIADO = A.CODIGO AND LA.CODIGOLICENCA = PLA.CODIGOLICENCA)
				                    INNER JOIN LICENCA L (Nolock)  ON (L.CODIGO = LA.CODIGOLICENCA)
                                    INNER JOIN STATUSPREFIXO SP (Nolock)  ON SP.CODIGO = PLA.CODIGOSTATUSPREFIXO
                                WHERE la.CodigoAssociado = @codigoAssociado ";

                                //Usuários Associados podem ver apenas Licenças com Prefixo Ativo ou Reativado
                                if (user.id_tipousuario != 1)
                                {
                                    sql += " AND SP.CODIGO IN (1,8) ";
                                }

                                sql += " ORDER BY L.NOME, PLA.NUMEROPREFIXO";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }

            
        }

        [GS1CNPAttribute(false)]
        public string BuscaUsuarios(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (parametro != null && parametro.campos != null && parametro.campos.codigoAssociado != null)
            {
                string sql = @"SELECT A.NOME, A.EMAIL, C.NOME AS STATUS, CASE B.INDICADORPRINCIPAL WHEN 1 THEN 'Sim' ELSE 'Não' END AS INDICADORPRINCIPAL
                                FROM USUARIO A (Nolock) 
                                INNER JOIN ASSOCIADOUSUARIO B  (Nolock) ON B.CODIGOUSUARIO = A .CODIGO
                                INNER JOIN STATUSUSUARIO C (Nolock)  ON C.CODIGO = A.CODIGOSTATUSUSUARIO
                                WHERE B.CODIGOASSOCIADO = @codigoAssociado ";                

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }
        
        [GS1CNPAttribute(false)]
        public object BuscarTipoCompartilhamento(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.All("TIPOCOMPARTILHAMENTO");
                return JsonConvert.SerializeObject(retorno);
            }
        }

        //TODO - colocar permisssão correta
        [GS1CNPAttribute(true, true, "EditarDadosAssociado", "VisualizarDadosAssociado")]
        public object AtualizarTipoCompartilhamento(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    string sql = @"UPDATE ASSOCIADO SET 
                                    CodigoTipoCompartilhamento = @tipoCompartilhamento,
                                    CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao 
                                 WHERE Codigo = @codigo";

                                    //Ativado = @ativado,
                                    //Aprovacao = @aprovacao,

                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object result = db.Select(sql, param);

                    return result;
                }
            }
            else
            {
                return "Não foi possível realizar a atualização.";
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisaDadosDoAssociado(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null)
            {

                string sql = @";WITH DADOS AS (SELECT DISTINCT A.CODIGO,
	                            A.NOME,
	                            A.CPFCNPJ,
                                A.CAD,
                                A.EMAIL,
	                            SA.NOME AS STATUSASSOCIADO,
	                            CASE WHEN A.CODIGOSITUACAOFINANCEIRA = 1 THEN
									'Sim'
								ELSE
									'Não' END AS SITUACAOFINANCEIRA,
								TC.CODIGO AS TIPOCOMPARTILHAMENTOCODIGO, 
	                            TC.NOME AS TIPOCOMPARTILHAMENTONOME,
	                            A.ENDERECO,
	                            A.NUMERO,
	                            A.COMPLEMENTO,
	                            A.BAIRRO,
	                            A.CIDADE,
	                            A.UF,
	                            CONVERT(VARCHAR(10), CONVERT(DATE, A.DATAATUALIZACAOCRM, 106),103) + ' '  + convert(VARCHAR(8), A.DATAATUALIZACAOCRM, 14) AS DATAATUALIZACAOCRM,
								CONVERT(VARCHAR(10), CONVERT(DATE, A.DATAMIGRACAO, 106), 103) + ' '  + convert(VARCHAR(8), A.DATAMIGRACAO, 14) AS DATAMIGRACAO,
                                (SELECT COUNT(*) FROM PRODUTO X  (Nolock)  WHERE X.CODIGOASSOCIADO = A.CODIGO AND X.CODIGOSTATUSGTIN <> 7) as QUANTIDADEPRODUTOS	

                            FROM ASSOCIADO AS A WITH (NOLOCK)
                            LEFT JOIN ASSOCIADOUSUARIO AS AU WITH (NOLOCK) ON AU.CODIGOASSOCIADO = A.CODIGO 
                            INNER JOIN STATUSASSOCIADO AS SA WITH (NOLOCK) ON SA.CODIGO = A.CODIGOSTATUSASSOCIADO
							INNER JOIN TIPOCOMPARTILHAMENTO AS TC WITH (NOLOCK) ON TC.CODIGO = A.CODIGOTIPOCOMPARTILHAMENTO
                            WHERE 1 = 1";

                            if(user.id_tipousuario != 1) {
                                sql += " AND AU.CODIGOUSUARIO = @codigoUsuario";
                            }

                if (parametro != null && parametro.campos != null)
                {
                    if (parametro.campos.nome != null)
                        sql += " AND A.NOME LIKE '%' + @nome + '%'";
                    if (parametro.campos.cpfcnpj != null)
                        sql += " AND A.CPFCNPJ LIKE '%' + @cpfcnpj + '%'";
                    if (parametro.campos.cad != null)
                        sql += " AND A.CAD LIKE '%' + @cad + '%'";
                    if (parametro.campos.email != null)
                        sql += " AND A.EMAIL LIKE '%' + @email + '%'";
                    if (parametro.campos.statusassociado != null)
                        sql += " AND SA.CODIGO = @statusassociado";
                    if (parametro.campos.situacaofinanceira != null)
                        sql += " AND A.CODIGOSITUACAOFINANCEIRA = @situacaofinanceira";
                    if (parametro.campos.tipocompartilhamentocodigo != null)
                        sql += " AND TC.CODIGO = @tipocompartilhamentocodigo";
                    if (parametro.campos.endereco != null)
                        sql += " AND A.ENDERECO LIKE '%' + @endereco + '%'";
                    if (parametro.campos.numero != null)
                        sql += " AND A.NUMERO LIKE '%' + @numero + '%'";
                    if (parametro.campos.bairro != null)
                        sql += " AND A.BAIRRO LIKE '%' + @bairro + '%'";
                    if (parametro.campos.complemento != null)
                        sql += " AND A.COMPLEMENTO LIKE '%' + @complemento + '%'";
                    if (parametro.campos.uf != null)
                        sql += " AND A.UF LIKE '%' + @uf + '%'";
                    if (parametro.campos.cidade != null)
                        sql += " AND A.CIDADE LIKE '%' + @cidade + '%'";
                }


                string registroPorPagina = "10";
                if (parametro.registroPorPagina > 0)
                    registroPorPagina = parametro.registroPorPagina.ToString();

                string numeropagina = "1";
                if (parametro.paginaAtual > 0)
                    numeropagina = parametro.paginaAtual.ToString();

                string ordenacao = "Nome ASC";
                if (parametro.ordenacao != string.Empty)
                    ordenacao = parametro.ordenacao;


                sql += @")
                                SELECT * 
                                ,TOTALREGISTROS = COUNT(*) OVER()
                                FROM DADOS WITH (NOLOCK)
                                 ORDER BY " + ordenacao + @"
                                OFFSET " + registroPorPagina + @" * (" + numeropagina + @"-1) ROWS FETCH NEXT " + registroPorPagina + @" ROWS ONLY";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    parametro.campos.codigoUsuario = user.id.ToString();

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        public bool ValidarAssociado(Int32 idAssociado)
        {
            try
            {
                List<dynamic> associados = this.getSession("ASSOCIADOS_USUARIO_LOGADO") as List<dynamic>;
                //dynamic param = ((JToken)parametro.where).ToExpando();
                bool associadolocalizado = false;

                for (Int32 index = 0; index <= associados.Count - 1; index++)
                {
                    if (Convert.ToInt32(associados[index].codigo) == idAssociado)
                    {
                        //this.setSession("ASSOCIADO_SELECIONADO", associados[index]);
                        return true;
                    }
                }
                return associadolocalizado;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao Selecionar Associado do Usuário.");
            }
        }

        public Associado BuscarAssociadoPorCAD(string cad)
        {
            if (!string.IsNullOrEmpty(cad))
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = new ExpandoObject();
                    param.cad = cad;

                    Func<IDataRecord, Associado> binder = r => new Associado()
                    {
                        codigo = r.GetInt64(r.GetOrdinal("codigo")),
                        nome = r.GetString(r.GetOrdinal("nome")),
                        cpfcnpj = r.GetString(r.GetOrdinal("cpfcnpj")),
                        cad = r.GetString(r.GetOrdinal("cad")),
                        indicadorcnaerestritivo = (r.IsDBNull(r.GetOrdinal("indicadorcnaerestritivo")) ? string.Empty : r.GetString(r.GetOrdinal("indicadorcnaerestritivo"))),
                        ativado = r.GetBoolean(r.GetOrdinal("ativado")),
                        aprovacao = r.GetBoolean(r.GetOrdinal("aprovacao"))
                    };

                    Associado associado = db.Find<Associado>("ASSOCIADO", "CAD = @cad", param, "*", binder);

                    return associado;
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        public Associado BuscarAssociadoPorGLN(string gln)
        {
            if (!string.IsNullOrEmpty(gln))
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = new ExpandoObject();
                    param.gln = gln;

                    Func<IDataRecord, Associado> binder = r => new Associado()
                    {
                        codigo = r.GetInt64(r.GetOrdinal("codigo")),
                        nome = r.GetString(r.GetOrdinal("nome")),
                        cpfcnpj = r.GetString(r.GetOrdinal("cpfcnpj")),
                        cad = r.GetString(r.GetOrdinal("cad")),
                        indicadorcnaerestritivo = (r.IsDBNull(r.GetOrdinal("indicadorcnaerestritivo")) ? string.Empty : r.GetString(r.GetOrdinal("indicadorcnaerestritivo"))),
                        ativado = r.GetBoolean(r.GetOrdinal("ativado")),
                        aprovacao = r.GetBoolean(r.GetOrdinal("aprovacao"))
                    };

                    Associado associado = db.Find<Associado>("ASSOCIADO A INNER JOIN LOCALIZACAOFISICA LF ON LF.CODIGOASSOCIADO = A.CODIGO", "LF.GLN = @gln", param, "*", binder);

                    return associado;
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }
    }
}
