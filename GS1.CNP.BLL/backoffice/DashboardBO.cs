﻿using System;
using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using System.Dynamic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("DashboardBO")]
    public class DashboardBO : CrudBO, IDisposable
    {
        public override string nomeTabela
        {
            get { return string.Empty; }
        }

        public override string nomePK
        {
            get { return string.Empty; }
        }


        [GS1CNPAttribute(false)]
        public object ConsultaBlocoProdutos(dynamic parametro)
        {

            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            using (Database db = new Database("BD"))
            {
                db.Open();

                string cadastradosAssociado = string.Empty,
                    publicadosAssociado = string.Empty,
                    desativadosAssociado = string.Empty;

                dynamic param = new ExpandoObject();
                /* old - flavio
                if (associado != null)
                {
                    cadastradosAssociado = " AND CodigoAssociado = @associado";
                    publicadosAssociado = " AND CodigoAssociado = @associado";
                    desativadosAssociado = " AND CodigoAssociado = @associado";
                    param.associado = associado.codigo;
                }
                else
                {
                    param = null;
                }*/

                param = null;

                string codigoAssocido = "";
                if (associado != null)
                    codigoAssocido = associado.codigo.ToString();
                else
                    return "0"; // 20170712

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                //TODO
                sb.Append(string.Format(@"Select * from DashboardHomePage (Nolock) where CodigoAssociado = '" + codigoAssocido + "'", cadastradosAssociado, publicadosAssociado, desativadosAssociado));
                /*
                if (user != null)
                {
                    if (user.id_tipousuario == 1)
                    {
                        sb.Append(", (SELECT COUNT(*) FROM ASSOCIADO A  (nolock)  WHERE CODIGOSTATUSASSOCIADO = 1) AS EMPRESASATIVAS");
                    }
                }
                */
                object resultado = db.Select(sb.ToString(), param);
                return JsonConvert.SerializeObject(resultado);
            }
        }



        [GS1CNPAttribute(false)]
        public object ConsultaProdutosEmPreenchimento(dynamic parametro)
        {
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            using (Database db = new Database("BD"))
            {
                db.Open();
                // string condition = "CodigoStatusGTIN IN (5)";
                string condition = "  1=1  ";
                dynamic param = new ExpandoObject();
                if (associado != null)
                {

                    param.associado = associado.codigo;

                    condition += " AND CodigoAssociado = @associado";


                    // return db.Count("SELECT Cont FROM {0} (Nolock) WHERE {1}", "PRODUTO", condition, param);
                    return db.Count("SELECT Cont FROM {0} (Nolock) WHERE {1}", "DashboardProdutos", condition, param);



                }
                else
                {
                    param.associado = DBNull.Value;
                    return "0";
                    //  return db.Count("SELECT sum(Cont)  FROM {0} (Nolock) WHERE {1}", "PRODUTO", condition);
                    return db.Count("SELECT sum(Cont)  FROM {0} (Nolock) WHERE {1}", "DashboardProdutos", condition);

                }
            }
        }

        [GS1CNPAttribute(false)]
        public object ConsultaTotalEtiquetas(dynamic parametro)
        {
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = new ExpandoObject();
                /* Flávio
                if (associado != null)
                    param.associado = associado.codigo;
                else
                    param.associado = DBNull.Value;
                */

                param.associado = DBNull.Value;
                string CodigoAssociado = string.Empty;
                if (associado != null)
                    CodigoAssociado = associado.codigo.ToString();
                else
                    return "0";
                string sql = @"Select * from DashboardEtiquetas (nolock) where CodigoAssociado = '" + CodigoAssociado + "'";

                return db.Select(sql, param);
            }
        }

        //[GS1CNPAttribute(false)]
        //public object ConsultaTotalEmpresasAtivas(dynamic parametro)
        //{
        //    Login user = (Login)this.getSession("USUARIO_LOGADO");

        //    if (user != null)
        //    {
        //        if (user.id_tipousuario == 1)
        //        {
        //            using (Database db = new Database("BD"))
        //            {
        //                db.Open();                       
        //                return db.Count("ASSOCIADO", "CodigoStatusAssociado = 1", null);                       
        //            }

        //        }
        //    }
        //    return null;
        //}

        [GS1CNPAttribute(false)]
        public object BannersEspacoBanner(dynamic parametro)
        {

            string sql = @"SELECT A.CODIGO AS CODIGOESPACOBANNER, 
			                      A.ALTURA, 
								  A.LARGURA, 
								  C.CODIGO AS CODIGOBANNER, 
								  C.CORFUNDO, 
								  C.NOMEIMAGEM, 
								  B.ORDEM, 
								  C.LINK, 
								  C.NOME AS NOMEBANNER 
                             FROM ESPACOBANNER A (NOLOCK)
                       INNER JOIN BANNERESPACOBANNER B (NOLOCK) 
					           ON B.CODIGOESPACOBANNER = A.CODIGO
                       INNER JOIN BANNER C (NOLOCK) 
					           ON C.CODIGO = B.CODIGOBANNER
                            WHERE A.CODIGO = " + parametro.where.codigo + @"
                              AND A.STATUS = 1
                              AND C.STATUS = 1
                              AND (CONVERT(DATETIME,CONVERT(VARCHAR(10),GETDATE(),101))) BETWEEN (CONVERT(DATETIME,CONVERT(VARCHAR(10),C.DATAINICIOPUBLICACAO,101))) 
														                              AND (CONVERT(DATETIME,CONVERT(VARCHAR(10),C.DATAFIMPUBLICACAO,101)))
																					  ORDER BY (CASE A.CODIGOTIPOESPACOBANNER WHEN 2 THEN NEWID() ELSE NULL END)
                                                                                      , ORDEM";


            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> banners = db.Select(sql, param);
                return JsonConvert.SerializeObject(banners);
            }
        }


        [GS1CNPAttribute(false)]
        public object ConsultarMensagens(dynamic parametro)
        {
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = new ExpandoObject();
                if (associado != null)
                    param.associado = associado.codigo;
                else
                    param.associado = DBNull.Value;

                param.usuario = user.id.ToString();

                string registroPorPagina = "4";
                if (parametro.registroPorPagina != null && parametro.registroPorPagina > 0)
                    registroPorPagina = parametro.registroPorPagina.ToString();

                string numeropagina = "1";
                if (parametro.paginaAtual > 0)
                    numeropagina = parametro.paginaAtual.ToString();

                string ordenacao = "DATASINCRONIZACAO DESC";
                if (parametro.ordenacao != string.Empty && parametro.ordenacao != null)
                    ordenacao = parametro.ordenacao;


                string sql = @"SELECT A.CODIGO
                                ,CONVERT(VARCHAR(10),CONVERT(DATETIME, A.DATASINCRONIZACAO,106),103) + ' '  + CONVERT(VARCHAR(8), CONVERT(DATETIME, A.DATASINCRONIZACAO,113), 14) AS DATASINCRONIZACAO 
                                , A.MENSAGEM, B.DATALEITURA, A.ASSUNTO  
                                FROM MENSAGEM A (Nolock)
                                LEFT JOIN MENSAGEMLEITURA B (Nolock) ON B.CODIGOMENSAGEM = A.CODIGO AND B.CODIGOUSUARIO = @usuario
                                WHERE STATUS = 1 
                                AND (CODIGOASSOCIADO = @associado or CODIGOASSOCIADO IS NULL)";


                sql = @";WITH DADOS AS ( " + sql;
                sql = sql + @")
                        SELECT * 
                        ,TOTALREGISTROS = COUNT(*) OVER()
                        , (SELECT COUNT(*) 
							FROM MENSAGEM A (Nolock)
							LEFT JOIN MENSAGEMLEITURA B (Nolock) ON B.CODIGOMENSAGEM = A.CODIGO AND B.CODIGOUSUARIO = @usuario
							WHERE STATUS = 1 
                            AND (CODIGOASSOCIADO = @associado or CODIGOASSOCIADO IS NULL)
							AND B.CODIGO IS NULL) AS TOTALNAOLIDAS

                        FROM DADOS WITH (NOLOCK)
                        ORDER BY " + ordenacao + @"
                        OFFSET " + registroPorPagina + @" * (" + numeropagina + @"-1) ROWS FETCH NEXT " + registroPorPagina + @" ROWS ONLY";


                List<dynamic> mensagens = db.Select(sql, param);
                return mensagens;
            }

            return null;
        }



        [GS1CNPAttribute(false)]
        public object ConsultarNoticias(dynamic parametro)
        {
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            using (Database db = new Database("BDBASEUNICACRM"))
            {
                db.Open();

                string sql = @"SELECT TOP 10 'https://www.gs1br.org/educacao-e-pratica/eventos/' + codename as URL ,
            			                                NAME as NOME,   
            			                                CONVERT(VARCHAR(10),PROPOSEDSTART,103) AS DATAINICIO                   
                                            FROM CAMPAIGN (NOLOCK)
                                            WHERE SMART_PUBLICAR = 1
                                                AND NEW_ESTEEVENTOSEREXCLUSIVO = 0
                                            AND PROPOSEDSTART > GETDATE()
                                            ORDER BY PROPOSEDSTART
                                            ";

                return db.Select(sql, null);
            }
            return null;
        }


        [GS1CNPAttribute(false)]
        public object ConsultarBoletos(dynamic parametro)
        {
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            if (associado != null)
            {
                using (Database db = new Database("BDBASEUNICACRM"))
                {
                    db.Open();

                    string agenciaContaNossoNumero = ConfigurationManager.AppSettings["AgenciaContaNossoNumero"];

                    string sql = @"SELECT TOP 10
	                                    B.ACCOUNTNUMBER,
	                                    CONVERT(VARCHAR(10), CONVERT(DATE, SMART_E1_VENCTO, 106), 103) AS DATAVENCIMENTO,
	                                    SMART_E1_VALOR AS VALORTITULO,
	                                    SMART_E1_GS1NEGNAME AS TIPOTITULONEGOCIO,
	                                    SMART_E1_NUM AS NUMEROTITULO,
	                                    '" + agenciaContaNossoNumero + @"' + ISNULL(SMART_NUMTITULOBCO, '') AS NUMBCO
                                    FROM SMART_TITULO A WITH(NOLOCK)
                                    INNER JOIN ACCOUNT B (NOLOCK) ON SMART_EMPRESA = ACCOUNTID
                                    WHERE SMART_E1_STATUS = 'A'
                                    AND SMART_E1_VENCTO < GETDATE()
                                    AND SMART_E1_PREFIXO IN ('REG', 'MAN')
                                    AND SMART_NUMTITULOBCO LIKE '112%'
                                    AND B.ACCOUNTNUMBER = '" + associado.cad.ToString() + @"'
                                    ORDER BY SMART_E1_VENCORI ASC";

                    return db.Select(sql, null);
                }
            }
            else
            {
                return null;
            }
        }

        public void Dispose() { }
    }
}
