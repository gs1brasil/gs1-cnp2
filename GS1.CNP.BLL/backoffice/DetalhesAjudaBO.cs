﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("DetalhesAjudaBO")]
    public class DetalhesAjudaBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "AJUDA"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarDados(dynamic parametro)
        {

            string sql = @"SELECT FA.CODIGO, FA.CODIGOFORMULARIO, FA.CODIGOTIPOAJUDA, FA.NOME, FA.AJUDA, FA.URLVIDEO, FA.CODIGOSTATUSPUBLICACAO
                            FROM FORMULARIOAJUDA FA  (Nolock) 
                            WHERE CODIGO = @codigo
                            AND FA.CODIGOSTATUSPUBLICACAO = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarCodigosAjuda(dynamic parametro)
        {

            string sql = @"SELECT CODIGO
                            FROM FORMULARIOAJUDA  (Nolock) 
                            WHERE CODIGOSTATUSPUBLICACAO = 1
                            AND AJUDA IS NOT NULL";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }
        
    }

}
