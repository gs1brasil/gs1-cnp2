﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GS1.CNP.BLL.Core;
using System.Data;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("DetalhesAjudaPassoaPassoBO")]
    public class DetalhesAjudaPassoaPassoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "AJUDA"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarAjudasPassoAPasso(dynamic parametro)
        {

            string sql = @"SELECT FA.CODIGO, FA.NOME, FA.URLVIDEO AS VIDEO, FA.AJUDA
	                            ,TOTALREGISTROS = COUNT(*) OVER()
                                FROM FORMULARIOAJUDAPASSOAPASSO FA WITH (NOLOCK)
                                WHERE FA.CODIGOSTATUSPUBLICACAO = 1
	                            AND FA.AJUDA IS NOT NULL
                                AND FA.CODIGOIDIOMA = @codigoidioma 
	                            AND FA.CODIGO = @codigo
                                ORDER BY ORDEM ASC, DATAALTERACAO DESC ";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro != null && parametro.campos != null)
                {
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                }

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }
        
    }

}
