﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.IO;
using System.Web;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("CrudBO")]
    public class EPCRFIDBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "EPCRFID"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public object BuscarEPCRFIDs(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT E.CODIGO, E.DATAALTERACAO, P.PRODUCTDESCRIPTION, P.GLOBALTRADEITEMNUMBER, E.CODIGOPRODUTO, T.CODIGO AS CODIGOTIPOHASH, T.NOME AS NOMEHASH, T.QUANTIDADEMAXIMACARACTERES, 
                                    E.SERIALINICIO, E.SERIALFIM, U.NOME AS USUARIO, P.CODITEM, P.NRPREFIXO, TF.CODIGO AS CODIGOTIPOFILTRO ,TF.NOME AS NOMEFILTRO
                                    FROM EPCRFID E  (Nolock) 
                                    LEFT JOIN PRODUTO P  (Nolock)   ON (P.CODIGOPRODUTO = E.CODIGOPRODUTO) AND (P.GLOBALTRADEITEMNUMBER IS NOT NULL)
                                    INNER JOIN TIPOHASH T  (Nolock) ON (T.CODIGO = E.CODIGOTIPOHASH)
                                    INNER JOIN TIPOFILTRO TF (Nolock)  ON (TF.CODIGO = E.CODIGOTIPOFILTRO) 
                                    INNER JOIN USUARIO U (Nolock)  ON (U.CODIGO = E.CODIGOUSUARIOALTERACAO)
                                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        //Apenas gerar o EPC/RFID, não realiza o download
        [GS1CNPAttribute("CadastrarEPCRFID",true,true)]
        public byte[] GerarEPCRFIDs(dynamic parametro)
        {
            byte[] bytes = null;
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (parametro != null)
            {
                try
                {
                    if (user != null)
                    {
                        if (parametro.campos != null)
                        {
                            string extension = ".txt";
                            string fullName = HttpContext.Current.Server.MapPath(string.Format("EPCRFID_{0}_{1}{2}", parametro.campos.itemGerado, parametro.campos.codigoproduto.Value.ToString(), extension));
                            FileStream Stream = new FileStream(fullName, FileMode.Create);
                            var mensagem = "";
                            int filtro = parametro.campos.codigotipofiltro, 
                                particao = 12 - parametro.campos.nrprefixo.ToString().Length;
                            string header = String.Empty,
                                   varianteLogistica = String.Empty;

                            if (parametro.campos.globaltradeitemnumber != null && parametro.campos.globaltradeitemnumber.ToString() != "")
                            {
                                parametro.campos.globaltradeitemnumber = FormataGTIN(parametro.campos.globaltradeitemnumber.ToString());
                                varianteLogistica = parametro.campos.globaltradeitemnumber.Value.ToString().Substring(0, 1);
                            }

                            //96 BITS
                            if ((parametro.campos.serialfim != null && parametro.campos.serialfim.ToString() != ""))
                            {

                                Int64 i = Convert.ToInt64(parametro.campos.serialinicio.Value);

                                do
                                {
                                    if (parametro.campos.itemGerado == "formato1")
                                    {
                                        header = "urn:epc:id:sgtin:";
                                        mensagem += header;
                                        mensagem += Convert.ToString(parametro.campos.nrprefixo.Value) + ".";
                                        mensagem += varianteLogistica + parametro.campos.coditem.Value + ".";
                                        //mensagem += String.Format("{0:D" + ((int)parametro.campos.quantidademaximacaracteres.Value) + "}", i) + "\r\n";
                                        mensagem += i + "\r\n";
                                    }

                                    if (parametro.campos.itemGerado == "formato2")
                                    {
                                        if (parametro.campos.codigotipohash == 1)
                                            mensagem += "urn:epc:tag:sgtin-96:" + filtro + ".";
                                        else if (parametro.campos.codigotipohash == 2)
                                            mensagem += "urn:epc:tag:sgtin-198:" + filtro + ".";

                                        mensagem += Convert.ToString(parametro.campos.nrprefixo.Value) + ".";
                                        mensagem += varianteLogistica + parametro.campos.coditem.Value + ".";
                                        //mensagem += String.Format("{0:D" + ((int)parametro.campos.quantidademaximacaracteres.Value) + "}", i) + "\r\n";
                                        mensagem += i + "\r\n";
                                    }

                                    if (parametro.campos.itemGerado == "hexadecimal")
                                    {
                                        string binary = ConverterDecimalParaBinario(parametro, filtro, particao, varianteLogistica, i.ToString(), "inteiro");
                                        mensagem += BinaryStringToHexString(binary) + "\r\n";
                                    }

                                    if (parametro.campos.itemGerado == "binario")
                                    {
                                        mensagem += ConverterDecimalParaBinario(parametro, filtro, particao, varianteLogistica, i.ToString(), "inteiro") + "\r\n";
                                    }

                                    i++;
                                }
                                while ((parametro.campos.serialfim != null && parametro.campos.serialfim.ToString() != "") && i <= (Int64)parametro.campos.serialfim);
                            }
                            //198 BITS
                            else
                            {
                                string i = parametro.campos.serialinicio.Value;

                                if (parametro.campos.itemGerado == "formato1")
                                {
                                    header = "urn:epc:id:sgtin:";
                                    mensagem += header;
                                    mensagem += Convert.ToString(parametro.campos.nrprefixo.Value) + ".";
                                    mensagem += varianteLogistica + parametro.campos.coditem.Value + ".";
                                    //mensagem += String.Format("{0:D" + ((int)parametro.campos.quantidademaximacaracteres.Value) + "}", i) + "\r\n";
                                    mensagem += i + "\r\n";
                                }

                                if (parametro.campos.itemGerado == "formato2")
                                {
                                    if (parametro.campos.codigotipohash == 1)
                                        mensagem += "urn:epc:tag:sgtin-96:" + filtro + ".";
                                    else if (parametro.campos.codigotipohash == 2)
                                        mensagem += "urn:epc:tag:sgtin-198:" + filtro + ".";

                                    mensagem += Convert.ToString(parametro.campos.nrprefixo.Value) + ".";
                                    mensagem += varianteLogistica + parametro.campos.coditem.Value + ".";
                                    //mensagem += String.Format("{0:D" + ((int)parametro.campos.quantidademaximacaracteres.Value) + "}", i) + "\r\n";
                                    mensagem += i + "\r\n";
                                }

                                if (parametro.campos.itemGerado == "hexadecimal")
                                {
                                    string binary = ConverterDecimalParaBinario(parametro, filtro, particao, varianteLogistica, i, "string");
                                    mensagem += BinaryStringToHexString(binary) + "\r\n";
                                }

                                if (parametro.campos.itemGerado == "binario")
                                {
                                    mensagem += ConverterDecimalParaBinario(parametro, filtro, particao, varianteLogistica, i, "string") + "\r\n";
                                }

                            }

                            byte[] bf = Encoding.UTF8.GetBytes(mensagem);

                            //Escreve arquivo no fluxo
                            Stream.Write(bf, 0, bf.Count());
                            Stream.Close();

                            return bf;    
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Não foi possível gerar a o EPCRFID. "+e.Message);
                }

            }
            return null;

        }

        public string ConverterDecimalParaBinario(dynamic parametro, int filtro, int particao, string varianteLogistica, string serial, string tipo)
        {
            string header = String.Empty;
            string filter_binary;

            try
            {
                if (parametro.campos.codigotipohash == 1)
                    header = "00110000";
                else if (parametro.campos.codigotipohash == 2)
                    header = "00110110";

                filter_binary = Convert.ToString(filtro, 2);
                //string value = String.Format("{0:D" + ((int)parametro.campos.quantidademaximacaracteres.Value) + "}", serial);
                string value = String.Format(serial);

                int quantidadeCaracteres = 0;

                string binary = header;
                binary += String.Format("{0:D3}", Convert.ToInt64(filter_binary));
                binary += String.Format("{0:D3}", Convert.ToInt64(Convert.ToString(particao, 2)));
                binary += Convert.ToString((Int64)parametro.campos.nrprefixo, 2);

                //O tamanho da Partição + NrPrefixo + CodItem tem que ser igual a 47
                quantidadeCaracteres += String.Format("{0:D3}", Convert.ToInt64(Convert.ToString(particao, 2))).Length;
                quantidadeCaracteres += Convert.ToString((Int64)parametro.campos.nrprefixo, 2).Length;

                binary += String.Format("{0:D" + (47 - quantidadeCaracteres) + "}", Convert.ToInt64(StringToBinary((varianteLogistica + parametro.campos.coditem))));
                //binary += String.Format("{0:D" + (47 - quantidadeCaracteres) + "}", StringToBinary((varianteLogistica + parametro.campos.coditem)));

                if (tipo == "inteiro")
                {
                    string temporario = String.Empty;
                    temporario = StringToBinary(serial);
                    binary += temporario.PadLeft(38, '0');
                }
                else if (tipo == "string")
                {
                    string temporario = String.Empty;
                    temporario = StringToBinaryASC(serial.ToString());
                    binary += temporario.PadLeft(140, '0');
                }

                return binary;
            }
            catch (Exception e)
            {
                throw new GS1TradeException("Dados incorretos.");
            }
        }

        public static string BinaryStringToHexString(string binary)
        {
            StringBuilder result = new StringBuilder(binary.Length / 8 + 1);

            // TODO: check all 1's or 0's... Will throw otherwise

            int mod4Len = binary.Length % 8;
            if (mod4Len != 0)
            {
                // pad to length multiple of 8
                binary = binary.PadLeft(((binary.Length / 8) + 1) * 8, '0');
            }

            for (int i = 0; i < binary.Length; i += 8)
            {
                string eightBits = binary.Substring(i, 8);
                result.AppendFormat("{0:X2}", Convert.ToByte(eightBits, 2));
            }

            return result.ToString();
        }

        public static string StringToBinary(string data)
        {
            string result = string.Empty;
            //foreach (char ch in data)
            //{
            //    result += Convert.ToString(Convert.ToInt32(ch.ToString()), 2).PadLeft(4, '0');
            //}

            result = Convert.ToString(Convert.ToInt32(data.ToString()), 2).PadLeft(4 * data.Length, '0');


            return result;
        }

        public static string StringToBinaryASC(string data)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in data.ToCharArray())
            {
                sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));
            }
            return sb.ToString();
        }

        public string FormataGTIN(string gtin)
        {
            string novoGtin = String.Empty;

            if (gtin.Length < 14)
            {
                novoGtin = "0" + gtin;
            }
            else
            {
                novoGtin = gtin;
            }

            return novoGtin;
        }

        //gera e faz o download do EPC/RFID
        //TODO - colocar permissao correta
        [GS1CNPAttribute(false,true)]
        public void GerarBaixarEPCRFID(dynamic parametro)
        {
            if (parametro != null)
            {

                byte[] bytes = GerarEPCRFIDs(parametro);

                if (bytes != null)
                {
                    HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.AddHeader("Content-Type", "text/plain  ");
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + string.Format("EPCRFID_{0}_{1}{2}", parametro.campos.itemGerado, parametro.campos.codigoproduto.Value.ToString(), ".txt"));
                    HttpContext.Current.Response.AddHeader("Content-Length", bytes.Length.ToString());
                    HttpContext.Current.Response.AddHeader("Cache-Control", "max-age=0");
                    HttpContext.Current.Response.AddHeader("Accept-Ranges", "none");
                    HttpContext.Current.Response.BinaryWrite(bytes);

                    if (File.Exists(HttpContext.Current.Server.MapPath(string.Format("EPCRFID_{0}_{1}{2}", parametro.campos.itemGerado, parametro.campos.codigoproduto.Value.ToString(), ".txt"))))
                    {
                        File.Delete(HttpContext.Current.Server.MapPath(string.Format("EPCRFID_{0}_{1}{2}", parametro.campos.itemGerado, parametro.campos.codigoproduto.Value.ToString(), ".txt")));
                    }

                    HttpContext.Current.Response.Flush();
                }
                else
                {
                    throw new GS1TradeException("Não foi possível gerar o EPC/RFID.");
                }
            }
        }

        [GS1CNPAttribute("CadastrarEPCRFID")]
        public object InsereHistoricoEPCRFID(dynamic parametro)
        {
            dynamic param = null;

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();

                    try
                    {
                        parametro.campos.codigoassociado = associado.codigo.ToString();
                        parametro.campos.codigousuario = user.id.ToString();

                        //Verifica se o Serial Inicio e Serial Fim para o Produto já foi cadastrado
                        string check = String.Empty;

                        //if (parametro.campos.serialfim == null)
                        if (parametro.campos.codigotipohash == 2)
                        {
                            check = @"SELECT COUNT(1) AS QUANTIDADE 
                                        FROM EPCRFID E 
					                    WHERE E.CODIGOPRODUTO = @codigoproduto
                                        AND E.CODIGOTIPOHASH = @codigotipohash
                                        AND E.CODIGOTIPOFILTRO = @codigotipofiltro
					                    AND (E.SERIALINICIO = @serialinicio)";
                        }
                        else
                        {
                            check = @"SELECT COUNT(1) AS QUANTIDADE 
                                        FROM EPCRFID E 
					                    WHERE E.CODIGOPRODUTO = @codigoproduto
                                        AND E.CODIGOTIPOHASH = @codigotipohash
                                        AND E.CODIGOTIPOFILTRO = @codigotipofiltro
					                    AND (
                                                Convert(bigint, @serialinicio) BETWEEN E.SERIALINICIO AND E.SERIALFIM
                                                OR Convert(bigint, @serialfim) BETWEEN E.SERIALINICIO AND E.SERIALFIM
                                                OR (E.SERIALINICIO >= Convert(bigint, @serialinicio) AND E.SERIALFIM <= Convert(bigint, @serialfim))
                                                OR (E.SERIALINICIO < Convert(bigint, @serialinicio) AND E.SERIALFIM > Convert(bigint, @serialfim))
                                            )";
                        }

                        dynamic paramCheck = null;

                        if (parametro.campos != null)
                        {
                            paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                        }

                        dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                        if (resultCheck.quantidade == 0)
                        {
                            if (parametro.campos != null)
                                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                            object retorno = null;
                            string sql = String.Empty;

                            if (parametro.campos.serialfim == null)
                            {
                                sql = @"INSERT INTO EPCRFID (DataAlteracao, CodigoProduto, CodigoTipoHash, SerialInicio, SerialFim, CodigoUsuarioAlteracao, CodigoTipoFiltro)
                                                        VALUES (getDate(), @codigoproduto, @codigotipohash, @serialinicio, null, @codigousuario, @codigotipofiltro)";
                            }
                            else
                            {
                                sql = @"INSERT INTO EPCRFID (DataAlteracao, CodigoProduto, CodigoTipoHash, SerialInicio, SerialFim, CodigoUsuarioAlteracao, CodigoTipoFiltro)
                                                        VALUES (getDate(), @codigoproduto, @codigotipohash, @serialinicio, @serialfim, @codigousuario, @codigotipofiltro)";
                            }
                            retorno = db.Select(sql, param);

                            db.Commit();
                            return JsonConvert.SerializeObject(retorno);
                        }
                        else
                        {
                            throw new GS1TradeException("O produto selecionado já possui EPC/RFID cadastrado para o Serial Início e o Serial Fim informado.");
                        }
                    }
                    catch (GS1TradeException e)
                    {
                        throw new GS1TradeException(e.Message.ToString());
                    }
                }
            }
            else
            {
                return "Não foi possível buscar os dados do associado.";
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisarEPCRFIDs(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT E.CODIGO, E.DATAALTERACAO, P.PRODUCTDESCRIPTION, P.GLOBALTRADEITEMNUMBER, E.CODIGOPRODUTO, T.CODIGO AS CODIGOTIPOHASH, T.NOME AS NOMEHASH, T.QUANTIDADEMAXIMACARACTERES, 
                                    E.SERIALINICIO, E.SERIALFIM, U.NOME AS USUARIO, P.CODITEM, P.NRPREFIXO, TF.CODIGO AS CODIGOTIPOFILTRO ,TF.NOME AS NOMEFILTRO
                                    FROM EPCRFID E (Nolock) 
                                    LEFT JOIN PRODUTO P (Nolock)  ON (P.CODIGOPRODUTO = E.CODIGOPRODUTO)
                                    INNER JOIN TIPOHASH T (Nolock)  ON (T.CODIGO = E.CODIGOTIPOHASH)
                                    INNER JOIN TIPOFILTRO TF  (Nolock) ON (TF.CODIGO = E.CODIGOTIPOFILTRO) 
                                    INNER JOIN USUARIO U  (Nolock) ON (U.CODIGO = E.CODIGOUSUARIOALTERACAO)
                                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString();

                if (parametro != null && parametro.campos != null)
                {

                    if (parametro.campos.codigoproduto != null)
                    {
                        sql += " AND E.CODIGOPRODUTO = @codigoproduto";
                    }

                    if (parametro.campos.statusgtin != null)
                    {
                        sql += " AND P.CODIGOSTATUSGTIN = @statusgtin";
                    }

                    if (parametro.campos.productdescription != null && parametro.campos.productdescription != "")
                    {
                        sql += " AND P.PRODUCTDESCRIPTION like '%' + @productdescription + '%'";
                    }

                    if (parametro.campos.codigotipohash != null && parametro.campos.codigotipohash.ToString() != "")
                    {
                        sql += " AND T.CODIGO = @codigotipohash";
                    }

                    if (parametro.campos.serialinicio != null && parametro.campos.serialinicio.ToString() != "")
                    {
                        sql += " AND E.SERIALINICIO like '%' + @serialinicio + '%'";
                    }

                    if (parametro.campos.serialfim != null && parametro.campos.serialfim.ToString() != "")
                    {
                        sql += " AND E.SERIALFIM like '%' + @serialfim + '%'";
                    }

                    if (parametro.campos.codigotipofiltro != null && parametro.campos.codigotipofiltro.ToString() != "")
                    {
                        sql += " AND TF.CODIGO = @codigotipofiltro";
                    }

                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutosAutoComplete(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, PRODUCTDESCRIPTION 
                                FROM PRODUTO P WITH (NOLOCK)
                                WHERE (P.GLOBALTRADEITEMNUMBER LIKE '%' + @nome + '%' 
                                OR P.PRODUCTDESCRIPTION LIKE '%' + @nome + '%')
                                AND P.GLOBALTRADEITEMNUMBER IS NOT NULL 
                                AND P.CODIGOASSOCIADO = " + associado.codigo.ToString();


                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    List<dynamic> produtos = db.Select(sql, param);

                    return JsonConvert.SerializeObject(produtos);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisaProdutos(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT P.CODIGOPRODUTO
                                    , P.PRODUCTDESCRIPTION
                                    , P.GLOBALTRADEITEMNUMBER
                                    , TG.NOME AS NOMETIPOGTIN
                                    , P.NRPREFIXO 
                                    , P.CODITEM 
                                FROM PRODUTO P WITH (NOLOCK)
                                    INNER JOIN TIPOGTIN TG WITH (NOLOCK) ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                WHERE P.GLOBALTRADEITEMNUMBER IS NOT NULL 
                                    AND P.CODIGOASSOCIADO = " + associado.codigo.ToString() + @"
                                    AND cast(P.globalTradeItemNumber as varchar(max)) like ('%' + ISNULL(@gtin, '') + '%')
                                    AND P.PRODUCTDESCRIPTION LIKE ('%' + ISNULL(@descricao, '') + '%')
                                    AND (P.CODIGOSTATUSGTIN = @codigostatusgtin OR @codigostatusgtin IS NULL OR @codigostatusgtin = '')
                                    AND (P.CODIGOTIPOGTIN = @codigotipogtin OR @codigotipogtin IS NULL OR @codigotipogtin = '')
                                    AND P.CODIGOSTATUSGTIN <> 7 ";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }
    }

}
