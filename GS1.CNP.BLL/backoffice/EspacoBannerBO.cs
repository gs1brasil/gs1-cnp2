﻿using System;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("EspacoBannerBO")]
    class EspacoBannerBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "EspacoBanner"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        public class EspacoBanner
        {
            public int id_Espacobanner { get; set; }
        }

        [GS1CNPAttribute("PesquisarBanner")]
        public object CarregarTodosEspacoBanners(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            string sql = @"SELECT CODIGO, NOME, DESCRICAO, STATUS, ALTURA, LARGURA, URLPUBLICACAO, QUANTIDADEMAXIMA
                                FROM ESPACOBANNER WITH (NOLOCK)";

            using (Database db = new Database("BD"))
            {
                db.Open();

                object retorno = db.Select(sql);

                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(true, true, "EditarBanner", "VisualizarBanner")]
        public override void Update(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            if (parametro != null)
            {
                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, long.Parse(parametro.where.ToString()));
            }

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            try
            {
                if (user != null)
                {
                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                    parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                    base.Update((object)parametro);
                }
            }
            catch (Exception e)
            {
                throw new GS1TradeException("Não foi possível salvar as alterações.", e);
            }
        }

        [GS1CNPAttribute("CadastrarBanner")]
        public override string Insert(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            try
            {
                if (user != null)
                {
                    parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                    var result = base.Insert((object)parametro);

                    if (!string.IsNullOrEmpty(result))
                    {
                        dynamic objResult = JsonConvert.DeserializeObject(result);

                        Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, Convert.ToInt64(objResult[0].codigo));
                    }

                    return result;
                }
                else
                    return "Não foi possível salvar.";
            }
            catch (Exception e)
            {
                throw new GS1TradeException("Não foi possível salvar.", e);
            }
        }
    }
}
