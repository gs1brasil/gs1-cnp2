﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using Newtonsoft.Json;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using GS1.CNP.BLL.Model;
using System.Dynamic;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("EstatisticaBO")]
    class EstatisticaBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "CHAVE"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarEstatisticasGepir(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");
            ParametroBO parametros_sistema = new ParametroBO();

            if (user != null && associado != null)
            {
                GepirRouterBO gepirrouterbo = new GepirRouterBO();
                SumarioGepir sumario = gepirrouterbo.SumarizaConsultasAssociado();


                List<PerfilGepir> perfisGepir = (List<PerfilGepir>)this.getSession("PERFIS_GEPIR");
                PerfilGepir perfilgepir = (from perfil in perfisGepir
                                           where perfil.id == associado.codigoperfilgepir
                                           select perfil).First();


                dynamic estatistica = new ExpandoObject();
                estatistica.limitediario = perfilgepir.daily_queries;
                estatistica.limitesemanal = perfilgepir.weekly_queries;
                estatistica.limitemensal = perfilgepir.monthly_queries;
                estatistica.limitediariodisponivel = perfilgepir.daily_queries - sumario.totaldiario;
                estatistica.limitesemanaldisponivel = perfilgepir.weekly_queries - sumario.totalsemanal;
                estatistica.limitemensaldisponivel = perfilgepir.monthly_queries - sumario.totalmensal;

                List<dynamic> retorno = new List<dynamic>();
                retorno.Add(estatistica);
                return JsonConvert.SerializeObject(retorno);
               
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarEstatisticasInbar(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");
            ParametroBO parametros_sistema = new ParametroBO();

            if (user != null && associado != null)
            {
                //Parâmetro de dias para filtrar a consulta
                dynamic variavel = new ExpandoObject();
                variavel.where = "gepirinbar.nudias.estatistica";
                Int32 numero_dias_estatistica = JsonConvert.DeserializeObject(parametros_sistema.CarregarParametroEspecifico(variavel))[0].valor;

//                string sql = @"SELECT 
//	                            (SELECT COUNT(*) FROM searchlogs WHERE OwnerKey = @cpfcnpj AND GETDATE() BETWEEN GETDATE() AND TIMESTAMP - @numero_dias_estatistica) AS 'ProdutosConsultados', 
//	                            (SELECT COUNT(*) FROM searchlogs WHERE RETURNCODE = 0 AND NUMBEROFHITS = 1 AND OwnerKey = @cpfcnpj AND GETDATE() BETWEEN GETDATE() AND TIMESTAMP - @numero_dias_estatistica) AS 'ExistentesCNP',
//	                            (SELECT COUNT(*) FROM searchlogs WHERE RETURNCODE <> 0 AND OwnerKey = @cpfcnpj AND GETDATE() BETWEEN GETDATE() AND TIMESTAMP - @numero_dias_estatistica) AS 'NaoExistentesCNP'";


                string sql = @"SELECT 
	                            (SELECT COUNT(*) FROM searchlogs  (Nolock)  WHERE OwnerKey = @cpfcnpj AND TIMESTAMP >= GETDATE() - @numero_dias_estatistica) AS 'ProdutosConsultados', 
	                            (SELECT COUNT(*) FROM searchlogs (Nolock)  WHERE RETURNCODE = 0 AND NUMBEROFHITS = 1 AND OwnerKey = @cpfcnpj AND TIMESTAMP >= GETDATE() - @numero_dias_estatistica) AS 'ExistentesCNP',
	                            (SELECT COUNT(*) FROM searchlogs (Nolock)  WHERE RETURNCODE <> 0 AND OwnerKey = @cpfcnpj AND TIMESTAMP >= GETDATE() - @numero_dias_estatistica) AS 'NaoExistentesCNP'";

                parametro.campos.cpfcnpj = associado.cpfcnpj.ToString();
                parametro.campos.numero_dias_estatistica = numero_dias_estatistica != 0 ? numero_dias_estatistica : 0;

                using (Database db = new Database("BDINBAR"))
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Open();
                    List<dynamic> retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

        [GS1CNPAttribute(false)]
        public string InformacoesMapa(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");
            ParametroBO parametros_sistema = new ParametroBO();

            if (user != null && associado != null)
            {
                //Parâmetro de número máximo de itens para retornar
                dynamic variavel = new ExpandoObject();
                variavel.where = "gepirinbar.nuregistros.mapa";
                Int32 max_items = JsonConvert.DeserializeObject(parametros_sistema.CarregarParametroEspecifico(variavel))[0].valor;

                //Parâmetro de dias para filtrar a consulta
                dynamic variavel2 = new ExpandoObject();
                variavel2.where = "gepirinbar.nudias.estatistica";
                Int32 numero_dias_estatistica = JsonConvert.DeserializeObject(parametros_sistema.CarregarParametroEspecifico(variavel2))[0].valor;
                parametro.campos.numero_dias_estatistica = numero_dias_estatistica != 0 ? numero_dias_estatistica : 0;

                string sql = @"SELECT TOP " + max_items + @" OWNERKEY, LATITUDE, LONGITUDE,
	                                CASE 
		                                WHEN RETURNCODE = 0 AND NUMBEROFHITS = 1 THEN 'ExistentesCNP'
		                                WHEN RETURNCODE <> 0 THEN 'NaoExistentesCNP'
	                                END AS SITUACAO
	                                FROM SEARCHLOGS (Nolock) 
	                                WHERE OWNERKEY = @cpfcnpj
                                    AND LATITUDE IS NOT NULL 
                                    AND LONGITUDE IS NOT NULL
                                    AND TIMESTAMP >= GETDATE() - @numero_dias_estatistica";

                parametro.campos.cpfcnpj = associado.cpfcnpj.ToString();

                using (Database db = new Database("BDINBAR"))
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Open();
                    List<dynamic> retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

    }
}
