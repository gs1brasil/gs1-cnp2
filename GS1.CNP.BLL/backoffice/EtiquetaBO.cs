﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using System.Configuration;
using GS1.CNP.BLL.Model;
using GS1.CNP.UTIL;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("EtiquetaBO")]
    public class EtiquetaBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "HistoricoGeracaoEtiqueta"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        [GS1CNPAttribute(false)]
        public object PesquisaProdutosGeracaoEtiqueta(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT   P.CODIGOPRODUTO
                                        ,P.GLOBALTRADEITEMNUMBER
                                        ,P.PRODUCTDESCRIPTION
                                        ,P.CODIGOTIPOGTIN
                                        ,TG.NOME AS NOMETIPOGTIN                                        
		                                ,SG.NOME AS NOMESTATUSGTIN
                                        --,P.ALTERNATEITEMIDENTIFICATIONID
                                        ,P.TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM AS QUANTIDADEPORCAIXA
                                        ,AC.NOME AS NOMEASSOCIADO
                                FROM PRODUTO P WITH (NOLOCK)                                   	                                
                                INNER JOIN TIPOGTIN TG  (Nolock) ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                INNER JOIN STATUSGTIN SG  (Nolock) ON SG.Codigo = P.CODIGOSTATUSGTIN
                                INNER JOIN ASSOCIADO AC (Nolock)  ON AC.CODIGO = P.CODIGOASSOCIADO
                                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString() +
                                @" AND P.GLOBALTRADEITEMNUMBER IS NOT NULL";

                //Usuários GS1 podem ver produtos cancelados e suspensos
                if (user.id_tipousuario != 1)
                {
                    sql += " AND P.CodigoStatusGTIN IN (1,3,6)";
                }

                if (parametro != null && parametro.campos != null)
                {

                    if (parametro.campos.globaltradeitemnumber != null && parametro.campos.globaltradeitemnumber.ToString() != string.Empty)
                    {
                        sql += " AND P.GLOBALTRADEITEMNUMBER = @globaltradeitemnumber";
                    }

                    if (parametro.campos.productdescription != null && parametro.campos.productdescription.ToString() != string.Empty)
                    {
                        sql += " AND P.PRODUCTDESCRIPTION LIKE '%' + @productdescription + '%'";
                    }

                    if (parametro.campos.pesquisaProduto != null && parametro.campos.globaltradeitemnumber == null && parametro.campos.pesquisaProduto != "")
                    {
                        sql += " AND (P.PRODUCTDESCRIPTION LIKE '%' + @pesquisaProduto + '%' OR P.GLOBALTRADEITEMNUMBER LIKE '%' + @pesquisaProduto + '%') ";
                    }


                    if (parametro.campos.codigostatusgtin != null && parametro.campos.codigostatusgtin.ToString() != string.Empty)
                    {
                        sql += " AND P.CODIGOSTATUSGTIN = @codigostatusgtin";
                    }

                    if (parametro.campos.codigotipogtin != null && parametro.campos.codigotipogtin.ToString() != string.Empty)
                    {
                        sql += " AND P.CODIGOTIPOGTIN = @codigotipogtin";
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutosAutoCompleteGeracaoEtiqueta(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, PRODUCTDESCRIPTION                                    
                                    FROM PRODUTO P WITH (NOLOCK)
                                    WHERE (P.GLOBALTRADEITEMNUMBER LIKE '%' + @nome + '%' 
                                    OR P.PRODUCTDESCRIPTION LIKE '%' + @nome + '%')
                                    AND P.CODIGOASSOCIADO = " + associado.codigo.ToString() +
                                    @" AND P.GLOBALTRADEITEMNUMBER IS NOT NULL";


                //Usuários GS1 podem ver produtos cancelados e suspensos
                if (user.id_tipousuario != 1)
                {
                    sql += " AND P.CodigoStatusGTIN IN (1,3)";
                }

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    List<dynamic> produtos = db.Select(sql, param);

                    return JsonConvert.SerializeObject(produtos);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTipoGeracao(dynamic parametro)
        {
            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM TIPOGERACAO WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTipoGeracaoPesquisa(dynamic parametro)
        {
            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM TIPOGERACAO WITH (NOLOCK)";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarPosicaoTexto(dynamic parametro)
        {
            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM POSICAOTEXTO WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string GerarCodigoBarras(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
           
            if (user != null && parametro != null)
            {
                dynamic param = new ExpandoObject();
                CodigoBarras util = new CodigoBarras();
                //param.imagedata =  "ImageHandler.ashx?ImageData=" + Convert.ToBase64String(util.GeraEtiqueta(parametro));

                if (parametro.where.codigotipocodigobarras.Value == 30)
                {
                    string validacaoUPCE = util.ConvertUPCAtoUPCE(parametro.where.codigogtin.Value);
                    if (validacaoUPCE != "-1" || validacaoUPCE != "-2")
                    {
                        throw new GS1TradeException("Esta faixa GTIN-12 não permite geração de UPC-E.");                        
                    }
                    
                }

                if (parametro.where.larguracodigobarras == null || parametro.where.alturacodigobarras == null)
                {
                    string sql = @"SELECT TOP 1 WIDTH, HEIGHT, LEFTGUARD, RIGHTGUARD, IDEALMODULEWIDTH  
                                    FROM BARCODETYPELISTMAGNIFICATION  (Nolock) 
                                    WHERE CODIGOBARCODETYPELIST = " + parametro.where.codigotipocodigobarras + @" 
                                    ORDER BY MAGNIFICATIONFACTOR DESC";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();
                        dynamic retorno = db.Select(sql, null);
                        if (retorno.Count > 0)
                        {
                            parametro.where.larguracodigobarras = retorno[0].width;
                            parametro.where.alturacodigobarras = retorno[0].height;
                            parametro.where.leftguard = retorno[0].leftguard;
                            parametro.where.rightguard = retorno[0].rightguard;
                            parametro.where.idealmodulewidth = retorno[0].idealmodulewidth;
                        }
                    }
                }

                //TODO - Geração externa do codigo de barras

                try
                {
                    if (ConfigurationManager.AppSettings["GeraCodigoBarrasWS"] == "1")
                    {
                        wsBarcode.WSBarcode wsbarcode = new wsBarcode.WSBarcode();
                        return wsbarcode.GerarCodigoBarras(JsonConvert.SerializeObject(parametro));
                    }
                    else
                    {
                        byte[] img = util.GeraEtiqueta(parametro);
                        if (img != null)
                            param.imagedata = Convert.ToBase64String(img);

                        return JsonConvert.SerializeObject(param);
                    }

                }
                catch (Exception ex)
                {

                    byte[] img = util.GeraEtiqueta(parametro);
                    if (img != null)
                        param.imagedata = Convert.ToBase64String(img);

                    return JsonConvert.SerializeObject(param);
                }

                //wsBarcode.WSBarcode wsbarcode = new wsBarcode.WSBarcode();
                //return wsbarcode.GerarCodigoBarras(JsonConvert.SerializeObject(parametro));

                //byte[] img = util.GeraEtiqueta(parametro);
                //if (img != null)
                //    param.imagedata = Convert.ToBase64String(img);

                //return JsonConvert.SerializeObject(param);
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarFabricantes(dynamic parametro)
        {
            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM FABRICANTE WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }


        [GS1CNPAttribute(false)]
        public string BuscarFabricantesTipoCodigoBarras(dynamic parametro)
        {
            string sql = @"SELECT	DISTINCT D.CODIGO, D.NOME, D.DESCRICAO
                            FROM DBO.MODELOETIQUETA A
                            INNER JOIN ModeloEtiquetaBarCodeTypeListMagnification B ON B.CodigoModeloEtiqueta = A.Codigo
                            INNER JOIN BarCodeTypeListMagnification C ON C.Codigo = B.CodigoBarCodeTypeListMagnification
                            INNER JOIN Fabricante D ON D.Codigo = A.CodigoFabricante
                            WHERE A.STATUS = 1 
                            AND D.STATUS = 1                            
                            AND B.CodigoBarCodeTypeList = @codigotipocodigobarras
                            AND A.LARGURA >= (SELECT TOP 1  [Width] FROM [BarCodeTypeListMagnification] 
											 WHERE [CodigoBarCodeTypeList] = @codigotipocodigobarras ORDER BY [MagnificationFactor])
                            AND A.ALTURA >= (SELECT TOP 1  [height] FROM [BarCodeTypeListMagnification] 
											 WHERE [CodigoBarCodeTypeList] = @codigotipocodigobarras ORDER BY [MagnificationFactor])";


            if (parametro.where.modelo128 != null)
                sql += " AND B.CODIGOMODELO = @modelo128";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = ((JToken)parametro.where).ToExpando();
                object retorno = db.Select(sql, param);                
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarEtiquetas(dynamic parametro)
        {
//            string sql = @"SELECT CODIGO,NOME,DESCRICAO,STATUS,CODIGOFABRICANTE,LARGURA
//                           ,ALTURA,TAMANHOFOLHA,QUANTIDADECOLUNAS,QUANTIDADELINHAS,MARGEMSUPERIOR
//	                       ,MARGEMINFERIOR,MARGEMESQUERDA,MARGEMDIREITA,ESPACAMENTOVERTICAL,ESPACAMENTOHORIZONTAL
//                        FROM DBO.MODELOETIQUETA
//                        WHERE STATUS = 1 AND CODIGOFABRICANTE = @codigofabricante";
            if (parametro.where.codigotipocodigobarras != null && parametro.where.codigofabricante != null)
            {

                string sql = @"SELECT	A.CODIGO, A.NOME, A.DESCRICAO, A.STATUS, A.CODIGOFABRICANTE, A.LARGURA
		                            , A.ALTURA, A.TAMANHOFOLHA, A.QUANTIDADECOLUNAS, A.QUANTIDADELINHAS, A.MARGEMSUPERIOR
		                            , A.MARGEMINFERIOR, A.MARGEMESQUERDA, A.MARGEMDIREITA, A.ESPACAMENTOVERTICAL, A.ESPACAMENTOHORIZONTAL
		                            ,B.CODIGOBARCODETYPELISTMAGNIFICATION, C.MAGNIFICATIONFACTOR, C.IDEALMODULEWIDTH, C.WIDTH, C.HEIGHT, C.LEFTGUARD, C.RIGHTGUARD, C.CODIGOBARCODETYPELIST
                            FROM DBO.MODELOETIQUETA A (Nolock) 
                            INNER JOIN ModeloEtiquetaBarCodeTypeListMagnification B  (Nolock) ON B.CodigoModeloEtiqueta = A.Codigo
                            INNER JOIN BarCodeTypeListMagnification C (Nolock)  ON C.Codigo = B.CodigoBarCodeTypeListMagnification
                            WHERE A.STATUS = 1 
                            AND A.CODIGOFABRICANTE = @codigofabricante
                            AND B.CodigoBarCodeTypeList = @codigotipocodigobarras
                            AND A.LARGURA >= (SELECT TOP 1  [Width] FROM [BarCodeTypeListMagnification] 
											 WHERE [CodigoBarCodeTypeList] = @codigotipocodigobarras ORDER BY [MagnificationFactor])
                            AND A.ALTURA >= (SELECT TOP 1  [height] FROM [BarCodeTypeListMagnification] 
											 WHERE [CodigoBarCodeTypeList] = @codigotipocodigobarras ORDER BY [MagnificationFactor])";


                if (parametro.where.modelo128 != null)
                    sql += " AND B.CODIGOMODELO = @modelo128";


                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = ((JToken)parametro.where).ToExpando();
                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            return null;
        }

        [GS1CNPAttribute(false)]
        public string BuscarModeloEtiquetas(dynamic parametro)
        {
            string sql = @"SELECT CODIGO,NOME
                        FROM DBO.MODELOETIQUETA (Nolock) 
                        WHERE STATUS = 1";


            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("PesquisarEtiqueta")]
        public string BuscarHistoricoEtiqueta(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT HI.CODIGO, P.CODIGOPRODUTO, HI.DATAIMPRESSAO, HI.TEXTO AS TEXTOLIVRE 
	                                , HI.CODIGOFABRICANTE, HI.QUANTIDADE AS QUANTIDADEETIQUETA, HI.DESCRICAO, HI.CODIGOPOSICAOTEXTO AS CODIGOPOSICAO, P.GLOBALTRADEITEMNUMBER
		                            , P.PRODUCTDESCRIPTION AS PRODUCTDESCRIPTION, TG.NOME AS NOMETIPOGTIN
		                            , T.CODIGO AS CODIGOTIPOGERACAO, T.NOME AS NOMETIPOGERACAO
		                            , BTL.CODIGO AS TIPOETIQUETA, BTL.NAME AS NOMETIPOETIQUETA
		                            , ME.CODIGO AS CODIGOETIQUETA, ME.NOME AS NOMEMODELOETIQUETA
		                            , U.CODIGO AS CODIGOUSUARIO, U.NOME AS NOMEUSUARIO, SG.NOME AS NOMESTATUSGTIN 
                                    , HI.FONTENOMEDESCRICAO, HI.FONTETAMANHODESCRICAO, HI.ALINHAMENTODESCRICAO, HI.FONTENOMETEXTOLIVRE, HI.FONTETAMANHOTEXTOLIVRE, HI.ALINHAMENTOTEXTOLIVRE
                                    ,HI.NRLOTE, CONVERT(VARCHAR(10),HI.DATAVENCIMENTO,103) AS DATAVENCIMENTO, HI.QUANTIDADEPORCAIXA, HI.MODELO128
                                    ,P.CODIGOTIPOGTIN, PAG.ALTERNATEITEMIDENTIFICATIONID
                                    , '" + associado.nome.ToString() + @"' AS NOMEASSOCIADO 
                                    ,HI.CODIGOHOMOLOGACAO, HI.SERIALINICIAL, HI.SERIALFIM
                                FROM HISTORICOGERACAOETIQUETA HI (Nolock) 
	                            INNER JOIN PRODUTO P (Nolock)  ON P.CODIGOPRODUTO = HI.CODIGOPRODUTO
	                            INNER JOIN TIPOGERACAO T  (Nolock) ON T.CODIGO = HI.CODIGOTIPOGERACAO
	                            INNER JOIN TIPOGTIN TG (Nolock)  ON TG.CODIGO = P.CODIGOTIPOGTIN	                        
                                INNER JOIN BARCODETYPELIST BTL (Nolock)  ON BTL.CODIGO = HI.CODIGOBARCODETYPELIST
	                            LEFT JOIN MODELOETIQUETA ME (Nolock)  ON ME.CODIGO = HI.CODIGOMODELOETIQUETA
	                            INNER JOIN USUARIO U  (Nolock) ON U.CODIGO = HI.CODIGOUSUARIO
                                INNER JOIN STATUSGTIN SG  (Nolock) ON SG.CODIGO = P.CODIGOSTATUSGTIN
                                LEFT JOIN PRODUTOAGENCIAREGULADORA PAG (Nolock)  ON PAG.CODIGO = HI.CODIGOHOMOLOGACAO
                                WHERE CODIGOASSOCIADO = " + associado.codigo.ToString();

                //Usuários Associados só podem ver Etiquetas com Status de GTIN Ativo ou Reativado
                if (user.id_tipousuario != 1)
                {
                    sql += " AND P.CODIGOSTATUSGTIN IN (1,3,6)";
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string PesquisarHistoricoEtiqueta(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT HI.CODIGO, P.CODIGOPRODUTO, HI.DATAIMPRESSAO, HI.TEXTO AS TEXTOLIVRE 
	                                , HI.CODIGOFABRICANTE, HI.QUANTIDADE AS QUANTIDADEETIQUETA, HI.DESCRICAO, HI.CODIGOPOSICAOTEXTO AS CODIGOPOSICAO, P.GLOBALTRADEITEMNUMBER
		                            , P.PRODUCTDESCRIPTION AS PRODUCTDESCRIPTION, TG.NOME AS NOMETIPOGTIN
		                            , T.CODIGO AS CODIGOTIPOGERACAO, T.NOME AS NOMETIPOGERACAO
		                            , BTL.CODIGO AS TIPOETIQUETA, BTL.NAME AS NOMETIPOETIQUETA
		                            , ME.CODIGO AS CODIGOETIQUETA, ME.NOME AS NOMEMODELOETIQUETA
		                            , U.CODIGO AS CODIGOUSUARIO, U.NOME AS NOMEUSUARIO, SG.NOME AS NOMESTATUSGTIN 
                                    ,HI.NRLOTE, CONVERT(VARCHAR(10),HI.DATAVENCIMENTO,103) AS DATAVENCIMENTO, HI.QUANTIDADEPORCAIXA, HI.MODELO128
                                    ,P.CODIGOTIPOGTIN, PAG.ALTERNATEITEMIDENTIFICATIONID
                                    , '" + associado.nome.ToString() + @"' AS NOMEASSOCIADO 
                                    ,HI.CODIGOHOMOLOGACAO, HI.SERIALINICIAL, HI.SERIALFIM
                                FROM HISTORICOGERACAOETIQUETA HI (Nolock) 
	                            INNER JOIN PRODUTO P (Nolock)  ON P.CODIGOPRODUTO = HI.CODIGOPRODUTO
	                            INNER JOIN TIPOGERACAO T (Nolock)  ON T.CODIGO = HI.CODIGOTIPOGERACAO
	                            INNER JOIN TIPOGTIN TG (Nolock)  ON TG.CODIGO = P.CODIGOTIPOGTIN
	                            INNER JOIN BARCODETYPELIST BTL (Nolock)  ON BTL.CODIGO = HI.CODIGOBARCODETYPELIST
	                            LEFT JOIN MODELOETIQUETA ME  (Nolock) ON ME.CODIGO = HI.CODIGOMODELOETIQUETA
	                            INNER JOIN USUARIO U (Nolock)  ON U.CODIGO = HI.CODIGOUSUARIO
                                INNER JOIN STATUSGTIN SG  (Nolock) ON SG.CODIGO = P.CODIGOSTATUSGTIN
                                LEFT JOIN PRODUTOAGENCIAREGULADORA PAG  (Nolock) ON PAG.CODIGO = HI.CODIGOHOMOLOGACAO
                                WHERE CODIGOASSOCIADO = " + associado.codigo.ToString();

                if (parametro != null && parametro.campos != null)
                {
                    //Usuários Associados só podem ver Etiquetas com Status de GTIN Ativo ou Reativado
                    if (user.id_tipousuario != 1)
                    {
                        sql += " AND P.CODIGOSTATUSGTIN IN (1,3)";
                    }

                    if (parametro.campos.globaltradeitemnumber != null)
                    {
                        sql += " AND P.GLOBALTRADEITEMNUMBER = @globaltradeitemnumber";
                    }

                    if (parametro.campos.codigotipogeracao != null)
                    {
                        sql += " AND T.CODIGO = @codigotipogeracao";
                    }

                    if (parametro.campos.codigotipocodigobarras != null)
                    {
                        sql += " AND BTL.CODIGO = @codigotipocodigobarras";
                    }

                    if (parametro.campos.codigoetiqueta != null)
                    {
                        sql += " AND ME.CODIGO = @codigoetiqueta";
                    }

                    if (parametro.campos.productdescription != null)
                    {
                        sql += " AND P.PRODUCTDESCRIPTION LIKE '%' + @productdescription + '%'";
                    }

                    if (parametro.campos.codigofabricante != null)
                    {
                        sql += " AND HI.CODIGOFABRICANTE = @codigofabricante";
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute("CadastrarEtiqueta",true,true)]
        public void CadastrarEtiqueta(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic dados = new ExpandoObject();
                    dados.codigousuario = user.id;
                    dados.codigoproduto = parametro.campos.codigoproduto.Value;
                    dados.codigotipogeracao = parametro.campos.codigotipogeracao.Value;
                    dados.dataimpressao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");                    
                    
                    if(parametro.campos.codigofabricante !=null)
                        dados.codigofabricante = parametro.campos.codigofabricante.Value;
                    if(parametro.campos.codigoetiqueta != null)
                        dados.codigomodeloetiqueta = parametro.campos.codigoetiqueta.Value;
                    if(parametro.campos.quantidadeetiqueta != null)
                        dados.quantidade = parametro.campos.quantidadeetiqueta.Value;
                    
                    dados.CodigoBarCodeTypeList = parametro.campos.tipoetiqueta.Value;
                    dados.alinhamentodescricao = parametro.campos.alinhamentodescricao.Value;
                    dados.fontenomedescricao = parametro.campos.fontenomedescricao.Value;
                    dados.fontetamanhodescricao = parametro.campos.fontetamanhodescricao.Value;
                    dados.alinhamentotextolivre = parametro.campos.alinhamentotextolivre.Value;
                    dados.fontenometextolivre = parametro.campos.fontenometextolivre.Value;
                    dados.fontetamanhotextolivre = parametro.campos.fontetamanhotextolivre.Value;
                    

                    if (parametro.campos.textolivre != null)
                        dados.texto = parametro.campos.textolivre.Value;

                    if (parametro.campos.descricao != null)
                    dados.descricao = parametro.campos.descricao.Value;

                    if (parametro.campos.codigoposicao != null)
                        dados.codigoposicaotexto = parametro.campos.codigoposicao.Value;



                    if (parametro.campos.modelo128 != null)
                        dados.modelo128 = parametro.campos.modelo128.Value;
                    
                    if (parametro.campos.nrlote != null)
                        dados.nrlote = parametro.campos.nrlote.Value;

                    if (parametro.campos.datavencimento != null && parametro.campos.datavencimento != string.Empty)
                        dados.datavencimento = DateTime.ParseExact(Convert.ToString(parametro.campos.datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);


                    if (parametro.campos.quantidadeporcaixa != null)
                        dados.quantidadeporcaixa = parametro.campos.quantidadeporcaixa.Value;

                    if (parametro.campos.codigohomologacao != null)
                        dados.codigohomologacao = parametro.campos.codigohomologacao.Value;

                    if (parametro.campos.serialinicial != null)
                        dados.serialinicial = parametro.campos.serialinicial.Value;

                    if (parametro.campos.serialfim != null)
                        dados.serialfim = parametro.campos.serialfim.Value;
                    

                    int codigoImportacao = db.Insert("HISTORICOGERACAOETIQUETA", "Codigo", dados);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(true, true, "EditarEtiqueta", "VisualizarEtiqueta")]
        public void EditarEtiqueta(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic dados = new ExpandoObject();
                    dados.codigousuario = user.id;
                    dados.codigo = parametro.campos.codigo.Value;
                    dados.codigoproduto = parametro.campos.codigoproduto.Value;
                    dados.codigotipogeracao = parametro.campos.codigotipogeracao.Value;
                    dados.dataimpressao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                    dados.codigofabricante = parametro.campos.codigofabricante.Value;
                    dados.codigomodeloetiqueta = parametro.campos.codigoetiqueta.Value;
                    dados.quantidade = parametro.campos.quantidadeetiqueta.Value;
                    dados.CodigoBarCodeTypeList = parametro.campos.tipoetiqueta.Value;

                    if (parametro.campos.textolivre != null)
                        dados.texto = parametro.campos.textolivre.Value;

                    if (parametro.campos.descricao != null)
                        dados.descricao = parametro.campos.descricao.Value;

                    if (parametro.campos.codigoposicao != null)
                        dados.codigoposicaotexto = parametro.campos.codigoposicao.Value;




                    if (parametro.campos.modelo128 != null)
                        dados.modelo128 = parametro.campos.modelo128.Value;

                    if (parametro.campos.nrlote != null)
                        dados.nrlote = parametro.campos.nrlote.Value;

                    if (parametro.campos.datavencimento != null && parametro.campos.datavencimento != string.Empty)
                        dados.datavencimento = DateTime.ParseExact(Convert.ToString(parametro.campos.datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);


                    if (parametro.campos.quantidadeporcaixa != null)
                        dados.quantidadeporcaixa = parametro.campos.quantidadeporcaixa.Value;

                    if (parametro.campos.codigohomologacao != null)
                        dados.codigohomologacao = parametro.campos.codigohomologacao.Value;

                    if (parametro.campos.serialinicial != null)
                        dados.serialinicial = parametro.campos.serialinicial.Value;

                    if (parametro.campos.serialfim != null)
                        dados.serialfim = parametro.campos.serialfim.Value;


                    int codigoImportacao = db.Update("HISTORICOGERACAOETIQUETA", "codigo", dados);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisaLotesGeracaoEtiqueta(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT NRLOTE, CONVERT(VARCHAR(10),CONVERT(DATE,DATAPROCESSAMENTO,106),103) AS DATAPROCESSAMENTO, CONVERT(VARCHAR(10),CONVERT(DATE,DATAVENCIMENTO,106),103) AS DATAVENCIMENTO FROM LOTE (Nolock)  WHERE CODIGOSTATUSLOTE = 2 AND CODIGOPRODUTO = " + parametro.campos.codigoproduto.ToString();


                if (parametro.campos.de_dataprocessamento != null && parametro.campos.de_dataprocessamento != string.Empty)
                {
                    parametro.campos.de_dataprocessamento = DateTime.ParseExact(Convert.ToString(parametro.campos.de_dataprocessamento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sql += " AND DATAPROCESSAMENTO >= CONVERT(DATETIME,CONVERT(VARCHAR(10),@de_dataprocessamento,101))";
                }

                if (parametro.campos.ate_dataprocessamento != null && parametro.campos.ate_dataprocessamento != string.Empty)
                {
                    parametro.campos.ate_dataprocessamento = DateTime.ParseExact(Convert.ToString(parametro.campos.ate_dataprocessamento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sql += " AND DATAPROCESSAMENTO <= CONVERT(DATETIME,CONVERT(VARCHAR(10),@ate_dataprocessamento,101))";
                }

                if (parametro.campos.de_datavencimento != null  && parametro.campos.de_datavencimento != string.Empty)
                {
                    parametro.campos.de_datavencimento = DateTime.ParseExact(Convert.ToString(parametro.campos.de_datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sql += " AND DATAVENCIMENTO >= CONVERT(DATETIME,CONVERT(VARCHAR(10),@de_datavencimento,101))";
                }

                if (parametro.campos.ate_datavencimento != null && parametro.campos.ate_datavencimento != string.Empty)
                {
                    parametro.campos.ate_datavencimento = DateTime.ParseExact(Convert.ToString(parametro.campos.ate_datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sql += " AND DATAVENCIMENTO <= CONVERT(DATETIME,CONVERT(VARCHAR(10),@ate_datavencimento,101))";
                }

                if (parametro.campos.nr_lote != null && parametro.campos.nr_lote != string.Empty)
                {
                    sql += " AND NRLOTE = @nr_lote";
                }


                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarHomologacoesAnatel(dynamic parametro)
        {
            string sql = @"SELECT A.Codigo, a.alternateItemIdentificationId 
                            FROM PRODUTOAGENCIAREGULADORA A (Nolock) 
                            INNER JOIN AGENCIAREGULADORA B (Nolock)  ON B.CODIGO = A.CODIGOAGENCIA
                            WHERE B.NOME = 'ANATEL' AND CODIGOPRODUTO = @codigoproduto";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarHomologacoesAnvisa(dynamic parametro)
        {
            string sql = @"SELECT A.Codigo, a.alternateItemIdentificationId 
                            FROM PRODUTOAGENCIAREGULADORA A (Nolock) 
                            INNER JOIN AGENCIAREGULADORA B (Nolock)  ON B.CODIGO = A.CODIGOAGENCIA
                            WHERE B.NOME = 'ANVISA' AND CODIGOPRODUTO = @codigoproduto";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

    }
}
