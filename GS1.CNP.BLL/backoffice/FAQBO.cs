﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("FAQBO")]
    public class FAQBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "FAQ"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarFAQs(dynamic parametro)
        {

            if (parametro != null && parametro.campos != null)
            {
                if (parametro.campos.registroporpagina == null)
                    parametro.campos.registroporpagina = 10;
            }

            string sql = @"SELECT CODIGO, NOME AS PERGUNTA, DESCRICAO AS RESPOSTA, URL AS VIDEO, QUANTIDADETOTAL = COUNT(*) OVER()
                                FROM FAQ (Nolock) 
                                WHERE CODIGOSTATUSPUBLICACAO = 1
                                AND CODIGOIDIOMA = @codigoidioma
                                AND (CODIGOSTATUSVISUALIZACAOFAQ = 1 OR CODIGOSTATUSVISUALIZACAOFAQ = 3)
                                ORDER BY ORDEM ASC, DATACADASTRO DESC
	                            OFFSET " + parametro.campos.registroporpagina + " * (@paginaatual - 1) ROWS FETCH NEXT " + parametro.campos.registroporpagina + " ROWS ONLY";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro != null && parametro.campos != null)
                {
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                }

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
