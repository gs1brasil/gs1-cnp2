﻿using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("GPCBO")]
    class GPCBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "GPC"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        [GS1CNPAttribute(false)]
        public object buscarSegmentos(dynamic parametro)
        {

            string sql = @" SELECT CODESEGMENT AS CODIGO
                                , IDIOMA
                                , TEXT AS NOME 
                            FROM TBSEGMENT WITH (NOLOCK) 
                            ORDER BY TEXT ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> consulta = db.Select(sql, param);

                return JsonConvert.SerializeObject(consulta);
            }
        }

        [GS1CNPAttribute(false)]
        public object buscarFamilia(dynamic parametro)
        {

            string sql = @" SELECT CODEFAMILY AS CODIGO
                                , IDIOMA
                                , TEXT AS NOME 
                            FROM TBFAMILY WITH (NOLOCK) 
                            WHERE CODESEGMENT = @codigosegmento
                            ORDER BY TEXT ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> consulta = db.Select(sql, param);

                return JsonConvert.SerializeObject(consulta);
            }
        }

        [GS1CNPAttribute(false)]
        public object buscarClasse(dynamic parametro)
        {

            string sql = @" SELECT CODECLASS AS CODIGO
                                , IDIOMA
                                , TEXT AS NOME 
                            FROM TBCLASS WITH (NOLOCK) 
                            WHERE CODEFAMILY = @codigofamilia
                            ORDER BY TEXT ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> consulta = db.Select(sql, param);

                return JsonConvert.SerializeObject(consulta);
            }
        }

        [GS1CNPAttribute(false)]
        public object buscarBrick(dynamic parametro)
        {

            string sql = @" SELECT CODEBRICK AS CODIGO
                                , IDIOMA
                                , TEXT AS NOME 
                            FROM TBBRICK WITH (NOLOCK) 
                            WHERE CODECLASS = @codigoclasse
                            ORDER BY TEXT ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> consulta = db.Select(sql, param);

                return JsonConvert.SerializeObject(consulta);
            }
        }
    }
}
