﻿using System;
using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using System.Data;
using GS1.CNP.DAL;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("GepirBO")]
    public class GepirBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "ConfigurationProfile"; }
        }

        public override string nomePK
        {
            get { return "Id"; }
        }
        
        public List<PerfilGepir> GetConfigurationProfile()
        {

            string sql = @"SELECT [Id]
                              ,[Name]
                              ,[DailyQueries]
                              ,[WeeklyQueries]
                              ,[MonthlyQueries]
                              ,[AutomaticAuthorization]
                              ,[SharedQueries]
                              ,[UnlimitedQueries]
                          FROM [dbo].[ConfigurationProfile]  (Nolock) ";

            using (Database db = new Database("BDBASEUNICAGEPIR"))
            {
                db.Open();

                Func<IDataRecord, PerfilGepir> binder = r => new PerfilGepir()
                {
                    id = r.GetInt32(0)                 
                    ,name = r.GetString(1)                 
                    ,daily_queries = r.GetInt32(2)
                    ,weekly_queries = r.GetInt32(3)
                    ,monthly_queries = r.GetInt32(4)
                    ,automatic_authorization = r.GetBoolean(5)
                    ,shared_queries = r.GetBoolean(6)
                    ,unlimited_queries = r.GetBoolean(7)                 
                };

                List<PerfilGepir> retorno = db.Select(sql, null, binder);
                return retorno;
            }
        }

        [GS1CNPAttribute(false)]
        public string ConsultarConfigurationProfile (dynamic parametro)
        {

            string sql = @"SELECT [Id]
                              ,[Name]
                              ,[DailyQueries]
                              ,[WeeklyQueries]
                              ,[MonthlyQueries]
                              ,[AutomaticAuthorization]
                              ,[SharedQueries]
                              ,[UnlimitedQueries]
                          FROM [dbo].[ConfigurationProfile]  (Nolock) ";

            using (Database db = new Database("BDBASEUNICAGEPIR"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]       
        public List<ReturnCodeMessage> ConsultarReturnCodeMessage()
        {

            string sql = @"SELECT VALUE, TEXT, TEXTPT FROM [dbo].[ReturnCodeMessage]  (Nolock) ";
            using (Database db = new Database("BDBASEUNICAGEPIR"))
            {
                db.Open();

                Func<IDataRecord, ReturnCodeMessage> binder = r => new ReturnCodeMessage()
                {
                    value = r.GetInt32(0)
                    ,
                    text = r.GetString(1)
                    ,
                    textpt = r.GetString(2)                    
                };

                List<ReturnCodeMessage> retorno = db.Select(sql, null, binder);
                return retorno;
            }
        }

    }
}
