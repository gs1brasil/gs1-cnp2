﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using System.Data;
using Newtonsoft.Json;
using System.Dynamic;
using System.Configuration;
using GS1.CNP.BLL.Model;
using GS1.CNP.BLL.wsGepirRouter40;
using StackExchange.Redis;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("GepirRouterBO")]
    public class GepirRouterBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        private bool ValidateGLN(string value)
        {
            if (value.Length == 13)
                return true;

            return false;
        }

        public void GepirRouterLOG(string metodo, object entrada, object saida, string urlws, object header, object responseHeader) 
        {

            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null)
            {                
                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic param = new ExpandoObject();
                    param.dataconsulta = DateTime.Now;
                    param.codigousuario = user != null ? user.id.ToString() : null;
                    param.codigoassociado = associado != null ? associado.codigo.ToString() : null;
                    param.metodo = metodo;
                    param.campos = JsonConvert.SerializeObject(entrada);
                    param.valores = JsonConvert.SerializeObject(saida);
                    param.ip = request.ServerVariables["REMOTE_ADDR"];
                    param.servidor = request.ServerVariables["LOCAL_ADDR"];
                    param.endpoint = urlws;
                    param.header = JsonConvert.SerializeObject(header);
                    param.responseheader = JsonConvert.SerializeObject(responseHeader);

                    try
                    {
                        db.Insert("LogGepirRouter", "Codigo", param);
                    }
                    catch(Exception e) {
                        throw new GS1TradeException("Não foi possível armazenar o Log do Gepir.");
                    }
                }
            }
        }

        public SumarioGepir SumarizaConsultasAssociado()
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");           

            if (user != null)
            {

                string sql = @"SELECT

		                        (SELECT COUNT(*) FROM LogGepirRouter  (Nolock) 
		                        WHERE (DATEPART(m, DataConsulta) = DATEPART(m, GETDATE())
		                        AND DATEPART(YEAR, DataConsulta) = DATEPART(YEAR, GETDATE()))
		                        and CodigoAssociado = @codigoassociado) AS TOTALMENSAL,

		                        (SELECT COUNT(*)
		                        FROM LogGepirRouter (Nolock) 
		                        WHERE DataConsulta BETWEEN CONVERT(DATETIME,CONVERT(VARCHAR(10),(GETDATE() + (1-DATEPART(DW,GETDATE()))),101))
							                           AND CONVERT(DATETIME,CONVERT(VARCHAR(10),(GETDATE() + (7-DATEPART(DW,GETDATE()))),101) + ' 23:59')
		                        AND CodigoAssociado = @codigoassociado) AS TOTALSEMANAL,

		                        (SELECT COUNT(*)
		                        FROM LogGepirRouter (Nolock) 
		                        WHERE DataConsulta BETWEEN CONVERT(DATETIME,CONVERT(VARCHAR(10),GETDATE(),101))
							                           AND CONVERT(DATETIME,CONVERT(VARCHAR(10),GETDATE(),101) + ' 23:59')
		                        AND CodigoAssociado = @codigoassociado) AS TOTALDIARIO,

                                GETDATE() AS DATASUMARIO";


                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();


                    Func<IDataRecord, SumarioGepir> binder = r => new SumarioGepir()
                    {
                        totalmensal = r.GetInt32(0)
                        ,
                        totalsemanal = r.GetInt32(1)
                        ,
                        totaldiario = r.GetInt32(2)
                        ,
                        dataSumario = r.GetDateTime(3)
                    };

                    dynamic param = new ExpandoObject();

                    param.codigoassociado = associado != null ? associado.codigo.ToString() : null;


                    try
                    {
                        List<SumarioGepir> retorno = db.Select(sql, param, binder);

                        if (retorno.Count > 0)
                        {
                            return retorno[0];
                        }
                        else 
                        {
                            return null;
                        }
                        
                    }
                    catch (Exception e)
                    {
                        throw new GS1TradeException("Não foi possível armazenar o Log do Gepir.");
                    }
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível armazenar o Log do Gepir.");
            }
        }

        public void ComputarConsultaGepir()
        {
            try
            {
                Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");
                if (associado != null && associado.codigoperfilgepir > 0)
                {
                    string HashConsultaGepir = ConfigurationManager.AppSettings["HashConsultaGepir"];
                    ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(ConfigurationManager.AppSettings["RedisHost"] + ",ssl=true,password=" + ConfigurationManager.AppSettings["RedisAccessKey"]);
                    IDatabase db = redis.GetDatabase();
                    SumarioGepir sumario = null;
                    
                    if (db.KeyExists(HashConsultaGepir)) // Verifica se a chave de consultas existe no Redis
                    {
                        // Busca a chave do associado
                        var t1 = db.HashScan(HashConsultaGepir, associado.codigo); 
                        
                        if (t1.Count() > 0)
                        {
                            var s = (from t in t1
                                       select t).First();

                            sumario = (SumarioGepir)ByteArrayToObject(s.Value);
                        }
                        else
                        {
                            //Sumariza do log e grava no cache Redis
                            sumario = SumarizaConsultasAssociado();
                            HashEntry[] hashEntryArr = new HashEntry[1];
                            hashEntryArr[0] = new HashEntry(associado.codigo, ObjectToByteArray(sumario));
                            db.HashSet(HashConsultaGepir, hashEntryArr);
                        }
                    }
                    else
                    {
                        //Sumariza do log e grava no cache Redis
                        sumario = SumarizaConsultasAssociado();
                        HashEntry[] hashEntryArr = new HashEntry[1];

                        hashEntryArr[0] = new HashEntry(associado.codigo, ObjectToByteArray(sumario));
                        db.HashSet(HashConsultaGepir, hashEntryArr);
                        db.KeyExpire(HashConsultaGepir, TimeSpan.FromHours(6));
                    }


                    if (sumario != null)
                    {
                        List<PerfilGepir> perfisGepir = (List<PerfilGepir>)this.getSession("PERFIS_GEPIR");
                        PerfilGepir perfilgepir = (from perfil in perfisGepir
                                                   where perfil.id == associado.codigoperfilgepir
                                                   select perfil).First();

                        if (!(perfilgepir.daily_queries > sumario.totaldiario && perfilgepir.weekly_queries > sumario.totalsemanal && perfilgepir.monthly_queries > sumario.totalmensal))
                        {
                            throw new GS1TradeException("Limite de consultas atingido");
                        }
                        else
                        {
                            sumario.totaldiario = sumario.totaldiario + 1;
                            sumario.totalsemanal = sumario.totalsemanal + 1;
                            sumario.totalmensal = sumario.totalmensal + 1;
                            HashEntry[] hashEntryArr = new HashEntry[1];
                            hashEntryArr[0] = new HashEntry(associado.codigo, ObjectToByteArray(sumario));
                            db.HashSet(HashConsultaGepir, hashEntryArr);                            
                        }
                    }
                    else
                    {
                        throw new GS1TradeException("Não foi possível efetuar a consulta no Gepir.");
                    }  
                }
                else
                {
                    throw new GS1TradeException("Não foi possível efetuar a consulta no Gepir. Associado sem perfil.");
                }                
            }
            catch (Exception e)
            {                
                throw e;
            }
           
        }

        public static byte[] ObjectToByteArray(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
        
        public static Object ByteArrayToObject(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }

        public router BuildHeader(router wsGepirRouter40)
        {
            RequestHeaderType header = new RequestHeaderType();
            header.requesterGLN = ConfigurationManager.AppSettings["SearchAllowedGLNTelasGEPIR"];
            header.onBehalfOfGLN = header.requesterGLN;
            header.isAuthenticated = true;
            header.cascade = int.Parse(ConfigurationManager.AppSettings["CascadeGEPIR"]); ;
            wsGepirRouter40.requestHeader = header;
            return wsGepirRouter40;
        }

        [GS1CNPAttribute(false)]
        public string ConsultarWSPartyByName(dynamic parametro)
        {

            try
            {
                if (parametro != null && parametro.campos != null)
                {
                    router wsRouter = new router();
                    wsRouter = BuildHeader(wsRouter);

                    getPartyByName partyname = new getPartyByName();
                    partyname.getPartyByName1 = new GetPartyByNameType();
                    partyname.getPartyByName1.gepirGetPartyRequestParameters = new GepirGetPartyRequestParametersType();

                    wsGepirRouter40.CountryCodeType codigopais = new wsGepirRouter40.CountryCodeType();
                    codigopais.codeListVersion = "4.0";
                    codigopais.Value = parametro.campos.pais;

                    partyname.getPartyByName1.gepirGetPartyRequestParameters.requestedPartyName = parametro.campos.nomeempresa != null ? parametro.campos.nomeempresa : String.Empty;
                    partyname.getPartyByName1.gepirGetPartyRequestParameters.requestedCountry = codigopais;
                    partyname.getPartyByName1.gepirGetPartyRequestParameters.requestedCity = parametro.campos.cidade != null ? parametro.campos.cidade : String.Empty;
                    partyname.getPartyByName1.gepirGetPartyRequestParameters.requestedPostalCode = parametro.campos.codigopostal != null ? parametro.campos.codigopostal : String.Empty;
                    partyname.getPartyByName1.gepirGetPartyRequestParameters.requestedStreetAddress = parametro.campos.endereco != null ? parametro.campos.endereco : String.Empty;

                    getPartyByNameResponse result = wsRouter.getPartyByName(partyname);
                    
                    GepirRouterLOG("GetPartyByName", parametro.campos, result, wsRouter.Url, wsRouter.requestHeader, wsRouter.responseHeader);

                   
                    foreach (PartyDataLineType item in result.gepirParty)
                    {                       
                        List<ReturnCodeMessage> returnCodes = (List<ReturnCodeMessage>)this.getSession("RETURN_CODE_GEPIR");
                        bool has = returnCodes.Any(ret => ret.value.ToString() == item.returnCode.Value);
                        if (has)
                        {
                            ReturnCodeMessage returnCodeMessage = (from returncode in returnCodes
                                                                    where returncode.value.ToString() == item.returnCode.Value
                                                                    select returncode).First();
                            item.returnCode.Value = item.returnCode.Value + " - " + returnCodeMessage.textpt;
                        }                        
                    }
                    
                    return JsonConvert.SerializeObject(result);
                }
                else {
                    return String.Empty;
                }
            }
            catch (GS1TradeException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                //TODO - Tratar erro consultas
                return String.Empty;
            }

        }

        [GS1CNPAttribute(false)]
        public string ConsultarWSPartyByGLN(dynamic parametro)
        {

            ComputarConsultaGepir();               

            if (parametro != null && parametro.campos != null)
            {

                GS1.CNP.BLL.wsGepirRouter.router gepirbr = new GS1.CNP.BLL.wsGepirRouter.router();
                GS1.CNP.BLL.wsGepirRouter.gepirRequestHeader header = new GS1.CNP.BLL.wsGepirRouter.gepirRequestHeader();
                header.requesterGln = ConfigurationManager.AppSettings["SearchAllowedGLNTelasGEPIR"];
                header.cascade = int.Parse(ConfigurationManager.AppSettings["CascadeGEPIR"]); ;

                    
                GS1.CNP.BLL.wsGepirRouter.GetPartyByGLN getPartyByGLN = new GS1.CNP.BLL.wsGepirRouter.GetPartyByGLN();
                getPartyByGLN.requestedGln = new string[1];
                getPartyByGLN.requestedGln[0] = parametro.campos.gln.ToString();

                gepirbr.gepirRequestHeaderValue = header;
                GS1.CNP.BLL.wsGepirRouter.gepirParty result = gepirbr.GetPartyByGLN(getPartyByGLN);
                    

                GepirRouterLOG("ConsultarWSPartyByGLN", parametro.campos, result, gepirbr.Url, gepirbr.gepirRequestHeaderValue, gepirbr.gepirResponseHeaderValue);

                if (result.partyDataLine.Count() > 0)
                {
                    foreach (GS1.CNP.BLL.wsGepirRouter.partyDataLineType item in result.partyDataLine)
                    {                            
                        List<ReturnCodeMessage> returnCodes = (List<ReturnCodeMessage>)this.getSession("RETURN_CODE_GEPIR");
                        bool has = returnCodes.Any(ret => ret.value.ToString() == item.returnCode);
                        if (has)
                        {
                            ReturnCodeMessage returnCodeMessage = (from returncode in returnCodes
                                                                    where returncode.value.ToString() == item.returnCode
                                                                    select returncode).First();
                            item.returnCode = item.returnCode + " - " + returnCodeMessage.textpt;
                        }                            
                    }

                    return JsonConvert.SerializeObject(result);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }            
        }

        [GS1CNPAttribute(false)]
        public string ConsultarWSPartyBySSCC(dynamic parametro)
        {            
            ComputarConsultaGepir();
            if (parametro != null && parametro.campos != null)
            {
                router wsRouter = new router();
                wsRouter = BuildHeader(wsRouter);

                RequestedKeyCodeType KeyCode = new RequestedKeyCodeType();
                KeyCode.codeListVersion = "4.0";
                KeyCode.Value = "SSCC";

                getKeyLicensee partyname = new getKeyLicensee();
                partyname.getKeyLicensee1 = new GepirRequestedKeyType[]{ 
                    new GepirRequestedKeyType() { 
                        requestedKeyCode = KeyCode,
                        requestedKeyValue = parametro.campos.sscc 
                    }
                };
                     
                getKeyLicenseeResponse result = wsRouter.getKeyLicensee(partyname);
                    
                GepirRouterLOG("GetPartyBySSCC", parametro.campos, result, wsRouter.Url, wsRouter.requestHeader, wsRouter.responseHeader);

                foreach (PartyDataLineType item in result.gepirParty)
                {
                    List<ReturnCodeMessage> returnCodes = (List<ReturnCodeMessage>)this.getSession("RETURN_CODE_GEPIR");
                    bool has = returnCodes.Any(ret => ret.value.ToString() == item.returnCode.Value);
                    if (has)
                    {
                        ReturnCodeMessage returnCodeMessage = (from returncode in returnCodes
                                                                where returncode.value.ToString() == item.returnCode.Value
                                                                select returncode).First();
                        item.returnCode.Value = item.returnCode.Value + " - " + returnCodeMessage.textpt;
                    }
                }

                return JsonConvert.SerializeObject(result);
            }
            else
            {
                return String.Empty;
            }
           
        }

        [GS1CNPAttribute(false)]
        public string ConsultarWSFullItemByGTIN(dynamic parametro)
        {
            
            ComputarConsultaGepir();
            if (parametro != null && parametro.campos != null)
            {

                GS1.CNP.BLL.wsGepirRouter.router gepirbr = new GS1.CNP.BLL.wsGepirRouter.router();
                GS1.CNP.BLL.wsGepirRouter.gepirRequestHeader header = new GS1.CNP.BLL.wsGepirRouter.gepirRequestHeader();
                header.requesterGln = ConfigurationManager.AppSettings["SearchAllowedGLNTelasGEPIR"];
                header.cascade = int.Parse(ConfigurationManager.AppSettings["CascadeGEPIR"]);
                    
                gepirbr.gepirRequestHeaderValue = header;
                GS1.CNP.BLL.wsGepirRouter.FullItemResponse result = gepirbr.GetFullItemByGTIN(parametro.campos.gtin.ToString());

                GepirRouterLOG("GetFullItemByGTIN", parametro.campos, result, gepirbr.Url, gepirbr.gepirRequestHeaderValue, gepirbr.gepirResponseHeaderValue);

                if (result.Header.NumberOfHits > 0)
                {

                    // TODO errorcode
                    return JsonConvert.SerializeObject(result);


                }
                else
                {
                    List<ReturnCodeMessage> returnCodes = (List<ReturnCodeMessage>)this.getSession("RETURN_CODE_GEPIR");
                    bool has = returnCodes.Any(ret => ret.value.ToString() == gepirbr.gepirResponseHeaderValue.returnCode.ToString());
                    if (has)
                    {
                        ReturnCodeMessage returnCodeMessage = (from returncode in returnCodes
                                                               where returncode.value.ToString() == gepirbr.gepirResponseHeaderValue.returnCode.ToString()
                                                               select returncode).First();
                        return JsonConvert.SerializeObject(gepirbr.gepirResponseHeaderValue.returnCode.ToString() + " - " + returnCodeMessage.textpt);
                    }

                    return String.Empty;
                }

            }
            else
            {
                return String.Empty;
            }
            
        }

        [GS1CNPAttribute(false)]
        public string ConsultarWSItemByGTIN(dynamic parametro)
        {

            ComputarConsultaGepir();
            if (parametro != null && parametro.campos != null)
            {
                router wsRouter = new router();
                wsRouter = BuildHeader(wsRouter);

                getItemByGTIN partyname = new getItemByGTIN();
                partyname.getItemByGTIN1 = new GetItemByGTINType();
                partyname.getItemByGTIN1.requestedGTIN = new string[1];

                partyname.getItemByGTIN1.requestedGTIN[0] = parametro.campos.gtin;
                    
                getItemByGTINResponse result = wsRouter.getItemByGTIN(partyname);
                    
                GepirRouterLOG("GetItemByGTIN", parametro.campos, result, wsRouter.Url, wsRouter.requestHeader, wsRouter.responseHeader);

                foreach (ItemDataLineType item in result.gepirItem)
                {
                    List<ReturnCodeMessage> returnCodes = (List<ReturnCodeMessage>)this.getSession("RETURN_CODE_GEPIR");
                    bool has = returnCodes.Any(ret => ret.value.ToString() == item.returnCode.Value);
                    if (has)
                    {
                        ReturnCodeMessage returnCodeMessage = (from returncode in returnCodes
                                                                where returncode.value.ToString() == item.returnCode.Value
                                                                select returncode).First();
                        item.returnCode.Value = item.returnCode.Value + " - " + returnCodeMessage.textpt;
                    }
                }


                return JsonConvert.SerializeObject(result);
            }
            else
            {
                return String.Empty;
            }
            
        }

        [GS1CNPAttribute(false)]
        public string ConsultarWSOwnerOfGLN(dynamic parametro)
        {

            ComputarConsultaGepir();
            if (parametro != null && parametro.campos != null)
            {
                ValidateGLN(parametro.campos.gln.ToString());

                router wsRouter = new router();
                wsRouter = BuildHeader(wsRouter);

                RequestedKeyCodeType KeyCode = new RequestedKeyCodeType();
                KeyCode.codeListVersion = "4.0";
                KeyCode.Value = "GLN";

                getKeyLicensee partyname = new getKeyLicensee();
                partyname.getKeyLicensee1 = new GepirRequestedKeyType[]{ 
                    new GepirRequestedKeyType() { 
                        requestedKeyCode = KeyCode,
                        requestedKeyValue = parametro.campos.gln != null ? parametro.campos.gln : String.Empty
                    }
                };

                getKeyLicenseeResponse result = wsRouter.getKeyLicensee(partyname);
                    
                GepirRouterLOG("GetOwnerOfGLN", parametro.campos, result, wsRouter.Url, wsRouter.requestHeader, wsRouter.responseHeader);

                foreach (PartyDataLineType item in result.gepirParty)
                {
                    List<ReturnCodeMessage> returnCodes = (List<ReturnCodeMessage>)this.getSession("RETURN_CODE_GEPIR");
                    bool has = returnCodes.Any(ret => ret.value.ToString() == item.returnCode.Value);
                    if (has)
                    {
                        ReturnCodeMessage returnCodeMessage = (from returncode in returnCodes
                                                                where returncode.value.ToString() == item.returnCode.Value
                                                                select returncode).First();
                        item.returnCode.Value = item.returnCode.Value + " - " + returnCodeMessage.textpt;
                    }
                }

                return JsonConvert.SerializeObject(result);
            }
            else
            {
                return String.Empty;
            }            
        }


        #region GEPIR 4

        [GS1CNPAttribute(false)]
        public string getKeyLicensee(dynamic parametro)
        {

            ComputarConsultaGepir();
            if (parametro != null && parametro.campos != null)
            {
                router wsRouter = new router();
                wsRouter = BuildHeader(wsRouter);

                RequestedKeyCodeType KeyCode = new RequestedKeyCodeType();
                KeyCode.codeListVersion = "4.0";
                KeyCode.Value = parametro.campos.key;

                getKeyLicensee partyname = new getKeyLicensee();
                partyname.getKeyLicensee1 = new GepirRequestedKeyType[]{ 
                new GepirRequestedKeyType() { 
                    requestedKeyCode = KeyCode,
                    requestedKeyValue = parametro.campos.value 
                }
            };

                getKeyLicenseeResponse result = wsRouter.getKeyLicensee(partyname);
                    
                GepirRouterLOG("getKeyLicensee", parametro.campos, result, wsRouter.Url, wsRouter.requestHeader, wsRouter.responseHeader);

                foreach (PartyDataLineType item in result.gepirParty)
                {
                    List<ReturnCodeMessage> returnCodes = (List<ReturnCodeMessage>)this.getSession("RETURN_CODE_GEPIR");
                    bool has = returnCodes.Any(ret => ret.value.ToString() == item.returnCode.Value);
                    if (has)
                    {
                        ReturnCodeMessage returnCodeMessage = (from returncode in returnCodes
                                                                where returncode.value.ToString() == item.returnCode.Value
                                                                select returncode).First();
                        item.returnCode.Value = item.returnCode.Value + " - " + returnCodeMessage.textpt;
                    }
                }

                return JsonConvert.SerializeObject(result);
            }
            else
            {
                return String.Empty;
            }            

        }

        #endregion        

    }

}
