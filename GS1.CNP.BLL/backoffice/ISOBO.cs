﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("ISOBO")]
    public class ISOBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TIPOURL"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string consultarPaisesISO(dynamic parametro)
        {

            string sql = "SELECT DISTINCT CODIGO AS CODIGOISO, CODIGOPAIS, NOME AS PAIS, SIGLA2, SIGLA3 FROM [DBO].[COUNTRYOFORIGINISO3166] ORDER BY NOME";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
