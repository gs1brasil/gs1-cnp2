﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Configuration;
using System.Net;
using GS1.CNP.BLL.Model;
using System.Globalization;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("IdiomaBO")]
    public class IdiomaBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "IDIOMA"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarIdiomasAtivos(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM IDIOMA WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarIdiomas(dynamic parametro)
        {

            string sql = @"SELECT I.CODIGO, I.NOME, I.STATUS, U.NOME AS NOMEUSUARIO, I.DATAALTERACAO, I.DESCRICAO 
                            FROM IDIOMA I WITH (NOLOCK)
                            INNER JOIN USUARIO U ON U.CODIGO = I.CODIGOUSUARIOALTERACAO";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string PesquisarIdiomas(dynamic parametro)
        {

            string sql = @"SELECT I.CODIGO, I.NOME, I.STATUS, U.NOME AS NOMEUSUARIO, I.DATAALTERACAO, I.DESCRICAO 
                            FROM IDIOMA I WITH (NOLOCK)
                            INNER JOIN USUARIO U (Nolock)  ON U.CODIGO = I.CODIGOUSUARIOALTERACAO
                            WHERE 1 = 1 ";

            if (parametro.campos.nome != null)
            {
                sql += " AND I.NOME LIKE '%' + @nome + '%'";
            }

            if (parametro.campos.descricao != null)
            {
                sql += " AND I.DESCRICAO LIKE '%' + @descricao + '%'";
            }

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarModulosAtivos(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO 
                            FROM MODULO (Nolock) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarFormulariosAtivos(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM FORMULARIO
                            WHERE CODIGOMODULO = @modulo
                            AND STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarCampos(dynamic parametro)
        {

            string sql = @"SELECT DISTINCT C.CODIGO AS CODIGOCAMPO, C.NOME AS NOMECAMPO, CI.CODIGO AS CODIGOCAMPOIDIOMA, CI.TEXTO AS NOMETRADUCAO, CI.COMENTARIO
                            FROM CAMPO C (Nolock) 
                            INNER JOIN FORMULARIOCAMPO FC  (Nolock) ON FC.CODIGOCAMPO = C.CODIGO
                            INNER JOIN FORMULARIO F (Nolock)  ON F.CODIGO = FC.CODIGOFORMULARIO
                            INNER JOIN CAMPOIDIOMA CI  (Nolock) ON CI.CODIGOCAMPO = C.CODIGO
                            WHERE F.CODIGO = @codigoformulario
                            AND CI.CODIGOIDIOMA = @codigo";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string AlteraCampo(dynamic parametro)
        {

            string sql = @"UPDATE CAMPOIDIOMA SET TEXTO = @nometraducao, COMENTARIO = @comentario WHERE CODIGOCAMPO = @codigocampo AND CODIGO = @codigocampoidioma";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string AlteraCampoMenu(dynamic parametro)
        {

            if (parametro != null && parametro.campos != null)
            {
                string sql = String.Empty;

                if(parametro.campos.codigoformulario == null || parametro.campos.codigoformulario == "") {
                    sql = @"UPDATE MODULOIDIOMA SET NOME = @nometraducao, DESCRICAO = @comentario WHERE CODIGOMODULO = @codigomodulo AND CODIGOIDIOMA = @codigoidioma"; 
                }
                else {
                    sql = @"UPDATE FORMULARIOIDIOMA SET NOME = @nometraducao, DESCRICAO = @comentario WHERE CODIGOFORMULARIO = @codigoformulario AND CODIGOIDIOMA = @codigoidioma"; 
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return null;
            }
        }

        [GS1CNPAttribute(false)]
        public string IdiomasTraducao(dynamic parametro)
        {

            string sql = @"SELECT IT.CODIGO, IT.NOME, IT.SIGLA
                            FROM IDIOMATRADUCAO IT (Nolock) 
                            LEFT JOIN IDIOMA I  (Nolock) ON I.CODIGOIDIOMATRADUCAO = IT.CODIGO
                            WHERE IT.STATUS = 1
                            AND I.CODIGOIDIOMATRADUCAO IS NULL";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("CadastrarIdiomas")]
        public string TraduzirTextos(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                string sql_campo_idioma = @"SELECT CODIGO, TEXTO, COMENTARIO, CODIGOCAMPO 
                                FROM CAMPOIDIOMA (Nolock) 
                                WHERE STATUS = 1
                                AND CODIGOIDIOMA = 1";

                string sql_modulo_idioma = @"SELECT CODIGOMODULO, NOME AS TEXTO, DESCRICAO AS COMENTARIO  
	                            FROM MODULOIDIOMA (Nolock) 
	                            WHERE STATUS = 1
                                AND CODIGOIDIOMA = 1";

                string sql_formulario_idioma = @"SELECT CODIGOFORMULARIO, NOME AS TEXTO, DESCRICAO AS COMENTARIO 
	                            FROM FORMULARIOIDIOMA (Nolock) 
	                            WHERE STATUS = 1
                                AND CODIGOIDIOMA = 1";

                string sql_ajuda_idioma = @"SELECT CODIGOFORMULARIO, CODIGOTIPOAJUDA, NOME, CODIGOSTATUSPUBLICACAO, CODIGOIDIOMA, CODIGOUSUARIOALTERACAO, DATAALTERACAO
	                            FROM FORMULARIOAJUDAPASSOAPASSO (Nolock) 
	                            WHERE CODIGOIDIOMA = 1";

                string sql_faq_idioma = @"SELECT NOME, DESCRICAO, DATACADASTRO, CODIGOUSUARIOALTERACAO, CODIGOSTATUSPUBLICACAO, CODIGOIDIOMA, CODIGOSTATUSVISUALIZACAOFAQ 
	                            FROM FAQ (Nolock) 
	                            WHERE CODIGOIDIOMA = 1";

                using (Database db = new Database("BD"))
                {
                    db.Open(true);                    

                    try
                    {
                        dynamic param = null;

                        parametro.campos.codigousuarioalteracao = user.id.ToString();

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        dynamic campo_idioma = db.Select(sql_campo_idioma, param);
                        dynamic modulo_idioma = db.Select(sql_modulo_idioma, param);
                        dynamic formulario_idioma = db.Select(sql_formulario_idioma, param);

                        dynamic ajuda_idioma = db.Select(sql_ajuda_idioma, param);
                        dynamic faq_idioma = db.Select(sql_faq_idioma, param);

                        //Inserção na tabela IDIOMA
                        string sql_idioma = @"INSERT INTO IDIOMA (NOME, DESCRICAO, STATUS, DATAALTERACAO, CODIGOUSUARIOALTERACAO, CODIGOIDIOMATRADUCAO)
                                                            VALUES(@nome, @sigla, @status, GETDATE(), @codigousuarioalteracao, @idiomatraducao)";

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        long codigoIdioma = db.Insert(sql_idioma, param);

                        if (codigoIdioma != null)
                        {
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();

                            //Inserção na tabela CAMPOIDIOMA
                            for (var i = 0; i < campo_idioma.Count; i++)
                            {
                                string texto = GoogleTranslateAPIPost(campo_idioma[i].texto, parametro.campos.sigla.ToString());
                                string comentario = GoogleTranslateAPIPost(campo_idioma[i].comentario, parametro.campos.sigla.ToString());

                                sb.Append(@"INSERT INTO CAMPOIDIOMA (TEXTO, COMENTARIO, STATUS, CODIGOCAMPO, CODIGOIDIOMA)
                                                            VALUES ('" + texto + "', '" + comentario + "', 1, " + campo_idioma[i].codigocampo + ", " + codigoIdioma + ");");
                            }

                            //Inserção na tabela MODULOIDIOMA
                            for (var i = 0; i < modulo_idioma.Count; i++)
                            {
                                string texto = GoogleTranslateAPIPost(modulo_idioma[i].texto, parametro.campos.sigla.ToString());
                                string comentario = GoogleTranslateAPIPost(modulo_idioma[i].comentario, parametro.campos.sigla.ToString());

                                sb.Append(@"INSERT INTO MODULOIDIOMA (CODIGOMODULO, CODIGOIDIOMA, NOME, DESCRICAO, STATUS)
                                                            VALUES (" + modulo_idioma[i].codigomodulo + ", " + codigoIdioma + ", '" + texto + "', '" + comentario + "', 1);");
                            }

                            //Inserção na tabela FORMULARIOIDIOMA
                            for (var i = 0; i < formulario_idioma.Count; i++)
                            {
                                string texto = GoogleTranslateAPIPost(formulario_idioma[i].texto, parametro.campos.sigla.ToString());
                                string comentario = GoogleTranslateAPIPost(formulario_idioma[i].comentario, parametro.campos.sigla.ToString());

                                sb.Append(@"INSERT INTO FORMULARIOIDIOMA (CODIGOFORMULARIO, CODIGOIDIOMA, NOME, DESCRICAO, STATUS)
                                                            VALUES (" + formulario_idioma[i].codigoformulario + ", " + codigoIdioma + ", '" + texto + "', '" + comentario + "', 1);");
                            }

                            int maxCodeAjudaPassoaPasso = 0;

                            List<dynamic> retornoAjuda = db.Select("SELECT MAX(CODIGO) AS CODIGO FROM FORMULARIOAJUDAPASSOAPASSO");

                            if (retornoAjuda != null && retornoAjuda.Count > 0)
                                maxCodeAjudaPassoaPasso = retornoAjuda[0].codigo;

                            //Inserção na tabela FORMULARIOAJUDAPASSOAPASSO
                            for (var i = 0; i < ajuda_idioma.Count; i++)
                            {
                                maxCodeAjudaPassoaPasso++;

                                string nome = GoogleTranslateAPIPost(ajuda_idioma[i].nome, parametro.campos.sigla.ToString());

                                DateTime dataalteracao = DateTime.ParseExact(ajuda_idioma[i].dataalteracao.ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                                sb.Append(@"INSERT INTO FORMULARIOAJUDAPASSOAPASSO (CODIGO, CODIGOFORMULARIO, CODIGOTIPOAJUDA, NOME, CODIGOSTATUSPUBLICACAO, CODIGOIDIOMA, CODIGOUSUARIOALTERACAO, DATAALTERACAO)
                                                            VALUES (" + maxCodeAjudaPassoaPasso + ", " + ajuda_idioma[i].codigoformulario + ", " + ajuda_idioma[i].codigotipoajuda + ", '" + nome + "', 0, " + codigoIdioma + ", " + ajuda_idioma[i].codigousuarioalteracao + ", '" + dataalteracao.ToString("yyyy-MM-dd HH:mm:ss") + "');");
                            }

                            //Inserção na tabela FAQ
                            for (var i = 0; i < faq_idioma.Count; i++)
                            {
                                string nome = GoogleTranslateAPIPost(faq_idioma[i].nome, parametro.campos.sigla.ToString());
                                string descricao = GoogleTranslateAPIPost(faq_idioma[i].descricao, parametro.campos.sigla.ToString());

                                DateTime datacadastro = DateTime.ParseExact(faq_idioma[i].datacadastro.ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                                sb.Append(@"INSERT INTO FAQ (NOME, DESCRICAO, DATACADASTRO, CODIGOUSUARIOALTERACAO, CODIGOSTATUSPUBLICACAO, CODIGOIDIOMA, CODIGOSTATUSVISUALIZACAOFAQ)
                                                            VALUES ('" + nome + "', '" + descricao + "', '" + datacadastro.ToString("yyyy-MM-dd HH:mm:ss") + "', " + faq_idioma[i].codigousuarioalteracao + ", 0, " + codigoIdioma + ", " + faq_idioma[i].codigostatusvisualizacaofaq + ");");
                            }

                            db.BeginTransaction();

                            object camposTraduzidos = db.Select(sb.ToString(), null);

                            db.Commit();
                            return JsonConvert.SerializeObject(camposTraduzidos);
                        }
                        else
                        {
                            db.Rollback();
                            throw new GS1TradeException("Não foi possível cadastrar o idioma informado.");
                        }
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível cadastrar o idioma informado.", e);
                    }
                }

            }

            return null;
        }

        [GS1CNPAttribute(true, true, "EditarIdiomas", "VisualizarIdiomas")]
        public string AlterarStatus(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                //string sql = "UPDATE IDIOMA SET STATUS = @STATUS WHERE CODIGO = @CODIGO";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    parametro.campos.codigousuarioalteracao = user.id.ToString();

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Update("IDIOMA", "codigo", param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return null;
        }

        [GS1CNPAttribute(false)]
        public string GoogleTranslateAPIPost(string texto, string target)
        {
            string keyAPI = ConfigurationManager.AppSettings["GoogleTranslateAPI"];
            string url = String.Format("https://www.googleapis.com/language/translate/v2?key={0}&q={1}&source=pt&target={2}", keyAPI, texto, target);

            WebClient webClient = new WebClient();
            webClient.Encoding = System.Text.Encoding.UTF8;
            string result = webClient.DownloadString(url);

            dynamic jsonresult = JsonConvert.DeserializeObject(result);

            if (jsonresult != null && jsonresult.data != null && jsonresult.data.translations != null)
            {
                return jsonresult.data.translations[0].translatedText.ToString();
            }
            else
            {
                return null;
            }

        }

        [GS1CNPAttribute(false)]
        public string BuscarTraducao(dynamic parametro)
        {

//            string sql = @"SELECT  D.DESCRICAO AS NOMEIDIOMA, A.CODIGOFORMULARIO, B.NOME, C.TEXTO, C.COMENTARIO 
//                            FROM FORMULARIOCAMPO A
//                            INNER JOIN CAMPO B ON B.CODIGO = A.CODIGOCAMPO
//                            INNER JOIN CAMPOIDIOMA C ON C.CODIGOCAMPO = B.CODIGO
//                            INNER JOIN IDIOMA D ON D.CODIGO = C.CODIGOIDIOMA                            
//                            ORDER BY  D.DESCRICAO, A.CODIGOFORMULARIO ";


            string sql = @"SELECT B.DESCRICAO AS NOMEIDIOMA, 0 as CODIGOFORMULARIO, 'MODULO_' + CONVERT(VARCHAR(MAX),a.CodigoModulo) as Nome, a.nome as texto, a.descricao as comentario
                            FROM MODULOIDIOMA A (Nolock) 
                            INNER JOIN IDIOMA B (Nolock)  ON B.CODIGO = A.CODIGOIDIOMA
                            
                            UNION

                            SELECT B.DESCRICAO AS NOMEIDIOMA, 0 as CODIGOFORMULARIO, 'FORMULARIO_' + CONVERT(VARCHAR(MAX),a.CodigoFormulario) as Nome, a.nome as texto, a.descricao as comentario
                            FROM FORMULARIOIDIOMA A (Nolock) 
                            INNER JOIN IDIOMA B (Nolock)  ON B.CODIGO = A.CODIGOIDIOMA

                            UNION

                            SELECT D.Descricao AS NOMEIDIOMA, A.CODIGOFORMULARIO, B.NOME, C.TEXTO, C.COMENTARIO 
                            FROM FORMULARIOCAMPO A (Nolock) 
                            INNER JOIN CAMPO B (Nolock)  ON B.CODIGO = A.CODIGOCAMPO
                            INNER JOIN CAMPOIDIOMA C (Nolock)  ON C.CODIGOCAMPO = B.CODIGO
                            INNER JOIN IDIOMA D  (Nolock) ON D.CODIGO = C.CODIGOIDIOMA
                            ORDER BY NOMEIDIOMA,CODIGOFORMULARIO ";

           
            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = new List<dynamic>();
                List<dynamic> dados = db.Select(sql, null);

                var idiomas = dados.GroupBy(idioma => idioma.nomeidioma)
                                     .Select(grp => grp.First())
                                     .ToList();

                foreach (var idioma in idiomas)
                {
                    var traducaoidioma = (from traducao in dados
                                         where traducao.nomeidioma == idioma.nomeidioma
                                          select traducao).ToList();

                    retorno.Add(traducaoidioma);
                }


                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false, false)]
        public string BuscarTraducaoHome(dynamic parametro)
        {
            string sql = @"SELECT D.Descricao AS NOMEIDIOMA, A.CODIGOFORMULARIO, B.NOME, C.TEXTO, C.COMENTARIO 
                            FROM FORMULARIOCAMPO A (Nolock) 
                            INNER JOIN CAMPO B (Nolock)  ON B.CODIGO = A.CODIGOCAMPO
                            INNER JOIN CAMPOIDIOMA C (Nolock)  ON C.CODIGOCAMPO = B.CODIGO
                            INNER JOIN IDIOMA D  (Nolock) ON D.CODIGO = C.CODIGOIDIOMA
							WHERE A.CODIGOFORMULARIO = 36
                            ORDER BY NOMEIDIOMA,CODIGOFORMULARIO";


            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = new List<dynamic>();
                List<dynamic> dados = db.Select(sql, null);

                var idiomas = dados.GroupBy(idioma => idioma.nomeidioma)
                                     .Select(grp => grp.First())
                                     .ToList();

                foreach (var idioma in idiomas)
                {
                    var traducaoidioma = (from traducao in dados
                                          where traducao.nomeidioma == idioma.nomeidioma
                                          select traducao).ToList();

                    retorno.Add(traducaoidioma);
                }


                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarItensMenus(dynamic parametro)
        {

            string sql = @"SELECT B.CODIGO AS CODIGOIDIOMA, B.DESCRICAO AS NOMEIDIOMA, NULL as CODIGOFORMULARIO, M.CODIGO AS CODIGOMODULO, M.NOME AS NOME, A.NOME AS NOMETRADUCAO, A.DESCRICAO AS COMENTARIO
                            FROM MODULOIDIOMA A (Nolock)
                            INNER JOIN IDIOMA B (Nolock) ON B.CODIGO = A.CODIGOIDIOMA
	                        INNER JOIN MODULO M  (Nolock)ON M.CODIGO = A.CODIGOMODULO
                            WHERE B.CODIGO = @codigo ";

                    if (parametro != null && parametro.campos != null && parametro.campos.codigomodulomenu != null)
                        sql += " AND M.CODIGO = @codigomodulomenu";

                    sql += @" UNION

                            SELECT B.CODIGO AS CODIGOIDIOMA, B.DESCRICAO AS NOMEIDIOMA, F.CODIGO as CODIGOFORMULARIO, NULL AS CODIGOMODULO, F.NOME AS NOME, A.NOME AS TEXTO, A.DESCRICAO AS COMENTARIO
                            FROM FORMULARIOIDIOMA A (Nolock)
                            INNER JOIN IDIOMA B (Nolock) ON B.CODIGO = A.CODIGOIDIOMA
	                        INNER JOIN FORMULARIO F (Nolock) ON F.CODIGO = A.CODIGOFORMULARIO
                            WHERE B.CODIGO = @codigo ";

                    if (parametro != null && parametro.campos != null && parametro.campos.codigomodulomenu != null)
                        sql += " AND F.CODIGOMODULO = @codigomodulomenu";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

    }

}
