﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GS1.CNP.BLL.Core;
using System.Data;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("ImportacaoProdutoBO")]
    public class ImportacaoProdutoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "PRODUTO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }


    }
}
