﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using System.Web;
using System.IO;
using System.Data;
using System.Dynamic;
using GS1.CNP.BLL.Model;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("ImportarGPCBO")]
    public class ImportarGPCBO : CrudBO
    {
        private static String EVENT_SOURCE = "GS1.CNP.ImportarGPCBO";
        private static String EVENT_LOG = "Application";

        int countImportados = 0,
            countNaoImportados = 0,
            countCommits = 0,
            QTDE_COMMIT = 400;

        public override string nomeTabela
        {
            get { return string.Empty; }
        }

        public override string nomePK
        {
            get { return string.Empty; }
        }

        [GS1CNPAttribute("ListarGPC")]
        public object Listar(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro != null && parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> dados = null;
                List<dynamic> resultado = null;
                string sql = string.Empty,
                    nivel = param.id;

                if (nivel == "#")
                {
                    sql = @"SELECT * FROM TBSEGMENT";
                    resultado = db.Select(sql, param);

                    dados = (from s in resultado
                             select new
                             {
                                 id = "s" + s.codesegment,
                                 text = string.Format("Segmento: {0} - {1}", s.codesegment, s.text),
                                 state = new { opened = false },
                                 children = true
                             }).ToList<dynamic>();
                }
                else if (nivel != "#")
                {
                    if (nivel.StartsWith("s"))
                    {
                        sql = @"SELECT * FROM TBFAMILY WHERE CODESEGMENT = @segment";
                        param.segment = param.id.ToString().Substring(1);

                        resultado = db.Select(sql, param);

                        dados = (from f in resultado
                                 select new
                                 {
                                     id = "f" + f.codefamily,
                                     text = string.Format("Familia: {0} - {1}", f.codefamily, f.text),
                                     state = new { opened = false },
                                     children = true
                                 }).ToList<dynamic>();
                    }
                    else if (nivel.StartsWith("f"))
                    {
                        sql = @"SELECT * FROM TBCLASS WHERE CODESEGMENT = @segment AND CODEFAMILY = @family";
                        param.family = param.id.ToString().Substring(1);

                        resultado = db.Select(sql, param);

                        dados = (from c in resultado
                                 select new
                                 {
                                     id = "c" + c.codeclass,
                                     text = string.Format("Classe: {0} - {1}", c.codeclass, c.text),
                                     state = new { opened = false },
                                     children = true
                                 }).ToList<dynamic>();
                    }
                    else if (nivel.StartsWith("c"))
                    {
                        sql = @"SELECT * FROM TBBRICK WHERE CODESEGMENT = @segment AND CODEFAMILY = @family AND CODECLASS = @classes";
                        param.classes = param.id.ToString().Substring(1);

                        resultado = db.Select(sql, param);

                        dados = (from b in resultado
                                 select new
                                 {
                                     id = "b" + b.codebrick,
                                     text = string.Format("Bloco: {0} - {1}", b.codebrick, b.text),
                                     state = new { opened = false },
                                     children = true
                                 }).ToList<dynamic>();
                    }
                    else if (nivel.StartsWith("b"))
                    {
                        sql = @"SELECT E.CODEBRICK, E.CODECLASS, E.CODEFAMILY, E.CODESEGMENT, E.CODETYPE, F.IDIOMA, F.TEXT, F.DEFINITION
                                FROM TBBRICKTYPE E
                                INNER JOIN TBTTYPE F ON F.CODETYPE = E.CODETYPE
                                WHERE E.CODESEGMENT = @segment
                                AND E.CODEFAMILY = @family
                                AND E.CODECLASS = @classes
                                AND E.CODEBRICK = @brick";

                        param.brick = param.id.ToString().Substring(1);

                        resultado = db.Select(sql, param);

                        dados = (from a in resultado
                                 select new
                                 {
                                     id = "a" + a.codetype,
                                     text = string.Format("Atributo: {0} - {1}", a.codetype, a.text),
                                     state = new { opened = false },
                                     children = true
                                 }).ToList<dynamic>();
                    }
                    else if (nivel.StartsWith("a"))
                    {
                        sql = @"SELECT G.CODEBRICK, G.CODETYPE, G.IDIOMA, G.CODEVALUE, H.TEXT, H.DEFINITION
                                FROM TBTYPEVALUE G
                                INNER JOIN TBTVALUE H ON H.CODEVALUE = G.CODEVALUE
                                WHERE G.CODEBRICK = @brick
                                AND G.CODETYPE = @attribute";

                        param.attribute = param.id.ToString().Substring(1);

                        resultado = db.Select(sql, param);

                        dados = (from v in resultado
                                 select new
                                 {
                                     id = "v" + v.codevalue,
                                     text = string.Format("Valor: {0} - {1}", v.codevalue, v.text),
                                     state = new { opened = false },
                                     children = false
                                 }).ToList<dynamic>();
                    }
                }

                return dados;
            }
        }

        [GS1CNPAttribute("ListarGPC")]
        public object Pesquisar(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro != null && parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                string sql = @"SELECT A.CodeSegment, B.CodeFamily, C.CodeClass, D.CodeBrick, E.CodeType as CodeAttribute, G.CodeValue,
                               A.Text as TextSegment, B.Text as TextFamily, C.Text as TextClass,
                               D.Text as TextBrick, F.Text as TextAttribute, H.Text as TextValue
                               FROM tbSegment A
                               LEFT JOIN tbFamily B WITH(INDEX(IDX_TBFAMILY)) ON B.CodeSegment = A.CodeSegment AND B.Idioma = A.Idioma
                               LEFT JOIN tbClass C WITH(INDEX(IDX_TBCLASS)) ON C.CodeFamily = B.CodeFamily AND C.CodeSegment = B.CodeSegment AND C.Idioma = B.Idioma
                               LEFT JOIN tbBrick D WITH(INDEX(IDX_TBBRICK)) ON D.CodeClass = C.CodeClass AND D.CodeFamily = C.CodeFamily AND D.CodeSegment = C.CodeSegment AND D.Idioma = C.Idioma
                               LEFT JOIN tbBrickType E WITH(INDEX(IDX_TBBRICKTYPE)) ON D.CodeBrick = E.CodeBrick AND D.CodeClass = E.CodeClass AND D.CodeFamily = E.CodeFamily AND D.CodeSegment = E.CodeSegment AND D.Idioma = E.Idioma
                               LEFT JOIN tbTType F WITH(NOLOCK) ON F.CodeType = E.CodeType AND F.Idioma = E.Idioma
                               LEFT JOIN TBTypeVALUE G WITH(INDEX(IDX_TBTYPEVALUE)) ON G.CodeType = E.CodeType AND G.CodeBrick = E.CodeBrick AND G.Idioma = E.Idioma
                               LEFT JOIN tbTValue H WITH(NOLOCK) ON H.CodeValue = G.CodeValue AND H.Idioma = G.Idioma
                               WHERE A.Idioma = 'pt-br' 
                               AND (A.TEXT LIKE '%' + @text + '%'
                               OR B.TEXT LIKE '%' + @text + '%'
                               OR C.TEXT LIKE '%' + @text + '%'
                               OR D.TEXT LIKE '%' + @text + '%'
                               OR F.TEXT LIKE '%' + @text + '%'
                               OR H.TEXT LIKE '%' + @text + '%')";

                List<dynamic> resultado = db.Select(sql, param);

                var dados = (from s in resultado.Where(x => x.textsegment != string.Empty)
                                 .DistinctBy(x => new { x.codesegment, x.textsegment })
                             select new
                             {
                                 id = "s" + s.codesegment,
                                 text = string.Format("Segmento: {0} - {1}", s.codesegment, s.textsegment),
                                 state = new { opened = false },
                                 children = (from f in resultado.Where(x => x.codesegment.Equals(s.codesegment) &&
                                                                       x.textfamily != string.Empty)
                                                 .DistinctBy(x => new { x.codefamily, x.textfamily })
                                             select new
                                             {
                                                 id = "f" + f.codefamily,
                                                 text = string.Format("Familia: {0} - {1}", f.codefamily, f.textfamily),
                                                 state = new { opened = false },
                                                 children = (from c in resultado.Where(x => x.codesegment.Equals(s.codesegment) &&
                                                                                       x.codefamily.Equals(f.codefamily) &&
                                                                                       x.textclass != string.Empty)
                                                                 .DistinctBy(x => new { x.codeclass, x.textclass })
                                                             select new
                                                             {
                                                                 id = "c" + c.codeclass,
                                                                 text = string.Format("Classe: {0} - {1}", c.codeclass, c.textclass),
                                                                 state = new { opened = false },
                                                                 children = (from b in resultado.Where(x => x.codesegment.Equals(s.codesegment) &&
                                                                                                       x.codefamily.Equals(f.codefamily) &&
                                                                                                       x.codeclass.Equals(c.codeclass) &&
                                                                                                       x.textbrick != string.Empty)
                                                                                 .DistinctBy(x => new { x.codebrick, x.textbrick })
                                                                             select new
                                                                             {
                                                                                 id = "b" + b.codebrick,
                                                                                 text = string.Format("Bloco: {0} - {1}", b.codebrick, b.textbrick),
                                                                                 state = new { opened = false },
                                                                                 children = (from a in resultado.Where(x => x.codesegment.Equals(s.codesegment) &&
                                                                                                                       x.codefamily.Equals(f.codefamily) &&
                                                                                                                       x.codeclass.Equals(c.codeclass) &&
                                                                                                                       x.codebrick.Equals(b.codebrick) &&
                                                                                                                       x.textattribute != string.Empty)
                                                                                                 .DistinctBy(x => new { x.codeattribute, x.textattribute })
                                                                                             select new
                                                                                             {
                                                                                                 id = "a" + a.codeattribute,
                                                                                                 text = string.Format("Atributo: {0} - {1}", a.codeattribute, a.textattribute),
                                                                                                 state = new { opened = false },
                                                                                                 children = (from v in resultado.Where(x => x.codesegment.Equals(s.codesegment) &&
                                                                                                                                       x.codefamily.Equals(f.codefamily) &&
                                                                                                                                       x.codeclass.Equals(c.codeclass) &&
                                                                                                                                       x.codebrick.Equals(b.codebrick) &&
                                                                                                                                       x.codeattribute.Equals(a.codeattribute) &&
                                                                                                                                       x.textvalue != string.Empty)
                                                                                                                 .DistinctBy(x => new { x.codevalue, x.textvalue })
                                                                                                             select new
                                                                                                             {
                                                                                                                 id = "v" + v.codevalue,
                                                                                                                 text = string.Format("Valor: {0} - {1}", v.codevalue, v.textvalue),
                                                                                                                 state = new { opened = false },
                                                                                                                 children = false
                                                                                                             }).ToList<dynamic>()
                                                                                             }).ToList<dynamic>()
                                                                             }).ToList<dynamic>()
                                                             }).ToList<dynamic>()
                                             }).ToList<dynamic>()
                             }).ToList<dynamic>();
                /*
                var dados = (from s in resultado
                             select new
                             {
                                 text = string.Format("Segmento: {0} - {1}", s.codesegment, s.text),
                                 state = new { opened = false },
                                 children = (from f in familia.Where(x => x.codesegment.Equals(s.codesegment))
                                             select new
                                             {
                                                 text = string.Format("Familia: {0} - {1}", f.codefamily, f.text),
                                                 state = new { opened = false },
                                                 children = (from c in classe.Where(x => x.codesegment.Equals(s.codesegment) &&
                                                                 x.codefamily.Equals(f.codefamily))
                                                             select new
                                                             {
                                                                 text = string.Format("Classe: {0} - {1}", c.codeclass, c.text),
                                                                 state = new { opened = false },
                                                                 children = (from b in bloco.Where(x => x.codesegment.Equals(s.codesegment) &&
                                                                                x.codefamily.Equals(f.codefamily) &&
                                                                                x.codeclass.Equals(c.codeclass))
                                                                             select new
                                                                             {
                                                                                 text = string.Format("Bloco: {0} - {1}", b.codebrick, b.text),
                                                                                 state = new { opened = false },
                                                                                 children = (from a in atributo.Where(x => x.codesegment.Equals(s.codesegment) &&
                                                                                                x.codefamily.Equals(f.codefamily) &&
                                                                                                x.codeclass.Equals(c.codeclass) &&
                                                                                                x.codebrick.Equals(b.codebrick))
                                                                                             select new
                                                                                             {
                                                                                                 text = string.Format("Atributo: {0} - {1}", a.codetype, a.text),
                                                                                                 state = new { opened = false },
                                                                                                 children = (from v in valor.Where(x => x.codetype.Equals(a.codetype) &&
                                                                                                                 x.codebrick.Equals(b.codebrick))
                                                                                                             select new
                                                                                                             {
                                                                                                                 text = string.Format("Valor: {0} - {1}", v.codevalue, v.text),
                                                                                                                 state = new { opened = false }
                                                                                                             }).ToList<dynamic>()
                                                                                             }).ToList<dynamic>()
                                                                             }).ToList<dynamic>()
                                                             }).ToList<dynamic>()
                                             }).ToList<dynamic>()
                             }).ToList<dynamic>();
                 */

                return dados;
            }
        }

        [GS1CNPAttribute("PesquisarHIstoricoGPC")]
        public object ListarHistorico(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            using (Database db = new Database("BD"))
            {
                db.Open();

                //dynamic param = null;

                //if (parametro != null && parametro.where != null)
                //    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                string sql = @"SELECT	A.CODIGO,
		                                A.ARQUIVO,
		                                A.DATA,
		                                A.ITENSIMPORTADOS,
		                                A.ITENSIMPORTADOS + A.ITENSNAOIMPORTADOS AS ITENS,
		                                A.CODIGOIMPORTACAORESULTADO,
		                                B.DESCRICAO AS RESULTADO,
		                                A.CODIGOUSUARIOALTERACAO,
		                                C.EMAIL AS USUARIO,
		                                A.TEMPOIMPORTACAO
                                FROM	IMPORTACAO A  (Nolock)
                                INNER JOIN IMPORTACAORESULTADO B  (Nolock) ON A.CODIGOIMPORTACAORESULTADO = B.CODIGO
                                INNER JOIN USUARIO C  (Nolock) ON A.CODIGOUSUARIOALTERACAO = C.CODIGO
                                ORDER BY A.DATA DESC";

                List<dynamic> historico = db.Select(sql);

                return historico;
            }
        }

        [GS1CNPAttribute("ImportarGPC")]
        public object Importar(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            dynamic param = null;
            string arquivo = null;
            Login user = null;

            try
            {
                user = this.getSession("USUARIO_LOGADO") as Login;
                if (user != null)
                {
                    Log.WriteInfo("ImportarGPCBO", "Importar - Iniciado");

                    if (parametro != null && parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    arquivo = HttpContext.Current.Server.MapPath(param.arquivo);

                    Task.Factory.StartNew(() => ImportarArquivo(user, arquivo));
                    return "true";
                }
                else
                {
                    throw new GS1TradeSessionException();
                }
            }
            catch
            {
                if (!string.IsNullOrEmpty(arquivo) && user != null)
                {
                    DateTime tempoImportacao = new DateTime(1900, 1, 1, 0, 0, 0);
                    TimeSpan timeSpan = tempoImportacao - tempoImportacao;
                    return RegistrarImportacao(arquivo, DateTime.Now, 0, 0, 2, timeSpan, user.id);
                }

                return null;
            }
        }

        private object ImportarArquivo(Login user, string arquivo)
        {
            //TODO - Remover LOG
            try
            {
                if (!EventLog.SourceExists(EVENT_SOURCE))
                    EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                EventLog.WriteEntry(EVENT_SOURCE, string.Format("ImportarArquivo - Iniciado-> Parâmetros: user.email={0} arquivo={1}", user.email, arquivo), EventLogEntryType.Information, 1);
            }
            catch (Exception)
            {

            }

            DateTime dataInicial = DateTime.Now,
                dataFinal = DateTime.MinValue;
            TimeSpan tempoImportacao = DateTime.Now.Subtract(dataInicial);

            int? codigoImportacao = null;

            try
            {
                DataSet ds = new DataSet();

                codigoImportacao = RegistrarImportacao(arquivo, dataInicial, 0, 0, 0, tempoImportacao, user.id);

                ds.ReadXml(arquivo);

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables["segment"] != null)
                    {
                        //dynamic dados = null;

                        using (Database db = new Database("BD"))
                        {
                            int timeout = 0;

                            if (!Int32.TryParse(ConfigurationManager.AppSettings["timeoutGPC"], out timeout))
                                timeout = 3600;

                            db.CommandTimeout = timeout;
                            db.Open(true);

                            string commandCreate = @"create table #segment  (segment_id int, code varchar(10), [text] varchar(1500), [definition] varchar(max), [schema_id] int, PRIMARY KEY(segment_id), UNIQUE (segment_id));
                                                 create table #family   (family_id int, code varchar(10), [text] varchar(1500), [definition] varchar(max), segment_id int, PRIMARY KEY(family_id), UNIQUE (family_id));
                                                 create table #class    (class_id int, code varchar(10), [text] varchar(1500), [definition] varchar(max), family_id int, PRIMARY KEY(class_id), UNIQUE (class_id));
                                                 create table #brick    ([brick_id] int, code varchar(10), [text] varchar(1500), [definition] varchar(max), class_id int, PRIMARY KEY([brick_id]), UNIQUE ([brick_id]));
                                                 create table #attType  (attType_id int, code varchar(10), [text] varchar(1500), [definition] varchar(max), [brick_id] int, PRIMARY KEY(attType_id), UNIQUE (attType_id));
                                                 create table #attValue (code varchar(10), [text] varchar(1500), [definition] varchar(max), attType_id int);";

                            string commandDrop = @"drop table #segment;
                                               drop table #family;
                                               drop table #class;
                                               drop table #brick;
                                               drop table #attType;
                                               drop table #attValue;";

                            dataInicial = DateTime.Now;
                            Console.WriteLine("Data inicial: {0}", dataInicial);

                            // Criar tabelas temporárias
                            db.ExecuteCommand(commandCreate);

                            // Segmento
                            ImportarSegmento(ds.Tables["segment"], db);
                            Debug.WriteLine(string.Format("Segmento: {0}", db.Count("#segment")));

                            // Familia
                            ImportarFamilia(ds.Tables["family"], db);
                            Debug.WriteLine(string.Format("Familia: {0}", db.Count("#family")));

                            // Classe
                            ImportarClasse(ds.Tables["class"], db);
                            Debug.WriteLine(string.Format("Classe: {0}", db.Count("#class")));

                            // Bloco
                            ImportarBloco(ds.Tables["brick"], db);
                            Debug.WriteLine(string.Format("Bloco: {0}", db.Count("#brick")));

                            // Atributo
                            ImportarAtributo(ds.Tables["attType"], db);
                            Debug.WriteLine(string.Format("Atributo: {0}", db.Count("#atttype")));

                            // Valor
                            ImportarValor(ds.Tables["attValue"], db);
                            Debug.WriteLine(string.Format("Valor: {0}", db.Count("#attvalue")));

                            // Processar registros
                            db.ExecuteProcedure("PROC_IMPORTAR_SEGMENTO", null);

                            // Excluir tabelas temporárias
                            db.ExecuteCommand(commandDrop);

                            dataFinal = DateTime.Now;
                            Console.WriteLine("Data final: {0}", dataFinal);

                            if (dataFinal == DateTime.MinValue)
                                tempoImportacao = DateTime.Now.Subtract(dataInicial);
                            else
                                tempoImportacao = dataFinal.Subtract(dataInicial);

                            return RegistrarImportacao(arquivo, dataInicial, countImportados, countNaoImportados, 1, tempoImportacao, user.id, codigoImportacao);
                        }
                    }
                }

                if (codigoImportacao > 0)
                {
                    if (dataFinal == DateTime.MinValue)
                        tempoImportacao = DateTime.Now.Subtract(dataInicial);
                    else
                        tempoImportacao = dataFinal.Subtract(dataInicial);

                    return RegistrarImportacao(arquivo, dataInicial, countImportados, countNaoImportados, 2, tempoImportacao, user.id, codigoImportacao);
                }
            }
            catch (Exception ex)
            {

                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, string.Format("ImportarArquivo - Erro: {0} - Stack: {1}", ex.Message, ex.StackTrace.ToString()), EventLogEntryType.Error, 1);
                }
                catch (Exception)
                {

                }

                Log.WriteError(ex.Message, ex);

                if (dataFinal == DateTime.MinValue)
                    tempoImportacao = DateTime.Now.Subtract(dataInicial);
                else
                    tempoImportacao = dataFinal.Subtract(dataInicial);

                return RegistrarImportacao(arquivo, dataInicial, countImportados, countNaoImportados, 2, tempoImportacao, user.id, codigoImportacao);
            }

            return null;
        }

        protected void ImportarSegmento(DataTable dt, Database db)
        {
            if (dt != null && db != null)
            {
                dynamic dados = null;

                db.BeginTransaction();

                foreach (DataRow s in dt.Rows)
                {
                    try
                    {
                        dados = new ExpandoObject();
                        dados.segment_id = s["segment_id"];
                        dados.code = s["code"].ToString();
                        dados.Text = s["text"];
                        dados.Definition = s["definition"];
                        dados.schema_id = s["schema_id"];

                        db.Insert("#segment", string.Empty, dados, false);
                        countImportados++;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(string.Format("ERROR: {0}", ex.Message));
                        countNaoImportados++;
                    }

                    countCommits++;

                    if (countCommits > QTDE_COMMIT)
                    {
                        countCommits = 0;
                        db.Commit();

                        db.BeginTransaction();
                    }
                }

                db.Commit();
            }
        }

        protected void ImportarFamilia(DataTable dt, Database db)
        {
            if (dt != null && db != null)
            {
                dynamic dados = null;

                db.BeginTransaction();

                foreach (DataRow f in dt.Rows)
                {
                    try
                    {
                        dados = new ExpandoObject();
                        dados.family_id = f["family_id"].ToString();
                        dados.code = f["code"].ToString();
                        dados.Text = f["text"];
                        dados.Definition = f["definition"];
                        dados.segment_id = f["segment_id"];

                        db.Insert("#family", string.Empty, dados, false);
                        countImportados++;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(string.Format("ERROR: {0}", ex.Message));
                        countNaoImportados++;
                    }

                    countCommits++;

                    if (countCommits > QTDE_COMMIT)
                    {
                        countCommits = 0;
                        db.Commit();

                        db.BeginTransaction();
                    }
                }

                db.Commit();
            }
        }

        protected void ImportarClasse(DataTable dt, Database db)
        {
            if (dt != null && db != null)
            {
                dynamic dados = null;

                db.BeginTransaction();

                foreach (DataRow c in dt.Rows)
                {
                    try
                    {
                        dados = new ExpandoObject();
                        dados.class_id = c["class_id"].ToString();
                        dados.code = c["code"].ToString();
                        dados.Text = c["text"];
                        dados.Definition = c["definition"];
                        dados.family_id = c["family_id"].ToString();

                        db.Insert("#class", string.Empty, dados, false);
                        countImportados++;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(string.Format("ERROR: {0}", ex.Message));
                        countNaoImportados++;
                    }

                    countCommits++;

                    if (countCommits > QTDE_COMMIT)
                    {
                        countCommits = 0;
                        db.Commit();

                        db.BeginTransaction();
                    }
                }

                db.Commit();
            }
        }

        protected void ImportarBloco(DataTable dt, Database db)
        {
            if (dt != null && db != null)
            {
                dynamic dados = null;

                db.BeginTransaction();

                // Bloco
                foreach (DataRow b in dt.Rows)
                {
                    try
                    {
                        dados = new ExpandoObject();
                        dados.brick_id = b["brick_id"];
                        dados.code = b["code"].ToString();
                        dados.Text = b["text"];
                        dados.Definition = b["definition"];
                        dados.class_id = b["class_id"];

                        db.Insert("#brick", string.Empty, dados, false);
                        countImportados++;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(string.Format("ERROR: {0}", ex.Message));
                        Debug.WriteLine(string.Format("BRICK_CODE: {0}", b["code"]));
                        countNaoImportados++;
                    }

                    countCommits++;

                    if (countCommits > QTDE_COMMIT)
                    {
                        countCommits = 0;
                        db.Commit();

                        db.BeginTransaction();
                    }
                }

                db.Commit();
            }
        }

        protected void ImportarAtributo(DataTable dt, Database db)
        {
            if (dt != null && db != null)
            {
                dynamic dados = null;

                db.BeginTransaction();

                foreach (DataRow a in dt.Rows)
                {
                    try
                    {
                        dados = new ExpandoObject();
                        dados.attType_id = a["attType_id"];
                        dados.code = a["code"].ToString();
                        dados.Text = a["text"];
                        dados.Definition = a["definition"];
                        dados.brick_id = a["brick_id"];

                        db.Insert("#attType", string.Empty, dados, false);
                        countImportados++;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(string.Format("ERROR: {0}", ex.Message));
                        countNaoImportados++;
                    }

                    countCommits++;

                    if (countCommits > QTDE_COMMIT)
                    {
                        countCommits = 0;
                        db.Commit();

                        db.BeginTransaction();
                    }
                }

                db.Commit();
            }
        }

        protected void ImportarValor(DataTable dt, Database db)
        {
            if (dt != null && db != null)
            {
                dynamic dados = null;

                db.BeginTransaction();

                foreach (DataRow v in dt.Rows)
                {
                    try
                    {
                        dados = new ExpandoObject();
                        dados.code = v["code"].ToString();
                        dados.Text = v["text"];
                        dados.Definition = v["definition"];
                        dados.attType_id = v["attType_id"];

                        db.Insert("#attValue", string.Empty, dados, false);
                        countImportados++;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(string.Format("ERROR: {0}", ex.Message));
                        countNaoImportados++;
                    }

                    countCommits++;

                    if (countCommits > QTDE_COMMIT)
                    {
                        countCommits = 0;
                        db.Commit();

                        db.BeginTransaction();
                    }
                }

                db.Commit();
            }
        }

        protected int RegistrarImportacao(string arquivo,
            DateTime dataInicial,
            int itensImportados,
            int itensNaoImportados,
            int status,
            TimeSpan tempoImportacao,
            long codigoUsuarioAlteracao,
            int? codigo = null)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                FileInfo fileInfo = new FileInfo(arquivo);

                dynamic dados = new ExpandoObject();
                dados.Arquivo = fileInfo.Name;
                dados.Data = dataInicial;
                dados.ItensImportados = itensImportados;
                dados.ItensNaoImportados = itensNaoImportados;
                dados.CodigoImportacaoResultado = status;
                dados.TempoImportacao = tempoImportacao;
                dados.Status = 1;
                dados.CodigoUsuarioAlteracao = codigoUsuarioAlteracao;
                if (status != 0 && codigo != null)
                    dados.Codigo = codigo.Value;

                int codigoImportacao;

                if (status == 0)
                    return codigoImportacao = db.Insert("Importacao", "Codigo", dados);
                else
                {
                    codigoImportacao = db.Update("Importacao", "Codigo", dados);

                    return codigo.Value;
                }
            }
        }

        [GS1CNPAttribute(false)]
        public object VerificarProcessamentoPendente(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = new ExpandoObject();
                    param.usuario = user.id;

                    string sql = @"SELECT COUNT(0) AS EXISTEIMPORTACAOPENDENTE
                               FROM IMPORTACAO  (Nolock)
                               WHERE CODIGOIMPORTACAORESULTADO = 0
                               --AND CODIGOUSUARIOALTERACAO = @usuario";

                    List<dynamic> retorno = db.Select(sql, param);

                    return retorno.FirstOrDefault();
                }
            }
            else
                throw new GS1TradeSessionException();
        }
    }
}
