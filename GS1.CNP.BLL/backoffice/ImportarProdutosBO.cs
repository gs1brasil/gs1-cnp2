﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using System.Web;
using System.IO;
using System.Dynamic;
using GS1.CNP.BLL.Model;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Reflection;
using Newtonsoft.Json;
using System.Globalization;
using System.Configuration;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("ImportarProdutosBO")]
    public class ImportarProdutosBO : CrudBO, IDisposable
    {
        public override string nomeTabela
        {
            get { return string.Empty; }
        }

        public override string nomePK
        {
            get { return string.Empty; }
        }

        [GS1CNPAttribute(false)]
        public object BuscarModelos(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                return db.All("ImportacaoTipoItem");
            }
        }

        [GS1CNPAttribute(false)]
        public object ListarHistorico(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

                dynamic param = new ExpandoObject();
                param.associado = associado.codigo;

                string sql = @"SELECT A.CODIGO,
                               ISNULL(A.ARQUIVO, '-') AS ARQUIVO,
                               A.ARQUIVORELATORIO,
                               A.DATA,
                               A.ITENSIMPORTADOS,
                               A.ITENSNAOIMPORTADOS,
                               B.DESCRICAO AS RESULTADO,
                               C.DESCRICAO AS TIPOIMPORTACAO,
                               D.DESCRICAO AS TIPOITEM,
                               A.TEMPOIMPORTACAO,
                               E.EMAIL as usuario
                               FROM IMPORTACAOPRODUTOS A (Nolock)
                               INNER JOIN IMPORTACAORESULTADO B (Nolock) ON A.CODIGOIMPORTACAORESULTADO = B.CODIGO
                               INNER JOIN IMPORTACAOTIPO C  (Nolock) ON A.CODIGOIMPORTACAOTIPO = C.CODIGO
                               INNER JOIN IMPORTACAOTIPOITEM D  (Nolock) ON A.CODIGOIMPORTACAOTIPOITEM = D.CODIGO
                               INNER JOIN USUARIO E (Nolock) ON A.CODIGOUSUARIOALTERACAO = E.CODIGO
                               WHERE CodigoAssociado = @associado";

                List<dynamic> historico = db.Select(sql, param);

                return historico;
            }
        }

        [GS1CNPAttribute(false)]
        public object dominios(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                string listname = parametro.where.listname.ToString().ToLower(),
                    tabela = parametro.where.tablename.ToString().ToLower(),
                    codigo = parametro.where.code.ToString(),
                    descricao = parametro.where.description.ToString(),
                    condicao = parametro.where.condition.ToString(),
                    sql = "SELECT {0} AS CODIGO, {1} AS DESCRICAO FROM {2}";

                object valor = parametro.where.value;

                if (listname.Equals("estado") && tabela.Equals("tbstatecountry"))
                {
                    object[] value = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)valor) as object[];
                    dynamic param = new ExpandoObject();
                    param.nome = value[0];

                    dynamic country = db.Find("countryoforiginiso3166", param);

                    if (country != null)
                        sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " countrycode = '" + country.sigla3 + "'";

                    valor = null;
                }
                else
                {
                    if (tabela.Equals("statusgtin"))
                    {
                        sql += " WHERE Codigo <> 5";
                    }
                    else if (tabela.Equals("tbfamily"))
                    {
                        object[] value = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)valor) as object[];
                        dynamic param = new ExpandoObject();
                        param.text = value[0];

                        dynamic segment = db.Find("tbsegment", "CodeSegment + ' - ' + Text = @text", param);

                        if (segment != null)
                            sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " CodeSegment = " + segment.codesegment;

                        valor = null;
                    }
                    else if (tabela.Equals("tbclass"))
                    {
                        object[] value = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)valor) as object[];
                        dynamic param = new ExpandoObject();
                        param.text = value[0];

                        dynamic segment = db.Find("tbsegment", "CodeSegment + ' - ' + Text = @text", param);

                        if (segment != null)
                        {
                            sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " CodeSegment = " + segment.codesegment;

                            param = new ExpandoObject();
                            param.text = value[1];
                            param.codesegment = segment.codesegment;

                            dynamic family = db.Find("tbfamily", "codefamily + ' - ' + Text = @text AND codesegment = @codesegment", param);

                            if (family != null)
                                sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " codefamily = " + family.codefamily;
                        }
                        valor = null;
                    }
                    else if (tabela.Equals("tbbrick"))
                    {
                        object[] value = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)valor) as object[];
                        dynamic param = new ExpandoObject();
                        param.text = value[0];

                        dynamic segment = db.Find("tbsegment", "CodeSegment + ' - ' + Text = @text", param);

                        if (segment != null)
                        {
                            sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " CodeSegment = " + segment.codesegment;

                            param = new ExpandoObject();
                            param.text = value[1];
                            param.codesegment = segment.codesegment;

                            dynamic family = db.Find("tbfamily", "codefamily + ' - ' + Text = @text AND codesegment = @codesegment", param);

                            if (family != null)
                            {
                                sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " codefamily = " + family.codefamily;

                                param = new ExpandoObject();
                                param.text = value[2];
                                param.codesegment = segment.codesegment;
                                param.codefamily = family.codefamily;

                                dynamic classes = db.Find("tbclass", "codeclass + ' - ' + Text = @text AND codesegment = @codesegment AND codefamily = @codefamily", param);

                                if (classes != null)
                                    sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " codeclass = " + classes.codeclass;
                            }
                        }
                        valor = null;
                    }
                    else if (tabela.Equals("tbstatecountry"))
                    {
                        object[] value = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)valor) as object[];
                        dynamic param = new ExpandoObject();
                        param.countryname = value[0];

                        dynamic country = db.Find("tbcountry", param);

                        if (country != null)
                            sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " countrycode = '" + country.countrycode + "'";

                        valor = null;
                    }
                }

                if (valor != null && !string.IsNullOrEmpty(valor.ToString()))
                    sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " {1} LIKE '%" + valor + "%' ";

                if (!string.IsNullOrEmpty(condicao))
                    sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + condicao;

                if (tabela.Equals("tbfamily") || tabela.Equals("tbclass") || tabela.Equals("tbbrick"))
                    sql += (sql.ToUpper().Contains(" ORDER BY ") ? ", TEXT ASC" : " ORDER BY TEXT ASC");

                return db.Select(string.Format(sql, codigo, descricao, tabela));
            }
        }

        private object dominios(string listname, string tabela, string codigo, string descricao, string condicao, string valor = "")
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                string sql = "SELECT {0} AS CODIGO, {1} AS DESCRICAO FROM {2} (Nolock)";

                if (listname.Equals("estado") && tabela.Equals("tbstatecountry"))
                {
                    tabela += @" A INNER JOIN COUNTRYOFORIGINISO3166 C ON C.SIGLA3 = A.COUNTRYCODE";
                }
                else
                {
                    if (tabela.Equals("statusgtin"))
                        sql += " WHERE Codigo <> 5";
                }

                if (!string.IsNullOrEmpty(valor))
                    sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + " {1} LIKE '%" + valor + "%' ";

                if (!string.IsNullOrEmpty(condicao))
                    sql += (sql.ToUpper().Contains(" WHERE ") ? " AND " : " WHERE ") + condicao;

                return db.Select(string.Format(sql, codigo, descricao, tabela));
            }
        }

        //TODO - colocar permissão correta
        [GS1CNPAttribute(false, true)]
        public void Download(int codigo, int tipoModelo, int padraoGDSN)
        {
            if (codigo > 0 && tipoModelo > 0 && padraoGDSN > 0)
            {
                HttpServerUtility Server = HttpContext.Current.Server;

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = new ExpandoObject();
                    param.codigo = codigo;
                    param.tipoModelo = tipoModelo;

                    List<dynamic> retorno = db.Select("SELECT * FROM IMPORTACAOMODELOS WHERE CODIGOMODELOTIPO = @tipoModelo AND CodigoImportacaoTipoItem = @codigo", param);

                    if (retorno != null && retorno.Count > 0)
                    {
                        dynamic ret = retorno.FirstOrDefault();
                        string path = string.Format("/templates/{0}", ret.arquivo);
                        string contentType = (tipoModelo == 1 ? "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" : "text/csv");
                        string arquivo = Server.MapPath(path);

                        if (File.Exists(arquivo))
                        {
                            string extension = Path.GetExtension(arquivo);

                            if (padraoGDSN == 1)
                                arquivo = arquivo.Replace(extension, "-GDSN" + extension);

                            if (File.Exists(arquivo))
                            {
                                byte[] bytes = System.IO.File.ReadAllBytes(arquivo);

                                HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });

                                HttpContext.Current.Response.Clear();
                                HttpContext.Current.Response.AddHeader("Content-Type", contentType);
                                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + new FileInfo(arquivo).Name);
                                HttpContext.Current.Response.AddHeader("Content-Length", bytes.Length.ToString());
                                HttpContext.Current.Response.AddHeader("Cache-Control", "max-age=0");
                                HttpContext.Current.Response.AddHeader("Accept-Ranges", "none");
                                HttpContext.Current.Response.BinaryWrite(bytes);

                                HttpContext.Current.Response.Flush();
                                HttpContext.Current.Response.End();
                            }
                            else
                                throw new GS1TradeException("Arquivo de template não encontrado.");
                        }
                        else
                            throw new GS1TradeException("Arquivo de template não encontrado.");
                    }
                }
            }
        }

        //TODO - colocar permissão correta
        [GS1CNPAttribute(false, true)]
        public object LerArquivo(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                dynamic param = null,
                    dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml"));

                if (parametro != null && parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                string arquivo = HttpContext.Current.Server.MapPath(param.arquivo),
                    templatePath = string.Empty;

                List<dynamic> dicionario = null;
                List<dynamic> lista = null;
                int tipoArquivo = -1,
                    template = 0,
                    padraoGDSN = 0,
                    qtdeLinhasPermitidas = 0;

                dynamic retorno = new ExpandoObject();
                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

                bool existeErro = false;

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ImportacaoQuantidadeLinhasPermitidas"]) && Core.Util.IsInteger(ConfigurationManager.AppSettings["ImportacaoQuantidadeLinhasPermitidas"]))
                    qtdeLinhasPermitidas = Convert.ToInt32(ConfigurationManager.AppSettings["ImportacaoQuantidadeLinhasPermitidas"]);

                if (param.tipoitem != null && !string.IsNullOrEmpty(param.tipoitem.ToString()))
                {
                    tipoArquivo = Convert.ToInt32(param.tipoitem);

                    tipoArquivo = RecuperarTipoArquivoPorID(tipoArquivo);
                }

                if (tipoArquivo != 0 && tipoArquivo != 12 && tipoArquivo != 13 && tipoArquivo != 14)
                    throw new GS1TradeException("Template do arquivo não identificado.");

                if (Path.GetExtension(arquivo).ToLower() == ".xlsx")
                {
                    padraoGDSN = RecuperarPadraoGDSN_XLS(arquivo);

                    if (tipoArquivo > 0)
                        template = (tipoArquivo * 10) + padraoGDSN;
                    else
                        template = tipoArquivo;

                    dicionario = RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                    templatePath = HttpContext.Current.Server.MapPath(string.Format("/templates/{0}{1}", (tipoArquivo == 0 ? "GLN" : "GTIN-" + tipoArquivo + (padraoGDSN == 1 ? "-GDSN" : string.Empty)), Path.GetExtension(arquivo)));

                    lista = LerArquivoXLS(dicionario, arquivo, templatePath, tipoArquivo, padraoGDSN, template, associado, qtdeLinhasPermitidas, ref existeErro) as List<dynamic>;
                }
                else
                {
                    padraoGDSN = RecuperarPadraoGDSN_CSV(arquivo);

                    if (tipoArquivo > 0)
                        template = (tipoArquivo * 10) + padraoGDSN;
                    else
                        template = tipoArquivo;

                    dicionario = RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                    templatePath = HttpContext.Current.Server.MapPath(string.Format("/templates/{0}{1}", (tipoArquivo == 0 ? "GLN" : "GTIN-" + tipoArquivo + (padraoGDSN == 1 ? "-GDSN" : string.Empty)), Path.GetExtension(arquivo)));

                    lista = LerArquivoCSV(dicionario, arquivo, templatePath, tipoArquivo, padraoGDSN, template, associado, qtdeLinhasPermitidas, ref existeErro) as List<dynamic>;
                }

                retorno.tipoitem = RecuperarTipoItem(tipoArquivo);
                retorno.tipoArquivo = tipoArquivo;
                retorno.arquivo = new FileInfo(arquivo).Name;
                retorno.padraoGDSN = padraoGDSN;
                retorno.gdsn = (padraoGDSN == 1 ? "Sim" : "Não");
                if (lista != null && lista.Count > 0)
                    retorno.lista = lista.OrderByDescending(x => x.errors != null && (x.errors as Dictionary<string, string>).Count > 0);
                else
                    throw new GS1TradeException("Arquivo vazio. Favor inserir registros antes de importar, ou utilize a Importação Manual.");

                retorno.colunas = (from dynamic d in dicionario
                                   where !d.nome.ToLower().Equals("indicadorgdsn")
                                   select new
                                   {
                                       field = d.nome.ToLower(),
                                       title = d.coluna,
                                       table = d.tabela.ToLower(),
                                       column = d.campo.ToLower(),
                                       type = d.tipo.ToLower(),
                                       required = (d.obrigatorio as string).Split(',').Contains(template.ToString()),
                                       listname = d.nomeitemlista.ToLower(),
                                       tablereference = d.tabelareferencia.ToLower(),
                                       fieldreference = d.camporeferencia.ToLower(),
                                       descriptionreference = d.campodescricao.ToLower(),
                                       condition = d.condicao.ToLower(),
                                       show = true
                                   });
                retorno.existeErro = existeErro;

                return retorno;
            }
            else
                throw new GS1TradeSessionException();
        }

        [GS1CNPAttribute(true, true, "ImportarProduto", "VisualizarImportarProduto")]
        public object Importar(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            DateTime dataInicial = DateTime.Now,
                dataFinal = DateTime.MinValue;
            TimeSpan tempoImportacao = TimeSpan.MinValue;

            Login user = this.getSession("USUARIO_LOGADO") as Login;
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            if (user != null)
            {
                if (parametro != null)
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        if (parametro.campos is JToken)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos) as dynamic;
                        else
                            param = parametro.campos;
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        if (param != null && param.lista.Length > 0)
                        {
                            int codigoProduto = 0,
                                itensNaoImportados = 0,
                                tipoArquivo = RecuperarTipoArquivoPorID(Convert.ToInt32(param.tipoArquivo)),
                                padraoGDSN = Convert.ToInt32(param.padraoGDSN),
                                template = (tipoArquivo == 0 ? 0 : (tipoArquivo * 10) + padraoGDSN);

                            dynamic dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml"));

                            List<dynamic> dicionario = RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                            dynamic[] lista = param.lista as dynamic[];

                            string arquivoLog = string.Format("log_importacao_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd_HHmmss")),
                                ultimoGTIN = string.Empty;

                            if (!Directory.Exists(HttpContext.Current.Server.MapPath("/relatorios")))
                                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("/relatorios"));

                            using (ExcelPackage pck = new ExcelPackage(new FileInfo(HttpContext.Current.Server.MapPath(string.Format("/relatorios/{0}", arquivoLog)))))
                            {
                                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Relatório");

                                AdicionaCabecalho(ws, dicionario, template);

                                if (tipoArquivo != 0)
                                    lista = lista.OrderByDescending(x => x.gtin != null && !string.IsNullOrEmpty(x.gtin.ToString())).ToArray<dynamic>();

                                foreach (dynamic d in lista)
                                {
                                    if (ValidarRegistro(db, dicionario, d, associado, tipoArquivo, template))
                                    {
                                        d.errors = null;

                                        if (tipoArquivo > 0)
                                        {
                                            if (d.gtin != null && !string.IsNullOrEmpty(d.gtin.ToString()))
                                                ultimoGTIN = d.gtin.ToString();

                                            codigoProduto = ImportarRegistroProduto(db, dicionario, d, tipoArquivo, RecuperarTipoGTIN(Convert.ToInt32(tipoArquivo)), padraoGDSN == 1, user, associado, ultimoGTIN);

                                            ImportarRegistroProdutoHierarquia(db, dicionario, d, codigoProduto, associado);

                                            ImportarRegistroProdutoURL(db, dicionario, d, codigoProduto);

                                            ImportarRegistroProdutoAgenciaReguladora(db, dicionario, d, codigoProduto);
                                        }
                                        else
                                        {
                                            if (d.gln != null && !string.IsNullOrEmpty(d.gln.ToString()))
                                                ultimoGTIN = d.gln.ToString();

                                            ImportarRegistroLocalizacaoFisica(db, dicionario, d, tipoArquivo, user, associado, ultimoGTIN);
                                        }
                                    }
                                    else
                                        itensNaoImportados++;
                                }

                                AdicionaErros(db, ws, dicionario, lista, padraoGDSN == 1);

                                pck.Save();
                            }

                            dataFinal = DateTime.Now;

                            tempoImportacao = dataFinal.Subtract(dataInicial);

                            int codigo = RegistrarImportacao(db, param.arquivo.ToString(), arquivoLog, dataInicial, lista.Length - itensNaoImportados, itensNaoImportados, 1, Convert.ToInt32(param.tipo), RecuperarIDPorTipoArquivo(tipoArquivo), tempoImportacao, user.id, associado);

                            if (codigo > 0)
                            {
                                string sql = @"SELECT A.CODIGO,
                                               A.ARQUIVORELATORIO,
                                               A.DATA,
                                               A.ITENSIMPORTADOS,
                                               B.DESCRICAO AS RESULTADO,
                                               C.DESCRICAO AS TIPOIMPORTACAO,
                                               D.DESCRICAO AS TIPOITEM,
                                               A.TEMPOIMPORTACAO,
                                               E.EMAIL AS USUARIO
                                               FROM IMPORTACAOPRODUTOS A
                                               INNER JOIN IMPORTACAORESULTADO B ON A.CODIGOIMPORTACAORESULTADO = B.CODIGO
                                               INNER JOIN IMPORTACAOTIPO C ON A.CODIGOIMPORTACAOTIPO = C.CODIGO
                                               INNER JOIN IMPORTACAOTIPOITEM D ON A.CODIGOIMPORTACAOTIPOITEM = D.CODIGO
                                               INNER JOIN USUARIO E ON A.CODIGOUSUARIOALTERACAO = E.CODIGO
                                               WHERE A.CODIGO = @codigo";

                                param = new ExpandoObject();
                                param.codigo = codigo;

                                List<dynamic> retorno = db.Select(sql, param);

                                if (retorno != null && retorno.Count > 0)
                                    return retorno[0];
                            }
                        }
                    }

                    return null;
                }
                else
                    throw new GS1TradeException("Não foram encontrados registros a serem importados.");
            }
            else
                throw new GS1TradeSessionException();
        }

        public object ImportarProdutosParaAprovacao(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            if (user != null)
            {
                if (parametro != null)
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        if (parametro.campos is JToken)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos) as dynamic;
                        else
                            param = parametro.campos;
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        if (param != null && param.lista.Length > 0)
                        {
                            string sql = @" SET NOCOUNT ON;
                                            DELETE FROM AprovacaoProduto WHERE CODIGOASSOCIADO = @associado;
                                            SET NOCOUNT OFF;
                                            INSERT INTO AprovacaoProduto (CODIGOASSOCIADO, GDSN, CODIGOTIPOARQUIVO, tipoAcao)
                                            VALUES (@associado, @padraoGDSN, @tipoArquivo, @tipoAcao)";

                            int codigo = db.Insert(sql, param);

                            if (codigo > 0)
                            {
                                foreach (dynamic item in param.lista)
                                {
                                    item.codigoaprovacao = codigo;

                                    if (((IDictionary<string, object>)item).ContainsKey("errors"))
                                    {
                                        ((IDictionary<string, object>)item).Remove("errors");
                                    }

                                    db.Insert("ProdutoTemp", string.Empty, item, false);
                                }

                                dynamic paramAprovacao = new ExpandoObject();
                                paramAprovacao.codigoassociado = associado.codigo;

                                return db.Find("AprovacaoProduto", "CODIGOASSOCIADO = @codigoassociado", paramAprovacao);
                            }
                        }
                    }
                }
            }

            return null;
        }

        public object ImportarLocalizacaoFisicaParaAprovacao(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                if (parametro != null)
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        if (parametro.campos is JToken)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos) as dynamic;
                        else
                            param = parametro.campos;
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        if (param != null && param.lista.Length > 0)
                        {
                            string sql = @" SET NOCOUNT ON;
                                            DELETE FROM AprovacaoProduto WHERE CODIGOASSOCIADO = @associado;
                                            SET NOCOUNT OFF;
                                            INSERT INTO AprovacaoProduto (CODIGOASSOCIADO, GDSN, CODIGOTIPOARQUIVO, tipoAcao)
                                            VALUES (@associado, @padraoGDSN, @tipoArquivo, @tipoAcao)";

                            int codigo = db.Insert(sql, param);

                            if (codigo > 0)
                            {
                                foreach (dynamic item in param.lista)
                                {
                                    item.codigoaprovacao = codigo;

                                    if (((IDictionary<string, object>)item).ContainsKey("errors"))
                                    {
                                        ((IDictionary<string, object>)item).Remove("errors");
                                    }

                                    db.Insert("LocalizacaoFisicaTemp", string.Empty, item, false);
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }

        [GS1CNPAttribute(true, true, "ImportarProduto", "VisualizarImportarProduto")]
        public object ImportarAtualizacao(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            DateTime dataInicial = DateTime.Now,
                dataFinal = DateTime.MinValue;
            TimeSpan tempoImportacao = TimeSpan.MinValue;

            Login user = this.getSession("USUARIO_LOGADO") as Login;
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            if (user != null)
            {
                if (parametro != null)
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        if (parametro.campos is JToken)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos) as dynamic;
                        else
                            param = parametro.campos;
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        if (param != null && param.lista.Length > 0)
                        {
                            int codigoProduto = 0,
                                itensNaoImportados = 0,
                                tipoArquivo = RecuperarTipoArquivoPorID(Convert.ToInt32(param.tipoArquivo)),
                                padraoGDSN = Convert.ToInt32(param.padraoGDSN),
                                template = (tipoArquivo == 0 ? 0 : (tipoArquivo * 10) + padraoGDSN);

                            dynamic dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml"));

                            List<dynamic> dicionario = RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                            dynamic[] lista = param.lista as dynamic[];

                            foreach (dynamic d in lista)
                            {
                                if (ValidarRegistro(db, dicionario, d, associado, tipoArquivo, template, false))
                                {
                                    d.errors = null;

                                    codigoProduto = AtualizarRegistroProduto(db, dicionario, d, tipoArquivo, RecuperarTipoGTIN(Convert.ToInt32(tipoArquivo)), padraoGDSN == 1, user, associado);

                                    db.Delete("DELETE FROM PRODUTOHIERARQUIA WHERE CodigoProdutoSuperior=" + codigoProduto.ToString());
                                    ImportarRegistroProdutoHierarquia(db, dicionario, d, codigoProduto, associado);

                                    db.Delete("DELETE FROM PRODUTOURL WHERE CODIGO=" + codigoProduto.ToString());
                                    ImportarRegistroProdutoURL(db, dicionario, d, codigoProduto);

                                    db.Delete("DELETE FROM PRODUTOAGENCIAREGULADORA WHERE CODIGOPRODUTO=" + codigoProduto.ToString());
                                    ImportarRegistroProdutoAgenciaReguladora(db, dicionario, d, codigoProduto);
                                }
                                else
                                    itensNaoImportados++;
                            }

                            dataFinal = DateTime.Now;

                            tempoImportacao = dataFinal.Subtract(dataInicial);

                            int codigo = RegistrarImportacao(db, param.arquivo.ToString(), "WebService", dataInicial, lista.Length - itensNaoImportados, itensNaoImportados, 1, Convert.ToInt32(param.tipo), RecuperarIDPorTipoArquivo(tipoArquivo), tempoImportacao, user.id, associado);

                            if (codigo > 0)
                            {
                                string sql = @"SELECT A.CODIGO,
                                               A.ARQUIVORELATORIO,
                                               A.DATA,
                                               A.ITENSIMPORTADOS,
                                               B.DESCRICAO AS RESULTADO,
                                               C.DESCRICAO AS TIPOIMPORTACAO,
                                               D.DESCRICAO AS TIPOITEM,
                                               A.TEMPOIMPORTACAO,
                                               E.EMAIL AS USUARIO
                                               FROM IMPORTACAOPRODUTOS A (NOlock) 
                                               INNER JOIN IMPORTACAORESULTADO B (NOlock)  ON A.CODIGOIMPORTACAORESULTADO = B.CODIGO
                                               INNER JOIN IMPORTACAOTIPO C (NOlock)  ON A.CODIGOIMPORTACAOTIPO = C.CODIGO
                                               INNER JOIN IMPORTACAOTIPOITEM D  (NOlock) ON A.CODIGOIMPORTACAOTIPOITEM = D.CODIGO
                                               INNER JOIN USUARIO E (NOlock)  ON A.CODIGOUSUARIOALTERACAO = E.CODIGO
                                               WHERE A.CODIGO = @codigo";

                                param = new ExpandoObject();
                                param.codigo = codigo;

                                List<dynamic> retorno = db.Select(sql, param);

                                if (retorno != null && retorno.Count > 0)
                                    return retorno[0];
                            }
                        }
                    }

                    return null;
                }
                else
                    throw new GS1TradeException("Não foram encontrados registros a serem importados.");
            }
            else
                throw new GS1TradeSessionException();
        }

        [GS1CNPAttribute(false)]
        public object Validar(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                if (parametro != null)
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        if (parametro.campos is JToken)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos) as dynamic;
                        else
                            param = parametro.campos;
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        if (param != null && param.lista.Length > 0)
                        {
                            dynamic retorno = new ExpandoObject(),
                                dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml"));

                            int tipoArquivo = Convert.ToInt32(param.tipoArquivo),
                                padraoGDSN = Convert.ToInt32(param.padraoGDSN),
                                template = (tipoArquivo == 0 ? 0 : (tipoArquivo * 10) + padraoGDSN),
                                qtdeLinhasPermitidas = 0;
                            List<dynamic> dicionario = RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                            dynamic[] lista = param.lista;

                            bool existeErro = false;
                            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ImportacaoQuantidadeLinhasPermitidas"]) && Core.Util.IsInteger(ConfigurationManager.AppSettings["ImportacaoQuantidadeLinhasPermitidas"]))
                                qtdeLinhasPermitidas = Convert.ToInt32(ConfigurationManager.AppSettings["ImportacaoQuantidadeLinhasPermitidas"]);

                            if (lista.Length <= qtdeLinhasPermitidas)
                            {
                                foreach (dynamic d in lista)
                                {
                                    if (!ValidarRegistro(db, dicionario, d, associado, tipoArquivo, template))
                                        existeErro = true;
                                }

                                retorno.lista = lista.OrderByDescending(x => (x as IDictionary<string, object>).ContainsKey("errors") && x.errors != null && (x.errors as Dictionary<string, string>).Count > 0);
                                retorno.existeErro = existeErro;

                                return retorno;
                            }
                            else
                                throw new GS1TradeException(string.Format("Número máximo de linhas por arquivo foi excedido. É permitido no máximo {0} linhas.", qtdeLinhasPermitidas));
                        }
                    }
                    return null;
                }
                else
                    throw new GS1TradeException("Não foram encontrados registros a serem importados.");
            }
            else
                throw new GS1TradeSessionException();
        }

        public object ValidarAtualizacao(dynamic parametro, bool acaoInserir = true)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                if (parametro != null)
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        if (parametro.campos is JToken)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos) as dynamic;
                        else
                            param = parametro.campos;
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        if (param != null && param.lista.Length > 0)
                        {
                            dynamic retorno = new ExpandoObject(),
                                dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml"));

                            int tipoArquivo = Convert.ToInt32(param.tipoArquivo),
                                padraoGDSN = Convert.ToInt32(param.padraoGDSN),
                                template = (tipoArquivo == 0 ? 0 : (tipoArquivo * 10) + padraoGDSN);
                            List<dynamic> dicionario = RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                            dynamic[] lista = param.lista;

                            bool existeErro = false;
                            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

                            foreach (dynamic d in lista)
                            {
                                if (!ValidarRegistro(db, dicionario, d, associado, tipoArquivo, template, acaoInserir))
                                    existeErro = true;
                            }

                            retorno.lista = lista.OrderByDescending(x => (x as IDictionary<string, object>).ContainsKey("errors") && x.errors != null && (x.errors as Dictionary<string, string>).Count > 0);
                            retorno.existeErro = existeErro;

                            return retorno;
                        }
                    }
                    return null;
                }
                else
                    throw new GS1TradeException("Não foram encontrados registros a serem importados.");
            }
            else
                throw new GS1TradeSessionException();
        }

        [GS1CNPAttribute(false)]
        public object ValidarTemplate(dynamic parametro)
        {
            dynamic param = null;

            if (parametro.campos != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos) as dynamic;

            if (param != null && param.tipoArquivo != null)
            {
                int tipoArquivo = RecuperarTipoArquivoPorID(Convert.ToInt32(param.tipoArquivo));

                parametro.campos.tipoArquivo = tipoArquivo;

                return Validar(parametro);
            }

            return null;
        }

        [GS1CNPAttribute(false)]
        public object RecuperarColunasTemplate(dynamic parametro)
        {
            if (parametro != null)
            {
                dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                if (param != null)
                {
                    dynamic dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml")),
                        retorno = new ExpandoObject();

                    int tipoArquivo = RecuperarTipoArquivoPorID(Convert.ToInt32(param.tipoArquivo)),
                        padraoGDSN = Convert.ToInt32(param.padraoGDSN),
                        template = (tipoArquivo == 0 ? 0 : (tipoArquivo * 10) + padraoGDSN);

                    List<dynamic> dicionario = RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                    retorno.tipoArquivo = tipoArquivo;
                    retorno.colunas = (from dynamic d in dicionario
                                       where !d.nome.ToLower().Equals("indicadorgdsn")
                                       select new
                                       {
                                           field = d.nome.ToLower(),
                                           title = d.coluna,
                                           table = d.tabela.ToLower(),
                                           column = d.campo.ToLower(),
                                           type = d.tipo.ToLower(),
                                           required = (d.obrigatorio as string).Split(',').Contains(template.ToString()),
                                           listname = d.nomeitemlista.ToLower(),
                                           tablereference = d.tabelareferencia.ToLower(),
                                           fieldreference = d.camporeferencia.ToLower(),
                                           descriptionreference = d.campodescricao.ToLower(),
                                           condition = d.condicao.ToLower(),
                                           show = true
                                       });

                    return retorno;
                }
            }

            return null;
        }

        private void AdicionaCabecalho(ExcelWorksheet ws, List<dynamic> dicionario, int template)
        {
            if (ws != null && dicionario != null)
            {
                ws.Cells[1, 1].Value = "ERRO";
                bool obrigatorio = false;

                for (int i = 0; i < dicionario.Count; i++)
                {
                    obrigatorio = false;

                    if (dicionario[i].obrigatorio != null && !string.IsNullOrEmpty(dicionario[i].obrigatorio) && (dicionario[i].obrigatorio as string).Split(',').Contains(template.ToString()))
                        obrigatorio = true;

                    ws.Cells[1, i + 2].Value = dicionario[i].coluna + (obrigatorio ? " (*)" : string.Empty);
                }

                using (var range = ws.Cells[1, 1, 1, dicionario.Count + 1])
                {
                    range.Style.Font.Bold = true;
                    range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    range.Style.Border.Top.Style = range.Style.Border.Right.Style = range.Style.Border.Bottom.Style = range.Style.Border.Left.Style = ExcelBorderStyle.Hair;
                }
            }
        }

        private void AdicionaErros(Database db, ExcelWorksheet ws, List<dynamic> dicionario, dynamic[] lista, bool gdsn)
        {
            if (ws != null && dicionario != null && lista != null)
            {
                lista = lista.OrderByDescending(x => (x as IDictionary<string, object>).ContainsKey("errors") && x.errors != null && (x.errors as Dictionary<string, string>).Count > 0).ToArray<dynamic>();

                for (int i = 0; i < lista.Length; i++)
                {
                    ws.Cells[i + 2, 1].Style.Border.Top.Style = ws.Cells[i + 2, 1].Style.Border.Right.Style = ws.Cells[i + 2, 1].Style.Border.Bottom.Style = ws.Cells[i + 2, 1].Style.Border.Left.Style = ExcelBorderStyle.Hair;

                    if ((lista[i] as IDictionary<string, object>).ContainsKey("errors") && lista[i].errors != null && (lista[i].errors as Dictionary<string, string>).Count > 0)
                    {
                        foreach (KeyValuePair<string, string> e in lista[i].errors as Dictionary<string, string>)
                        {
                            ws.Cells[i + 2, 1].Value += e.Value + Environment.NewLine;
                        }
                    }

                    for (int j = 0; j < dicionario.Count; j++)
                    {
                        if (dicionario[j].nome.ToLower().Equals("indicadorgdsn"))
                            ws.Cells[i + 2, j + 2].Value = gdsn ? "Sim" : "Não";
                        else if ((lista[i] as IDictionary<string, object>).ContainsKey(dicionario[j].nome.ToLower()))
                        {
                            ws.Cells[i + 2, j + 2].Value = (lista[i] as IDictionary<string, object>)[dicionario[j].nome.ToLower()];
                        }

                        ws.Cells[i + 2, j + 2].Style.Border.Top.Style = ws.Cells[i + 2, j + 2].Style.Border.Right.Style = ws.Cells[i + 2, j + 2].Style.Border.Bottom.Style = ws.Cells[i + 2, j + 2].Style.Border.Left.Style = ExcelBorderStyle.Hair;
                    }
                }
            }
        }

        private int RegistrarImportacao(Database db, string arquivo, string arquivoRelatorio, DateTime dataInicial, int itensImportados, int itensNaoImportados, int status, int codigoImportacaoTipo, int codigoImportacaoTipoItem, TimeSpan tempoImportacao, long codigoUsuarioAlteracao, Associado associado)
        {
            dynamic dados = new ExpandoObject();
            dados.Arquivo = arquivo;
            dados.ArquivoRelatorio = arquivoRelatorio;
            dados.Data = dataInicial;
            dados.ItensImportados = itensImportados;
            dados.ItensNaoImportados = itensNaoImportados;
            dados.CodigoImportacaoResultado = status;
            dados.CodigoImportacaoTipo = codigoImportacaoTipo;
            dados.CodigoImportacaoTipoItem = codigoImportacaoTipoItem;
            dados.TempoImportacao = tempoImportacao;
            dados.Status = 1;
            dados.CodigoUsuarioAlteracao = codigoUsuarioAlteracao;
            dados.CodigoAssociado = associado.codigo;

            return db.Insert("ImportacaoProdutos", "Codigo", dados);
        }

        internal bool isDropdown(string fieldname)
        {
            return fieldname == "indicadorgdsn" ||
                   fieldname == "compartilhadados" ||
                   fieldname == "itemcomercialmodelo" ||
                   fieldname == "indicadormercadoriasperigosas" ||
                   fieldname == "indicadoritemcomercialliberadocompra" ||
                   fieldname == "indicadorunidadedespacho" ||
                   fieldname == "indicadorunidadebasicavenda" ||
                   fieldname == "indicadorunidadefaturamento" ||
                   fieldname == "indicadoritemcomercialunidadeconsumo" ||
                   fieldname == "statusgln";
        }

        private object LerArquivoXLS(List<dynamic> dicionario, string arquivo, string templatePath, int tipoArquivo, int padraoGDSN, int template, Associado associado, int qtdeLinhasPermitidas, ref bool existeErro)
        {
            if (dicionario != null && !string.IsNullOrEmpty(arquivo) && !string.IsNullOrEmpty(templatePath))
            {
                using (ExcelPackage pck = new ExcelPackage(new FileInfo(arquivo)))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets[1];

                    if (ws.Dimension.Rows <= 1)
                        return null;

                    using (ExcelPackage pckTemp = new ExcelPackage(new FileInfo(templatePath)))
                    {
                        if (pck.Workbook.Worksheets.Count > 0 && pckTemp.Workbook.Worksheets.Count > 0)
                        {
                            ExcelWorksheet wsTemp = pckTemp.Workbook.Worksheets[1];

                            int rowsCount = ws.Dimension.End.Row - ws.Dimension.Start.Row;
                            List<string> columns = new List<string>();

                            if (rowsCount > 0)
                            {
                                bool valido = ValidarTemplateXLS(ws, wsTemp, tipoArquivo, ref columns);

                                if (valido)
                                {
                                    if (ws.Dimension.End.Row <= qtdeLinhasPermitidas)
                                    {
                                        List<dynamic> result = new List<dynamic>();
                                        dynamic item;
                                        int contador = 0;

                                        using (Database db = new Database("BD"))
                                        {
                                            db.Open();

                                            for (int i = ws.Dimension.Start.Row + 1; i <= ws.Dimension.End.Row; i++)
                                            {
                                                if (!ValidarLinhaVaziaXLS(ws, i, columns.Count))
                                                {
                                                    item = new ExpandoObject();

                                                    contador++;
                                                    item._id = contador;

                                                    for (int j = 1; j <= columns.Count; j++)
                                                    {
                                                        string columName = columns[j - 1].Replace("(*)", string.Empty).Trim().ToLower();

                                                        dynamic col = (from dynamic d in dicionario
                                                                       where d.coluna.ToLower().Equals(columName)
                                                                       select d).FirstOrDefault();

                                                        if (col != null)
                                                        {
                                                            if (col.nome.ToLower().Equals("indicadorgdsn"))
                                                            {
                                                                continue;
                                                            }

                                                            if (isDropdown(col.nome.ToLower()) && !col.nome.ToLower().Equals("statusgln"))
                                                            {
                                                                if (ws.Cells[i, j].Text.Equals("0"))
                                                                    ((IDictionary<string, object>)item).Add(col.nome.ToLower(), "Não");
                                                                else if (ws.Cells[i, j].Text.Equals("1"))
                                                                    ((IDictionary<string, object>)item).Add(col.nome.ToLower(), "Sim");
                                                                else
                                                                    ((IDictionary<string, object>)item).Add(col.nome.ToLower(), ws.Cells[i, j].Text.ToLower().Equals("sim") ? "Sim" : "Não");
                                                            }
                                                            else if (isDropdown(col.nome.ToLower()) && col.nome.ToLower().Equals("statusgln"))
                                                            {
                                                                if (ws.Cells[i, j].Text.Equals("0"))
                                                                    ((IDictionary<string, object>)item).Add(col.nome.ToLower(), "Inativo");
                                                                else if (ws.Cells[i, j].Text.Equals("1"))
                                                                    ((IDictionary<string, object>)item).Add(col.nome.ToLower(), "Ativo");
                                                                else
                                                                    ((IDictionary<string, object>)item).Add(col.nome.ToLower(), ws.Cells[i, j].Text.ToLower().Equals("ativo") ? "Ativo" : "Inativo");
                                                            }
                                                            else if (!string.IsNullOrEmpty(col.tabelareferencia) && !string.IsNullOrEmpty(ws.Cells[i, j].Text))
                                                            {
                                                                dynamic registro = RecuperarRegistroPorDescricao(db, col.tabelareferencia, col.camporeferencia, col.campodescricao, ws.Cells[i, j].Text);
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), (registro != null ? registro.descricao : ws.Cells[i, j].Text));
                                                            }
                                                            else if (col.tipo.ToLower().Equals("int") && !string.IsNullOrEmpty(ws.Cells[i, j].Text) && GS1.CNP.BLL.Core.Util.IsInteger(ws.Cells[i, j].Text))
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), Convert.ToInt32(ws.Cells[i, j].Text));
                                                            else if (col.tipo.ToLower().Equals("datetime") && !string.IsNullOrEmpty(ws.Cells[i, j].Text) && !GS1.CNP.BLL.Core.Util.IsDateBR(ws.Cells[i, j].Text))
                                                            {
                                                                long date = long.Parse(ws.Cells[i, j].Value.ToString());
                                                                DateTime dt = DateTime.FromOADate(date);
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), dt.ToString("d/M/yyyy"));
                                                            }
                                                            else
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), ws.Cells[i, j].Text);
                                                        }
                                                    }

                                                    if (!ValidarRegistro(db, dicionario, item, associado, tipoArquivo, template))
                                                        existeErro = true;

                                                    result.Add(item);
                                                }
                                            }

                                            return result;
                                        }
                                    }
                                    else
                                        throw new GS1TradeException(string.Format("Número máximo de linhas por arquivo foi excedido. É permitido no máximo {0} linhas.", qtdeLinhasPermitidas));
                                }
                                else
                                    throw new GS1TradeException(RecuperarMensagemTemplateInvalido(tipoArquivo));
                            }
                        }
                    }
                }
            }

            return null;
        }

        private object LerArquivoCSV(List<dynamic> dicionario, string arquivo, string templatePath, int tipoArquivo, int padraoGDSN, int template, Associado associado, int qtdeLinhasPermitidas, ref bool existeErro)
        {
            if (dicionario != null && !string.IsNullOrEmpty(arquivo) && !string.IsNullOrEmpty(templatePath))
            {
                using (StreamReader reader = new StreamReader(arquivo, Encoding.GetEncoding(1252), true))
                {
                    using (StreamReader readerTemp = new StreamReader(templatePath, Encoding.GetEncoding(1252), true))
                    {
                        List<string> columns = new List<string>();
                        List<dynamic> result = new List<dynamic>();
                        dynamic item;
                        int contador = 1;
                        string[] values;
                        string line = reader.ReadLine(),
                            lineTemp = readerTemp.ReadLine();

                        if (line != null && lineTemp != null)
                        {
                            bool valido = ValidarTemplateCSV(line, lineTemp, tipoArquivo, ref columns);

                            if (valido)
                            {
                                if (System.IO.File.ReadAllLines(arquivo).Length <= qtdeLinhasPermitidas)
                                {
                                    using (Database db = new Database("BD"))
                                    {
                                        db.Open();

                                        while ((line = reader.ReadLine()) != null)
                                        {
                                            values = line.Split(';');

                                            if (!ValidarLinhaVaziaCSV(values))
                                            {
                                                item = new ExpandoObject();

                                                item._id = contador;
                                                contador++;

                                                if (columns.Count != values.Length)
                                                    throw new GS1TradeException(string.Format("Linha {0} inválida. Número de colunas da linha é diferente do número de colunas do cabeçalho. Favor validar novamente o arquivo.", contador));

                                                for (int j = 0; j < columns.Count; j++)
                                                {
                                                    string columName = columns[j].Replace("(*)", string.Empty).Trim();

                                                    dynamic col = (from dynamic d in dicionario
                                                                   where d.coluna.ToLower().Equals(columName.ToLower())
                                                                   select d).FirstOrDefault();

                                                    if (col != null)
                                                    {
                                                        if (col.nome.ToLower().Equals("indicadorgdsn"))
                                                        {
                                                            continue;
                                                        }

                                                        if (isDropdown(col.nome.ToLower()) && !col.nome.ToLower().Equals("statusgln"))
                                                        {
                                                            if (values[j].Equals("0"))
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), "Não");
                                                            else if (values[j].Equals("1"))
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), "Sim");
                                                            else
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), values[j].ToLower().Equals("sim") ? "Sim" : "Não");
                                                        }
                                                        else if (isDropdown(col.nome.ToLower()) && col.nome.ToLower().Equals("statusgln"))
                                                        {
                                                            if (values[j].Equals("0"))
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), "Inativo");
                                                            else if (values[j].Equals("1"))
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), "Ativo");
                                                            else
                                                                ((IDictionary<string, object>)item).Add(col.nome.ToLower(), values[j].ToLower().Equals("ativo") ? "Ativo" : "Inativo");
                                                        }
                                                        else if (!string.IsNullOrEmpty(col.tabelareferencia) && !string.IsNullOrEmpty(values[j]))
                                                        {
                                                            dynamic registro = RecuperarRegistroPorDescricao(db, col.tabelareferencia, col.camporeferencia, col.campodescricao, values[j]);
                                                            ((IDictionary<string, object>)item).Add(col.nome.ToLower(), (registro != null ? registro.descricao : values[j]));
                                                        }
                                                        else if (col.tipo.ToLower().Equals("int") && !string.IsNullOrEmpty(values[j]) && GS1.CNP.BLL.Core.Util.IsInteger(values[j]))
                                                            ((IDictionary<string, object>)item).Add(col.nome.ToLower(), Convert.ToInt32(values[j]));
                                                        else if (col.tipo.ToLower().Equals("datetime") && !string.IsNullOrEmpty(values[j]) && !GS1.CNP.BLL.Core.Util.IsDateBR(values[j]))
                                                        {
                                                            long date = long.Parse(values[j]);
                                                            DateTime dt = DateTime.FromOADate(date);
                                                            ((IDictionary<string, object>)item).Add(col.nome.ToLower(), dt.ToString("d/M/yyyy"));
                                                        }
                                                        else
                                                            ((IDictionary<string, object>)item).Add(col.nome.ToLower(), values[j]);
                                                    }
                                                }

                                                if (!ValidarRegistro(db, dicionario, item, associado, tipoArquivo, template))
                                                    existeErro = true;

                                                result.Add(item);
                                            }
                                        }

                                        return result;
                                    }
                                }
                                else
                                    throw new GS1TradeException(string.Format("Número máximo de linhas por arquivo foi excedido. É permitido no máximo {0} linhas.", qtdeLinhasPermitidas));
                            }
                            else
                                throw new GS1TradeException(RecuperarMensagemTemplateInvalido(tipoArquivo));
                        }
                    }
                }
            }

            return null;
        }

        private int RecuperarPadraoGDSN_XLS(string arquivo)
        {
            if (!string.IsNullOrEmpty(arquivo))
            {
                using (ExcelPackage pck = new ExcelPackage(new FileInfo(arquivo)))
                {
                    if (pck.Workbook.Worksheets.Count > 0)
                    {
                        ExcelWorksheet ws = pck.Workbook.Worksheets[1];
                        int rowsCount = ws.Dimension.End.Row - ws.Dimension.Start.Row,
                            padraoGDSN = -1;

                        if (rowsCount > 0)
                        {
                            //string nomeColunaPadraoGTIN = ws.Cells[ws.Dimension.Start.Row, ws.Dimension.Start.Column].Text.ToLower().Replace("(*)", string.Empty).Trim();

                            //if (nomeColunaPadraoGTIN.Equals("gdsn?"))
                            padraoGDSN = 2;// ws.Cells[ws.Dimension.Start.Row + 1, ws.Dimension.Start.Column].Text.ToLower().Equals("sim") ? 1 : 2;

                            return padraoGDSN;
                        }
                        else
                            throw new GS1TradeException("Arquivo vazio. Favor inserir registros antes de importar, ou utilize a Importação Manual.");
                    }
                }
            }

            return -1;
        }

        private int RecuperarPadraoGDSN_CSV(string arquivo)
        {
            if (!string.IsNullOrEmpty(arquivo))
            {
                using (StreamReader reader = new StreamReader(arquivo, Encoding.GetEncoding(1252), true))
                {
                    string line = reader.ReadLine();

                    if (line == null || reader.EndOfStream)
                        throw new GS1TradeException("Arquivo vazio. Favor inserir registros antes de importar, ou utilize a Importação Manual.");

                    if (line.IndexOf(';') < 0)
                        throw new GS1TradeException("Formato do arquivo inválido. Os campos devem estar separados por ponto e virgula (;).");

                    if (line != null)
                    {
                        string[] colunas = line.Split(';');

                        int posicaoGDSN = -1,
                            padraoGDSN = -1;

                        string nomeColuna;

                        // Pega posição do campo GDSN no cabeçalho
                        for (int j = 0; j < colunas.Length; j++)
                        {
                            nomeColuna = colunas[j].ToLower().Replace("(*)", string.Empty).Trim();

                            if (nomeColuna.Equals("gdsn?"))
                            {
                                posicaoGDSN = j;
                                break;
                            }
                        }

                        line = reader.ReadLine();

                        if (line != null)
                        {
                            colunas = line.Split(';');

                            //if (posicaoGDSN > -1)
                            padraoGDSN = 2;// colunas[posicaoGDSN].Trim().ToLower().Equals("sim") ? 1 : 2;

                            return padraoGDSN;
                        }
                        else
                            throw new GS1TradeException("Campo GTIN/GLN não encontrado.");
                    }
                }
            }

            return -1;
        }

        //private int RecuperarTipoTemplateXLS(string arquivo, List<DynamicXml> dicionario, ref int padraoGDSN)
        //{
        //    if (!string.IsNullOrEmpty(arquivo))
        //    {
        //        using (ExcelPackage pck = new ExcelPackage(new FileInfo(arquivo)))
        //        {
        //            if (pck.Workbook.Worksheets.Count > 0)
        //            {
        //                ExcelWorksheet ws = pck.Workbook.Worksheets[1];
        //                int rowsCount = ws.Dimension.End.Row - ws.Dimension.Start.Row;

        //                if (rowsCount > 0)
        //                {
        //                    // Pega o campo GTIN obrigatório no dicionario
        //                    dynamic field = (from dynamic d in dicionario
        //                                     where d.gtin.ToLower().Equals("s") && d.nome.ToLower().Contains("gtin")
        //                                     select d).FirstOrDefault();

        //                    if (field != null)
        //                    {
        //                        int posicaoGTIN = -1;
        //                        string nomeColuna;

        //                        // Pega posição do campo GTIN no cabeçalho
        //                        for (int j = ws.Dimension.Start.Column; j <= ws.Dimension.End.Column; j++)
        //                        {
        //                            nomeColuna = ws.Cells[ws.Dimension.Start.Row, j].Text.ToLower().Replace("(*)", string.Empty).Trim();

        //                            if (nomeColuna.Equals("número de identificação gln"))
        //                                return 0;

        //                            if (nomeColuna.Equals(field.coluna.ToLower()))
        //                            {
        //                                posicaoGTIN = j;
        //                                break;
        //                            }
        //                        }

        //                        if (posicaoGTIN > -1)
        //                        {
        //                            string gtin = ws.Cells[ws.Dimension.Start.Row + 1, posicaoGTIN].Text;
        //                            //string nomeColunaPadraoGTIN = ws.Cells[ws.Dimension.Start.Row, ws.Dimension.Start.Column].Text.ToLower().Replace("(*)", string.Empty).Trim();

        //                            //if (nomeColunaPadraoGTIN.Equals("gdsn?"))
        //                            padraoGDSN = 2;// ws.Cells[ws.Dimension.Start.Row + 1, ws.Dimension.Start.Column].Text.ToLower().Equals("sim") ? 1 : 2;

        //                            if (!string.IsNullOrEmpty(gtin))
        //                                return gtin.Trim().Length;
        //                        }
        //                    }
        //                    else
        //                        throw new GS1TradeException("Campo GTIN/GLN não encontrado.");
        //                }
        //                else
        //                    throw new GS1TradeException("Arquivo vazio. Favor inserir registros antes de importar, ou utilize a Importação Manual.");
        //            }
        //        }
        //    }

        //    return -1;
        //}

        //private int RecuperarTipoTemplateCSV(string arquivo, List<DynamicXml> dicionario, ref int padraoGDSN)
        //{
        //    if (!string.IsNullOrEmpty(arquivo))
        //    {
        //        using (StreamReader reader = new StreamReader(arquivo, Encoding.GetEncoding(1252), true))
        //        {
        //            string line = reader.ReadLine();

        //            if (line == null || reader.EndOfStream)
        //                throw new GS1TradeException("Arquivo vazio. Favor inserir registros antes de importar, ou utilize a Importação Manual.");

        //            if (line.IndexOf(';') < 0)
        //                throw new GS1TradeException("Formato do arquivo inválido. Os campos devem estar separados por ponto e virgula (;).");

        //            if (line != null)
        //            {
        //                string[] colunas = line.Split(';');

        //                if (colunas.Contains("Número de Identificação GLN (*)"))
        //                    return 0;

        //                // Pega o campo GTIN obrigatório no dicionario
        //                dynamic field = (from dynamic d in dicionario
        //                                 where d.gtin.ToLower().Equals("s") && d.nome.ToLower().Contains("gtin")
        //                                 select d).FirstOrDefault();

        //                if (field != null)
        //                {
        //                    int posicaoGTIN = -1,
        //                        posicaoGDSN = -1;

        //                    string nomeColuna;

        //                    // Pega posição do campo GTIN no cabeçalho
        //                    for (int j = 0; j < colunas.Length; j++)
        //                    {
        //                        nomeColuna = colunas[j].ToLower().Replace("(*)", string.Empty).Trim();

        //                        /*if (nomeColuna.Equals("gdsn?"))
        //                            posicaoGDSN = j;
        //                        else*/
        //                        if (nomeColuna.Equals(field.coluna.ToLower()))
        //                            posicaoGTIN = j;

        //                        if (/*posicaoGDSN > -1 &&*/ posicaoGTIN > -1)
        //                            break;
        //                    }

        //                    line = reader.ReadLine();

        //                    if (line != null)
        //                    {
        //                        colunas = line.Split(';');

        //                        //if (posicaoGDSN > -1)
        //                        padraoGDSN = 2;// colunas[posicaoGDSN].Trim().ToLower().Equals("sim") ? 1 : 2;

        //                        if (posicaoGTIN > -1)
        //                        {
        //                            string gtin = colunas[posicaoGTIN];

        //                            if (!string.IsNullOrEmpty(gtin))
        //                                return gtin.Trim().Length;
        //                        }
        //                    }
        //                }
        //                else
        //                    throw new GS1TradeException("Campo GTIN/GLN não encontrado.");
        //            }
        //        }
        //    }

        //    return -1;
        //}

        public List<dynamic> RecuperarMapeamentos(List<DynamicXml> dicionario, int tipo)
        {
            if (dicionario != null && tipo > -1)
            {
                List<dynamic> map = (from dynamic m in dicionario
                                     where (m.templates as string).Split(',').Contains(tipo.ToString())
                                     select m).ToList();

                return map;
            }

            return null;
        }

        private bool ValidarTemplateXLS(ExcelWorksheet ws, ExcelWorksheet wsTemp, int tipoArquivo, ref List<string> columns)
        {
            if (ws != null && wsTemp != null)
            {
                int start,
                    end;

                if (ws.Dimension.End.Column >= wsTemp.Dimension.End.Column)
                {
                    start = ws.Dimension.Start.Column;
                    end = ws.Dimension.End.Column;
                }
                else
                {
                    start = wsTemp.Dimension.Start.Column;
                    end = wsTemp.Dimension.End.Column;
                }

                for (int i = start; i <= end; i++)
                {
                    if (ws.Cells[1, i].Text.Trim() != wsTemp.Cells[1, i].Text.Trim())
                        throw new GS1TradeException(RecuperarMensagemTemplateInvalido(tipoArquivo));

                    if (!string.IsNullOrEmpty(ws.Cells[1, i].Text.Trim()))
                        columns.Add(ws.Cells[1, i].Text.Trim());
                }

                return true;
            }

            return false;
        }

        private bool ValidarTemplateCSV(string header, string headerTemp, int tipoArquivo, ref List<string> columns)
        {
            if (!string.IsNullOrEmpty(header) && !string.IsNullOrEmpty(headerTemp))
            {
                string[] headers = header.Split(';');
                string[] headersTemp = headerTemp.Split(';');

                if (headers.Length != headersTemp.Length)
                    throw new GS1TradeException(RecuperarMensagemTemplateInvalido(tipoArquivo));

                for (int i = 0; i < headers.Length; i++)
                {
                    if (headers[i].Trim() != headersTemp[i].Trim())
                        throw new GS1TradeException(RecuperarMensagemTemplateInvalido(tipoArquivo));

                    columns.Add(headers[i].Trim());
                }

                return true;
            }

            return false;
        }

        private bool ValidarLinhaVaziaXLS(ExcelWorksheet ws, int linha, int colunas)
        {
            if (ws != null && linha > 0 && colunas > 0)
            {
                for (int i = 1; i < colunas; i++)
                {
                    if (!string.IsNullOrEmpty(ws.Cells[linha, i].Text.Trim()))
                        return false;
                }
            }

            return true;
        }

        private bool ValidarLinhaVaziaCSV(string[] colunas)
        {
            if (colunas != null && colunas.Length > 0)
            {
                for (int i = 1; i < colunas.Length; i++)
                {
                    if (!string.IsNullOrEmpty(colunas[i].Trim()))
                        return false;
                }
            }

            return true;
        }

        internal bool ValidarRegistro(Database db, List<dynamic> dicionario, dynamic registro, Associado associado, int tipoArquivo, int template, bool acaoInserir = true)
        {
            if (registro != null && ((IDictionary<string, object>)registro).Count > 0)
            {
                Dictionary<string, string> erros = new Dictionary<string, string>();
                bool erro,
                    obrigatorio = false;

                foreach (KeyValuePair<string, object> field in ((IDictionary<string, object>)registro))
                {
                    dynamic col = (from dynamic d in dicionario
                                   where d.nome.ToLower().Equals(field.Key.ToLower())
                                   select d).FirstOrDefault();

                    erro = false;

                    if (col != null)
                    {
                        string value = string.Empty;

                        if (field.Value != null)
                            value = field.Value.ToString();

                        if (tipoArquivo != 0)
                        {
                            string campo = field.Key.ToLower();

                            if (campo.Equals("altura") ||
                                campo.Equals("alturaunidademedida") ||
                                campo.Equals("largura") ||
                                campo.Equals("larguraunidademedida") ||
                                campo.Equals("profundidade") ||
                                campo.Equals("profundidadeunidademedida") ||
                                campo.Equals("conteudoliquido") ||
                                campo.Equals("conteudoliquidounidademedida") ||
                                campo.Equals("pesobruto") ||
                                campo.Equals("pesobrutounidademedida") ||
                                campo.Equals("pesoliquido") ||
                                campo.Equals("pesoliquidounidademedida"))
                            {
                                dynamic paramTipoProduto = new ExpandoObject();
                                paramTipoProduto.valor = registro.tipoproduto.ToString();

                                var tipoProduto = db.Find("tipoproduto", "codigo = @valor OR nome = @valor", paramTipoProduto);

                                obrigatorio = (tipoProduto != null && tipoProduto.campoobrigatorio == 1);
                            }
                            else if (campo.Equals("statuscodigo"))
                            {
                                obrigatorio = (registro as IDictionary<string, object>).ContainsKey("gtin") && !string.IsNullOrEmpty(registro.gtin.ToString());
                            }
                            else
                            {
                                obrigatorio = false;
                            }
                        }

                        if (obrigatorio || (!string.IsNullOrEmpty(value) && col.obrigatorio == "") || (col.obrigatorio as string).Split(',').Contains(template.ToString()))
                        {
                            if (string.IsNullOrEmpty(col.tabelareferencia) && !isDropdown(col.nome))
                            {
                                string tipo = col.tipo;
                                switch (field.Key.ToLower().Equals("gtininferior") || field.Key.ToLower().Equals("quantidadeinferior") ? "array" : tipo)
                                {
                                    case "int":
                                    case "long":
                                        if (!GS1.CNP.BLL.Core.Util.IsInteger(value))
                                        {
                                            erros.Add(field.Key, string.Format("Erro! Campo: {0} - {1}", col.coluna, "Valor informado não é um valor inteiro válido."));
                                            erro = true;
                                        }
                                        break;
                                    case "decimal":
                                        if (string.IsNullOrEmpty(value))
                                        {
                                            erros.Add(field.Key, string.Format("Erro! O campo {0} é obrigatório.", col.coluna));
                                            erro = true;
                                        }
                                        else if (!GS1.CNP.BLL.Core.Util.IsDecimal(value))
                                        {
                                            erros.Add(field.Key, string.Format("Erro! Campo: {0} - {1}", col.coluna, "Valor informado não é um valor decimal válido."));
                                            erro = true;
                                        }
                                        break;
                                    case "datetime":
                                        if (!GS1.CNP.BLL.Core.Util.IsDateBR(value))
                                        {
                                            erro = true;
                                            erros.Add(field.Key, string.Format("Erro! Campo: {0} - {1}", col.coluna, "Valor informado não é uma data válida."));
                                        }
                                        break;
                                    case "array":
                                        if (!string.IsNullOrEmpty(value))
                                        {
                                            string[] array = value.Split(',');

                                            foreach (string arr in array)
                                            {
                                                if (!GS1.CNP.BLL.Core.Util.IsInteger(arr))
                                                {
                                                    erros.Add(field.Key, string.Format("O valor informado no campo {0} é inválido.", col.coluna));
                                                    erro = true;
                                                    break;
                                                }
                                            }
                                        }
                                        break;
                                    case "string":
                                    case "text":
                                    default:
                                        if (string.IsNullOrEmpty(value))
                                        {
                                            erros.Add(field.Key, string.Format("Erro! Campo: {0} - {1}", col.coluna, "Campo obrigatório."));
                                            erro = true;
                                        }
                                        break;
                                }
                            }

                            if (!erro)
                            {
                                if (isDropdown(field.Key))
                                {
                                    if (field.Key.ToLower().Equals("statusgln") && !value.ToLower().Equals("ativo") && !value.ToLower().Equals("inativo"))
                                        erros.Add(field.Key, string.Format("{0} para o item não existe.", col.coluna));
                                    else if (!field.Key.ToLower().Equals("statusgln") && !value.ToLower().Equals("sim") && !value.ToLower().Equals("não"))
                                        erros.Add(field.Key, string.Format("{0} para o item não existe.", col.coluna));
                                }
                                if (!string.IsNullOrEmpty(col.tabelareferencia) && !string.IsNullOrEmpty(col.camporeferencia))
                                {
                                    if (string.IsNullOrEmpty(value) && col.obrigatorio != null && 
                                        (col.obrigatorio as string).Split(',').Contains(template.ToString()))
                                        erros.Add(field.Key, string.Format("Erro! O campo {0} é obrigatório.", col.coluna));
                                    else
                                    {
                                        string nometabela = col.tabelareferencia.ToString();
                                        dynamic param = new ExpandoObject();
                                        param.valor = value;

                                        var condicao = string.Format("{0} = @valor", col.campodescricao);

                                        if (Core.Util.IsInteger(value.Replace(".", string.Empty)))
                                        {
                                            if (nometabela.ToLower().Equals("ncm"))
                                                condicao =
                                                    string.Format(
                                                        "cast(replace({0},\'.\',\'\') as int) = cast(replace(@valor,\'.\',\'\') as int)",
                                                        col.camporeferencia);
                                            else if (nometabela.ToLower().Equals("cest"))
                                                condicao = string.Format(
                                                    "cast(replace({0},\'.\',\'\') as int) = cast(replace(@valor,\'.\',\'\') as int)",
                                                    col.nomeitemlista);
                                            else
                                                condicao =
                                                    string.Format(
                                                        "{0} = cast(replace(@valor,\'.\',\'\') as int) OR {1} = cast(@valor as varchar)",
                                                        col.camporeferencia, col.campodescricao);
                                        }

                                        if (db.Count(nometabela, condicao, param) <= 0)
                                            erros.Add(field.Key, string.Format("Erro! {0} para o item não existe.", col.coluna));
                                    }
                                }

                                if (field.Key.ToLower() == "datacriacaocodigo")
                                {
                                    if (GS1.CNP.BLL.Core.Util.IsDateBR(value))
                                    {
                                        DateTime dtCriacao = DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                        if (dtCriacao != null)
                                        {
                                            if (dtCriacao < DateTime.Today)
                                            {
                                                erros.Add(field.Key, string.Format("A {0} não deverá ser menor que a data atual.", col.coluna));
                                            }
                                        }
                                    }
                                }
                                else if (!string.IsNullOrEmpty(value) && (field.Key.ToLower() == "temperaturaminimaarmazenamento" ||
                                    field.Key.ToLower() == "temperaturamaximaarmazenamento" ||
                                    field.Key.ToLower() == "altura" ||
                                    field.Key.ToLower() == "largura" ||
                                    field.Key.ToLower() == "profundidade" ||
                                    field.Key.ToLower() == "conteudoliquido" ||
                                    field.Key.ToLower() == "pesobruto" ||
                                    field.Key.ToLower() == "pesoliquido" ||
                                    field.Key.ToLower() == "aliquotaipi"))
                                {
                                    if (GS1.CNP.BLL.Core.Util.IsDecimal(value))
                                    {
                                        double valueTemp = Convert.ToDouble(value);

                                        if (valueTemp > 999.99 || valueTemp < -999.99)
                                        {
                                            erros.Add(field.Key, string.Format("A valor {0} aceitável para o campo {1} é {2}999,99.", (valueTemp > 0 ? "máximo" : "mínimo"), col.coluna, (valueTemp > 0 ? string.Empty : "-")));
                                        }
                                    }
                                }
                                else if (field.Key.ToLower() == "gtin")
                                {
                                    if (!string.IsNullOrEmpty(value) && GS1.CNP.BLL.Core.Util.IsInteger(value))
                                    {
                                        if (value.Length != tipoArquivo)
                                            erros.Add(field.Key, "GTIN diferente do esperado para este template. Favor utilizar o template correto para a importação deste produto.");
                                        else
                                        {
                                            if (new ProdutoBO().ValidaGTIN(value))
                                            {
                                                dynamic param = new ExpandoObject();
                                                param.gtin = registro.gtin;
                                                param.associado = associado.codigo;

                                                string sql = "SELECT * FROM Produto WHERE globalTradeItemNumber = @gtin";
                                                List<dynamic> produtos = db.Select(sql, param);

                                                int count = produtos.Where(x => x.codigoassociado.Equals(associado.codigo) || !x.codigoassociado.Equals(associado.codigo) && !x.codigostatusgtin.Equals(7)).Count();

                                                if (count > 0 && acaoInserir)
                                                    erros.Add(field.Key, RecuperarMotivo(db, 2));
                                                else
                                                {
                                                    long prefixo = RecuperarPrefixo(value, tipoArquivo, associado.codigo);

                                                    if (prefixo == 0)
                                                        erros.Add(field.Key, RecuperarMotivo(db, 4));
                                                    else if (prefixo == -1)
                                                        erros.Add(field.Key, RecuperarMotivo(db, 6));
                                                    else
                                                    {
                                                        if (RecuperarSituacaoFinanceira(db, associado) != 1)
                                                            erros.Add(field.Key, RecuperarMotivo(db, 5));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                erros.Add(field.Key, RecuperarMotivo(db, 3));
                                            }
                                        }
                                    }
                                    else if (!acaoInserir && string.IsNullOrEmpty(value))
                                    {
                                        erros.Add(field.Key, string.Format("O {0} não existe em nossa base de dados.", col.coluna));
                                    }
                                }
                                else if (field.Key.ToLower() == "gln" && GS1.CNP.BLL.Core.Util.IsInteger(value))
                                {
                                    if (new LocalizacoesFisicasBO().ValidaGLN(value))
                                    {
                                        int count = db.Count("LocalizacaoFisica", "gln = @gln", registro);

                                        if (count > 0)
                                            erros.Add(field.Key, RecuperarMotivo(db, 2));
                                        else
                                        {
                                            long prefixo = RecuperarPrefixo(value, tipoArquivo, associado.codigo);

                                            if (prefixo == 0)
                                                erros.Add(field.Key, RecuperarMotivo(db, 4));
                                            else if (prefixo == -1)
                                                erros.Add(field.Key, RecuperarMotivo(db, 6));
                                            else
                                            {
                                                if (RecuperarSituacaoFinanceira(db, associado) != 1)
                                                    erros.Add(field.Key, RecuperarMotivo(db, 5));
                                            }
                                        }
                                    }
                                    else
                                        erros.Add(field.Key, RecuperarMotivo(db, 3));
                                }
                                else if (field.Key.ToLower() == "gtinorigem")
                                {
                                    bool erroGTINOrigem = false;

                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        if (!string.IsNullOrEmpty(value) && GS1.CNP.BLL.Core.Util.IsInteger(value))
                                        {
                                            int count = db.Count("Produto", "globalTradeItemNumber = @gtinorigem", registro);

                                            if (count == 0)
                                            {
                                                erroGTINOrigem = true;
                                                erros.Add(field.Key, string.Format("O {0} não existe em nossa base de dados.", col.coluna));
                                            }
                                        }
                                        else if (!GS1.CNP.BLL.Core.Util.IsInteger(value))
                                        {
                                            erroGTINOrigem = true;
                                            erros.Add(field.Key, string.Format("O valor informado no campo {0} é inválido.", col.coluna));
                                        }

                                        if (!erroGTINOrigem)
                                        {
                                            if ((registro as IDictionary<string, object>).ContainsKey("gtin") && registro.gtin != null)
                                            {
                                                string gtin = registro.gtin.ToString();

                                                if (!string.IsNullOrEmpty(gtin) && gtin.Length == 14 && GS1.CNP.BLL.Core.Util.IsInteger(gtin))
                                                {
                                                    string gtinorigem = value.TrimStart(new char[] { '0' });
                                                    string novogtinorigem = gtin.Substring(1, gtin.Length - 2).TrimStart(new Char[] { '0' });
                                                    int digitoVerificador = RecuperarDigitoVerificador(novogtinorigem);
                                                    novogtinorigem += digitoVerificador.ToString();

                                                    if (!gtinorigem.Equals(novogtinorigem))
                                                    {
                                                        erros.Add(field.Key, string.Format("O campo {0} deve ser parte do campo GTIN.", col.coluna));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                        erros.Add(field.Key, string.Format("O campo {0} é de preenchimento obrigatório.", col.coluna));
                                }
                                else if (field.Key.ToLower() == "quantidadeitensporcaixa")
                                {
                                    if (string.IsNullOrEmpty(value))
                                        erros.Add(field.Key, string.Format("O campo {0} é de preenchimento obrigatório.", col.coluna));
                                    else if (!string.IsNullOrEmpty(value) && !GS1.CNP.BLL.Core.Util.IsInteger(value))
                                        erros.Add(field.Key, string.Format("O valor informado no campo {0} é inválido.", col.coluna));
                                }
                                else if (field.Key.ToLower() == "gtininferior")
                                {
                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        string[] array = value.Split(',');
                                        dynamic param = new ExpandoObject();
                                        param.gtininferior = 0;

                                        foreach (string arr in array)
                                        {
                                            if (!string.IsNullOrEmpty(arr) && GS1.CNP.BLL.Core.Util.IsInteger(arr))
                                            {
                                                param.gtininferior = arr;

                                                int count = db.Count("Produto", "globalTradeItemNumber = @gtininferior", param);

                                                if (count == 0)
                                                {
                                                    erros.Add(field.Key, string.Format("O {0} não existe em nossa base de dados.", col.coluna));
                                                    break;
                                                }
                                            }
                                            else
                                                erros.Add(field.Key, string.Format("O campo {0} é de preenchimento obrigatório.", col.coluna));
                                        }
                                    }
                                }
                                else if (field.Key.ToLower().Contains("nomeurl") || field.Key.ToLower().Contains("url") || field.Key.ToLower().Equals("tipourl") ||
                                    field.Key.ToLower().Equals("identificacaoalternativa") || field.Key.ToLower().Equals("agenciareguladora"))
                                {
                                    if (!string.IsNullOrEmpty(value))
                                    {
                                        List<dynamic> colUrl = (from dynamic d in dicionario
                                                                where d.grupo.Equals(col.grupo.ToString())
                                                                && !d.nome.ToLower().Equals(field.Key.ToLower())
                                                                select d).ToList<dynamic>();

                                        foreach (dynamic d in colUrl)
                                        {
                                            if ((((IDictionary<string, object>)registro)[d.nome.ToLower()] == null || string.IsNullOrEmpty(((IDictionary<string, object>)registro)[d.nome.ToLower()].ToString())) && !erros.ContainsKey(d.nome.ToLower()))
                                                erros.Add(d.nome.ToLower(), string.Format("O campo {0} é de preenchimento obrigatório.", d.coluna));
                                        }
                                    }
                                }
                            }
                        }
                        else
                            continue;
                    }
                }

                registro.errors = erros;

                return erros.Count == 0;
            }

            return false;
        }

        private string RecuperarMotivo(Database db, int codigo)
        {
            dynamic param = new ExpandoObject();
            param.codigo = codigo;
            dynamic motivo = db.Find("ImportacaoStatusMotivos", param);

            if (motivo != null)
                return motivo.descricao;

            return string.Empty;
        }

        private int RecuperarSituacaoFinanceira(Database db, Associado associado)
        {
            if (db != null && associado != null)
            {
                dynamic param = new ExpandoObject();
                param.codigo = associado.codigo;

                dynamic situacaoFinanceira = db.Find("Associado", param);

                if (situacaoFinanceira != null)
                {
                    return situacaoFinanceira.codigosituacaofinanceira;
                }
            }

            return 0;
        }

        private int ImportarRegistroProduto(Database db, List<dynamic> dicionario, dynamic registro, int tipoArquivo, int codigoTipoGTIN, bool gdsn, Login user, Associado associado, string ultimoGTIN, bool acaoInserir = true)
        {
            if (db != null && dicionario != null && registro != null)
            {
                string tablename = "Produto";
                dynamic linha = new ExpandoObject();

                List<dynamic> colunas = (from dynamic col in dicionario
                                         where col.tabela.ToLower().Equals(tablename.ToLower())
                                         select col).ToList<dynamic>();
                var produto = new ProdutoBO();

                foreach (dynamic c in colunas)
                {
                    if (((IDictionary<string, object>)registro).ContainsKey(c.nome.ToLower()) && ((IDictionary<string, object>)registro)[c.nome.ToLower()] != null && !string.IsNullOrEmpty(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString()))
                    {
                        if (c.nome.ToLower().IndexOf("idioma") >= 0)
                        {
                            dynamic dados = new ExpandoObject();
                            dados.descricao = ((IDictionary<string, object>)registro)[c.nome.ToLower()];
                            dynamic idioma = db.Find("idioma", "lower(descricao) = lower(@descricao)", dados);

                            if (idioma != null)
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), idioma.codigo);
                        }
                        else if (c.nome.ToLower().Equals("gtinorigem"))
                        {
                            dynamic p = new ExpandoObject();
                            p.gtinorigem = registro.gtinorigem;
                            p.associado = associado.codigo;

                            dynamic resultado = db.Find("Produto", "GlobalTradeItemNumber = @gtinorigem AND CodigoAssociado = @associado", p);

                            if (resultado != null)
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), resultado.codigoproduto);
                        }
                        else
                        {
                            if (isDropdown(c.nome.ToLower()))
                            {
                                string valor = ((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString();

                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), valor.ToLower().Equals("sim") ? 1 : 0);
                            }
                            else if (!string.IsNullOrEmpty(c.tabelareferencia.ToString()))
                            {
                                string valor = ((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString();
                                dynamic result = RecuperarRegistroPorDescricao(db, c.tabelareferencia.ToString(), c.camporeferencia.ToString(), c.campodescricao.ToString(), valor);

                                if (result != null)
                                    ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), result.codigo);
                            }
                            else
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), PegarValor(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString(), c));
                        }
                    }
                }

                if (linha != null && ((IDictionary<string, object>)linha).Count > 0)
                {
                    linha.indicadorgdsn = gdsn ? 1 : 0;
                    linha.CodigoTipoGTIN = codigoTipoGTIN;
                    linha.CodigoAssociado = associado.codigo;
                    linha.CodigoUsuarioCriacao = user.id;
                    linha.importClassificationType = 6;
                    linha.informationProvider = RecuperarGLNProvedorInformacao(db, associado);
                    linha.DataInclusao = DateTime.Now;

                    if (!(linha as IDictionary<string, object>).ContainsKey("codigostatusgtin") || string.IsNullOrEmpty(linha.codigostatusgtin.ToString()))
                    {
                        linha.codigostatusgtin = 1;
                    }

                    if (!((IDictionary<string, object>)linha).ContainsKey("globaltradeitemnumber") || string.IsNullOrEmpty(linha.globaltradeitemnumber.ToString()))
                    {
                        if (!string.IsNullOrEmpty(ultimoGTIN))
                        {
                            linha.NrPrefixo = RecuperarPrefixo(ultimoGTIN, tipoArquivo, associado.codigo);
                        }
                        else
                        {
                            if (tipoArquivo == 12 || tipoArquivo == 13)
                            {
                                dynamic prefixo = JsonConvert.DeserializeObject(produto.PegarGTINValido(codigoTipoGTIN.ToString()).ToString());

                                if (prefixo == null)
                                    throw new GS1TradeException("Não existe nenhuma licença ativa");
                                else
                                    linha.NrPrefixo = prefixo[0].numeroprefixo.Value;
                            }
                            else if (tipoArquivo == 14)
                            {
                                string ret = produto.RetornarGTINProdutoOrigem(registro.gtinorigem.ToString());
                                dynamic retornoGTINOrigem = JsonConvert.DeserializeObject(ret);

                                if (retornoGTINOrigem != null)
                                {
                                    dynamic dadosretornados = retornoGTINOrigem[0];
                                    dynamic retornaProximaVariante = JsonConvert.DeserializeObject(produto.RetornarProximaVariante(registro.gtinorigem.ToString()));

                                    if (retornaProximaVariante != null)
                                    {
                                        string indicador = retornaProximaVariante[0].codigo;
                                        int tamanho = -1,
                                            tipogtin = Convert.ToInt32(dadosretornados.codigotipogtin.ToString());


                                        switch (tipogtin)
                                        {
                                            //GTIN-8
                                            case 1:
                                                tamanho = 7;
                                                break;
                                            //GTIN-12
                                            case 2:
                                                tamanho = 11;
                                                break;
                                            //GTIN-13
                                            case 3:
                                                tamanho = 12;
                                                break;
                                        }
                                        var gtin = indicador.ToString() + "".PadLeft(13 - tamanho - 1, '0') + dadosretornados.globaltradeitemnumber.ToString().Substring(0, tamanho);
                                        string verificador = produto.GeraDigitoVerificador(Convert.ToString(gtin));

                                        linha.globaltradeitemnumber = gtin + verificador;
                                        linha.variantelogistica = indicador.ToString();
                                        linha.coditem = dadosretornados.coditem.Value;
                                        linha.nrprefixo = dadosretornados.nrprefixo.Value;
                                    }
                                    else
                                    {
                                        throw new GS1TradeException("Número máximo de variantes logísticas alcançada.");
                                    }
                                }
                            }
                        }

                        if (tipoArquivo == 12 || tipoArquivo == 13)
                        {
                            dynamic gtinGerado = produto.GeradorGTIN(linha.NrPrefixo.ToString(), tipoArquivo);

                            linha.globaltradeitemnumber = gtinGerado.globaltradeitemnumber;
                            linha.coditem = gtinGerado.coditem;
                        }
                    }
                    else
                    {
                        linha.NrPrefixo = RecuperarPrefixo(ultimoGTIN, tipoArquivo, associado.codigo);
                        linha.coditem = RecuperarNumeroItem(linha.globaltradeitemnumber.ToString(), linha.NrPrefixo);
                    }

                    return db.Insert(tablename, string.Empty, linha);
                }
            }

            return 0;
        }

        private int ImportarRegistroProdutoHierarquia(Database db, List<dynamic> dicionario, dynamic registro, int codigo, Associado associado)
        {
            if (db != null && dicionario != null && registro != null)
            {
                string tablename = "ProdutoHierarquia";
                dynamic linha;

                if ((registro as IDictionary<string, object>).ContainsKey("gtininferior") && (registro as IDictionary<string, object>).ContainsKey("quantidadeinferior"))
                {
                    string[] listaGTINInferior = registro.gtininferior.ToString().Split(',');
                    string[] listaQuantidadeInferior = registro.quantidadeinferior.ToString().Split(',');

                    if (listaGTINInferior.Length > 0)
                    {
                        for (int i = 0; i < listaGTINInferior.Length; i++)
                        {
                            linha = new ExpandoObject();
                            string gtininferior = listaGTINInferior[i];
                            string quantidade = listaQuantidadeInferior[i];

                            if (!string.IsNullOrEmpty(gtininferior))
                            {
                                dynamic p = new ExpandoObject();
                                p.gtinorigem = gtininferior;
                                p.associado = associado.codigo;

                                dynamic resultado = db.Find("Produto", "GlobalTradeItemNumber = @gtinorigem AND CodigoAssociado = @associado", p);

                                if (resultado != null)
                                {
                                    linha.CodigoProdutoInferior = resultado.codigoproduto;
                                    linha.CodigoProdutoSuperior = codigo;
                                    linha.Quantidade = quantidade;

                                    return db.Insert(tablename, string.Empty, linha, false);
                                }
                                else
                                {
                                    registro.errors.Add("gtininferior", string.Format("O GTIN Inferior de número '{0}' não foi encontrado em nossa base de dados.", gtininferior));
                                }
                            }
                        }
                    }
                }
            }

            return 0;
        }

        private int ImportarRegistroProdutoURL(Database db, List<dynamic> dicionario, dynamic registro, int codigo)
        {
            if (db != null && dicionario != null && registro != null)
            {
                string tablename = "ProdutoURL";
                dynamic linha = null;
                int rowsAffected = 0;

                List<dynamic> mapeamentos = (from dynamic m in dicionario
                                             where m.tabela.ToLower().Equals(tablename.ToLower())
                                             select m).ToList<dynamic>();

                int index = 6;
                List<dynamic> colunas = (from dynamic c in mapeamentos
                                         where c.grupo.Equals(index.ToString())
                                         select c).ToList<dynamic>();

                while (colunas != null && colunas.Count > 0)
                {
                    linha = new ExpandoObject();

                    foreach (dynamic c in colunas)
                    {
                        if (((IDictionary<string, object>)registro).ContainsKey(c.nome.ToLower()) && ((IDictionary<string, object>)registro)[c.nome.ToLower()] != null && !string.IsNullOrEmpty(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString()))
                        {
                            if (!string.IsNullOrEmpty(c.tabelareferencia.ToString()))
                            {
                                string valor = ((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString();
                                dynamic result = RecuperarRegistroPorDescricao(db, c.tabelareferencia.ToString(), c.camporeferencia.ToString(), c.campodescricao.ToString(), valor);

                                if (result != null)
                                    ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), result.codigo);
                            }
                            else
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), PegarValor(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString(), c));
                        }
                    }

                    if (((IDictionary<string, object>)linha).Count > 0)
                    {
                        linha.codigoproduto = codigo;
                        linha.status = 1;

                        rowsAffected += db.Insert(tablename, string.Empty, linha, false);
                    }

                    index++;
                    colunas = (from dynamic c in mapeamentos
                               where c.grupo.Equals(index.ToString())
                               select c).ToList<dynamic>();
                }

                return rowsAffected;
            }

            return 0;
        }

        private int ImportarRegistroProdutoAgenciaReguladora(Database db, List<dynamic> dicionario, dynamic registro, int codigo)
        {
            if (db != null && dicionario != null && registro != null)
            {
                string tablename = "ProdutoAgenciaReguladora";
                dynamic linha = null;
                int rowsAffected = 0;

                List<dynamic> mapeamentos = (from dynamic m in dicionario
                                             where m.tabela.ToLower().Equals(tablename.ToLower())
                                             select m).ToList<dynamic>();

                int index = 1;
                List<dynamic> colunas = (from dynamic c in mapeamentos
                                         where c.grupo.Equals(index.ToString())
                                         select c).ToList<dynamic>();

                while (colunas != null && colunas.Count > 0)
                {
                    linha = new ExpandoObject();

                    foreach (dynamic c in colunas)
                    {
                        if (((IDictionary<string, object>)registro).ContainsKey(c.nome.ToLower()) && ((IDictionary<string, object>)registro)[c.nome.ToLower()] != null && !string.IsNullOrEmpty(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString()))
                        {
                            if (!string.IsNullOrEmpty(c.tabelareferencia.ToString()))
                            {
                                string valor = ((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString();
                                dynamic result = RecuperarRegistroPorDescricao(db, c.tabelareferencia.ToString(), c.camporeferencia.ToString(), c.campodescricao.ToString(), valor);

                                if (result != null)
                                    ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), result.codigo);
                            }
                            else
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), PegarValor(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString(), c));
                        }
                    }

                    if (((IDictionary<string, object>)linha).Count > 0)
                    {
                        linha.codigoproduto = codigo;

                        rowsAffected += db.Insert(tablename, string.Empty, linha, false);
                    }

                    index++;
                    colunas = (from dynamic c in mapeamentos
                               where c.grupo.Equals(index.ToString())
                               select c).ToList<dynamic>();
                }

                return rowsAffected;
            }

            return 0;
        }

        private int ImportarRegistroLocalizacaoFisica(Database db, List<dynamic> dicionario, dynamic registro, int tipoArquivo, Login user, Associado associado, string ultimoGLN)
        {
            if (db != null && dicionario != null && registro != null)
            {
                string tablename = "LocalizacaoFisica";
                dynamic linha = new ExpandoObject();

                List<dynamic> colunas = (from dynamic col in dicionario
                                         where col.tabela.ToLower().Equals(tablename.ToLower())
                                         select col).ToList<dynamic>();
                var locfisica = new LocalizacoesFisicasBO();

                foreach (dynamic c in colunas)
                {
                    if (((IDictionary<string, object>)registro).ContainsKey(c.nome.ToLower()) && ((IDictionary<string, object>)registro)[c.nome.ToLower()] != null && !string.IsNullOrEmpty(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString()))
                    {
                        if (isDropdown(c.nome.ToLower()))
                        {
                            string valor = ((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString();

                            ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), valor.ToLower().Equals("ativo") ? 1 : 0);
                        }
                        else if (!string.IsNullOrEmpty(c.tabelareferencia.ToString()))
                        {
                            string valor = ((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString();
                            dynamic result = RecuperarRegistroPorDescricao(db, c.tabelareferencia.ToString(), c.camporeferencia.ToString(), c.campodescricao.ToString(), valor);

                            if (result != null)
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), c.campo.ToString().ToLower().Equals("pais") ? result.descricao : result.codigo);
                        }
                        else
                        {
                            object valor = PegarValor(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString(), c);

                            if (c.campo.ToString().ToLower().Equals("cep"))
                                valor = valor.ToString().Replace(".", string.Empty).Replace("-", string.Empty);

                            ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), valor);
                        }
                    }
                }

                if (linha != null && ((IDictionary<string, object>)linha).Count > 0)
                {
                    linha.CodigoAssociado = associado.codigo;
                    linha.CodigoUsuarioAlteracao = user.id;

                    if (!((IDictionary<string, object>)linha).ContainsKey("gln") || string.IsNullOrEmpty(linha.gln.ToString()))
                    {
                        long prefixo = 0;
                        if (!string.IsNullOrEmpty(ultimoGLN))
                        {
                            prefixo = RecuperarPrefixo(ultimoGLN, tipoArquivo, associado.codigo);
                        }
                        else
                        {
                            dynamic retPrefixo = JsonConvert.DeserializeObject(locfisica.BuscarPrefixo(null).ToString());

                            if (retPrefixo == null)
                                throw new GS1TradeException("Não existe nenhuma licença ativa");

                            prefixo = retPrefixo[0].prefixo.Value;
                        }

                        dynamic gtinGerado = locfisica.GeradorGLN(prefixo.ToString());
                        linha.gln = gtinGerado.gln;
                        linha.prefixo = prefixo;
                        linha.numeroitem = gtinGerado.numeroitem;

                    }
                    else
                    {
                        int nItem = 0;
                        linha.prefixo = RecuperarPrefixo(ultimoGLN, tipoArquivo, associado.codigo);
                        string numeroItem = RecuperarNumeroItem(linha.gln.ToString(), linha.prefixo);

                        if (Int32.TryParse(numeroItem, out nItem))
                            linha.numeroitem = nItem;
                    }

                    linha.DataAlteracao = DateTime.Now;

                    return db.Insert(tablename, string.Empty, linha);
                }
            }

            return 0;
        }

        private int AtualizarRegistroProduto(Database db, List<dynamic> dicionario, dynamic registro, int tipoArquivo, int codigoTipoGTIN, bool gdsn, Login user, Associado associado, bool acaoInserir = true)
        {
            if (db != null && dicionario != null && registro != null)
            {
                string tablename = "Produto";
                dynamic linha = new ExpandoObject();

                List<dynamic> colunas = (from dynamic col in dicionario
                                         where col.tabela.ToLower().Equals(tablename.ToLower())
                                         select col).ToList<dynamic>();
                var produto = new ProdutoBO();

                foreach (dynamic c in colunas)
                {
                    if (((IDictionary<string, object>)registro).ContainsKey(c.nome.ToLower()) && ((IDictionary<string, object>)registro)[c.nome.ToLower()] != null && !string.IsNullOrEmpty(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString()))
                    {
                        if (c.nome.ToLower().IndexOf("idioma") >= 0)
                        {
                            dynamic dados = new ExpandoObject();
                            dados.descricao = ((IDictionary<string, object>)registro)[c.nome.ToLower()];
                            dynamic idioma = db.Find("idioma", "lower(descricao) = lower(@descricao)", dados);

                            if (idioma != null)
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), idioma.codigo);
                        }
                        else if (c.nome.ToLower().Equals("gtinorigem"))
                        {
                            dynamic p = new ExpandoObject();
                            p.gtinorigem = registro.gtinorigem;
                            p.associado = associado.codigo;

                            dynamic resultado = db.Find("Produto", "GlobalTradeItemNumber = @gtinorigem AND CodigoAssociado = @associado", p);

                            if (resultado != null)
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), resultado.codigoproduto);
                        }
                        else
                        {
                            if (isDropdown(c.nome.ToLower()))
                            {
                                string valor = ((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString();

                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), valor.ToLower().Equals("sim") ? 1 : 0);
                            }
                            else if (!string.IsNullOrEmpty(c.tabelareferencia.ToString()))
                            {
                                string valor = ((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString();
                                dynamic result = RecuperarRegistroPorDescricao(db, c.tabelareferencia.ToString(), c.camporeferencia.ToString(), c.campodescricao.ToString(), valor);

                                if (result != null)
                                    ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), result.codigo);
                            }
                            else
                                ((IDictionary<string, object>)linha).Add(c.campo.ToString().ToLower(), PegarValor(((IDictionary<string, object>)registro)[c.nome.ToLower()].ToString(), c));
                        }
                    }
                }

                if (linha != null && ((IDictionary<string, object>)linha).Count > 0)
                {
                    linha.indicadorgdsn = gdsn ? 1 : 0;
                    linha.CodigoTipoGTIN = codigoTipoGTIN;
                    linha.CodigoAssociado = associado.codigo;
                    linha.CodigoUsuarioCriacao = user.id;
                    linha.importClassificationType = 6;
                    linha.informationProvider = RecuperarGLNProvedorInformacao(db, associado);
                    linha.DataInclusao = DateTime.Now;
                    linha.NrPrefixo = RecuperarPrefixo(linha.globaltradeitemnumber.ToString(), tipoArquivo, associado.codigo);
                    linha.coditem = RecuperarNumeroItem(linha.globaltradeitemnumber.ToString(), linha.NrPrefixo);

                    db.Update(tablename, "globaltradeitemnumber", linha);

                    var retorno = db.Find("Produto", "globalTradeItemNumber=@globaltradeitemnumber", linha);

                    if (retorno != null)
                        return Convert.ToInt32(retorno.codigoproduto);
                }
            }

            return 0;
        }


        private object PegarValor(string valor, dynamic dicionario)
        {
            if (!string.IsNullOrEmpty(valor.Trim()) && dicionario != null)
            {
                string typeName = dicionario.tipo;
                Type tipo = null;

                switch (typeName.ToLower())
                {
                    case "int":
                    case "integer":
                    case "int16":
                    case "int32":
                        tipo = typeof(int);
                        break;
                    case "long":
                    case "int64":
                        tipo = typeof(long);
                        break;
                    case "string":
                    case "text":
                        tipo = typeof(string);
                        break;
                    case "datetime":
                        tipo = typeof(DateTime);
                        break;
                    case "decimal":
                        tipo = typeof(decimal);
                        break;
                }

                object o = null;

                if (tipo == typeof(DateTime))
                    o = DateTime.ParseExact(valor.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                else
                    o = Convert.ChangeType(valor.Trim(), tipo);

                if (typeName.ToLower().Equals("datetime"))
                    return Convert.ToDateTime(o).ToString("yyyy-MM-dd");
                else if (typeName.ToLower().Equals("decimal"))
                    return Convert.ToDecimal(o);

                return o;
            }
            else
                return valor.Trim();
        }

        private int RecuperarTipoGTIN(int tipo)
        {
            if (tipo > 0)
            {
                switch (tipo)
                {
                    case 12:
                        return 2;
                    case 13:
                        return 3;
                    case 14:
                        return 4;
                }
            }

            return tipo;
        }

        public int RecuperarTipoArquivoPorID(int id)
        {
            if (id >= 0)
            {
                switch (id)
                {
                    case 1:
                        return 12;
                    case 2:
                        return 13;
                    case 3:
                        return 14;
                    case 5:
                        return 0;
                }
            }

            return id;
        }

        private int RecuperarIDPorTipoArquivo(int tipo)
        {
            if (tipo >= 0)
            {
                switch (tipo)
                {
                    case 0:
                        return 5;
                    case 12:
                        return 1;
                    case 13:
                        return 2;
                    case 14:
                        return 3;
                }
            }

            return tipo;
        }

        internal object RecuperarRegistroPorDescricao(Database db, string tabela, string codigo, string descricao, string valor)
        {
            if (db != null && !string.IsNullOrEmpty(tabela) && !string.IsNullOrEmpty(valor))
            {
                dynamic param = new ExpandoObject();
                param.valor = valor;

                var condicao = $"{descricao} = @valor";
                if (GS1.CNP.BLL.Core.Util.IsInteger(valor.Replace(".", string.Empty)))
                {
                    if (tabela.ToLower().Equals("ncm"))
                        condicao =
                            $"cast(replace({codigo},\'.\',\'\') as int) = cast(replace(@valor,\'.\',\'\') as int)";
                    else if (tabela.ToLower().Equals("cest"))
                        condicao = "cast(replace(CEST,\'.\',\'\') as int) = cast(replace(@valor,\'.\',\'\') as int)";
                    else
                        condicao = $"{codigo} = cast(replace(@valor,\'.\',\'\') as int) OR {descricao} = cast(@valor as varchar)";
                }

                return db.Find(tabela, condicao, param, string.Format("{0} as codigo, {1} as descricao", codigo, descricao));
            }
            else
                return valor;
        }

        private long RecuperarPrefixo(string gtin, int tipoArquivo, long associado)
        {
            if (!string.IsNullOrEmpty(gtin))
            {
                string sql = @"SELECT *
                               FROM PREFIXOLICENCAASSOCIADO (Nolock) 
                               WHERE @gtin LIKE '%' + CAST(NUMEROPREFIXO AS VARCHAR) + '%'
                               AND codigoassociado = @associado
                               AND codigolicenca IN ({0})";

                string licenca = string.Empty;

                dynamic param = new ExpandoObject();
                param.gtin = gtin;
                param.associado = associado;

                switch (tipoArquivo)
                {
                    case 0:
                    case 13:
                        licenca = "1";
                        break;
                    case 12:
                        licenca = "3";
                        break;
                    case 14:
                        licenca = "1,2,3";
                        break;
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    List<dynamic> retorno = db.Select(string.Format(sql, licenca), param);

                    if (retorno != null && retorno.Count > 0)
                    {
                        dynamic prefixo = retorno.Where(x => x.codigostatusprefixo == 1 || x.codigostatusprefixo == 8).FirstOrDefault();

                        if (prefixo != null)
                        {
                            return prefixo != null ? prefixo.numeroprefixo : -1;
                        }
                    }
                }
            }

            return 0;
        }

        private string RecuperarNumeroItem(string gtin, long prefixo)
        {
            if (!string.IsNullOrEmpty(gtin) && prefixo > 0)
            {
                string pref = prefixo.ToString();
                string numeroItem = gtin.Substring(pref.Length + (gtin.Length == 14 ? 1 : 0), gtin.Length - (pref.Length + 1 + (gtin.Length == 14 ? 1 : 0)));

                return numeroItem;
            }

            return string.Empty;
        }

        private long RecuperarGLNProvedorInformacao(Database db, Associado associado)
        {
            if (db != null && associado != null)
            {
                dynamic param = new ExpandoObject();
                param.associado = associado.codigo;

                string condition = @"CODIGOASSOCIADO = @associado AND INDICADORPRINCIPAL = 1";

                dynamic retorno = db.Find("LOCALIZACAOFISICA", condition, param, "TOP 1 *");

                if (retorno != null)
                    return retorno.gln;
            }

            return 0;
        }

        private int RecuperarDigitoVerificador(string gtin)
        {
            if (gtin != null)
            {
                var digitoCalculo = gtin.Reverse().ToArray();
                var resultado = 0;

                for (var i = 0; i < digitoCalculo.Count(); i++)
                {
                    if ((i + 1) % 2 == 1)
                        resultado += (Convert.ToInt32(digitoCalculo[i].ToString()) * 3);
                    else
                        resultado += Convert.ToInt32(digitoCalculo[i].ToString());
                }

                int verificadorResultado = 10 - (resultado % 10);

                if (verificadorResultado == 10)
                    verificadorResultado = 0;

                return verificadorResultado;
            }
            else
                return -1;
        }

        private string RecuperarMensagemTemplateInvalido(int tipoArquivo)
        {
            if (tipoArquivo >= 0)
            {
                string nomeTemplate = RecuperarTipoItem(tipoArquivo);

                return string.Format("Esta planilha não corresponde a um template correto para importação. Verifique o tipo de item informado e a versão do template.", nomeTemplate);
            }

            return "O template utilizado não corresponde ao Tipo de GTIN presente na primeira linha de dados do arquivo.";
        }

        internal string RecuperarTipoItem(int tipoArquivo)
        {
            if (tipoArquivo >= 0)
            {
                switch (tipoArquivo)
                {
                    case 0:
                        return "GLN (Localizações Físicas)";
                    case 12:
                        return "GTIN-12 (Itens Comerciais USA/Canada)";
                    case 13:
                        return "GTIN-13 (Itens Comerciais)";
                    case 14:
                        return "GTIN-14 (Itens Logísticos)";
                }
            }

            return string.Empty;
        }

        [GS1CNPAttribute(true, true, "ImportarProduto", "VisualizarImportarProduto")]
        protected object Atualizar(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            DateTime dataInicial = DateTime.Now,
                dataFinal = DateTime.MinValue;
            TimeSpan tempoImportacao = TimeSpan.MinValue;

            Login user = this.getSession("USUARIO_LOGADO") as Login;
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            if (user != null)
            {
                if (parametro != null)
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        param = parametro.campos;
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        if (param != null && param.lista.Length > 0)
                        {
                            int codigoProduto = 0,
                                itensNaoImportados = 0,
                                tipoArquivo = RecuperarTipoArquivoPorID(Convert.ToInt32(param.tipoArquivo)),
                                padraoGDSN = Convert.ToInt32(param.padraoGDSN),
                                template = (tipoArquivo == 0 ? 0 : (tipoArquivo * 10) + padraoGDSN);

                            dynamic dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml"));

                            List<dynamic> dicionario = RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);

                            dynamic[] lista = param.lista as dynamic[];

                            string arquivoLog = string.Format("log_importacao_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd_HHmmss")),
                                ultimoGTIN = string.Empty;

                            if (!Directory.Exists(HttpContext.Current.Server.MapPath("/relatorios")))
                                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("/relatorios"));

                            using (ExcelPackage pck = new ExcelPackage(new FileInfo(HttpContext.Current.Server.MapPath(string.Format("/relatorios/{0}", arquivoLog)))))
                            {
                                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Relatório");

                                AdicionaCabecalho(ws, dicionario, template);

                                if (tipoArquivo != 0)
                                    lista = lista.OrderByDescending(x => x.gtin != null && !string.IsNullOrEmpty(x.gtin)).ToArray<dynamic>();

                                foreach (dynamic d in lista)
                                {
                                    if (ValidarRegistro(db, dicionario, d, associado, tipoArquivo, template))
                                    {
                                        d.errors = null;

                                        if (tipoArquivo > 0)
                                        {
                                            if (d.gtin != null && !string.IsNullOrEmpty(d.gtin))
                                                ultimoGTIN = d.gtin.ToString();

                                            codigoProduto = ImportarRegistroProduto(db, dicionario, d, tipoArquivo, RecuperarTipoGTIN(Convert.ToInt32(tipoArquivo)), padraoGDSN == 1, user, associado, ultimoGTIN);

                                            ImportarRegistroProdutoHierarquia(db, dicionario, d, codigoProduto, associado);

                                            ImportarRegistroProdutoURL(db, dicionario, d, codigoProduto);

                                            ImportarRegistroProdutoAgenciaReguladora(db, dicionario, d, codigoProduto);
                                        }
                                        else
                                            ImportarRegistroLocalizacaoFisica(db, dicionario, d, tipoArquivo, user, associado, string.Empty);
                                    }
                                    else
                                        itensNaoImportados++;
                                }

                                AdicionaErros(db, ws, dicionario, lista, padraoGDSN == 1);

                                pck.Save();
                            }

                            dataFinal = DateTime.Now;

                            tempoImportacao = dataFinal.Subtract(dataInicial);

                            int codigo = RegistrarImportacao(db, param.arquivo.ToString(), arquivoLog, dataInicial, lista.Length - itensNaoImportados, itensNaoImportados, 1, Convert.ToInt32(param.tipo), RecuperarIDPorTipoArquivo(tipoArquivo), tempoImportacao, user.id, associado);

                            if (codigo > 0)
                            {
                                string sql = @"SELECT A.CODIGO,
                                               A.ARQUIVORELATORIO,
                                               A.DATA,
                                               A.ITENSIMPORTADOS,
                                               B.DESCRICAO AS RESULTADO,
                                               C.DESCRICAO AS TIPOIMPORTACAO,
                                               D.DESCRICAO AS TIPOITEM,
                                               A.TEMPOIMPORTACAO,
                                               E.EMAIL AS USUARIO
                                               FROM IMPORTACAOPRODUTOS A
                                               INNER JOIN IMPORTACAORESULTADO B ON A.CODIGOIMPORTACAORESULTADO = B.CODIGO
                                               INNER JOIN IMPORTACAOTIPO C ON A.CODIGOIMPORTACAOTIPO = C.CODIGO
                                               INNER JOIN IMPORTACAOTIPOITEM D ON A.CODIGOIMPORTACAOTIPOITEM = D.CODIGO
                                               INNER JOIN USUARIO E ON A.CODIGOUSUARIOALTERACAO = E.CODIGO
                                               WHERE A.CODIGO = @codigo";

                                param = new ExpandoObject();
                                param.codigo = codigo;

                                List<dynamic> retorno = db.Select(sql, param);

                                if (retorno != null && retorno.Count > 0)
                                    return retorno[0];
                            }
                        }
                    }

                    return null;
                }
                else
                    throw new GS1TradeException("Não foram encontrados registros a serem importados.");
            }
            else
                throw new GS1TradeSessionException();
        }


        public void Dispose() { }
    }
}
