﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("IntegracaoBO")]
    public class IntegracaoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TOKEN"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTokens(dynamic parametro)
        {
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (associado != null)
            {
                string sql = @"SELECT CODIGO, CODIGOASSOCIADO, TOKEN, DESCRICAO, STATUS, CODIGOSTATUSTOKEN, CONVERT(VARCHAR(10),CONVERT(DATE, DATAEXPIRACAO,106),103) AS DATAEXPIRACAO
                                FROM TOKEN WITH (NOLOCK)
                                WHERE CODIGOASSOCIADO = '" + associado.codigo.ToString() + "'";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
                return null;
        }

        [GS1CNPAttribute("CadastrarIntegracao",true,true)]
        public object CadastrarToken(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();

                    Guid identificador = Guid.NewGuid();

                    parametro.campos.codigousuarioalteracao = user.id.ToString();
                    parametro.campos.codigoassociado = associado.codigo.ToString();
                    parametro.campos.token = identificador;
                    parametro.campos.status = 1;
                    parametro.campos.dataalteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                    parametro.campos.datacadastro = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");

                    //Converte campos data
                    if (parametro.campos.dataexpiracao != null)
                        parametro.campos.dataexpiracao = parametro.campos.dataexpiracao.ToString("MM/dd/yyyy HH:mm:ss.fff");

                    dynamic arrayIntegracoes = parametro.campos.arrayIntegracoes;
                    parametro.campos.Remove("arrayIntegracoes");

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    int idInserido = db.Insert(nomeTabela, nomePK, param, true);
                    dynamic result = ((List<dynamic>)db.Select("SELECT CODIGO, TOKEN FROM TOKEN WHERE CODIGO = " + idInserido, null)).FirstOrDefault();

                    if (idInserido != null)
                    {
                        Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, idInserido);
                    }

                    if (result != null)
                    {
                        try
                        {
                            //Inserção na tabela UsuárioAssociado
                            if (result.codigo.ToString() != null && arrayIntegracoes != null)
                            {
                                for (int i = 0; i < (((Newtonsoft.Json.Linq.JContainer)((arrayIntegracoes)))).Count; i++)
                                {
                                    string sqlIntegracao = "INSERT INTO TOKENFUNCIONALIDADEINTEGRACAO VALUES (" + result.codigo.ToString() + ", " + (((Newtonsoft.Json.Linq.JContainer)((arrayIntegracoes))))[i] + ", GETDATE()," + user.id.ToString() + ")";
                                    db.Select(sqlIntegracao, null);
                                }
                            }

                            db.Commit();
                        }
                        catch (Exception ex)
                        {
                            db.Rollback();
                            throw new GS1TradeException("Não foi possível gerar o token.");

                        }
                    }
                }

                return "";
            }
            else
            {
                return "Não foi possível realizar o cadastro.";
            }

        }

        [GS1CNPAttribute("EditarIntegracao", true, true)]
        public void AtualizarToken(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {

                    db.Open(true);
                    db.BeginTransaction();

                    dynamic param = new ExpandoObject();
                    param.codigo = Convert.ToInt64(parametro.where.ToString());

                    try
                    {
                        //Inserção na tabela TOKENHISTORICO
                        string inserthistorico = @"INSERT INTO TOKENHISTORICO (CODIGO, CODIGOASSOCIADO, TOKEN, DESCRICAO, STATUS, CODIGOSTATUSTOKEN, DATAALTERACAO,
                                                        DATACADASTRO, DATAEXPIRACAO, CODIGOUSUARIOALTERACAO, DATAHISTORICO)
                                                        SELECT CODIGO, CODIGOASSOCIADO, TOKEN, DESCRICAO, STATUS, CODIGOSTATUSTOKEN, DATAALTERACAO,
                                                            DATACADASTRO, DATAEXPIRACAO, CODIGOUSUARIOALTERACAO, GETDATE()
                                                            FROM TOKEN WITH (NOLOCK)
                                                            WHERE TOKEN.CODIGO = @codigo";

                        db.Insert(inserthistorico, param, false);

                        parametro.campos.dataalteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        parametro.campos.codigousuarioalteracao = user.id.ToString();

                        dynamic arrayIntegracoes = parametro.campos.arrayIntegracoes;
                        parametro.campos.Remove("arrayIntegracoes");

                        base.Update((object)parametro);

                        string sql_delete = "DELETE FROM TOKENFUNCIONALIDADEINTEGRACAO WHERE CODIGOTOKEN = " + param.codigo;
                        db.Select(sql_delete, null);

                        //Inserção na tabela TokenFuncionalidadeIntegracao
                        if (arrayIntegracoes != null)
                        {
                            for (int i = 0; i < (((Newtonsoft.Json.Linq.JContainer)((arrayIntegracoes)))).Count; i++)
                            {
                                string sqlIntegracao = "INSERT INTO TOKENFUNCIONALIDADEINTEGRACAO VALUES (" + param.codigo + ", " + (((Newtonsoft.Json.Linq.JContainer)((arrayIntegracoes))))[i] + ", GETDATE()," + user.id.ToString() + ")";
                                db.Select(sqlIntegracao, null);
                            }
                        }

                        db.Commit();
                    }
                    catch (Exception ex)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível realizar a atualização.", ex);
                    }
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a atualização.");
            }

        }

        [GS1CNPAttribute("ExcluirIntegracao", true, true)]
        public object RemoverToken(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                try
                {
                    string sql = "DELETE FROM TOKEN WHERE CODIGO = @codigo";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Exclusão não é permitida. O Token possui dados vinculados.", e);
                }
            }
            else
            {
                return "Não foi possível remover o item selecionado.";
            }

        }

        [GS1CNPAttribute(false)]
        public object BuscarToken(dynamic parametro)
        {
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (associado != null)
            {
                string sql = @"SELECT CODIGO, CODIGOASSOCIADO, TOKEN, DESCRICAO, STATUS, CODIGOSTATUSTOKEN, CONVERT(VARCHAR(10),CONVERT(DATE, DATAEXPIRACAO,106),103) AS DATAEXPIRACAO
                                    FROM TOKEN WITH (NOLOCK)
                                    WHERE CODIGOASSOCIADO = '" + associado.codigo.ToString() + "'";

                if (parametro != null && parametro.campos != null)
                {
                    if (parametro.campos.token != null)
                    {
                        sql += " AND token = @token";
                    }

                    if (parametro.campos.codigostatustoken != null)
                    {
                        sql += " AND codigostatustoken = @codigostatustoken";
                    }

                    if (parametro.campos.dataexpiracao != null)
                    {
                        sql += " AND @dataexpiracao = CONVERT(VARCHAR(10),CONVERT(DATE, DATEADD(YEAR, 1, CONVERT(DATETIME, DATAEXPIRACAO, 103)),106),103)";
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
                return null;
        }

        [GS1CNPAttribute(false)]
        public string BuscarFuncionalidadesIntegracao(dynamic parametro)
        {

            string sql = string.Empty;

            if (parametro != null && parametro.campos != null && parametro.campos.codigo != null)
            {

                sql = @"SELECT FI.CODIGO, FI.DESCRICAO, 
	                        (
	                            SELECT COUNT(*) FROM TOKENFUNCIONALIDADEINTEGRACAO Z
	                            WHERE (Z.CODIGOTOKEN = @codigo)
		                        AND Z.CODIGOFUNCIONALIDADEINTEGRACAO = FI.CODIGO
                            ) AS CHECKED
                        FROM FUNCIONALIDADEINTEGRACAO FI
                        WHERE FI.STATUS = 1";
            }
            else
            {
                sql = @"SELECT FI.CODIGO, FI.DESCRICAO, 0 AS CHECKED
                            FROM FUNCIONALIDADEINTEGRACAO FI
                            WHERE FI.STATUS = 1";

            }

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }


}
