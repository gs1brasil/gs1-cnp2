﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using System.IO;
using System.Dynamic;
using System.Xml;
using System.Runtime.Serialization;
using System.Data.SqlClient;


namespace GS1.CNP.BLL.Backoffice
{
    public class IntegracaoCrmBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "Associado"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        public List<CADAssociado> BuscarTodosAssociados()
        {
            string sql = @"SELECT A.CAD FROM ASSOCIADO A (NOlock) 
                            WHERE A.CAD IS NOT NULL AND A.CODIGOSTATUSASSOCIADO = 1";


            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                List<dynamic> associados = db.Select(sql, param);

                List<CADAssociado> myDeserializedObjList = (List<CADAssociado>)Newtonsoft.Json.JsonConvert.DeserializeObject(JsonConvert.SerializeObject(associados), typeof(List<CADAssociado>));

                return myDeserializedObjList;
            }
        }
       
        public List<UsuarioCRM> BuscarAssociadosAtualizadosCRM()
        {

            using (GS1.CNP.DAL.Database dbbaseunica = new GS1.CNP.DAL.Database("BDBASEUNICACRM"))
            {
                
                dbbaseunica.Open(true);

                Func<IDataRecord, UsuarioCRM> usuarioCRM = r => new UsuarioCRM()
                {
                    id = r.GetGuid(0)
                    ,nome = r.GetString(1)
                    ,nomefantasia = (r.IsDBNull(2) ? string.Empty : r.GetString(2))
                    ,relacionamento = (r.IsDBNull(3) ? string.Empty : r.GetInt32(3).ToString())
                    ,cad = (r.IsDBNull(4) ? string.Empty : r.GetString(4))
                    ,cpfcnpj = r.GetString(5)
                    ,email = r.GetString(6)                    
                    ,bl_adimplente = (r.IsDBNull(7) ? Convert.ToBoolean(0) : Convert.ToBoolean(r.GetInt32(7))) 
                    ,bl_premium = (r.IsDBNull(8) ? Convert.ToBoolean(0) : Convert.ToBoolean(r.GetInt32(8)))                     
                    ,inscricaoestadual = (r.IsDBNull(9) ? string.Empty : r.GetString(9))
                    ,estadoname = (r.IsDBNull(10) ? string.Empty : r.GetString(10))
                    ,numero = (r.IsDBNull(11) ? string.Empty : r.GetString(11))
                    ,address1_city = (r.IsDBNull(12) ? string.Empty : r.GetString(12))
                    ,logradouroprincipal = (r.IsDBNull(13) ? string.Empty : r.GetString(13))
                    ,complementoprincipal = (r.IsDBNull(14) ? string.Empty : r.GetString(14))
                    ,address1_postalcode = (r.IsDBNull(15) ? string.Empty : r.GetString(15))
                    ,bairroprincipal = (r.IsDBNull(16) ? string.Empty : r.GetString(16))
                    ,representante_smart_pessoaname = (r.IsDBNull(17) ? string.Empty : r.GetString(17))
                    ,representante_smart_cpf = (r.IsDBNull(18) ? string.Empty : r.GetString(18))
                    ,representante_telephone1 = (r.IsDBNull(19) ? string.Empty : r.GetString(19))
                    ,representante_telephone2 = (r.IsDBNull(20) ? string.Empty : r.GetString(20))
                    ,representante_telephone3 = (r.IsDBNull(21) ? string.Empty : r.GetString(21))
                    ,representante_emailaddress1 = (r.IsDBNull(22) ? string.Empty : r.GetString(22))
                    ,representante_emailaddress2 = (r.IsDBNull(23) ? string.Empty : r.GetString(23))
                    ,glninformativo = (r.IsDBNull(24) ? string.Empty : r.GetString(24))
                    ,pais = (r.IsDBNull(25) ? string.Empty : r.GetString(25))
                    ,telephone1 = (r.IsDBNull(26) ? string.Empty : r.GetString(26))
                    ,smart_ddidddtelefoneprincipal = (r.IsDBNull(27) ? string.Empty : r.GetString(27))
                    ,datamodificacao = (r.IsDBNull(28) ? DateTime.MinValue : r.GetDateTime(28))
                    ,gs1br_cnp20 = (r.IsDBNull(29) ? false : r.GetBoolean(29))

                };

                string sqlBaseUnicaCRM = @"SELECT [ACCOUNTID]
                                                  ,[NAME]
                                                  ,[SMART_NOMEFANTASIA]
                                                  ,[SMART_CONDICAODERELACIONAMENTO]
                                                  ,[ACCOUNTNUMBER]
                                                  ,[SMART_CNPJCPF]
                                                  ,[EMAIL]
                                                  ,[STATUS_ADIMPLENTE]
                                                  ,[STATUS_PREMIUM]
                                                  ,[SMART_INSCRICAOESTADUAL]
                                                  ,[SMART_ESTADONAME]
                                                  ,[SMART_NUMERO]
                                                  ,[ADDRESS1_CITY]
                                                  ,[SMART_LOGRADOUROPRINCIPAL]
                                                  ,[SMART_COMPLEMENTOPRINCIPAL]
                                                  ,[ADDRESS1_POSTALCODE]
                                                  ,[SMART_BAIRROPRINCIPAL]
                                                  ,[SMART_PESSOANAME]
                                                  ,[SMART_CPF]
                                                  ,[representante_telephone1]
                                                  ,[representante_telephone2]
                                                  ,[representante_telephone3]
                                                  ,[EMAILADDRESS1]
                                                  ,[EMAILADDRESS2]
                                                  ,[SMART_CODIGOGLN]
                                                  ,[SMART_PAISNAME]
                                                  ,[TELEPHONE1]
                                                  ,[SMART_DDIDDDTELEFONEPRINCIPAL]
                                                  ,[DATA DE MODIFICAÇÃO]
                                                  ,[gs1br_cnp20]
                                      FROM [CRM].[dbo].[vw_apenas_associados_atualizados] (Nolock)
                                        WHERE [EMAIL] IS NOT NULL
                                        AND [SMART_CNPJCPF] IS NOT NULL
                                        AND [NAME] IS NOT NULL";

                dynamic paramBaseUnicaCRM = new ExpandoObject();
                
                List<UsuarioCRM> dadosassociado = dbbaseunica.Select(sqlBaseUnicaCRM, paramBaseUnicaCRM, usuarioCRM);
                
                return dadosassociado;
            }
        }

        public List<UsuarioCRM> BuscarAssociadosAtualizadosCRM(string CAD)
        {

            using (GS1.CNP.DAL.Database dbbaseunica = new GS1.CNP.DAL.Database("BDBASEUNICACRM"))
            {

                dbbaseunica.Open(true);

                Func<IDataRecord, UsuarioCRM> usuarioCRM = r => new UsuarioCRM()
                {
                    id = r.GetGuid(0)
                    ,nome = r.GetString(1)
                    ,nomefantasia = (r.IsDBNull(2) ? string.Empty : r.GetString(2))
                    ,relacionamento = (r.IsDBNull(3) ? string.Empty : r.GetInt32(3).ToString())
                    ,cad = (r.IsDBNull(4) ? string.Empty : r.GetString(4))
                    ,cpfcnpj = r.GetString(5)
                    ,email = r.GetString(6)
                    ,bl_adimplente = (r.IsDBNull(7) ? Convert.ToBoolean(0) : Convert.ToBoolean(r.GetInt32(7)))
                    ,bl_premium = (r.IsDBNull(8) ? Convert.ToBoolean(0) : Convert.ToBoolean(r.GetInt32(8)))
                    ,inscricaoestadual = (r.IsDBNull(9) ? string.Empty : r.GetString(9))
                    ,estadoname = (r.IsDBNull(10) ? string.Empty : r.GetString(10))
                    ,numero = (r.IsDBNull(11) ? string.Empty : r.GetString(11))
                    ,address1_city = (r.IsDBNull(12) ? string.Empty : r.GetString(12))
                    ,logradouroprincipal = (r.IsDBNull(13) ? string.Empty : r.GetString(13))
                    ,complementoprincipal = (r.IsDBNull(14) ? string.Empty : r.GetString(14))
                    ,address1_postalcode = (r.IsDBNull(15) ? string.Empty : r.GetString(15))
                    ,bairroprincipal = (r.IsDBNull(16) ? string.Empty : r.GetString(16))
                    ,representante_smart_pessoaname = (r.IsDBNull(17) ? string.Empty : r.GetString(17))
                    ,representante_smart_cpf = (r.IsDBNull(18) ? string.Empty : r.GetString(18))
                    ,representante_telephone1 = (r.IsDBNull(19) ? string.Empty : r.GetString(19))
                    ,representante_telephone2 = (r.IsDBNull(20) ? string.Empty : r.GetString(20))
                    ,representante_telephone3 = (r.IsDBNull(21) ? string.Empty : r.GetString(21))
                    ,representante_emailaddress1 = (r.IsDBNull(22) ? string.Empty : r.GetString(22))
                    ,representante_emailaddress2 = (r.IsDBNull(23) ? string.Empty : r.GetString(23))
                    ,glninformativo = (r.IsDBNull(24) ? string.Empty : r.GetString(24))
                    ,pais = (r.IsDBNull(25) ? string.Empty : r.GetString(25))
                    ,telephone1 = (r.IsDBNull(26) ? string.Empty : r.GetString(26))
                    ,smart_ddidddtelefoneprincipal = (r.IsDBNull(27) ? string.Empty : r.GetString(27))
                    ,gs1br_cnp20 = (r.IsDBNull(28) ? false : r.GetBoolean(28))
                    ,datamodificacao = (r.IsDBNull(29) ? DateTime.MinValue : r.GetDateTime(29))
                };

//                string sqlBaseUnicaCRM = @"SELECT [ACCOUNTID]
//                                                  ,[NAME]
//                                                  ,[SMART_NOMEFANTASIA]
//                                                  ,[SMART_CONDICAODERELACIONAMENTO]
//                                                  ,[ACCOUNTNUMBER]
//                                                  ,[SMART_CNPJCPF]
//                                                  ,[EMAIL]
//                                                  ,[STATUS_ADIMPLENTE]
//                                                  ,[STATUS_PREMIUM]
//                                                  ,[SMART_INSCRICAOESTADUAL]
//                                                  ,[SMART_ESTADONAME]
//                                                  ,[SMART_NUMERO]
//                                                  ,[ADDRESS1_CITY]
//                                                  ,[SMART_LOGRADOUROPRINCIPAL]
//                                                  ,[SMART_COMPLEMENTOPRINCIPAL]
//                                                  ,[ADDRESS1_POSTALCODE]
//                                                  ,[SMART_BAIRROPRINCIPAL]
//                                                  ,[SMART_PESSOANAME]
//                                                  ,[SMART_CPF]
//                                                  ,[representante_telephone1]
//                                                  ,[representante_telephone2]
//                                                  ,[representante_telephone3]
//                                                  ,[EMAILADDRESS1]
//                                                  ,[EMAILADDRESS2]
//                                                  ,[SMART_CODIGOGLN]
//                                                  ,[SMART_PAISNAME]
//                                                  ,[TELEPHONE1]
//                                                  ,[SMART_DDIDDDTELEFONEPRINCIPAL]
//                                                  ,[DATA DE MODIFICAÇÃO]
//                                                  ,[gs1br_cnp20]
//                                      FROM [CRM].[dbo].[vw_apenas_associados_atualizados]
//                                        WHERE [EMAIL] IS NOT NULL
//                                        AND [SMART_CNPJCPF] IS NOT NULL
//                                        AND [NAME] IS NOT NULL
//                                        AND [ACCOUNTNUMBER] = '" + CAD + @"'";

                string sqlBaseUnicaCRM = @"SELECT TOP 1 A.ACCOUNTID
		                                                ,A.NAME
		                                                ,A.SMART_NOMEFANTASIA		
		                                                ,A.SMART_CONDICAODERELACIONAMENTO
		                                                ,A.ACCOUNTNUMBER
		                                                ,A.SMART_CNPJCPF
		                                                ,A.EMAILADDRESS1
		                                                ,CASE WHEN EXISTS (
						                                                SELECT	Z.SMART_GESTAODELICENCASEPRODUTOID,
								                                                Z.SMART_NAME,
								                                                Z.SMART_FINANCEIROSTATUS,
								                                                Z.SMART_ASSOCIADO                  
						                                                FROM SMART_GESTAODELICENCASEPRODUTO Z WITH(NOLOCK)
						                                                WHERE Z.SMART_ASSOCIADO = A.ACCOUNTID 
						                                                AND Z.SMART_FINANCEIROSTATUS NOT IN (1,3)
					                                                ) THEN 1 ELSE 0
		                                                END AS STATUS_ADIMPLENTE
		                                                , COALESCE ((
						                                                SELECT TOP 1 1                                 
						                                                FROM SMART_LICENCA X (NOLOCK)
						                                                WHERE X.SMART_EMPRESA = A.ACCOUNTID
						                                                AND X.SMART_NEGOCIONAME LIKE '%EPC%'
						                                                AND X.SMART_PAP_STATUS = 2
					                                                ),0) AS STATUS_PREMIUM
                                                        ,A.SMART_INSCRICAOESTADUAL
                                                        ,A.SMART_ESTADONAME
                                                        ,A.SMART_NUMERO
                                                        ,A.ADDRESS1_CITY
                                                        ,A.SMART_LOGRADOUROPRINCIPAL
                                                        ,A.SMART_COMPLEMENTOPRINCIPAL
                                                        ,A.ADDRESS1_POSTALCODE
                                                        ,A.SMART_BAIRROPRINCIPAL
                                                        ,B.SMART_PESSOANAME
                                                        ,B.SMART_CPF
                                                        ,B.TELEPHONE1
                                                        ,B.TELEPHONE2
                                                        ,B.TELEPHONE3
                                                        ,B.EMAILADDRESS1
                                                        ,B.EMAILADDRESS2
                                                        ,A.SMART_CODIGOGLN
	                                                    ,A.SMART_PAISNAME
	                                                    ,A.TELEPHONE1
	                                                    ,A.SMART_DDIDDDTELEFONEPRINCIPAL
	                                                    ,A.gs1br_cnp20
                                                        ,a.ModifiedOn [DATA DE MODIFICAÇÃO]

                                                    FROM ACCOUNT A (NOLOCK)
                                                LEFT JOIN CONTACT B ON B.ACCOUNTID = A.ACCOUNTID AND B.SMART_REPRESENTANTELEGAL = 1
                                                WHERE a.EMAILADDRESS1 IS NOT NULL
                                                AND [SMART_CNPJCPF] IS NOT NULL
                                                AND [NAME] IS NOT NULL
                                        AND [ACCOUNTNUMBER] = '" + CAD + @"'";

                dynamic paramBaseUnicaCRM = new ExpandoObject();

                List<UsuarioCRM> dadosassociado = dbbaseunica.Select(sqlBaseUnicaCRM, paramBaseUnicaCRM, usuarioCRM);
                return dadosassociado;
            }
        }

        public List<PrefixoLicencaAssociadoCRM> BuscarAssociadoLicencaPrefixoAtuallizadosCRM()
        {

            using (GS1.CNP.DAL.Database dbbaseunica = new GS1.CNP.DAL.Database("BDBASEUNICACRM"))
            {
             
                Func<IDataRecord, PrefixoLicencaAssociadoCRM> prefixolicencaassociadoCRM = r => new PrefixoLicencaAssociadoCRM()
                {
                    prefixo = (r.IsDBNull(0) ? string.Empty : r.GetString(0))
                    ,data_aniversario = (r.IsDBNull(1) ? DateTime.MinValue : r.GetDateTime(1))
                    ,flagprincipal = (r.IsDBNull(2) ? 0 : r.GetInt32(2))
                    ,licenca = (r.IsDBNull(3) ? string.Empty : r.GetString(3))
                    ,negocio = (r.IsDBNull(4) ? string.Empty : r.GetString(4))
                    ,descricaogtin8 = (r.IsDBNull(5) ? string.Empty : r.GetString(5))
                    ,qtde_digitos = (r.IsDBNull(6) ? 0 : r.GetInt32(6))
                    ,stcadastral = (r.IsDBNull(7) ? 0 : r.GetInt32(7))
                    ,stfinanceiro = (r.IsDBNull(8) ? 0 : r.GetInt32(8))
                    ,datamodificacao = (r.IsDBNull(9) ? DateTime.MinValue: r.GetDateTime(9))
                    ,associado = (r.IsDBNull(10) ? string.Empty : r.GetString(10))
                    ,status_da_licenca = (r.IsDBNull(11) ? 0 : r.GetInt32(11))
                    ,CAD = (r.IsDBNull(12) ? string.Empty : r.GetString(12))
                    ,id = r.GetGuid(13)
                };
                                                            
                dbbaseunica.Open(true);
                string sqlBaseUnicaCRM = @"SELECT [PREFIXO]
                                              ,[DATA_ANIVERSARIO]
                                              ,[FLAGPRINCIPAL]
                                              ,[LICENCA]
                                              ,[NEGOCIO]
                                              ,[DESCRIÇÃO_GTIN8]
                                              ,[QTDE_DIGITOS]
                                              ,[STCADASTRAL]
                                              ,[STFINANCEIRO]
                                              ,[DATA DE MODIFICAÇÃO]
                                              ,[ASSOCIADO]
	                                          ,[STATUS_DA_LICENÇA]
	                                          ,[CAD]
	                                          ,[ID]
                                          FROM [dbo].[vw_LICENCAS_atualizadas] (NOlock) 
                                          WHERE [STATUS_DA_LICENÇA] IS NOT NULL
                                          AND PREFIXO IS NOT NULL
										  AND PREFIXO <> ''
										  AND ISNUMERIC (PREFIXO) = 1";

                dynamic paramBaseUnicaCRM = new ExpandoObject();
                
                List<PrefixoLicencaAssociadoCRM> dadosassociado = dbbaseunica.Select(sqlBaseUnicaCRM, paramBaseUnicaCRM, prefixolicencaassociadoCRM);
                return dadosassociado;
            }
        }

        public List<PrefixoLicencaAssociadoCRM> BuscarAssociadoLicencaPrefixoAtuallizadosCRM(string CAD)
        {

            using (GS1.CNP.DAL.Database dbbaseunica = new GS1.CNP.DAL.Database("BDBASEUNICACRM"))
            {

                Func<IDataRecord, PrefixoLicencaAssociadoCRM> prefixolicencaassociadoCRM = r => new PrefixoLicencaAssociadoCRM()
                {
                    prefixo = (r.IsDBNull(0) ? string.Empty : r.GetString(0))
                    ,data_aniversario = (r.IsDBNull(1) ? DateTime.MinValue : r.GetDateTime(1))
                    ,flagprincipal = (r.IsDBNull(2) ? 0 : r.GetInt32(2))
                    ,licenca = (r.IsDBNull(3) ? string.Empty : r.GetString(3))
                    ,negocio = (r.IsDBNull(4) ? string.Empty : r.GetString(4))
                    ,descricaogtin8 = (r.IsDBNull(5) ? string.Empty : r.GetString(5))
                    ,qtde_digitos = (r.IsDBNull(6) ? 0 : r.GetInt32(6))
                    ,stcadastral = (r.IsDBNull(7) ? 0 : r.GetInt32(7))
                    ,stfinanceiro = (r.IsDBNull(8) ? 0 : r.GetInt32(8))
                    ,datamodificacao = (r.IsDBNull(9) ? DateTime.MinValue: r.GetDateTime(9))
                    ,associado = (r.IsDBNull(10) ? string.Empty : r.GetString(10))
                    ,status_da_licenca = (r.IsDBNull(11) ? 0 : r.GetInt32(11))
                    ,CAD = (r.IsDBNull(12) ? string.Empty : r.GetString(12))
                    ,id = r.GetGuid(13)
                };

                dbbaseunica.Open(true);
//                string sqlBaseUnicaCRM = @"SELECT [PREFIXO]
//                                              ,[DATA_ANIVERSARIO]
//                                              ,[FLAGPRINCIPAL]
//                                              ,[LICENCA]
//                                              ,[NEGOCIO]
//                                              ,[QTDE_DIGITOS]
//                                              ,[STCADASTRAL]
//                                              ,[STFINANCEIRO]
//                                              ,[DATA DE MODIFICAÇÃO]
//                                              ,[ASSOCIADO]
//	                                          ,[STATUS_DA_LICENÇA]
//	                                          ,[CAD]
//	                                          ,[ID]
//                                          FROM [dbo].[vw_LICENCAS_atualizadas]
//                                          WHERE [CAD] = '" + CAD + @"' AND [STATUS_DA_LICENÇA] IS NOT NULL";

                string sqlBaseUnicaCRM = @"SELECT DISTINCT SMART_PAP_CODLIC AS PREFIXO
                                                        ,SMART_PAI_DATASN           AS DATA_ANIVERSARIO
                                                        ,PRODUCTTYPECODE            AS FLAGPRINCIPAL
                                                        ,SMART_CODIGOATRIBUICAO     AS LICENCA
	                                                    ,SMART_NEGOCIONAME          AS NEGOCIO
                                                        ,smart_produtodogtin8    AS DESCRIÇÃO_GTIN8
                                                        ,SMART_PAP_QTDDIG           AS QTDE_DIGITOS                              
                                                        ,SMART_STATUSGLP            AS STCADASTRAL
                                                        ,SMART_FINANCEIROSTATUS     AS STFINANCEIRO
                                                        ,SL.ModifiedOn              as [DATA DE MODIFICAÇÃO]                                        
                                                        ,smart_empresaName		    AS ASSOCIADO
	                                                    ,SMART_PAP_STATUS		    AS STATUS_DA_LICENCA
	                                                    ,A.ACCOUNTNUMBER		    AS CAD
	                                                    ,ACCOUNTID                  AS ID
                                                    FROM SMART_LICENCA SL (NOLOCK)
                                                    INNER JOIN SMART_GESTAODELICENCASEPRODUTO SG (NOLOCK) ON SL.SMART_CONTRATO = SG.SMART_GESTAODELICENCASEPRODUTOID
                                                    INNER JOIN PRODUCT P (NOLOCK) ON SL.SMART_NEGOCIO = P.PRODUCTID
                                                    INNER JOIN ACCOUNT A (NOLOCK) ON SL.SMART_EMPRESA = A.AccountId 
                                                    WHERE SMART_PAP_CODLIC IS NOT NULL AND SMART_PAP_CODLIC <> '' AND ISNUMERIC (SMART_PAP_CODLIC) = 1
                                                    AND ((SMART_CODIGOATRIBUICAO = 'GTIN-8' AND SMART_PRODUTODOGTIN8 IS NOT NULL) OR SMART_CODIGOATRIBUICAO <> 'GTIN-8')
                                                    AND SMART_PAP_STATUS IS NOT NULL
                                                    AND A.ACCOUNTNUMBER = '" + CAD + @"'";

                dynamic paramBaseUnicaCRM = new ExpandoObject();

                List<PrefixoLicencaAssociadoCRM> dadosassociado = dbbaseunica.Select(sqlBaseUnicaCRM, paramBaseUnicaCRM, prefixolicencaassociadoCRM);
                return dadosassociado;
            }
        }

        //public string UpdateUsuariosCRM()
        //{
        //    using (Database db = new Database("BD"))
        //    {
        //        using (GS1.CNP.DAL.Database dbbaseunica = new GS1.CNP.DAL.Database("BDBASEUNICACRM"))
        //        {
        //            dbbaseunica.Open();
        //            dbbaseunica.ExecuteProcedure("PROC_JOB_SINCRONIZACAO_CNP20", null);
                   
        //        }

        //        //db.Open();
        //        //dynamic param2 = new ExpandoObject();
        //        //param2.xmlassociados = SerializeAssociados(BuscarAssociadosAtualizadosCRM());
        //        //param2.xmllicencas = SerializeLicencas(BuscarAssociadoLicencaPrefixoAtuallizadosCRM());                
        //        //object retorno = db.ExecuteProcedure("PROC_JOB_SINCRONIZACAO_CRM", param2);

        //        db.Open();
        //        object retorno = db.ExecuteProcedure("PROC_JOB_SINCRONIZACAO_CRM_V2", null);
               

        //        return JsonConvert.SerializeObject(retorno);
        //    }
        //}


        public string UpdateUsuariosCRM()
        {
            using (Database db = new Database("BD"))
            {
                dynamic param = new ExpandoObject();                               
                db.Open();
                dynamic dataAtualizacao = db.Find("Parametro", "chave = 'parametros.integracoes.ultimasincronizacaocrmrgc'", param, "convert(datetime,valor) as valor");                

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["BDBASEUNICACRM"].ConnectionString;
                using (SqlConnection sourceConnection = new SqlConnection(connectionString))
                {       
                    sourceConnection.Open();
                    SqlCommand commandSourceData = new SqlCommand(@" select a.[accountid]
				                                                            ,a.[name]
				                                                            ,a.[smart_nomefantasia]
				                                                            ,a.[smart_condicaoderelacionamento]
				                                                            ,a.[accountnumber]
				                                                            ,a.[smart_cnpjcpf]
				                                                            ,a.[email]
				                                                            ,a.[status_adimplente]
				                                                            ,a.[status_premium]
				                                                            ,a.[smart_inscricaoestadual]
				                                                            ,a.[smart_estadoname]
				                                                            ,a.[smart_numero]
				                                                            ,a.[address1_city]
				                                                            ,a.[smart_logradouroprincipal]
				                                                            ,a.[smart_complementoprincipal]
				                                                            ,a.[address1_postalcode]
				                                                            ,a.[smart_bairroprincipal]
				                                                            ,a.[smart_pessoaname]
				                                                            ,a.[smart_cpf]
				                                                            ,a.[representante_telephone1]
				                                                            ,a.[representante_telephone2]
				                                                            ,a.[representante_telephone3]
				                                                            ,a.[emailaddress1]
				                                                            ,a.[emailaddress2]
				                                                            ,a.[smart_codigogln]
				                                                            ,a.[smart_paisname]
				                                                            ,a.[telephone1]
				                                                            ,a.[smart_ddidddtelefoneprincipal]
				                                                            ,a.[data de modificação]
				                                                            ,a.[gs1br_cnp20]
	                                                            from [crm].[dbo].[vw_apenas_associados_atualizados] a	
	                                                            where a.[email] is not null
	                                                            and a.[smart_cnpjcpf] is not null
	                                                            and a.[name] is not null
   	                                                            and a.[data de modificação] > FORMAT(@dataatualizacaocrm , 'yyyy-MM-dd HH:mm:ss:fff')", sourceConnection);


                    SqlParameter paramCRM = new SqlParameter();
                    paramCRM.ParameterName = "@dataatualizacaocrm";
                    paramCRM.Value = dataAtualizacao.valor;
                    commandSourceData.Parameters.Add(paramCRM);
                    
                    SqlDataReader reader = commandSourceData.ExecuteReader();


                    db.ExecuteCommand("TRUNCATE TABLE vw_apenas_associados_atualizados;", null);

                    string connectionStringDestino = System.Configuration.ConfigurationManager.ConnectionStrings["BD"].ConnectionString;
                    using (SqlConnection destinationConnection = new SqlConnection(connectionStringDestino))
                    {
                        destinationConnection.Open();

                        using (SqlBulkCopy bulkCopy =
                               new SqlBulkCopy(destinationConnection))
                        {
                            bulkCopy.DestinationTableName =
                                "dbo.vw_apenas_associados_atualizados";

                            try
                            {                                
                                bulkCopy.WriteToServer(reader);
                                destinationConnection.Close();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            finally
                            {
                                reader.Close();
                                sourceConnection.Close();
                                destinationConnection.Close();
                            }
                        }
                    }

                    sourceConnection.Close();
                }

                
                using (SqlConnection sourceConnection = new SqlConnection(connectionString))
                {
                    sourceConnection.Open();
                    SqlCommand commandSourceData = new SqlCommand(@" select   a.[prefixo]
			                                                            ,a.[data_aniversario]
			                                                            ,a.[flagprincipal]
			                                                            ,a.[licenca]
			                                                            ,a.[negocio]
			                                                            ,a.[descrição_gtin8]
			                                                            ,a.[qtde_digitos]
			                                                            ,a.[stcadastral]
			                                                            ,a.[stfinanceiro]		
			                                                            ,a.[associado]
			                                                            ,a.[status_da_licença]
			                                                            ,a.[cad]
			                                                            ,a.[id]
			                                                            ,a.[data de modificação]			
	                                                            from [dbo].[vw_licencas_atualizadas] a
	                                                            where [status_da_licença] is not null
	                                                            and prefixo is not null
	                                                            and prefixo <> ''
	                                                            and isnumeric (prefixo) = 1
	                                                            and a.[data de modificação] > FORMAT(@dataatualizacaocrm , 'yyyy-MM-dd HH:mm:ss:fff')", sourceConnection);


                    SqlParameter paramCRM = new SqlParameter();
                    paramCRM.ParameterName = "@dataatualizacaocrm";
                    paramCRM.Value = dataAtualizacao.valor;
                    commandSourceData.Parameters.Add(paramCRM);

                    SqlDataReader reader = commandSourceData.ExecuteReader();


                    db.ExecuteCommand("TRUNCATE TABLE vw_licencas_atualizadas;", null);

                    string connectionStringDestino = System.Configuration.ConfigurationManager.ConnectionStrings["BD"].ConnectionString;
                    using (SqlConnection destinationConnection = new SqlConnection(connectionStringDestino))
                    {
                        destinationConnection.Open();

                        using (SqlBulkCopy bulkCopy =
                               new SqlBulkCopy(destinationConnection))
                        {
                            bulkCopy.DestinationTableName =
                                "dbo.vw_licencas_atualizadas";

                            try
                            {
                                bulkCopy.WriteToServer(reader);
                                destinationConnection.Close();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            finally
                            {
                                reader.Close();
                                sourceConnection.Close();
                                destinationConnection.Close();
                            }
                        }
                    }

                    sourceConnection.Close();
                }







                db.Open();
                object retorno = db.ExecuteProcedure("PROC_JOB_SINCRONIZACAO_CRM_V2", null);
                return JsonConvert.SerializeObject(retorno);
            }
        }



        public object UpdateUsuariosCRM(string CAD)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param2 = new ExpandoObject();
                param2.xmlassociados = SerializeAssociados(BuscarAssociadosAtualizadosCRM(CAD));
                param2.xmllicencas = SerializeLicencas(BuscarAssociadoLicencaPrefixoAtuallizadosCRM(CAD));

                object retorno = db.ExecuteProcedure("PROC_JOB_SINCRONIZACAO_CRM", param2);

                return JsonConvert.SerializeObject(retorno);
            }
        }

        public struct CADAssociado
        {
            public string cad { get; set; }

        }

        public class PrefixoLicencaAssociadoCRM
        {
            public string prefixo { get; set; }
            public DateTime? data_aniversario { get; set; }
            public int flagprincipal { get; set; }
            public string licenca { get; set; }
            public string negocio { get; set; }
            public int? qtde_digitos { get; set; }
            public int? stcadastral { get; set; }
            public int? stfinanceiro { get; set; }
            
            public string associado { get; set; }
	        public int? status_da_licenca { get; set; }
            public string CAD { get; set; }
            public Guid id { get; set; }
            public DateTime? datamodificacao { get; set; }

            public string descricaogtin8 { get; set; }

        }

        private string SerializeAssociados(List<UsuarioCRM> records)
        {
            string retVal = null;
            using (var tw = new StringWriter())
            using (var xw = new XmlTextWriter(tw))
            {
                var ser = new DataContractSerializer(typeof(List<UsuarioCRM>));
                ser.WriteObject(xw, records);
                retVal = tw.ToString();
            }
            return retVal;
        }

        private string SerializeLicencas(List<PrefixoLicencaAssociadoCRM> records)
        {
            string retVal = null;
            using (var tw = new StringWriter())
            using (var xw = new XmlTextWriter(tw))
            {
                var ser = new DataContractSerializer(typeof(List<PrefixoLicencaAssociadoCRM>));
                ser.WriteObject(xw, records);
                retVal = tw.ToString();
            }
            return retVal;
        }
       
    }

}


