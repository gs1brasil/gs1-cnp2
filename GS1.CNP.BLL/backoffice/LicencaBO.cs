﻿using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;


namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("LicencaBO")]
    class LicencaBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "Licenca"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        [GS1CNPAttribute(false)]
        public object buscarLicencasAtivasAssociado(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null) 
            {
                string sql = @"SELECT DISTINCT L.CODIGO, L.NOME, L.DESCRICAO, L.STATUS 
                                FROM LICENCA L WITH (NOLOCK) 
                                INNER JOIN LICENCAASSOCIADO LA ON (LA.CODIGOLICENCA = L.CODIGO)
                                WHERE L.STATUS = 1
                                AND LA.CODIGOASSOCIADO = " + associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> consulta = db.Select(sql, param);

                    return JsonConvert.SerializeObject(consulta);
                }
            }

            return null;
        }

        [GS1CNPAttribute(false)]
        public object buscarTipoGtinAssociado(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null) 
            {
                string sql = @"SELECT DISTINCT L.CODIGO AS CODIGOLICENCA, L.NOME AS NOMELICENCA, L.DESCRICAO, TG.NOME AS NOME, TG.CODIGO AS CODIGO
                                FROM LICENCA L WITH (NOLOCK) 
                                INNER JOIN LICENCAASSOCIADO LA (NOLOCK) ON (LA.CODIGOLICENCA = L.CODIGO)
				                INNER JOIN TIPOGTINLICENCA T (NOLOCK)  ON (T.CODIGOLICENCA = L.CODIGO)
				                INNER JOIN TIPOGTIN TG (NOLOCK)   ON (TG.CODIGO = T.CODIGOTIPOGTIN)
                                WHERE L.STATUS = 1
                                AND LA.CODIGOASSOCIADO = " + associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> consulta = db.Select(sql, param);

                    return JsonConvert.SerializeObject(consulta);
                }
            }

            return null;
        }
    }
}
