﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("EtiquetaBO")]
    public class LinguaBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "LINGUA"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarLinguagensAtivas(dynamic parametro) 
        {
            string sql = @"SELECT * FROM lingua (nolock) WHERE Status = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql);
                return JsonConvert.SerializeObject(retorno);
            }
        
        }
    }
}
