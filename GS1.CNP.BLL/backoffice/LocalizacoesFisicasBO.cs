﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using System.Data;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using GS1.CNP.BLL.Model;
using System.IO;
using System.Web;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("LocalizacoesFisicasBO")]
    public class LocalizacoesFisicasBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "LOCALIZACAOFISICA"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        internal string GeraDigitoVerificador(string gln)
        {
            if (gln != null)
            {
                var digitoCalculo = gln.Reverse().ToArray();
                var resultado = 0;

                for (var i = 0; i < digitoCalculo.Count(); i++)
                {
                    if ((i + 1) % 2 == 1)
                    {
                        resultado += (Convert.ToInt32(digitoCalculo[i].ToString()) * 3);
                    }
                    else
                    {
                        resultado += Convert.ToInt32(digitoCalculo[i].ToString());
                    }
                }

                int verificadorResultado = 10 - (resultado % 10);
                if (verificadorResultado == 10) verificadorResultado = 0;

                return verificadorResultado.ToString();
            }
            else
            {
                return null;
            }
        }

        //Gerador de GLN
        internal dynamic GeradorGLN(string prefixo)
        {

            dynamic retorno = JsonConvert.DeserializeObject(VerificarSequenciaPrefixo(prefixo));

            if (retorno != null)
            {
                string ultimogerado = retorno[0].ultimoglngerado;
                string sequencialGerado = String.Empty, GLNGerado;
                int tamanhoGlnSemVerificador = 12;
                int tamanhoPrefixo = prefixo.ToString().Length;
                int tamanhoSequencial = tamanhoGlnSemVerificador - tamanhoPrefixo, sequencial;
                string globaltradeitemnumber = String.Empty;
                dynamic numerosGerados = new ExpandoObject();

                if (ultimogerado == null || ultimogerado == "")
                    sequencial = 1;
                else
                {
                    sequencial = Convert.ToInt32(ultimogerado) + 1;

                    if ((tamanhoPrefixo == 11 && sequencial.ToString().Length > 1) || (tamanhoPrefixo == 10 && sequencial.ToString().Length > 2) ||
                        (tamanhoPrefixo == 9 && sequencial.ToString().Length > 3) || (tamanhoPrefixo == 8 && sequencial.ToString().Length > 4) ||
                        (tamanhoPrefixo == 7 && sequencial.ToString().Length > 5))
                    {

                        globaltradeitemnumber = "";

                        throw new GS1TradeException("Quantidade de localizações físicas máxima atingida.");
                    }
                }

                //Gera Número sequencial
                while (sequencialGerado.ToString().Length < tamanhoSequencial - sequencial.ToString().Length)
                {
                    sequencialGerado += "0";
                }

                sequencialGerado += sequencial;
                numerosGerados.numeroitem = sequencialGerado;

                GLNGerado = prefixo + sequencialGerado;

                //Gera dígito verificador
                string verificador = GeraDigitoVerificador(GLNGerado);
                GLNGerado = GLNGerado + verificador;
                numerosGerados.gln = GLNGerado;
                numerosGerados.prefixo = prefixo;
                return numerosGerados;
            }
            else
            {
                return null;
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarLocalizacoesFisicas(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT LF.CODIGO, LF.NOME, PG.CODIGO AS CODIGOPAPELGLN, LF.GLN, LF.STATUS, LF.PREFIXO, LF.CEP, LF.TELEFONE, LF.ENDERECO, LF.NUMERO, LF.BAIRRO, LF.COMPLEMENTO, 
		                            LF.ESTADO, LF.CIDADE, LF.PAIS, LF.OBSERVACAO, LF.NOMEIMAGEM, LF.LATITUDE, LF.LONGITUDE, LF.EMAIL, LF.INDICADORPRINCIPAL, 
									CONVERT(VARCHAR(10),CONVERT(DATE,LF.DATAALTERACAO,106),103) + ' '  + convert(VARCHAR(8), GETDATE(), 14) AS 'DATAALTERACAO' 
                                FROM LOCALIZACAOFISICA LF WITH (NOLOCK)
	                                INNER JOIN PAPELGLN PG (Nolock)  ON (PG.CODIGO = LF.CODIGOPAPELGLN)
                                WHERE LF.CODIGOASSOCIADO = " + associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string VerificarSequenciaPrefixo(string prefixo)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

//                string sql = @"SELECT MAX(NUMEROITEM) AS ULTIMOGLNGERADO
//                                FROM LOCALIZACAOFISICA LF
//                                LEFT JOIN ASSOCIADO A ON (A.CODIGO = LF.CODIGOASSOCIADO)
//                                LEFT JOIN PREFIXOLICENCAASSOCIADO PLU ON (PLU.CODIGOASSOCIADO = LF.CODIGOASSOCIADO)
//                                WHERE LF.PREFIXO = @prefixo
//                                AND LF.CODIGOASSOCIADO = " + associado.codigo.ToString();


                string sql = @"SELECT MAX(NUMEROITEM) AS ULTIMOGLNGERADO
                                FROM LOCALIZACAOFISICA LF (Nolock) 
                                LEFT JOIN ASSOCIADO A(Nolock)  ON (A.CODIGO = LF.CODIGOASSOCIADO)
                                LEFT JOIN PREFIXOLICENCAASSOCIADO PLU (Nolock)  ON (PLU.CODIGOASSOCIADO = LF.CODIGOASSOCIADO)
                                WHERE CHARINDEX(CONVERT(VARCHAR(20),@prefixo),CONVERT(VARCHAR(20),LF.PREFIXO)) > 0";                                

                                //WHERE LF.PREFIXO = @prefixo
                                //AND LF.CODIGOASSOCIADO = " + associado.codigo.ToString();
                                

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = new ExpandoObject();
                    param.prefixo = prefixo;
                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public bool ValidaGLN(string gln)
        {
            if (gln != null)
            {

                var digitoCalculo = gln.Substring(0, gln.Count() - 1);
                var verificador = gln.Substring(gln.Count() - 1);
                var resultado = 0;

                for (var i = 0; i < digitoCalculo.Count(); i++)
                {

                    if (i % 2 == 1)
                    {
                        var resultLocal = (Convert.ToInt32(digitoCalculo[i].ToString()) * 3);
                        resultado += resultLocal;
                    }
                    else
                    {
                        resultado += Convert.ToInt32(digitoCalculo[i].ToString());
                    }
                }

                int verificadorResultado = 10 - (resultado % 10);

                if (verificadorResultado == 10)
                {
                    verificadorResultado = 0;
                }

                if (verificadorResultado == Convert.ToInt32(verificador))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        [GS1CNPAttribute("CadastrarLocalizacaoFisica",true,true)]
        public object CadastrarLocalizacoesFisicas(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                using (Database db = new Database("BD"))
                {
                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                    parametro.campos.codigoassociado = associado.codigo.ToString();

                    //Verifica se o nome da Localização Física já foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM LOCALIZACAOFISICA LF WHERE LF.NOME = @nome AND LF.CODIGOASSOCIADO = @codigoassociado";

                    db.Open();
                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {

                        try
                        {
                            if (parametro != null && parametro.campos != null && parametro.campos.prefixo != null)
                            {
                                dynamic glnGerado = new ExpandoObject();
                                glnGerado = GeradorGLN(parametro.campos.prefixo.ToString());

                                if (glnGerado != null)
                                {
                                    if (((IDictionary<String, object>)glnGerado).ContainsKey("gln")) parametro.campos.gln = glnGerado.gln;
                                    if (((IDictionary<String, object>)glnGerado).ContainsKey("numeroitem")) parametro.campos.numeroitem = glnGerado.numeroitem;
                                    if (((IDictionary<String, object>)glnGerado).ContainsKey("prefixo")) parametro.campos.prefixo = glnGerado.prefixo;
                                }
                            }

                            if (parametro.campos.gln != null && ValidaGLN(parametro.campos.gln.Value))
                            {
                                parametro.campos.dataalteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                                parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                                dynamic result = db.Insert(this.nomeTabela, this.nomePK, GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos));

                                if (result != null)
                                {
                                    Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, Convert.ToInt64(result));
                                }

                                return result;
                            }
                            else
                            {
                                throw new GS1TradeException("Código GLN inválido!");
                            }

                        }
                        catch (GS1TradeException e)
                        {
                            throw new GS1TradeException(e.Message.ToString());
                        }
                        catch (Exception e)
                        {
                            throw new GS1TradeException("Não foi possível cadastrar a Localização Física informada.", e);
                        }
                    }
                    else
                    {
                        throw new GS1TradeException("Localização Física já cadastrada.");
                    }
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar o cadastro.");
            }

        }

        [GS1CNPAttribute(true, true, "EditarLocalizacaoFisica", "VisualizarLocalizacaoFisica")]
        public void EditarLocalizacaoFisica(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                using (Database db = new Database("BD"))
                {

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                    parametro.campos.codigoassociado = associado.codigo.ToString();

                    db.Open(true);
                    db.BeginTransaction();

                    //Inserção na tabela LOCALIZACAOFISICAHISTORICO
                    string sql_historico = @"INSERT INTO LOCALIZACAOFISICAHISTORICO (CODIGO, NOME, DESCRICAO, STATUS, PREFIXO, GLN, NUMEROITEM, CODIGOPAPELGLN, ENDERECO, NUMERO, COMPLEMENTO,
                                                    CEP, CIDADE, ESTADO, PAIS, BAIRRO, NOMEIMAGEM, OBSERVACAO, DATAALTERACAO, CODIGOUSUARIOALTERACAO, INDICADORPRINCIPAL,
                                                    DATACANCELAMENTO, DATASUSPENSAO, DATAREUTILIZACAO, CODIGOASSOCIADO, LATITUDE, LONGITUDE, TELEFONE, EMAIL, DATAHISTORICO)
	                                        SELECT CODIGO, NOME, DESCRICAO, STATUS, PREFIXO, GLN, NUMEROITEM, CODIGOPAPELGLN, ENDERECO, NUMERO, COMPLEMENTO,
                                                    CEP, CIDADE, ESTADO, PAIS, BAIRRO, NOMEIMAGEM, OBSERVACAO, DATAALTERACAO, CODIGOUSUARIOALTERACAO, INDICADORPRINCIPAL,
                                                    DATACANCELAMENTO, DATASUSPENSAO, DATAREUTILIZACAO, CODIGOASSOCIADO, LATITUDE, LONGITUDE, TELEFONE, EMAIL, GETDATE()
				                                            FROM LOCALIZACAOFISICA WITH (NOLOCK)
				                                            WHERE LOCALIZACAOFISICA.CODIGO = @codigo";

                    dynamic param1 = null;

                    if (parametro.campos != null)
                        param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql_historico, param1);


                    //Verifica se o nome da LOCALIZACAOFISICA já foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM LOCALIZACAOFISICA LF WHERE LF.NOME = @nome AND LF.CODIGO <> @codigo AND LF.CODIGOASSOCIADO = @codigoassociado";

                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {
                        try
                        {
                            if (parametro.campos.gln != null && ValidaGLN(Convert.ToString(parametro.campos.gln.Value)))
                            {
                                parametro.campos.dataalteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                                parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                                //Itens que não podem ser inseridos
                                //parametro.campos.Remove("prefixo");

                                db.Update(this.nomeTabela, "codigo", GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos));

                                db.Commit();
                            }
                            else
                            {
                                db.Rollback();
                                throw new GS1TradeException("Código GLN inválido!");
                            }

                        }
                        catch (Exception e)
                        {
                            db.Rollback();
                            throw new GS1TradeException("Não foi possível cadastrar a Localização Física informada.", e);
                        }
                    }
                    else
                    {
                        db.Rollback();
                        throw new GS1TradeException("Localização Física já cadastrada.");
                    }
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a atualização.");
            }

        }

        [GS1CNPAttribute("ExcluirLocalizacaoFisica",true,true)]
        public object RemoverLocalizacoesFisicas(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                try
                {
                    string sql = "DELETE FROM LOCALIZACAOFISICA WHERE CODIGO = @codigo";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Exclusão não é permitida. A Localização Física possui dados vinculados.", e);
                }
            }
            else
            {
                return "Não foi possível remover o item selecionado.";
            }

        }

        [GS1CNPAttribute(false)]
        public object PesquisaLocalizacaoFisica(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT LF.CODIGO, LF.NOME, PG.CODIGO AS CODIGOPAPELGLN, LF.GLN, LF.STATUS, LF.PREFIXO, LF.CEP, LF.TELEFONE, LF.ENDERECO, LF.NUMERO, LF.BAIRRO, LF.COMPLEMENTO, 
		                            LF.ESTADO, LF.CIDADE, LF.PAIS, LF.OBSERVACAO, LF.NOMEIMAGEM, LF.LATITUDE, LF.LONGITUDE 
                                FROM LOCALIZACAOFISICA LF WITH (NOLOCK)
	                                INNER JOIN PAPELGLN PG (NOlock)  ON (PG.CODIGO = LF.CODIGOPAPELGLN)
                                WHERE LF.CODIGOASSOCIADO = " + associado.codigo.ToString();

                if (parametro != null && parametro.campos != null)
                {
                    if (parametro.campos.nome != null) 
                        sql += " AND LF.NOME LIKE '%' + @nome + '%'";
                    if (parametro.campos.status != null) 
                        sql += " AND LF.STATUS = @status";
                    if (parametro.campos.codigopapelgln != null)
                        sql += " AND PG.CODIGO = @codigopapelgln";
                    if (parametro.campos.prefixo != null)
                        sql += " AND LF.PREFIXO LIKE '%' + @prefixo + '%'";
                    if (parametro.campos.gln != null)
                        sql += " AND LF.GLN LIKE '%' + @gln + '%'";
                    if (parametro.campos.cep != null)
                        sql += " AND LF.CEP LIKE '%' + @cep + '%'";
                    if (parametro.campos.telefone != null)
                        sql += " AND LF.TELEFONE LIKE '%' + @telefone + '%'";
                    if (parametro.campos.endereco != null)
                        sql += " AND LF.ENDERECO LIKE '%' + @endereco + '%'";
                    if (parametro.campos.numero != null)
                        sql += " AND LF.NUMERO LIKE '%' + @numero + '%'";
                    if (parametro.campos.bairro != null)
                        sql += " AND LF.BAIRRO LIKE '%' + @bairro + '%'";
                    if (parametro.campos.complemento != null)
                        sql += " AND LF.COMPLEMENTO LIKE '%' + @complemento + '%'";
                    if (parametro.campos.estado != null)
                        sql += " AND LF.ESTADO = @estado";
                    if (parametro.campos.cidade != null)
                        sql += " AND LF.CIDADE = @cidade";
                    if (parametro.campos.pais != null)
                        sql += " AND LF.PAIS = @pais";
                    if (parametro.campos.observacao != null)
                        sql += " AND LF.OBSERVACAO LIKE '%' + @observacao + '%'";
                    if (parametro.campos.email != null)
                        sql += " AND LF.EMAIL LIKE '%' + @email + '%'";
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        //TODO - colocar permissão correta
        [GS1CNPAttribute(false,true)]
        public void DeletarImagem(dynamic parametro)
        {

            if (parametro != null && parametro.campos != null)
            {
                if (parametro.campos.nomeimagem.Value.Contains(System.Configuration.ConfigurationManager.AppSettings["URLBlobAzure"]))
                {
                    GS1.CNP.BLL.Core.Util.deleteBlobAzure(parametro.campos.nomeimagem.Value);
                }
                else if (parametro.campos.nomeimagem.Value.Contains("/upload"))
                {
                    if (File.Exists(HttpContext.Current.Server.MapPath(parametro.campos.nomeimagem.Value))) {
                        File.Delete(HttpContext.Current.Server.MapPath(parametro.campos.nomeimagem.Value));
                    }
                }

                string sql = "UPDATE LOCALIZACAOFISICA SET NOMEIMAGEM = NULL WHERE CODIGO = @codigo";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql, param);
                }
            }

        }

        [GS1CNPAttribute(false)]
        public LocalizacaoFisica BuscarLocalizacoesFisicasPK(string codigolocalizacaofisica)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT A.NOME, A.ENDERECO, A.NUMERO, A.COMPLEMENTO, A.CEP, A.CIDADE, A.ESTADO, A.PAIS, A.BAIRRO, A.NOMEIMAGEM, A.TELEFONE, B.NOME AS PAPELGLN, C.NOME AS NOMEASSOCIADO, A.EMAIL
                                , A.LATITUDE, A.LONGITUDE, A.GLN 
                                FROM LOCALIZACAOFISICA A (NOlock) 
                                INNER JOIN PAPELGLN B (NOlock)  ON B.CODIGO = A.CODIGOPAPELGLN
                                INNER JOIN ASSOCIADO C (NOlock)  ON C.CODIGO = A.CODIGOASSOCIADO
                                WHERE A.CODIGO = " + codigolocalizacaofisica + @" AND A.CODIGOASSOCIADO = " + associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();
      
                    Func<IDataRecord, LocalizacaoFisica> binder = r => new LocalizacaoFisica()
                    {
                        nome = (r.IsDBNull(0) ? string.Empty : r.GetString(0)),
                        endereco = (r.IsDBNull(1) ? string.Empty : r.GetString(1)),
                        numero = (r.IsDBNull(2) ? string.Empty : r.GetString(2)),
                        complemento = (r.IsDBNull(3) ? string.Empty : r.GetString(3)),
                        cep = (r.IsDBNull(4) ? string.Empty : r.GetString(4)),
                        cidade = (r.IsDBNull(5) ? string.Empty : r.GetString(5)),
                        estado = (r.IsDBNull(6) ? string.Empty : r.GetString(6)),
                        pais = (r.IsDBNull(7) ? string.Empty : r.GetString(7)),
                        bairro = (r.IsDBNull(8) ? string.Empty : r.GetString(8)),
                        nomeimagem = (r.IsDBNull(9) ? string.Empty : r.GetString(9)),
                        telefone = (r.IsDBNull(10) ? string.Empty : r.GetString(10)),
                        nomepapelgln = (r.IsDBNull(11) ? string.Empty : r.GetString(11)),
                        razaosocial = (r.IsDBNull(12) ? string.Empty : r.GetString(12)),
                        email = (r.IsDBNull(13) ? string.Empty : r.GetString(13)),
                        latitude = (r.IsDBNull(14) ? string.Empty : r.GetString(14)),
                        longitude = (r.IsDBNull(15) ? string.Empty : r.GetString(15)),
                        gln = r.GetInt64(16).ToString()
                    };
                    dynamic param = null;

                    List<LocalizacaoFisica> localizacaofisica = db.Select<LocalizacaoFisica>(sql, param, binder);
                    if (localizacaofisica.Count > 0)
                    {
                        return localizacaofisica[0];
                    }
                    else
                    {
                        return null;
                    }
                    
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarPapeisGLN(dynamic parametro)
        {

            string sql = "SELECT CODIGO, NOME, DESCRICAO, STATUS FROM PAPELGLN WITH (NOLOCK) WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarGLNAssociado(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT GLN AS NOMEGLN 
	                            FROM LOCALIZACAOFISICA L
	                            WHERE L.STATUS = 1
                                AND L.CODIGOASSOCIADO = " + associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return null;
        }

        [GS1CNPAttribute(false)]
        public string BuscarPrefixo(dynamic parametro) 
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @" SELECT TOP 1 PLA.NumeroPrefixo AS PREFIXO
                                FROM PrefixoLicencaAssociado PLA  (NOlock) 
                                    INNER JOIN Licenca L (NOlock)  ON PLA.CodigoLicenca = L.Codigo
                                    INNER JOIN Associado A (NOlock)  ON PLA.CodigoAssociado = A.Codigo
	                                INNER JOIN LicencaAssociado LA (NOlock)  ON (L.Codigo = LA.CodigoLicenca AND A.Codigo = LA.CodigoAssociado)
                                    LEFT JOIN AssociadoUsuario AU  (NOlock) ON A.Codigo = AU.CodigoAssociado 
                                WHERE PLA.CodigoAssociado =  " + associado.codigo.ToString() + @"
                                    AND (AU.CodigoUsuario = " + user.id.ToString() + @" 
                                            OR 1 = (SELECT CodigoTipoUsuario FROM Usuario WHERE Codigo = " + user.id.ToString() + @")
	                                    )
                                    AND L.Codigo = 1
                                    AND PLA.CodigoStatusPrefixo IN (1,8)
                                ORDER BY LA.DataAniversario ASC";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return null;
        }
    }

    public class LocalizacaoFisica
    { 
        public string nome  { get; set; } 
        public string endereco  { get; set; } 
        public string numero  { get; set; } 
        public string complemento  { get; set; }        
        public string cep { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }
        public string bairro { get; set; }
        public string nomeimagem { get; set; }
        public string telefone { get; set; }
        public string nomepapelgln { get; set; }
        public string razaosocial { get; set; }
        public string email { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string gln { get; set; }        
    }


}
