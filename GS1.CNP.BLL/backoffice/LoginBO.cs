﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GS1.CNP.BLL.Core;
using System.Dynamic;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Collections;
using System.Data.Common;
using System.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Net;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using System.Reflection;
using System.Diagnostics;
using GS1.CNP.UTIL;
using GS1.CNP.BLL.Model;
using System.Web.Security;


namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("LoginBO")]
    public class LoginBO : CrudBO
    {

        private static String EVENT_SOURCE = "GS1.CNP.RECAPTCHA";
        private static String EVENT_LOG = "Application";

        public override string nomeTabela
        {
            get { return "dbo.USUARIO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public object CarregarParametrosLogin(dynamic parametro)
        {
            string sql = @"SELECT CHAVE, VALOR FROM PARAMETRO WITH (NOLOCK) WHERE STATUS = 1";

            if (parametro.where != null)
            {
                sql = @"SELECT CHAVE, VALOR FROM PARAMETRO WITH (NOLOCK) WHERE CHAVE = '" + parametro.where + "' AND STATUS = 1";
            }

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> parametros = db.Select(sql, param);

                return JsonConvert.SerializeObject(parametros);
            }
        }

        [GS1CNPAttribute(false)]
        public List<dynamic> CarregarParametrosLoginBO(string parametro)
        {
            string sql = @"SELECT CHAVE, VALOR FROM PARAMETRO WITH (NOLOCK) WHERE CHAVE = '" + parametro + "' AND STATUS = 1";

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();

                dynamic param = null;

                List<dynamic> parametros = db.Select(sql, param);

                return parametros;
            }
        }

        //[GS1CNPAttribute(false,true)]
        [GS1CNPAttribute(false, true)]
        public dynamic Logar(dynamic parametro)
        {
            LoginResult resultLogin = new LoginResult();

            string sql = @"SELECT	U.CODIGO, U.CODIGOPERFIL, U.CODIGOTIPOUSUARIO, U.NOME		
		                    
		                    , COALESCE(U.SENHA,'') AS SENHA
		                    , U.CODIGOSTATUSUSUARIO		
                            , CASE WHEN (U.CODIGOTIPOUSUARIO = 3) --usuario do sistema
			                    THEN
				                    0
			                    ELSE 
				                    CASE					
					                    WHEN CONVERT(DATETIME,CONVERT(VARCHAR(10),GETDATE(),101)) >= CONVERT(DATETIME,CONVERT(VARCHAR(10),DATEADD(DAY, COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.senha.nudias.validade'),180), COALESCE(U.DATAALTERACAOSENHA,U.DATAALTERACAO)),101)) THEN 2
					                    WHEN CONVERT(DATETIME,CONVERT(VARCHAR(10),GETDATE(),101)) >= CONVERT(DATETIME,CONVERT(VARCHAR(10),DATEADD(DAY, COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.senha.nudias.validade'),180)
								                    -COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.senha.nudias.avisoexpiracao'),10), COALESCE(U.DATAALTERACAOSENHA,U.DATAALTERACAO)),101)) THEN 1
					                    ELSE 0
				                    END 
			                    END AS AVISOSENHA	       		   
		                    ,COALESCE(DATEDIFF(DAY, GETDATE(),(DATEADD(DAY, COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.senha.nudias.validade'),180), COALESCE(U.DATAALTERACAOSENHA,U.DATAALTERACAO)))),0) AS DIASVENCIMENTOSENHA						  
                            ,U.EMAIL
		                    ,U.CPFCNPJ    
                            , U.QUANTIDADETENTATIVA                               
		                    ,COALESCE(CONVERT(INT,(SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.senha.nutentativas')),0) AS QTDETENTATIVASBLOQUEIO
                            ,COALESCE(CONVERT(INT,(SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.senha.nutentativasprerecaptcha')),0) AS QTDETENTATIVASRECAPTCHA                            
                            ,(SELECT COUNT(*) FROM ASSOCIADOUSUARIO X LEFT JOIN ASSOCIADO Z ON Z.CODIGO = X.CODIGOASSOCIADO WHERE X.CODIGOUSUARIO = U.CODIGO AND Z.CODIGOSTATUSASSOCIADO = 1) AS TOTALASSOCIADOSATIVOS
                            , COALESCE(A.IndicadorPrincipal,0) AS IndicadorPrincipal

                            FROM USUARIO U WITH (NOLOCK) 
                            LEFT JOIN ASSOCIADOUSUARIO A (NOLOCK)  ON A.CodigoUsuario = U.Codigo and A.IndicadorPrincipal = 1                            
                    WHERE  U.EMAIL = @login";

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                {
                    if (parametro.where is JToken)
                        param = ((JToken)parametro.where).ToExpando();
                    else
                        param = parametro.where;
                }

                Func<IDataRecord, Login> binder = r => new Login()
                {
                    id = r.GetInt64(0),
                    id_perfil = r.GetInt32(1),
                    id_tipousuario = r.GetInt32(2),
                    nome = r.GetString(3)
                 ,
                    senha = r.GetString(4)
                 ,
                    id_statususuario = r.GetInt32(5)
                 ,
                    avisosenha = r.GetInt32(6)
                 ,
                    diasvencimentosenha = r.GetInt32(7)
                 ,
                    email = r.GetString(8)
                 ,
                    CPFCNPJ = (r.IsDBNull(9) ? string.Empty : r.GetString(9))
                 ,
                    quantidade_tentativa = r.GetInt32(10)
                 ,
                    qtdetentativasbloqueio = r.GetInt32(11)
                 ,
                    qtdetentativasrecaptcha = r.GetInt32(12)
                 ,
                    qtdeassociadosativos = r.GetInt32(13)
                 ,
                    indicadorprincipal = r.GetInt32(14)
                };



                List<Login> usuarios = db.Select<Login>(sql, param, binder);

                //param.cpfcnpj = "";

                //Usuário encontrado na base local
                if (usuarios != null && usuarios.Count() > 0)
                {
                    Login usuario = usuarios.FirstOrDefault();

                    //int quantidadeErros = Convert.ToInt32(usuario.qtdediasbloqsenha);
                    int quantidadeErros = Convert.ToInt32(usuario.qtdetentativasbloqueio);
                    int quantidadeRecaptcha = Convert.ToInt32(usuario.qtdetentativasrecaptcha);
                    bool existeBoletos = ExisteBoletos(usuario.id);

                    //TODO buscar de enumerador
                    //if ((usuario.id_statususuario == 1 || usuario.id_statususuario == 5) && (usuario.id_statususuariopai == 1 || usuario.id_statususuariopai == 5 || usuario.id_statususuariopai == 4) || (usuario.id_statususuariopai == 6 && usuario.toleranciainadimplente >= 0))
                    if ((usuario.id_statususuario == 1 || usuario.id_statususuario == 5 || (usuario.id_statususuario != 3)) && (usuario.id_tipousuario == 1 || (usuario.qtdeassociadosativos > 0 && usuario.id_tipousuario == 2) || (usuario.qtdeassociadosativos == 0 && usuario.id_tipousuario == 2 && existeBoletos)))
                    {
                        if (usuario.quantidade_tentativa < quantidadeErros + quantidadeRecaptcha)
                        {

                            //Verifica se errou senha e não informou CAPTCHA
                            if (usuario.quantidade_tentativa > quantidadeRecaptcha && (param.response != "" && param.response != null) || usuario.quantidade_tentativa == 0 || usuario.quantidade_tentativa <= quantidadeRecaptcha)
                            {
                                //TODO - tratamento try cast                        
                                if (((usuario.quantidade_tentativa <= quantidadeRecaptcha || usuario.quantidade_tentativa == 0) && (param.response == "" || param.response == null)) || VerificareCAPTCHA(param.response))
                                {

                                    bool senhaValidada = false;

                                    //TODO - tipo usuario 2
                                    //if (usuario.id_tipousuario == 2 && usuario.indicadorprincipal == 1) // Se principal em alguma empresa verifica senha na integração
                                    //{
                                    //    ResutlIntegracao resultIntegracao = null;
                                    //    senhaValidada = verificarWSGS1(param.login, param.senha, ref resultIntegracao, usuario.CPFCNPJ);

                                    //    if (!senhaValidada)
                                    //    {
                                    //        atualizaQuantidadeTentativas(usuario.quantidade_tentativa + 1, param.login);
                                    //        resultLogin.status_login = false;
                                    //        resultLogin.quantidade_tentativa = usuario.quantidade_tentativa + 1;
                                    //        resultLogin.mensagem = "Email e/ou Senha Inválidos.";
                                    //    }
                                    //}

                                    if (resultLogin.mensagem == null || resultLogin.mensagem == string.Empty)
                                    {

                                        if (senhaValidada || usuario.senha == Crypto.GeraSHA1(param.senha))
                                        {
                                            var perfil = "";
                                            List<string> permissoes = new List<string>();
                                            List<PerfilGepir> perfisGepir = new List<PerfilGepir>();
                                            List<ReturnCodeMessage> returnCodeMessageGEPIR = new List<ReturnCodeMessage>();

                                            if (!existeBoletos)
                                            {
                                                //Login com sucesso                                        
                                                perfil = BuscarModulos(db, Convert.ToInt64(usuario.id));                                               
                                                permissoes = BuscarPermissoes(db, Convert.ToInt64(usuario.id));
                                                
                                                GepirBO gepirbo = new GepirBO();
                                                perfisGepir = gepirbo.GetConfigurationProfile();
                                                returnCodeMessageGEPIR = gepirbo.ConsultarReturnCodeMessage();

                                            }

                                            this.setSession("PERFIL_USUARIO_LOGADO", perfil);
                                            this.setSession("PERMISSOES_USUARIO_LOGADO", permissoes);
                                            this.setSession("PERFIS_GEPIR", perfisGepir);
                                            this.setSession("RETURN_CODE_GEPIR", returnCodeMessageGEPIR);

                                            usuario.termoadesaopendente = Convert.ToInt32(BuscarStatusTermoAceite(db, Convert.ToInt64(usuario.id)));
                                            usuario.permissoes = permissoes;

                                            this.setSession("USUARIO_LOGADO", usuario);
                                            resultLogin.status_login = true;
                                            resultLogin.quantidade_tentativa = 0;
                                            resultLogin.mensagem = string.Empty;

                                            if (usuario.quantidade_tentativa > 0)
                                                atualizaQuantidadeTentativas(0, param.login);

                                            //Grava último acesso
                                            atualizaUltimoAcesso(Convert.ToInt64(usuario.id));

                                            //Grava log acesso
                                            if (!HttpContext.Current.Request.FilePath.ToLower().Contains("produtoservice.asmx"))
                                                Log.WriteInfo(null, "Login", usuario.id);

                                        }
                                        else
                                        {
                                            atualizaQuantidadeTentativas(usuario.quantidade_tentativa + 1, param.login);
                                            resultLogin.status_login = false;
                                            if (usuario.quantidade_tentativa + 1 > quantidadeRecaptcha)
                                            {
                                                resultLogin.quantidade_tentativa = usuario.quantidade_tentativa + 1;
                                            }
                                            else
                                            {
                                                resultLogin.quantidade_tentativa = 0;
                                            }

                                            resultLogin.mensagem = "Email e/ou Senha Inválidos.";
                                        }
                                    }
                                }
                                else
                                {
                                    atualizaQuantidadeTentativas(usuario.quantidade_tentativa + 1, param.login);
                                    resultLogin.status_login = false;
                                    resultLogin.quantidade_tentativa = usuario.quantidade_tentativa + 1;
                                    resultLogin.mensagem = "Erro na digitação do código de segurança. Tente novamente.";

                                    try
                                    {
                                        Log.WriteError(resultLogin.mensagem);
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                            }
                            else
                            {
                                resultLogin.status_login = false;
                                resultLogin.quantidade_tentativa = usuario.quantidade_tentativa;
                                resultLogin.mensagem = "Por favor informe o código de segurança.";

                                try
                                {
                                    Log.WriteError(resultLogin.mensagem);
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        else
                        {
                            //TODO - parametrizar status

                            //string sqlQtdeBloqSenha = "select valor from parametro where chave = 'autenticacao.senha.nutentativas'";

                            int statusBloqueioSenha = 4;
                            db.Select("UPDATE Usuario SET CodigoStatusUsuario = " + statusBloqueioSenha + " WHERE Codigo = '" + usuario.id + "'");
                            resultLogin.status_login = false;
                            resultLogin.quantidade_tentativa = usuario.quantidade_tentativa;
                            string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
                            resultLogin.mensagem = "Usuário bloqueado! Quantidade Máxima de Tentativas Atingida. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.";
                        }
                    }
                    else
                    {
                        resultLogin.status_login = false;
                        resultLogin.quantidade_tentativa = 0;
                        string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);

                        if (usuario.qtdeassociadosativos <= 0 && usuario.id_tipousuario == 2)
                        {
                            resultLogin.mensagem = "Acesso negado. Dados de Associado não encontrado. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.";
                        }
                        else
                        {
                            resultLogin.mensagem = "Acesso negado. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.";
                        }



                    }

                }
                else //Usuário Não encontradado
                {

                    if ((param.response == "" || param.response == null) || VerificareCAPTCHA(param.response))
                    {

                        //if (((IDictionary<string, Object>)param).ContainsKey("cpfcnpj") && param.cpfcnpj != null && param.cpfcnpj != "")
                        if (((IDictionary<string, Object>)param).ContainsKey("cpfcnpj") && param.cpfcnpj != null)
                        {

                            ResutlIntegracao resultIntegracao = null;
                            bool usuarioLocalizado = false;
                            string cpfcnpj = param.cpfcnpj;

                            //VALIDA LOGIN E SENHA 
                            //usuarioLocalizado = verificarWSCNP(param.login, param.senha, ref resultIntegracao);

                            //SE VALIDADO BUSCA INFORMACOES DO USUARIO
                            //if (usuarioLocalizado)
                            //    usuarioLocalizado = consultaUsuarioWSCNP(param.login, ref cpfcnpj, ref resultIntegracao);


                            //CASO NAO LOCALIZADO NO CNP BUSCA NA BASE PRINCIPAL
                            if (!usuarioLocalizado)
                                usuarioLocalizado = verificarWSGS1(param.login, param.senha, ref resultIntegracao, cpfcnpj);

                            //TODO - remover hardcode
                            //if(param.senha == "User123!")
                            //    usuarioLocalizado = true;

                            if (usuarioLocalizado)
                            {
                                string cpfcnpjformatado = UTIL.Format.formataCPFCNPJ(cpfcnpj);
                                if (cpfcnpjformatado == string.Empty)
                                {
                                    resultLogin.status_login = false;
                                    resultLogin.primeiro_acesso = true;
                                    resultLogin.quantidade_tentativa = 0;
                                    resultLogin.mensagem = "CPF/CNPJ Inválidos.";
                                    return JsonConvert.SerializeObject(resultLogin);
                                }


                                UsuarioCRM usuarioCRM = buscaDadosCRM(cpfcnpjformatado, param.login);

                                //remover hard code
                                if (usuarioCRM != null)
                                {
                                    usuarioCRM.senha = Crypto.GeraSHA1(param.senha);
                                    if (usuarioCRM.nome == string.Empty)
                                    {
                                        usuarioCRM.nome = "USUARIO FIXO POR ERRO DE DADOS DO CRM";
                                    }
                                }

                                if (usuarioCRM != null && usuarioCRM.nome != null)
                                {
                                    usuarioCRM.email = param.login;

                                    //LOGIN COM SUCESSO
                                    //PRIMEIRO ACESSO - GRAVA USUARIO NA BASE LOCAL                            
                                    insereUsuarioPrimeiroAcesso(usuarioCRM);


                                    if (usuarioCRM.bl_adimplente && usuarioCRM.gs1br_cnp20)
                                    {
                                        Func<IDataRecord, Login> binderNovoUser = r => new Login()
                                        {
                                            id = r.GetInt64(0),
                                            id_perfil = r.GetInt32(1),
                                            id_tipousuario = r.GetInt32(2),
                                            nome = r.GetString(3)
                                            ,
                                            senha = r.GetString(4)
                                            ,
                                            id_statususuario = r.GetInt32(5)
                                            ,
                                            avisosenha = r.GetInt32(6)
                                            ,
                                            diasvencimentosenha = r.GetInt32(7)
                                            ,
                                            email = r.GetString(8)
                                            ,
                                            CPFCNPJ = (r.IsDBNull(9) ? string.Empty : r.GetString(9))
                                            ,
                                            quantidade_tentativa = r.GetInt32(10)
                                            ,
                                            qtdetentativasbloqueio = r.GetInt32(11)
                                        };
                                        List<Login> usuariosNovo = db.Select<Login>(sql, param, binderNovoUser);

                                        if (usuariosNovo != null && usuariosNovo.Count() > 0)
                                        {
                                            dynamic usuarioNovo = usuariosNovo.FirstOrDefault();

                                            var perfil = BuscarModulos(db, Convert.ToInt32(usuarioNovo.id));
                                            this.setSession("PERFIL_USUARIO_LOGADO", perfil);

                                            List<string> permissoes = BuscarPermissoes(db, Convert.ToInt32(usuarioNovo.id));
                                            this.setSession("PERMISSOES_USUARIO_LOGADO", permissoes);

                                            usuarioNovo.termoadesaopendente = Convert.ToInt32(BuscarStatusTermoAceite(db, Convert.ToInt64(usuarioNovo.id)));
                                            usuarioNovo.permissoes = permissoes;

                                            this.setSession("USUARIO_LOGADO", usuarioNovo);
                                            resultLogin.status_login = true;
                                            resultLogin.quantidade_tentativa = 0;
                                            resultLogin.mensagem = string.Empty;

                                            //Grava último acesso
                                            atualizaUltimoAcesso(Convert.ToInt64(usuario.id));

                                            //Grava log acesso
                                            Log.WriteInfo(null, "Login", usuario.id);
                                        }
                                        else
                                        {
                                            resultLogin.status_login = false;
                                            resultLogin.primeiro_acesso = true;
                                            resultLogin.quantidade_tentativa = 0;
                                            string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
                                            resultLogin.mensagem = "Não foi possível realizar o login. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.";
                                        }
                                    }
                                    else
                                    {
                                        resultLogin.status_login = false;
                                        resultLogin.primeiro_acesso = true;
                                        resultLogin.quantidade_tentativa = 0;
                                        string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
                                        resultLogin.mensagem = "Acesso negado. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.";
                                    }
                                }
                                else
                                {
                                    resultLogin.status_login = false;
                                    resultLogin.primeiro_acesso = true;
                                    resultLogin.quantidade_tentativa = 0;

                                    string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
                                    resultLogin.mensagem = "Não foi possível realizar o login. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.";

                                    try
                                    {
                                        Log.WriteError(resultLogin.mensagem);
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                            }
                            else
                            {
                                resultLogin.status_login = false;
                                resultLogin.primeiro_acesso = true;
                                resultLogin.quantidade_tentativa = 1;
                                resultLogin.mensagem = "Email e/ou Senha Inválidos.";
                                try
                                {
                                    Log.WriteError(resultLogin.mensagem);
                                }
                                catch (Exception)
                                {

                                }
                            }

                        }
                        else
                        {
                            resultLogin.status_login = false;
                            resultLogin.primeiro_acesso = true;
                            resultLogin.quantidade_tentativa = 0;
                            resultLogin.mensagem = "Este é seu primeiro acesso. Por favor informe o CPF/CNPJ.";
                        }
                    }
                    else
                    {
                        resultLogin.primeiro_acesso = true;
                        resultLogin.status_login = false;
                        resultLogin.quantidade_tentativa = 1;
                        resultLogin.mensagem = "Código de Validação Incorreto.";
                    }
                }

                return JsonConvert.SerializeObject(resultLogin);
            }
        }

        //private bool consultaUsuarioWSCNP(string email, ref string cpfcnpj, ref ResutlIntegracao resultIntegracao)
        //{
        //    try
        //    {
        //        string wsCNPRequesterGln = ConfigurationManager.AppSettings["wsCNPRequesterGln"];
        //        string wsCNPWSUSER = ConfigurationManager.AppSettings["wsCNPWSUSER"];

        //        wsCNP.CnpServicesHeader header = new wsCNP.CnpServicesHeader();
        //        header.RequesterGln = wsCNPRequesterGln;
        //        header.WSUSER = wsCNPWSUSER;

        //        wsCNP.AuthService wscnpAuthSerice = new wsCNP.AuthService();
        //        wscnpAuthSerice.CnpServicesHeaderValue = header;

        //        wsCNP.User user = wscnpAuthSerice.GetUser(email);
        //        if (user != null)
        //        {
        //            cpfcnpj = user.Associado.CpfCnpj;
        //            return true;
        //        }

        //        return false;

        //    }
        //    catch (Exception)
        //    {

        //        string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
        //        throw new GS1TradeException("Não foi possível realizar o login. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.");
        //    }

        //}

        //public bool verificarWSCNP(string email, string senha, ref ResutlIntegracao resultIntegracao)
        //{
        //    try
        //    {
        //        string wsCNPRequesterGln = ConfigurationManager.AppSettings["wsCNPRequesterGln"];
        //        string wsCNPWSUSER = ConfigurationManager.AppSettings["wsCNPWSUSER"];

        //        wsCNP.CnpServicesHeader header = new wsCNP.CnpServicesHeader();
        //        header.RequesterGln = wsCNPRequesterGln;
        //        header.WSUSER = wsCNPWSUSER;

        //        wsCNP.AuthService wscnpAuthSerice = new wsCNP.AuthService();
        //        wscnpAuthSerice.CnpServicesHeaderValue = header;

        //        if (wscnpAuthSerice.Validate(email, senha))
        //        {
        //            return true;
        //        }

        //        return false;

        //    }
        //    catch (Exception ex)
        //    {
        //        string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
        //        throw new GS1TradeException("Não foi possível realizar o login. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.", ex);
        //        //throw new Exception("Erro ao buscar informações do usuário." + ex.ToString());
        //    }

        //}

        public bool verificarWSGS1(string email, string senha, ref ResutlIntegracao resultIntegracao, string cpfcnpj = "")
        {
            try
            {
                string UserSRV = ConfigurationManager.AppSettings["UserSRV"];
                string PassSRV = ConfigurationManager.AppSettings["PassSRV"];
                //TODO - remover bypass de certificado
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                wsGS1.WebService wsgs1 = new wsGS1.WebService();
                return wsgs1.Autentica(UserSRV, PassSRV, cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", ""), email, senha);
            }
            catch (Exception ex)
            {
                string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
                throw new GS1TradeException("Não foi possível realizar o login. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.", ex);
            }

        }

        private UsuarioCRM buscaDadosCRM(string cpfcnpj, string email)
        {
            UsuarioBO usuarioBO = new UsuarioBO();
            return usuarioBO.ConsultarCRM(cpfcnpj, email);
        }

        private void insereUsuarioPrimeiroAcesso(UsuarioCRM usuarioCRM)
        {
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                try
                {
                    db.Open(true);
                    db.BeginTransaction();

                    //Inserção na tabela Usuario                
                    string sql = @"INSERT INTO [dbo].[Usuario]([Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email]
                                    ,[TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao]
                                    ,[MensagemObservacao],[DataAlteracaoSenha],[QuantidadeTentativa]
                                    ,[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],[TelefoneComercial]
                                    ,[Ramal],[Departamento],[Cargo],[CPFCNPJ])
                                    VALUES (@nome,@statususuario,2,2,@email
                                    ,NULL,@senha,1,GETDATE()
                                    ,NULL,NULL,0
                                    ,NULL,GETDATE(),NULL,NULL
                                    ,NULL,NULL,NULL,@cpfcnpj)";

                    dynamic param = new ExpandoObject();
                    param.nome = usuarioCRM.nome;
                    param.email = usuarioCRM.email;
                    param.senha = usuarioCRM.senha;
                    param.cpfcnpj = usuarioCRM.cpfcnpj.ToString().Replace(".", "").Replace("-", "").Replace("/", "");

                    if (usuarioCRM.bl_adimplente)
                    {
                        param.statususuario = 1;
                    }
                    else
                    {
                        param.statususuario = 3;
                    }

                    if (!usuarioCRM.gs1br_cnp20)
                    {
                        param.statususuario = 3;
                    }


                    param.mensagemobservacao = "";


                    long codigousuario = db.Insert(sql, param, true);

                    //Inserção na tabela Associado
                    string sql2 = @"INSERT INTO [dbo].[Associado]
                                   ([CAD],[NOME],[DESCRICAO],[EMAIL],[CPFCNPJ],[INSCRICAOESTADUAL]
                                   ,[CODIGOTIPOASSOCIADO],[MENSAGEMOBSERVACAO],[DATAALTERACAO]
                                   ,[DATAATUALIZACAOCRM],[DATASTATUSINADIMPLENTE],[CODIGOUSUARIOALTERACAO]
                                   ,[ENDERECO],[NUMERO],[COMPLEMENTO],[BAIRRO],[CIDADE],[UF]
                                   ,[CEP],[CODIGOTIPOCOMPARTILHAMENTO],[CODIGOSTATUSASSOCIADO],[CODIGOSITUACAOFINANCEIRA])
                             VALUES
                                   (@cad,@nome,@nome,@email,@cpfcnpj,@inscricaoestadual
                                   ,@premium,NULL,GETDATE()
                                   ,NULL,NULL,1
                                   ,@logradouroprincipal,@numero,@complementoprincipal,@bairroprincipal,@address1_city,@estadoname
                                   ,@address1_postalcode,1,@statusassociado,@situacaofinanceira)";


                    dynamic param2 = new ExpandoObject();
                    param2.codigousuario = codigousuario;
                    param2.cpfcnpj = usuarioCRM.cpfcnpj.ToString().Replace(".", "").Replace("-", "").Replace("/", "");
                    if (Convert.ToInt32(usuarioCRM.bl_premium) == 1)
                    {
                        param2.premium = 1;
                    }
                    else
                    {
                        param2.premium = 2;
                    };

                    param2.cad = usuarioCRM.cad;
                    param2.nome = usuarioCRM.nome;
                    param2.email = usuarioCRM.email.ToString();
                    param2.inscricaoestadual = usuarioCRM.inscricaoestadual.ToString();
                    param2.logradouroprincipal = usuarioCRM.logradouroprincipal.ToString();
                    param2.numero = usuarioCRM.numero.ToString();
                    param2.complementoprincipal = usuarioCRM.complementoprincipal.ToString();
                    param2.bairroprincipal = usuarioCRM.bairroprincipal.ToString();
                    param2.address1_city = usuarioCRM.address1_city.ToString();
                    param2.estadoname = usuarioCRM.estadoname.ToString();
                    param2.address1_postalcode = usuarioCRM.address1_postalcode.ToString();

                    if (usuarioCRM.bl_adimplente)
                    {
                        param2.situacaofinanceira = 1;
                    }
                    else
                    {
                        param2.situacaofinanceira = 2;
                    }


                    if (!usuarioCRM.gs1br_cnp20)
                    {
                        param2.statusassociado = 4;
                    }
                    else
                    {
                        if (usuarioCRM.relacionamento == "2")
                        {
                            param2.statusassociado = 1;
                        }
                        else
                        {
                            param2.statusassociado = 6;
                        }
                    }


                    int codigoassociado = db.Insert(sql2, param2, true);

                    //Inserção na tabela AssociadoHistorico
                    string sql3 = @"INSERT INTO ASSOCIADOHISTORICO ([CODIGOASSOCIADO], [CAD],[NOME],[DESCRICAO],[EMAIL],[CPFCNPJ],[INSCRICAOESTADUAL]
                                   ,[CODIGOTIPOASSOCIADO],[MENSAGEMOBSERVACAO],[DATAALTERACAO]
                                   ,[DATAATUALIZACAOCRM],[DATASTATUSINADIMPLENTE],[CODIGOUSUARIOALTERACAO]
                                   ,[ENDERECO],[NUMERO],[COMPLEMENTO],[BAIRRO],[CIDADE],[UF]
                                   ,[CEP],[CODIGOTIPOCOMPARTILHAMENTO],[CODIGOSTATUSASSOCIADO],[CODIGOSITUACAOFINANCEIRA], DATAHISTORICO)
                	                
                                    SELECT [CODIGO],[CAD],[NOME],[DESCRICAO],[EMAIL],[CPFCNPJ],[INSCRICAOESTADUAL]
                                   ,[CODIGOTIPOASSOCIADO],[MENSAGEMOBSERVACAO],[DATAALTERACAO]
                                   ,[DATAATUALIZACAOCRM],[DATASTATUSINADIMPLENTE],[CODIGOUSUARIOALTERACAO]
                                   ,[ENDERECO],[NUMERO],[COMPLEMENTO],[BAIRRO],[CIDADE],[UF]
                                   ,[CEP],[CODIGOTIPOCOMPARTILHAMENTO],[CODIGOSTATUSASSOCIADO],[CODIGOSITUACAOFINANCEIRA], GETDATE()
                				    FROM ASSOCIADO 
                				    WHERE ASSOCIADO.CODIGO = @codigoassociado";

                    dynamic param3 = new ExpandoObject();
                    param3.codigoassociado = codigoassociado;

                    db.Insert(sql3, param3, false);

                    //Inserção na tabela UsuarioHistorico
                    string sql4 = @"INSERT INTO USUARIOHISTORICO ([Codigo],[Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email]
                                                                ,[TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao]
                                                                ,[MensagemObservacao],[DataAlteracaoSenha],[QuantidadeTentativa]
                                                                ,[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],[TelefoneComercial]
                                                                ,[Ramal],[Departamento],[Cargo],[CPFCNPJ], DATAHISTORICO)
	                                   SELECT [Codigo],[Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email]
                                                ,[TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao]
                                                ,[MensagemObservacao],[DataAlteracaoSenha],[QuantidadeTentativa]
                                                ,[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],[TelefoneComercial]
                                                ,[Ramal],[Departamento],[Cargo],[CPFCNPJ],GETDATE()
											FROM USUARIO
				                            WHERE USUARIO.CODIGO = @codigousuario";

                    dynamic param4 = new ExpandoObject();
                    param4.codigousuario = codigousuario;

                    db.Insert(sql4, param4, false);


                    //Associado associado e usuário
                    string sql10 = @"INSERT INTO ASSOCIADOUSUARIO VALUES (@codigousuario, @codigoassociado,GETDATE(),NULL,1,1)";
                    dynamic param10 = new ExpandoObject();
                    param10.codigousuario = codigousuario;
                    param10.codigoassociado = codigoassociado;
                    db.Insert(sql10, param10, false);


                    //Buscar licencas e prefixos da base unica/crm
                    using (GS1.CNP.DAL.Database dbbaseunica = new GS1.CNP.DAL.Database("BDBASEUNICACRM"))
                    {

                        dbbaseunica.Open(true);
                        string sqlBaseUnicaCRM = @"SELECT DISTINCT SMART_PAP_CODLIC AS PREFIXO
                                                        ,SMART_PAI_DATASN           AS DATA_ANIVERSARIO
                                                        ,PRODUCTTYPECODE            AS FLAGPRINCIPAL
                                                        ,SMART_CODIGOATRIBUICAO     AS LICENCA
	                                                    ,SMART_NEGOCIONAME          AS NEGOCIO
                                                        ,SMART_PAP_QTDDIG           AS QTDE_DIGITOS                              
                                                        ,SMART_STATUSGLP            AS STCADASTRAL
                                                        ,SMART_FINANCEIROSTATUS     AS STFINANCEIRO
                                                        ,smart_empresaName		    AS ASSOCIADO
	                                                    ,SMART_PAP_STATUS		    AS STATUS_DA_LICENCA
	                                                    ,A.ACCOUNTNUMBER		    AS CAD
	                                                    ,ACCOUNTID                  AS ID
                                                        ,SMART_PRODUTODOGTIN8       AS DESCRICAO_GTIN8
                                                    FROM SMART_LICENCA SL (NOLOCK)
                                                    INNER JOIN SMART_GESTAODELICENCASEPRODUTO SG (NOLOCK) ON SL.SMART_CONTRATO = SG.SMART_GESTAODELICENCASEPRODUTOID
                                                    INNER JOIN PRODUCT P (NOLOCK) ON SL.SMART_NEGOCIO = P.PRODUCTID
                                                    INNER JOIN ACCOUNT A  (NOLOCK)  ON SL.SMART_EMPRESA = A.AccountId 
                                                    WHERE  SG.SMART_ASSOCIADO = @id
                                                    AND SMART_PAP_CODLIC IS NOT NULL AND SMART_PAP_CODLIC <> '' AND ISNUMERIC (SMART_PAP_CODLIC) = 1
                                                    AND ((SMART_CODIGOATRIBUICAO = 'GTIN-8' AND SMART_PRODUTODOGTIN8 IS NOT NULL) OR SMART_CODIGOATRIBUICAO <> 'GTIN-8')
                                                    AND SMART_PAP_STATUS IS NOT NULL";

                        dynamic paramBaseUnicaCRM = new ExpandoObject();
                        paramBaseUnicaCRM.id = usuarioCRM.id;


                        List<dynamic> dadosassociado = dbbaseunica.Select(sqlBaseUnicaCRM, paramBaseUnicaCRM);
                        if (dadosassociado.Count > 0)
                        {
                            Dictionary<string, int> licencaDic = new Dictionary<string, int>();
                            List<dynamic> licencasCRM = dadosassociado.Select(item => item.licenca).Distinct().ToList();

                            foreach (string licenca in licencasCRM)
                            {
                                string sql6 = @"SELECT CODIGO FROM LICENCA WHERE NOME = @nomelicenca";
                                dynamic param6 = new ExpandoObject();
                                param6.nomelicenca = licenca;

                                List<dynamic> licencas = db.Select(sql6, param6);
                                if (licencas.Count > 0)
                                {
                                    if (!licencaDic.ContainsKey(licenca))
                                    {
                                        licencaDic.Add(licenca, licencas[0].codigo);
                                    }

                                    //Inserção na tabela FornecedorHistorico
                                    string sql5 = @"INSERT INTO [dbo].[LicencaAssociado]([CodigoLicenca],[CodigoAssociado],[Status],[DataAniversario],[DataAlteracao])
                                            SELECT @codigolicenca,@codigoassociado,1,@dataaniversario, GETDATE()";

                                    dynamic param5 = new ExpandoObject();
                                    param5.codigoassociado = codigoassociado;
                                    param5.codigolicenca = licencas[0].codigo;
                                    param5.dataaniversario = dadosassociado[0].data_aniversario;

                                    db.Insert(sql5, param5, false);
                                }
                            }

                            foreach (var item in dadosassociado)
                            {
                                if (licencaDic.ContainsKey(item.licenca) && item.prefixo != string.Empty)
                                {
                                    string sql7 = @"INSERT INTO [dbo].[PrefixoLicencaAssociado] ([CodigoLicenca],[CodigoAssociado],[NumeroPrefixo],[DataAlteracao],[CodigoStatusPrefixo])
                                                SELECT @codigolicenca, @codigoassociado, @prefixo,GETDATE(), 1"; //TODO - VALIDAR STATUS PREFIXO
                                    dynamic param7 = new ExpandoObject();
                                    param7.codigolicenca = licencaDic[item.licenca];
                                    param7.codigoassociado = codigoassociado;
                                    param7.prefixo = item.prefixo;
                                    db.Insert(sql7, param7, false);

                                    //Insere produtos GTIN-8
                                    if (licencaDic[item.licenca] == 2 && item.descricao_gtin8 != null && item.descricao_gtin8 != string.Empty)
                                    {
                                        string sql11 = @"INSERT INTO [dbo].[Produto]
			                                        ([globalTradeItemNumber]
			                                        ,[productDescription]
			                                        ,[CodigoTipoGTIN]
			                                        ,[NrPrefixo]
			                                        ,[CodItem]
			                                        ,[CodigoAssociado]
			                                        ,[CodigoUsuarioCriacao]          
			                                        ,[DataInclusao]
			                                        ,[CodigoStatusGTIN]
			                                        ,indicadorgdsn)
                                                    VALUES (@prefixo, @descricao_gtin8, 1, @prefixo, SUBSTRING(@prefixo,4,4), @codigoassociado, 1, GETDATE(), 1, 0)";
                                        dynamic param11 = new ExpandoObject();
                                        param11.codigoassociado = codigoassociado;
                                        param11.prefixo = item.prefixo;
                                        param11.descricao_gtin8 = item.descricao_gtin8;
                                        db.Insert(sql11, param11, false);
                                    }


                                }
                            }


                            if (usuarioCRM.glninformativo == string.Empty)
                            {
                                string sqlGTINGLNInformativo = @"SELECT CAST(((MIN (SMART_PAP_CODLIC)) + '00000000000') AS VARCHAR(11))+'1' AS PREFIXO
                                                            FROM SMART_LICENCA SL (NOLOCK)
                                                            INNER JOIN SMART_GESTAODELICENCASEPRODUTO SG (NOLOCK) ON SL.SMART_CONTRATO = SG.SMART_GESTAODELICENCASEPRODUTOID
                                                            INNER JOIN PRODUCT P (NOLOCK) ON SL.SMART_NEGOCIO = P.PRODUCTID
                                                            WHERE  SG.SMART_ASSOCIADO = @id
                                                            AND P.SMART_CODIGOATRIBUICAO = 'GTIN-13'";
                                dynamic paramGTINGLNInformativo = new ExpandoObject();
                                paramGTINGLNInformativo.id = usuarioCRM.id;

                                List<dynamic> GTINGLNInformativo = dbbaseunica.Select(sqlGTINGLNInformativo, paramGTINGLNInformativo);
                                if (GTINGLNInformativo.Count > 0 && GTINGLNInformativo[0].prefixo != string.Empty)
                                {
                                    //TODO completar digitos e colocar DV
                                    int dv = ProdutoBO.CalculaDV(GTINGLNInformativo[0].prefixo);
                                    if (dv > 0)
                                    {
                                        usuarioCRM.glninformativo = GTINGLNInformativo[0].prefixo + dv;
                                    }
                                    else
                                    {
                                        usuarioCRM.glninformativo = string.Empty;
                                    }
                                }
                            }

                        }
                    }


                    if (usuarioCRM.glninformativo != string.Empty)
                    {
                        //Inserção do GLN informativo
                        string sql8 = @"INSERT INTO [dbo].[LocalizacaoFisica]
                                       ( [Nome],[Descricao],[Status],[Prefixo],[GLN],[NumeroItem],[CodigoPapelGLN]
                                        ,[Endereco],[Numero],[Complemento],[CEP],[Cidade],[Estado],[Pais],[Bairro]
                                        ,[NomeImagem],[Observacao],[DataAlteracao],[CodigoUsuarioAlteracao]
                                        ,[IndicadorPrincipal],[DataCancelamento],[DataSuspensao],[DataReutilizacao]
                                        ,[CodigoAssociado],[Latitude],[Longitude],[Telefone],[Email])
                                SELECT TOP 1 @nome,@nome,1,SUBSTRING(@glninformativo,0,12),@glninformativo,1,1
                                        ,@logradouroprincipal,@numero,@complementoprincipal,@address1_postalcode,@address1_city,@estadoname,@pais
                                        ,@bairroprincipal,NULL,NULL,GETDATE(),1,1,NULL,NULL,NULL
                                        ,@codigoassociado,null,null,@telefone,@email
                                FROM LOCALIZACAOFISICA 
                                WHERE NOT EXISTS (SELECT TOP 1 GLN FROM LOCALIZACAOFISICA WHERE GLN = @glninformativo AND CODIGOASSOCIADO = @codigoassociado)";

                        dynamic param8 = new ExpandoObject();
                        param8.codigousuario = codigousuario;
                        param8.nome = usuarioCRM.nome;
                        param8.glninformativo = usuarioCRM.glninformativo;
                        param8.logradouroprincipal = usuarioCRM.logradouroprincipal.ToString();
                        param8.numero = usuarioCRM.numero.ToString();
                        param8.complementoprincipal = usuarioCRM.complementoprincipal.ToString();
                        param8.address1_postalcode = usuarioCRM.address1_postalcode.ToString();
                        param8.address1_city = usuarioCRM.address1_city.ToString();
                        param8.estadoname = usuarioCRM.estadoname.ToString();
                        param8.pais = usuarioCRM.pais.ToString();
                        param8.bairroprincipal = usuarioCRM.bairroprincipal.ToString();
                        param8.codigoassociado = codigoassociado;
                        param8.telefone = usuarioCRM.smart_ddidddtelefoneprincipal.ToString() + usuarioCRM.telephone1.ToString();
                        param8.email = usuarioCRM.email.ToString();

                        db.Insert(sql8, param8, false);
                    }

                    if (usuarioCRM.representante_smart_cpf != null && usuarioCRM.representante_smart_cpf != string.Empty)
                    {
                        //Inserção do Representante Legal
                        string sql9 = @"INSERT INTO [dbo].[ContatoAssociado]
                                       ([CodigoAssociado]
                                       ,[Nome],[CPFCNPJ],[CodigoTipoContato]
                                       ,[Telefone1],[Telefone2],[Telefone3]
                                       ,[Email],[Email2]
                                       ,[Status],[Observacao],[IndicadorPrincipal])
                                    VALUES (@codigoassociado
                                        ,@nome,@cpfcnpj,1
                                        ,@telefone1,@telefone2,@telefone3
                                        ,@mail1,@mail2
                                        ,1,NULL,1)";

                        dynamic param9 = new ExpandoObject();
                        param9.codigousuario = codigousuario;
                        param9.codigoassociado = codigoassociado;

                        if (usuarioCRM.representante_smart_pessoaname == null || usuarioCRM.representante_smart_pessoaname == string.Empty)
                        {
                            param9.nome = usuarioCRM.nome;
                        }
                        else
                        {
                            param9.nome = usuarioCRM.representante_smart_pessoaname;
                        }

                        param9.cpfcnpj = usuarioCRM.representante_smart_cpf.ToString().Replace(".", "").Replace("-", "").Replace("/", "");
                        param9.telefone1 = usuarioCRM.representante_telephone1;
                        param9.telefone2 = usuarioCRM.representante_telephone2;
                        param9.telefone3 = usuarioCRM.representante_telephone3;
                        param9.mail1 = usuarioCRM.representante_emailaddress1;
                        param9.mail2 = usuarioCRM.representante_emailaddress2;

                        db.Insert(sql9, param9, false);
                    }


                    //Inserção do perfil padrão GEPIR

                    string sql12 = @"INSERT INTO AssociadoPerfilGepir (CodigoAssociado,CodigoPerfilGepir, CodigoUsuarioAlteracao,DataAlteracao)
                                   SELECT @codigoassociado, COALESCE((select convert(bigint,valor) from [parametro]  where chave = 'gepir.perfilpadrao'),3),1,GETDATE()";

                    dynamic param12 = new ExpandoObject();
                    param12.codigoassociado = codigoassociado;

                    db.Insert(sql12, param12, false);



                    db.Commit();

                }
                catch (Exception ex)
                {
                    db.Rollback();
                    string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
                    throw new GS1TradeException("Não foi possível realizar o cadastro. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.", ex);
                }
            }
        }

        private void atualizaQuantidadeTentativas(int quantidade, string email)
        {
            //TODO - verificar tratamento
            try
            {
                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic paramQtd = new ExpandoObject();
                    paramQtd.quantidade = quantidade;
                    paramQtd.email = email;

                    string sql1 = "UPDATE Usuario SET QuantidadeTentativa = @quantidade WHERE Email = @email";

                    db.Update(sql1, paramQtd);
                }
            }
            catch (Exception ex)
            {
                throw new GS1TradeException("Erro ao atualizar dados do usuário.", ex);
            }

        }

        private void atualizaUltimoAcesso(long codigousuario)
        {
            //TODO - verificar tratamento
            try
            {
                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic param = new ExpandoObject();
                    param.codigousuario = codigousuario;

                    //Inserção na tabela UsuarioHistorico
                    string sql4 = @"INSERT INTO USUARIOHISTORICO ([Codigo],[Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email]
                                                                ,[TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao]
                                                                ,[MensagemObservacao],[DataAlteracaoSenha],[QuantidadeTentativa]
                                                                ,[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],[TelefoneComercial]
                                                                ,[Ramal],[Departamento],[Cargo],[CPFCNPJ], DATAHISTORICO)
	                                   SELECT [Codigo],[Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email]
                                                ,[TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao]
                                                ,[MensagemObservacao],[DataAlteracaoSenha],[QuantidadeTentativa]
                                                ,[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],[TelefoneComercial]
                                                ,[Ramal],[Departamento],[Cargo],[CPFCNPJ],GETDATE()
											FROM USUARIO
				                            WHERE USUARIO.CODIGO = @codigousuario";

                    db.Insert(sql4, param, false);

                    string sql1 = "UPDATE Usuario SET DataUltimoAcesso = getdate() WHERE Codigo = @codigousuario";

                    db.Update(sql1, param);
                }
            }
            catch (Exception ex)
            {
                throw new GS1TradeException("Erro ao atualizar dados do usuário.", ex);
            }

        }

        public string BuscarModulos(GS1.CNP.DAL.Database db, Int64 usuarioId)
        {
            string sql = string.Empty;

            //            sql = @"SELECT DISTINCT D.CODIGO, D.NOME, D.URL, D.ICONE
            //                    FROM PERFIL A WITH (NOLOCK)
            //                    INNER JOIN PERFILFUNCIONALIDADE B WITH (NOLOCK) ON B.CODIGOPERFIL = A.CODIGO
            //                    INNER JOIN FUNCIONALIDADE C WITH (NOLOCK) ON C.CODIGO = B.CODIGOFUNCIONALIDADE
            //                    INNER JOIN MODULO D WITH (NOLOCK) ON D.CODIGO = C.CODIGOMODULO
            //                    INNER JOIN USUARIO E WITH (NOLOCK) ON E.CODIGOPERFIL = A.CODIGO
            //                    LEFT JOIN FORNECEDOR F WITH (NOLOCK) ON F.CODIGOUSUARIO = E.CODIGO
            //                    LEFT JOIN USUARIO G WITH (NOLOCK) ON G.CODIGO = E.CODIGOUSUARIOFORNECEDOR
            //					LEFT JOIN FORNECEDOR H WITH (NOLOCK) ON H.CODIGOUSUARIO = G.CODIGO
            //                    WHERE E.CODIGO = @ID
            //                    AND A.STATUS = 1 AND C.STATUS = 1 AND D.STATUS = 1 
            //                    AND ( 
            //							E.CODIGOUSUARIOFORNECEDOR IS NULL
            //							AND
            //							(
            //								E.CODIGOSTATUSUSUARIO = 1
            //								OR (
            //									E.CODIGOSTATUSUSUARIO = 6
            //									AND E.CODIGOTIPOUSUARIO = 2
            //									AND  CASE F.CODIGOTIPOASSOCIADO 
            //										 WHEN 1 THEN (SELECT DATEDIFF(day,GETDATE(),DATEADD(DAY,COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.nudias.bloqueio.inadimplente.premium' ),0),F.DATASTATUSINADIMPLENTE)))
            //										 WHEN 0 THEN (SELECT DATEDIFF(day,GETDATE(),DATEADD(DAY,COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.nudias.bloqueio.inadimplente.standard' ),0),F.DATASTATUSINADIMPLENTE)))
            //									     END >= 0
            //									)
            //							)							
            //							OR											
            //							E.CODIGOUSUARIOFORNECEDOR IS NOT NULL
            //							AND
            //							(	
            //								G.CODIGOSTATUSUSUARIO = 1
            //								OR (
            //									G.CODIGOSTATUSUSUARIO = 6
            //									AND G.CODIGOTIPOUSUARIO = 2
            //									AND CASE H.CODIGOTIPOASSOCIADO 
            //										WHEN 1 THEN (SELECT DATEDIFF(day,GETDATE(),DATEADD(DAY,COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.nudias.bloqueio.inadimplente.premium' ),0),H.DATASTATUSINADIMPLENTE)))
            //										WHEN 0 THEN (SELECT DATEDIFF(day,GETDATE(),DATEADD(DAY,COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.nudias.bloqueio.inadimplente.standard' ),0),H.DATASTATUSINADIMPLENTE)))
            //									     END >= 0
            //								)
            //							)
            //														
            //						)";







            sql = @"SELECT (SELECT DISTINCT MODULO.CODIGO AS Codigo, MODULO.NOME AS Nome, MODULO.ICONE as Icone, MODULO.URL as Url, Formulario.Codigo, Formulario.Nome, Formulario.Url, Formulario.Icone
                            ,MODULO.Ordem, Formulario.Ordem
                            FROM FORMULARIO AS FORMULARIO
                            INNER JOIN MODULO AS MODULO ON MODULO.CODIGO = FORMULARIO.CODIGOMODULO
                            INNER JOIN FUNCIONALIDADE AS FUNCIONALIDADE ON FUNCIONALIDADE.CodigoFormulario = FORMULARIO.Codigo
                            INNER JOIN PERFILFUNCIONALIDADE AS PERFILFUNCIONALIDADE ON PERFILFUNCIONALIDADE.CODIGOFUNCIONALIDADE =  FUNCIONALIDADE.CODIGO
                            INNER JOIN USUARIO AS USUARIO ON USUARIO.CODIGOPERFIL = PERFILFUNCIONALIDADE.CODIGOPERFIL
                            WHERE USUARIO.CODIGO = @ID
                            AND FORMULARIO.STATUS = 1 AND MODULO.STATUS = 1 AND FUNCIONALIDADE.STATUS = 1
                            AND USUARIO.CODIGOSTATUSUSUARIO = 1 
                            AND FORMULARIO.INDICADORMENU = 1
                            AND   ( SELECT
										CASE			
											WHEN  A.CODIGOTIPOACEITE = 2 AND B.CODIGOUSUARIO IS NULL THEN 1 --ACEITE PARA TODOS
											WHEN A.CODIGOTIPOACEITE = 1 AND B.CODIGOUSUARIO IS NULL AND A.DATACRIACAO <= C.DATACADASTRO THEN 2 --ACEITE PARA NOVOS
											ELSE NULL
										END
									FROM TERMOADESAO A
									LEFT JOIN TERMOADESAOUSUARIO B ON B.CODIGOTERMOADESAO = A.CODIGO AND B.CODIGOUSUARIO = @ID 
									LEFT JOIN USUARIO C ON C.CODIGO = @ID
									WHERE A.CODIGOSTATUSTERMOADESAO = 2
								) IS NULL
                            ORDER BY MODULO.Ordem, Formulario.Ordem
                            FOR XML AUTO
                            ,ROOT('Modulos')
                    ) AS XMLMENU";



            dynamic parametro = JObject.Parse("{ ID: " + usuarioId.ToString() + " }").ToExpando();

            List<dynamic> retorno = db.Select(sql, parametro);
            if (retorno.Count > 0)
            {
                return retorno[0].xmlmenu;
            }
            else
            {
                return string.Empty;
            }



        }

        public List<string> BuscarPermissoes(GS1.CNP.DAL.Database db, Int64 usuarioId)
        {
            string sql = string.Empty;

            //            sql = @"SELECT C.NOME
            //                    FROM PERFIL A WITH (NOLOCK)
            //                    INNER JOIN PERFILFUNCIONALIDADE B WITH (NOLOCK) ON B.CODIGOPERFIL = A.CODIGO
            //                    INNER JOIN FUNCIONALIDADE C WITH (NOLOCK) ON C.CODIGO = B.CODIGOFUNCIONALIDADE
            //                    INNER JOIN USUARIO E WITH (NOLOCK) ON E.CODIGOPERFIL = A.CODIGO
            //                    LEFT JOIN FORNECEDOR F WITH (NOLOCK) ON F.CODIGOUSUARIO = E.CODIGO
            //                    LEFT JOIN USUARIO G WITH (NOLOCK) ON G.CODIGO = E.CODIGOUSUARIOFORNECEDOR
            //					LEFT JOIN FORNECEDOR H WITH (NOLOCK) ON H.CODIGOUSUARIO = G.CODIGO
            //                    WHERE E.CODIGO = @ID
            //                    AND A.STATUS = 1 AND C.STATUS = 1 
            //                    AND ( 
            //							E.CODIGOUSUARIOFORNECEDOR IS NULL
            //							AND
            //							(
            //								E.CODIGOSTATUSUSUARIO = 1
            //								OR (
            //									E.CODIGOSTATUSUSUARIO = 6
            //									AND E.CODIGOTIPOUSUARIO = 2
            //									AND  CASE F.CODIGOTIPOASSOCIADO 
            //										 WHEN 1 THEN (SELECT DATEDIFF(day,GETDATE(),DATEADD(DAY,COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.nudias.bloqueio.inadimplente.premium' ),0),F.DATASTATUSINADIMPLENTE)))
            //										 WHEN 0 THEN (SELECT DATEDIFF(day,GETDATE(),DATEADD(DAY,COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.nudias.bloqueio.inadimplente.standard' ),0),F.DATASTATUSINADIMPLENTE)))
            //									     END >= 0
            //									)
            //							)							
            //							OR											
            //							E.CODIGOUSUARIOFORNECEDOR IS NOT NULL
            //							AND
            //							(	
            //								G.CODIGOSTATUSUSUARIO = 1
            //								OR (
            //									G.CODIGOSTATUSUSUARIO = 6
            //									AND G.CODIGOTIPOUSUARIO = 2
            //									AND CASE H.CODIGOTIPOASSOCIADO 
            //										WHEN 1 THEN (SELECT DATEDIFF(day,GETDATE(),DATEADD(DAY,COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.nudias.bloqueio.inadimplente.premium' ),0),H.DATASTATUSINADIMPLENTE)))
            //										WHEN 0 THEN (SELECT DATEDIFF(day,GETDATE(),DATEADD(DAY,COALESCE((SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.nudias.bloqueio.inadimplente.standard' ),0),H.DATASTATUSINADIMPLENTE)))
            //									     END >= 0
            //								)
            //							)
            //														
            //						)";


            sql = @"SELECT A.NOME
                    FROM FUNCIONALIDADE A
                    INNER JOIN PERFILFUNCIONALIDADE B ON B.CODIGOFUNCIONALIDADE = A.CODIGO
                    INNER JOIN USUARIO C ON C.CODIGOPERFIL = B.CODIGOPERFIL
                    WHERE C.CODIGO = @ID
                    AND A.STATUS = 1 
                    AND (C.CODIGOSTATUSUSUARIO = 1 OR C.CODIGOSTATUSUSUARIO = 5 AND A.CODIGO = 1)
                    AND   ( SELECT
										CASE			
											WHEN  A.CODIGOTIPOACEITE = 2 AND B.CODIGOUSUARIO IS NULL THEN 1 --ACEITE PARA TODOS
											WHEN A.CODIGOTIPOACEITE = 1 AND B.CODIGOUSUARIO IS NULL AND A.DATACRIACAO <= C.DATACADASTRO THEN 2 --ACEITE PARA NOVOS
											ELSE NULL
										END
									FROM TERMOADESAO A
									LEFT JOIN TERMOADESAOUSUARIO B ON B.CODIGOTERMOADESAO = A.CODIGO AND B.CODIGOUSUARIO = @ID 
									LEFT JOIN USUARIO C ON C.CODIGO = @ID
									WHERE A.CODIGOSTATUSTERMOADESAO = 2
								) IS NULL";


            dynamic parametro = new ExpandoObject();
            parametro.ID = usuarioId;

            Func<IDataRecord, string> binder = r => r.GetString(0);

            List<string> result = db.Select<string>(sql, parametro, binder);

            return result;
        }

        private bool BuscarStatusTermoAceite(GS1.CNP.DAL.Database db, Int64 usuarioId)
        {
            string sql = string.Empty;

            sql = @"SELECT 
		                CASE			
			                WHEN  A.CODIGOTIPOACEITE = 2 AND B.CodigoUsuario IS NULL THEN 1 --Aceite para todos
			                WHEN A.CODIGOTIPOACEITE = 1 AND B.CodigoUsuario IS NULL AND A.DATACRIACAO <= C.DataCadastro THEN 2 --Aceite para novos
			                ELSE NULL
		                END
	                FROM TermoAdesao A
	                    LEFT JOIN TermoAdesaoUsuario B ON B.CodigoTermoAdesao = A.Codigo AND B.CodigoUsuario = @ID 
	                    LEFT JOIN USUARIO C ON C.CODIGO = @ID
	                WHERE A.CodigoStatusTermoAdesao = 2
                        AND CASE			
			                WHEN  A.CODIGOTIPOACEITE = 2 AND B.CODIGOUSUARIO IS NULL THEN 1 --ACEITE PARA TODOS
			                WHEN A.CODIGOTIPOACEITE = 1 AND B.CODIGOUSUARIO IS NULL AND A.DATACRIACAO <= C.DATACADASTRO THEN 2 --ACEITE PARA NOVOS
			                ELSE NULL
		                END IS NOT NULL";


            dynamic parametro = new ExpandoObject();
            parametro.ID = usuarioId;

            Func<IDataRecord, int> binder = r => r.GetInt32(0);

            List<int> result = db.Select<int>(sql, parametro, binder);

            return result.Count > 0;
        }


        [GS1CNPAttribute(false)]
        public List<Associado> BuscarAssociadosUsuario(dynamic parametro)
        {
            try
            {
                string sql = string.Empty;
                sql = @"SELECT A.CODIGO, A.NOME, A.CPFCNPJ, A.CAD, A.INDICADORCNAERESTRITIVO, A.ATIVADO, A.APROVACAO, P.CodigoPerfilGepir
					    FROM ASSOCIADO A
					    INNER JOIN ASSOCIADOUSUARIO B ON B.CODIGOASSOCIADO = A.CODIGO
                        LEFT JOIN ASSOCIADOPERFILGEPIR P ON P.CodigoAssociado = A.Codigo
					    WHERE B.CODIGOUSUARIO = @usuarioId ";


                if (this.getSession("PERMISSOES_USUARIO_LOGADO") != null && ((IEnumerable)this.getSession("PERMISSOES_USUARIO_LOGADO")).Cast<object>().ToList().Count == 0)
                {
                    sql = sql + " AND (A.CodigoStatusAssociado = 1 OR A.CodigoStatusAssociado = 5)";
                }else{
                    sql = sql + " AND A.CodigoStatusAssociado = 1";
                }

                dynamic param = null;

                if (parametro.where is JToken)
                    param = ((JToken)parametro.where).ToExpando();
                else
                    param = parametro.where;

                Func<IDataRecord, Associado> binder = r => new Associado()
                {
                    codigo = r.GetInt64(0),
                    nome = r.GetString(1),
                    cpfcnpj = r.GetString(2),
                    cad = r.GetString(3),
                    indicadorcnaerestritivo = (r.IsDBNull(4) ? string.Empty : r.GetString(4)),
                    ativado = r.GetBoolean(5),
                    aprovacao = r.GetBoolean(6),
                    codigoperfilgepir = r.GetInt32(7)
                };

                List<Associado> result;

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    result = db.Select(sql, param, binder);
                    this.setSession("ASSOCIADOS_USUARIO_LOGADO", result);

                    List<Associado> associados = this.getSession("ASSOCIADOS_USUARIO_LOGADO") as List<Associado>;
                    if (associados.Count > 0)
                    {
                        this.setSession("ASSOCIADO_SELECIONADO", associados[0]);
                    }

                }

                return result;
            }
            catch (Exception ex)
            {

                throw new Exception("Erro ao Buscar Associados do Usuário.");
            }

        }

        [GS1CNPAttribute(false)]
        public object BuscarAssociadosUsuarioAutoComplete(dynamic parametro)
        {

            string sql = @"SELECT A.CODIGO, A.NOME, A.CPFCNPJ, A.CAD, A.INDICADORCNAERESTRITIVO, P.CodigoPerfilGepir 
					        FROM ASSOCIADO A WITH (NOLOCK)
                            LEFT JOIN ASSOCIADOPERFILGEPIR P  (NOLOCK) ON P.CodigoAssociado = A.Codigo					        
					        WHERE A.CODIGOSTATUSASSOCIADO = 1
                            AND (A.NOME LIKE '%' + @nome + '%' OR A.CPFCNPJ LIKE '%' + @nome + '%' OR A.CAD LIKE '%' + @nome + '%')";

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();

                dynamic param = null;
                Func<IDataRecord, Associado> binder = r => new Associado()
                {
                    codigo = r.GetInt64(0),
                    nome = r.GetString(1),
                    cpfcnpj = r.GetString(2),
                    cad = r.GetString(3),
                    indicadorcnaerestritivo = (r.IsDBNull(4) ? string.Empty : r.GetString(4)),
                    codigoperfilgepir = r.GetInt32(5)
                };

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<Associado> associados = db.Select(sql, param, binder);

                return JsonConvert.SerializeObject(associados);
            }
        }

        [GS1CNPAttribute(false)]
        public void AlteraAssociado(dynamic parametro)
        {
            try
            {
                List<Associado> associados = this.getSession("ASSOCIADOS_USUARIO_LOGADO") as List<Associado>;
                dynamic param = ((JToken)parametro.where).ToExpando();
                bool associadolocalizado = false;

                if (param.codigo == -1)
                {
                    HttpContext.Current.Session.Remove("ASSOCIADO_SELECIONADO");
                }
                else
                {
                    for (Int32 index = 0; index <= associados.Count - 1; index++)
                    {
                        if (Convert.ToString(associados[index].codigo) == Convert.ToString(param.codigo))
                        {
                            this.setSession("ASSOCIADO_SELECIONADO", associados[index]);
                            associadolocalizado = true;
                        }
                    }
                    if (!associadolocalizado)
                    {
                        HttpContext.Current.Session.Remove("ASSOCIADO_SELECIONADO");
                        //throw new Exception("Erro ao Selecionar Associado do Usuário.");
                    }
                }

            }
            catch (Exception ex)
            {
                HttpContext.Current.Session.Remove("ASSOCIADO_SELECIONADO");
                //throw new Exception("Erro ao Selecionar Associado do Usuário.");
            }

        }

        [GS1CNPAttribute(false)]
        public void AlteraAssociadoGS1(dynamic parametro)
        {
            try
            {
                dynamic param = ((JToken)parametro.where).ToExpando();

                if (param != null && param.codigo != null)
                {
                    Associado associado = new Associado();
                    associado.codigo = param.codigo;
                    associado.nome = param.nome;
                    associado.cpfcnpj = param.cpfcnpj;
                    associado.cad = param.cad;
                    associado.indicadorcnaerestritivo = param.indicadorcnaerestritivo;
                    associado.codigoperfilgepir = param.codigoperfilgepir;
                    this.setSession("ASSOCIADO_SELECIONADO", associado);
                }
                else
                {
                    HttpContext.Current.Session.Remove("ASSOCIADO_SELECIONADO");
                }

            }
            catch (Exception ex)
            {
                HttpContext.Current.Session.Remove("ASSOCIADO_SELECIONADO");
                //throw new Exception("Erro ao Selecionar Associado do Usuário.");
            }

        }

        [GS1CNPAttribute(false)]
        public string VerificarAssociadoSelecionado(dynamic parametro)
        {
            try
            {
                return JsonConvert.SerializeObject(((Associado)this.getSession("ASSOCIADO_SELECIONADO")));
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao Selecionar Associado do Usuário.");
            }

        }

        [GS1CNPAttribute(false)]
        public string RetornaModulosUsuarioLogado(dynamic parametro)
        {
            try
            {
                Login user = this.getSession("USUARIO_LOGADO") as Login;

                if (user != null && (user.id_statususuario == 1 || user.id_statususuario == 5))
                {
                    return JsonConvert.SerializeObject(this.getSession("PERFIL_USUARIO_LOGADO").ToString());
                }
                else
                    return string.Empty;
            }
            catch (Exception)
            {

                throw new Exception("Erro ao Buscar Permissões do Usuário.");
            }

        }

        [GS1CNPAttribute(false)]
        public string RetornaFuncionalidadesUsuarioLogado(dynamic parametro)
        {
            try
            {

                return JsonConvert.SerializeObject(((IEnumerable)this.getSession("PERMISSOES_USUARIO_LOGADO")).Cast<object>().ToList());
            }
            catch (Exception)
            {

                throw new Exception("Erro ao Buscar Permissões do Usuário.");
            }

        }

        [GS1CNPAttribute(false)]
        public string RetornaUsuarioLogado(dynamic parametro)
        {
            try
            {
                return JsonConvert.SerializeObject(this.getSession("USUARIO_LOGADO"));
            }
            catch (Exception)
            {
                throw new Exception("Erro ao Buscar Usuário.");
            }
        }

        private string GeraSenha()
        {
            string guid = Guid.NewGuid().ToString().Replace("-", "");

            Random clsRan = new Random();
            Int32 tamanhoSenha = clsRan.Next(7, 10);

            string senha = "";
            for (Int32 i = 0; i <= tamanhoSenha; i++)
            {
                senha += guid.Substring(clsRan.Next(1, guid.Length), 1);
            }

            return senha;
        }

        public string GeraSHA1(string valor)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(valor, "SHA1");
        }

        public string GerarMD5(string valor)
        {

            // Cria uma nova intância do objeto que implementa o algoritmo para criptografia MD5
            MD5 md5Hasher = MD5.Create();

            // Criptografa o valor passado
            byte[] valorCriptografado = md5Hasher.ComputeHash(Encoding.Default.GetBytes(valor));

            // Cria um StringBuilder para passarmos os bytes gerados para ele
            StringBuilder strBuilder = new StringBuilder();

            // Converte cada byte em um valor hexadecimal e adiciona ao string builder e formata cada um como uma string hexadecimal.
            for (int i = 0; i < valorCriptografado.Length; i++)
            {
                strBuilder.Append(valorCriptografado[i].ToString("x2"));
            }

            // retorna o valor criptografado como string
            return strBuilder.ToString();
        }

        [GS1CNPAttribute(false, true)]
        public int Recupera(dynamic parametro)
        {
            string email = parametro.campos["email"];
            string codigo = parametro.campos["codigo"];

            List<string> listParam = new List<string>();
            listParam.Add(email);
            listParam.Add(codigo);

            Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase(chaveDB);
            string sql = @"SELECT CASE 
			                            --WHEN EXISTS (SELECT TOP 1 Z.CODIGOASSOCIADO FROM AssociadoUsuario Z WHERE Z.CodigoUsuario = A.CODIGO AND Z.IndicadorPrinciPal = 1) THEN 5
			                            WHEN CodigoStatusUsuario = 3 THEN 4
			                            WHEN CodigoStatusUsuario = 2 THEN 2
			                            ELSE 1
			                            END AS STATUSRECUPERA

                        FROM USUARIO A
                        WHERE A.Email = @email";

            using (DbConnection conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction trans = conn.BeginTransaction();

                DbCommand cmd = db.GetSqlStringCommand(sql);
                cmd.Connection = conn;
                cmd.Transaction = trans;

                DbParameter parameter = cmd.CreateParameter();
                parameter.ParameterName = "@email";
                parameter.Value = listParam[0];
                cmd.Parameters.Add(parameter);

                //Execura query para validar se o email está cadastrado no banco
                var result = cmd.ExecuteScalar();

                if (result != null)
                {

                    #region usuário ativo
                    if ((int)result == 1)
                    {
                        var novasenha = GeraSenha();

                        string sql1 = "UPDATE Usuario SET Senha = @nova, CodigoStatusUsuario = 5, QuantidadeTentativa = 0 WHERE Email = @email";// and Codigo = @codigo";
                        DbCommand cmd1 = db.GetSqlStringCommand(sql1);

                        cmd1.Connection = db.CreateConnection();
                        cmd1.Connection.Open();

                        var senhaCriptografada = GeraSHA1(novasenha);

                        DbParameter parameter2 = cmd1.CreateParameter();
                        parameter2.ParameterName = "@nova";
                        parameter2.Value = senhaCriptografada;
                        cmd1.Parameters.Add(parameter2);

                        DbParameter parameter1 = cmd1.CreateParameter();
                        parameter1.ParameterName = "@email";
                        parameter1.Value = listParam[0];
                        cmd1.Parameters.Add(parameter1);

                        //Executa query para atualizar campo no banco
                        cmd1.ExecuteReader();

                        #region Envio de email
                        //Envio de email
                        try
                        {

                            String url = ConfigurationManager.AppSettings["URLCaminhoImagemEmail"];
                            String urlTrade = ConfigurationManager.AppSettings["URLCaminhoCNPEmail"];
                            String urlGS1 = ConfigurationManager.AppSettings["URLCaminhoGS1Email"];
                            String urlFacebook = ConfigurationManager.AppSettings["URLCaminhoFacebookEmail"];
                            String urlLinkedin = ConfigurationManager.AppSettings["URLCaminhoLinkedinEmail"];
                            String urlTwitter = ConfigurationManager.AppSettings["URLCaminhoTwitterEmail"];
                            String urlYoutube = ConfigurationManager.AppSettings["URLCaminhoYoutubeEmail"];

                            string mensagem = @"<div id='principal' style='text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; color:#000000; background-color:#FFFFFF; font-family: verdana, arial, helvetica; '>
		                                        <table align='center' width='600' border='0' cellpadding='0' cellspacing='0' style='border-width: 0px; background-color: #ffffff;'>
			                                        <tr valign='top'>
				                                        <td><p style=' text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;'>
                                                            <img width='600' height='134' alt='' hspace=1 vspace=1 src='" + url + @"/header_email.png'></p>
                                                        </td>
			                                        </tr>
			                                        <tr style='font-size: 14pt; font-weight:bold; color:#F48332;'>
				                                        <td><p style='padding:0px 10px 0px 10px;'> Caro usuário, </p> </td>
			                                        </tr>
			                                        <tr>
				                                        <td>
                                                            <p style='padding:10px 10px 0px 10px;'> 
                                                                <span style='font-size: 11pt; font-family: verdana, arial, helvetica;'>
                                                                    Este é um e-mail automático. Por favor não responda. <br><br>
                                                                    Você solicitou a recuperação de senha no sistema CNP.<br><br>
                                                                    Login: " + listParam[0] + @"<br>
                                                                    Senha recuperada: " + novasenha + @"<br><br>  
                                                                    Troque sua senha no próximo acesso ao CNP (a troca de senha é obrigatória).<br>                                          
                                                                    Para acessar o CNP, clique <a href='" + urlTrade + @"'> aqui </a><br><br>
                                                                    Equipe <b> GS1 </b><span style='color: red'>Brasil</span><br>
                                                                </span>
                                                                <div style='font-size: 8pt; text-align:left; font-family: verdana, arial, helvetica;'>
                                                                    Acompanhe: <a href='" + urlFacebook + @"'><img src='" + url + @"/fb.png'></a> <a href='" + urlLinkedin + @"'><img src='" + url + @"/linkedin.png'></a> <a href='" + urlTwitter + @"'><img src='" + url + @"/twitter.png'></a> <a href='" + urlYoutube + @"'><img src='" + url + @"/youtube.png'></a> 
                                                                </div>
                                                            </p>
                                                        </td>
			                                        </tr>
			                                        <tr valign='top'>
				                                        <td><p style='text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;'><img width='600' height='134' alt='' hspace=1 vspace=1 src='" + url + @"/footer_email.png'></p></td>
			                                        </tr>
		                                        </table>
	                                        </div>";

                            //TODO - Retirar hardcode do email

                            //email = "igorfragola@softbox.com.br;flaviaoliveira@softbox.com.br;eduardolima@softbox.com.br";

                            GS1.CNP.BLL.Core.Util.EnviarEmail(email, "[CNP] Nova senha", mensagem, true);
                            return 1;
                        }
                        catch (Exception)
                        {
                            trans.Rollback();
                            //Erro de envio de email
                            return 3;
                        }
                        #endregion
                    }
                    else
                    {
                        trans.Commit();
                        return (int)result;
                    }
                    #endregion
                }
                else
                {
                    trans.Rollback();
                    //Retorno de usuário inexistente
                    return 0;
                }
            }

        }

        [GS1CNPAttribute(false)]
        public int Alterarsenha(dynamic parametro)
        {
            List<string> listParam = new List<string>();
            Login user = this.getSession("USUARIO_LOGADO") as Login;
            Core.Util util = new Core.Util();

            if (user != null)
            {

                string senha_atual = parametro.campos["senha_atual"];
                string nova_senha = parametro.campos["nova_senha"];
                string confirmar_senha = parametro.campos["confirmar_senha"];

                listParam.Add(senha_atual);
                listParam.Add(nova_senha);
                listParam.Add(confirmar_senha);
                listParam.Add(user.nome);
                listParam.Add(user.id.ToString());
                listParam.Add(user.email);

                //Cria conexão com banco
                Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase(chaveDB);
                string sql = "SELECT 1 FROM Usuario WITH (NOLOCK) where Email = @email AND Senha = @senha";
                DbCommand cmd = db.GetSqlStringCommand(sql);

                cmd.Connection = db.CreateConnection();
                cmd.Connection.Open();

                DbParameter parameter = cmd.CreateParameter();
                parameter.ParameterName = "@email";
                parameter.Value = listParam[5];
                cmd.Parameters.Add(parameter);

                //Criptografa senha
                var senha = util.GeraSHA1(listParam[0]);

                DbParameter parameter1 = cmd.CreateParameter();
                parameter1.ParameterName = "@senha";
                parameter1.Value = senha;
                cmd.Parameters.Add(parameter1);


                //Executa query para avaliar se os campos são correspondentes ao banco
                var result1 = cmd.ExecuteScalar();

                if (result1 != null && (int)result1 == 1)
                {
                    char[] nova_senha_dividida = nova_senha.ToCharArray();
                    char[] ultima_senha_dividida = senha_atual.ToCharArray();
                    int contador = 0;

                    //Quantidade de caracteres iguais na 'senha atual' e na 'nova senha'
                    for (int i = 0; i < nova_senha_dividida.Length; i++)
                    {
                        for (int j = 0; j < ultima_senha_dividida.Length; j++)
                        {
                            if (nova_senha_dividida[i] == ultima_senha_dividida[j])
                                contador++;
                        }
                    }

                    //Se 'nova senha' conter 60% dos caracteres da última senha, o sistema retorna erro
                    if (contador >= ((nova_senha_dividida.Length / 2) + 1))
                        return 2;
                    else
                    {

                        //Seleciona quantidade de senhas que serão consideradas
                        string sql_senha_n_dias = "SELECT DISTINCT Valor FROM parametro WITH (NOLOCK) WHERE chave = 'autenticacao.senha.diferente.n.ultimas'";
                        DbCommand cmd_senha_n_dias = db.GetSqlStringCommand(sql_senha_n_dias);

                        cmd_senha_n_dias.Connection = db.CreateConnection();
                        cmd_senha_n_dias.Connection.Open();

                        var result_n_dias = cmd_senha_n_dias.ExecuteScalar();

                        string sql_senhas = "SELECT TOP (CAST (@nsenhas AS int) ) Senha FROM UsuarioHistoricoSenha WITH (NOLOCK) WHERE CodigoUsuario = @codigoUsuario ORDER BY DataHistorico DESC";

                        DbCommand cmd_pega_n_senhas = db.GetSqlStringCommand(sql_senhas);

                        cmd_pega_n_senhas.Connection = db.CreateConnection();
                        cmd_pega_n_senhas.Connection.Open();

                        DbParameter parameter_codigo_user1 = cmd_pega_n_senhas.CreateParameter();
                        parameter_codigo_user1.ParameterName = "@codigoUsuario";
                        parameter_codigo_user1.Value = listParam[4];
                        cmd_pega_n_senhas.Parameters.Add(parameter_codigo_user1);

                        DbParameter n_senhas = cmd_pega_n_senhas.CreateParameter();
                        n_senhas.ParameterName = "@nsenhas";
                        n_senhas.Value = result_n_dias;
                        cmd_pega_n_senhas.Parameters.Add(n_senhas);

                        var result_n_senhas = cmd_pega_n_senhas.ExecuteReader();
                        var percorreResult = result_n_senhas;
                        var contadorSenhasRepetidas = 0;

                        while (percorreResult.Read())
                        {
                            if (percorreResult[0].ToString() == util.GeraSHA1(nova_senha))
                            {
                                contadorSenhasRepetidas++;
                            }
                        }

                        if (contadorSenhasRepetidas == 0)
                        {
                            //Atualiza campo senha no banco
                            try
                            {
                                string sql1 = "UPDATE Usuario SET Senha = @senhanova, DataAlteracaoSenha = @dataAtual WHERE Codigo = @id";

                                //TODO - enumerador
                                if (user.id_statususuario == 5)
                                {
                                    sql1 = sql1 + ";UPDATE Usuario SET CodigoStatusUsuario = 1 WHERE Codigo = @id;";
                                }

                                DbCommand cmd1 = db.GetSqlStringCommand(sql1);

                                cmd1.Connection = db.CreateConnection();
                                cmd1.Connection.Open();

                                //Rotina de criptografar senha
                                var senhaCriptografada = util.GeraSHA1(listParam[1]);

                                DbParameter parameter2 = cmd1.CreateParameter();
                                parameter2.ParameterName = "@senhanova";
                                parameter2.Value = senhaCriptografada;
                                cmd1.Parameters.Add(parameter2);

                                DbParameter parameter3 = cmd1.CreateParameter();
                                parameter3.ParameterName = "@id";
                                parameter3.Value = listParam[4];
                                cmd1.Parameters.Add(parameter3);

                                DbParameter parameter4 = cmd1.CreateParameter();
                                parameter4.ParameterName = "@dataAtual";
                                parameter4.Value = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff"); ;
                                cmd1.Parameters.Add(parameter4);

                                //Executa query que atualiza a senha no banco
                                var result2 = cmd1.ExecuteReader();


                                //Caso troca obrigatória por bloqueio ou expiração de senha refaz a sessão após a troca
                                if ((user.id_statususuario == 5) || (user.avisosenha == 2))
                                {
                                    GS1.CNP.DAL.Database db2 = new GS1.CNP.DAL.Database("BD");
                                    db2.Open();

                                    string sqluser = @"SELECT CODIGO, CODIGOPERFIL, CODIGOTIPOUSUARIO, NOME, QUANTIDADETENTATIVA, SENHA, CODIGOSTATUSUSUARIO, EMAIL
                                                           FROM USUARIO WITH (NOLOCK)
                                                           WHERE CODIGO =" + user.id.ToString() + " AND CodigoTipoUsuario in (1,2)"; ;

                                    //dynamic param = ((JToken)parametro.where).ToExpando();
                                    Func<IDataRecord, Login> binder = r => new Login()
                                    {
                                        id = r.GetInt64(0),
                                        id_perfil = r.GetInt32(1),
                                        id_tipousuario = r.GetInt32(2),
                                        nome = r.GetString(3),
                                        quantidade_tentativa = r.GetInt32(4),
                                        senha = r.GetString(5),
                                        id_statususuario = r.GetInt32(6),
                                        email = r.GetString(7)
                                    };
                                    List<Login> usuarios = db2.Select<Login>(sqluser, null, binder);

                                    var perfil = BuscarModulos(db2, Convert.ToInt32(user.id));
                                    this.setSession("PERFIL_USUARIO_LOGADO", perfil);

                                    List<string> permissoes = BuscarPermissoes(db2, Convert.ToInt32(user.id));
                                    this.setSession("PERMISSOES_USUARIO_LOGADO", permissoes);

                                    if (usuarios != null && usuarios.Count() > 0)
                                    {
                                        dynamic usuarioRelogado = usuarios.FirstOrDefault();
                                        usuarioRelogado.permissoes = permissoes;

                                        this.setSession("USUARIO_LOGADO", usuarioRelogado);
                                    }

                                }

                            }
                            catch (Exception e)
                            {
                                throw e;
                            }

                            //Insere senha nova na tabela histórico usuário
                            try
                            {
                                string sql_historico = "INSERT INTO USUARIOHISTORICOSENHA (CODIGOUSUARIO, SENHA, DATAHISTORICO) VALUES (@codigoUsuario, @senhaatual, @dataHistorico)";
                                DbCommand cmd_historico = db.GetSqlStringCommand(sql_historico);

                                cmd_historico.Connection = db.CreateConnection();
                                cmd_historico.Connection.Open();

                                DbParameter parameter_codigo_user = cmd_historico.CreateParameter();
                                parameter_codigo_user.ParameterName = "@codigoUsuario";
                                parameter_codigo_user.Value = listParam[4];
                                cmd_historico.Parameters.Add(parameter_codigo_user);

                                //Rotina de criptografar senha
                                var senhaCriptografada = util.GeraSHA1(listParam[0]);

                                DbParameter parameter_senha_atual = cmd_historico.CreateParameter();
                                parameter_senha_atual.ParameterName = "@senhaatual";
                                parameter_senha_atual.Value = senhaCriptografada;
                                cmd_historico.Parameters.Add(parameter_senha_atual);

                                DbParameter parameter_data = cmd_historico.CreateParameter();
                                parameter_data.ParameterName = "@dataHistorico";
                                parameter_data.Value = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                                cmd_historico.Parameters.Add(parameter_data);

                                //Executa query que insere a senha na tabela de histórico
                                var resultado_consulta = cmd_historico.ExecuteReader();
                                return 1;

                            }
                            catch (Exception e)
                            {
                                throw e;
                            }

                            //return 4;
                        }
                        else
                            return 3; //Erro caso a senha ja foi utilizada anteriormente

                    }
                }
                else
                    return 0;
            }
            else
                return 0;
        }

        [GS1CNPAttribute(false, true)]
        public bool VerificareCAPTCHA(string response)
        {

            // web client
            string resposta = string.Empty;
            WebClient client = new WebClient();
            client.Headers["Content-type"] = "application/x-www-form-urlencoded";

            if (ConfigurationManager.AppSettings.Count > 0)
            {
                string reCAPTCHAURL = ConfigurationManager.AppSettings["reCAPTCHAURL"];
                string reCAPTCHAKEY = ConfigurationManager.AppSettings["reCAPTCHAPRIVATEKEY"];

                if ((reCAPTCHAURL != null && reCAPTCHAURL != string.Empty) && (reCAPTCHAKEY != null && reCAPTCHAKEY != string.Empty))
                {
                    try
                    {
                        string data = string.Format("secret={0}&response={1}", reCAPTCHAKEY, response);
                        Uri reCAPTCHARequest = new Uri(reCAPTCHAURL.ToString());

                        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(reCAPTCHARequest);

                        request.Method = "POST";
                        request.ContentType = "text/plain;charset=utf-8";
                        request.ContentType = "application/x-www-form-urlencoded";
                        request.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

                        System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                        byte[] bytes = encoding.GetBytes(data);

                        request.ContentLength = bytes.Length;
                        using (Stream requestStream = request.GetRequestStream())
                        {
                            requestStream.Write(bytes, 0, bytes.Length);
                        }

                        HttpWebResponse requestResponse = (HttpWebResponse)request.GetResponse();

                        StreamReader srResponse = new StreamReader(requestResponse.GetResponseStream());
                        resposta = srResponse.ReadToEnd();

                        if (!string.IsNullOrEmpty(resposta))
                        {
                            dynamic result = JsonConvert.DeserializeObject(resposta);

                            if (result != null && result.success != null && result.success == true)
                            {
                                return true;
                            }
                            else
                            {
                                try
                                {
                                    if (!EventLog.SourceExists(EVENT_SOURCE))
                                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                                    EventLog.WriteEntry(EVENT_SOURCE, string.Format("Erro ao validar RECAPTCHA: {0} secret={1}&response={2} Url: {3}", resposta, reCAPTCHAKEY, response, reCAPTCHAURL), EventLogEntryType.Information, 1);
                                }
                                catch (Exception)
                                {

                                }

                                return false;
                            }
                        }
                        else
                        {
                            throw new GS1TradeException("Erro ao validar código RECAPTCHA.");
                        }
                    }
                    catch (Exception ex)
                    {

                        try
                        {
                            if (!EventLog.SourceExists(EVENT_SOURCE))
                                EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                            EventLog.WriteEntry(EVENT_SOURCE, string.Format("Erro ao validar código RECAPTCHA. Erro de comunicação com o servidor.: {0} secret={1}&response={2}", resposta, reCAPTCHAKEY, response), EventLogEntryType.Information, 1);
                        }
                        catch (Exception)
                        {

                        }

                        throw new GS1TradeException("Erro ao validar código RECAPTCHA. Erro de comunicação com o servidor.", ex);
                    }
                }
                else
                {
                    throw new GS1TradeException("Erro ao validar código RECAPTCHA.");
                }
            }
            else
            {
                throw new GS1TradeException("Erro ao validar código RECAPTCHA.");
            }
        }

        [GS1CNPAttribute(false)]
        public string RetornaChavePublicaRECAPTHA(dynamic parametro)
        {
            try
            {
                return ConfigurationManager.AppSettings["reCAPTCHAPUBLICKEY"];
            }
            catch (Exception)
            {
                return null;
            }
        }

        [GS1CNPAttribute(false)]
        public string VerificaUsuarioLogado(dynamic parametro)
        {
            JavaScriptSerializer oJS = new JavaScriptSerializer();
            return oJS.Serialize(this.getSession("USUARIO_LOGADO") != null);
        }

        public static bool ValidaCPF(string cpf)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["URLXDATACPF"];
                string usuario = ConfigurationManager.AppSettings["USERXDATA"];
                string senha = ConfigurationManager.AppSettings["PWDXDATA"];

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "/pf?format=json&cpf=" + cpf);
                request.MaximumAutomaticRedirections = 4;
                request.MaximumResponseHeadersLength = 4;
                request.Credentials = new NetworkCredential(usuario, senha);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer oJS = new JavaScriptSerializer();
                    apiXDATA.i2cReceitaFederalSituacaoCadastralPFResponse oRootObject = new apiXDATA.i2cReceitaFederalSituacaoCadastralPFResponse();
                    var objText = reader.ReadToEnd();
                    oRootObject = oJS.Deserialize<apiXDATA.i2cReceitaFederalSituacaoCadastralPFResponse>(objText);

                    //if (oRootObject.Response.Output != null)
                    //    return true;
                    if (oRootObject.Response.Output.Situacao == "REGULAR")
                        return true;

                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new GS1TradeException("Erro ao tentar validar o documento informado.", ex);
            }
        }

        public static bool ValidaCNPJ(string cnpj)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["URLXDATACNPJ"];
                string usuario = ConfigurationManager.AppSettings["USERXDATA"];
                string senha = ConfigurationManager.AppSettings["PWDXDATA"];

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "/pj?format=json&cnpj=" + cnpj);
                request.MaximumAutomaticRedirections = 4;
                request.MaximumResponseHeadersLength = 4;
                request.Credentials = new NetworkCredential(usuario, senha);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer oJS = new JavaScriptSerializer();
                    apiXDATA.i2cReceitaFederalSituacaoCadastralPJResponse oRootObject = new apiXDATA.i2cReceitaFederalSituacaoCadastralPJResponse();
                    var objText = reader.ReadToEnd();
                    oRootObject = oJS.Deserialize<apiXDATA.i2cReceitaFederalSituacaoCadastralPJResponse>(objText);

                    if (oRootObject.Response.Output != null)
                        return true;
                    //if (oRootObject.Response.Output.Situacao == "REGULAR")
                    //    return true;

                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new GS1TradeException("Erro ao tentar validar o documento informado.", ex);
            }
        }

        [GS1CNPAttribute(false, true)]
        public void Logout(dynamic parametro)
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
        }

        [GS1CNPAttribute(false)]
        public string VerificaPermissoes(dynamic parametro)
        {

            JavaScriptSerializer oJS = new JavaScriptSerializer();

            try
            {
                if (parametro != null && parametro.campos != null)
                {
                    if (!string.IsNullOrEmpty(parametro.campos.nomeBO.ToString()) && !string.IsNullOrEmpty(parametro.campos.nomeMethod.ToString()))
                    {
                        string BO_NAMESPACE = "GS1.CNP.BLL";

                        List<string> permissoes = HttpContext.Current.Session["PERMISSOES_USUARIO_LOGADO"] as List<string>;

                        Type BOClassType = null;
                        MethodInfo method = null;
                        Assembly assembly = Assembly.Load(BO_NAMESPACE);

                        BOClassType = assembly.GetType(String.Format("{0}.{1}", BO_NAMESPACE, parametro.campos.nomeBO.ToString()));

                        method = BOClassType.GetMethod(parametro.campos.nomeMethod.ToString());

                        if (method != null)
                        {
                            GS1CNPAttribute attrBO = BOClassType.GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute;
                            GS1CNPAttribute attrMethod = method.GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute;

                            //if (permissoes == null || permissoes.Count == 0 || !parametro.campos.nomeBO.ToString().Equals(attrBO.ChavePermissao) ||
                            //    !permissoes.Contains(attrMethod.ChavePermissao))
                            if (!attrMethod.GS1CNPValidaPermissao(permissoes))
                                return oJS.Serialize(false);
                            else
                                return oJS.Serialize(true);
                        }
                        else
                            return oJS.Serialize(false);
                    }
                    else
                        return oJS.Serialize(false);
                }
                else
                    return oJS.Serialize(false);
            }
            catch (Exception e)
            {
                return oJS.Serialize(false);
            }
        }

        #region WebService ProdutoServices
        public dynamic ValidarUsuario(dynamic parametro)
        {
            string sql = @"select u.Email, 
	                        u.Senha,
	                        u.CodigoStatusUsuario as status,
	                        u.CodigoPerfil as perfil	   
                        from usuario as u WITH (NOLOCK) 
                        WHERE u.Email = @email
                        AND u.Senha = @password";

            if (parametro != null)
            {
                parametro.password = Crypto.GeraSHA1(parametro.password);

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic usuario = db.Select(sql, parametro);

                    return usuario;
                }
            }
            else
            {
                throw new GS1TradeException("Parametros incorretos");
            }
        }
        #endregion

        [GS1CNPAttribute(false)]
        public string BuscarFAQs(dynamic parametro)
        {
            string sql = @"SELECT CODIGO, NOME AS PERGUNTA, DESCRICAO AS RESPOSTA, URL AS VIDEO
                            FROM FAQ  (NOLOCK) 
                            WHERE CODIGOSTATUSPUBLICACAO = 1
                            AND CODIGOIDIOMA = @codigoidioma
                            AND (CODIGOSTATUSVISUALIZACAOFAQ = 2 OR CODIGOSTATUSVISUALIZACAOFAQ = 3)
                            ORDER BY ORDEM ASC, DATACADASTRO DESC";

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.campos != null)
                {
                    parametro.campos.codigoidioma = parametro.campos.codigoidioma == null ? 1 : parametro.campos.codigoidioma;
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                }

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public object VerificarPesquisaSatisfacaoDisponivel(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"SELECT PS.CODIGO, PS.NOME, PS.DESCRICAO, PS.URL, PS.INICIO, PS.FIM, PSU.CODIGO AS CODIGOPESQUISASATISFACAOUSUARIO, 
	                                PA.NOME AS NOMEPUBLICOALVO 
	                                FROM PESQUISASATISFACAO PS  (NOLOCK) 
	                                LEFT JOIN PESQUISASATISFACAOUSUARIO PSU (NOLOCK)  ON PSU.CODIGOPESQUISASATISFACAO = PS.CODIGO
	                                INNER JOIN PUBLICOALVO PA  (NOLOCK) ON PA.CODIGO = PS.CODIGOPUBLICOALVO
	                                WHERE GETDATE() >= CONVERT(VARCHAR(8),PS.INICIO, 112) + ' 00:00' AND GETDATE() < CONVERT(VARCHAR(8),PS.FIM ,112) + ' 23:59'
	                                AND PSU.CODIGO IS NULL
	                                AND (
		                                CASE (SELECT INDICADORPRINCIPAL FROM ASSOCIADOUSUARIO WHERE CODIGOUSUARIO = " + user.id.ToString() + @") 
			                                WHEN 1 THEN 1
			                                WHEN 0 THEN 2
			                                ELSE 2
		                                END
	                                ) = 1";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return null;
            }
        }

        [GS1CNPAttribute(false)]
        public object ConsultarUltimoAcesso(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"SELECT TOP(1) CONVERT(VARCHAR(10),CONVERT(DATETIME, DATAULTIMOACESSO,106),103) + ' '  + CONVERT(VARCHAR(8), CONVERT(DATETIME, DATAULTIMOACESSO,113), 14) AS 'ACESSO'
                                 FROM USUARIOHISTORICO (NOLOCK) 
                                 WHERE CODIGO = " + user.id.ToString() +
                                 @" AND DATAULTIMOACESSO IS NOT NULL
                                    ORDER BY DATAULTIMOACESSO DESC";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return null;
            }
        }

        private bool ExisteBoletos(long codigo)
        {
            if (codigo > 0)
            {
                string cad = string.Empty;
                List<Associado> associados = null;
                string sql = string.Empty;
                sql = @"SELECT A.CODIGO, A.NOME, A.CPFCNPJ, A.CAD, A.INDICADORCNAERESTRITIVO, A.ATIVADO, A.APROVACAO
					    FROM ASSOCIADO A INNER JOIN ASSOCIADOUSUARIO B ON B.CODIGOASSOCIADO = A.CODIGO
					    WHERE B.CODIGOUSUARIO = @usuarioId AND A.CodigoStatusAssociado in (1, 5)";

                dynamic param = new ExpandoObject();
                param.usuarioId = codigo;

                Func<IDataRecord, Associado> binder = r => new Associado()
                {
                    codigo = r.GetInt64(0),
                    nome = r.GetString(1),
                    cpfcnpj = r.GetString(2),
                    cad = r.GetString(3),
                    indicadorcnaerestritivo = (r.IsDBNull(4) ? string.Empty : r.GetString(4)),
                    ativado = r.GetBoolean(5),
                    aprovacao = r.GetBoolean(6)
                };

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    associados = db.Select(sql, param, binder);
                }

                // Buscar CAD
                if (associados != null && associados.Count > 1)
                    cad = associados.Select(x => x.cad).Aggregate((current, next) => "'" + current + "','" + next + "'");
                else if (associados.Count == 1)
                    cad = "'" + associados.FirstOrDefault().cad + "'";
                else
                    cad = "''";

                using (DAL.Database db = new DAL.Database("BDBASEUNICACRM"))
                {
                    db.Open();

                    sql = @"SELECT COUNT(0) AS CONTADOR
                                   FROM SMART_TITULO A (NOLOCK)
                                   INNER JOIN ACCOUNT B (NOLOCK) ON SMART_EMPRESA = ACCOUNTID
                                   WHERE SMART_E1_STATUS = 'A'
                                   AND SMART_E1_VENCORI < GETDATE()
                                   AND SMART_E1_PREFIXO IN ('REG', 'MAN')
                                   AND SMART_NUMTITULOBCO LIKE '112%'
                                   AND B.ACCOUNTNUMBER IN (" + cad + ")";

                    List<dynamic> retorno = db.Select(sql, null);

                    dynamic resultado = retorno.First();

                    return resultado != null && Convert.ToInt32(resultado.contador) > 0;
                }
            }
            else
            {
                return false;
            }
        }

        private List<Associado> BuscarAssociadosUsuario(long codigo)
        {
            try
            {
                string sql = string.Empty;
                sql = @"SELECT A.CODIGO, A.NOME, A.CPFCNPJ, A.CAD, A.INDICADORCNAERESTRITIVO, A.ATIVADO, A.APROVACAO, P.CodigoPerfilGepir
					    FROM ASSOCIADO A INNER JOIN ASSOCIADOUSUARIO B ON B.CODIGOASSOCIADO = A.CODIGO
                        LEFT JOIN ASSOCIADOPERFILGEPIR P ON P.CodigoAssociado = A.Codigo
					    WHERE B.CODIGOUSUARIO = @usuarioId AND A.CodigoStatusAssociado = 1";

                dynamic param = new ExpandoObject();
                param.usuarioId = codigo;

                Func<IDataRecord, Associado> binder = r => new Associado()
                {
                    codigo = r.GetInt64(0),
                    nome = r.GetString(1),
                    cpfcnpj = r.GetString(2),
                    cad = r.GetString(3),
                    indicadorcnaerestritivo = (r.IsDBNull(4) ? string.Empty : r.GetString(4)),
                    ativado = r.GetBoolean(5),
                    aprovacao = r.GetBoolean(6),
                    codigoperfilgepir = r.GetInt32(7)
                };

                List<Associado> result;

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    result = db.Select(sql, param, binder);
                }

                return result;
            }
            catch
            {
                throw new Exception("Erro ao Buscar Associados do Usuário.");
            }
        }
    }
}
