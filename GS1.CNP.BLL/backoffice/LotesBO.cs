﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Web;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("LotesBO")]
    public class LotesBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "LOTE"; }
        }

        public override string nomePK
        {
            get { return "codigo"; }
        }

        [GS1CNPAttribute(false)]
        public object BuscarLotes(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT L.CODIGO, L.CODIGOSTATUSLOTE AS STATUSLOTE, L.NRLOTE, CONVERT(VARCHAR(10),CONVERT(DATE,L.DATAPROCESSAMENTO,106),103) + ' ' + SUBSTRING( convert(varchar, DATAPROCESSAMENTO,108),1,5) AS DATAPROCESSAMENTO, 
	                                CONVERT(VARCHAR(10),CONVERT(DATE,L.DATAVENCIMENTO,106),103) + ' ' + SUBSTRING( convert(varchar, DATAVENCIMENTO,108),1,5)  AS DATAVENCIMENTO, L.TIPOPROCESSAMENTOBIZSTEP, L.STATUSPRODUTODISPOSITIONS, L.INFORMACOESADICIONAIS,
	                                P.CODIGOPRODUTO AS CODIGOPRODUTO, P.INFORMATIONPROVIDER AS GLN, P.PRODUCTDESCRIPTION, P.GLOBALTRADEITEMNUMBER, U.NOME AS NOMEUSUARIO, B.NOME AS BIZSTEP, D.NOME AS DISPOSITIONS, 
                                    B.CODIGO AS CODIGOBIZSTEP, D.CODIGO AS CODIGODISPOSITIONS, P.NRPREFIXO, P.CODITEM, P.VARIANTELOGISTICA, L.QUANTIDADEITENS 
                                    ,CONVERT(VARCHAR(10),CONVERT(DATE, L.DATAALTERACAO,106),103) as DATAALTERACAO, CONVERT(VARCHAR(10),CONVERT(DATE, COALESCE((SELECT MIN(X.DATAALTERACAO) FROM LOTEHISTORICO X WHERE X.CodigoLote = L.CODIGO),L.DATAALTERACAO),106),103) AS DATACADASTRO 
                                    ,SUBSTRING( convert(varchar, DATAPROCESSAMENTO,108),1,5) HORAPROCESSAMENTO
									,SUBSTRING( convert(varchar, DATAVENCIMENTO,108),1,5) HORAVENCIMENTO
                                FROM LOTE L WITH (NOLOCK)
                                LEFT JOIN PRODUTO P WITH (NOLOCK) ON P.CODIGOPRODUTO = L.CODIGOPRODUTO
                                INNER JOIN USUARIO U WITH (NOLOCK) ON U.CODIGO = L.CODIGOUSUARIOALTERACAO
                                INNER JOIN BIZSTEP B WITH (NOLOCK) ON B.CODIGO = L.CODIGOBIZSTEP
                                INNER JOIN DISPOSITIONS D WITH (NOLOCK) ON D.CODIGO = L.CODIGODISPOSITIONS 
                                WHERE L.CODIGOASSOCIADO = " + associado.codigo.ToString();

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutosAutoComplete(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, PRODUCTDESCRIPTION 
                                FROM PRODUTO P WITH (NOLOCK)
                                WHERE (P.GLOBALTRADEITEMNUMBER LIKE '%' + @nome + '%' 
                                OR P.PRODUCTDESCRIPTION LIKE '%' + @nome + '%')
                                AND P.GLOBALTRADEITEMNUMBER IS NOT NULL 
                                AND P.CODIGOASSOCIADO = " + associado.codigo.ToString();


                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    List<dynamic> produtos = db.Select(sql, param);

                    return JsonConvert.SerializeObject(produtos);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisaProdutos(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT P.CODIGOPRODUTO
                                    , P.PRODUCTDESCRIPTION
                                    , P.GLOBALTRADEITEMNUMBER
                                    , TG.NOME AS NOMETIPOGTIN
                                    , P.NRPREFIXO 
                                    , P.CODITEM 
                                    , P.INFORMATIONPROVIDER AS GLN
                                    , P.VARIANTELOGISTICA 
                                FROM PRODUTO P WITH (NOLOCK)
                                    INNER JOIN TIPOGTIN TG WITH (NOLOCK) ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                WHERE P.GLOBALTRADEITEMNUMBER IS NOT NULL 
                                    AND P.CODIGOASSOCIADO = " + associado.codigo.ToString() + @"
                                    AND cast(P.globalTradeItemNumber as varchar(max)) like ('%' + ISNULL(@gtin, '') + '%')
                                    AND P.PRODUCTDESCRIPTION LIKE ('%' + ISNULL(@descricao, '') + '%')
                                    AND (P.CODIGOSTATUSGTIN = @codigostatusgtin OR @codigostatusgtin IS NULL OR @codigostatusgtin = '')
                                    AND (P.CODIGOTIPOGTIN = @codigotipogtin OR @codigotipogtin IS NULL OR @codigotipogtin = '')
                                    AND P.CODIGOSTATUSGTIN IN (1,3,6)
                                    AND P.INFORMATIONPROVIDER IS NOT NULL ";

                if(parametro != null && parametro.campos != null) {

                    if (parametro.campos.gtin != null && parametro.campos.gtin != "")
                    {
                        sql += " AND P.GLOBALTRADEITEMNUMBER = @gtin";
                    }

                    if (parametro.campos.descricao != null && parametro.campos.descricao != "")
                    {
                        sql += " AND P.PRODUCTDESCRIPTION LIKE '%' + @descricao + '%'";
                    }

                    if (parametro.campos.codigostatusgtin != null && parametro.campos.codigostatusgtin.ToString() != "")
                    {
                        sql += " AND P.CODIGOSTATUSGTIN = @codigostatusgtin";
                    }

                    if (parametro.campos.codigotipogtin != null && parametro.campos.codigotipogtin.ToString() != "")
                    {
                        sql += " AND P.CODIGOTIPOGTIN = @codigotipogtin";
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public void VerificaPermissaoLotes(dynamic parametro)
        {
            List<string> permissoes = HttpContext.Current.Session["PERMISSOES_USUARIO_LOGADO"] as List<string>;

            if (parametro != null && parametro.campos != null && parametro.campos.permissao != null && !permissoes.Contains(parametro.campos.permissao.ToString()))
            {
                if (parametro.campos.permissao == "PublicarLotes")
                {
                    throw new GS1TradeException("Usuário sem permissão para publicar lotes.");
                }
            }
        }

        [GS1CNPAttribute(false)]
        public object InsereLotes(dynamic parametro)
        {
            dynamic param = null;

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");
            Newtonsoft.Json.Linq.JArray urls = new Newtonsoft.Json.Linq.JArray();
            List<string> permissoes = HttpContext.Current.Session["PERMISSOES_USUARIO_LOGADO"] as List<string>;

            if (parametro != null && parametro.campos != null && parametro.campos.permissao != null && !permissoes.Contains(parametro.campos.permissao.ToString()))
            {
                if (parametro.campos.permissao == "CadastrarLotes")
                {
                    throw new GS1TradeException("Sem permissão para acessar esta funcionalidade.");
                }
            }

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();
                    parametro.campos.codigoassociado = associado.codigo.ToString();

                    if(parametro != null && parametro.campos != null && parametro.campos.nrlote == null) parametro.campos.nrlote = "";

                    //Verifica se o Lote já foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM LOTE L WHERE L.NRLOTE = @nrlote AND L.NRLOTE IS NOT NULL AND L.CODIGOASSOCIADO = @codigoassociado";

                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {

                        try
                        {
                            parametro.campos.codigousuarioalteracao = user.id.ToString();
                            parametro.campos.dataalteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                            parametro.campos.tipoprocessamentobizstep = geraBizStep(parametro.campos.bizstep.Value);
                            parametro.campos.statusprodutodispositions = geraDisposition(parametro.campos.disposition.Value);  

                            if (parametro.campos.urls != null && parametro.campos.urls.ToString() != "") urls = parametro.campos.urls;

                            //Converte campos data
                            if (parametro.campos.datavencimento != null)
                                parametro.campos.datavencimento = parametro.campos.datavencimento.ToString("MM/dd/yyyy HH:mm:ss.fff");
                            if (parametro.campos.datacertificado != null)
                                parametro.campos.datacertificado = parametro.campos.datacertificado.ToString("MM/dd/yyyy HH:mm:ss.fff");

                            string sql = String.Empty;

                            //Itens que não podem ser inseridos
                            parametro.campos.Remove("disposition");
                            parametro.campos.Remove("bizstep");
                            parametro.campos.Remove("urls");

                            if (parametro.campos != null)
                                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                            int idInserido = 0;

                            if ((parametro.campos.permissao == "PublicarLotes") && parametro.campos.codigostatuslote == 1)
                            {
                                ((IDictionary<String, Object>)param).Remove("permissao");
                                param.codigostatuslote = 2;
                                idInserido = db.Insert(nomeTabela, nomePK, param, true);
                            }
                            else if ((parametro.campos.permissao == "CadastrarLotes") && parametro.campos.codigostatuslote == 1)
                            {
                                ((IDictionary<String, Object>)param).Remove("permissao");
                                idInserido = db.Insert(nomeTabela, nomePK, param, true);
                            }

                            //Inserção de URLs
                            if (urls.Count > 0)
                            {
                                string sqlUrls = InserirUrls(urls, idInserido.ToString());
                                if (sqlUrls != null && sqlUrls != "")
                                {
                                    object retornoUrls = db.Select(sqlUrls, null);
                                }
                            }

                            db.Commit();
                            return JsonConvert.SerializeObject(idInserido);
                        }
                        catch (GS1TradeException e)
                        {
                            db.Rollback();
                            throw new GS1TradeException(e.Message.ToString());
                        }
                    }
                    else
                    {
                        throw new GS1TradeException("O lote informado já está cadastrado.");
                    }
                }
            }
            else
            {
                return "Não foi possível buscar os dados do associado.";
            }
        }

        [GS1CNPAttribute(true, true, "EditarLotes", "VisualizarLotes")]
        public void EditarLotes(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");
            Newtonsoft.Json.Linq.JArray urls = new Newtonsoft.Json.Linq.JArray();
            List<string> permissoes = HttpContext.Current.Session["PERMISSOES_USUARIO_LOGADO"] as List<string>;

            if (parametro != null && parametro.campos != null && parametro.campos.permissao != null && !permissoes.Contains(parametro.campos.permissao.ToString()))
            {
                if (parametro.campos.permissao == "EditarLotes")
                {
                    throw new GS1TradeException("Sem permissão para acessar esta funcionalidade.");
                }
            }

            if (user != null && associado != null)
            {

                using (Database db = new Database("BD"))
                {

                    //Só permite edição de lote que ainda não foi publicado
                    if (parametro != null && parametro.campos != null)
                    {
                        db.Open(true);
                        db.BeginTransaction();

                        //Inserção na tabela LOTESHISTORICO
                        string sql_historico = @"INSERT INTO LOTEHISTORICO (CODIGOLOTE, CODIGOSTATUSLOTE, CODIGOPRODUTO, NRLOTE, DATAPROCESSAMENTO, DATAVENCIMENTO, TIPOPROCESSAMENTOBIZSTEP, STATUSPRODUTODISPOSITIONS, INFORMACOESADICIONAIS,
                                                        DATAALTERACAO, CODIGOUSUARIOALTERACAO, CODIGOASSOCIADO, CODIGOBIZSTEP, CODIGODISPOSITIONS, DATAHISTORICO, QUANTIDADEITENS)
	                                            SELECT CODIGO, CODIGOSTATUSLOTE, CODIGOPRODUTO, NRLOTE, DATAPROCESSAMENTO, DATAVENCIMENTO, TIPOPROCESSAMENTOBIZSTEP, STATUSPRODUTODISPOSITIONS, INFORMACOESADICIONAIS,
                                                        DATAALTERACAO, CODIGOUSUARIOALTERACAO, CODIGOASSOCIADO, CODIGOBIZSTEP, CODIGODISPOSITIONS, GETDATE(), QUANTIDADEITENS
				                                        FROM LOTE WITH (NOLOCK)
				                                        WHERE LOTE.CODIGO = @codigo";

                        dynamic param1 = null;

                        if (parametro.campos != null)
                            param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        db.Select(sql_historico, param1);

                        //Verifica se o LOTE já foi cadastrado
                        string check = "SELECT COUNT(1) AS QUANTIDADE FROM LOTE L WHERE L.NRLOTE = @nrlote AND L.CODIGOASSOCIADO = " + associado.codigo + " AND L.CODIGO <> @codigo";
                        if (parametro != null && parametro.campos != null && parametro.campos.nrlote == null) parametro.campos.nrlote = "";
                        parametro.campos.codigoassociado = associado.codigo.ToString();
                        dynamic paramCheck = null, param = null;

                        if (parametro.campos != null)
                            paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                        if (resultCheck.quantidade == 0)
                        {

                            try
                            {
                                //Converte campos data
                                if (parametro.campos.datavencimento != null)
                                    parametro.campos.datavencimento = parametro.campos.datavencimento.ToString("MM/dd/yyyy HH:mm:ss.fff");
                                if (parametro.campos.datacertificado != null)
                                    parametro.campos.datacertificado = parametro.campos.datacertificado.ToString("MM/dd/yyyy HH:mm:ss.fff");

                                if (parametro.campos.urls != null && parametro.campos.urls.ToString() != "") urls = parametro.campos.urls;
                                parametro.campos.tipoprocessamentobizstep = geraBizStep(parametro.campos.bizstep.Value);
                                parametro.campos.statusprodutodispositions = geraDisposition(parametro.campos.disposition.Value);

                                //Inserção de URLs
                                if (urls.Count > 0)
                                {
                                    string sqlUrls = InserirUrls(urls, parametro.campos.codigo.ToString());
                                    if (sqlUrls != null && sqlUrls != "")
                                    {
                                        object retornoUrls = db.Select(sqlUrls, null);
                                    }
                                }

                                //Itens que não podem ser inseridos
                                parametro.campos.Remove("disposition");
                                parametro.campos.Remove("bizstep");
                                parametro.campos.Remove("urls");

                                parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                                parametro.campos.codigousuarioalteracao = user.id.ToString();
                                parametro.campos.codigoassociado = associado.codigo.ToString();

                                if (parametro.campos != null)
                                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                                //Não permite fazer nenhuma alteração se o Lote já estiver publicado
                                if (!(parametro.campos.permissao == "PublicarLotes" || parametro.campos.permissao == "EditarLotes") && parametro.campos.codigostatuslote == 2)
                                {
                                    ((IDictionary<String, Object>)param).Remove("permissao");
                                    db.Update(nomeTabela, nomePK, param);
                                }
                                else if ((parametro.campos.permissao == "PublicarLotes") && parametro.campos.codigostatuslote == 1)
                                {
                                    ((IDictionary<String, Object>)param).Remove("permissao");
                                    param.codigostatuslote = 2;
                                    db.Update(nomeTabela, nomePK, param);
                                }
                                else if ((parametro.campos.permissao == "EditarLotes") && parametro.campos.codigostatuslote == 1)
                                {
                                    ((IDictionary<String, Object>)param).Remove("permissao");
                                    db.Update(nomeTabela, nomePK, param);
                                } 

                                db.Commit();
                            }
                            catch (Exception e)
                            {
                                db.Rollback();
                                throw new GS1TradeException("Não foi possível cadastrar o Lote informado.", e);
                            }

                        }
                        else
                        {
                            db.Rollback();
                            throw new GS1TradeException("O lote informado já está cadastrado.");
                        }
                    }

                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a atualização.");
            }

        }

        [GS1CNPAttribute(false)]
        public object PesquisarLotes(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT L.CODIGO, L.CODIGOSTATUSLOTE AS STATUSLOTE, L.NRLOTE, CONVERT(VARCHAR(10),CONVERT(DATE,L.DATAPROCESSAMENTO,106),103) + ' ' + SUBSTRING( convert(varchar, DATAPROCESSAMENTO,108),1,5) AS DATAPROCESSAMENTO, 
	                                CONVERT(VARCHAR(10),CONVERT(DATE,L.DATAVENCIMENTO,106),103) + ' ' + SUBSTRING( convert(varchar, DATAVENCIMENTO,108),1,5) AS DATAVENCIMENTO, L.TIPOPROCESSAMENTOBIZSTEP, L.STATUSPRODUTODISPOSITIONS, L.INFORMACOESADICIONAIS,
	                                P.CODIGOPRODUTO AS CODIGOPRODUTO, P.INFORMATIONPROVIDER AS GLN, P.PRODUCTDESCRIPTION, P.GLOBALTRADEITEMNUMBER, U.NOME AS NOMEUSUARIO, B.NOME AS BIZSTEP, D.NOME AS DISPOSITIONS,
                                    B.CODIGO AS CODIGOBIZSTEP, D.CODIGO AS CODIGODISPOSITIONS, P.NRPREFIXO, P.CODITEM, P.VARIANTELOGISTICA, L.QUANTIDADEITENS
                                    ,CONVERT(VARCHAR(10),CONVERT(DATE, L.DATAALTERACAO,106),103) as DATAALTERACAO, CONVERT(VARCHAR(10),CONVERT(DATE, COALESCE((SELECT MIN(X.DATAALTERACAO) FROM LOTEHISTORICO X WHERE X.CodigoLote = L.CODIGO),L.DATAALTERACAO),106),103) AS DATACADASTRO 
                                    ,SUBSTRING( convert(varchar, DATAPROCESSAMENTO,108),1,5) HORAPROCESSAMENTO
									,SUBSTRING( convert(varchar, DATAVENCIMENTO,108),1,5) HORAVENCIMENTO
                                FROM LOTE L WITH (NOLOCK)
                                INNER JOIN PRODUTO P WITH (NOLOCK) ON P.CODIGOPRODUTO = L.CODIGOPRODUTO 
                                INNER JOIN USUARIO U WITH (NOLOCK) ON U.CODIGO = L.CODIGOUSUARIOALTERACAO 
                                INNER JOIN BIZSTEP B WITH (NOLOCK) ON B.CODIGO = L.CODIGOBIZSTEP
                                INNER JOIN DISPOSITIONS D WITH (NOLOCK) ON D.CODIGO = L.CODIGODISPOSITIONS 
                                WHERE L.CODIGOASSOCIADO = " + associado.codigo.ToString();

                if (parametro != null && parametro.campos != null)
                {

                    if ((parametro.campos.de_dataprocessamento != null && parametro.campos.de_dataprocessamento != "") || (parametro.campos.ate_dataprocessamento != null && parametro.campos.ate_dataprocessamento != ""))
                    {

                        if((parametro.campos.de_dataprocessamento != null && parametro.campos.de_dataprocessamento != "") && ((parametro.campos.ate_dataprocessamento == null || parametro.campos.ate_dataprocessamento == ""))) {
                            parametro.campos.de_dataprocessamento = DateTime.ParseExact(Convert.ToString(parametro.campos.de_dataprocessamento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            sql += @" AND CONVERT(DATETIME,CONVERT(VARCHAR(10),L.DATAPROCESSAMENTO,101) + ' ' + CONVERT(VARCHAR(10),L.DATAPROCESSAMENTO,108)) >= CONVERT(DATETIME,CONVERT(VARCHAR(10),@de_dataprocessamento + ' ' + @de_horaprocessamento,101)) ";
                        }
                        else if((parametro.campos.ate_dataprocessamento != null && parametro.campos.ate_dataprocessamento != "") && ((parametro.campos.de_dataprocessamento == null || parametro.campos.de_dataprocessamento == ""))) {
                            parametro.campos.ate_dataprocessamento = DateTime.ParseExact(Convert.ToString(parametro.campos.ate_dataprocessamento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            sql += @" AND CONVERT(DATETIME,CONVERT(VARCHAR(10),L.DATAPROCESSAMENTO,101) + ' ' + CONVERT(VARCHAR(10),L.DATAPROCESSAMENTO,108)) <= CONVERT(DATETIME,CONVERT(VARCHAR(10),@ate_dataprocessamento + ' ' + @ate_horaprocessamento,101)) ";
                        }
                        else {
                            parametro.campos.de_dataprocessamento = DateTime.ParseExact(Convert.ToString(parametro.campos.de_dataprocessamento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            parametro.campos.ate_dataprocessamento = DateTime.ParseExact(Convert.ToString(parametro.campos.ate_dataprocessamento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);


                            if (parametro.campos.de_dataprocessamento != null && parametro.campos.de_dataprocessamento != "" && parametro.campos.de_horaprocessamento != null && parametro.campos.de_horaprocessamento != "")
                            {
                                parametro.campos.de_dataprocessamento += " " + parametro.campos.de_horaprocessamento;
                            }

                            //if (!string.IsNullOrEmpty(parametro.campos.ate_dataprocessamento.ToString()) && !string.IsNullOrEmpty(parametro.campos.ate_horaprocessamento.ToString()))
                            if(parametro.campos.ate_dataprocessamento != null && parametro.campos.ate_dataprocessamento != "" && parametro.campos.ate_horaprocessamento != null && parametro.campos.ate_horaprocessamento != "")
                            {
                                parametro.campos.ate_dataprocessamento += " " + parametro.campos.ate_horaprocessamento;
                            }
                            //else if (!string.IsNullOrEmpty(parametro.campos.ate_dataprocessamento.ToString()))
                            else if (parametro.campos.ate_dataprocessamento != null && parametro.campos.ate_dataprocessamento != "")
                            {
                                parametro.campos.ate_dataprocessamento += " 23:59:59";
                            }

                            sql += @" AND CONVERT(DATETIME,CONVERT(VARCHAR(10),L.DATAPROCESSAMENTO,101) + ' ' + CONVERT(VARCHAR(10),L.DATAPROCESSAMENTO,108)) BETWEEN CONVERT(DATETIME,CONVERT(VARCHAR(10),@de_dataprocessamento,101) + SUBSTRING(@de_dataprocessamento,11,6)) AND CONVERT(DATETIME,CONVERT(VARCHAR(10),@ate_dataprocessamento,101) + SUBSTRING(@ate_dataprocessamento,11,6)) ";
                        }
                    }

                    if ((parametro.campos.de_datavencimento != null && parametro.campos.de_datavencimento != "") || (parametro.campos.ate_datavencimento != null && parametro.campos.ate_datavencimento != ""))
                    {

                        if ((parametro.campos.de_datavencimento != null && parametro.campos.de_datavencimento != "") && ((parametro.campos.ate_datavencimento == null || parametro.campos.ate_datavencimento == "")))
                        {
                            parametro.campos.de_datavencimento = DateTime.ParseExact(Convert.ToString(parametro.campos.de_datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);                            
                            sql += @" AND CONVERT(DATETIME,CONVERT(VARCHAR(10),L.DATAVENCIMENTO,101) + ' ' + CONVERT(VARCHAR(10),L.DATAVENCIMENTO,108)) >= CONVERT(DATETIME,CONVERT(VARCHAR(10),@de_datavencimento + ' ' + @de_horavencimento,101)) ";
                        }
                        else if ((parametro.campos.ate_datavencimento != null && parametro.campos.ate_datavencimento != "") && ((parametro.campos.de_datavencimento == null || parametro.campos.de_datavencimento == "")))
                        {
                            parametro.campos.ate_datavencimento = DateTime.ParseExact(Convert.ToString(parametro.campos.ate_datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            sql += @" AND CONVERT(DATETIME,CONVERT(VARCHAR(10),L.DATAVENCIMENTO,101) + ' ' + CONVERT(VARCHAR(10),L.DATAVENCIMENTO,108)) <= CONVERT(DATETIME,CONVERT(VARCHAR(10),@ate_datavencimento + ' ' + @ate_horavencimento,101)) ";
                        }
                        else
                        {
                            parametro.campos.de_datavencimento = DateTime.ParseExact(Convert.ToString(parametro.campos.de_datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            parametro.campos.ate_datavencimento = DateTime.ParseExact(Convert.ToString(parametro.campos.ate_datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            //if (!string.IsNullOrEmpty(parametro.campos.de_datavencimento.ToString()) && !string.IsNullOrEmpty(parametro.campos.de_horavencimento.ToString()))
                            if (parametro.campos.de_datavencimento != null && parametro.campos.de_datavencimento != "" && parametro.campos.de_horavencimento != null && parametro.campos.de_horavencimento != "")
                            {
                                parametro.campos.de_datavencimento += " " + parametro.campos.de_horavencimento;
                            }


                            //if (!string.IsNullOrEmpty(parametro.campos.ate_datavencimento.ToString()) && !string.IsNullOrEmpty(parametro.campos.ate_horavencimento.ToString()))
                            if (parametro.campos.ate_datavencimento != null && parametro.campos.ate_datavencimento != "" && parametro.campos.ate_horavencimento != null && parametro.campos.ate_horavencimento != "")
                            {
                                parametro.campos.ate_datavencimento += " " + parametro.campos.ate_horavencimento;
                            }
                            //else if (!string.IsNullOrEmpty(parametro.campos.ate_datavencimento.ToString()))
                            else if (parametro.campos.ate_datavencimento != null && parametro.campos.ate_datavencimento != "")
                            {
                                parametro.campos.ate_datavencimento += " 23:59:59";
                            }

                            sql += @" AND CONVERT(DATETIME,CONVERT(VARCHAR(10),L.DATAVENCIMENTO,101) + ' ' + CONVERT(VARCHAR(10),L.DATAVENCIMENTO,108)) BETWEEN CONVERT(DATETIME,CONVERT(VARCHAR(10),@de_datavencimento,101) + SUBSTRING(@de_datavencimento,11,6)) AND CONVERT(DATETIME,CONVERT(VARCHAR(10),@ate_datavencimento,101) + SUBSTRING(@ate_datavencimento,11,6)) ";
                            
                        }
                    }

                    if (parametro.campos.codigostatuslote != null)
                    {
                        sql += " AND L.CODIGOSTATUSLOTE = @codigostatuslote";
                    }

                    if (parametro.campos.produto != null)
                    {
                        sql += " AND P.PRODUCTDESCRIPTION LIKE '%' + @produto + '%'";
                    }

                    if (parametro.campos.gtin != null)
                    {
                        sql += " AND P.GLOBALTRADEITEMNUMBER = @gtin";
                    }

                    if (parametro.campos.codigobizstep != null)
                    {
                        sql += " AND L.CODIGOBIZSTEP = @codigobizstep";
                    }

                    if (parametro.campos.codigodispositions != null)
                    {
                        sql += " AND L.CODIGODISPOSITIONS = @codigodispositions";
                    }

                    if (parametro.campos.lote != null)
                    {
                        sql += " AND L.NRLOTE = @lote";
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false, true)]
        public string InserirUrls(dynamic urls, string codigolote)
        {
            string sql = String.Empty;

            if (codigolote != null)
            {
                if (urls != null)
                {
                    foreach (var item in urls)
                    {
                        if (item.status == "novo" || item.status == "novo-e-editado")
                        {
                            sql += "INSERT INTO LOTEURL (NOME, URL, STATUS, CODIGOLOTE) VALUES ('" + item.nome + "', '" + item.url + "', 1, " + codigolote + "); ";
                        }
                        else if (item.status == "editado")
                        {
                            sql += @"INSERT INTO LOTEURLHISTORICO (CODIGO, NOME, URL, STATUS, CODIGOLOTE, DATAHISTORICO)
	                                                    SELECT CODIGO, NOME, URL, STATUS, CODIGOLOTE, GETDATE()
				                                            FROM LOTEURL WITH (NOLOCK)
				                                            WHERE LOTEURL.CODIGO = " + item.codigo + "; ";

                            sql += "UPDATE LOTEURL SET NOME = '" + item.nome + "', URL = '" + item.url + "' WHERE CODIGO = " + item.codigo + "; ";
                        }
                        else if (item.status == "excluido")
                        {
                            sql += "DELETE FROM LOTEURL WHERE CODIGO = " + item.codigo + "; ";
                        }
                    }
                }
            }

            return sql;
        }

        [GS1CNPAttribute(false)]
        public string BuscarURLsLote(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT LU.CODIGO, LU.NOME, LU.URL
	                FROM LOTEURL LU  (NOLOCK) 
	                WHERE LU.STATUS = 1
	                AND CODIGOLOTE = @codigo";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return "";
        }

        public string geraBizStep(string bizstep)
        {
            if (bizstep != null)
            {
                string stepgerado = String.Empty;
                stepgerado = "<bizStep>urn:epcglobal:cbv:bizstep:" + bizstep + "</bizStep>";
                return stepgerado;
            }
            else
            {
                return null;
            }
        }

        public string geraDisposition(string disposition)
        {
            if (disposition != null)
            {
                string stepgerado = String.Empty;
                stepgerado = "<disposition>urn:epcglobal:cbv:disp:" + disposition + "</disposition>";
                return stepgerado;
            }
            else
            {
                return null;
            }
        }
    }

}
