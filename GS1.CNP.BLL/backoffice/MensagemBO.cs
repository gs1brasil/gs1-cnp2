﻿using System;
using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using System.Dynamic;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("MensagemBO")]
    public class MensagemBO : CrudBO, IDisposable
    {
        public override string nomeTabela
        {
            get { return string.Empty; }
        }

        public override string nomePK
        {
            get { return string.Empty; }
        }

        public void Dispose() { }

        [GS1CNPAttribute(false)]
        public void marcarMensagemLida(dynamic parametro)
        {            
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = new ExpandoObject();                

                param.usuario = user.id.ToString();

                param.codigomensagem = parametro.where.codigo.ToString();

                string sql = "INSERT INTO MENSAGEMLEITURA (CODIGOMENSAGEM, CODIGOUSUARIO,DATALEITURA) VALUES (@codigomensagem, @usuario, getdate())";
                db.Select(sql, param);

            }
        }


        [GS1CNPAttribute(false)]
        public object ConsultarMensagem(dynamic parametro)
        {
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = new ExpandoObject();
                if (associado != null)
                    param.associado = associado.codigo;
                else
                    param.associado = DBNull.Value;

                param.usuario = user.id.ToString();

                param.codigo = parametro.where.codigomensagem.ToString();



                string sql = @"SELECT A.CODIGO
                                ,CONVERT(VARCHAR(10),CONVERT(DATETIME, A.DATASINCRONIZACAO,106),103) + ' '  + CONVERT(VARCHAR(8), CONVERT(DATETIME, A.DATASINCRONIZACAO,113), 14) AS DATASINCRONIZACAO 
                                , A.MENSAGEM, B.DATALEITURA, A.ASSUNTO  
                                FROM MENSAGEM A  (NOLOCK) 
                                LEFT JOIN MENSAGEMLEITURA B  (NOLOCK) ON B.CODIGOMENSAGEM = A.CODIGO AND B.CODIGOUSUARIO = @usuario
                                WHERE STATUS = 1 
                                AND (CODIGOASSOCIADO = @associado or CODIGOASSOCIADO IS NULL)
                                AND A.CODIGO = @codigo";


                List<dynamic> mensagens = db.Select(sql, param);
                return mensagens;
            }

            return null;
        }


    }
}
