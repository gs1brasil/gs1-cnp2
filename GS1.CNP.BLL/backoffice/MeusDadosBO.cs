﻿using System;
using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("MeusDadosBO")]
    class MeusDadosBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "USUARIO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarMeusDados(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"SELECT u.Codigo,
	                            u.Nome as nomeUsuario,
	                            u.Email,
	                            su.Nome as statusUsuario,
							    p.Nome as perfilUsuario,
	                            u.TelefoneCelular,
	                            u.TelefoneResidencial,
	                            u.TelefoneComercial,
	                            u.Ramal,
	                            u.Cargo,
	                            u.Departamento,
	                            u.CPFCNPJ as cpfcnpjUsuario
                            FROM Usuario as u WITH (NOLOCK)
                            INNER JOIN StatusUsuario as su WITH (NOLOCK) on su.Codigo = u.CodigoStatusUsuario
						    INNER JOIN Perfil as p WITH (NOLOCK) on p.Codigo = u.CodigoPerfil
                            WHERE u.Codigo = @codigoUsuario";

                using (Database db = new Database("BD"))
                {
                    parametro.campos.codigoUsuario = user.id.ToString();

                    db.Open();
                    dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
            
        }

        [GS1CNPAttribute(false, true)]
        public object AtualizarMeusDados(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null && user.id == Convert.ToInt32(parametro.campos.codigo))
            {
                using (Database db = new Database("BD"))
                {
                    string sql = @"UPDATE usuario SET 
	                                Nome = @nomeusuario, 
	                                CPFCNPJ = @cpfcnpjusuario, 
	                                Email = @email, 
	                                TelefoneCelular = @telefonecelular, 
	                                TelefoneComercial = @telefonecomercial, 
	                                Ramal = @ramal,
	                                Departamento = @departamento,
	                                Cargo = @cargo
                                WHERE Codigo = @codigo";

                    //Verifica se o CPF já foi cadastrado
                    //string check = "SELECT COUNT(1) AS QUANTIDADE FROM USUARIO U WHERE U.CPFCNPJ = @cpfcnpjusuario AND U.CODIGO <> @codigo";

                    db.Open();
                    //dynamic paramCheck = null;

                    //if (parametro.campos != null)
                    //{
                    //    paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    //}

                    //dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    //if (resultCheck.quantidade == 0)
                    //{
                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        return result;
                    //}
                    //else
                    //{
                    //    throw new GS1TradeException("CPF/CNPJ já cadastrado.");
                    //}
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a atualização.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscaEmpresasAssociadas(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"SELECT a.Codigo,
	                                a.Nome,
	                                a.CPFCNPJ
                                FROM Associado as a WITH (NOLOCK)
                                INNER JOIN AssociadoUsuario as au WITH (NOLOCK) on au.CodigoAssociado = a.Codigo
                                WHERE au.CodigoUsuario = @codigoUsuario";

                using (Database db = new Database("BD"))
                {
                    parametro.campos.codigoUsuario = user.id.ToString();

                    db.Open();
                    dynamic param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return "Não foi possível realizar a consulta.";
            }
        }
    }
}
