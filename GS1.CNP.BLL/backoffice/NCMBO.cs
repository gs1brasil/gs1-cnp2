﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("NCMBO")]
    public class NCMBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "NCM"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string PesquisaNCMAtivos(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NCM, DESCRICAO
                            FROM NCM WITH (NOLOCK) 
                            WHERE STATUS = 1 ";

            if (parametro.campos != null)
            {

                if (parametro.campos.ncm != null && parametro.campos.ncm != "")
                {
                    sql += " AND REPLACE(NCM, '.', '')  = REPLACE(@ncm, '.', '') ";
                }

                if (parametro.campos.descricao != null && parametro.campos.descricao != "")
                {
                    sql += " AND DESCRICAO LIKE '%' + @descricao + '%' ";
                }
            }

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }

        }
    }

}
