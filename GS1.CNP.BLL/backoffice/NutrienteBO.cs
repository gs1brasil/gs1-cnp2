﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("NutrienteBO")]
    public class NutrienteBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "nutrientTypeCodeINFOODS"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarNutrientesAtivos(dynamic parametro)
        {
            string sql = @"SELECT CODIGO, NOME, SIGLA, TIPOUNIDADEMEDIDA
                            FROM NutrientTypeCodeINFOODS WITH (NOLOCK) 
                            WHERE STATUS = 1
                            ORDER BY TIPOUNIDADEMEDIDA ASC, NOME ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

    }

}
