﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("PapelGlnBO")]
    public class PapelGlnBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "PAPELGLN"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarPapeisGLN(dynamic parametro)
        {

            string sql = "SELECT CODIGO, NOME, DESCRICAO, STATUS FROM PAPELGLN WITH (NOLOCK)";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("CadastrarPapelGLN",true,true)]
        public object CadastrarPapelGln(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    //Verifica se o nome do Papel GLN já foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM PAPELGLN P WHERE P.NOME = @nome";

                    db.Open();
                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {

                        string sql = "INSERT INTO PAPELGLN VALUES (@nome, @descricao, @status, GETDATE(), @CodigoUsuarioAlteracao)";

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        long result = db.Insert(sql, param);

                        if (result > 0)
                        {
                            Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, result);
                        }
                    }
                    else
                    {
                        throw new GS1TradeException("Papel GLN já cadastrado.");
                    }
                }

                return "";
            }
            else
            {
                return "Não foi possível realizar o cadastro.";
            }

        }

        [GS1CNPAttribute(true, true, "EditarPapelGLN", "VisualizarPapelGLN")]
        public object AtualizarPapelGln(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {

                    db.Open(true);
                    db.BeginTransaction();

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    //Inserção na tabela PAPELGLNHISTORICO
                    string sql_historico = @"INSERT INTO PAPELGLNHISTORICO (CODIGO, NOME, DESCRICAO, STATUS, DATAALTERACAO, CODIGOUSUARIOALTERACAO, DATAHISTORICO)
	                                        SELECT CODIGO, NOME, DESCRICAO, STATUS, DATAALTERACAO, CODIGOUSUARIOALTERACAO, GETDATE()
				                                            FROM PAPELGLN WITH (NOLOCK)
				                                            WHERE PAPELGLN.CODIGO = @codigo";

                    dynamic param1 = null;

                    if (parametro.campos != null)
                        param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql_historico, param1);

                    //Verifica se o nome Papel GLN já foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM PAPELGLN P WHERE P.NOME = @nome AND P.CODIGO <> @codigo";

                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {
                        //Atualiza tabela PAPELGLN
                        string sql = "UPDATE PAPELGLN SET NOME = @nome, DESCRICAO = @descricao, STATUS = @status, CODIGOUSUARIOALTERACAO = @CodigoUsuarioAlteracao, DATAALTERACAO = GETDATE() WHERE CODIGO = @codigo";

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);
                        
                        db.Commit();
                        return result;
                    }
                    else
                    {
                        db.Rollback();
                        throw new GS1TradeException("Papel GLN já cadastrado.");
                    }
                }

            }
            else
            {
                return "Não foi possível realizar a atualização.";
            }

        }

        [GS1CNPAttribute("ExcluirPapelGLN", true, true)]
        public object RemoverPapelGLN(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                try
                {
                    string sql = "DELETE FROM PAPELGLN WHERE CODIGO = @codigo";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Exclusão não é permitida. O Papel GLN possui dados vinculados.", e);
                }
            }
            else
            {
                return "Não foi possível remover o item selecionado.";
            }

        }

        [GS1CNPAttribute(false)]
        public object BuscarPapelGLN(dynamic parametro)
        {

            string sql = "SELECT CODIGO, NOME, DESCRICAO, STATUS FROM PAPELGLN WITH (NOLOCK) ";
            
            if(parametro != null && parametro.campos != null) {

                sql += " WHERE 1 = 1 ";

                if(parametro.campos.nome != null) {
                    sql += " AND nome like '%' + @nome + '%'";
                }

                if (parametro.campos.status != null) {
                    sql += " AND status = @status";
                }

                if (parametro.campos.descricao != null) {
                    sql += " AND descricao like '%' + @descricao + '%'";
                }
            }

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> fornecedor = db.Select(sql, param);

                return JsonConvert.SerializeObject(fornecedor);
            }
        }
    }


}
