﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GS1.CNP.BLL.Model;
using System.Reflection;
using StackExchange.Redis;
using System.Web.Configuration;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("ParametroBO")]
    public class ParametroBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "PARAMETRO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public object CarregarParametroEspecifico(dynamic parametro)
        {
            string sql = @"SELECT CHAVE, VALOR FROM PARAMETRO WITH (NOLOCK) WHERE CHAVE = '" + parametro.where + "' AND STATUS = 1";

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();

                List<dynamic> parametros = db.Select(sql);

                return JsonConvert.SerializeObject(parametros);
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarParametros(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, CHAVE, VALOR FROM PARAMETRO WITH (NOLOCK)
                            WHERE CHAVE <> 'parametros.integracoes.ultimaSincronizacaoCrmRgc'

                            UNION ALL

                            SELECT CODIGO, CHAVE, CONVERT(VARCHAR(10),CONVERT(DATETIME,valor,106),103) + ' '  + CONVERT(VARCHAR(8), CONVERT(DATETIME,valor,113), 14) AS VALOR
                            FROM PARAMETRO
                            WHERE CHAVE = 'parametros.integracoes.ultimaSincronizacaoCrmRgc'";

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();

                List<dynamic> parametros = db.Select(sql);

                return JsonConvert.SerializeObject(parametros);
            }
        }

        [GS1CNPAttribute("EditarParametro")]
        public void SalvarParametros(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            if (parametro != null)
            {
                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, 1);
            }

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            try
            {
                if (user != null)
                {

                    using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                    {
                        try
                        {

                            db.Open(true);
                            db.BeginTransaction();

                            if (parametro != null && parametro.campos != null)
                            {
                                parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                                parametro.campos.CodigoUsuarioAlteracao = user.id;

                                string sql_parametro = @"INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'autenticacao.senha.diferente.n.ultimas'; 
                                                        UPDATE PARAMETRO SET VALOR = @senhadiferenteultimas, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'autenticacao.senha.diferente.n.ultimas';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'autenticacao.senha.nudias.avisoexpiracao'; 
                                                        UPDATE PARAMETRO SET VALOR = @diasavisoexpiracao, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'autenticacao.senha.nudias.avisoexpiracao';
                                                         
                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'autenticacao.senha.nudias.validade'; 
                                                        UPDATE PARAMETRO SET VALOR = @diasvalidadesenha, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'autenticacao.senha.nudias.validade';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'autenticacao.senha.nutentativas'; 
                                                        UPDATE PARAMETRO SET VALOR = @tentativassenha, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'autenticacao.senha.nutentativas';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'bo.nu.telefone'; 
                                                        UPDATE PARAMETRO SET VALOR = @numerotelefone, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'bo.nu.telefone';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'midia.nuurl.foto';
                                                        UPDATE PARAMETRO SET VALOR = @nuurlfoto WHERE chave = 'midia.nuurl.foto';
                                                        
                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'midia.nuurl.youtube';
                                                        UPDATE PARAMETRO SET VALOR = @nuurlyoutube WHERE chave = 'midia.nuurl.youtube';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'midia.nuurl.linkeddata';
                                                        UPDATE PARAMETRO SET VALOR = @nuurllinkeddata WHERE chave = 'midia.nuurl.linkeddata';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'midia.nuurl.produto';
                                                        UPDATE PARAMETRO SET VALOR = @nuurlproduto WHERE chave = 'midia.nuurl.produto';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'midia.nuurl.reserva';
                                                        UPDATE PARAMETRO SET VALOR = @nuurlreserva WHERE chave = 'midia.nuurl.reserva';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'midia.numax.foto';
                                                        UPDATE PARAMETRO SET VALOR = @nufoto WHERE chave = 'midia.numax.foto';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'grid.resultados.por.pagina';
                                                        UPDATE PARAMETRO SET VALOR = @resultadosporpagina WHERE chave = 'grid.resultados.por.pagina';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'ajuda.urlchat';
                                                        UPDATE PARAMETRO SET VALOR = @urlchat WHERE chave = 'ajuda.urlchat';
                                                        
                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'autenticacao.senha.nutentativasprerecaptcha'; 
                                                        UPDATE PARAMETRO SET VALOR = @tentativassenhasemrecaptcha, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'autenticacao.senha.nutentativasprerecaptcha';                                                      

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'certificacao.numeses.vencimento.pesosmedidas'; 
                                                        UPDATE PARAMETRO SET VALOR = @validadecertificacaopesosmedidas, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'certificacao.numeses.vencimento.pesosmedidas';
                                                        
                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'certificacao.numeses.vencimento.codigobarras'; 
                                                        UPDATE PARAMETRO SET VALOR = @validadecertificacaocodigobarras, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'certificacao.numeses.vencimento.codigobarras';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'email.envio.relatorio.mensal'; 
                                                        UPDATE PARAMETRO SET VALOR = @emailenvio, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'email.envio.relatorio.mensal';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'gepir.perfilpadrao'; 
                                                        UPDATE PARAMETRO SET VALOR = @perfilpadrao, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'gepir.perfilpadrao';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'gepir.diarenovacaofranquia'; 
                                                        UPDATE PARAMETRO SET VALOR = @diarenovacaofranquia, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'gepir.diarenovacaofranquia';

                                                        INSERT INTO [dbo].[ParametroHistorico] ([CodigoParametro],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao],[DataHistorico])
                                                        SELECT [Codigo],[Chave],[Valor],[CodigoUsuarioAlteracao],[DataAlteracao], GETDATE() FROM Parametro WHERE chave = 'gepir.glnconsultapadrao'; 
                                                        UPDATE PARAMETRO SET VALOR = @glnconsultapadrao, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE chave = 'gepir.glnconsultapadrao';


                                                        ";




                                                        

                                db.Update(sql_parametro, ((JToken)parametro.campos).ToExpando());

                                db.Commit();
                            }
                        }
                        catch (Exception)
                        {
                            db.Rollback();
                            throw;
                            //TODO - tratar
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new GS1TradeException("Não foi possível salvar os parâmetros selecionados.", e);
            }
        }

        [GS1CNPAttribute(false)]
        public int BuscarQuantidadeSessoesAtivas(dynamic parametro)
        {
            try
            {

                var sessionSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
                string host = sessionSection.Providers[0].Parameters["host"];
                string accessKey = sessionSection.Providers[0].Parameters["accessKey"];
                if (host != string.Empty && accessKey != string.Empty)
                {
                    ConnectionMultiplexer connection = ConnectionMultiplexer.Connect(host + ",ssl=true,password=" + accessKey);
                    var server = connection.GetServer(host, 6380);
                    return server.Keys(pattern: "*_Data*").Count();
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                return 0;
             
            }
                       
           
        }
    }
}
