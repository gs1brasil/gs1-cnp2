﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("PerfilBO")]
    class PerfilBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "Perfil"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        [GS1CNPAttribute(false)]
        public object buscarPerfis(dynamic parametro)
        {

            using (Database db = new Database("BD"))
            {

                //Database db = new Database("BD");
                db.Open();

                object retorno = db.All("PERFIL");

                return JsonConvert.SerializeObject(retorno);
            }

            //            //TODO - parametrizar quantidade por pagina
            //            string numeropagina = "0";
            //            if (parametro.paginaatual != null && parametro.paginaatual != "")
            //                numeropagina = parametro.paginaatual;

            //            string sql = @";WITH DADOS AS (
            //	                            SELECT p.Codigo, p.Nome, p.Descricao, p.Status, p.CodigoTipoUsuario, p.DataAlteracao, p.CodigoUsuarioAlteracao 
            //	                            FROM perfil p
            //                            )
            //                            SELECT Codigo, Nome, Descricao, Status, CodigoTipoUsuario, DataAlteracao, CodigoUsuarioAlteracao 
            //                            ,TOTALREGISTROS = COUNT(*) OVER()
            //                            FROM DADOS
            //                            ORDER BY Codigo
            //                            OFFSET " + numeropagina  + @" ROWS FETCH NEXT 10 ROWS ONLY";

            //            Database db = new Database("BD");
            //            db.Open();

            //            dynamic param = null;

            //            if (parametro.where != null)
            //                param = GS1.Trade.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            //            List<dynamic> usuarios = db.Select(sql, param);

            //            return JsonConvert.SerializeObject(usuarios);

        }

        [GS1CNPAttribute(false)]
        public object verificaPerfil(dynamic parametro)
        {

            string sql = @"SELECT P.CODIGO, P.NOME, P.DESCRICAO, P.STATUS, P.CODIGOTIPOUSUARIO, P.DATAALTERACAO, P.CODIGOUSUARIOALTERACAO 
                            FROM PERFIL P WITH (NOLOCK)
                            WHERE P.STATUS = 1 AND P.CODIGOTIPOUSUARIO = " + parametro.where.codigo;

            using (Database db = new Database("BD"))
            {

                db.Open();
                //Database db = new Database("BD");

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> usuarios = db.Select(sql, param);

                return JsonConvert.SerializeObject(usuarios);
            }
        }

        [GS1CNPAttribute(false)]
        public object buscarPerfilTipoUsuario(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            if (user != null)
            {
                string sql = string.Empty;
                if (user.id_tipousuario == 2)
                {
                    parametro.where.codigo = 2;
                }

                sql = @"SELECT P.CODIGO, P.NOME, P.DESCRICAO, P.STATUS, P.CODIGOTIPOUSUARIO, P.DATAALTERACAO, P.CODIGOUSUARIOALTERACAO 
                         FROM PERFIL P WITH (NOLOCK) 
                         WHERE P.STATUS = 1 
                            AND P.CODIGOTIPOUSUARIO = @codigo";

                using (Database db = new Database("BD"))
                {

                    db.Open();

                    dynamic param = null;

                    if (parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    List<dynamic> usuarios = db.Select(sql, param);

                    return JsonConvert.SerializeObject(usuarios);
                }
            }
            return null;
        }

        [GS1CNPAttribute(false)]
        public object BuscarTipoUsuario(dynamic parametro)
        {

            //Database db = new Database("BD");
            //db.Open();

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.All("TIPOUSUARIO");
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public object CarregarArvorePermissoes(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            string sql = "";
            bool disabled = false;

            using (Database db = new Database("BD"))
            {

                db.Open();

                try
                {
                    if (parametro.campos.codigo != null)
                    {
                        sql = @"SELECT M.CODIGO, M.NOME, M.DESCRICAO, M.STATUS, M.ICONE, A.CODIGO AS CODIGOFORMULARIO, A.NOME AS NOMEFORMULARIO, A.DESCRICAO AS DESCRICAOFORMULARIO, F.NOME AS 'NOMEFUNCIONALIDADE', F.CODIGO AS 'CODIGOFUNCIONALIDADE', F.DESCRICAO AS 'DESCRICAOFUNCIONALIDADE', F.CODIGOTIPOUSUARIO, 
                                        (SELECT COUNT(0) FROM PERFILFUNCIONALIDADE PF WHERE PF.CODIGOFUNCIONALIDADE = F.CODIGO AND PF.CODIGOPERFIL = '" + parametro.campos.codigo + @"') AS 'PERMISSAO'
                                        FROM MODULO M WITH (NOLOCK)
                                        INNER JOIN FORMULARIO A WITH (NOLOCK) ON M.CODIGO = A.CODIGOMODULO
                                        INNER JOIN FUNCIONALIDADE F WITH (NOLOCK) ON (A.CODIGO = F.CODIGOFORMULARIO) 
	                                        WHERE M.STATUS = 1
                                            AND CODIGOTIPOUSUARIO = " + parametro.campos.codigotipousuario +
                                            " ORDER BY M.Ordem, A.Ordem";

                        if (parametro.campos.codigo == 1 || parametro.campos.disabled == true)
                            disabled = true;
                    }
                    else
                    {
                        sql = @"SELECT M.CODIGO, M.NOME, M.DESCRICAO, M.STATUS, M.ICONE, A.CODIGO AS CODIGOFORMULARIO, A.NOME AS NOMEFORMULARIO, A.DESCRICAO AS DESCRICAOFORMULARIO, F.NOME AS 'NOMEFUNCIONALIDADE', F.CODIGO AS 'CODIGOFUNCIONALIDADE', F.DESCRICAO AS 'DESCRICAOFUNCIONALIDADE', F.CODIGOTIPOUSUARIO, 
                                        0 AS 'PERMISSAO'
                                        FROM MODULO M WITH (NOLOCK)
                                        INNER JOIN FORMULARIO A WITH (NOLOCK) ON M.CODIGO = A.CODIGOMODULO
                                        INNER JOIN FUNCIONALIDADE F WITH (NOLOCK) ON (A.CODIGO = F.CODIGOFORMULARIO) 
	                                        WHERE M.STATUS = 1
                                            AND CODIGOTIPOUSUARIO = " + parametro.campos.codigotipousuario +
                                            " ORDER BY M.Ordem, A.Ordem";
                    }

                    //Database db = new Database("BD");
                    //db.Open();

                    dynamic param = null;

                    if (parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    List<dynamic> arvore = db.Select(sql, param);

                    //{ text: atributo[i].nome, icon: atributo[i].icone, children: [], state: { opened: true, disabled: fldisabled} }
                    var result = (from dynamic modulo in arvore.DistinctBy(x => new { x.codigo, x.nome, x.descricao, x.icone })
                                  select new
                                  {
                                      text = modulo.nome,
                                      icon = modulo.icone,
                                      state = new
                                      {
                                          opened = true,
                                          disabled = disabled
                                      },
                                      children = (from formulario in arvore.Where(x => x.codigo.Equals(modulo.codigo))
                                                  .DistinctBy(x => new { x.codigoformulario })
                                                  select new
                                                  {
                                                      text = formulario.descricaoformulario,
                                                      icon = formulario.icone,
                                                      state = new
                                                      {
                                                          opened = false,
                                                          //selected = formulario.permissao.Equals(1),
                                                          disabled = disabled
                                                      },
                                                      children = (from funcionalidade in arvore.Where(x => x.codigoformulario.Equals(formulario.codigoformulario))
                                                                  .DistinctBy(x => new { x.codigofuncionalidade })
                                                                  select new
                                                                  {
                                                                      id = funcionalidade.codigofuncionalidade,
                                                                      text = funcionalidade.descricaofuncionalidade,
                                                                      icon = funcionalidade.icone,
                                                                      state = new
                                                                      {
                                                                          opened = false,
                                                                          selected = funcionalidade.permissao.Equals(1),
                                                                          disabled = disabled
                                                                      },
                                                                  }).ToList<dynamic>()
                                                  }).ToList<dynamic>()


                                  }).ToList<dynamic>();

                    return JsonConvert.SerializeObject(result);

                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Não foi possível carregar as permissões do perfil.", e);
                }
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarPerfilEspecifico(dynamic parametro)
        {

            using (Database db = new Database("BD"))
            {
                db.Open();
                string sql = @"SELECT P.CODIGO, P.NOME, P.DESCRICAO, P.STATUS, P.CODIGOTIPOUSUARIO, P.DATAALTERACAO, P.CODIGOUSUARIOALTERACAO, T.CODIGO AS 'TIPOUSUARIOCODIGO', T.NOME AS 'TIPOUSUARIONOME', T.DESCRICAO AS 'TIPOUSUARIODESCRICAO', T.STATUS AS 'TIPOUSUARIOSTATUS' FROM PERFIL P INNER JOIN TIPOUSUARIO T WITH (NOLOCK) ON (P.CODIGOTIPOUSUARIO = T.CODIGO)
                                WHERE P.CODIGO = '" + parametro.where.codigo + "'";

                //Database db = new Database("BD");
                //db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> usuarios = db.Select(sql, param);

                return JsonConvert.SerializeObject(usuarios);
            }
        }

        [GS1CNPAttribute("CadastrarPerfil", true, true)]
        public void InserePermissao(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = this.getSession("USUARIO_LOGADO") as Login;

            using (Database db = new Database("BD"))
            {
                db.Open();

                try
                {
                    if (user != null)
                    {
                        if (parametro.campos.codigo == 1)
                        {
                            throw new GS1TradeException("Não foi possível alterar as permissões do administrador.");
                        }

                        if (parametro.campos.atualiza == true)
                        {
                            db.Delete("DELETE FROM PERFILFUNCIONALIDADE WHERE CODIGOPERFIL = '" + parametro.campos.codigo.ToString() + "'");
                        }

                        if (parametro != null && parametro.campos != null && parametro.campos.permissoes != null && parametro.campos.permissoes.Count > 0)
                        {
                            parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                            parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                            string dml = @"INSERT INTO PERFILFUNCIONALIDADE VALUES ";

                            foreach (var item in parametro.campos.permissoes)
                            {
                                if (((string)item.Value).StartsWith("j") == false)
                                    dml += "( '" + parametro.campos.codigo + "', '" + item.Value + "', '" + parametro.campos.DataAlteracao + "', '" + parametro.campos.CodigoUsuarioAlteracao + "'),";
                            }
                            dml = dml.Replace("[", string.Empty).Replace("]", string.Empty).Replace("\"", string.Empty);
                            dml = dml.Remove(dml.Length - 1);

                            db.Delete("DELETE FROM PERFILFUNCIONALIDADE WHERE CODIGOPERFIL = '" + parametro.campos.codigo.ToString() + "'");

                            long result = db.Insert(dml, null, false);

                            if (result > 0)
                            {
                                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, result);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Não foi possível inserir as permissões do perfil.", e);
                }
            }
        }

        [GS1CNPAttribute(false)]
        public object carregaModulo(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                object retorno = db.All("MODULO");

                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(true, true, "EditarPerfil", "VisualizarPerfil")]
        public override void Update(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            if (parametro != null)
            {
                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, long.Parse(parametro.where.ToString()));
            }

            Login user = this.getSession("USUARIO_LOGADO") as Login;

            using (Database db = new Database("BD"))
            {
                db.Open();

                if (user != null)
                {
                    if(user.id_tipousuario != 1)
                        parametro.campos.Remove("CodigoTipoUsuario");

                    if (parametro.campos.Status == 0)
                    {
                        string sql = "SELECT F.* FROM PERFIL F WITH (NOLOCK) INNER JOIN USUARIO U WITH (NOLOCK) ON (U.CODIGOPERFIL = F.CODIGO) WHERE F.NOME = '" + parametro.campos.Nome + "'";

                        dynamic param = null;

                        if (parametro.where != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                        List<dynamic> usuarios = db.Select(sql, param);

                        if (usuarios.Count == 0)
                        {

                            parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                            parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                            base.Update((object)parametro);
                        }
                        else
                            throw new GS1TradeException("Este perfil está em uso, não foi possível inativar.");
                    }
                    else
                    {
                        parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                        base.Update((object)parametro);
                    }
                }
            }
        }

        [GS1CNPAttribute(false)]
        public object verificaUsuario(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                string sql = "SELECT (1) FROM PERFIL WITH (NOLOCK) WHERE NOME = '" + parametro.campos.nome + "' AND CODIGO <> '" + parametro.campos.codigo + "'";

                //Database db = new Database("BD");
                //db.Open();
                dynamic param = null;
                List<dynamic> usuario = db.Select(sql, param);
                return JsonConvert.SerializeObject(usuario);
            }
        }

        [GS1CNPAttribute("CadastrarPerfil", true, true)]
        public override string Insert(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = this.getSession("USUARIO_LOGADO") as Login;

            try
            {
                if (user != null)
                {
                    parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    var result = base.Insert((object)parametro);

                    if (!string.IsNullOrEmpty(result))
                    {
                        dynamic objResult = JsonConvert.DeserializeObject(result);

                        Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, Convert.ToInt64(objResult[0].codigo));
                    }

                    return result;
                }
                else
                    return "Não foi possível realizar o cadastro.";
            }
            catch (Exception e)
            {
                throw new GS1TradeException("Não foi possível carregar as permissões do perfil.", e);
            }

        }

        [GS1CNPAttribute("ExcluirPerfil", true, true)]
        public override void Delete(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            if (parametro != null)
            {
                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, long.Parse(parametro.where.ToString()));
            }

            if (parametro.where < 5)
            {
                throw new GS1TradeException("Este é um perfil do sistema e não pode ser excluído.");
            }
            else
            {
                try
                {
                    base.Delete((object)parametro);
                }
                catch (Exception e)
                {

                    throw new GS1TradeException("Exclusão não é permitida. Perfil em uso.", e);
                }
            }
        }

        [GS1CNPAttribute("PesquisarPerfil")]
        public override string SelectFiltro(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);
            return base.SelectFiltro((object)parametro);
        }

        [GS1CNPAttribute("PesquisarPerfil")]
        public string BuscarPerfis(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            string sql = @"select p.Codigo,
	                            p.Nome,
	                            p.Descricao,
	                            p.Status,
	                            p.DataAlteracao,
	                            p.CodigoUsuarioAlteracao,
	                            p.CodigoTipoUsuario,
	                            tu.Nome as nometipousuario
                                ,(select count(z.codigo) from Usuario z where z.CodigoPerfil = p.codigo) as TotalUsuarios
                            from perfil as p WITH(NOLOCK)
                            inner join TipoUsuario as tu WITH(NOLOCK) on tu.Codigo = p.CodigoTipoUsuario";

            //Database db = new Database("BD");
            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = null;

                if (parametro.where != null && (((Newtonsoft.Json.Linq.JContainer)(parametro.where))).Count != 0)
                {
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    sql += " WHERE ";

                    if (parametro.where.nome != null)
                        sql += "(@nome IS NULL OR P.NOME LIKE '%' + @nome + '%') AND";
                    if (parametro.where.descricao != null)
                        sql += "(@descricao IS NULL OR P.DESCRICAO LIKE '%' + @descricao + '%') AND";
                    if (parametro.where.codigotipousuario != null)
                        sql += "(@codigotipousuario IS NULL OR CODIGOTIPOUSUARIO = @codigotipousuario) AND";
                    if (parametro.where.status != null)
                        sql += "(@status IS NULL OR P.STATUS = @status) AND";

                    sql = sql.Remove(sql.Length - 4);
                }

                string registroPorPagina = "10";
                if (parametro.registroPorPagina > 0)
                    registroPorPagina = parametro.registroPorPagina.ToString();

                string numeropagina = "1";
                if (parametro.paginaAtual > 0)
                    numeropagina = parametro.paginaAtual.ToString();

                string ordenacao = "Codigo ASC";
                if (parametro.ordenacao != string.Empty)
                    ordenacao = parametro.ordenacao;


                sql = @";WITH DADOS AS ( " + sql;
                sql = sql + @")
                            SELECT * 
                            ,TOTALREGISTROS = COUNT(*) OVER()
                            FROM DADOS WITH (NOLOCK)
                            ORDER BY " + ordenacao + @"
                            OFFSET " + registroPorPagina + @" * (" + numeropagina + @"-1) ROWS FETCH NEXT " + registroPorPagina + @" ROWS ONLY";

                List<dynamic> perfil = db.Select(sql, param);
                return JsonConvert.SerializeObject(perfil);
            }
        }

        public class Perfil
        {
            public int id_perfil { get; set; }
        }
    }
}
