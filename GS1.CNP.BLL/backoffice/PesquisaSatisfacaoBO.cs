﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("PesquisaSatisfacaoBO")]
    public class PesquisaSatisfacaoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "PESQUISASATISFACAO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarPesquisaSatisfacao(dynamic parametro)
        {

            string sql = @"SELECT PS.CODIGO, PS.NOME AS NOME, CONVERT(VARCHAR(10),CONVERT(DATE,PS.INICIO,106),103) AS INICIO, CONVERT(VARCHAR(10),CONVERT(DATE,PS.FIM,106),103) AS FIM, U.NOME AS USUARIO, PS.DATAALTERACAO, 
                                PS.DESCRICAO, PS.CODIGOPUBLICOALVO, PS.URL, PS.STATUS 
                                FROM PESQUISASATISFACAO PS 
                                INNER JOIN USUARIO U ON U.CODIGO = PS.CODIGOUSUARIOALTERACAO
                                INNER JOIN PUBLICOALVO PA ON PA.CODIGO = PS.CODIGOPUBLICOALVO";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("CadastrarPesquisaSatisfacao", true, true)]
        public object CadastrarPesquisaSatisfacao(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                    db.Open();

                    //Verifica se a pesquisa de satisfação já foi cadastrada
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM PESQUISASATISFACAO P WHERE P.NOME = @nome";

                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {

                        //Converte campos data
                        if (parametro.campos.inicio != null)
                            parametro.campos.inicio = parametro.campos.inicio.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        if (parametro.campos.fim != null)
                            parametro.campos.fim = parametro.campos.fim.ToString("MM/dd/yyyy HH:mm:ss.fff");

                        string sql = @"INSERT INTO PESQUISASATISFACAO (nome, status, descricao, inicio, fim, url, dataalteracao, codigousuarioalteracao, codigopublicoalvo) 
                                        VALUES (@nome, @status, @descricao, @inicio, @fim, @url, GETDATE(), @CodigoUsuarioAlteracao, @codigopublicoalvo)";

                        dynamic param = null;

                        if (parametro.campos != null) {
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                        }

                        long result = db.Insert(sql, param);

                        if (result > 0)
                        {
                            Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, result);
                        }
                    }
                    else
                    {
                        throw new GS1TradeException("Pesquisa de Satisfação já cadastrada.");
                    }
                }

                return "";
            }
            else
            {
                return "Não foi possível realizar o cadastro.";
            }

        }

        [GS1CNPAttribute(true, true, "EditarPesquisaSatisfacao", "VisualizarPesquisaSatisfacao")]
        public object EditarPesquisaSatisfacao(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {

                    db.Open(true);
                    db.BeginTransaction();

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    //Inserção na tabela PESQUISASATISFACAOHISTORICO
                    string sql_historico = @"INSERT INTO PESQUISASATISFACAOHISTORICO (CODIGO, NOME, DESCRICAO, STATUS, INICIO, FIM, URL, CODIGOUSUARIOALTERACAO, DATAALTERACAO, CODIGOPUBLICOALVO, DATAHISTORICO)
	                                        SELECT CODIGO, NOME, DESCRICAO, STATUS, INICIO, FIM, URL, CODIGOUSUARIOALTERACAO, DATAALTERACAO, CODIGOPUBLICOALVO, GETDATE()
				                                            FROM PESQUISASATISFACAO WITH (NOLOCK)
				                                            WHERE PESQUISASATISFACAO.CODIGO = @codigo";

                    dynamic param1 = null;

                    if (parametro.campos != null)
                        param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql_historico, param1);

                    //Verifica se o título da Pesquisa de Satisfação já foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM PESQUISASATISFACAO P WHERE P.NOME = @nome AND P.CODIGO <> @codigo";
                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {
                        //Converte campos data
                        if (parametro.campos.inicio != null)
                            parametro.campos.inicio = parametro.campos.inicio.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        if (parametro.campos.fim != null)
                            parametro.campos.fim = parametro.campos.fim.ToString("MM/dd/yyyy HH:mm:ss.fff");

                        //Atualiza tabela PESQUISASATISFACAO
                        string sql = @"UPDATE PESQUISASATISFACAO SET NOME = @nome, STATUS = @status, descricao = @descricao, inicio = @inicio, fim = @fim, url = @url, CODIGOUSUARIOALTERACAO = @CodigoUsuarioAlteracao, 
                                        codigopublicoalvo = @codigopublicoalvo, DATAALTERACAO = GETDATE() WHERE CODIGO = @codigo";
                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        db.Commit();
                        return result;
                    }
                    else
                    {
                        db.Rollback();
                        throw new GS1TradeException("Pesquisa de Satisfação já cadastrada.");
                    }
                }

            }
            else
            {
                return "Não foi possível realizar a atualização.";
            }

        }

        [GS1CNPAttribute("ExcluirPesquisaSatisfacao", true, true)]
        public object ExcluirPesquisaSatisfacao(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                try
                {
                    string sql = "DELETE FROM PESQUISASATISFACAO WHERE CODIGO = @codigo";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Exclusão não é permitida. A Pesquisa de Satisfação possui dados vinculados.", e);
                }
            }
            else
            {
                return "Não foi possível remover o item selecionado.";
            }

        }

        [GS1CNPAttribute(false)]
        public object PesquisarPesquisaSatisfacao(dynamic parametro)
        {

            string sql = @"SELECT PS.CODIGO, PS.NOME AS NOME, CONVERT(VARCHAR(10),CONVERT(DATE,PS.INICIO,106),103) AS INICIO, CONVERT(VARCHAR(10),CONVERT(DATE,PS.FIM,106),103) AS FIM, U.NOME AS USUARIO, PS.DATAALTERACAO, 
                                PS.DESCRICAO, PS.CODIGOPUBLICOALVO, PS.URL, PS.STATUS 
                                FROM PESQUISASATISFACAO PS   (NOLOCK) 
                                INNER JOIN USUARIO U  (NOLOCK)  ON U.CODIGO = PS.CODIGOUSUARIOALTERACAO
                                INNER JOIN PUBLICOALVO PA   (NOLOCK)  ON PA.CODIGO = PS.CODIGOPUBLICOALVO ";
            
            if(parametro != null && parametro.campos != null) {

                sql += " WHERE 1 = 1 ";

                if(parametro.campos.nome != null) {
                    sql += " AND PS.NOME like '%' + @nome + '%'";
                }

                if (parametro.campos.status != null) {
                    sql += " AND PS.STATUS = @status";
                }

                if (parametro.campos.descricao != null)
                {
                    sql += " AND PS.DESCRICAO like '%' + @descricao + '%'";
                }

                if (parametro.campos.url != null)
                {
                    sql += " AND PS.URL like '%' + @url + '%'";
                }

                if (parametro.campos.codigopublicoalvo != null)
                {
                    sql += " AND PS.CODIGOPUBLICOALVO = @codigopublicoalvo";
                }

                if ((parametro.campos.inicio != null && parametro.campos.inicio != "") && (parametro.campos.fim == null || parametro.campos.fim == ""))
                {
                    parametro.campos.inicio = DateTime.ParseExact(Convert.ToString(parametro.campos.inicio.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sql += @" AND @inicio = PS.INICIO ";
                }
                else if ((parametro.campos.inicio == null || parametro.campos.inicio == "") && parametro.campos.fim != null && parametro.campos.fim != "")
                {
                    parametro.campos.fim = DateTime.ParseExact(Convert.ToString(parametro.campos.fim.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sql += @" AND @fim = PS.FIM ";
                }
                else if (parametro.campos.inicio != null && parametro.campos.inicio != "" && parametro.campos.fim != null && parametro.campos.fim != "")
                {
                    parametro.campos.inicio = DateTime.ParseExact(Convert.ToString(parametro.campos.inicio.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    parametro.campos.fim = DateTime.ParseExact(Convert.ToString(parametro.campos.fim.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    sql += @" AND (@inicio >= PS.INICIO AND @fim <= PS.FIM) ";
                }
            }

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> fornecedor = db.Select(sql, param);

                return JsonConvert.SerializeObject(fornecedor);
            }
        }

        [GS1CNPAttribute(false, true)]
        public override void Update(dynamic parametro)
        {
            base.Update((object)parametro);
        }

        [GS1CNPAttribute(false)]
        public object CadastrarPesquisaSatisfacaoUsuario(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {

                    parametro.campos.codigousuario = user.id.ToString();
                    db.Open();

                    string sql = @"INSERT INTO PESQUISASATISFACAOUSUARIO (CODIGOPESQUISASATISFACAO, CODIGOUSUARIO, DATAALTERACAO, CODIGOSTATUSPESQUISASATISFACAOUSUARIO) 
                                    VALUES (@codigo, @codigousuario, GETDATE(), @status)";

                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    long result = db.Insert(sql, param);
                    return result;
                }
            }
            else
            {
                return "Não foi cadastrar a Pesquisa de Satisfação.";
            }

        }
    }


}
