﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GS1.CNP.BLL.Core;
using System.Data;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using GS1.CNP.BLL.Model;
using System.IO;
using System.Web;
using OfficeOpenXml;
using System.Reflection;
using System.Data.SqlClient;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("ProdutoBO")]
    public class ProdutoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "PRODUTO"; }
        }

        public override string nomePK
        {
            get { return "CODIGOPRODUTO"; }
        }

        [GS1CNPAttribute(false)]
        private dynamic ManusearGTIN(dynamic parametro)
        {
            if (parametro != null && parametro.campos != null)
            {
                if (parametro.campos.codigotipogtin != null)
                {
                    //GTIN-12 ou GTIN-13
                    if (parametro.campos.codigotipogtin == 2 || parametro.campos.codigotipogtin == 3)
                    {
                        string menorPrefixo = String.Empty;
                        dynamic obterprefixo = JsonConvert.DeserializeObject(PegarGTINValido(parametro.campos.codigotipogtin.ToString()));

                        //Seleciona o menor Prefixo
                        if (obterprefixo != null)
                        {
                            menorPrefixo = obterprefixo[0].numeroprefixo;
                        }

                        if (menorPrefixo != null && menorPrefixo != "")
                        {

                            //GTIN-12
                            if (parametro.campos.codigotipogtin == 2)
                            {
                                parametro.campos.codigostatusgtin = 1;
                                return GeradorGTIN(menorPrefixo, 12);
                            }
                            //GTIN-13
                            else if (parametro.campos.codigotipogtin == 3)
                            {
                                parametro.campos.codigostatusgtin = 1;
                                return GeradorGTIN(menorPrefixo, 13);
                            }

                            return null;
                        }
                        else
                        {
                            throw new GS1TradeException("Quantidade de produtos máxima atingida ou licença inválida.");
                        }
                    }
                    //GTIN-14
                    else if (parametro.campos.codigotipogtin == 4)
                    {

                        dynamic retornoGTINOrigem = JsonConvert.DeserializeObject(RetornarGTINProdutoOrigem(parametro.campos.codigogtinorigem.ToString()));

                        if (retornoGTINOrigem != null)
                        {
                            //GTIN-8
                            if (retornoGTINOrigem[0].codigotipogtin == 1)
                            {
                                dynamic dadosretornados = retornoGTINOrigem[0];
                                dynamic retornaProximaVariante = JsonConvert.DeserializeObject(RetornarProximaVariante(parametro.campos.codigoprodutoorigem.ToString()));

                                if (retornaProximaVariante != null)
                                {
                                    string indicador = retornaProximaVariante[0].codigo;

                                    var gtin = indicador.ToString() + "00000" + dadosretornados.globaltradeitemnumber.ToString().Substring(0, 7);
                                    string verificador = GeraDigitoVerificador(gtin);

                                    dynamic numerosGerados = new ExpandoObject();
                                    numerosGerados.globaltradeitemnumber = gtin + verificador;
                                    numerosGerados.globaltradeitemnumber = gtin + verificador;
                                    numerosGerados.variantelogistica = indicador.ToString();
                                    numerosGerados.coditem = dadosretornados.coditem;
                                    numerosGerados.nrprefixo = dadosretornados.nrprefixo;
                                    parametro.campos.codigostatusgtin = 1;
                                    return numerosGerados;
                                    //$scope.onSave($scope.itemForm, 'GerarGTIN');
                                }
                                else
                                {
                                    throw new GS1TradeException("Número máximo de variantes logísticas alcançada.");
                                }
                            }
                            //GTIN-12
                            else if (retornoGTINOrigem[0].codigotipogtin == 2)
                            {

                                dynamic dadosretornados = retornoGTINOrigem[0];
                                dynamic retornaProximaVariante = JsonConvert.DeserializeObject(RetornarProximaVariante(parametro.campos.codigoprodutoorigem.ToString()));

                                if (retornaProximaVariante != null)
                                {
                                    string indicador = retornaProximaVariante[0].codigo;

                                    var gtin = indicador.ToString() + "0" + dadosretornados.globaltradeitemnumber.ToString().Substring(0, 11);
                                    string verificador = GeraDigitoVerificador(Convert.ToString(gtin));

                                    dynamic numerosGerados = new ExpandoObject();
                                    numerosGerados.globaltradeitemnumber = gtin + verificador;
                                    numerosGerados.variantelogistica = indicador.ToString();
                                    numerosGerados.coditem = dadosretornados.coditem;
                                    numerosGerados.nrprefixo = dadosretornados.nrprefixo;
                                    parametro.campos.codigostatusgtin = 1;
                                    return numerosGerados;
                                    //$scope.onSave($scope.itemForm, 'GerarGTIN');
                                }
                                else
                                {
                                    throw new GS1TradeException("Número máximo de variantes logísticas alcançada.");
                                }
                            }
                            //GTIN-13
                            else if (retornoGTINOrigem[0].codigotipogtin == 3)
                            {

                                dynamic dadosretornados = retornoGTINOrigem[0];
                                dynamic retornaProximaVariante = JsonConvert.DeserializeObject(RetornarProximaVariante(parametro.campos.codigoprodutoorigem.ToString()));

                                if (retornaProximaVariante != null)
                                {
                                    string indicador = retornaProximaVariante[0].codigo;

                                    var gtin = indicador.ToString() + dadosretornados.globaltradeitemnumber.ToString().Substring(0, 12);
                                    string verificador = GeraDigitoVerificador(Convert.ToString(gtin));

                                    dynamic numerosGerados = new ExpandoObject();
                                    numerosGerados.globaltradeitemnumber = gtin + verificador;
                                    numerosGerados.variantelogistica = indicador.ToString();
                                    numerosGerados.coditem = dadosretornados.coditem;
                                    numerosGerados.nrprefixo = dadosretornados.nrprefixo;
                                    parametro.campos.codigostatusgtin = 1;
                                    return numerosGerados;
                                    //$scope.onSave($scope.itemForm, 'GerarGTIN');
                                }
                                else
                                {
                                    throw new GS1TradeException("Número máximo de variantes logísticas alcançada.");
                                }
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        //Gerador de GTIN
        public dynamic GeradorGTIN(string prefixo, int tamanho)
        {

            dynamic retorno = JsonConvert.DeserializeObject(VerificarSequenciaPrefixoProduto(prefixo, tamanho));

            if (retorno != null)
            {
                string ultimogerado = retorno[0].ultimoglngerado;
                string sequencialGerado = String.Empty, GTINGerado, digitoVerificador;
                int tamanhoGlnSemVerificador = (tamanho - 1);
                int tamanhoPrefixo = prefixo.ToString().Length;
                int tamanhoSequencial = tamanhoGlnSemVerificador - tamanhoPrefixo, sequencial;
                string globaltradeitemnumber = String.Empty;
                dynamic numerosGerados = new ExpandoObject();

                if (ultimogerado == null || ultimogerado == "")
                    sequencial = 1;
                else
                {
                    sequencial = Convert.ToInt32(ultimogerado) + 1;

                    if ((tamanhoPrefixo == 11 && sequencial.ToString().Length > 1) || (tamanhoPrefixo == 10 && sequencial.ToString().Length > 2) ||
                        (tamanhoPrefixo == 9 && sequencial.ToString().Length > 3) || (tamanhoPrefixo == 8 && sequencial.ToString().Length > 4) ||
                        (tamanhoPrefixo == 7 && sequencial.ToString().Length > 5))
                    {

                        globaltradeitemnumber = "";

                        throw new GS1TradeException("Quantidade de produtos máxima atingida.");
                    }
                }

                //Gera Número sequencial
                while (sequencialGerado.ToString().Length < tamanhoSequencial - sequencial.ToString().Length)
                {
                    sequencialGerado += "0";
                }

                sequencialGerado += sequencial;
                numerosGerados.coditem = sequencialGerado;

                GTINGerado = prefixo + sequencialGerado;

                //Gera dígito verificador
                string verificador = GeraDigitoVerificador(GTINGerado);
                GTINGerado = GTINGerado + verificador;
                numerosGerados.globaltradeitemnumber = GTINGerado;
                numerosGerados.nrprefixo = prefixo;
                //$scope.onSave($scope.itemForm, 'GerarGTIN');
                return numerosGerados;
            }
            else
            {
                return null;
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutos(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT P.CODIGOPRODUTO, P.GLOBALTRADEITEMNUMBER, P.CODIGOPRODUTOORIGEM, P.CODIGOTIPOGTIN, P.NRPREFIXO, P.CODITEM, TP.CODIGO AS CODIGOTIPOPRODUTO, TP.NOME AS NOMETIPOPRODUTO,
                                        P.PRODUCTDESCRIPTION, CONVERT(VARCHAR(10),CONVERT(DATE, P.DATAINCLUSAO,106),103) AS DATAINCLUSAO, CONVERT(VARCHAR(10),CONVERT(DATE,P.DATASUSPENSAO,106),103) AS DATASUSPENSAO,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.DATAREATIVACAO,106),103) AS DATAREATIVACAO, TG.NOME AS NOMETIPOGTIN, CONVERT(VARCHAR(10),CONVERT(DATE, P.DATAALTERACAO,106),103) AS DATAALTERACAO, 
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.DATACANCELAMENTO,106),103) AS DATACANCELAMENTO, CONVERT(VARCHAR(10),CONVERT(DATE,P.DATAREUTILIZACAO,106),103) AS DATAREUTILIZACAO,
                                        P.CODIGOLINGUA, P.COUNTRYCODE, P.ESTADO, P.INDICADORCOMPARTILHADADOS, P.OBSERVACOES, P.CODIGOASSOCIADO,
                                        P.CODIGOSTATUSGTIN, P.CODESEGMENT, P.CODEFAMILY, P.CODECLASS, P.CODEBRICK, P.CODEBRICKATTRIBUTE, P.TRADEITEMCOUNTRYOFORIGIN,
                                        P.INFORMATIONPROVIDER, LF.NOME AS NAMEOFINFORMATIONPROVIDER, P.BRANDNAME, P.MODELNUMBER,
                                        P.IMPORTCLASSIFICATIONTYPE, P.IMPORTCLASSIFICATIONVALUE, P.ISTRADEITEMABASEUNIT, P.STORAGEHANDLINGTEMPMINIMUMUOM,
                                        P.ISTRADEITEMACONSUMERUNIT, P.ISTRADEITEMAMODEL, P.ISTRADEITEMANINVOICEUNIT, P.MINIMUMTRADEITEMLIFESPANFROMTIMEOFPRODUCTION,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.STARTAVAILABILITYDATETIME,106),103) AS STARTAVAILABILITYDATETIME, CONVERT(VARCHAR(10),CONVERT(DATE,P.ENDAVAILABILITYDATETIME,106),103) AS ENDAVAILABILITYDATETIME,
                                        P.DEPTH, P.DEPTHMEASUREMENTUNITCODE, P.HEIGHT,
                                        P.HEIGHTMEASUREMENTUNITCODE, P.WIDTH, P.WIDTHMEASUREMENTUNITCODE, P.NETCONTENT, P.NETCONTENTMEASUREMENTUNITCODE, P.GROSSWEIGHT,
                                        P.GROSSWEIGHTMEASUREMENTUNITCODE, P.NETWEIGHT, P.NETWEIGHTMEASUREMENTUNITCODE, P.PACKAGINGTYPECODE, P.PALLETTYPECODE,
                                        P.ISTRADEITEMANORDERABLEUNIT, P.ISTRADEITEMADESPATCHUNIT, P.ORDERSIZINGFACTOR, P.ORDERQUANTITYMULTIPLE, P.ORDERQUANTITYMINIMUM,
                                        P.QUANTITYOFLAYERSPERPALLET, P.QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER, P.QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM,
                                        P.QUANTITYOFTRADEITEMSPERPALLETLAYER, P.DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM, P.STORAGEHANDLINGTEMPERATUREMAXIMUM,
                                        P.STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE, P.STACKINGFACTOR, P.ISDANGEROUSSUBSTANCEINDICATED,
                                        P.TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM, P.IPIPERC, N.DESCRICAO AS NCMDESCRICAO, P.INDICADORGDSN, P.VARIANTELOGISTICA,
                                        P2.PRODUCTDESCRIPTION AS NOMEPRODUTOORIGEM, U.NOME AS NOMEUSUARIOCRIACAO, U2.NOME AS NOMEUSUARIOALTERACAO, A.NOME AS RAZAOSOCIAL, A.CPFCNPJ, 
                                        P2.GLOBALTRADEITEMNUMBER AS CODIGOGTINORIGEM,
                                        P.CODIGOCEST, C.CEST AS CESTNUMBER, C.DESCRICAO_CEST AS CESTDESCRICAO
                                FROM PRODUTO P WITH (NOLOCK) 
                                LEFT JOIN TIPOPRODUTO TP (NOLOCK)  ON (TP.CODIGO = P.CODIGOTIPOPRODUTO)
	                            LEFT JOIN TRADEITEMUNITDESCRIPTORCODES TIUDC  (NOLOCK) ON (TIUDC.CODIGO = TP.CODIGOTRADEITEMUNITDESCRIPTORCODES)
                                LEFT JOIN LICENCA L (NOLOCK)  ON (L.CODIGO = P.CODIGOTIPOGTIN)
	                            LEFT JOIN LOCALIZACAOFISICA LF (NOLOCK)  ON (LF.GLN = P.INFORMATIONPROVIDER)
                                LEFT JOIN NCM N  (NOLOCK) ON (N.NCM = P.IMPORTCLASSIFICATIONVALUE)
                                LEFT JOIN PRODUTO P2  (NOLOCK) ON (P2.CODIGOPRODUTO = P.CODIGOPRODUTOORIGEM)
                                INNER JOIN TIPOGTIN TG  (NOLOCK) ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                LEFT JOIN USUARIO   U (NOLOCK) ON (U.CODIGO = P.CODIGOUSUARIOCRIACAO)
	                            LEFT JOIN USUARIO U2  (NOLOCK) ON (U2.CODIGO = P.CODIGOUSUARIOALTERACAO)
                                LEFT JOIN ASSOCIADO A (NOLOCK)  ON (A.CODIGO = P.CODIGOASSOCIADO)
                                LEFT JOIN CEST C (NOLOCK)  ON (C.CODIGOCEST = P.CODIGOCEST)
                                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString();

                                //Retira status gtin TRANSFERIDO se não for usuário GS1
                                if (user.id_tipousuario != 1)
                                {
                                    sql += " AND P.CODIGOSTATUSGTIN <> 7 ";
                                }


                string registroPorPagina = "10";
                if (parametro.registroPorPagina > 0)
                    registroPorPagina = parametro.registroPorPagina.ToString();

                string numeropagina = "1";
                if (parametro.paginaAtual > 0)
                    numeropagina = parametro.paginaAtual.ToString();

                string ordenacao = "CodigoProduto ASC";
                if (parametro.ordenacao != null && parametro.ordenacao != string.Empty)
                    ordenacao = parametro.ordenacao;


                sql = @";WITH DADOS AS ( " + sql;
                sql = sql + @")
                        SELECT * 
                        ,TOTALREGISTROS = COUNT(*) OVER()
                        FROM DADOS WITH (NOLOCK)
                        ORDER BY " + ordenacao + @"
                        OFFSET " + registroPorPagina + @" * (" + numeropagina + @"-1) ROWS FETCH NEXT " + registroPorPagina + @" ROWS ONLY";


                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return "";
        }

        [GS1CNPAttribute(false)]
        public string CarregarDropdownPrefixo(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT DISTINCT A.NUMEROPREFIXO AS NOME 
										FROM PREFIXOLICENCAASSOCIADO A
										INNER JOIN TIPOGTINLICENCA B ON B.CodigoLicenca = A.CodigoLicenca
		                            WHERE A.CODIGOASSOCIADO = " + associado.codigo.ToString();

                if (parametro.campos != null && parametro.campos.licenca != null)
                {
                    sql += " AND B.CODIGOTIPOGTIN = " + parametro.campos.licenca;
                }

                sql += @" AND A.CODIGOSTATUSPREFIXO = 1";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        private string VerificarSequenciaPrefixoProduto(string prefixo, int tamanho)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            int codigotipogtin = 0;
            if (tamanho == 13) { codigotipogtin = 3; }
            if (tamanho == 14) { codigotipogtin = 4; }

            if (user != null && associado != null)
            {

                string sql = @"SELECT MAX(CODITEM) AS ULTIMOGLNGERADO
                                FROM PRODUTO P
                                LEFT JOIN ASSOCIADO A ON (A.CODIGO = P.CODIGOASSOCIADO)
                                LEFT JOIN PREFIXOLICENCAASSOCIADO PLU ON (PLU.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                WHERE P.NRPREFIXO = " + prefixo;
                                //" AND P.CODIGOASSOCIADO = " + associado.codigo.ToString();

                if (codigotipogtin > 0)
                {
                    sql += " AND P.CODIGOTIPOGTIN=" + codigotipogtin.ToString();
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public bool ValidaGTIN(string gtin)
        {
            if (gtin != null)
            {

                var digitoCalculo = gtin.Substring(0, gtin.Count() - 1).Reverse().ToArray();
                var verificador = gtin.Substring(gtin.Count() - 1);
                var resultado = 0;

                for (var i = 0; i < digitoCalculo.Count(); i++)
                {

                    if ((i + 1) % 2 == 1)
                    {
                        var resultLocal = (Convert.ToInt32(digitoCalculo[i].ToString()) * 3);
                        resultado += resultLocal;
                    }
                    else
                    {
                        resultado += Convert.ToInt32(digitoCalculo[i].ToString());
                    }
                }

                int verificadorResultado = 10 - (resultado % 10);

                if (verificadorResultado == 10)
                {
                    verificadorResultado = 0;
                }

                if (verificadorResultado == Convert.ToInt32(verificador))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        internal string GeraDigitoVerificador(string gtin)
        {
            if (gtin != null)
            {
                var digitoCalculo = gtin.Reverse().ToArray();
                var resultado = 0;

                for (var i = 0; i < digitoCalculo.Count(); i++)
                {
                    if ((i + 1) % 2 == 1)
                    {
                        resultado += (Convert.ToInt32(digitoCalculo[i].ToString()) * 3);
                    }
                    else
                    {
                        resultado += Convert.ToInt32(digitoCalculo[i].ToString());
                    }
                }

                int verificadorResultado = 10 - (resultado % 10);
                if (verificadorResultado == 10) verificadorResultado = 0;

                return verificadorResultado.ToString();
            }
            else
            {
                return null;
            }
        }

        [GS1CNPAttribute(false)]
        public static int CalculaDV(string gtin)
        {
            if (gtin != null)
            {
                var verificador = gtin.Substring(gtin.Count() - 1);
                var resultado = 0;

                for (var i = 0; i < gtin.Count(); i++)
                {

                    if ((i + 1) % 2 == 1)
                    {
                        var resultLocal = (Convert.ToInt32(gtin[i].ToString()) * 3);
                        resultado += resultLocal;
                    }
                    else
                    {
                        resultado += Convert.ToInt32(gtin[i].ToString());
                    }
                }

                int verificadorResultado = 10 - (resultado % 10);

                if (verificadorResultado == 10)
                {
                    verificadorResultado = 0;
                }

                return verificadorResultado;
            }
            else
            {
                return -1;
            }
        }

        [GS1CNPAttribute("CadastrarProduto", true, true)]
        public object CadastrarProduto(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                using (Database db = new Database("BD"))
                {

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                    parametro.campos.codigoassociado = associado.codigo.ToString();

                    //Verifica se a Descrição do Produto já foi cadastrada
                    //string check = "SELECT COUNT(1) AS QUANTIDADE FROM PRODUTO P WHERE P.PRODUCTDESCRIPTION = @productdescription AND P.CODIGOASSOCIADO = @codigoassociado";

                    db.Open();
                    //dynamic paramCheck = null;

                    //if (parametro.campos != null)
                    //{
                    //    paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    //}

                    //dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    //if (resultCheck.quantidade == 0)
                    //{

                    try
                    {

                        db.Open(true);
                        db.BeginTransaction();

                        if (parametro != null && parametro.campos != null)
                        {

                            //Gerar GTIN
                            if (parametro.campos.tipoFuncionalidade == "GerarGTIN")
                            {
                                dynamic gtinGerado = new ExpandoObject();
                                gtinGerado = ManusearGTIN(parametro);

                                if (gtinGerado != null)
                                {
                                    if (((IDictionary<String, object>)gtinGerado).ContainsKey("variantelogistica")) parametro.campos.variantelogistica = gtinGerado.variantelogistica;
                                    if (((IDictionary<String, object>)gtinGerado).ContainsKey("globaltradeitemnumber")) parametro.campos.globaltradeitemnumber = gtinGerado.globaltradeitemnumber;
                                    if (((IDictionary<String, object>)gtinGerado).ContainsKey("coditem")) parametro.campos.coditem = gtinGerado.coditem;
                                    if (((IDictionary<String, object>)gtinGerado).ContainsKey("nrprefixo")) parametro.campos.nrprefixo = gtinGerado.nrprefixo;
                                }
                            }

                            if (parametro.campos.globaltradeitemnumber != null && !ValidaGTIN(parametro.campos.globaltradeitemnumber.ToString()))
                            {
                                throw new GS1TradeException("Código GTIN inválido!");
                            }

                            parametro.campos.DataInclusao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                            parametro.campos.CodigoUsuarioCriacao = user.id.ToString();

                            //Converte campos data
                            if (parametro.campos.startavailabilitydatetime != null)
                                parametro.campos.startavailabilitydatetime = parametro.campos.startavailabilitydatetime.ToString("MM/dd/yyyy HH:mm:ss.fff");
                            if (parametro.campos.endavailabilitydatetime != null)
                                parametro.campos.endavailabilitydatetime = parametro.campos.endavailabilitydatetime.ToString("MM/dd/yyyy HH:mm:ss.fff");

                            //Data de Alteração
                            parametro.campos.dataalteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");

                            //GTIN Em elaboração
                            if (parametro.campos.globaltradeitemnumber == null)
                                parametro.campos.codigostatusgtin = 5;

                            if (parametro.campos.indicadorcompartilhadados != null)
                                parametro.campos.indicadorcompartilhadados = parametro.campos.indicadorcompartilhadados == "True" ? "1" : "0";
                            else
                                parametro.campos.indicadorcompartilhadados = "1";


                            //if (parametro.campos.indicadorgdsn != null)
                            //    parametro.campos.indicadorgdsn = parametro.campos.indicadorgdsn == true ? "1" : "0";
                            //else
                            //    parametro.campos.indicadorgdsn = "0";


                            Newtonsoft.Json.Linq.JArray imagens = new Newtonsoft.Json.Linq.JArray();
                            Newtonsoft.Json.Linq.JArray urls = new Newtonsoft.Json.Linq.JArray();
                            Newtonsoft.Json.Linq.JArray gtinsinferior = new Newtonsoft.Json.Linq.JArray();
                            Newtonsoft.Json.Linq.JArray atributosbrick = new Newtonsoft.Json.Linq.JArray();
                            Newtonsoft.Json.Linq.JArray agencias = new Newtonsoft.Json.Linq.JArray();

                            dynamic informacoesNutricionais = new Object();
                            dynamic alergenos = new Object();
                            
                            if (parametro.campos.imagens != null) imagens = parametro.campos.imagens;
                            if (parametro.campos.urls != null) urls = parametro.campos.urls;
                            if (parametro.campos.gtinsinferior != null) gtinsinferior = parametro.campos.gtinsinferior;
                            if (parametro.campos.atributosbrick != null) atributosbrick = parametro.campos.atributosbrick;
                            if (parametro.campos.agencias != null) agencias = parametro.campos.agencias;
                            if (parametro.campos.informacoesNutricionais != null) informacoesNutricionais = parametro.campos.informacoesNutricionais;
                            if (parametro.campos.alergenos != null) alergenos = parametro.campos.alergenos;

                            //Itens que não podem ser inseridos
                            parametro.campos.Remove("imagens");
                            parametro.campos.Remove("urls");
                            parametro.campos.Remove("ncmdescricao");
                            parametro.campos.Remove("gtinsinferior");
                            parametro.campos.Remove("nomeprodutoorigem");
                            parametro.campos.Remove("codigogtinorigem");
                            parametro.campos.Remove("atributosbrick");
                            parametro.campos.Remove("tipoFuncionalidade");
                            parametro.campos.Remove("agencias");
                            parametro.campos.Remove("agenciasExcluidas");
                            parametro.campos.Remove("informacoesNutricionais");
                            parametro.campos.Remove("alergenos");
                            parametro.campos.Remove("cestnumber");
                            parametro.campos.Remove("cestdescricao");

                            dynamic paramInsert = null;

                            if (parametro.campos != null)
                                paramInsert = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                            int idInserido = db.Insert(nomeTabela, nomePK, paramInsert, true);

                            dynamic resultado = ((List<dynamic>)db.Select("SELECT * FROM PRODUTO WHERE CODIGOPRODUTO = " + idInserido, null)).FirstOrDefault();

                            //Inserção de imagens
                            if (imagens.Count > 0)
                            {
                                string sqlImagens = InserirImagens(imagens, resultado.codigoproduto.ToString());
                                if (sqlImagens != null && sqlImagens != "")
                                {
                                    object retornoImagens = db.Select(sqlImagens, null);
                                }
                            }

                            //Inserção de URLs
                            if (urls.Count > 0)
                            {
                                string sqlUrls = InserirUrls(urls, resultado.codigoproduto.ToString());
                                if (sqlUrls != null && sqlUrls != "")
                                {
                                    object retornoUrls = db.Select(sqlUrls, null);
                                }
                            }
                            
                            if (agencias.Count > 0)
                            {
                                string sqlAgencias = InserirAgencias(agencias, resultado.codigoproduto.ToString());
                                if (sqlAgencias != null && sqlAgencias != "")
                                {
                                    object retornoAgencias = db.Select(sqlAgencias, null);
                                }
                                
                            }

                            InserirInformacoesNutricionais(db, informacoesNutricionais, resultado.codigoproduto.ToString());
                            InserirAlergenos(db, alergenos, resultado.codigoproduto.ToString());

                            //Inserção de GTIN Inferior
                            if (gtinsinferior.Count > 0)
                            {
                                string retornoGtinInferior = SalvarGTINInferior(gtinsinferior, resultado.codigoproduto.ToString());
                                if (retornoGtinInferior != null && retornoGtinInferior != "")
                                {
                                    object retornoGtins = db.Select(retornoGtinInferior, null);
                                }
                            }

                            //Inserção de Atributos Brick
                            if (atributosbrick.Count > 0)
                            {
                                string retornoAtributosBrick = SalvarAtributosBrick(atributosbrick, resultado.codigoproduto.ToString());
                                if (retornoAtributosBrick != null && retornoAtributosBrick != "")
                                {
                                    object retornoBricks = db.Select(retornoAtributosBrick, null);
                                }
                            }

                            Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, Convert.ToInt64(resultado.codigoproduto));

                            db.Commit();
                            return JsonConvert.SerializeObject(resultado);
                        }

                        return null;
                    }
                    catch (GS1TradeException e)
                    {
                        db.Rollback();
                        throw new GS1TradeException(e.Message.ToString());
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível cadastrar o Produto informado.", e);
                    }
                    //}
                    //else
                    //{
                    //    throw new GS1TradeException("Descrição do Produto já cadastrada.");
                    //}
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar o cadastro.");
            }

        }

        private void InserirInformacoesNutricionais(Database db, dynamic informacoesNutricionais, string codigoproduto)
        {
            if (codigoproduto != null && informacoesNutricionais != null)
            {
                if (informacoesNutricionais.produtoNutrientHeader != null)
                {
                    InserirHeaderInformacoesNutricionais(db, informacoesNutricionais.produtoNutrientHeader, codigoproduto);
                }

                Newtonsoft.Json.Linq.JArray listDetails = new Newtonsoft.Json.Linq.JArray();
                if (informacoesNutricionais.produtoNutrientDetail != null)
                {
                    listDetails = informacoesNutricionais.produtoNutrientDetail;
                    InserirDetailInformacoesNutricionais(db, listDetails, codigoproduto);
                }
            }
        }

        private void InserirHeaderInformacoesNutricionais(Database db, dynamic header, string codigoproduto)
        {
            string delete = @"DELETE FROM ProdutoNutrientHeader WHERE CodigoProduto = " + codigoproduto;
            db.Select(delete, null);

            string insert = @"INSERT INTO ProdutoNutrientHeader (dailyValueIntakeReference, servingSizeDescription, CodigoProduto)
	                          VALUES('" + header.dailyValueIntakeReference + "', '" + header.servingSizeDescription + "', " + codigoproduto + ")";
            db.Select(insert, null);

        }

        private void InserirDetailInformacoesNutricionais(Database db, dynamic details, string codigoproduto)
        {
            string delete = @"DELETE FROM ProdutoNutrientDetail WHERE CodigoProduto = " + codigoproduto;
            db.Select(delete, null);

            if ( details != null)
            {
                foreach (var item in details)
                {
                    string insert = @"INSERT INTO dbo.ProdutoNutrientDetail (CodigoProduto, dailyValueIntakePercent, measurementPrecisionCode, nutrientTypeCode, quantityContained)
                                    VALUES (" + codigoproduto + ", '" + item.dailyValueIntakePercent + "', '" + item.measurementPrecisionCode + "', '" + item.nutrientTypeCode.sigla + "', '" + item.quantityContained + "')";
                    db.Select(insert, null);
                }
            }
        }

        public void InserirAlergenos(Database db, dynamic alergenos, string codigoproduto)
        {
            string deleteRelatedInformation = @"DELETE FROM AllergenRelatedInformation
                                                WHERE Codigo in (
	                                                SELECT PA.CodigoAllergenRelatedInformation
	                                                FROM ProdutoAllergen PA
	                                                WHERE PA.CodigoProduto = " + codigoproduto + ")";
            db.Select(deleteRelatedInformation, null);

            string deleteProdutoAllergen = @"DELETE FROM ProdutoAllergen
	                                                WHERE CodigoProduto = " + codigoproduto;
            db.Select(deleteProdutoAllergen, null);

            if (alergenos != null)
            {
                foreach (var item in alergenos)
                {
                    dynamic paramInsert = new ExpandoObject();
                    paramInsert.CodigoAllergenTypeCode = item.allergenRelatedInformation.allergen.codigo.ToString();
                    paramInsert.allergenSpecificationAgency = item.allergenSpecificationAgency != null && item.allergenSpecificationAgency != "" ? item.allergenSpecificationAgency : null;
                    paramInsert.allergenSpecificationName = item.allergenSpecificationName != null && item.allergenSpecificationName != "" ? item.allergenSpecificationName : null;
                    paramInsert.allergenStatement = item.allergenStatement != null && item.allergenStatement != "" ? item.allergenStatement : null;

                    int idInserido = db.Insert("AllergenRelatedInformation", "CODIGO", paramInsert, true);

                    string insert = @"INSERT INTO ProdutoAllergen (CodigoProduto, CodigoAllergenRelatedInformation, levelOfContainmentCode)
                                    VALUES (" + codigoproduto + ", " + idInserido + ", '" + item.levelOfContainmentCode.codigo.ToString() + "')";
                    db.Select(insert, null);
                }
            }
        }

        [GS1CNPAttribute(false, true)]
        public string InserirImagens(Newtonsoft.Json.Linq.JArray imagens, string codigoproduto)
        {
            string sql = String.Empty;

            if (codigoproduto != null)
            {
                if (imagens != null)
                {
                    for (int i = 0; i < imagens.Count; i++)
                    {
                        sql += "INSERT INTO PRODUTOIMAGEM (URL, CODIGOPRODUTO, STATUS) VALUES ('" + imagens[i].ToString() + "', " + codigoproduto + ", 1); ";
                    }
                }
            }

            return sql;
        }

        [GS1CNPAttribute(false, true)]
        public string InserirUrls(dynamic urls, string codigoproduto)
        {
            string sql = String.Empty;

            if (codigoproduto != null)
            {
                if (urls != null)
                {
                    foreach (var item in urls)
                    {
                        if (item.status == "novo" || item.status == "novo-e-editado")
                        {
                            sql += "INSERT INTO PRODUTOURL (NOME, URL, STATUS, CODIGOPRODUTO, CODIGOTIPOURL) VALUES ('" + item.nome + "', '" + item.url + "', 1, " + codigoproduto + ", " + item.codigotipourl + "); ";
                        }
                        else if (item.status == "editado")
                        {
                            sql += @"INSERT INTO PRODUTOURLHISTORICO (CODIGO, NOME, URL, STATUS, CODIGOPRODUTO, CODIGOTIPOURL, DATAHISTORICO)
	                                                    SELECT CODIGO, NOME, URL, STATUS, CODIGOPRODUTO, CODIGOTIPOURL, GETDATE()
				                                            FROM PRODUTOURL WITH (NOLOCK)
				                                            WHERE PRODUTOURL.CODIGO = " + item.codigo + "; ";

                            sql += "UPDATE PRODUTOURL SET NOME = '" + item.nome + "', URL = '" + item.url + "', CODIGOTIPOURL = " + item.codigotipourl + " WHERE CODIGO = " + item.codigo + "; ";
                        }
                        else if (item.status == "excluido")
                        {
                            sql += "DELETE FROM PRODUTOURL WHERE CODIGO = " + item.codigo + "; ";
                        }
                    }
                }
            }

            return sql;
        }
        
        [GS1CNPAttribute(false, true)]
        public string InserirAgencias(dynamic agencias, string codigoproduto)
        {
            string sql = String.Empty;

            if (codigoproduto != null && agencias != null)
            {
                foreach (var item in agencias)
                {
                    if (item.editar == 1)
                    {
                        sql += @"INSERT INTO ProdutoAgenciaReguladoraHistorico (Codigo, CodigoProduto, CodigoAgencia, AlternateItemIdentificationId, DataHistorico)
                                    SELECT Codigo, CodigoProduto, CodigoAgencia, AlternateItemIdentificationId, GETDATE() FROM ProdutoAgenciaReguladora WITH (NOLOCK)
                                    WHERE Codigo = '"+item.codigo+"'";
                        
                        sql += @"UPDATE ProdutoAgenciaReguladora SET CodigoAgencia = '"+item.alternateitemidentificationagency+"', alternateItemIdentificationId = '"+item.alternateitemidentificationid.ToString()+"' WHERE Codigo = '"+item.codigo+"'; ";
                    }
                    
                    if (item.novo == 1) 
                    {
                        sql += @"INSERT INTO ProdutoAgenciaReguladora (CodigoProduto, CodigoAgencia, alternateItemIdentificationId) 
                            VALUES ('" + codigoproduto + "', '" + item.alternateitemidentificationagency + "', '" + item.alternateitemidentificationid + "'); ";
                    }
                }
            }

            return sql;
        }
        
        [GS1CNPAttribute(false)]
        public string ExcluirAgencias(dynamic agencias)
        {
            string sql = String.Empty;
            
            if (agencias != null)
            {
                foreach (var item in agencias)
                {
                    sql += @"DELETE FROM ProdutoAgenciaReguladora WHERE Codigo = '"+item.codigo+"' ";
                }
            }
               
            return sql;
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutosAutoComplete(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                string sql = @"SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, PRODUCTDESCRIPTION 
                                FROM PRODUTO P WITH (NOLOCK)
                                WHERE (P.GLOBALTRADEITEMNUMBER LIKE '%' + @nome + '%' 
                                OR P.PRODUCTDESCRIPTION LIKE '%' + @nome + '%')
                                AND P.CODIGOASSOCIADO = " + associado.codigo.ToString();


                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    List<dynamic> produtos = db.Select(sql, param);

                    return JsonConvert.SerializeObject(produtos);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public void SalvarStatusLog(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {
                if (parametro != null && parametro.campos != null && parametro.campos.codigoproduto != null)
                {
                    //Salvando modificações em LOG
                    Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, Convert.ToInt64(parametro.campos.codigoproduto));
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível atualizar o LOG.");
            }
        }

        [GS1CNPAttribute(true, true, "EditarProduto", "VisualizarProduto")]
        public object EditarProduto(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            dynamic imagens = new Newtonsoft.Json.Linq.JArray();
            dynamic informacoesNutricionais = new Object();
            dynamic alergenos = new Object();
            Newtonsoft.Json.Linq.JArray urls = new Newtonsoft.Json.Linq.JArray(),
                    gtinsinferior = new Newtonsoft.Json.Linq.JArray(),
                    atributosbrick = new Newtonsoft.Json.Linq.JArray(),
                    agencias = new Newtonsoft.Json.Linq.JArray(),
                    agenciasExcluidas = new Newtonsoft.Json.Linq.JArray();

            string codigo;

            if (user != null && associado != null)
            {

                using (Database db = new Database("BD"))
                {

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                    parametro.campos.codigoassociado = associado.codigo.ToString();

                    db.Open(true);
                    db.BeginTransaction();

                    //Inserção na tabela PRODUTOHISTORICO
                    string sql_historico = @"INSERT INTO PRODUTOHISTORICO (CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, CODIGOPRODUTOORIGEM, CODIGOTIPOGTIN, NRPREFIXO, CODITEM, CODIGOTIPOPRODUTO,
                                                    PRODUCTDESCRIPTION, DATAINCLUSAO, DATASUSPENSAO, DATAREATIVACAO, DATAALTERACAO,
                                                    DATACANCELAMENTO, DATAREUTILIZACAO, CODIGOLINGUA, COUNTRYCODE, ESTADO, INDICADORCOMPARTILHADADOS, OBSERVACOES, CODIGOASSOCIADO, 
                                                    CODIGOSTATUSGTIN, CODIGOUSUARIOCRIACAO, CODIGOUSUARIOALTERACAO, CODIGOUSUARIOEXCLUSAO, CODESEGMENT,
                                                    CODEFAMILY, CODECLASS, CODEBRICK, TRADEITEMCOUNTRYOFORIGIN, INFORMATIONPROVIDER, BRANDNAME, MODELNUMBER, 
                                                    IMPORTCLASSIFICATIONTYPE, IMPORTCLASSIFICATIONVALUE, ISTRADEITEMABASEUNIT, CODEBRICKATTRIBUTE,
                                                    ISTRADEITEMACONSUMERUNIT, ISTRADEITEMAMODEL, ISTRADEITEMANINVOICEUNIT, MINIMUMTRADEITEMLIFESPANFROMTIMEOFPRODUCTION, 
                                                    STARTAVAILABILITYDATETIME, ENDAVAILABILITYDATETIME, DEPTH, DEPTHMEASUREMENTUNITCODE, HEIGHT,
                                                    HEIGHTMEASUREMENTUNITCODE, WIDTH, WIDTHMEASUREMENTUNITCODE, NETCONTENT, NETCONTENTMEASUREMENTUNITCODE, GROSSWEIGHT,
                                                    GROSSWEIGHTMEASUREMENTUNITCODE, NETWEIGHT, NETWEIGHTMEASUREMENTUNITCODE, PACKAGINGTYPECODE, PALLETTYPECODE, 
                                                    ISTRADEITEMANORDERABLEUNIT, ISTRADEITEMADESPATCHUNIT, ORDERSIZINGFACTOR, ORDERQUANTITYMULTIPLE, ORDERQUANTITYMINIMUM,
                                                    QUANTITYOFLAYERSPERPALLET, QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER, QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM,
                                                    QUANTITYOFTRADEITEMSPERPALLETLAYER, DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM, STORAGEHANDLINGTEMPERATUREMAXIMUM,
                                                    STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE, STACKINGFACTOR, ISDANGEROUSSUBSTANCEINDICATED, INDICADORGDSN, VARIANTELOGISTICA, 
                                                    TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM, BARCODECERTIFIED, DATAQUALITYCERTIFIED, DATAQUALITYCERTIFIEDAGENCYCODE,
                                                    ALLGDSNATTRIBUTES, CANALCOMUNICACAODADOS, PROPRIODONOINFORMACAO, VALIDADODONODAINFORMACAO, IPIPERC, STORAGEHANDLINGTEMPMINIMUMUOM, DATAHISTORICO)
	                                        SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, CODIGOPRODUTOORIGEM, CODIGOTIPOGTIN, NRPREFIXO, CODITEM, CODIGOTIPOPRODUTO,
                                                    PRODUCTDESCRIPTION, P.DATAINCLUSAO, P.DATASUSPENSAO, DATAREATIVACAO, DATAALTERACAO,
                                                    P.DATACANCELAMENTO, P.DATAREUTILIZACAO, CODIGOLINGUA, COUNTRYCODE, P.ESTADO, INDICADORCOMPARTILHADADOS, OBSERVACOES, P.CODIGOASSOCIADO, 
                                                    CODIGOSTATUSGTIN, CODIGOUSUARIOCRIACAO, P.CODIGOUSUARIOALTERACAO, CODIGOUSUARIOEXCLUSAO, CODESEGMENT,
                                                    CODEFAMILY, CODECLASS, CODEBRICK, TRADEITEMCOUNTRYOFORIGIN, INFORMATIONPROVIDER, BRANDNAME, MODELNUMBER, 
                                                    IMPORTCLASSIFICATIONTYPE, IMPORTCLASSIFICATIONVALUE, ISTRADEITEMABASEUNIT, CODEBRICKATTRIBUTE,
                                                    ISTRADEITEMACONSUMERUNIT, ISTRADEITEMAMODEL, ISTRADEITEMANINVOICEUNIT, MINIMUMTRADEITEMLIFESPANFROMTIMEOFPRODUCTION, 
                                                    STARTAVAILABILITYDATETIME, ENDAVAILABILITYDATETIME, DEPTH, DEPTHMEASUREMENTUNITCODE, HEIGHT,
                                                    HEIGHTMEASUREMENTUNITCODE, WIDTH, WIDTHMEASUREMENTUNITCODE, NETCONTENT, NETCONTENTMEASUREMENTUNITCODE, GROSSWEIGHT,
                                                    GROSSWEIGHTMEASUREMENTUNITCODE, NETWEIGHT, NETWEIGHTMEASUREMENTUNITCODE, PACKAGINGTYPECODE, PALLETTYPECODE, 
                                                    ISTRADEITEMANORDERABLEUNIT, ISTRADEITEMADESPATCHUNIT, ORDERSIZINGFACTOR, ORDERQUANTITYMULTIPLE, ORDERQUANTITYMINIMUM,
                                                    QUANTITYOFLAYERSPERPALLET, QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER, QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM,
                                                    QUANTITYOFTRADEITEMSPERPALLETLAYER, DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM, STORAGEHANDLINGTEMPERATUREMAXIMUM, 
                                                    STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE, STACKINGFACTOR, ISDANGEROUSSUBSTANCEINDICATED, INDICADORGDSN, VARIANTELOGISTICA, 
                                                    TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM, BARCODECERTIFIED, DATAQUALITYCERTIFIED, DATAQUALITYCERTIFIEDAGENCYCODE,
                                                    ALLGDSNATTRIBUTES, CANALCOMUNICACAODADOS, PROPRIODONOINFORMACAO, VALIDADODONODAINFORMACAO, IPIPERC, STORAGEHANDLINGTEMPMINIMUMUOM, GETDATE()
				                                            FROM PRODUTO P WITH (NOLOCK)
				                                        WHERE P.CODIGOPRODUTO = @codigoproduto";

                    dynamic param1 = null;

                    if (parametro.campos != null)
                        param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql_historico, param1);

                    //Verifica se a DESCRIÇÃO DO PRODUTO já foi cadastrada
                    //string check = "SELECT COUNT(1) AS QUANTIDADE FROM PRODUTO P WHERE P.PRODUCTDESCRIPTION = @productdescription AND P.CODIGOASSOCIADO = @codigoassociado AND P.CODIGOPRODUTO <> @codigoproduto";

                    //dynamic paramCheck = null;

                    //if (parametro.campos != null)
                    //    paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    //dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    //if (resultCheck.quantidade == 0)
                    //{
                    try
                    {

                        //Gerar GTIN
                        if (parametro.campos.tipoFuncionalidade == "GerarGTIN")
                        {
                            dynamic gtinGerado = new ExpandoObject();
                            gtinGerado = ManusearGTIN(parametro);

                            if (gtinGerado != null)
                            {
                                if (((IDictionary<String, object>)gtinGerado).ContainsKey("variantelogistica")) parametro.campos.variantelogistica = gtinGerado.variantelogistica;
                                if (((IDictionary<String, object>)gtinGerado).ContainsKey("globaltradeitemnumber")) parametro.campos.globaltradeitemnumber = gtinGerado.globaltradeitemnumber;
                                if (((IDictionary<String, object>)gtinGerado).ContainsKey("coditem")) parametro.campos.coditem = gtinGerado.coditem;
                                if (((IDictionary<String, object>)gtinGerado).ContainsKey("nrprefixo")) parametro.campos.nrprefixo = gtinGerado.nrprefixo;
                            }
                        }

                        if (parametro.campos.globaltradeitemnumber != null && parametro.campos.globaltradeitemnumber.ToString() != "" && !ValidaGTIN(parametro.campos.globaltradeitemnumber.Value.ToString()))
                        {
                            throw new GS1TradeException("Código GTIN inválido!");
                        }

                        if (parametro.campos.imagens != null && parametro.campos.imagens.ToString() != "") imagens = parametro.campos.imagens;
                        if (parametro.campos.urls != null && parametro.campos.urls.ToString() != "") urls = parametro.campos.urls;
                        if (parametro.campos.gtinsinferior != null && parametro.campos.gtinsinferior.ToString() != "") gtinsinferior = parametro.campos.gtinsinferior;
                        if (parametro.campos.atributosbrick != null && parametro.campos.atributosbrick.ToString() != "") atributosbrick = parametro.campos.atributosbrick;
                        if (parametro.campos.agencias != null && parametro.campos.agencias.ToString() != "") agencias = parametro.campos.agencias;
                        if (parametro.campos.agenciasExcluidas != null && parametro.campos.agenciasExcluidas.ToString() != "") agenciasExcluidas = parametro.campos.agenciasExcluidas;
                        if (parametro.campos.informacoesNutricionais != null) informacoesNutricionais = parametro.campos.informacoesNutricionais;
                        if (parametro.campos.alergenos != null) alergenos = parametro.campos.alergenos;

                        codigo = parametro.campos.codigoproduto;

                        //Itens que não podem ser alterados
                        parametro.campos.Remove("nomeimagem");
                        parametro.campos.Remove("nometipoproduto");
                        parametro.campos.Remove("nometipogtin");
                        parametro.campos.Remove("dataalteracao");
                        parametro.campos.Remove("datainclusao");
                        parametro.campos.Remove("datasuspensao");
                        parametro.campos.Remove("datareativacao");
                        parametro.campos.Remove("datacancelamento");
                        parametro.campos.Remove("datareutilizacao");
                        parametro.campos.Remove("nameofinformationprovider");
                        parametro.campos.Remove("imagens");
                        parametro.campos.Remove("urls");
                        parametro.campos.Remove("ncmdescricao");
                        parametro.campos.Remove("gtininferiordescricao");
                        parametro.campos.Remove("gtinsinferior");
                        parametro.campos.Remove("nomeprodutoorigem");
                        parametro.campos.Remove("atributosbrick");
                        parametro.campos.Remove("nomeusuarioalteracao");
                        parametro.campos.Remove("nomeusuariocriacao");
                        parametro.campos.Remove("razaosocial");
                        parametro.campos.Remove("cpfcnpj");
                        parametro.campos.Remove("codigogtinorigem");
                        parametro.campos.Remove("totalregistros");
                        parametro.campos.Remove("tipoFuncionalidade");
                        parametro.campos.Remove("agencias");
                        parametro.campos.Remove("agenciasExcluidas");
                        parametro.campos.Remove("informacoesNutricionais");
                        parametro.campos.Remove("alergenos");
                        parametro.campos.Remove("cestnumber");
                        parametro.campos.Remove("cestdescricao");

                        if (parametro.campos.startavailabilitydatetime != null && parametro.campos.startavailabilitydatetime != "")
                            parametro.campos.startavailabilitydatetime = parametro.campos.startavailabilitydatetime.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        if (parametro.campos.endavailabilitydatetime != null && parametro.campos.endavailabilitydatetime != "")
                            parametro.campos.endavailabilitydatetime = parametro.campos.endavailabilitydatetime.ToString("MM/dd/yyyy HH:mm:ss.fff");

                        //Caso GTIN-8 não atualiza descrição
                        if (parametro.campos.codigotipogtin == 1)
                            parametro.campos.Remove("productdescription");

                        if (parametro.campos.codigolingua == null)
                            parametro.campos.Remove("codigolingua");

                        //Data de Alteração
                        parametro.campos.dataalteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");

                        //Suspensão de GTIN
                        if (parametro.campos.codigostatusgtin == 2)
                            parametro.campos.datasuspensao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        //Reativação de GTIN
                        if (parametro.campos.codigostatusgtin == 3)
                            parametro.campos.datareativacao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        //Cancelamento de GTIN
                        if (parametro.campos.codigostatusgtin == 4)
                            parametro.campos.datacancelamento = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        //Reutilização de GTIN
                        if (parametro.campos.codigostatusgtin == 6)
                            parametro.campos.datareutilizacao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");

                        if (parametro.campos.indicadorcompartilhadados != null && (parametro.campos.indicadorcompartilhadados == "True" || parametro.campos.indicadorcompartilhadados == 1))
                            parametro.campos.indicadorcompartilhadados = "1";
                        else
                            parametro.campos.indicadorcompartilhadados = "0";

                        //if (parametro.campos.indicadorgdsn != null)
                        //    parametro.campos.indicadorgdsn = parametro.campos.indicadorgdsn == true ? "1" : "0";
                        //else
                        //    parametro.campos.indicadorgdsn = "0";

                        //Inserção de imagens
                        if (imagens.Count > 0)
                        {
                            string sqlImagens = InserirImagens(imagens, codigo);
                            if (sqlImagens != null && sqlImagens != "")
                            {
                                object retornoImagens = db.Select(sqlImagens, null);
                            }
                        }

                        //Inserção de URLs
                        if (urls.Count > 0)
                        {
                            string sqlUrls = InserirUrls(urls, codigo);
                            if (sqlUrls != null && sqlUrls != "")
                            {
                                object retornoUrls = db.Select(sqlUrls, null);
                            }
                        }
                        
                        //Inserção de Agências
                        if (agencias.Count > 0)
                        {
                            string sqlAgencias = InserirAgencias(agencias, codigo);
                            if (sqlAgencias != null && sqlAgencias != "")
                            {
                                object retornoAgencias = db.Select(sqlAgencias, null);
                            }  
                        }
                        
                        //Exclusão de Agências
                        if (agenciasExcluidas.Count > 0)
                        {
                            string sqlAgenciasExcluidas = ExcluirAgencias(agenciasExcluidas);
                            if (sqlAgenciasExcluidas != null && sqlAgenciasExcluidas != "")
                            {
                                object retornoAgenciasExcluidas = db.Select(sqlAgenciasExcluidas, null);
                            }    
                        }

                        InserirInformacoesNutricionais(db, informacoesNutricionais, codigo);
                        InserirAlergenos(db, alergenos, codigo);

                        //Inserção de GTIN Inferior
                        if (gtinsinferior.Count > 0)
                        {
                            string retornoGtinInferior = SalvarGTINInferior(gtinsinferior, codigo);
                            if (retornoGtinInferior != null && retornoGtinInferior != "")
                            {
                                object retornoUrls = db.Select(retornoGtinInferior, null);
                            }
                        }

                        //Inserção de Atributos Brick
                        if (atributosbrick.Count > 0)
                        {
                            string retornoAtributosBrick = SalvarAtributosBrick(atributosbrick, codigo);
                            if (retornoAtributosBrick != null && retornoAtributosBrick != "")
                            {
                                object retornoBricks = db.Select(retornoAtributosBrick, null);
                            }
                        }

                        dynamic paramUpdate = null;

                        if (parametro.campos != null)
                            paramUpdate = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        db.Update(this.nomeTabela, this.nomePK.ToLower(), paramUpdate);
                        db.Commit();
                        return ((List<dynamic>)db.Select("SELECT * FROM PRODUTO WHERE CODIGOPRODUTO = " + codigo, null)).FirstOrDefault();

                    }
                    catch (GS1TradeException e)
                    {
                        db.Rollback();
                        throw new GS1TradeException(e.Message.ToString());
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível cadastrar o Produto informado.", e);
                    }
                    //}
                    //else
                    //{
                    //    db.Rollback();
                    //    throw new GS1TradeException("Descrição do Produto já cadastrada.");
                    //}
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a atualização.");
            }

        }

        [GS1CNPAttribute("ExcluirProduto", true, true)]
        public object RemoverProduto(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    try
                    {
                        string sql = @" DELETE FROM PRODUTOURLHISTORICO WHERE CODIGOPRODUTO = @codigoproduto;
                                        DELETE FROM PRODUTOURL WHERE CODIGOPRODUTO = @codigoproduto;
                                        DELETE FROM PRODUTOIMAGEMHISTORICO WHERE CODIGOPRODUTO = @codigoproduto;
                                        DELETE FROM PRODUTOIMAGEM WHERE CODIGOPRODUTO = @codigoproduto;
                                        DELETE FROM PRODUTOHISTORICO WHERE CODIGOPRODUTO = @codigoproduto;
                                        DELETE FROM PRODUTO WHERE CODIGOPRODUTO = @codigoproduto;";

                        //string sql = "UPDATE PRODUTO SET STATUS = 3 WHERE CODIGOPRODUTO = @codigoproduto";


                        db.Open(true);
                        db.BeginTransaction();

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        db.Commit();
                        return result;
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Exclusão não é permitida. O Produto possui dados vinculados.", e);
                    }
                }
            }
            else
            {
                return "Não foi possível remover o item selecionado.";
            }

        }

        [GS1CNPAttribute(false, true)]
        public void DeletarImagem(dynamic parametro)
        {

            if (parametro != null && parametro.campos != null)
            {
                //Histórico de imagem
                string sql_historico = @"INSERT INTO PRODUTOIMAGEMHISTORICO (CODIGO, URL, CODIGOPRODUTO, STATUS, DATAHISTORICO)
	                                                    SELECT CODIGO, URL, CODIGOPRODUTO, STATUS, GETDATE()
				                                            FROM PRODUTOIMAGEM WITH (NOLOCK)
				                                            WHERE PRODUTOIMAGEM.CODIGO = @codigo";

                string sql = "DELETE FROM PRODUTOIMAGEM WHERE CODIGO = @codigo AND URL = @url";

                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    try
                    {
                        db.Select(sql_historico, param);
                        db.Select(sql, param);

                        //Remover imagem fisicamente
                        if (parametro.campos.url.Value.Contains(System.Configuration.ConfigurationManager.AppSettings["URLBlobAzure"]))
                        {
                            GS1.CNP.BLL.Core.Util.deleteBlobAzure(parametro.campos.url.Value);
                        }
                        else if (parametro.campos.url.Value.Contains("/upload"))
                        {
                            if (File.Exists(HttpContext.Current.Server.MapPath(parametro.campos.url.Value)))
                            {
                                File.Delete(HttpContext.Current.Server.MapPath(parametro.campos.url.Value));
                            }
                        }

                        db.Commit();
                    }
                    catch (Exception)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível excluir a imagem selecionada.");
                    }
                }
            }

        }

        [GS1CNPAttribute(false, true)]
        public void GerarXMLProduto(string codigoproduto)
        {

            string sql = @";WITH DADOS AS ( SELECT P.CODIGOPRODUTO, P.GLOBALTRADEITEMNUMBER, P.CODIGOPRODUTOORIGEM, P.CODIGOTIPOGTIN, P.CODITEM, TP.CODIGO AS CODIGOTIPOPRODUTO, TP.NOME AS NOMETIPOPRODUTO,
                                        P.PRODUCTDESCRIPTION, CONVERT(VARCHAR(10),CONVERT(DATE, P.DATAINCLUSAO,106),103) AS DATAINCLUSAO, CONVERT(VARCHAR(10),CONVERT(DATE,P.DATASUSPENSAO,106),103) AS DATASUSPENSAO,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.DATAREATIVACAO,106),103) AS DATAREATIVACAO, TG.NOME AS NOMETIPOGTIN, CONVERT(VARCHAR(10),CONVERT(DATE, P.DATAALTERACAO,106),103) AS DATAALTERACAO, 
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.DATACANCELAMENTO,106),103) AS DATACANCELAMENTO, CONVERT(VARCHAR(10),CONVERT(DATE,P.DATAREUTILIZACAO,106),103) AS DATAREUTILIZACAO,
                                        P.CODIGOLINGUA, P.COUNTRYCODE, P.ESTADO, CASE P.INDICADORCOMPARTILHADADOS WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS INDICADORCOMPARTILHADADOS, 
                                        P.OBSERVACOES, P.CODIGOASSOCIADO, P.CODIGOSTATUSGTIN, P.CODESEGMENT, P.CODEFAMILY, P.CODECLASS, P.CODEBRICK, P.CODEBRICKATTRIBUTE, P.TRADEITEMCOUNTRYOFORIGIN,
                                        P.INFORMATIONPROVIDER, LF.NOME AS NAMEOFINFORMATIONPROVIDER, P.BRANDNAME, P.MODELNUMBER, PAR.ALTERNATEITEMIDENTIFICATIONID,
                                        P.IMPORTCLASSIFICATIONTYPE, P.IMPORTCLASSIFICATIONVALUE, 
										CASE P.ISTRADEITEMABASEUNIT WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS ISTRADEITEMABASEUNIT,
                                        CASE P.ISTRADEITEMACONSUMERUNIT WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS ISTRADEITEMACONSUMERUNIT, 
										CASE P.ISTRADEITEMAMODEL WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS ISTRADEITEMAMODEL, 
										CASE P.ISTRADEITEMANINVOICEUNIT WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS ISTRADEITEMANINVOICEUNIT, 
										P.MINIMUMTRADEITEMLIFESPANFROMTIMEOFPRODUCTION,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.STARTAVAILABILITYDATETIME,106),103) AS STARTAVAILABILITYDATETIME, 
										CONVERT(VARCHAR(10),CONVERT(DATE,P.ENDAVAILABILITYDATETIME,106),103) AS ENDAVAILABILITYDATETIME,
                                        P.DEPTH, P.DEPTHMEASUREMENTUNITCODE, P.HEIGHT, P.STORAGEHANDLINGTEMPMINIMUMUOM, 
                                        P.HEIGHTMEASUREMENTUNITCODE, P.WIDTH, P.WIDTHMEASUREMENTUNITCODE, P.NETCONTENT, P.NETCONTENTMEASUREMENTUNITCODE, P.GROSSWEIGHT,
                                        P.GROSSWEIGHTMEASUREMENTUNITCODE, P.NETWEIGHT, P.NETWEIGHTMEASUREMENTUNITCODE, P.PACKAGINGTYPECODE, P.PALLETTYPECODE,
                                        CASE P.ISTRADEITEMANORDERABLEUNIT WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS ISTRADEITEMANORDERABLEUNIT, 
										CASE P.ISTRADEITEMADESPATCHUNIT WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS ISTRADEITEMADESPATCHUNIT, 
										P.ORDERSIZINGFACTOR, P.ORDERQUANTITYMULTIPLE, P.ORDERQUANTITYMINIMUM,
                                        P.QUANTITYOFLAYERSPERPALLET, P.QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER, P.QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM,
                                        P.QUANTITYOFTRADEITEMSPERPALLETLAYER, P.DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM, P.STORAGEHANDLINGTEMPERATUREMAXIMUM,
                                        P.STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE, P.STACKINGFACTOR, 
										CASE P.ISDANGEROUSSUBSTANCEINDICATED WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS ISDANGEROUSSUBSTANCEINDICATED,
                                        P.TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM, P.IPIPERC, N.DESCRICAO AS NCMDESCRICAO, 
                                        CASE P.INDICADORGDSN WHEN 1 THEN 'SIM' WHEN 0 THEN 'NÃO' END AS INDICADORGDSN,
                                        P.VARIANTELOGISTICA, P2.PRODUCTDESCRIPTION AS NOMEPRODUTOORIGEM, U.NOME AS NOMEUSUARIOCRIACAO, U2.NOME AS NOMEUSUARIOALTERACAO, 
                                        P2.GLOBALTRADEITEMNUMBER AS CODIGOGTINORIGEM     
                        FROM PRODUTO P WITH (NOLOCK)
                        LEFT JOIN TIPOPRODUTO TP (NOLOCK) ON (TP.CODIGO = P.CODIGOTIPOPRODUTO)
	                    LEFT JOIN TRADEITEMUNITDESCRIPTORCODES TIUDC  (NOLOCK) ON (TIUDC.CODIGO = TP.CODIGOTRADEITEMUNITDESCRIPTORCODES)
                        LEFT JOIN LICENCA L  (NOLOCK) ON (L.CODIGO = P.CODIGOTIPOGTIN)
	                    LEFT JOIN LOCALIZACAOFISICA LF  (NOLOCK) ON (LF.GLN = P.INFORMATIONPROVIDER)
                        LEFT JOIN NCM N (NOLOCK)  ON (N.NCM = P.IMPORTCLASSIFICATIONVALUE)
                        LEFT JOIN PRODUTO P2  (NOLOCK) ON (P2.CODIGOPRODUTO = P.CODIGOPRODUTOORIGEM)
                        INNER JOIN TIPOGTIN TG  (NOLOCK) ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                        LEFT JOIN USUARIO U  (NOLOCK) ON (U.CODIGO = P.CODIGOUSUARIOCRIACAO)
	                    LEFT JOIN USUARIO U2 (NOLOCK)  ON (U2.CODIGO = P.CODIGOUSUARIOALTERACAO)
                        LEFT JOIN PRODUTOAGENCIAREGULADORA PAR (NOLOCK)  ON (PAR.CODIGOPRODUTO = P.CODIGOPRODUTO)
				        WHERE P.CODIGOPRODUTO = " + codigoproduto + @"
				        ) 
			        SELECT  ( SELECT * FROM DADOS 
			        FOR XML RAW ('PRODUTO'), ROOT ('PRODUTOS'), ELEMENTS) AS XMLPRODUTO";

            using (Database db = new Database("BD"))
            {
                db.Open();

                //object retorno = db.Select(sql, null);
                dynamic retorno = ((List<dynamic>)db.Select(sql, null)).FirstOrDefault();

                byte[] bytes = Encoding.UTF8.GetBytes(retorno.xmlproduto);

                if (bytes != null)
                {
                    HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.AddHeader("Content-Type", "text/xml");
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + string.Format("XMLProduto_{0}_{1}{2}", codigoproduto.ToString(), DateTime.Now.ToString("ddMMyyyy_HHmmss"), ".xml"));
                    HttpContext.Current.Response.AddHeader("Content-Length", bytes.Length.ToString());
                    HttpContext.Current.Response.AddHeader("Cache-Control", "max-age=0");
                    HttpContext.Current.Response.AddHeader("Accept-Ranges", "none");
                    HttpContext.Current.Response.BinaryWrite(bytes);

                    HttpContext.Current.Response.Flush();
                }
                else
                {
                    throw new GS1TradeException("Não foi possível gerar o XML do Produto.");
                }
            }
        }

        [GS1CNPAttribute(false)]
        public string PesquisaGTINOrigem(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, PRODUCTDESCRIPTION, TG.NOME AS NOMETIPOGTIN 
                                FROM PRODUTO P WITH (NOLOCK)
                                LEFT JOIN LICENCA L  (NOLOCK) ON (L.CODIGO = P.CODIGOTIPOGTIN)
								INNER JOIN TIPOGTIN TG (NOLOCK)  ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                WHERE P.GLOBALTRADEITEMNUMBER IS NOT NULL
                                AND TG.CODIGO <> 4 
                                AND P.CODIGOSTATUSGTIN <> 7 
                                AND P.CODIGOASSOCIADO = " + associado.codigo.ToString();
                                

                //Retira status gtin CANCELADO se nao for usuário GS1
                if (user.id_tipousuario != 1)
                {
                    sql += " AND P.CODIGOSTATUSGTIN <> 4 ";
                }

                if (parametro.campos != null)
                {

                    if (parametro.campos.codigoproduto != null && parametro.campos.codigoproduto.ToString() != "")
                    {
                        sql += " AND P.CODIGOPRODUTO = @codigoproduto";
                    }

                    if (parametro.campos.globaltradeitemnumber != null && parametro.campos.globaltradeitemnumber.ToString() != "")
                    {
                        sql += " AND P.GLOBALTRADEITEMNUMBER = @globaltradeitemnumber";
                    }

                    if (parametro.campos.productdescription != null && parametro.campos.productdescription != "")
                    {
                        sql += " AND P.PRODUCTDESCRIPTION LIKE '%' + @productdescription + '%'";
                    }

                    if (parametro.campos.endavailabilitydatetime != null && parametro.campos.endavailabilitydatetime != "" && parametro.campos.startavailabilitydatetime != null && parametro.campos.startavailabilitydatetime != "")
                    {
                        parametro.campos.startavailabilitydatetime = DateTime.ParseExact(Convert.ToString(parametro.campos.startavailabilitydatetime.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        parametro.campos.endavailabilitydatetime = DateTime.ParseExact(Convert.ToString(parametro.campos.endavailabilitydatetime.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        sql += @" AND P.DATAINCLUSAO BETWEEN @startavailabilitydatetime AND @endavailabilitydatetime + ' 23:59' ";
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false, true)]
        public string SalvarGTINInferior(dynamic obj, string codigoproduto)
        {

            string sql = String.Empty;

            if (obj != null)
            {
                for (var i = 0; i < obj.Count; i++)
                {
                    if (obj[i].statusinferior == "novo")
                    {
                        //TODO: RETIRAR HARDCODE DE QUANTIDADE
                        sql += @"INSERT INTO PRODUTOHIERARQUIA (CODIGOPRODUTOSUPERIOR, CODIGOPRODUTOINFERIOR, QUANTIDADE) VALUES 
                                    (" + codigoproduto + ", " + obj[i].codigoproduto + ", " + obj[i].quantidade + "); ";
                    }
                    else if (obj[i].statusinferior == "excluido")
                    {
                        sql += @"DELETE FROM PRODUTOHIERARQUIA WHERE CODIGOPRODUTOINFERIOR = " + obj[i].codigoprodutoinferior + "; ";
                    }
                    else
                    {
                        sql += @"UPDATE PRODUTOHIERARQUIA SET QUANTIDADE = " + obj[i].quantidade + " WHERE CODIGOPRODUTOINFERIOR = " + obj[i].codigoprodutoinferior + "; ";
                    }
                }

            }

            return sql;
        }

        [GS1CNPAttribute(false)]
        public string PesquisaGTINInferior(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, PRODUCTDESCRIPTION, TG.NOME AS NOMETIPOGTIN 
                                FROM PRODUTO P WITH (NOLOCK)
                                LEFT JOIN LICENCA L  (NOLOCK) ON (L.CODIGO = P.CODIGOTIPOGTIN)
                                INNER JOIN TIPOGTIN TG (NOLOCK)  ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString() +
                                @" AND P.GLOBALTRADEITEMNUMBER IS NOT NULL 
                                AND P.CODIGOSTATUSGTIN <> 7 ";

                //Retira status gtin CANCELADO se nao for usuário GS1
                if (user.id_tipousuario != 1)
                {
                    sql += " AND P.CODIGOSTATUSGTIN <> 4 ";
                }

                if (parametro.campos != null)
                {

                    if (parametro.campos.codigoprodutosuperior != null && parametro.campos.codigoprodutosuperior.ToString() != "")
                    {
                        sql += " AND P.CODIGOPRODUTO <> @codigoprodutosuperior";
                    }

                    if (parametro.campos.codigoproduto != null && parametro.campos.codigoproduto != "")
                    {
                        sql += " AND CODIGOPRODUTO = @codigoproduto";
                    }

                    if (parametro.campos.globaltradeitemnumber != null && parametro.campos.globaltradeitemnumber != "")
                    {
                        sql += " AND GLOBALTRADEITEMNUMBER = @globaltradeitemnumber";
                    }

                    if (parametro.campos.productdescription != null && parametro.campos.productdescription != "")
                    {
                        sql += " AND PRODUCTDESCRIPTION LIKE '%' + @productdescription + '%'";
                    }

                    if (parametro.campos.endavailabilitydatetime != null && parametro.campos.endavailabilitydatetime != "" && parametro.campos.startavailabilitydatetime != null && parametro.campos.startavailabilitydatetime != "")
                    {
                        parametro.campos.startavailabilitydatetime = DateTime.ParseExact(Convert.ToString(parametro.campos.startavailabilitydatetime.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        parametro.campos.endavailabilitydatetime = DateTime.ParseExact(Convert.ToString(parametro.campos.endavailabilitydatetime.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        sql += @" AND P.DATAINCLUSAO BETWEEN @startavailabilitydatetime AND @endavailabilitydatetime + ' 23:59' ";
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarGTINInferior(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT PH.CODIGOPRODUTOSUPERIOR, PH.CODIGOPRODUTOINFERIOR, PH.QUANTIDADE, P.PRODUCTDESCRIPTION, P.GLOBALTRADEITEMNUMBER 
                                    FROM PRODUTOHIERARQUIA PH (NOLOCK) 
                                    INNER JOIN PRODUTO P (NOLOCK)  ON (P.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR)
                                    LEFT JOIN LICENCA L  (NOLOCK) ON (L.CODIGO = P.CODIGOTIPOGTIN)
                                    WHERE PH.CODIGOPRODUTOSUPERIOR = @codigoprodutosuperior";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisaProdutos(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT P.CODIGOPRODUTO, P.GLOBALTRADEITEMNUMBER, P.CODIGOPRODUTOORIGEM, P.CODIGOTIPOGTIN, P.NRPREFIXO, P.CODITEM, TP.CODIGO AS CODIGOTIPOPRODUTO, TP.NOME AS NOMETIPOPRODUTO,
                                        P.PRODUCTDESCRIPTION, CONVERT(VARCHAR(10),CONVERT(DATE, P.DATAINCLUSAO,106),103) AS DATAINCLUSAO, CONVERT(VARCHAR(10),CONVERT(DATE,P.DATASUSPENSAO,106),103) AS DATASUSPENSAO,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.DATAREATIVACAO,106),103) AS DATAREATIVACAO, TG.NOME AS NOMETIPOGTIN, CONVERT(VARCHAR(10),CONVERT(DATE, P.DATAALTERACAO,106),103) AS DATAALTERACAO,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.DATACANCELAMENTO,106),103) AS DATACANCELAMENTO, CONVERT(VARCHAR(10),CONVERT(DATE,P.DATAREUTILIZACAO,106),103) AS DATAREUTILIZACAO,
                                        P.CODIGOLINGUA, P.COUNTRYCODE, P.ESTADO, P.INDICADORCOMPARTILHADADOS, P.OBSERVACOES, P.CODIGOASSOCIADO,
                                        P.CODIGOSTATUSGTIN, P.CODESEGMENT, P.CODEFAMILY, P.CODECLASS, P.CODEBRICK, P.CODEBRICKATTRIBUTE, P.TRADEITEMCOUNTRYOFORIGIN,
                                        P.INFORMATIONPROVIDER, LF.NOME AS NAMEOFINFORMATIONPROVIDER, P.BRANDNAME, P.MODELNUMBER,                               
                                        P.IMPORTCLASSIFICATIONTYPE, P.IMPORTCLASSIFICATIONVALUE, P.ISTRADEITEMABASEUNIT,
                                        P.ISTRADEITEMACONSUMERUNIT, P.ISTRADEITEMAMODEL, P.ISTRADEITEMANINVOICEUNIT, P.MINIMUMTRADEITEMLIFESPANFROMTIMEOFPRODUCTION,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.STARTAVAILABILITYDATETIME,106),103) AS STARTAVAILABILITYDATETIME, CONVERT(VARCHAR(10),CONVERT(DATE,P.ENDAVAILABILITYDATETIME,106),103) AS ENDAVAILABILITYDATETIME,
                                        P.DEPTH, P.DEPTHMEASUREMENTUNITCODE, P.HEIGHT, P.STORAGEHANDLINGTEMPMINIMUMUOM, 
                                        P.HEIGHTMEASUREMENTUNITCODE, P.WIDTH, P.WIDTHMEASUREMENTUNITCODE, P.NETCONTENT, P.NETCONTENTMEASUREMENTUNITCODE, P.GROSSWEIGHT,
                                        P.GROSSWEIGHTMEASUREMENTUNITCODE, P.NETWEIGHT, P.NETWEIGHTMEASUREMENTUNITCODE, P.PACKAGINGTYPECODE, P.PALLETTYPECODE,
                                        P.ISTRADEITEMANORDERABLEUNIT, P.ISTRADEITEMADESPATCHUNIT, P.ORDERSIZINGFACTOR, P.ORDERQUANTITYMULTIPLE, P.ORDERQUANTITYMINIMUM,
                                        P.QUANTITYOFLAYERSPERPALLET, P.QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER, P.QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM,
                                        P.QUANTITYOFTRADEITEMSPERPALLETLAYER, P.DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM, P.STORAGEHANDLINGTEMPERATUREMAXIMUM,
                                        P.STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE, P.STACKINGFACTOR, P.ISDANGEROUSSUBSTANCEINDICATED,
                                        P.TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM, P.IPIPERC, N.DESCRICAO AS NCMDESCRICAO, P.INDICADORGDSN, P.VARIANTELOGISTICA,
                                        P2.PRODUCTDESCRIPTION AS NOMEPRODUTOORIGEM, U.NOME AS NOMEUSUARIOCRIACAO, U2.NOME AS NOMEUSUARIOALTERACAO, A.NOME AS RAZAOSOCIAL, A.CPFCNPJ,
                                        P2.GLOBALTRADEITEMNUMBER AS CODIGOGTINORIGEM 
                                    FROM PRODUTO P WITH (NOLOCK)
                                    LEFT JOIN TIPOPRODUTO TP ON (TP.CODIGO = P.CODIGOTIPOPRODUTO)
	                                LEFT JOIN TRADEITEMUNITDESCRIPTORCODES TIUDC ON (TIUDC.CODIGO = TP.CODIGOTRADEITEMUNITDESCRIPTORCODES)
                                    LEFT JOIN LICENCA L ON (L.CODIGO = P.CODIGOTIPOGTIN)
	                                LEFT JOIN LOCALIZACAOFISICA LF ON (LF.GLN = P.INFORMATIONPROVIDER)
                                    LEFT JOIN NCM N ON (N.NCM = P.IMPORTCLASSIFICATIONVALUE)
                                    LEFT JOIN PRODUTO P2 ON (P2.CODIGOPRODUTO = P.CODIGOPRODUTOORIGEM)
                                    INNER JOIN TIPOGTIN TG ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                    LEFT JOIN USUARIO U ON (U.CODIGO = P.CODIGOUSUARIOCRIACAO)
	                                LEFT JOIN USUARIO U2 ON (U2.CODIGO = P.CODIGOUSUARIOALTERACAO)
                                    LEFT JOIN ASSOCIADO A ON (A.CODIGO = P.CODIGOASSOCIADO)
                                    WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString();

                //Retira status gtin TRANSFERIDO se não for usuário GS1
                if (user.id_tipousuario != 1)
                {
                    sql += " AND P.CODIGOSTATUSGTIN <> 7 ";
                }

                if (parametro != null && parametro.campos != null)
                {

                    if (parametro.campos.codigoproduto != null && parametro.campos.codigoproduto != "")
                    {
                        sql += " AND P.CODIGOPRODUTO = @codigoproduto";
                    }

                    if (parametro.campos.globaltradeitemnumber != null && parametro.campos.globaltradeitemnumber != "")
                    {
                        sql += " AND P.GLOBALTRADEITEMNUMBER = @globaltradeitemnumber";
                    }

                    if (parametro.campos.codigostatusgtin != null && parametro.campos.codigostatusgtin != "")
                    {
                        sql += " AND P.CODIGOSTATUSGTIN = @codigostatusgtin";
                    }

                    if (parametro.campos.productdescription != null && parametro.campos.productdescription != "")
                    {
                        sql += " AND P.PRODUCTDESCRIPTION LIKE '%' + @productdescription + '%'";
                    }

                    if (parametro.campos.tradeitemunitdescriptor != null && parametro.campos.tradeitemunitdescriptor != "")
                    {
                        sql += " AND P.CODIGOTIPOPRODUTO = @tradeitemunitdescriptor";
                    }

                    if (parametro.campos.brandname != null && parametro.campos.brandname != "")
                    {
                        sql += " AND P.BRANDNAME LIKE '%' + @brandname + '%'";
                    }

                    if (parametro.campos.codigotipogtin != null && parametro.campos.codigotipogtin != "")
                    {
                        sql += " AND P.CODIGOTIPOGTIN = @codigotipogtin";
                    }
                }


                string registroPorPagina = "10";
                if (parametro.registroPorPagina > 0)
                    registroPorPagina = parametro.registroPorPagina.ToString();

                string numeropagina = "1";
                if (parametro.paginaAtual > 0)
                    numeropagina = parametro.paginaAtual.ToString();

                string ordenacao = "1 ASC";
                if (parametro.ordenacao != string.Empty)
                    ordenacao = parametro.ordenacao;


                sql = @";WITH DADOS AS ( " + sql;
                sql = sql + @")
                        SELECT * 
                        ,TOTALREGISTROS = COUNT(*) OVER()
                        FROM DADOS WITH (NOLOCK)
                        ORDER BY " + ordenacao + @"
                        OFFSET " + registroPorPagina + @" * (" + numeropagina + @"-1) ROWS FETCH NEXT " + registroPorPagina + @" ROWS ONLY";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    List<dynamic> fornecedor = db.Select(sql, param);

                    return JsonConvert.SerializeObject(fornecedor);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarImagensProduto(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT CODIGO, URL 
		                        FROM PRODUTOIMAGEM PI WITH (NOLOCK)
		                        WHERE CODIGOPRODUTO = @codigoproduto
		                        AND STATUS = 1";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return "";
        }


        [GS1CNPAttribute(false)]
        public string BuscarImagensProdutoPreview(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @" SELECT CODIGO, URL 
	                                FROM PRODUTOURL WITH (NOLOCK)
	                                WHERE CODIGOPRODUTO = @codigoproduto
	                                AND STATUS = 1
	                                AND CODIGOTIPOURL = (SELECT CODIGO FROM TIPOURL WHERE NOME = 'Foto')
                                UNION ALL
                                SELECT CODIGO, URL 
		                            FROM PRODUTOIMAGEM PI WITH (NOLOCK)
		                            WHERE CODIGOPRODUTO = @codigoproduto
		                            AND STATUS = 1";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return "";
        }
        [GS1CNPAttribute(false)]
        public string BuscarURLsProduto(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT PU.CODIGO, PU.NOME, PU.URL, PU.CODIGOTIPOURL, T.NOME AS TIPOURL
		                        FROM PRODUTOURL PU (NOLOCK) 
			                    INNER JOIN TIPOURL T ON (T.CODIGO = PU.CODIGOTIPOURL)
		                        WHERE PU.STATUS = 1
                                AND CODIGOPRODUTO = @codigoproduto";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return "";
        }
        
        [GS1CNPAttribute(false)]
        public string BuscarAgenciasReguladoras(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT p.Codigo,
	                                a.Nome as NomeAgencia,
                                    a.codigo as alternateitemidentificationagency, 
	                                p.alternateItemIdentificationId 
                                FROM ProdutoAgenciaReguladora p
                                INNER JOIN AgenciaReguladora a ON a.Codigo = p.CodigoAgencia
		                        WHERE a.STATUS = 1
                                AND p.CodigoProduto = @codigoproduto";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return "";
        }

        [GS1CNPAttribute(false)]
        public string BuscarTodosAtributosBrick(dynamic parametro)
        {

            object user = this.getSession("USUARIO_LOGADO");
            object associado = this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                if (parametro != null && parametro.campos != null && parametro.campos.codebrick != null &&
                    parametro.campos.codeclass != null && parametro.campos.codefamily != null && parametro.campos.codesegment != null)
                {
                    string sql = @";WITH DADOS AS (
	                                SELECT P.CODETYPE, B.TEXT, B.IDIOMA, P.CODEBRICK, P.CODECLASS, P.CODEFAMILY, P.CODESEGMENT
		                                FROM PRODUTOBRICKTYPEVALUE P
		                                LEFT JOIN TBTTYPE B ON B.CODETYPE = P.CODETYPE
		                                WHERE P.CODEBRICK = @codebrick AND P.CODECLASS = @codeclass AND P.CODEFAMILY = @codefamily AND P.CODESEGMENT = @codesegment ";

                    if (parametro.campos.codigoproduto != null)
                    {
                        sql += " AND (P.CODIGOPRODUTO = @codigoproduto OR @codigoproduto IS NULL) ";
                    }
                    else
                    {
                        sql += " AND (P.CODIGOPRODUTO = NULL OR NULL IS NULL) ";
                    }

                    sql += @" UNION

	                                SELECT B.CODETYPE, B.TEXT, B.IDIOMA, A.CODEBRICK, A.CODECLASS, A.CODEFAMILY, A.CODESEGMENT
		                                FROM TBBRICKTYPE A
		                                INNER JOIN TBTTYPE B ON B.CODETYPE = A.CODETYPE
		                                WHERE A.CODEBRICK = @codebrick AND A.CODECLASS = @codeclass AND A.CODEFAMILY = @codefamily AND A.CODESEGMENT = @codesegment
                                )
                                SELECT P.CODIGO AS CODIGOPRODUTOBRICKTYPEVALUE, A.CODETYPE, A.TEXT, A.IDIOMA, A.CODEBRICK, A.CODECLASS, A.CODEFAMILY, A.CODESEGMENT 
                                    ,(SELECT TOP 1 P.CODEVALUE 
			                                FROM PRODUTOBRICKTYPEVALUE P 
			                                WHERE P.CODEBRICK = A.CODEBRICK AND P.CODECLASS = A.CODECLASS AND P.CODEFAMILY = A.CODEFAMILY AND P.CODESEGMENT = A.CODESEGMENT ";

                    if (parametro.campos.codigoproduto != null)
                    {
                        sql += " AND (P.CODIGOPRODUTO = @codigoproduto) ";
                    }
                    else
                    {
                        sql += " AND (P.CODIGOPRODUTO = NULL) ";
                    }

                    sql += @" AND P.CODETYPE = A.CODETYPE
	                                ) AS CODEVALUE
                                FROM DADOS A
                                LEFT JOIN PRODUTOBRICKTYPEVALUE P ON P.CODETYPE = A.CODETYPE AND P.CODEBRICK = A.CODEBRICK AND P.CODECLASS = A.CODECLASS 
													AND P.CODEFAMILY = A.CODEFAMILY AND P.CODESEGMENT = A.CODESEGMENT ";

                    if (parametro.campos.codigoproduto != null)
                    {
                        sql += " AND (P.CODIGOPRODUTO = @codigoproduto) ";
                    }
                    else
                    {
                        sql += " AND (P.CODIGOPRODUTO = NULL) ";
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();
                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object retorno = db.Select(sql, param);
                        return JsonConvert.SerializeObject(retorno);
                    }
                }
            }

            return null;
        }

        [GS1CNPAttribute(false)]
        public string BuscarDropdownAtributoBrick(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT D.CODEVALUE, D.TEXT FROM TBBRICKTYPE A
                                INNER JOIN TBTTYPE B ON B.CODETYPE = A.CODETYPE
                                INNER JOIN TBTYPEVALUE C ON C.CODETYPE = B.CODETYPE AND C.CODEBRICK = A.CODEBRICK
                                INNER JOIN TBTVALUE D ON D.CODEVALUE = C.CODEVALUE
                                WHERE A.CODEBRICK = @codebrick AND A.CODECLASS = @codeclass AND A.CODEFAMILY = @codefamily AND A.CODESEGMENT = @codesegment 
                                ORDER BY TEXT ";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }

            return null;
        }

        [GS1CNPAttribute(false, true)]
        public string SalvarAtributosBrick(dynamic obj, string codigoproduto)
        {

            string sql = String.Empty;

            if (obj != null)
            {
                for (var i = 0; i < obj.Count; i++)
                {
                    if ((obj[i].codigoprodutobricktypevalue == null || obj[i].codigoprodutobricktypevalue.ToString() == "") && obj[i].status != null && obj[i].status.ToString() == "novo")
                    {
                        sql += @"INSERT INTO PRODUTOBRICKTYPEVALUE (CODIGOPRODUTO, CODEBRICK, CODECLASS, CODEFAMILY, CODESEGMENT, IDIOMA, CODETYPE, CODEVALUE)
                                VALUES (" + codigoproduto + ", " + obj[i].codebrick + ", " + obj[i].codeclass + ", "
                                          + obj[i].codefamily + ", " + obj[i].codesegment + ", '" + obj[i].idioma + "', "
                                          + obj[i].codetype + ", " + obj[i].codevalue + "); ";
                    }
                    else if (obj[i].codigoprodutobricktypevalue != null && obj[i].codigoprodutobricktypevalue.ToString() != "" && obj[i].status != null && obj[i].status.ToString() == "editado")
                    {
                        sql += @"UPDATE PRODUTOBRICKTYPEVALUE SET CODEBRICK = " + obj[i].codebrick + ", CODECLASS = " + obj[i].codeclass +
                                ", CODEFAMILY = " + obj[i].codefamily + ", CODESEGMENT = " + obj[i].codesegment +
                                ", IDIOMA = '" + obj[i].idioma + "', CODETYPE = " + obj[i].codetype + ", CODEVALUE = " + obj[i].codevalue +
                                " WHERE CODIGO = " + obj[i].codigoprodutobricktypevalue + "; ";
                    }
                    else if (obj[i].codigoprodutobricktypevalue != null && obj[i].codigoprodutobricktypevalue.ToString() != "" && obj[i].status != null && obj[i].status.ToString() == "excluido")
                    {
                        sql += @"DELETE FROM PRODUTOBRICKTYPEVALUE WHERE CODIGO = " + obj[i].codigoprodutobricktypevalue + "; ";
                    }
                }

            }

            return sql;
        }

        internal string RetornarGTINProdutoOrigem(string codigogtinorigem)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT CODIGOPRODUTO, GLOBALTRADEITEMNUMBER, NRPREFIXO, CODITEM, PRODUCTDESCRIPTION, TG.NOME AS NOMETIPOGTIN, TG.CODIGO AS CODIGOTIPOGTIN  
                                FROM PRODUTO P WITH (NOLOCK)
                                LEFT JOIN LICENCA L (NOLOCK)  ON (L.CODIGO = P.CODIGOTIPOGTIN)
								INNER JOIN TIPOGTIN TG  (NOLOCK) ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                WHERE P.GLOBALTRADEITEMNUMBER IS NOT NULL
                                AND P.CODIGOASSOCIADO = " + associado.codigo.ToString() +
                                " AND P.GLOBALTRADEITEMNUMBER = " + codigogtinorigem;

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        internal string RetornarProximaVariante(string codigoprodutoorigem)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT TOP 1 CODIGO FROM (
                                    SELECT 1 AS CODIGO
                                    UNION SELECT 2 AS CODIGO
                                    UNION SELECT 3 AS CODIGO
                                    UNION SELECT 4 AS CODIGO
                                    UNION SELECT 5 AS CODIGO
                                    UNION SELECT 6 AS CODIGO
                                    UNION SELECT 7 AS CODIGO
                                    UNION SELECT 8 AS CODIGO
                                    UNION SELECT 9 AS CODIGO
                                ) AS DADOS
                                WHERE DADOS.CODIGO NOT IN (
                                    SELECT SUBSTRING(CONVERT(VARCHAR(30),B.GLOBALTRADEITEMNUMBER),1,1)
                                    FROM PRODUTO B
                                    WHERE B.CODIGOPRODUTOORIGEM = " + codigoprodutoorigem +
                                    @" AND B.GLOBALTRADEITEMNUMBER IS NOT NULL
                                )
                                ORDER BY CODIGO ASC";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    object retorno = db.Select(sql, null);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public bool VerificaExistenciaGTIN(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                //Verifica se o GTIN informado existe
                string check = @"SELECT 1 AS EXIST 
                                    FROM PRODUTO
                                    WHERE GLOBALTRADEITEMNUMBER = @globaltradeitemnumber";

                db.Open();
                dynamic resultCheck = ((List<dynamic>)db.Select(check, parametro.campos)).FirstOrDefault();
                return resultCheck != null && resultCheck.exist == 1 ? true : false;
            }
        }


        [GS1CNPAttribute(false)]
        public object VerificaExistenciaGTINModulo(long GTIN, string modulo)
        {
            using (Database db = new Database("BD"))
            {
                //Verifica se o GTIN informado existe
                string check = @"SELECT A.CODIGOPRODUTO, B.CODIGOPRODUTO CODIGOPRODUTOMODULO  
                                    FROM PRODUTO A
                                    LEFT JOIN " + modulo + @" B ON B.CODIGOPRODUTO = A.CODIGOPRODUTO
                                    WHERE GLOBALTRADEITEMNUMBER = " + GTIN.ToString();

               
                db.Open();

                dynamic retorno = db.Select(check, null);
                if (retorno != null && retorno.Count > 0)
                {
                    return JsonConvert.SerializeObject(retorno);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }

            }
        }


        [GS1CNPAttribute(false)]
        public object VerificaExistenciaGTIN14Superior(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                //Verifica se a Descrição do Produto já foi cadastrada
                string select = @"  SELECT P.CodigoProduto
                                    FROM Produto P
                                    WHERE P.CodigoProdutoOrigem = @codigoProduto
                                        AND P.CodigoTipoGTIN = 4";

                db.Open();
                dynamic paramCheck = null;

                if (parametro.where != null)
                    paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                return db.Select(select, paramCheck);
            }

        }

        [GS1CNPAttribute(false)]
        public object PegarGTINValido(string codigotipogtin)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                //Verifica se a Descrição do Produto já foi cadastrada
                string check = @"SELECT NUMEROPREFIXO FROM (
	                                SELECT MIN(NUMEROPREFIXO) AS NUMEROPREFIXO
	                                FROM PREFIXOLICENCAASSOCIADO PLA
	                                INNER JOIN LICENCA L ON L.CODIGO = PLA.CODIGOLICENCA
	                                INNER JOIN TipoGTINLicenca TGL ON TGL.CodigoLicenca = L.Codigo
	                                WHERE TGL.CODIGOTIPOGTIN <> 1 
	                                AND TGL.CODIGOTIPOGTIN = " + codigotipogtin +
                                    @" AND PLA.CODIGOSTATUSPREFIXO IN (1, 8) 
	                                AND PLA.CODIGOASSOCIADO = " + associado.codigo.ToString() + @"
                                    AND (
								                (TGL.CODIGOTIPOGTIN = 2 
									                AND								
									                (CASE LEN(NUMEROPREFIXO)	WHEN 6  THEN 100000
																                WHEN 7  THEN 10000
																                WHEN 8  THEN 1000
																                WHEN 9  THEN 100
																                WHEN 10 THEN 10
																                ELSE 0 END > (SELECT COALESCE(MAX(X.CODITEM),0) FROM PRODUTO X WHERE X.NRPREFIXO = PLA.NUMEROPREFIXO
                                                                                              AND X.CODIGOTIPOGTIN = 2)))
								                OR

								                (TGL.CODIGOTIPOGTIN <> 2 AND 
										                (CASE LEN(NUMEROPREFIXO)	WHEN 7  THEN 99999
																                WHEN 8  THEN 9999
																                WHEN 9  THEN 999
																                WHEN 10 THEN 99
																                WHEN 11 THEN 9
																                ELSE 0 END > (SELECT COALESCE(MAX(X.CODITEM),0) FROM PRODUTO X WHERE X.NRPREFIXO = PLA.NUMEROPREFIXO
                                                                                              AND X.CODIGOTIPOGTIN = TGL.CODIGOTIPOGTIN)))
								
						                )";

                check += @"UNION 

	                                SELECT MIN(NUMEROPREFIXO) AS NUMEROPREFIXO
	                                FROM PREFIXOLICENCAASSOCIADO PLA
	                                INNER JOIN LICENCA L ON L.CODIGO = PLA.CODIGOLICENCA
	                                INNER JOIN TipoGTINLicenca TGL ON TGL.CodigoLicenca = L.Codigo
	                                WHERE TGL.CODIGOTIPOGTIN = 1 
	                                AND NUMEROPREFIXO NOT IN (SELECT Z.GLOBALTRADEITEMNUMBER FROM Produto Z WHERE Z.CODIGOTIPOGTIN = 1 AND Z.GLOBALTRADEITEMNUMBER IS NOT NULL)
	                                AND TGL.CODIGOTIPOGTIN = " + codigotipogtin + 
                                    @" AND PLA.CODIGOSTATUSPREFIXO IN (1, 8) 
	                                AND PLA.CODIGOASSOCIADO = " + associado.codigo.ToString();
                check += @")AS DADOS
                                WHERE NUMEROPREFIXO IS NOT NULL";

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic retorno = db.Select(check, null);
                    if (retorno != null && retorno.Count > 0)
                    {
                        return JsonConvert.SerializeObject(retorno);
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(null);
                    }
                }

            }

            return null;

        }

        [GS1CNPAttribute(false)]
        public string BuscarGTIN8NaoUsado(dynamic parametro)
        {

            Login user = (Login)this.getSession("USUARIO_LOGADO");
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            if (user != null && associado != null)
            {

                string sql = @"SELECT PLA.CODIGOLICENCA, PLA.CODIGOASSOCIADO, PLA.NUMEROPREFIXO, PLA.DATAALTERACAO 
                                FROM PREFIXOLICENCAASSOCIADO PLA (NOLOCK) 
                                WHERE PLA.CODIGOLICENCA = 2
                                AND PLA.CODIGOASSOCIADO = " + associado.codigo.ToString() + @"
                                AND PLA.CODIGOSTATUSPREFIXO = 1
                                AND PLA.NUMEROPREFIXO NOT IN (SELECT GLOBALTRADEITEMNUMBER FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = PLA.NUMEROPREFIXO)";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível buscar os dados.");
            }
        }

        [GS1CNPAttribute(false)]
        public void Exportar(dynamic parametro)
        {
            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                if (parametro != null && associado != null)
                {
                    dynamic param = ConvertJTokenToObject((JToken)parametro.campos);

                    dynamic dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Exportacao.xml"));

                    string sql = @"SELECT {0}
                               FROM PRODUTO P WITH (NOLOCK)
                               WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString(),
                           sql_qtdeurl = @"SELECT ISNULL(MAX(TOTAL), 0) AS TOTAL
                                       FROM (SELECT COUNT(0) AS TOTAL
                                             FROM PRODUTOURL WITH (NOLOCK)
                                             WHERE CODIGOPRODUTO IN (" + string.Format(sql, "CODIGOPRODUTO") + @")
                                             GROUP BY CODIGOPRODUTO) AS T",
                         sql_qtdeagencias = @"SELECT ISNULL(MAX(TOTAL), 0) AS TOTAL
                                       FROM (SELECT COUNT(0) AS TOTAL
                                             FROM PRODUTOAGENCIAREGULADORA WITH (NOLOCK)
                                             WHERE CODIGOPRODUTO IN (" + string.Format(sql, "CODIGOPRODUTO") + @")
                                             GROUP BY CODIGOPRODUTO) AS T";
                        

                    string tipoArquivo = string.Empty;

                    if (parametro != null && parametro.where != null)
                        tipoArquivo = parametro.where.tipoarquivo.ToString().ToLower();

                    if (user.id_tipousuario == 2)
                        sql += " AND P.CODIGOSTATUSGTIN <> 7";

                    if (param != null)
                    {
                        if (((IDictionary<string, object>)param).ContainsKey("codigoproduto") && param.codigoproduto != null && !string.IsNullOrEmpty(param.codigoproduto.ToString()))
                            sql += " AND P.CODIGOPRODUTO = " + param.codigoproduto.ToString();
                        if (((IDictionary<string, object>)param).ContainsKey("globaltradeitemnumber") && param.globaltradeitemnumber != null && !string.IsNullOrEmpty(param.globaltradeitemnumber.ToString()))
                            sql += " AND P.GLOBALTRADEITEMNUMBER = " + param.globaltradeitemnumber.ToString();
                        if (((IDictionary<string, object>)param).ContainsKey("codigostatusgtin") && param.codigostatusgtin != null && !string.IsNullOrEmpty(param.codigostatusgtin.ToString()))
                            sql += " AND P.CODIGOSTATUSGTIN = " + param.codigostatusgtin.ToString();
                        if (((IDictionary<string, object>)param).ContainsKey("productdescription") && param.productdescription != null && !string.IsNullOrEmpty(param.productdescription.ToString()))
                            sql += " AND P.PRODUCTDESCRIPTION LIKE '%" + param.productdescription.ToString() + "%'";
                        if (((IDictionary<string, object>)param).ContainsKey("tradeitemunitdescriptor") && param.tradeitemunitdescriptor != null && !string.IsNullOrEmpty(param.tradeitemunitdescriptor.ToString()))
                            sql += " AND P.CODIGOTIPOPRODUTO = " + param.tradeitemunitdescriptor.ToString();
                        if (((IDictionary<string, object>)param).ContainsKey("brandname") && param.brandname != null && !string.IsNullOrEmpty(param.brandname.ToString()))
                            sql += " AND P.BRANDNAME LIKE '%" + param.brandname.ToString() + "%'";
                        if (((IDictionary<string, object>)param).ContainsKey("codigotipogtin") && param.codigotipogtin != null && !string.IsNullOrEmpty(param.codigotipogtin.ToString()))
                            sql += " AND P.CODIGOTIPOGTIN = " + param.codigotipogtin.ToString();
                    }

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        List<dynamic> result = db.Select(string.Format(sql, "*"), parametro);

                        dynamic resultQtdeURL = (db.Select(sql_qtdeurl, parametro) as List<dynamic>).FirstOrDefault();
                        dynamic resultQtdeAgencias = (db.Select(sql_qtdeagencias, parametro) as List<dynamic>).FirstOrDefault();

                        int qtdeURL = 0, qtdeAgencias = 0;

                        if (resultQtdeURL != null)
                            qtdeURL = Convert.ToInt32(resultQtdeURL.total);

                        if (resultQtdeAgencias != null)
                            qtdeAgencias = Convert.ToInt32(resultQtdeAgencias.total);

                        string path = HttpContext.Current.Server.MapPath(@"\Export"),
                            fileformat = @"Export_produtos_gs1_{0}." + (!string.IsNullOrEmpty(tipoArquivo) && tipoArquivo.Equals("xlsx") ? "xlsx" : "csv"),
                            arquivo = Path.Combine(path, string.Format(fileformat, DateTime.Now.ToString("ddMMyyyy_HHmmss"))),
                            contentType = string.Empty;

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        byte[] bytes = null;

                        if (!string.IsNullOrEmpty(tipoArquivo) && tipoArquivo.Equals("xlsx"))
                        {
                            contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                            using (ExcelPackage pck = new ExcelPackage())
                            {
                                var ws = pck.Workbook.Worksheets.Add("Content");

                                //AdicionarCabecalhoXLSX(ws, dic.column as List<DynamicXml>, qtdeURL);

                                AdicionarValoresXLSX(db, ws, dic.column as List<DynamicXml>, result, qtdeURL, qtdeAgencias, arquivo);

                                bytes = pck.GetAsByteArray();
                            }
                        }
                        else
                        {
                            contentType = "text/csv";

                            StringBuilder csv = new StringBuilder(10 * result.Count * (dic.column as List<DynamicXml>).Count);

                            //AdicionarCabecalhoCSV(csv, dic.column as List<DynamicXml>, qtdeURL);

                            AdicionarValoresCSV(db, csv, dic.column as List<DynamicXml>, result, qtdeURL, qtdeAgencias, arquivo);

                            var data = Encoding.UTF8.GetBytes(csv.ToString());
                            bytes = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
                        }

                        if (bytes != null && bytes.Length > 0)
                        {
                            HttpContext.Current.Response.SetCookie(new HttpCookie("fileDownload", "true") { Path = "/" });

                            HttpContext.Current.Response.Clear();
                            HttpContext.Current.Response.AddHeader("Content-Type", contentType);
                            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + new FileInfo(arquivo).Name);
                            HttpContext.Current.Response.AddHeader("Content-Length", bytes.Length.ToString());
                            HttpContext.Current.Response.AddHeader("Cache-Control", "max-age=0");
                            HttpContext.Current.Response.AddHeader("Accept-Ranges", "none");
                            HttpContext.Current.Response.BinaryWrite(bytes);

                            HttpContext.Current.Response.Flush();
                            HttpContext.Current.Response.End();
                        }
                    }
                }
            }
            else
                throw new GS1TradeSessionException();
        }

        private void AdicionarCabecalhoCSV(StringBuilder csv, List<DynamicXml> columns, int qtdeURL, int qtdeAgencia)
        {
            if (csv != null)
            {
                int idx = 1;

                csv.Append("Tipo do GTIN");

                foreach (dynamic column in columns)
                {
                    if (column.coluna.ToLower().Equals("url"))
                    {
                        for (int i = 1; i <= qtdeURL; i++)
                        {
                            idx++;
                            csv.Append(";Nome URL " + i.ToString());

                            idx++;
                            csv.Append(";URL " + i.ToString());

                            idx++;
                            csv.Append(";Tipo de URL " + i.ToString());
                        }
                    }
                    else
                    {
                        idx++;
                        csv.Append(";" + column.coluna);
                    }
                }

                csv.Append(Environment.NewLine);
            }
        }

        private void AdicionarValoresCSV(Database db, StringBuilder csv, List<DynamicXml> columns, List<dynamic> result, int qtdeURL, int qtdeAgencia, string fileName)
        {

            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            #region SQL
            string sqlQuery = @"SELECT TG.NOME AS 'Tipo do GTIN', CASE P.INDICADORGDSN WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'GDSN?', 
	                                SG.NOME AS 'Status do GTIN', P2.GLOBALTRADEITEMNUMBER AS 'GTIN Origem', P.TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM AS 'Quantidade',
                                    P.GLOBALTRADEITEMNUMBER AS GTIN, P.BRANDNAME AS 'Marca',
	                                P.MODELNUMBER AS 'Número do Modelo', P.PRODUCTDESCRIPTION AS 'Descrição do Produto', 
	                                CONVERT(VARCHAR(10),CONVERT(DATE,P.STARTAVAILABILITYDATETIME,106),103) AS 'Data Inicial de Disponibilidade', 
	                                CONVERT(VARCHAR(10),CONVERT(DATE,P.ENDAVAILABILITYDATETIME,106),103) AS 'Data Final de Disponibilidade', N.DESCRICAO AS NCM,
	                                P.CODESEGMENT + ' - ' + TBS.TEXT AS 'Segmento', P.CODEFAMILY + ' - ' + TBF.TEXT AS 'Família', P.CODECLASS + ' - ' + TBC.TEXT AS 'Classe', P.CODEBRICK + ' - ' + TBB.TEXT AS 'Brick',
                                    
                                    SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, PAG.ALTERNATEITEMIDENTIFICATIONID) + ',//,'
				                                FROM PRODUTOAGENCIAREGULADORA PAG
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PAG.CODIGOPRODUTO
				                                WHERE PAG.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, PAG.ALTERNATEITEMIDENTIFICATIONID) + ',//,'
				                                FROM PRODUTOAGENCIAREGULADORA PAG
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PAG.CODIGOPRODUTO
				                                WHERE PAG.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'Número de Identificação Alternativa',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, AR.NOME) + ',//,'
				                                FROM PRODUTOAGENCIAREGULADORA PAG
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PAG.CODIGOPRODUTO
                                                INNER JOIN AGENCIAREGULADORA AR ON AR.CODIGO = PAG.CODIGOAGENCIA
				                                WHERE PAG.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, AR.NOME) + ',//,'
				                                FROM PRODUTOAGENCIAREGULADORA PAG
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PAG.CODIGOPRODUTO
                                                INNER JOIN AGENCIAREGULADORA AR ON AR.CODIGO = PAG.CODIGOAGENCIA
				                                WHERE PAG.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'Agência Reguladora',
                                    
	                                (SELECT TOP 1 C.NOME FROM COUNTRYOFORIGINISO3166 C WHERE C.CODIGOPAIS = P.COUNTRYCODE) AS 'País/Mercado de Destino', 
	                                (SELECT TOP 1 C.NOME FROM COUNTRYOFORIGINISO3166 C WHERE C.CODIGOPAIS = P.TRADEITEMCOUNTRYOFORIGIN) AS 'País de Origem', 
	                                P.ESTADO AS 'Estado', P.HEIGHT AS 'Altura', UC.SIGLA AS 'Altura - UN Medida',
	                                P.WIDTH AS 'Largura', UC1.SIGLA AS 'Largura - UN Medida', 
	                                P.DEPTH AS 'Profundidade', UC2.SIGLA AS 'Profundidade - UN Medida',
	                                P.NETCONTENT AS 'Conteúdo Líquido', UC3.SIGLA AS 'Cont. Liq. - UN Medida', 
	                                P.GROSSWEIGHT AS 'Peso Bruto', UC4.SIGLA AS 'Peso Bruto - UN Medida', 
	                                P.NETWEIGHT AS 'Peso Líquido', UC5.SIGLA AS 'Peso Líq. - UN Medida',
	                                P.MINIMUMTRADEITEMLIFESPANFROMTIMEOFPRODUCTION AS 'Tempo mínimo (dias) de vida útil do produto após produção',
	                                CASE P.INDICADORCOMPARTILHADADOS WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Compartilha Dados?',
	                                P.OBSERVACOES AS 'Observação', TP.NOME AS 'Tipo de Produto', P.STACKINGFACTOR AS 'Fator de Empilhamento',
	                                P.QUANTITYOFLAYERSPERPALLET AS 'Quantidade de Camadas por Pallet', P.QUANTITYOFTRADEITEMSPERPALLETLAYER AS 'Quantidade de Itens Comerciais em uma Única Camada',
	                                P.QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER AS 'Quantidade de Itens Comerciais uma Camada Completa', 
	                                P.QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM AS 'Quantidade de Camadas Completas em Item Comercial', 

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, POR.GLOBALTRADEITEMNUMBER) + ', '
				                                FROM PRODUTOHIERARQUIA PH
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
				                                WHERE PH.CODIGOPRODUTOSUPERIOR = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, POR.GLOBALTRADEITEMNUMBER) + ', '
				                                FROM PRODUTOHIERARQUIA PH
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
				                                WHERE PH.CODIGOPRODUTOSUPERIOR = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''))
	                                ) AS 'GTIN Inferior',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, PH.QUANTIDADE) + ', '
				                                FROM PRODUTOHIERARQUIA PH
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
				                                WHERE PH.CODIGOPRODUTOSUPERIOR = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, PH.QUANTIDADE) + ', '
				                                FROM PRODUTOHIERARQUIA PH
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
				                                WHERE PH.CODIGOPRODUTOSUPERIOR = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''))
	                                ) AS 'Quantidade',

	                                P.IPIPERC AS 'Alíquota de Impostos IPI', 
	                                CASE P.ISTRADEITEMANORDERABLEUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Item Comercial Liberado para Compra',
	                                CASE P.ISTRADEITEMADESPATCHUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Unidade de Despacho', 
	                                CASE P.ISTRADEITEMABASEUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Unidade Básica de Venda',
	                                CASE P.ISTRADEITEMANINVOICEUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Unidade de Faturamento',
	                                CASE P.ISTRADEITEMACONSUMERUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador se Item Comercial é Unidade de Consumo',
	                                CASE P.ISTRADEITEMAMODEL WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Item Comercial é um modelo',
	                                P.ORDERSIZINGFACTOR AS 'Unidade de Medida do Pedido', P.ORDERQUANTITYMULTIPLE AS 'Múltiplo da Quantidade para Pedido',
	                                P.ORDERQUANTITYMINIMUM AS 'Quantidade Mínima para Pedido', P.PACKAGINGTYPECODE AS 'Tipo da Embalagem', 
	                                P.DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM AS 'Temperatura Mínima de Armazenamento/manuseio', 
	                                P.STORAGEHANDLINGTEMPMINIMUMUOM AS 'Unidade de Medida da Temperatura de Armazenamento/manuseio Mínima', 
	                                P.STORAGEHANDLINGTEMPERATUREMAXIMUM AS 'Temperatura Máxima de Armazenamento/manuseio', 
	                                P.STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE AS 'Unidade de Medida da Temperatura Armazenamento/manuseio Máxima', 
	                                CASE P.ISDANGEROUSSUBSTANCEINDICATED WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Mercadorias Perigosas',
	                                P.INFORMATIONPROVIDER AS 'GLN do Provedor da Informação', 
	                                LI.NOME AS 'Língua',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, PURL.NOME) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, PURL.NOME) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'Nome URL',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, PURL.URL) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, PURL.URL) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'URL',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, TU.NOME ) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN TIPOURL TU ON TU.CODIGO = PURL.CODIGOTIPOURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, TU.NOME) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN TIPOURL TU ON TU.CODIGO = PURL.CODIGOTIPOURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'Tipo URL'

                                FROM PRODUTO P WITH (NOLOCK)
                                LEFT JOIN TIPOPRODUTO TP ON (TP.CODIGO = P.CODIGOTIPOPRODUTO)
                                LEFT JOIN TRADEITEMUNITDESCRIPTORCODES TIUDC ON (TIUDC.CODIGO = TP.CODIGOTRADEITEMUNITDESCRIPTORCODES)
                                LEFT JOIN LICENCA L ON (L.CODIGO = P.CODIGOTIPOGTIN)
                                LEFT JOIN LOCALIZACAOFISICA LF ON (LF.GLN = P.INFORMATIONPROVIDER)
                                LEFT JOIN NCM N ON (N.NCM = P.IMPORTCLASSIFICATIONVALUE)
                                LEFT JOIN PRODUTO P2 ON (P2.CODIGOPRODUTO = P.CODIGOPRODUTOORIGEM)
                                INNER JOIN TIPOGTIN TG ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                LEFT JOIN USUARIO U ON (U.CODIGO = P.CODIGOUSUARIOCRIACAO)
                                LEFT JOIN STATUSGTIN SG ON (SG.CODIGO = P.CODIGOSTATUSGTIN)
                                LEFT JOIN TBSEGMENT TBS ON (TBS.CODESEGMENT = P.CODESEGMENT)
                                LEFT JOIN TBFAMILY TBF ON (TBF.CODEFAMILY = P.CODEFAMILY)
                                LEFT JOIN TBCLASS TBC ON (TBC.CODECLASS = P.CODECLASS)
                                LEFT JOIN TBBRICK TBB ON (TBB.CODEBRICK = P.CODEBRICK)
                                LEFT JOIN LINGUA LI ON (LI.CODIGO = P.CODIGOLINGUA)
                                LEFT JOIN UNECEREC20 UC ON (UC.COMMONCODE = P.HEIGHTMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC1 ON (UC1.COMMONCODE = P.WIDTHMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC2 ON (UC2.COMMONCODE = P.DEPTHMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC3 ON (UC3.COMMONCODE = P.NETCONTENTMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC4 ON (UC4.COMMONCODE = P.GROSSWEIGHTMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC5 ON (UC5.COMMONCODE = P.NETWEIGHTMEASUREMENTUNITCODE)
                                LEFT JOIN PRODUTOAGENCIAREGULADORA PAR WITH (NOLOCK) ON (PAR.CODIGOPRODUTO = P.CODIGOPRODUTO)
                                LEFT JOIN AGENCIAREGULADORA AR WITH (NOLOCK) ON (AR.CODIGO = PAR.CODIGOAGENCIA)
                                WHERE P.CODIGOPRODUTO IN (SELECT P.CODIGOPRODUTO
					                FROM PRODUTO P WITH (NOLOCK)
					                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString() + ")";
            #endregion

            //Create a connection to the database
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["BD"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, conn);

            //Fill a C# dataset with query results
            DataTable t1 = new DataTable();
            using (SqlDataAdapter a = new SqlDataAdapter(cmd))
            {
                a.Fill(t1);
            }

            #region Estrutura das Agências Reguladoras
            qtdeAgencia = qtdeAgencia >= 5 ? 5 : qtdeAgencia;

            for (int i = 0; i < qtdeAgencia; i++)
            {
                t1.Columns.Add("Número de Identificação Alternativa " + (i + 1), typeof(String));
                t1.Columns.Add("Agência Reguladora " + (i + 1), typeof(String));
            }

            foreach (DataRow row in t1.Rows)
            {
                string[] nridentificacaoalternativa = row["Número de Identificação Alternativa"].ToString() != String.Empty ? row["Número de Identificação Alternativa"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];
                string[] agenciareguladora = row["Agência Reguladora"].ToString() != String.Empty ? row["Agência Reguladora"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];

                for (var i = 0; i < (nridentificacaoalternativa.Length >= 5 ? 5 : nridentificacaoalternativa.Length); i++)
                {
                    row["Número de Identificação Alternativa " + (i + 1)] = nridentificacaoalternativa[i];
                }

                for (var i = 0; i < (agenciareguladora.Length >= 5 ? 5 : agenciareguladora.Length); i++)
                {
                    row["Agência Reguladora " + (i + 1)] = agenciareguladora[i];
                }
            }

            t1.Columns.Remove("Número de Identificação Alternativa");
            t1.Columns.Remove("Agência Reguladora");
            #endregion

            #region Estrutura das URLs
            qtdeURL = qtdeURL >= 3 ? 3 : qtdeURL;

            for (int i = 0; i < qtdeURL; i++)
            {
                t1.Columns.Add("Nome URL " + (i + 1), typeof(String));
                t1.Columns.Add("URL " + (i + 1), typeof(String));
                t1.Columns.Add("Tipo URL " + (i + 1), typeof(String));
            }

            foreach (DataRow row in t1.Rows)
            {
                string[] nomesurls = row["Nome URL"].ToString() != String.Empty ? row["Nome URL"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];
                string[] urls = row["URL"].ToString() != String.Empty ? row["URL"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];
                string[] tiposurls = row["Tipo URL"].ToString() != String.Empty ? row["Tipo URL"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];

                for (var i = 0; i < (nomesurls.Length >= 3 ? 3 : nomesurls.Length); i++)
                {
                    row["Nome URL " + (i + 1)] = nomesurls[i];
                }

                for (var i = 0; i < (urls.Length >= 3 ? 3 : urls.Length); i++)
                {
                    row["URL " + (i + 1)] = urls[i];
                }

                for (var i = 0; i < (tiposurls.Length >= 3 ? 3 : tiposurls.Length); i++)
                {
                    row["Tipo URL " + (i + 1)] = tiposurls[i];
                }
            }

            t1.Columns.Remove("Nome URL");
            t1.Columns.Remove("URL");
            t1.Columns.Remove("Tipo URL");
            #endregion

            for (int i = 0; i < t1.Columns.Count; i++)
            {
                csv.Append(t1.Columns[i].ColumnName);
                csv.Append(i == t1.Columns.Count - 1 ? "\n" : ";");
            }

            foreach (DataRow row in t1.Rows)
            {
                for (int i = 0; i < t1.Columns.Count; i++)
                {
                    csv.Append(row[i].ToString());
                    csv.Append(i == t1.Columns.Count - 1 ? "\n" : ";");
                }
            }

            csv.Append(Environment.NewLine);
        }

        private void AdicionarCabecalhoXLSX(ExcelWorksheet ws, List<DynamicXml> columns, int qtdeURL, int qtdeAgencia)
        {
            if (ws != null && columns != null && columns.Count > 0)
            {
                int idx = 1;

                ws.Cells[1, 1].Value = "Tipo do GTIN";

                foreach (dynamic column in columns)
                {
                    if (column.coluna.ToLower().Equals("url"))
                    {
                        for (int i = 1; i <= qtdeURL; i++)
                        {
                            idx++;
                            ws.Cells[1, idx].Value = "Nome URL " + i.ToString();

                            idx++;
                            ws.Cells[1, idx].Value = "URL " + i.ToString();

                            idx++;
                            ws.Cells[1, idx].Value = "Tipo de URL " + i.ToString();
                        }
                    }
                    else
                    {
                        idx++;
                        ws.Cells[1, idx].Value = column.coluna;
                    }
                }
            }
        }

        private void AdicionarValoresXLSX(Database db, ExcelWorksheet ws, List<DynamicXml> columns, List<dynamic> result, int qtdeURL, int qtdeAgencia, string fileName)
        {

            Associado associado = (Associado)this.getSession("ASSOCIADO_SELECIONADO");

            #region SQL
            string sqlQuery = @"SELECT TG.NOME AS 'Tipo do GTIN', CASE P.INDICADORGDSN WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'GDSN?', 
	                                SG.NOME AS 'Status do GTIN', P2.GLOBALTRADEITEMNUMBER AS 'GTIN Origem', P.TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM AS 'Quantidade',
                                    P.GLOBALTRADEITEMNUMBER AS GTIN, P.BRANDNAME AS 'Marca',
	                                P.MODELNUMBER AS 'Número do Modelo', P.PRODUCTDESCRIPTION AS 'Descrição do Produto', 
	                                CONVERT(VARCHAR(10),CONVERT(DATE,P.STARTAVAILABILITYDATETIME,106),103) AS 'Data Inicial de Disponibilidade', 
	                                CONVERT(VARCHAR(10),CONVERT(DATE,P.ENDAVAILABILITYDATETIME,106),103) AS 'Data Final de Disponibilidade', N.DESCRICAO AS NCM,
	                                P.CODESEGMENT + ' - ' + TBS.TEXT AS 'Segmento', P.CODEFAMILY + ' - ' + TBF.TEXT AS 'Família', P.CODECLASS + ' - ' + TBC.TEXT AS 'Classe', P.CODEBRICK + ' - ' + TBB.TEXT AS 'Brick',
                                    
                                    SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, PAG.ALTERNATEITEMIDENTIFICATIONID) + ',//,'
				                                FROM PRODUTOAGENCIAREGULADORA PAG
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PAG.CODIGOPRODUTO
				                                WHERE PAG.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, PAG.ALTERNATEITEMIDENTIFICATIONID) + ',//,'
				                                FROM PRODUTOAGENCIAREGULADORA PAG
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PAG.CODIGOPRODUTO
				                                WHERE PAG.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'Número de Identificação Alternativa',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, AR.NOME) + ',//,'
				                                FROM PRODUTOAGENCIAREGULADORA PAG
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PAG.CODIGOPRODUTO
                                                INNER JOIN AGENCIAREGULADORA AR ON AR.CODIGO = PAG.CODIGOAGENCIA
				                                WHERE PAG.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, AR.NOME) + ',//,'
				                                FROM PRODUTOAGENCIAREGULADORA PAG
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PAG.CODIGOPRODUTO
                                                INNER JOIN AGENCIAREGULADORA AR ON AR.CODIGO = PAG.CODIGOAGENCIA
				                                WHERE PAG.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'Agência Reguladora',
                                    
	                                (SELECT TOP 1 C.NOME FROM COUNTRYOFORIGINISO3166 C WHERE C.CODIGOPAIS = P.COUNTRYCODE) AS 'País/Mercado de Destino', 
	                                (SELECT TOP 1 C.NOME FROM COUNTRYOFORIGINISO3166 C WHERE C.CODIGOPAIS = P.TRADEITEMCOUNTRYOFORIGIN) AS 'País de Origem', 
	                                P.ESTADO AS 'Estado', P.HEIGHT AS 'Altura', UC.SIGLA AS 'Altura - UN Medida',
	                                P.WIDTH AS 'Largura', UC1.SIGLA AS 'Largura - UN Medida', 
	                                P.DEPTH AS 'Profundidade', UC2.SIGLA AS 'Profundidade - UN Medida',
	                                P.NETCONTENT AS 'Conteúdo Líquido', UC3.SIGLA AS 'Cont. Liq. - UN Medida', 
	                                P.GROSSWEIGHT AS 'Peso Bruto', UC4.SIGLA AS 'Peso Bruto - UN Medida', 
	                                P.NETWEIGHT AS 'Peso Líquido', UC5.SIGLA AS 'Peso Líq. - UN Medida',
	                                P.MINIMUMTRADEITEMLIFESPANFROMTIMEOFPRODUCTION AS 'Tempo mínimo (dias) de vida útil do produto após produção',
	                                CASE P.INDICADORCOMPARTILHADADOS WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Compartilha Dados?',
	                                P.OBSERVACOES AS 'Observação', TP.NOME AS 'Tipo de Produto', P.STACKINGFACTOR AS 'Fator de Empilhamento',
	                                P.QUANTITYOFLAYERSPERPALLET AS 'Quantidade de Camadas por Pallet', P.QUANTITYOFTRADEITEMSPERPALLETLAYER AS 'Quantidade de Itens Comerciais em uma Única Camada',
	                                P.QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER AS 'Quantidade de Itens Comerciais uma Camada Completa', 
	                                P.QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM AS 'Quantidade de Camadas Completas em Item Comercial', 

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, POR.GLOBALTRADEITEMNUMBER) + ', '
				                                FROM PRODUTOHIERARQUIA PH
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
				                                WHERE PH.CODIGOPRODUTOSUPERIOR = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, POR.GLOBALTRADEITEMNUMBER) + ', '
				                                FROM PRODUTOHIERARQUIA PH
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
				                                WHERE PH.CODIGOPRODUTOSUPERIOR = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''))
	                                ) AS 'GTIN Inferior',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, PH.QUANTIDADE) + ', '
				                                FROM PRODUTOHIERARQUIA PH
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
				                                WHERE PH.CODIGOPRODUTOSUPERIOR = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, PH.QUANTIDADE) + ', '
				                                FROM PRODUTOHIERARQUIA PH
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
				                                WHERE PH.CODIGOPRODUTOSUPERIOR = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''))
	                                ) AS 'Quantidade',

	                                P.IPIPERC AS 'Alíquota de Impostos IPI', 
	                                CASE P.ISTRADEITEMANORDERABLEUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Item Comercial Liberado para Compra',
	                                CASE P.ISTRADEITEMADESPATCHUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Unidade de Despacho', 
	                                CASE P.ISTRADEITEMABASEUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Unidade Básica de Venda',
	                                CASE P.ISTRADEITEMANINVOICEUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Unidade de Faturamento',
	                                CASE P.ISTRADEITEMACONSUMERUNIT WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador se Item Comercial é Unidade de Consumo',
	                                CASE P.ISTRADEITEMAMODEL WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Item Comercial é um modelo',
	                                P.ORDERSIZINGFACTOR AS 'Unidade de Medida do Pedido', P.ORDERQUANTITYMULTIPLE AS 'Múltiplo da Quantidade para Pedido',
	                                P.ORDERQUANTITYMINIMUM AS 'Quantidade Mínima para Pedido', P.PACKAGINGTYPECODE AS 'Tipo da Embalagem', 
	                                P.DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM AS 'Temperatura Mínima de Armazenamento/manuseio', 
	                                P.STORAGEHANDLINGTEMPMINIMUMUOM AS 'Unidade de Medida da Temperatura de Armazenamento/manuseio Mínima', 
	                                P.STORAGEHANDLINGTEMPERATUREMAXIMUM AS 'Temperatura Máxima de Armazenamento/manuseio', 
	                                P.STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE AS 'Unidade de Medida da Temperatura Armazenamento/manuseio Máxima', 
	                                CASE P.ISDANGEROUSSUBSTANCEINDICATED WHEN 1 THEN 'Sim' WHEN 0 THEN 'Não' END AS 'Indicador de Mercadorias Perigosas',
	                                P.INFORMATIONPROVIDER AS 'GLN do Provedor da Informação', 
	                                LI.NOME AS 'Língua',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, PURL.NOME) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, PURL.NOME) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'Nome URL',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, PURL.URL) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, PURL.URL) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'URL',

	                                SUBSTRING(
		                                STUFF( 
			                                (SELECT CONVERT(VARCHAR, TU.NOME ) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN TIPOURL TU ON TU.CODIGO = PURL.CODIGOTIPOURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,''), 0, (LEN(STUFF( 
			                                (SELECT CONVERT(VARCHAR, TU.NOME) + ',//,'
				                                FROM PRODUTOURL PURL
				                                INNER JOIN TIPOURL TU ON TU.CODIGO = PURL.CODIGOTIPOURL
				                                INNER JOIN PRODUTO POR ON POR.CODIGOPRODUTO = PURL.CODIGOPRODUTO
				                                WHERE PURL.CODIGOPRODUTO = P.CODIGOPRODUTO
				                                FOR XML PATH('')			
		                                ),1,0,'')) - 3)
	                                ) AS 'Tipo URL'

                                FROM PRODUTO P WITH (NOLOCK)
                                LEFT JOIN TIPOPRODUTO TP ON (TP.CODIGO = P.CODIGOTIPOPRODUTO)
                                LEFT JOIN TRADEITEMUNITDESCRIPTORCODES TIUDC ON (TIUDC.CODIGO = TP.CODIGOTRADEITEMUNITDESCRIPTORCODES)
                                LEFT JOIN LICENCA L ON (L.CODIGO = P.CODIGOTIPOGTIN)
                                LEFT JOIN LOCALIZACAOFISICA LF ON (LF.GLN = P.INFORMATIONPROVIDER)
                                LEFT JOIN NCM N ON (N.NCM = P.IMPORTCLASSIFICATIONVALUE)
                                LEFT JOIN PRODUTO P2 ON (P2.CODIGOPRODUTO = P.CODIGOPRODUTOORIGEM)
                                INNER JOIN TIPOGTIN TG ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                LEFT JOIN USUARIO U ON (U.CODIGO = P.CODIGOUSUARIOCRIACAO)
                                LEFT JOIN STATUSGTIN SG ON (SG.CODIGO = P.CODIGOSTATUSGTIN)
                                LEFT JOIN TBSEGMENT TBS ON (TBS.CODESEGMENT = P.CODESEGMENT)
                                LEFT JOIN TBFAMILY TBF ON (TBF.CODEFAMILY = P.CODEFAMILY)
                                LEFT JOIN TBCLASS TBC ON (TBC.CODECLASS = P.CODECLASS)
                                LEFT JOIN TBBRICK TBB ON (TBB.CODEBRICK = P.CODEBRICK)
                                LEFT JOIN LINGUA LI ON (LI.CODIGO = P.CODIGOLINGUA)
                                LEFT JOIN UNECEREC20 UC ON (UC.COMMONCODE = P.HEIGHTMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC1 ON (UC1.COMMONCODE = P.WIDTHMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC2 ON (UC2.COMMONCODE = P.DEPTHMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC3 ON (UC3.COMMONCODE = P.NETCONTENTMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC4 ON (UC4.COMMONCODE = P.GROSSWEIGHTMEASUREMENTUNITCODE)
                                LEFT JOIN UNECEREC20 UC5 ON (UC5.COMMONCODE = P.NETWEIGHTMEASUREMENTUNITCODE)
                                LEFT JOIN PRODUTOAGENCIAREGULADORA PAR WITH (NOLOCK) ON (PAR.CODIGOPRODUTO = P.CODIGOPRODUTO)
                                LEFT JOIN AGENCIAREGULADORA AR WITH (NOLOCK) ON (AR.CODIGO = PAR.CODIGOAGENCIA)
                                WHERE P.CODIGOPRODUTO IN (SELECT P.CODIGOPRODUTO
					                FROM PRODUTO P WITH (NOLOCK)
					                WHERE P.CODIGOASSOCIADO = " + associado.codigo.ToString() + ")";
            #endregion

            //Create a connection to the database
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["BD"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand(sqlQuery, conn);

            cmd.CommandTimeout = 120;

            //Fill a C# dataset with query results
            DataTable t1 = new DataTable();
            using (SqlDataAdapter a = new SqlDataAdapter(cmd))
            {
                a.Fill(t1);
            }

            #region Estrutura das Agências Reguladoras
            qtdeAgencia = qtdeAgencia >= 5 ? 5 : qtdeAgencia;

            for (int i = 0; i < qtdeAgencia; i++)
            {
                t1.Columns.Add("Número de Identificação Alternativa " + (i + 1), typeof(String));
                t1.Columns.Add("Agência Reguladora " + (i + 1), typeof(String));
            }

            foreach (DataRow row in t1.Rows)
            {
                string[] nridentificacaoalternativa = row["Número de Identificação Alternativa"].ToString() != String.Empty ? row["Número de Identificação Alternativa"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];
                string[] agenciareguladora = row["Agência Reguladora"].ToString() != String.Empty ? row["Agência Reguladora"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];

                for (var i = 0; i < (nridentificacaoalternativa.Length >= 5 ? 5 : nridentificacaoalternativa.Length); i++)
                {
                    row["Número de Identificação Alternativa " + (i + 1)] = nridentificacaoalternativa[i];
                }

                for (var i = 0; i < (agenciareguladora.Length >= 5 ? 5 : agenciareguladora.Length); i++)
                {
                    row["Agência Reguladora " + (i + 1)] = agenciareguladora[i];
                }
            }

            t1.Columns.Remove("Número de Identificação Alternativa");
            t1.Columns.Remove("Agência Reguladora");
            #endregion

            #region Estrutura das URLs
            qtdeURL = qtdeURL >= 3 ? 3 : qtdeURL;

            for (int i = 0; i < qtdeURL; i++)
            {
                t1.Columns.Add("Nome URL " + (i + 1), typeof(String));
                t1.Columns.Add("URL " + (i + 1), typeof(String));
                t1.Columns.Add("Tipo URL " + (i + 1), typeof(String));
            }

            foreach (DataRow row in t1.Rows)
            {
                string[] nomesurls = row["Nome URL"].ToString() != String.Empty ? row["Nome URL"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];
                string[] urls = row["URL"].ToString() != String.Empty ? row["URL"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];
                string[] tiposurls = row["Tipo URL"].ToString() != String.Empty ? row["Tipo URL"].ToString().Split(new string[] { ",//," }, StringSplitOptions.None) : new string[0];

                for (var i = 0; i < (nomesurls.Length >= 3 ? 3 : nomesurls.Length); i++)
                {
                    row["Nome URL " + (i + 1)] = nomesurls[i]; 
                }

                for (var i = 0; i < (urls.Length >= 3 ? 3 : urls.Length); i++)
                {
                    row["URL " + (i + 1)] = urls[i];
                }

                for (var i = 0; i < (tiposurls.Length >= 3 ? 3 : tiposurls.Length); i++)
                {
                    row["Tipo URL " + (i + 1)] = tiposurls[i];
                }
            }

            t1.Columns.Remove("Nome URL");
            t1.Columns.Remove("URL");
            t1.Columns.Remove("Tipo URL");
            #endregion

            var file = new FileInfo(fileName);

            //Load the data table into the excel file and save
            using (ExcelPackage pck = new ExcelPackage(file))
            {
                ExcelWorksheet ws1 = pck.Workbook.Worksheets.Add("Data");
                ws.Cells["A1"].LoadFromDataTable(t1, true);
                pck.Save();
            }

            ws.Cells.AutoFitColumns();
            ws.Column(4).Style.Numberformat.Format = "0";
            ws.Column(6).Style.Numberformat.Format = "0";
            ws.Column(59).Style.Numberformat.Format = "0";

            conn.Close();
        }

        public static object ConvertJTokenToObject(JToken token)
        {
            if (token is JValue)
            {
                return ((JValue)token).Value;
            }
            if (token is JObject)
            {
                dynamic expando = new ExpandoObject();

                (from childToken in ((JToken)token) where childToken is JProperty select childToken as JProperty).ToList().ForEach(property =>
                {
                    ((IDictionary<string, object>)expando).Add(property.Name, ConvertJTokenToObject(property.Value));
                });

                return expando;
            }
            if (token is JArray)
            {
                object[] array = new object[((JArray)token).Count];
                int index = 0;

                foreach (JToken arrayItem in ((JArray)token))
                {
                    array[index] = ConvertJTokenToObject(arrayItem);
                    index++;
                }

                return array;
            }

            throw new ArgumentException(string.Format("Unknown token type '{0}'", token.GetType()), "token");
        }

        [GS1CNPAttribute(false)]
        public string VerificaBrickInformacaoNutricional(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                //Verifica se a Descrição do Produto já foi cadastrada
                string select = @"  SELECT COUNT(0) as count
                                    FROM tbBrick b
                                    WHERE  (b.CodeSegment = 50000000 AND b.CodeFamily <> 50210000
	                                    OR B.CodeFamily = 10110000
	                                    OR B.CodeClass IN (51121900, 51121600)
	                                    OR b.CodeBrick IN (10000654, 10000466))
	                                    AND B.CodeBrick = @brick";

                db.Open();
                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                object retorno = db.Select(select, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutoNutrientHeader(dynamic parametro)
        {
            string sql = @"SELECT servingSizeDescription, dailyValueIntakeReference 
		                    FROM ProdutoNutrientHeader PNH WITH (NOLOCK)
		                    WHERE PNH.CODIGOPRODUTO = @codigoproduto";

            dynamic param = null;
            if (parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (Database db = new Database("BD"))
            {
                db.Open();
                
                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutoNutrientDetail(dynamic parametro)
        {
            string sql = @"select pnd.quantityContained
		                        , pnd.measurementPrecisionCode
		                        , pnd.dailyValueIntakePercent
		                        , pnd.Codigo
		                        , nt.Codigo as nt_codigo
		                        , nt.nome as nt_nome
		                        , nt.Sigla as nt_sigla
		                        , nt.tipoUnidadeMedida as nt_tipounidademedida
	                        from ProdutoNutrientDetail pnd
		                        inner join nutrientTypeCodeINFOODS nt on pnd.nutrientTypeCode = nt.Sigla
	                        where pnd.CodigoProduto = @codigoproduto";

            dynamic param = null;
            if (parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (Database db = new Database("BD"))
            {
                db.Open();

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarProdutoAlergeno(dynamic parametro)
        {
            string sql = @"SELECT AT.Codigo AS at_codigo
	                            , at.sigla as at_sigla
	                            , at.descricao as at_descricao
	                            , pa.Codigo as codigo
	                            , pa.levelOfContainmentCode
                            FROM ProdutoAllergen PA
	                            INNER JOIN AllergenRelatedInformation ARI ON PA.CodigoAllergenRelatedInformation = ARI.Codigo
	                            INNER JOIN AllergenTypeCode AT ON ARI.CodigoAllergenTypeCode = AT.Codigo
                            WHERE PA.CodigoProduto = @codigoproduto";

            dynamic param = null;
            if (parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (Database db = new Database("BD"))
            {
                db.Open();

                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        public string RetornaCodigoProdutoByGTIN(dynamic parametro)
        {
            string sql = @"SELECT P.CODIGOPRODUTO 
                            FROM PRODUTO P (NOLOCK) 
                            WHERE P.GLOBALTRADEITEMNUMBER = @globaltradeitemnumber";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic retorno = ((List<dynamic>)db.Select(sql, parametro.campos)).FirstOrDefault();

                if (retorno != null)
                {
                    return retorno.codigoproduto.ToString();
                }
                else
                {
                    return "";
                }
            }
        }
        

        #region PRODUTOGDDSERVICE

        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPCore(string GTINS, string GLN)
        {

            string sql = @"SELECT P.CODIGOPRODUTO, P.GLOBALTRADEITEMNUMBER, P.CODIGOPRODUTOORIGEM, P.CODIGOTIPOGTIN, P.NRPREFIXO, P.CODITEM, TP.CODIGO AS CODIGOTIPOPRODUTO, TP.NOME AS NOMETIPOPRODUTO,
                                        P.PRODUCTDESCRIPTION, CONVERT(VARCHAR(10),CONVERT(DATE, P.DATAINCLUSAO,106),103) AS DATAINCLUSAO, CONVERT(VARCHAR(10),CONVERT(DATE,P.DATASUSPENSAO,106),103) AS DATASUSPENSAO,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.DATAREATIVACAO,106),103) AS DATAREATIVACAO, TG.NOME AS NOMETIPOGTIN, CONVERT(VARCHAR(10),CONVERT(DATE, P.DATAALTERACAO,106),103) AS DATAALTERACAO,
                                        CONVERT(VARCHAR(10),CONVERT(DATE,P.DATACANCELAMENTO,106),103) AS DATACANCELAMENTO, CONVERT(VARCHAR(10),CONVERT(DATE,P.DATAREUTILIZACAO,106),103) AS DATAREUTILIZACAO,
                                        P.CODIGOLINGUA, P.COUNTRYCODE, P.ESTADO, P.INDICADORCOMPARTILHADADOS, P.OBSERVACOES, P.CODIGOASSOCIADO,
                                        P.CODIGOSTATUSGTIN, P.CODESEGMENT, P.CODEFAMILY, P.CODECLASS, P.CODEBRICK, P.CODEBRICKATTRIBUTE, P.TRADEITEMCOUNTRYOFORIGIN,
                                        P.INFORMATIONPROVIDER, LF.NOME AS NAMEOFINFORMATIONPROVIDER, P.BRANDNAME, P.MODELNUMBER,                               
                                        P.IMPORTCLASSIFICATIONTYPE, P.IMPORTCLASSIFICATIONVALUE, P.ISTRADEITEMABASEUNIT,
                                        P.ISTRADEITEMACONSUMERUNIT, P.ISTRADEITEMAMODEL, P.ISTRADEITEMANINVOICEUNIT, P.MINIMUMTRADEITEMLIFESPANFROMTIMEOFPRODUCTION,
                                        P.STARTAVAILABILITYDATETIME, CONVERT(VARCHAR(10),CONVERT(DATE,P.ENDAVAILABILITYDATETIME,106),103) AS ENDAVAILABILITYDATETIME,
                                        P.DEPTH, P.DEPTHMEASUREMENTUNITCODE, P.HEIGHT, P.STORAGEHANDLINGTEMPMINIMUMUOM, 
                                        P.HEIGHTMEASUREMENTUNITCODE, P.WIDTH, P.WIDTHMEASUREMENTUNITCODE, P.NETCONTENT, P.NETCONTENTMEASUREMENTUNITCODE, P.GROSSWEIGHT,
                                        P.GROSSWEIGHTMEASUREMENTUNITCODE, P.NETWEIGHT, P.NETWEIGHTMEASUREMENTUNITCODE, P.PACKAGINGTYPECODE, P.PALLETTYPECODE,
                                        P.ISTRADEITEMANORDERABLEUNIT, P.ISTRADEITEMADESPATCHUNIT, P.ORDERSIZINGFACTOR, P.ORDERQUANTITYMULTIPLE, P.ORDERQUANTITYMINIMUM,
                                        P.QUANTITYOFLAYERSPERPALLET, P.QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER, P.QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM,
                                        P.QUANTITYOFTRADEITEMSPERPALLETLAYER, P.DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM, P.STORAGEHANDLINGTEMPERATUREMAXIMUM,
                                        P.STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE, P.STACKINGFACTOR, P.ISDANGEROUSSUBSTANCEINDICATED,
                                        P.TOTALQUANTITYOFNEXTLOWERLEVELTRADEITEM, P.IPIPERC, N.DESCRICAO AS NCMDESCRICAO, P.INDICADORGDSN, P.VARIANTELOGISTICA,
                                        P2.PRODUCTDESCRIPTION AS NOMEPRODUTOORIGEM, U.NOME AS NOMEUSUARIOCRIACAO, U2.NOME AS NOMEUSUARIOALTERACAO, A.NOME AS RAZAOSOCIAL, A.CPFCNPJ,
                                        P2.GLOBALTRADEITEMNUMBER AS CODIGOGTINORIGEM
                                        ,LG.NOME AS LINGUA
                                        
                                        ,P.isTradeItemAService
                                        ,P.isTradeItemNonphysical
                                        ,P.isTradeItemRecalled
                                        ,P.additionalTradeItemDescription
                                        ,SG.NOME AS STATUSGTIN
                                        
                                    FROM PRODUTO P WITH (NOLOCK)
                                    LEFT JOIN TIPOPRODUTO TP ON (TP.CODIGO = P.CODIGOTIPOPRODUTO)
	                                LEFT JOIN TRADEITEMUNITDESCRIPTORCODES TIUDC ON (TIUDC.CODIGO = TP.CODIGOTRADEITEMUNITDESCRIPTORCODES)
                                    LEFT JOIN LICENCA L ON (L.CODIGO = P.CODIGOTIPOGTIN)
	                                LEFT JOIN LOCALIZACAOFISICA LF ON (LF.GLN = P.INFORMATIONPROVIDER)
                                    LEFT JOIN NCM N ON (N.NCM = P.IMPORTCLASSIFICATIONVALUE)
                                    LEFT JOIN PRODUTO P2 ON (P2.CODIGOPRODUTO = P.CODIGOPRODUTOORIGEM)
                                    INNER JOIN TIPOGTIN TG ON (TG.CODIGO = P.CODIGOTIPOGTIN)
                                    LEFT JOIN USUARIO U ON (U.CODIGO = P.CODIGOUSUARIOCRIACAO)
	                                LEFT JOIN USUARIO U2 ON (U2.CODIGO = P.CODIGOUSUARIOALTERACAO)
                                    LEFT JOIN ASSOCIADO A ON (A.CODIGO = P.CODIGOASSOCIADO)
                                    LEFT JOIN LINGUA LG ON LG.CODIGO = P.CODIGOLINGUA
                                    INNER JOIN STATUSGTIN SG ON SG.CODIGO = P.CODIGOSTATUSGTIN                                    
                                    WHERE P.GLOBALTRADEITEMNUMBER in (" + GTINS + @")
                                    AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = P.CodigoAssociado)
                                    AND P.CODIGOSTATUSGTIN <> 7";



            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.gln = GLN;
                List<dynamic> produtos = db.Select(sql, param);
                return produtos;
            }

        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPCoreBuscarAgenciasReguladoras(dynamic parametro)
        {
            

            string sql = @"SELECT p.Codigo,
	                            a.Nome as NomeAgencia,
                                a.codigo as alternateitemidentificationagency, 
	                            p.alternateItemIdentificationId 
                            FROM ProdutoAgenciaReguladora p (NOLOCK) 
                            INNER JOIN AgenciaReguladora a (NOLOCK)  ON a.Codigo = p.CodigoAgencia
		                    WHERE a.STATUS = 1
                            AND p.CodigoProduto = @codigoproduto";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = db.Select(sql, parametro);
                return retorno;
            }
           
        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPCorePesquisaGTINInferior(dynamic parametro)
        {

            string sql = @"SELECT C.GLOBALTRADEITEMNUMBER, C.PRODUCTDESCRIPTION, A.Quantidade
                            FROM ProdutoHierarquia A (NOLOCK) 
                            INNER JOIN Produto B (NOLOCK)  ON B.CodigoProduto = A.CodigoProdutoSuperior AND B.CodigoStatusGTIN <> 7
                            INNER JOIN Produto C (NOLOCK)  ON C.CodigoProduto = A.CodigoProdutoInferior AND C.CodigoStatusGTIN <> 7
                            WHERE A.CodigoProdutoSuperior =  @codigoprodutosuperior";


            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = db.Select(sql, parametro);
                return retorno;
            }

        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPCorePesquisaURL(dynamic parametro)
        {

            string sql = @"SELECT A.URL, B.NOME AS TIPOURL 
                            FROM PRODUTOURL A
                            INNER JOIN TIPOURL B ON B.CODIGO = A.CODIGOTIPOURL
                            INNER JOIN PRODUTO C ON C.CODIGOPRODUTO = A.CODIGOPRODUTO AND C.CODIGOSTATUSGTIN <> 7
                            WHERE A.CODIGOPRODUTO = @codigoproduto";


            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = db.Select(sql, parametro);
                return retorno;
            }

        }


        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPCalcados(string GTINS, string GLN)
        {

            string sql = @"SELECT P.CodigoProduto, P.GLOBALTRADEITEMNUMBER, P.predominantMaterial                                        
                            FROM PRODUTO P WITH (NOLOCK)                                                        
                            WHERE P.GLOBALTRADEITEMNUMBER in (" + GTINS + @")
                            AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = P.CodigoAssociado)
                            AND P.CODIGOSTATUSGTIN <> 7";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.gln = GLN;
                List<dynamic> produtos = db.Select(sql, param);
                return produtos;
            }

        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetProdutoSharedCommonComponentsSize(dynamic parametro)
        {
            string sql = @"SELECT  A.CodigoProduto
                                  ,A.descriptiveSize
                                  ,A.sizeCode
                                  ,A.sizeSystemCode
                          FROM ProdutoSharedCommonComponentsSize A (NOLOCK) 
                          INNER JOIN Produto B (NOLOCK)  on B.CodigoProduto = A.CodigoProduto AND B.CodigoStatusGTIN <> 7
                          AND A.CodigoProduto = @codigoproduto";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = db.Select(sql, parametro);
                return retorno;
            }
        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetProdutoSharedCommonComponentsColour(dynamic parametro)
        {
            string sql = @"SELECT  A.CodigoProduto
                                  ,A.colourCode
                                  ,A.colourDescription
                                  ,A.colourCodeAgency
                           FROM ProdutoSharedCommonComponentsColour A
                           INNER JOIN Produto B on B.CodigoProduto = A.CodigoProduto AND B.CodigoStatusGTIN <> 7
                           AND A.CodigoProduto = @codigoproduto";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = db.Select(sql, parametro);
                return retorno;
            }
        }

        [GS1CNPAttribute(false)]
        public void InsertCNPCalcados(dynamic parametro, Database db)
        {

            if (((IDictionary<String, object>)parametro).ContainsKey("predominantmaterial"))
            {
                string sql = @"UPDATE PRODUTO SET predominantMaterial = @predominantmaterial
                           FROM PRODUTO
                           WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                           AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)";

                List<dynamic> retorno = db.Select(sql, parametro);
            }

            foreach (ExpandoObject size in parametro.sizes)
            {
                this.InsertProdutoSharedCommonComponentsSize(size, db);
            }

            foreach (ExpandoObject colour in parametro.colours)
            {
                InsertProdutoSharedCommonComponentsColour(colour, db);
            }
        }

        [GS1CNPAttribute(false)]
        public void InsertCNPCalcados(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();
                    db.BeginTransaction();
                    InsertCNPCalcados(parametro, db);
                    db.Commit();
                }
                catch (Exception e)
                {
                    db.Rollback();
                    throw new GS1TradeException("Não foi possível cadastrar.", e);
                }
            }

        }


        [GS1CNPAttribute(false)]
        public void InsertProdutoSharedCommonComponentsSize(dynamic parametro, Database db)
        {
            string sql = @"INSERT INTO [dbo].[ProdutoSharedCommonComponentsSize] ([CodigoProduto],[descriptiveSize],[sizeCode],[sizeSystemCode])
                           SELECT TOP 1 Codigoproduto,@descriptivesize,@sizecode,@sizesystemcode FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                           AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)";

            if (db == null)
            {
                using (Database db2 = new Database("BD"))
                {
                    db2.Open();
                    List<dynamic> retorno = db2.Select(sql, parametro);
                }
            }else{                
                List<dynamic> retorno = db.Select(sql, parametro);
            }
        }

        [GS1CNPAttribute(false)]
        public void InsertProdutoSharedCommonComponentsColour(dynamic parametro, Database db)
        {
            string sql = @"INSERT INTO [dbo].[ProdutoSharedCommonComponentsColour] ([CodigoProduto],[colourCode],[colourDescription],[colourCodeAgency])
                           SELECT TOP 1 Codigoproduto,@colourcode,@colourdescription,@colourcodeagency FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                           AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)";
            if(db == null){
                using (Database db2 = new Database("BD"))
                {
                    db2.Open();
                    List<dynamic> retorno = db2.Select(sql, parametro);
                }
            }else{                
                List<dynamic> retorno = db.Select(sql, parametro);
            }
        }


        [GS1CNPAttribute(false)]
        public void DeleteProdutoSharedCommonComponentsSize(dynamic parametro)
        {
            string sql = @"DELETE FROM [dbo].[ProdutoSharedCommonComponentsSize] WHERE [CodigoProduto] IN (
                               SELECT TOP 1 Codigoproduto FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                               AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)
                           )";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = db.Select(sql, parametro);
            }
        }

        [GS1CNPAttribute(false)]
        public void DeleteProdutoSharedCommonComponentsColour(dynamic parametro)
        {
            string sql = @"DELETE FROM [dbo].[ProdutoSharedCommonComponentsColour] WHERE [CodigoProduto] IN (
                            SELECT TOP 1 Codigoproduto,@colourcode,@colourdescription,@colourcodeagency FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                            AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)
                          )";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> retorno = db.Select(sql, parametro);
            }
        }

        [GS1CNPAttribute(false)]
        public void UpdateCNPCalcados(dynamic parametro)
        {                        
            using (Database db = new Database("BD")){
                try {
                    db.Open();
                    db.BeginTransaction();

                    string sqlSize = @"DELETE FROM [dbo].[ProdutoSharedCommonComponentsSize] WHERE [CodigoProduto] IN (
                                   SELECT TOP 1 Codigoproduto FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                                   AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)
                               )";
                    db.Select(sqlSize, parametro);

                    string sqlColour = @"DELETE FROM [dbo].[ProdutoSharedCommonComponentsColour] WHERE [CodigoProduto] IN (
                                SELECT TOP 1 Codigoproduto FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)
                              )";
                    db.Select(sqlColour, parametro);

                    this.InsertCNPCalcados(parametro, db);

                    db.Commit();
                }
                     catch (Exception e)
                     {
                         db.Rollback();
                         throw new GS1TradeException("Não foi possível cadastrar o Produto informado.", e);
                     }

                }
        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPBrick(string Bricks)
        {

            string sql = @"SELECT A.CodeBrick, A.CodeClass, A.CodeFamily, A.CodeSegment                                        
                            FROM tbBrick A WITH (NOLOCK)                                                        
                            WHERE A.CodeBrick in (" + Bricks + @")";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> bricks = db.Select(sql, null);
                return bricks;
            }

        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPPrefixoAssociado(string GLN)
        {

            string sql = @"SELECT A.CodigoLicenca, A.CodigoAssociado, A.NumeroPrefixo, A.CodigoStatusPrefixo
                            FROM PREFIXOLICENCAASSOCIADO A (NOLOCK) 
                            WHERE A.CodigoAssociado IN ( SELECT TOP 1 CodigoAssociado FROM LocalizacaoFisica WHERE GLN = @GLN)
                            AND A.CodigoStatusPrefixo IN (1,8)";

            using (Database db = new Database("BD"))
            {
                dynamic param = new ExpandoObject();
                param.GLN = GLN;
                db.Open();
                List<dynamic> prefixos = db.Select(sql, param);
                return prefixos;
            }

        }

        [GS1CNPAttribute(false)]
        public string InsertCNPCore(dynamic tradeItem, List<dynamic>  additionaltradeitemidentificationList, List<dynamic> nextLowerLevelList)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();
                    db.BeginTransaction();
                    string retorno = string.Empty;

                    if (nextLowerLevelList.Count() > 0 && tradeItem.codigotipogtin == 4)
                    {
                        string sqlOrigem = "SELECT CODIGOPRODUTO FROM PRODUTO (NOLOCK)  WHERE globalTradeItemNumber = " + nextLowerLevelList[0].gtin.ToString();
                        List<dynamic> produtoOrigem = db.Select(sqlOrigem);
                        if(produtoOrigem.Count() >0){
                            tradeItem.codigoprodutoorigem = produtoOrigem[0].codigoproduto;
                            tradeItem.totalquantityofnextlowerleveltradeitem = nextLowerLevelList[0].quantity;
                        }else{
                            retorno += "Produto Origem GTIN " + nextLowerLevelList[0].gtin.ToString() + "  não cadastrado;\n";
                        }
                    }


                    if (retorno == string.Empty)
                    {
                        int idInserido = db.Insert(nomeTabela, nomePK, tradeItem, true);


                        if (nextLowerLevelList.Count() > 0 && tradeItem.codigotipogtin != 4)
                        {
                            foreach (dynamic item in nextLowerLevelList)
                            {
                                item.codigoprodutosuperior = idInserido;
                                string sql = @"INSERT INTO [dbo].[ProdutoHierarquia] ([CodigoProdutoSuperior],[CodigoProdutoInferior],[Quantidade])
                                        SELECT @codigoprodutosuperior, A.CodigoProduto, @quantity FROM Produto A
                                        WHERE globalTradeItemNumber = @gtin;
                                        SELECT @@ROWCOUNT;";
                                int qtdhierarquia = db.Insert(sql, item, true);
                                if (qtdhierarquia == 0)
                                    retorno += "GTIN " + item.gtin + " da hierarquia não cadastrado;\n";
                            }
                        }


                        if (additionaltradeitemidentificationList.Count() > 0)
                        {
                            foreach (dynamic item in additionaltradeitemidentificationList)
                            {
                                item.codigoproduto = idInserido;
                                string sql = @"INSERT INTO [dbo].[ProdutoAgenciaReguladora] ([CodigoProduto],[CodigoAgencia],[alternateItemIdentificationId])
                                           SELECT @codigoproduto, A.Codigo, @additionaltradeitemidentificationvalue FROM AGENCIAREGULADORA a
                                           WHERE A.NOME = @additionaltradeitemidentificationtypecode;
                                           SELECT @@ROWCOUNT;";
                                int qtdagencia = db.Insert(sql, item, true);
                                if (qtdagencia == 0)
                                    retorno += "Agência " + item.additionaltradeitemidentificationtypecode + " não cadastrada;\n";
                            }
                        }
                    }

                    if (retorno != string.Empty)
                    {
                        db.Rollback();
                    }
                    else
                    {
                        db.Commit();
                    }

                    return retorno;

                }
                catch (SqlException e)
                {
                    if (e.Number == 2627)
                    {
                        db.Rollback();
                        return "GTIN já cadastrado";
                    }
                    db.Rollback();
                    throw new GS1TradeException("Não foi possível cadastrar o Produto informado.", e);
                }
                catch (Exception e)
                {
                    db.Rollback();
                    throw new GS1TradeException("Não foi possível cadastrar o Produto informado.", e);
                }                
            }           
        }

        [GS1CNPAttribute(false)]
        public string UpdateCNPCore(string GLN, ref dynamic tradeItem, List<dynamic> additionaltradeitemidentificationList, List<dynamic> nextLowerLevelList)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();
                    db.BeginTransaction();
                    string retorno = string.Empty;
                    bool flReativado = false;

                    string sqlProduto = @" SELECT TOP 1 P.CODIGOPRODUTO, P.CODIGOSTATUSGTIN, P.PRODUCTDESCRIPTION, A.INDICADORCNAERESTRITIVO, C.NOME AS STATUSGTIN 
                                            FROM PRODUTO P (NOLOCK) 
                                            INNER JOIN ASSOCIADO A  (NOLOCK) ON A.CODIGO = P.CODIGOASSOCIADO
                                            INNER JOIN STATUSGTIN C (NOLOCK)  ON C.CODIGO = P.CODIGOSTATUSGTIN
                                            WHERE P.GLOBALTRADEITEMNUMBER =" + tradeItem.globaltradeitemnumber + @"
                                            AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN =" + GLN + @" AND X.CodigoAssociado = P.CodigoAssociado)
                                            AND P.CODIGOSTATUSGTIN NOT IN (7)";
                    List<dynamic> produtos = db.Select(sqlProduto, null);
                    if (produtos.Count() > 0)
                    {
                        if ((((IDictionary<String, object>)tradeItem).ContainsKey("productdescription") && tradeItem.productdescription != null && tradeItem.productdescription != produtos[0].productdescription) ||
                            (((IDictionary<String, object>)tradeItem).ContainsKey("codebrick") && tradeItem.codebrick != null || 
                             ((IDictionary<String, object>)tradeItem).ContainsKey("codeclass") && tradeItem.codeclass != null || 
                             ((IDictionary<String, object>)tradeItem).ContainsKey("codefamily") && tradeItem.codefamily != null || 
                             ((IDictionary<String, object>)tradeItem).ContainsKey("codesegment") && tradeItem.codesegment != null))
                        {
                            if (produtos[0].indicadorcnaerestritivo != null && produtos[0].indicadorcnaerestritivo != string.Empty  && produtos[0].indicadorcnaerestritivo == "1")
                            {
                                retorno += "Associado com CNAE restritivo, não permite alteração de status";
                            }
                            else
                            {
                                if (produtos[0].codigostatusgtin == 1)
                                {
                                    tradeItem.codigostatusgtin = 3;
                                    flReativado = true;
                                }

                                Log.WriteInfo("ProdutoBO", "UpdateCNPCore", Convert.ToInt64(produtos[0].codigoproduto));
                            }                            
                        }

                        
                        if (retorno == string.Empty)
                        {
                            tradeItem.CODIGOPRODUTO = produtos[0].codigoproduto;
                            //tradeItem.Remove("globaltradeitemnumber");
                            //tradeItem.globaltradeitemnumber = null;
                            ((IDictionary<String, object>)tradeItem).Remove("globaltradeitemnumber");
                            
                            db.Update(nomeTabela, nomePK, tradeItem);

                            if (additionaltradeitemidentificationList.Count() > 0)
                            {
                                string sqldel = "DELETE FROM [dbo].[ProdutoAgenciaReguladora] WHERE CodigoProduto = " + tradeItem.CODIGOPRODUTO.ToString();
                                db.Select(sqldel, null);
                                foreach (dynamic item in additionaltradeitemidentificationList)
                                {
                                    item.codigoproduto = tradeItem.CODIGOPRODUTO;
                                    string sql = @"INSERT INTO [dbo].[ProdutoAgenciaReguladora] ([CodigoProduto],[CodigoAgencia],[alternateItemIdentificationId])
                                           SELECT @codigoproduto, A.Codigo, @additionaltradeitemidentificationvalue FROM AGENCIAREGULADORA a
                                           WHERE A.NOME = @additionaltradeitemidentificationtypecode;
                                           SELECT @@ROWCOUNT;";
                                    int qtdagencia = db.Insert(sql, item, true);
                                    if (qtdagencia == 0)
                                        retorno += "Agência " + item.additionaltradeitemidentificationtypecode + " não cadastrada;\n";
                                }
                            }
                            if (flReativado)
                                tradeItem.statusgtin = "Reativado";
                        }
                    }
                    else
                    {
                        retorno += "GTIN " + tradeItem.gtin + " não cadastrado;\n";
                    }
                                       
                    if (retorno != string.Empty)
                    {
                        db.Rollback();
                    }
                    else
                    {
                        db.Commit();
                    }

                    return retorno;

                }
                catch (SqlException e)
                {
                    if (e.Number == 2627)
                    {
                        db.Rollback();
                        return "GTIN já cadastrado";
                    }
                    db.Rollback();
                    throw new GS1TradeException("Não foi possível cadastrar o Produto informado.", e);
                }
                catch (Exception e)
                {
                    db.Rollback();
                    throw new GS1TradeException("Não foi possível cadastrar o Produto informado.", e);
                }
            }
        }

        [GS1CNPAttribute(false)]
        public object PegarGTINValidoCore(string codigotipogtin, string codigoassociado)
        {
                       
            //Verifica se a Descrição do Produto já foi cadastrada
            string check = @"SELECT NUMEROPREFIXO FROM (
	                            SELECT MIN(NUMEROPREFIXO) AS NUMEROPREFIXO
	                            FROM PREFIXOLICENCAASSOCIADO PLA
	                            INNER JOIN LICENCA L ON L.CODIGO = PLA.CODIGOLICENCA
	                            INNER JOIN TipoGTINLicenca TGL ON TGL.CodigoLicenca = L.Codigo
	                            WHERE TGL.CODIGOTIPOGTIN <> 1 
	                            AND TGL.CODIGOTIPOGTIN = " + codigotipogtin +
                                @" AND PLA.CODIGOSTATUSPREFIXO IN (1, 8) 
	                            AND PLA.CODIGOASSOCIADO = " + codigoassociado + @"
                                AND (
								            (TGL.CODIGOTIPOGTIN = 2 
									            AND								
									            (CASE LEN(NUMEROPREFIXO)	WHEN 6  THEN 100000
																            WHEN 7  THEN 10000
																            WHEN 8  THEN 1000
																            WHEN 9  THEN 100
																            WHEN 10 THEN 10
																            ELSE 0 END > (SELECT COALESCE(MAX(X.CODITEM),0) FROM PRODUTO X WHERE X.NRPREFIXO = PLA.NUMEROPREFIXO
                                                                                            AND X.CODIGOTIPOGTIN = 2)))
								            OR

								            (TGL.CODIGOTIPOGTIN <> 2 AND 
										            (CASE LEN(NUMEROPREFIXO)	WHEN 7  THEN 99999
																            WHEN 8  THEN 9999
																            WHEN 9  THEN 999
																            WHEN 10 THEN 99
																            WHEN 11 THEN 9
																            ELSE 0 END > (SELECT COALESCE(MAX(X.CODITEM),0) FROM PRODUTO X WHERE X.NRPREFIXO = PLA.NUMEROPREFIXO
                                                                                            AND X.CODIGOTIPOGTIN = TGL.CODIGOTIPOGTIN)))
								
						            )";

            check += @"UNION 

	                            SELECT MIN(NUMEROPREFIXO) AS NUMEROPREFIXO
	                            FROM PREFIXOLICENCAASSOCIADO PLA
	                            INNER JOIN LICENCA L ON L.CODIGO = PLA.CODIGOLICENCA
	                            INNER JOIN TipoGTINLicenca TGL ON TGL.CodigoLicenca = L.Codigo
	                            WHERE TGL.CODIGOTIPOGTIN = 1 
	                            AND NUMEROPREFIXO NOT IN (SELECT Z.GLOBALTRADEITEMNUMBER FROM Produto Z WHERE Z.CODIGOTIPOGTIN = 1 AND Z.GLOBALTRADEITEMNUMBER IS NOT NULL)
	                            AND TGL.CODIGOTIPOGTIN = " + codigotipogtin +
                                @" AND PLA.CODIGOSTATUSPREFIXO IN (1, 8) 
	                            AND PLA.CODIGOASSOCIADO = " + codigoassociado;
            check += @")AS DADOS
                            WHERE NUMEROPREFIXO IS NOT NULL";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic retorno = db.Select(check, null);
                if (retorno != null && retorno.Count > 0)
                {
                    return retorno;
                }
                else
                {
                    return null;
                }
            }

        }

        public dynamic GeradorGTINCore(string prefixo, int tamanho)
        {

            dynamic retorno = VerificarSequenciaPrefixoProdutoCore(prefixo, tamanho);

            if (retorno != null)
            {
                string ultimogerado = retorno[0].ultimoglngerado;
                string sequencialGerado = String.Empty, GTINGerado, digitoVerificador;
                int tamanhoGlnSemVerificador = (tamanho - 1);
                int tamanhoPrefixo = prefixo.ToString().Length;
                int tamanhoSequencial = tamanhoGlnSemVerificador - tamanhoPrefixo, sequencial;
                string globaltradeitemnumber = String.Empty;
                dynamic numerosGerados = new ExpandoObject();

                if (ultimogerado == null || ultimogerado == "")
                    sequencial = 1;
                else
                {
                    sequencial = Convert.ToInt32(ultimogerado) + 1;

                    if ((tamanhoPrefixo == 11 && sequencial.ToString().Length > 1) || (tamanhoPrefixo == 10 && sequencial.ToString().Length > 2) ||
                        (tamanhoPrefixo == 9 && sequencial.ToString().Length > 3) || (tamanhoPrefixo == 8 && sequencial.ToString().Length > 4) ||
                        (tamanhoPrefixo == 7 && sequencial.ToString().Length > 5))
                    {

                        globaltradeitemnumber = "";

                        //throw new GS1TradeException("Quantidade de produtos máxima atingida.");
                        return null;
                    }
                }

                //Gera Número sequencial
                while (sequencialGerado.ToString().Length < tamanhoSequencial - sequencial.ToString().Length)
                {
                    sequencialGerado += "0";
                }

                sequencialGerado += sequencial;
                numerosGerados.coditem = sequencialGerado;

                GTINGerado = prefixo + sequencialGerado;

                //Gera dígito verificador
                string verificador = GeraDigitoVerificador(GTINGerado);
                GTINGerado = GTINGerado + verificador;
                numerosGerados.globaltradeitemnumber = GTINGerado;
                numerosGerados.nrprefixo = prefixo;
                //$scope.onSave($scope.itemForm, 'GerarGTIN');
                return numerosGerados;
            }
            else
            {
                return null;
            }
        }

        private dynamic VerificarSequenciaPrefixoProdutoCore(string prefixo, int tamanho)
        {           
            int codigotipogtin = 0;
            if (tamanho == 13) { codigotipogtin = 3; }
            if (tamanho == 14) { codigotipogtin = 4; }



                string sql = @"SELECT MAX(CODITEM) AS ULTIMOGLNGERADO
                                FROM PRODUTO P (NOLOCK) 
                                LEFT JOIN ASSOCIADO A (NOLOCK)  ON (A.CODIGO = P.CODIGOASSOCIADO)
                                LEFT JOIN PREFIXOLICENCAASSOCIADO PLU (NOLOCK)  ON (PLU.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                WHERE P.NRPREFIXO = " + prefixo;                

                if (codigotipogtin > 0)
                {
                    sql += " AND P.CODIGOTIPOGTIN=" + codigotipogtin.ToString();
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();
                    dynamic param = null;
                    dynamic retorno = db.Select(sql, null);
                    if (retorno != null && retorno.Count > 0)
                    {
                        return retorno;
                    }
                    else
                    {
                        return null;
                    }
                }            
        }
      

        [GS1CNPAttribute(false)]
        public void UpdateCNPLifeSpan(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();

                    if (((IDictionary<String, object>)parametro).ContainsKey("minimumtradeitemlifespanfromtimeofproduction"))
                    {
                        string sql = @"UPDATE PRODUTO SET minimumTradeItemLifespanFromTimeOfProduction = @minimumtradeitemlifespanfromtimeofproduction
                           FROM PRODUTO
                           WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                           AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)";

                        List<dynamic> retorno = db.Select(sql, parametro);
                    }
                }
                catch (Exception e)
                {                    
                    throw new GS1TradeException("Não foi possível cadastrar o Validade informada.", e);
                }
            }
        }


        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPLifeSpan(string GTINS, string GLN)
        {

            string sql = @"SELECT P.CodigoProduto, P.GLOBALTRADEITEMNUMBER, P.minimumtradeitemlifespanfromtimeofproduction                                        
                            FROM PRODUTO P WITH (NOLOCK)                                                        
                            WHERE P.GLOBALTRADEITEMNUMBER in (" + GTINS + @")
                            AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = P.CodigoAssociado)
                            AND P.CODIGOSTATUSGTIN <> 7";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.gln = GLN;
                List<dynamic> produtos = db.Select(sql, param);
                return produtos;
            }

        }


        [GS1CNPAttribute(false)]
        public void UpdateCNPCompra(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();

                    //if (((IDictionary<String, object>)parametro).ContainsKey("minimumtradeitemlifespanfromtimeofproduction"))
                    //{
                        string sql = @"UPDATE PRODUTO SET   packagingTypeCode           = @packagingtypedescription,
                                                            orderingUnitOfMeasure       = @orderingUnitOfMeasure,
                                                            orderQuantityMinimum        = @orderQuantityMinimum,
                                                            orderQuantityMultiple       = @orderQuantityMultiple,
                                                            startAvailabilityDateTime   = @startAvailabilityDateTime,
                                                            endAvailabilityDateTime     = @endAvailabilityDateTime

                                       FROM PRODUTO
                                       WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                                       AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)";

                        List<dynamic> retorno = db.Select(sql, parametro);
                    //}
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Não foi possível cadastrar o Validade informada.", e);
                }
            }
        }

        [GS1CNPAttribute(false)]
        public void InsertCNPCompra(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();

                    string sql = string.Empty;

                    if (((IDictionary<String, object>)parametro).ContainsKey("packagingtypedescription") && parametro.packagingtypedescription != null)
                    {
                        sql += " packagingTypeCode           = @packagingtypedescription,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("orderingUnitOfMeasure") && parametro.orderingUnitOfMeasure != null)
                    {
                        sql += " orderingUnitOfMeasure       = @orderingUnitOfMeasure,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("orderQuantityMinimum") && parametro.orderQuantityMinimum != null)
                    {
                        sql += " orderQuantityMinimum        = @orderQuantityMinimum,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("orderQuantityMultiple") && parametro.orderQuantityMultiple != null)
                    {
                        sql += " orderQuantityMultiple       = @orderQuantityMultiple,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("startAvailabilityDateTime") && parametro.startAvailabilityDateTime != null)
                    {
                        sql += " startAvailabilityDateTime   = @startAvailabilityDateTime,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("endAvailabilityDateTime") && parametro.endAvailabilityDateTime != null)
                    {
                        sql += " endAvailabilityDateTime     = @endAvailabilityDateTime,";
                    }

                    if (sql != string.Empty)
                    {
                        sql = "UPDATE PRODUTO SET " + sql.Substring(0, sql.Length - 1) + @" FROM PRODUTO
                                WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)";

                        List<dynamic> retorno = db.Select(sql, parametro);
                    }
                    else
                    {
                        throw new GS1TradeException("Não foi possível cadastrar o informação de compra informada. Nenhuma informação encontrada.");
                    }
                    
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Não foi possível cadastrar o Validade informada.", e);
                }
            }
        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPCompra(string GTINS, string GLN)
        {

            string sql = @"SELECT P.CodigoProduto, P.GLOBALTRADEITEMNUMBER, P.packagingTypeCode, P.orderingUnitOfMeasure, P.orderQuantityMinimum, P.orderQuantityMultiple, P.startAvailabilityDateTime, P.endAvailabilityDateTime                                        
                            FROM PRODUTO P WITH (NOLOCK)                                                        
                            WHERE P.GLOBALTRADEITEMNUMBER in (" + GTINS + @")
                            AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = P.CodigoAssociado)
                            AND P.CODIGOSTATUSGTIN <> 7";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.gln = GLN;
                List<dynamic> produtos = db.Select(sql, param);
                return produtos;
            }

        }


       

        [GS1CNPAttribute(false)]
        public void InsertProdutoDutyFeeTax(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();
                    string sql = @"INSERT INTO [dbo].[ProdutoDutyFeeTax] ([CodigoProduto],[dutyFeeTaxTypeCode],[dutyFeeTaxTypeDescription],[dutyFeeTaxAmount],[dutyFeeTaxBasis],[dutyFeeTaxCategoryCode], [dutyFeeTaxCountrySubdivisionCode]
                                                                         ,[dutyFeeTaxExemptPartyRoleCode], [dutyFeeTaxRate], [dutyFeeTaxReductionCriteriaDescription])
                                   SELECT TOP 1 Codigoproduto, @dutyfeetaxtypecode, @dutyfeetaxtypedescription ,@dutyfeetaxamount, @dutyfeetaxbasis, @dutyfeetaxcategorycode, @dutyfeetaxcountrysubdivisioncode
                                          , @dutyfeetaxexemptpartyrolecode, @dutyfeetaxrate, @dutyfeetaxreductioncriteriadescription
                                    FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                                   AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)";

                    List<dynamic> retorno = db.Select(sql, parametro);                    
                }
                catch (Exception e)
                {                   
                    throw new GS1TradeException("Não foi possível cadastrar.", e);
                }
            }
        }


        [GS1CNPAttribute(false)]
        public void DeleteProdutoDutyFeeTax(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();
                    string sql = @"DELETE FROM [dbo].[ProdutoDutyFeeTax] WHERE CODIGOPRODUTO IN (
                                   SELECT TOP 1 Codigoproduto
                                    FROM PRODUTO WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                                   AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)
                                  )";

                    List<dynamic> retorno = db.Select(sql, parametro);
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Não foi possível cadastrar.", e);
                }
            }
        }


        [GS1CNPAttribute(false)]
        public List<dynamic> GetProdutoDutyFeeTax(string GTINS, string GLN)
        {
            string sql = @"SELECT P.GLOBALTRADEITEMNUMBER
                                  ,A.[Codigo]
                                  ,A.[CodigoProduto]
                                  ,A.[dutyFeeTaxAmount]
                                  ,A.[dutyFeeTaxBasis]
                                  ,A.[dutyFeeTaxCategoryCode]
                                  ,A.[dutyFeeTaxCountrySubdivisionCode]
                                  ,A.[dutyFeeTaxExemptPartyRoleCode]
                                  ,A.[dutyFeeTaxRate]
                                  ,A.[dutyFeeTaxReductionCriteriaDescription]
                                  ,A.[dutyFeeTaxTypeCode]
                                  ,A.[dutyFeeTaxTypeDescription]
                          FROM [dbo].[ProdutoDutyFeeTax] A (NOLOCK) 
                          INNER JOIN Produto P  (NOLOCK) on P.CodigoProduto = A.CodigoProduto 
                          WHERE P.GLOBALTRADEITEMNUMBER in (" + GTINS + @")
                          AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = P.CodigoAssociado)
                          AND P.CODIGOSTATUSGTIN <> 7
                          ORDER BY dutyFeeTaxTypeCode";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.gln = GLN;
                List<dynamic> retorno = db.Select(sql, param);
                return retorno;
            }
        }

        [GS1CNPAttribute(false)]
        public List<dynamic> GetProdutoLogistica(string GTINS, string GLN)
        {
            string sql = @"SELECT   A.GLOBALTRADEITEMNUMBER
                                    ,A.CODIGOPRODUTO
                                    ,B.NOME tradeItemUnitDescriptorCode
                                    ,A.totalQuantityOfNextLowerLevelTradeItem
                                    ,A.quantityOfCompleteLayersContainedInATradeItem
                                    ,A.quantityOfLayersPerPallet
                                    ,A.quantityOfTradeItemContainedInACompleteLayer
                                    ,A.quantityOfTradeItemsPerPalletLayer
                                    ,A.stackingFactor
                                    ,A.depth
                                    ,A.depthMeasurementUnitCode
                                    ,A.height
                                    ,A.heightMeasurementUnitCode
                                    ,A.netContent
                                    ,A.netContentMeasurementUnitCode
                                    ,A.width
                                    ,A.widthMeasurementUnitCode
                                    ,A.grossWeight
                                    ,A.grossWeightMeasurementUnitCode
                                    ,A.netWeight
                                    ,A.netWeightMeasurementUnitCode
                                    ,A.storageHandlingTempMinimumUOM
                                    ,A.storageHandlingTemperatureMaximum
                                    ,A.storageHandlingTemperatureMaximumunitOfMeasure
                                    ,A.isDangerousSubstanceIndicated

                          FROM PRODUTO A
                          LEFT JOIN TIPOPRODUTO B ON B.CODIGO = A.CODIGOTIPOPRODUTO 
                          WHERE A.GLOBALTRADEITEMNUMBER in (" + GTINS + @")
                          AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = a.CodigoAssociado)
                          AND A.CODIGOSTATUSGTIN <> 7";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.gln = GLN;
                List<dynamic> retorno = db.Select(sql, param);
                return retorno;
            }
        }

        [GS1CNPAttribute(false)]
        public void InsertCNPLogistica(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                try
                {
                    db.Open();

                    string sql = string.Empty;

                    if (((IDictionary<String, object>)parametro).ContainsKey("packagingtypedescription") && parametro.packagingtypedescription != null)
                    {
                        sql += " CodigoTipoProduto           = (SELECT CODIGO FROM TIPOPRODUTO WHERE NOME = @packagingtypedescription)";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("totalquantityofnextlowerleveltradeitem") && parametro.totalquantityofnextlowerleveltradeitem != null)
                    {
                        sql += " totalquantityofnextlowerleveltradeitem           = @totalquantityofnextlowerleveltradeitem,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("quantityofcompletelayerscontainedinatradeitem") && parametro.quantityofcompletelayerscontainedinatradeitem != null)
                    {
                        sql += " quantityOfCompleteLayersContainedInATradeItem           = @quantityofcompletelayerscontainedinatradeitem,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("quantityoflayersperpallet") && parametro.quantityoflayersperpallet != null)
                    {
                        sql += " quantityOfLayersPerPallet           = @quantityoflayersperpallet,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("quantityoftradeitemcontainedinacompletelayer") && parametro.quantityoftradeitemcontainedinacompletelayer != null)
                    {
                        sql += " quantityOfTradeItemContainedInACompleteLayer           = @quantityoftradeitemcontainedinacompletelayer,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("quantityoftradeitemsperpalletlayer") && parametro.quantityoftradeitemsperpalletlayer != null)
                    {
                        sql += " quantityOfTradeItemsPerPalletLayer           = @quantityoftradeitemsperpalletlayer,";
                    }



                    if (((IDictionary<String, object>)parametro).ContainsKey("stackingfactor") && parametro.stackingfactor != null)
                    {
                        sql += " stackingFactor           = @stackingfactor,";
                    }



                    if (((IDictionary<String, object>)parametro).ContainsKey("depth") && parametro.depth != null)
                    {
                        sql += " depth           = @depth,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("depthmeasurementunitcode") && parametro.depthmeasurementunitcode != null)
                    {
                        sql += " depthMeasurementUnitCode           = @depthmeasurementunitcode,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("height") && parametro.height != null)
                    {
                        sql += " height           = @height,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("heightmeasurementunitcode") && parametro.heightmeasurementunitcode != null)
                    {
                        sql += " heightMeasurementUnitCode           = @heightmeasurementunitcode,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("netcontent") && parametro.netcontent != null)
                    {
                        sql += " netContent           = @netcontent,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("netcontentmeasurementunitcode") && parametro.netcontentmeasurementunitcode != null)
                    {
                        sql += " netContentMeasurementUnitCode           = @netcontentmeasurementunitcode,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("width") && parametro.width != null)
                    {
                        sql += " width           = @width,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("widthmeasurementunitcode") && parametro.widthmeasurementunitcode != null)
                    {
                        sql += " widthMeasurementUnitCode           = @widthmeasurementunitcode,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("grossweight") && parametro.grossweight != null)
                    {
                        sql += " grossWeight           = @grossweight,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("grossweightmeasurementunitcode") && parametro.grossweightmeasurementunitcode != null)
                    {
                        sql += " grossWeightMeasurementUnitCode           = @grossweightmeasurementunitcode,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("netweight") && parametro.netweight != null)
                    {
                        sql += " netWeight           = @netweight,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("netweightmeasurementunitcode") && parametro.netweightmeasurementunitcode != null)
                    {
                        sql += " netWeightMeasurementUnitCode           = @netweightmeasurementunitcode,";
                    }


                    if (((IDictionary<String, object>)parametro).ContainsKey("storagehandlingtempminimumuom") && parametro.storagehandlingtempminimumuom != null)
                    {
                        sql += " storageHandlingTempMinimumUOM           = @storagehandlingtempminimumuom,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("storagehandlingtemperaturemaximum") && parametro.storagehandlingtemperaturemaximum != null)
                    {
                        sql += " storageHandlingTemperatureMaximum           = @storagehandlingtemperaturemaximum,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("storagehandlingtemperaturemaximumunitofmeasure") && parametro.storagehandlingtemperaturemaximumunitofmeasure != null)
                    {
                        sql += " storageHandlingTemperatureMaximumunitOfMeasure           = @storagehandlingtemperaturemaximumunitofmeasure,";
                    }

                    if (((IDictionary<String, object>)parametro).ContainsKey("isdangeroussubstanceindicated") && parametro.isdangeroussubstanceindicated != null)
                    {
                        sql += " isDangerousSubstanceIndicated           = @isdangeroussubstanceindicated,";
                    }

                 

                    if (sql != string.Empty)
                    {
                        sql = "UPDATE PRODUTO SET " + sql.Substring(0, sql.Length - 1) + @" FROM PRODUTO
                                WHERE GLOBALTRADEITEMNUMBER = @gtin AND CodigoStatusGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = PRODUTO.CodigoAssociado)";

                        List<dynamic> retorno = db.Select(sql, parametro);
                    }
                    else
                    {
                        throw new GS1TradeException("Não foi possível cadastrar o informação de compra informada. Nenhuma informação encontrada.");
                    }

                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Não foi possível cadastrar o Validade informada.", e);
                }
            }
        }


        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPInformacoesNutricionais(string GTINS, string GLN)
        {

            string sql = @"SELECT  P.GLOBALTRADEITEMNUMBER, P.CODIGOPRODUTO, PH.CODIGO CODIOHEADER, PH.DAILYVALUEINTAKEREFERENCE, PH.SERVINGSIZEDESCRIPTION
                            ,PD.DAILYVALUEINTAKEPERCENT,  PD.MEASUREMENTPRECISIONCODE, PD.NUTRIENTTYPECODE, PD.QUANTITYCONTAINED
                            FROM PRODUTONUTRIENTHEADER PH (NOLOCK) 
                            INNER JOIN PRODUTONUTRIENTDETAIL PD  (NOLOCK) ON PD.CODIGOPRODUTO = PH.CODIGOPRODUTO
                            INNER JOIN PRODUTO P (NOLOCK)  ON P.CODIGOPRODUTO = PH.CODIGOPRODUTO                                                        
                            WHERE P.GLOBALTRADEITEMNUMBER in (" + GTINS + @")
                            AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = P.CodigoAssociado)
                            AND P.CODIGOSTATUSGTIN <> 7
                            ORDER BY GLOBALTRADEITEMNUMBER";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.gln = GLN;
                List<dynamic> produtos = db.Select(sql, param);
                return produtos;
            }

        }

        [GS1CNPAttribute(false)]
        public int InsertCNPInformacoesNutricionaisHeader(dynamic parametro)
        {            
            using (Database db = new Database("BD"))
            {
                db.Open();
                string sql = @"INSERT INTO PRODUTONUTRIENTHEADER (CODIGOPRODUTO, DAILYVALUEINTAKEREFERENCE, SERVINGSIZEDESCRIPTION) 
                                SELECT TOP 1 P.CODIGOPRODUTO, @dailyvalueintakereference, @servingsizedescription
                                FROM PRODUTO P WHERE P.GLOBALTRADEITEMNUMBER = @gtin AND P.CODIGOSTATUSGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LOCALIZACAOFISICA X WHERE X.GLN = @gln AND X.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                AND NOT EXISTS (SELECT TOP 1 CODIGOPRODUTO FROM PRODUTONUTRIENTHEADER Y WHERE Y.CODIGOPRODUTO = P.CODIGOPRODUTO);
                                SELECT @@ROWCOUNT";

                return db.ExecuteCommand(sql, parametro);                
            }
        }

        [GS1CNPAttribute(false)]
        public int InsertCNPInformacoesNutricionaisDetail(dynamic parametro)
        {            
            using (Database db = new Database("BD"))
            {
                db.Open();
                string sql = @"INSERT INTO DBO.PRODUTONUTRIENTDETAIL (CODIGOPRODUTO, DAILYVALUEINTAKEPERCENT, MEASUREMENTPRECISIONCODE, NUTRIENTTYPECODE, QUANTITYCONTAINED)
                                SELECT TOP 1 P.CODIGOPRODUTO, @dailyvalueintakepercent, @measurementprecisioncode, @nutrienttypecode, @quantitycontained
                                FROM PRODUTO P WHERE P.GLOBALTRADEITEMNUMBER = @gtin AND P.CODIGOSTATUSGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LOCALIZACAOFISICA X WHERE X.GLN = @gln AND X.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                AND NOT EXISTS (SELECT TOP 1 CODIGOPRODUTO FROM PRODUTONUTRIENTDETAIL Y WHERE Y.CODIGOPRODUTO = P.CODIGOPRODUTO AND Y.nutrienttypecode = @nutrienttypecode);
                                SELECT @@ROWCOUNT";
                return db.ExecuteCommand(sql, parametro);               
            }
        }


        [GS1CNPAttribute(false)]
        public int UpdateCNPInformacoesNutricionaisHeader(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();

                string sql = @"DELETE FROM PRODUTONUTRIENTHEADER WHERE CODIGOPRODUTO IN (
                                SELECT TOP 1 P.CODIGOPRODUTO
                                FROM PRODUTO P WHERE P.GLOBALTRADEITEMNUMBER = @gtin AND P.CODIGOSTATUSGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LOCALIZACAOFISICA X WHERE X.GLN = @gln AND X.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                )";

                db.Select(sql, parametro);

                return InsertCNPInformacoesNutricionaisHeader(parametro);
            }
        }

        [GS1CNPAttribute(false)]
        public int UpdateCNPInformacoesNutricionaisDetail(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();
                string sql = @"DELETE FROM PRODUTONUTRIENTDETAIL WHERE CODIGOPRODUTO IN (
                                SELECT TOP 1 P.CODIGOPRODUTO
                                FROM PRODUTO P WHERE P.GLOBALTRADEITEMNUMBER = @gtin AND P.CODIGOSTATUSGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LOCALIZACAOFISICA X WHERE X.GLN = @gln AND X.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                )";
                db.Select(sql, parametro);
                return InsertCNPInformacoesNutricionaisDetail(parametro);
            }
        }


        [GS1CNPAttribute(false)]
        public List<dynamic> GetCNPInformacoesAlergenos(string GTINS, string GLN)
        {

            string sql = @"SELECT P.GLOBALTRADEITEMNUMBER, P.CODIGOPRODUTO, AT.SIGLA, PA.LEVELOFCONTAINMENTCODE
                            FROM PRODUTOALLERGEN PA (NOLOCK) 
                            INNER JOIN ALLERGENRELATEDINFORMATION AR  (NOLOCK) ON AR.CODIGO = PA.CODIGOALLERGENRELATEDINFORMATION
                            INNER JOIN ALLERGENTYPECODE AT (NOLOCK)  ON AT.CODIGO = AR.CODIGOALLERGENTYPECODE
                            INNER JOIN PRODUTO P (NOLOCK)  ON P.CODIGOPRODUTO = PA.CODIGOPRODUTO                                                         
                            WHERE P.GLOBALTRADEITEMNUMBER in (" + GTINS + @")
                            AND EXISTS (SELECT TOP 1 X.GLN FROM LocalizacaoFisica X WHERE X.GLN = @gln AND X.CodigoAssociado = P.CodigoAssociado)
                            AND P.CODIGOSTATUSGTIN <> 7
                            ORDER BY GLOBALTRADEITEMNUMBER";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.gln = GLN;
                List<dynamic> produtos = db.Select(sql, param);
                return produtos;
            }
        }

        [GS1CNPAttribute(false)]
        public int InsertCNPInformacoesAlergenos(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();
                string sql = @"INSERT INTO AllergenRelatedInformation (CodigoAllergenTypeCode) 
                                SELECT TOP 1 CODIGO FROM AllergenTypeCode WHERE SIGLA = @allergentypecode;";
                int codigoallergenrelatedinformation = db.Insert(sql, parametro, true);

                if (codigoallergenrelatedinformation > 0)
                {
                    parametro.codigoallergenrelatedinformation = codigoallergenrelatedinformation;
                    string sql2 = @"INSERT INTO ProdutoAllergen (CODIGOPRODUTO, CODIGOALLERGENRELATEDINFORMATION, LEVELOFCONTAINMENTCODE) 
                                SELECT TOP 1 P.CODIGOPRODUTO, @codigoallergenrelatedinformation, @levelofcontainmentcode
                                FROM PRODUTO P WHERE P.GLOBALTRADEITEMNUMBER = @gtin AND P.CODIGOSTATUSGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LOCALIZACAOFISICA X WHERE X.GLN = @gln AND X.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                AND NOT EXISTS (SELECT TOP 1 PA.CODIGO
                                                FROM PRODUTOALLERGEN PA
                                                INNER JOIN ALLERGENRELATEDINFORMATION AR ON AR.CODIGO = PA.CODIGOALLERGENRELATEDINFORMATION
                                                INNER JOIN ALLERGENTYPECODE AT ON AT.CODIGO = AR.CODIGOALLERGENTYPECODE
                                                WHERE PA.CODIGOPRODUTO = P.CODIGOPRODUTO
                                                AND SIGLA = @allergentypecode);
                                SELECT @@ROWCOUNT";
                    
                     return db.ExecuteCommand(sql2, parametro);
                }

                return 0;
                
            }
        }

        [GS1CNPAttribute(false)]
        public int UpdateCNPInformacoesAlergenos(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();
                string sql = @"DELETE FROM AllergenRelatedInformation WHERE CODIGO IN (
                                SELECT PA.CODIGOALLERGENRELATEDINFORMATION
                                FROM PRODUTO P
                                INNER JOIN PRODUTOALLERGEN PA ON PA.CODIGOPRODUTO = P.CODIGOPRODUTO
                                WHERE P.GLOBALTRADEITEMNUMBER = @gtin AND P.CODIGOSTATUSGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LOCALIZACAOFISICA X WHERE X.GLN = @gln AND X.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                )";
                db.Select(sql, parametro);


                string sql2 = @"DELETE FROM PRODUTOALLERGEN WHERE CODIGOPRODUTO IN (
                                SELECT TOP 1 P.CODIGOPRODUTO
                                FROM PRODUTO P WHERE P.GLOBALTRADEITEMNUMBER = @gtin AND P.CODIGOSTATUSGTIN <> 7
                                AND EXISTS (SELECT TOP 1 X.GLN FROM LOCALIZACAOFISICA X WHERE X.GLN = @gln AND X.CODIGOASSOCIADO = P.CODIGOASSOCIADO)
                                )";
                db.Select(sql2, parametro);

                return InsertCNPInformacoesAlergenos(parametro);
            }
        }
       
        #endregion

    }

}
