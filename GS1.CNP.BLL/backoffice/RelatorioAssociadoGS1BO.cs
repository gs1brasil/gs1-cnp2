using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioAssociadoGS1BO")]
    class RelatorioAssociadoGS1BO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return ""; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarListaLicenca(dynamic parametro)
        {
            string sql = "SELECT Codigo as 'key', Nome as value FROM Licenca (NOLOCK)  WHERE Status = 1 ORDER BY Nome ASC";
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (GS1.CNP.DAL.Database db = new DAL.Database("BD"))
            {
                db.Open();

                List<dynamic> retorno = db.Select(sql, param);

                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarListaStatusAssociado(dynamic parametro)
        {
            string sql = "SELECT Codigo as 'key', Nome as value FROM StatusAssociado (NOLOCK)  WHERE Status = 1 ORDER BY Nome ASC";
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (GS1.CNP.DAL.Database db = new DAL.Database("BD"))
            {
                db.Open();

                List<dynamic> retorno = db.Select(sql, param);

                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("AcessarRelatorioAssociadoGS1", true, true)]
        public string relatorioAssociadoGS1(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;
            

            if (user != null)
            {
                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
                parametro.where.associado = null;
                
                if (associado != null)
                {
                    parametro.where.associado = associado.codigo;
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;
                    string query = @"SELECT a.CPFCNPJ															as 'cpf_cnpj'
										, a.Nome																as 'nome_razao_social'
										, sa.Nome																as 'status'
										, cam.Nome																as 'contato_master'
										, CASE 
		                                    WHEN LEN(emailsConcatenados.emails) > 0 THEN LEFT(emailsConcatenados.emails, LEN(emailsConcatenados.emails) - 1)	
	                                      END as 'email_contato'
										, la.DataAniversario													AS 'data_vencimento_taxa'
										, a.CEP																	AS 'cep'
										, a.Endereco															AS 'endereco'
										, a.Complemento															AS 'complemento'
										, a.Bairro																AS 'bairro'
										, a.Cidade																AS 'cidade'
										, a.UF																	AS 'uf'
										, a.DataAtualizacaoCRM													AS 'ultima_sincronizacao'
										, a.InscricaoEstadual													AS 'inscricao_estadual'
										, l.Nome																AS 'licenca'
										, pla.NumeroPrefixo														AS 'prefixo_licenca'
										, la.Status																AS 'status_licenca'
									FROM Associado a
                                        left join StatusAssociado AS sa on (a.CodigoStatusAssociado = sa.Codigo)
										left join LicencaAssociado AS la on (a.Codigo = la.CodigoAssociado)
										left join Licenca AS l on (la.CodigoLicenca = l.Codigo)
										left join PrefixoLicencaAssociado pla on (la.CodigoLicenca = pla.CodigoLicenca and la.CodigoAssociado = pla.CodigoAssociado)
										left join ContatoAssociado AS cam on (a.Codigo = cam.CodigoAssociado and cam.IndicadorPrincipal = 1)
										CROSS APPLY (
											SELECT ca.Email + ';'
											FROM   ContatoAssociado AS ca
											WHERE  a.Codigo = ca.CodigoAssociado
											FOR XML PATH(''), TYPE
										) emailsXml (email)
										CROSS APPLY (
											SELECT emailsXml.email.value('.', 'NVARCHAR(MAX)')
										) emailsConcatenados (emails)
									WHERE 1 = 1
										AND (a.Codigo = @associado OR @associado IS NULL)
										AND a.nome LIKE ('%' + ISNULL(@nome, '') + '%')
										AND (l.Codigo = @licenca OR @licenca IS NULL)
										AND (a.CodigoStatusAssociado = @status OR @status IS NULL)
									ORDER BY a.Nome ASC";

                    if (parametro != null && parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
