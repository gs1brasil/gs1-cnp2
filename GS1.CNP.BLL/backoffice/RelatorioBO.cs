﻿using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioBO")]
    class RelatorioBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return ""; }
        }

        [GS1CNPAttribute("RelatorioLogErro")]
        public object relatorioLogErro(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");
            if (user != null)
            {
                //Database db = new Database("BD");
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        parametro.campos.datainicial = parametro.campos.datainicial.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        parametro.campos.datafinal = parametro.campos.datafinal.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    object retorno = db.ExecuteProcedure("PROC_REL_LOGERRO", param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }

        [GS1CNPAttribute("RelatorioLogMonitoramentoAcao")]
        public object relatorioLogMonitoramentoAcao(dynamic parametro)
        {
            object user = this.getSession("USUARIO_LOGADO");
            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        parametro.campos.datainicial = parametro.campos.datainicial.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        parametro.campos.datafinal = parametro.campos.datafinal.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    object retorno = db.ExecuteProcedure("PROC_REL_LOG_MONITORAMENTO", param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarUsuario(dynamic parametro)
        {

            string sql = "SELECT CODIGO, NOME FROM USUARIO WITH (NOLOCK) WHERE NOME LIKE + '%' + @nome + '%'";

            //Database db = new Database("BD");
            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> usuario = db.Select(sql, param);

                return JsonConvert.SerializeObject(usuario);
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarLogin(dynamic parametro)
        {

            string sql = "SELECT CODIGO, EMAIL FROM USUARIO WITH (NOLOCK) WHERE EMAIL LIKE + '%' + @login + '%'";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> email = db.Select(sql, param);

                return JsonConvert.SerializeObject(email);
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarRazaoSocial(dynamic parametro)
        {

            string sql = "SELECT CODIGO, NOME FROM ASSOCIADO WITH (NOLOCK) WHERE NOME LIKE + '%' + @nome + '%'";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> razaosocial = db.Select(sql, param);

                return JsonConvert.SerializeObject(razaosocial);
            }
        }

        [GS1CNPAttribute(false)]

        public object BuscarRazaoSocialComCAD(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, CAD, NOME
                           FROM ASSOCIADO WITH (NOLOCK)
                           WHERE NOME + ' - ' + CAD + ' - ' + CPFCNPJ LIKE '%' + @nome + '%'";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                List<dynamic> razaosocial = db.Select(sql, param);

                return JsonConvert.SerializeObject(razaosocial);
            }
        }

        [GS1CNPAttribute(false)]
        public object UsuariosDoAssociado(dynamic parametro)
        {

            string sql = @"SELECT U.CODIGO, U.NOME
	                        FROM USUARIO U 
	                        INNER JOIN ASSOCIADOUSUARIO AU ON AU.CODIGOUSUARIO = U.CODIGO
	                        INNER JOIN ASSOCIADO A ON A.CODIGO = AU.CODIGOASSOCIADO
	                        WHERE A.CODIGO = @associado
                            ORDER BY U.NOME ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro != null && parametro.campos != null && parametro.campos.associado != null && parametro.campos.associado.codigo != null)
                {
                    parametro.campos.associado = parametro.campos.associado.codigo;
                }

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> razaosocial = db.Select(sql, param);

                return JsonConvert.SerializeObject(razaosocial);
            }
        }
    }
}
