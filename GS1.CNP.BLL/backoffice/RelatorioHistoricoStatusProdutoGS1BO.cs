using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioHistoricoStatusProdutoGS1BO")]
    class RelatorioHistoricoStatusProdutoGS1BO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return ""; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarListaStatusGTIN(dynamic parametro)
        {
            string sql = "SELECT Codigo as 'key', Nome as value FROM StatusGTIN WHERE Status = 1  AND Codigo <> 7 ORDER BY Codigo ASC";
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (GS1.CNP.DAL.Database db = new DAL.Database("BD"))
            {
                db.Open();

                List<dynamic> retorno = db.Select(sql, param);

                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarListaTipoGTIN(dynamic parametro)
        {
            string sql = "SELECT Codigo as 'key', Nome as value FROM TipoGTIN WHERE Status = 1 ORDER BY Codigo ASC";
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (GS1.CNP.DAL.Database db = new DAL.Database("BD"))
            {
                db.Open();

                List<dynamic> retorno = db.Select(sql, param);

                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("AcessarRelatorioHistoricoStatusProdutoGS1", true, true)]
        public string relatorioHistoricoStatusProdutoGS1(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
                parametro.where.associado = null;

                if (associado != null)
                {
                    parametro.where.associado = associado.codigo;
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    db.CommandTimeout = 500;

                    dynamic param = null;
                    string query = @"SELECT * 
                                        FROM (
		                                        SELECT 1 as tbl
			                                        , a.CPFCNPJ										AS 'cpfcnpj'
			                                        , a.Nome										AS 'nome_razao_social'
			                                        , p.globalTradeItemNumber						AS 'gtin'
			                                        , p.NrPrefixo									AS 'prefixo'
			                                        , tg.Nome										AS 'tipo_gtin'
			                                        , p.brandName									AS 'marca_produto'
			                                        , sg.Nome										AS 'status_atual'
			                                        , br.Text										AS 'code_brick'
			                                        , ( 
					                                    SELECT TOP 1 ar.Nome
					                                    FROM AgenciaReguladora ar
					                                    WHERE ar.Codigo = par.CodigoAgencia 
				                                    )												AS 'agencia_registro'
			                                        , uc.Nome										AS 'usuario_inclusao'
			                                        , par.alternateItemIdentificationId				AS 'codigo_interno'
			                                        , p.productDescription							AS 'descricao_produto'
			                                        , p.DataInclusao								AS 'data_criacao'
			                                        , (
					                                    SELECT top 1 c.Nome
					                                    FROM CountryOfOriginISO3166 c
					                                    WHERE p.tradeItemCountryOfOrigin = c.CodigoPais
				                                    )												AS 'pais_origem'
				                                    , (
					                                    SELECT top 1 c.Nome
					                                    FROM CountryOfOriginISO3166 c
					                                    WHERE p.countryCode = c.CodigoPais
				                                    )												AS 'pais_destino'
			                                        , tp.Nome										AS 'tipo_produto'
			                                        , i.Nome										AS 'lingua'
			                                        , p.Estado										AS 'estado'
			                                        , CAST(p.width AS VARCHAR(12)) + ' ' + CAST(p.widthMeasurementUnitCode AS VARCHAR(12))			AS 'largura'
			                                        , CAST(p.depth AS VARCHAR(12)) + ' ' + CAST(p.depthMeasurementUnitCode AS VARCHAR(12))			AS 'profundidade'
			                                        , CAST(p.height AS VARCHAR(12)) + ' ' + CAST(p.heightMeasurementUnitCode AS VARCHAR(12))		AS 'altura'
			                                        , CAST(p.netWeight AS VARCHAR(12)) + ' ' + CAST(p.netWeightMeasurementUnitCode AS VARCHAR(12))	AS 'peso_liquido'
			                                        , CAST(p.grossWeight AS VARCHAR(12)) + ' ' + CAST(p.grossWeightMeasurementUnitCode AS VARCHAR(12))	AS 'peso_bruto'
			                                        , p.ipiPerc										AS 'ipi'
			                                        , tu.Nome										AS 'tipo_url'
			                                        , pu.URL										AS 'url'
			                                        , CASE p.IndicadorCompartilhaDados
					                                    WHEN 1 THEN 'Sim'
					                                    WHEN 0 THEN 'N�o'
					                                    ELSE ''
					                                    END											AS 'compartilha_dados'
			                                        , CAST(p.Observacoes AS VARCHAR(MAX))			AS 'observacoes'
			                                        , p.importClassificationValue					AS 'ncm'
			                                        , pinf.globalTradeItemNumber					AS 'gtin_inferior'
			                                        , pinf.productDescription						AS 'descricao_inferior'
			                                        , ph.Quantidade									AS 'qtd_itens_inferior'
			                                        , sg.Nome										AS 'status_historico'
			                                        , p.DataAlteracao								AS 'data_status_historico'
			                                        , p.DataSuspensao								AS 'data_suspensao'
			                                        , p.DataReativacao								AS 'data_reativacao'
			                                        , p.DataReutilizacao							AS 'data_reutilizacao'
			                                        , p.DataCancelamento							AS 'data_cancelamento'
			                                        , ua.Nome										AS 'usuario_alteracao'
		                                        FROM Produto p
			                                        LEFT JOIN Associado a ON (p.CodigoAssociado = a.Codigo)
                                                    LEFT JOIN PrefixoLicencaAssociado pla ON p.NrPrefixo = pla.NumeroPrefixo
			                                        LEFT JOIN TipoGTIN tg ON (p.CodigoTipoGTIN = tg.Codigo)
			                                        LEFT JOIN StatusGTIN sg ON (p.CodigoStatusGTIN = sg.Codigo)
			                                        LEFT JOIN tbBrick br ON (p.CodeBrick = br.CodeBrick)
			                                        LEFT JOIN Usuario uc ON (p.CodigoUsuarioCriacao = uc.Codigo)
			                                        LEFT JOIN Usuario ua ON (p.CodigoUsuarioAlteracao = ua.Codigo)
			                                        LEFT JOIN TipoProduto tp ON (p.CodigoTipoProduto = tp.Codigo)
			                                        LEFT JOIN Lingua i ON (p.CodigoLingua = i.Codigo)
			                                        LEFT JOIN ProdutoURL pu ON (p.CodigoProduto = pu.CodigoProduto)
			                                        LEFT JOIN TipoURL tu ON (pu.CodigoTipoURL = tu.Codigo)
			                                        LEFT JOIN ProdutoHierarquia ph ON (p.CodigoProduto = ph.CodigoProdutoSuperior)
			                                        LEFT JOIN Produto pinf ON (ph.CodigoProdutoInferior = pinf.CodigoProduto)
				                                    LEFT JOIN ProdutoAgenciaReguladora par ON (par.codigoproduto = p.CodigoProduto)

		                                        WHERE (a.Codigo = @associado OR @associado IS NULL)
                                                    AND sg.Codigo <> 7
                                                    AND (pla.CodigoStatusPrefixo <> 4 OR pla.CodigoStatusPrefixo IS NULL) 
			                                        AND a.Nome LIKE ('%' + ISNULL(@nome, '') + '%')
			                                        AND (p.globalTradeItemNumber = @gtin or @gtin IS NULL)
			                                        AND p.productDescription LIKE ('%' + ISNULL(@descricao, '') + '%')
			                                        AND (p.DataInclusao >= @inicio OR @inicio IS NULL) 
                                                    AND (p.DataInclusao < @fim OR @fim IS NULL) 
			                                        AND (sg.Codigo = @statusgtin OR @statusgtin IS NULL)
			                                        AND (tg.Codigo = @tipogtin OR @tipogtin IS NULL)
				                                    AND p.CodigoStatusGTIN <> ISNULL((
						                                    SELECT TOP 1 a.CodigoStatusGTIN 
						                                    FROM ProdutoHistorico a
						                                    WHERE p.CodigoProduto = a.CodigoProduto
						                                    ORDER BY a.DataHistorico DESC
				                                    ), 0)
			                                    GROUP BY a.CPFCNPJ
			                                        , a.Nome
			                                        , p.globalTradeItemNumber
			                                        , p.NrPrefixo
			                                        , tg.Nome
			                                        , p.brandName
			                                        , sg.Nome
			                                        , br.Text
			                                        , par.CodigoAgencia
			                                        , uc.Nome
			                                        , par.alternateItemIdentificationId
			                                        , p.productDescription
			                                        , p.DataInclusao
			                                        , p.tradeItemCountryOfOrigin
				                                    , p.countryCode
			                                        , tp.Nome
			                                        , i.Nome
			                                        , p.Estado
			                                        , p.width
				                                    , p.widthMeasurementUnitCode
			                                        , p.depth
				                                    , p.depthMeasurementUnitCode
			                                        , p.height
				                                    , p.heightMeasurementUnitCode
			                                        , p.netWeight
				                                    , p.netWeightMeasurementUnitCode
			                                        , p.grossWeight
				                                    , p.grossWeightMeasurementUnitCode
			                                        , p.ipiPerc
			                                        , tu.Nome
			                                        , pu.URL
			                                        , p.IndicadorCompartilhaDados
				                                    , CAST(p.Observacoes AS VARCHAR(MAX))
			                                        , p.importClassificationValue
			                                        , pinf.globalTradeItemNumber
			                                        , pinf.productDescription
			                                        , ph.Quantidade
				                                    , p.DataAlteracao
			                                        , p.DataSuspensao
			                                        , p.DataReativacao
			                                        , p.DataReutilizacao
			                                        , p.DataCancelamento
			                                        , ua.Nome
			

	                                        UNION ALL 

		                                        SELECT 2 as tbl
			                                        , a.CPFCNPJ										AS 'cpfcnpj'
			                                        , a.Nome										AS 'nome_razao_social'
			                                        , pat.globalTradeItemNumber						AS 'gtin'
			                                        , pat.NrPrefixo									AS 'prefixo'
			                                        , tg.Nome										AS 'tipo_gtin'
			                                        , p.brandName									AS 'marca_produto'
			                                        , sg.Nome										AS 'status_atual'
			                                        , br.Text										AS 'code_brick'
			                                        , ( 
					                                    SELECT TOP 1 ar.Nome
					                                    FROM AgenciaReguladora ar
					                                    WHERE ar.Codigo = par.CodigoAgencia	
				                                    )												AS 'agencia_registro'
			                                        , uc.Nome										AS 'usuario_inclusao'
			                                        , par.alternateItemIdentificationId				AS 'codigo_interno'
			                                        , p.productDescription							AS 'descricao_produto'
			                                        , p.DataInclusao								AS 'data_criacao'
			                                        , (
					                                    SELECT top 1 c.Nome
					                                    FROM CountryOfOriginISO3166 c
					                                    WHERE p.tradeItemCountryOfOrigin = c.CodigoPais
				                                    )												AS 'pais_origem'
				                                    , (
					                                    SELECT top 1 c.Nome
					                                    FROM CountryOfOriginISO3166 c
					                                    WHERE p.countryCode = c.CodigoPais
				                                    )												AS 'pais_destino'
			                                        , tp.Nome										AS 'tipo_produto'
			                                        , i.Nome										AS 'lingua'
			                                        , p.Estado										AS 'estado'
			                                        , CAST(p.width AS VARCHAR(12)) + ' ' + CAST(p.widthMeasurementUnitCode AS VARCHAR(12))			AS 'largura'
			                                        , CAST(p.depth AS VARCHAR(12)) + ' ' + CAST(p.depthMeasurementUnitCode AS VARCHAR(12))			AS 'profundidade'
			                                        , CAST(p.height AS VARCHAR(12)) + ' ' + CAST(p.heightMeasurementUnitCode AS VARCHAR(12))		AS 'altura'
			                                        , CAST(p.netWeight AS VARCHAR(12)) + ' ' + CAST(p.netWeightMeasurementUnitCode AS VARCHAR(12))	AS 'peso_liquido'
			                                        , CAST(p.grossWeight AS VARCHAR(12)) + ' ' + CAST(p.grossWeightMeasurementUnitCode AS VARCHAR(12))	AS 'peso_bruto'
			                                        , p.ipiPerc										AS 'ipi'
			                                        , tu.Nome										AS 'tipo_url'
			                                        , pu.URL										AS 'url'
			                                        , CASE p.IndicadorCompartilhaDados
					                                    WHEN 1 THEN 'Sim'
					                                    WHEN 0 THEN 'N�o'
					                                    ELSE ''
					                                    END											AS 'compartilha_dados'
			                                        , CAST(p.Observacoes AS VARCHAR(MAX))			AS 'observacoes'
			                                        , p.importClassificationValue					AS 'ncm'
			                                        , pinf.globalTradeItemNumber					AS 'gtin_inferior'
			                                        , pinf.productDescription						AS 'descricao_inferior'
			                                        , ph.Quantidade									AS 'qtd_itens_inferior'
			                                        , sgh.Nome										AS 'status_historico'
			                                        , p.DataAlteracao								AS 'data_status_historico'
			                                        , p.DataSuspensao								AS 'data_suspensao'
			                                        , p.DataReativacao								AS 'data_reativacao'
			                                        , p.DataReutilizacao							AS 'data_reutilizacao'
			                                        , p.DataCancelamento							AS 'data_cancelamento'
			                                        , ua.Nome										AS 'usuario_alteracao'
		                                        FROM ProdutoHistorico p
			                                        LEFT JOIN Associado a ON (p.CodigoAssociado = a.Codigo)
			                                        LEFT JOIN TipoGTIN tg ON (p.CodigoTipoGTIN = tg.Codigo)
			                                        LEFT JOIN Produto pat ON (p.CodigoProduto = pat.CodigoProduto)
                                                    LEFT JOIN PrefixoLicencaAssociado pla ON pat.NrPrefixo = pla.NumeroPrefixo
			                                        LEFT JOIN StatusGTIN sg ON (pat.CodigoStatusGTIN = sg.Codigo)
			                                        LEFT JOIN StatusGTIN sgh ON (p.CodigoStatusGTIN = sgh.Codigo)
			                                        LEFT JOIN tbBrick br ON (p.CodeBrick = br.CodeBrick)
			                                        LEFT JOIN Usuario uc ON (p.CodigoUsuarioCriacao = uc.Codigo)
			                                        LEFT JOIN Usuario ua ON (p.CodigoUsuarioAlteracao = ua.Codigo)
			                                        LEFT JOIN TipoProduto tp ON (p.CodigoTipoProduto = tp.Codigo)
			                                        LEFT JOIN Lingua i ON (p.CodigoLingua = i.Codigo)
			                                        LEFT JOIN ProdutoURL pu ON (p.CodigoProduto = pu.CodigoProduto)
			                                        LEFT JOIN TipoURL tu ON (pu.CodigoTipoURL = tu.Codigo)
			                                        LEFT JOIN ProdutoHierarquia ph ON (p.CodigoProduto = ph.CodigoProdutoSuperior)
			                                        LEFT JOIN Produto pinf ON (ph.CodigoProdutoInferior = pinf.CodigoProduto)
				                                    LEFT JOIN ProdutoAgenciaReguladora par ON (par.codigoproduto = pinf.CodigoProduto)
												
		                                        WHERE (a.Codigo = @associado OR @associado IS NULL)
                                                    AND sg.Codigo <> 7
                                                    AND (pla.CodigoStatusPrefixo <> 4 OR pla.CodigoStatusPrefixo IS NULL)
			                                        AND a.Nome LIKE ('%' + ISNULL(@nome, '') + '%')
			                                        AND (pat.globalTradeItemNumber = @gtin or @gtin IS NULL)
			                                        AND p.productDescription LIKE ('%' + ISNULL(@descricao, '') + '%')
			                                        AND (p.DataInclusao >= @inicio OR @inicio IS NULL) 
                                                    AND (p.DataInclusao < @fim OR @fim IS NULL)
			                                        AND (sg.Codigo = @statusgtin OR @statusgtin IS NULL)
			                                        AND (tg.Codigo = @tipogtin OR @tipogtin IS NULL)
				                                    AND p.CodigoStatusGTIN <> ISNULL((
						                                    SELECT TOP 1 a.CodigoStatusGTIN 
						                                    FROM ProdutoHistorico a
						                                    WHERE p.CodigoProduto = a.CodigoProduto
							                                    AND a.DataHistorico < p.DataHistorico
						                                    ORDER BY a.DataHistorico DESC
				                                    ), 0)
				                                    GROUP BY a.CPFCNPJ
			                                        , a.Nome
			                                        , pat.globalTradeItemNumber
			                                        , pat.NrPrefixo
			                                        , tg.Nome
			                                        , p.brandName
			                                        , sg.Nome
			                                        , br.Text
			                                        , par.CodigoAgencia
			                                        , uc.Nome
			                                        , par.alternateItemIdentificationId
			                                        , p.productDescription
			                                        , p.DataInclusao
			                                        , p.tradeItemCountryOfOrigin
				                                    , p.countryCode
			                                        , tp.Nome
			                                        , i.Nome
			                                        , p.Estado
			                                        , p.width
				                                    , p.widthMeasurementUnitCode
			                                        , p.depth
				                                    , p.depthMeasurementUnitCode
			                                        , p.height
				                                    , p.heightMeasurementUnitCode
			                                        , p.netWeight
				                                    , p.netWeightMeasurementUnitCode
			                                        , p.grossWeight
				                                    , p.grossWeightMeasurementUnitCode
			                                        , p.ipiPerc
			                                        , tu.Nome
			                                        , pu.URL
			                                        , p.IndicadorCompartilhaDados
			                                        , CAST(p.Observacoes AS VARCHAR(MAX))
			                                        , p.importClassificationValue
			                                        , pinf.globalTradeItemNumber
			                                        , pinf.productDescription
			                                        , ph.Quantidade
			                                        , sgh.Nome
			                                        , p.DataAlteracao
			                                        , p.DataSuspensao
			                                        , p.DataReativacao
			                                        , p.DataReutilizacao
			                                        , p.DataCancelamento
			                                        , ua.Nome
                                        ) r
                                        ORDER BY r.nome_razao_social ASC
		                                    , r.gtin ASC
		                                    , tbl ASC
		                                    , data_status_historico DESC";

                    if (parametro != null && parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    if (param != null)
                    {
                        param.inicio = Core.Util.ChangeDateFormat(param.inicio.ToString());
                        param.fim = Core.Util.ChangeDateFormat(param.fim.ToString());

                        if (!string.IsNullOrEmpty(param.fim))
                        {
                            param.fim += " 23:59:59";
                        }
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
