using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioImpressaoGS1BO")]
    class RelatorioImpressaoGS1BO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return ""; }
        }

        [GS1CNPAttribute("AcessarRelatorioImpressaoGS1", true, true)]
        public string relatorioImpressaoGS1(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
                parametro.where.associado = null;

                if (associado != null)
                {
                    parametro.where.associado = associado.codigo;
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;
                    string query = @"SELECT
	                                    Associado.CPFCNPJ,
	                                    Associado.Nome,
	                                    Produto.globalTradeItemNumber AS 'GTIN',
	                                    Produto.productDescription AS 'Descricao',
	                                    TipoGeracao.Nome AS 'Tipo_Impressao',
	                                    HistoricoGeracaoEtiqueta.DataImpressao,
	                                    HistoricoGeracaoEtiqueta.Texto AS 'Texto_Livre',
	                                    Fabricante.Nome AS 'Fabricante',
	                                    ModeloEtiqueta.Nome AS 'Formato_Etiqueta',
	                                    HistoricoGeracaoEtiqueta.Quantidade AS 'Quantidade_Etiquetas'
                                    FROM [HistoricoGeracaoEtiqueta]
	                                    INNER JOIN [Produto] ON Produto.CodigoProduto = HistoricoGeracaoEtiqueta.CodigoProduto
	                                    INNER JOIN [Associado] ON Associado.Codigo = Produto.CodigoAssociado
	                                    LEFT JOIN [TipoGeracao] ON TipoGeracao.Codigo = HistoricoGeracaoEtiqueta.CodigoTipoGeracao
	                                    LEFT JOIN [ModeloEtiqueta] ON ModeloEtiqueta.Codigo = HistoricoGeracaoEtiqueta.CodigoModeloEtiqueta
	                                    LEFT JOIN [Fabricante] ON Fabricante.Codigo = HistoricoGeracaoEtiqueta.CodigoFabricante
                                    WHERE (Associado.Codigo = @associado OR @associado IS NULL)
	                                    AND Associado.Nome LIKE ('%' + ISNULL(@nome, '') + '%')
	                                    AND (HistoricoGeracaoEtiqueta.DataImpressao >= @inicio OR @inicio IS NULL)
                                        AND (HistoricoGeracaoEtiqueta.DataImpressao < @fim OR @fim IS NULL)
	                                    AND (Produto.globalTradeItemNumber = @gtin OR @gtin IS NULL)
	                                    AND Produto.productDescription LIKE ('%' + ISNULL(@produto, '') + '%')
                                    ORDER BY
	                                    HistoricoGeracaoEtiqueta.DataImpressao DESC";

                    if (parametro != null && parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    if (param != null)
                    {
                        param.inicio = Core.Util.ChangeDateFormat(param.inicio.ToString());
                        param.fim = Core.Util.ChangeDateFormat(param.fim.ToString());
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
