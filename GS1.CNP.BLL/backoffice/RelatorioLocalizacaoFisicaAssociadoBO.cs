using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioLocalizacaoFisicaAssociadoBO")]
    class RelatorioLocalizacaoFisicaAssociadoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return ""; }
        }

        [GS1CNPAttribute("AcessarRelatorioLocalizacaoFisicaAssociado", true, true)]
        public string relatorioLocalizacaoFisicaAssociado(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
                parametro.where.associado = null;

                if (associado != null)
                {
                    parametro.where.associado = associado.codigo;
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;
                    string query = @"SELECT LF.GLN				AS 'gln'
	                                    , LF.Nome				AS 'nome'
                                        , P.Nome				AS 'papel'
                                        , LF.Status				AS 'status'
										, CA.Nome				AS 'representante'
	                                    , LF.CEP				AS 'cep'
	                                    , LF.Endereco			AS 'endereco'
	                                    , LF.Numero				AS 'numero'
	                                    , LF.Complemento		AS 'complemento'
	                                    , LF.Bairro				AS 'bairro'
	                                    , LF.Cidade				AS 'cidade'
	                                    , LF.Estado				AS 'estado'
	                                    , LF.Telefone			AS 'telefone'
	                                    , LF.Observacao			AS 'observacao'
                                        , LF.DataAlteracao		AS 'alteracao'
                                    FROM [LocalizacaoFisica] LF
                                        INNER JOIN [PapelGLN] P ON (LF.CodigoPapelGLN = P.Codigo)
										INNER JOIN [Associado] A ON (LF.CodigoAssociado = A.Codigo)
                                        LEFT JOIN ContatoAssociado CA ON (A.Codigo = CA.CodigoAssociado AND CA.IndicadorPrincipal = 1)
                                    WHERE A.Codigo = @associado
                                        AND (LF.GLN = @gln or @gln IS NULL)
	                                    AND LF.Nome LIKE ('%' + ISNULL(@nome, '') + '%')
	                                    AND (LF.DataAlteracao >= @inicio OR @inicio IS NULL)
                                        AND (LF.DataAlteracao < @fim OR @fim IS NULL)
                                    ORDER BY
	                                    LF.GLN ASC";

                    if (parametro != null && parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    if (param != null)
                    {
                        param.inicio = Core.Util.ChangeDateFormat(param.inicio.ToString());
                        param.fim = Core.Util.ChangeDateFormat(param.fim.ToString());

                        if (!string.IsNullOrEmpty(param.fim))
                        {
                            param.fim += " 23:59:59";
                        }
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
