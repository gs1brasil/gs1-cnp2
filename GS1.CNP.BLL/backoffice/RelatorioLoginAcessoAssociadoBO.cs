using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioLoginAcessoAssociadoBO")]
    class RelatorioLoginAcessoAssociadoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return ""; }
        }

        [GS1CNPAttribute("AcessarRelatorioAcessoAssociado", true, true)]
        public string relatorioLoginAcessoAssociado(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
                parametro.where.associado = null;

                if (associado != null)
                {
                    parametro.where.associado = associado.codigo;
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;
                    string query = @"SELECT Usuario.Nome                    AS 'Nome_Usuario',
	                                    Usuario.Email                       AS 'Login_Usuario',
	                                    Perfil.Nome                         AS 'Perfil',
	                                    Usuario.DataUltimoAcesso            AS 'Data_Ultimo_Acesso',
	                                    Usuario.DataCadastro                AS 'Data_Criacao',
	                                    DATEADD(D, CAST((
					                                    SELECT VALOR 
					                                    FROM Parametro 
					                                    WHERE Chave = 'autenticacao.senha.nudias.validade'
				                                    ) AS INTEGER
			                                    ), COALESCE(Usuario.DataAlteracaoSenha, Usuario.DataAlteracao, Usuario.DataCadastro)
		                                    )                               AS 'Data_Expiracao_Senha'	
                                    FROM [Usuario]
	                                    INNER JOIN [Perfil] ON Perfil.Codigo = Usuario.CodigoPerfil
	                                    INNER JOIN [AssociadoUsuario] ON AssociadoUsuario.CodigoUsuario = Usuario.Codigo
	                                    INNER JOIN [Associado] ON Associado.Codigo = AssociadoUsuario.CodigoAssociado
                                    WHERE Associado.Codigo = @associado
                                        AND Usuario.codigotipousuario = 2 
                                        AND Usuario.Nome LIKE ('%' + ISNULL(@nomeusuario, '') + '%')
	                                    AND Usuario.Email LIKE ('%' + ISNULL(@login, '') + '%')
	                                    AND (Usuario.DataUltimoAcesso >= @inicio OR @inicio IS NULL)
                                        AND (Usuario.DataUltimoAcesso < @fim OR @fim IS NULL)
                                    ORDER BY Usuario.Nome ASC";

                    if (parametro != null && parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    if (param != null)
                    {
                        param.inicio = Core.Util.ChangeDateFormat(param.inicio.ToString());
                        param.fim = Core.Util.ChangeDateFormat(param.fim.ToString());

                        if (!string.IsNullOrEmpty(param.fim))
                        {
                            param.fim += " 23:59:59";
                        }
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
