using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioLoginAcessoGS1BO")]
    class RelatorioLoginAcessoGS1BO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return ""; }
        }

        [GS1CNPAttribute("AcessarRelatorioAcessoGS1", true, true)]
        public string relatorioLoginAcessoGS1(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {

                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
                parametro.where.associado = null;

                if (associado != null)
                {
                    parametro.where.associado = associado.codigo;
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;
                    string query = @"SELECT DISTINCT Usuario.CPFCNPJ		AS 'CPF_Usuario',
	                                    Associado.Nome						AS 'Razao_Social',
	                                    Usuario.Nome						AS 'Nome_Usuario',
	                                    Usuario.Email						AS 'Login_Usuario',
	                                    Perfil.Nome							AS 'Perfil',
	                                    Usuario.DataUltimoAcesso			AS 'Data_Ultimo_Acesso',
	                                    Usuario.DataCadastro				AS 'Data_Criacao',
	                                    DATEADD(D, CAST((
					                                    SELECT VALOR 
					                                    FROM Parametro 
					                                    WHERE Chave = 'autenticacao.senha.nudias.validade'
				                                    ) AS INTEGER
			                                    ), COALESCE(Usuario.DataAlteracaoSenha, Usuario.DataAlteracao, Usuario.DataCadastro)
		                                    )								AS 'Data_Expiracao_Senha'	
                                    FROM [Usuario]
	                                    INNER JOIN [Perfil] ON Perfil.Codigo = Usuario.CodigoPerfil
	                                    LEFT JOIN [AssociadoUsuario] ON AssociadoUsuario.CodigoUsuario = Usuario.Codigo
	                                    LEFT JOIN [Associado] ON Associado.Codigo = AssociadoUsuario.CodigoAssociado
                                    WHERE (Associado.Codigo = @associado OR @associado IS NULL)
	                                    AND (Usuario.CPFCNPJ = @cpfcnpj OR @cpfcnpj IS NULL)
	                                    AND (Associado.Nome LIKE ('%' + ISNULL(@nomerazaosocial, '') + '%') OR @nomerazaosocial IS NULL)
	                                    AND (Usuario.Nome LIKE ('%' + ISNULL(@nomeusuario, '') + '%') OR @nomeusuario IS NULL)
	                                    AND (Usuario.Email = @login OR @login IS NULL)
                                        AND (Usuario.DataUltimoAcesso >= @inicio OR @inicio IS NULL)
                                        AND (Usuario.DataUltimoAcesso < @fim OR @fim IS NULL)";

                    if (parametro != null && parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    if (param != null)
                    {
                        param.inicio = Core.Util.ChangeDateFormat(param.inicio.ToString());
                        param.fim = Core.Util.ChangeDateFormat(param.fim.ToString());

                        if (!string.IsNullOrEmpty(param.fim))
                        {
                            param.fim += " 23:59:59";
                        }
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
