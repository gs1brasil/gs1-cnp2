using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Globalization;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioMensalCNPBO")]
    public class RelatorioMensalCNPBO : CrudBO
    {
        public override string nomeTabela { get { return ""; } }
        public override string nomePK { get { return ""; } }

        [GS1CNPAttribute("AcessarRelatorioMensalCNP", true, true)]
        public string relatorioMensalCNP(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    string query = @"SELECT
                                     (SELECT COUNT(0) FROM ASSOCIADO WITH(NOLOCK) WHERE @inicio is null or DATAALTERACAO BETWEEN @inicio AND @fim) AS TOTALEMPRESAS,
                                     (SELECT COUNT(0) FROM PRODUTO WITH(NOLOCK) WHERE @inicio is null or DATAINCLUSAO BETWEEN @inicio AND @fim) AS TOTALPRODUTOS,
                                     (SELECT AVG(QTDE) FROM (SELECT Convert(Date, A.DATAALTERACAO, 103) as mes, COUNT(0) AS QTDE FROM LOG A WITH(NOLOCK) INNER JOIN USUARIO B WITH(NOLOCK) ON A.CODIGOUSUARIO = B.CODIGO WHERE A.URL LIKE '/LoginBO/Logar%' AND A.DATAALTERACAO BETWEEN @inicio AND @fim AND B.CODIGOTIPOUSUARIO = 2 GROUP BY Convert(Date, A.DATAALTERACAO, 103)) as T) AS TOTALACESSOSDIA,
                                     (SELECT COUNT(0) FROM LOG A WITH(NOLOCK) INNER JOIN USUARIO B WITH(NOLOCK) ON A.CODIGOUSUARIO = B.CODIGO WHERE URL LIKE '/LoginBO/Logar%' AND A.DATAALTERACAO BETWEEN @inicio AND @fim AND B.CODIGOTIPOUSUARIO = 2) AS TOTALACESSOSMES,
                                     (SELECT COUNT(0) FROM USUARIO WITH(NOLOCK) WHERE @inicio is null or DATACADASTRO BETWEEN @inicio AND @fim AND CODIGOTIPOUSUARIO = 2) AS TOTALUSUARIOS,
                                     (SELECT COUNT(0) FROM HISTORICOGERACAOETIQUETA WITH(NOLOCK) WHERE (@inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim)) AS QNTOPERACOESIMPETIQUETAS,
                                     (SELECT COUNT(0) FROM (SELECT CODIGOASSOCIADO FROM HISTORICOGERACAOETIQUETA A WITH(NOLOCK) INNER JOIN PRODUTO B WITH(NOLOCK) ON A.CODIGOPRODUTO = B.CODIGOPRODUTO WHERE @inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim GROUP BY CODIGOASSOCIADO) AS T) AS EMPRESASIMPETIQUETAS,
                                     (SELECT COUNT(0) FROM HISTORICOGERACAOETIQUETA WITH(NOLOCK) WHERE CODIGOBARCODETYPELIST IN (6,7) AND (@inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim) AND (@tipoetiqueta = 0 OR @tipoetiqueta = 1)) AS TOTALETIQUETASEAN8,
                                     (SELECT COUNT(0) FROM HISTORICOGERACAOETIQUETA WITH(NOLOCK) WHERE CODIGOBARCODETYPELIST IN (26,27,28,29) AND (@inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim) AND (@tipoetiqueta = 0 OR @tipoetiqueta = 2)) AS TOTALETIQUETASEAN12,
                                     (SELECT COUNT(0) FROM HISTORICOGERACAOETIQUETA WITH(NOLOCK) WHERE CODIGOBARCODETYPELIST IN (2,3,4,5) AND (@inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim) AND (@tipoetiqueta = 0 OR @tipoetiqueta = 3)) AS TOTALETIQUETASEAN13,
                                     (SELECT COUNT(0) FROM HISTORICOGERACAOETIQUETA WITH(NOLOCK) WHERE CODIGOBARCODETYPELIST IN (24) AND (@inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim) AND (@tipoetiqueta = 0 OR @tipoetiqueta = 4)) AS TOTALETIQUETASITF14,
                                     (SELECT COUNT(0) FROM HISTORICOGERACAOETIQUETA WITH(NOLOCK) WHERE CODIGOBARCODETYPELIST IN (1,8) AND (@inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim) AND (@tipoetiqueta = 0 OR @tipoetiqueta = 5)) AS TOTALETIQUETASGS1128,
                                     (SELECT COUNT(0) FROM HISTORICOGERACAOETIQUETA WITH(NOLOCK) WHERE CODIGOBARCODETYPELIST IN (10,11,12,13,14,15,16,17,18,19,20,21,22,23) AND (@inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim) AND (@tipoetiqueta = 0 OR @tipoetiqueta = 6)) AS TOTALETIQUETASDATABAR,
                                     (SELECT COUNT(0) FROM HISTORICOGERACAOETIQUETA WITH(NOLOCK) WHERE CODIGOBARCODETYPELIST IN (9) AND (@inicio is null or DATAIMPRESSAO BETWEEN @inicio AND @fim) AND (@tipoetiqueta = 0 OR @tipoetiqueta = 7)) AS TOTALETIQUETASDATAMATRIX";

                    dynamic param = null;

                    if (parametro != null && parametro.where != null)
                    {
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                        if (param != null)
                        {
                            int tipo = Convert.ToInt32(param.tipo.ToString());

                            if (tipo == 0)
                            {
                                param.inicio = null;
                                param.fim = null;
                            }
                            else if (tipo == 1)
                            {
                                DateTime inicio = DateTime.ParseExact("01/" + param.mes.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                param.inicio = inicio;
                                param.fim = new DateTime(inicio.Year, inicio.Month, DateTime.DaysInMonth(inicio.Year, inicio.Month), 23, 59, 59);
                            }
                        }
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno.FirstOrDefault());
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
