using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioProdutoGS1BO")]
     class RelatorioProdutoGS1BO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return ""; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarListaTipoGTIN(dynamic parametro)
        {
            string sql = "SELECT Codigo as 'key', Nome as value FROM TipoGTIN WHERE Status = 1 ORDER BY Nome ASC";
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (GS1.CNP.DAL.Database db = new DAL.Database("BD"))
            {
                db.Open();

                List<dynamic> retorno = db.Select(sql, param);

                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarListaStatusGTIN(dynamic parametro)
        {
            string sql = "SELECT Codigo as 'key', Nome as value FROM StatusGTIN WHERE Status = 1 AND Codigo <> 7 ORDER BY Nome ASC";
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            using (GS1.CNP.DAL.Database db = new DAL.Database("BD"))
            {
                db.Open();

                List<dynamic> retorno = db.Select(sql, param);

                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("AcessarRelatorioProdutoGS1", true, true)]
        public string relatorioProdutoGS1(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;
                parametro.where.associado = null;

                if (associado != null)
                {
                    parametro.where.associado = associado.codigo;
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;
//                    string query = @"SELECT Associado.CPFCNPJ							AS 'CPFCNPJ_Associado'
//                                        , Associado.Nome                                AS 'NomeRazaoSocialAssociado'
//                                        , Produto.globalTradeItemNumber
//                                        , Prefixo.NumeroPrefixo
//                                        , StatusPrefixo.Nome                            AS 'Status_Prefixo'
//                                        , TipoGTIN.Nome                                 AS 'Tipo_GTIN'
//                                        , Produto.brandName
//                                        , StatusGTIN.Nome                               AS 'StatusProduto'
//                                        , AgenciaReguladora.Nome                        AS 'AgenciaRegistro'
//                                        , Produto.alternateItemIdentificationId
//                                        , UsuarioCriacao.Nome                           AS 'UsuarioCriacao'
//                                        , Produto.productDescription
//                                        , Produto.DataInclusao
//                                        , Produto.DataSuspensao
//                                        , Produto.DataReativacao
//                                        , Produto.DataCancelamento
//                                        , Produto.DataReutilizacao
//                                        , (
//                                            SELECT top 1 c.Nome
//                                            FROM CountryOfOriginISO3166 c
//                                            WHERE Produto.tradeItemCountryOfOrigin = c.CodigoPais
//                                        )                                               AS 'PaisOrigem'
//                                        , (
//                                            SELECT top 1 c.Nome
//                                            FROM CountryOfOriginISO3166 c
//                                            WHERE Produto.countryCode = c.CodigoPais
//                                        )                                               AS 'PaisDestino'
//                                        , TipoProduto.Descricao                         AS 'TipoProduto'
//                                        , Lingua.Descricao                              AS 'Lingua'
//                                        , Produto.Estado
//                                        , CAST(Produto.width AS VARCHAR(12)) + ' ' + CAST(Produto.widthMeasurementUnitCode AS VARCHAR(12))              AS 'largura'
//                                        , CAST(Produto.depth AS VARCHAR(12)) + ' ' + CAST(Produto.depthMeasurementUnitCode AS VARCHAR(12))              AS 'profundidade'
//                                        , CAST(Produto.height AS VARCHAR(12)) + ' ' + CAST(Produto.heightMeasurementUnitCode AS VARCHAR(12))            AS 'altura'
//                                        , CAST(Produto.netWeight AS VARCHAR(12)) + ' ' + CAST(Produto.netWeightMeasurementUnitCode AS VARCHAR(12))      AS 'peso_liquido'
//                                        , CAST(Produto.grossWeight AS VARCHAR(12)) + ' ' + CAST(Produto.grossWeightMeasurementUnitCode AS VARCHAR(12))  AS 'peso_bruto'
//                                        , Produto.ipiPerc
//                                        , TipoURL.Nome                                  AS 'TipoURL'
//                                        , ProdutoURL.URL                                AS 'URL'
//                                        , ProdutoImagem.URL                             AS 'URL_Imagem'
//                                        , CASE Produto.IndicadorCompartilhaDados
//                                                WHEN 1 THEN 'Sim'
//                                                WHEN 0 THEN 'Não'
//                                                ELSE ''
//                                            END                                         AS 'CompartilhaDados'
//                                        , Produto.Observacoes
//                                        , Produto.importclassificationvalue             AS 'NCM'
//                                        , ProdutoInferior.globalTradeItemNumber         AS 'GTIN_inferior'
//                                        , ProdutoInferior.productDescription            AS 'descricao_inferior'
//                                        , ProdutoHierarquia.Quantidade
//                                        , tbSegment.Text                                AS 'Segmento'
//                                        , tbFamily.Text                                 AS 'Familia'
//                                        , tbClass.Text                                  AS 'Classe'
//                                        , tbBrick.Text                                  AS 'brick'
//                                        , ty.Text                                       AS 'AtributoBrick'
//                                        , vl.Text                                       AS 'ValorAtributoBrick'
//
//                                    FROM [Produto]
//                                        LEFT JOIN [Associado] ON (Produto.CodigoAssociado = Associado.Codigo)
//                                        LEFT JOIN [PrefixoLicencaAssociado] AS prefixo ON prefixo.NumeroPrefixo = produto.NrPrefixo
//                                        LEFT JOIN [TipoGTIN] ON TipoGTIN.Codigo = Produto.CodigoTipoGTIN
//                                        LEFT JOIN [StatusGTIN] ON Produto.CodigoStatusGTIN = StatusGTIN.Codigo
//                                        LEFT JOIN [StatusPrefixo] ON prefixo.CodigoStatusPrefixo = StatusPrefixo.Codigo
//                                        LEFT JOIN [TipoProduto] ON TipoProduto.Codigo = Produto.CodigoTipoProduto
//                                        LEFT JOIN [Lingua] ON Lingua.Codigo = Produto.CodigoLingua
//                                        LEFT JOIN [ProdutoURL] ON ProdutoURL.CodigoProduto = Produto.CodigoProduto
//                                        LEFT JOIN [TipoURL] ON TipoURL.Codigo = ProdutoURL.CodigoTipoURL
//                                        LEFT JOIN [ProdutoImagem] ON ProdutoImagem.CodigoProduto = Produto.CodigoProduto
//                                        LEFT JOIN [ProdutoHierarquia] ON ProdutoHierarquia.CodigoProdutoSuperior = Produto.CodigoProduto
//                                        LEFT JOIN [Produto] AS ProdutoInferior ON ProdutoInferior.CodigoProduto = ProdutoHierarquia.CodigoProdutoInferior
//                                        LEFT JOIN [tbSegment] ON (Produto.CodeSegment = [tbSegment].CodeSegment)
//                                        LEFT JOIN [tbFamily] ON (Produto.CodeFamily = [tbFamily].CodeFamily)
//                                        LEFT JOIN [tbClass] ON (Produto.CodeClass = [tbClass].CodeClass)
//                                        LEFT JOIN [tbBrick] ON (Produto.CodeBrick = [tbBrick].CodeBrick)
//                                        LEFT JOIN AgenciaReguladora ON (Produto.alternateItemIdentificationAgency = AgenciaReguladora.Codigo)
//                                        LEFT JOIN Usuario AS UsuarioCriacao ON (Produto.CodigoUsuarioCriacao = UsuarioCriacao.Codigo)
//                                        LEFT join ProdutoBrickTypeValue v ON Produto.CodigoProduto = v.CodigoProduto
//                                                    --AND ty.CodeType = v.CodeType --Retirado 
//                                                    AND Produto.CodeBrick = v.CodeBrick
//                                                    AND Produto.CodeClass = v.CodeClass
//                                                    AND Produto.CodeFamily = v.CodeFamily
//                                                    AND Produto.CodeSegment = v.CodeSegment
//                                        left join tbBrickType t ON v.CodeBrick = t.CodeBrick
//				                                    AND v.CodeType = t.CodeType
//                                                    AND v.CodeClass = t.CodeClass
//                                                    AND v.CodeFamily = t.CodeFamily
//                                                    AND v.CodeSegment = t.CodeSegment
//                                        LEFT join tbTType ty ON t.CodeType = ty.CodeType
//                                        LEFT join tbTValue vl ON v.CodeValue = vl.CodeValue
//                                    WHERE (Associado.Codigo = @associado OR @associado IS NULL)
//                                        AND StatusGTIN.Codigo <> 7
//                                        AND (StatusPrefixo.Codigo <> 4 OR StatusPrefixo.Codigo IS NULL) 
//                                        AND (Produto.globalTradeItemNumber = @gtin OR @gtin IS NULL)
//	                                    AND Produto.productDescription LIKE ('%' + ISNULL(@produto, '') + '%')
//	                                    AND (Produto.DataInclusao >= @inicio OR @inicio IS NULL)
//                                        AND (Produto.DataInclusao < @fim OR @fim IS NULL)
//	                                    AND (Produto.CodigoStatusGTIN = @statusgtin OR @statusgtin IS NULL)
//	                                    AND (TipoGTIN.Codigo = @tipogtin OR @tipogtin IS NULL)
//                                    ORDER BY Associado.Nome,
//	                                    Produto.productDescription ASC";

                    string query = @"SELECT Associado.CPFCNPJ							AS 'CPFCNPJ_Associado'
	                                    , Associado.Nome								AS 'NomeRazaoSocialAssociado'
	                                    , Produto.globalTradeItemNumber
	                                    , Prefixo.NumeroPrefixo
	                                    , StatusPrefixo.Nome							AS 'Status_Prefixo'
	                                    , TipoGTIN.Nome									AS 'Tipo_GTIN'
	                                    , Produto.brandName
	                                    , StatusGTIN.Nome								AS 'StatusProduto'
	                                    , AgenciaReguladora.Nome						AS 'AgenciaRegistro'
	                                    , par.alternateItemIdentificationId
	                                    , UsuarioCriacao.Nome							AS 'UsuarioCriacao'
	                                    , Produto.productDescription
	                                    , Produto.DataInclusao
	                                    , Produto.DataSuspensao
	                                    , Produto.DataReativacao
	                                    , Produto.DataCancelamento
	                                    , Produto.DataReutilizacao
	                                    , (
											SELECT top 1 c.Nome
											FROM CountryOfOriginISO3166 c
											WHERE Produto.tradeItemCountryOfOrigin = c.CodigoPais
										)												AS 'PaisOrigem'
										, (
											SELECT top 1 c.Nome
											FROM CountryOfOriginISO3166 c
											WHERE Produto.countryCode = c.CodigoPais
										)												AS 'PaisDestino'
	                                    , TipoProduto.Descricao							AS 'TipoProduto'
	                                    , Lingua.Descricao								AS 'Lingua'
	                                    , Produto.Estado
	                                    , CAST(Produto.width AS VARCHAR(12)) + ' ' + CAST(Produto.widthMeasurementUnitCode AS VARCHAR(12))				AS 'largura'
	                                    , CAST(Produto.depth AS VARCHAR(12)) + ' ' + CAST(Produto.depthMeasurementUnitCode AS VARCHAR(12))				AS 'profundidade'
	                                    , CAST(Produto.height AS VARCHAR(12)) + ' ' + CAST(Produto.heightMeasurementUnitCode AS VARCHAR(12))			AS 'altura'
	                                    , CAST(Produto.netWeight AS VARCHAR(12)) + ' ' + CAST(Produto.netWeightMeasurementUnitCode AS VARCHAR(12))		AS 'peso_liquido'
	                                    , CAST(Produto.grossWeight AS VARCHAR(12)) + ' ' + CAST(Produto.grossWeightMeasurementUnitCode AS VARCHAR(12))	AS 'peso_bruto'
	                                    , Produto.ipiPerc
	                                    , TipoURL.Nome									AS 'TipoURL'
	                                    , ProdutoURL.URL								AS 'URL'
	                                    , ProdutoImagem.URL								AS 'URL_Imagem'
	                                    , CASE Produto.IndicadorCompartilhaDados
			                                    WHEN 1 THEN 'Sim'
			                                    WHEN 0 THEN 'Não'
			                                    ELSE ''
		                                    END											AS 'CompartilhaDados'
	                                    , Produto.Observacoes
	                                    , Produto.importclassificationvalue				AS 'NCM'
	                                    , ProdutoInferior.globalTradeItemNumber			AS 'GTIN_inferior'
	                                    , ProdutoInferior.productDescription			AS 'descricao_inferior'
	                                    , ProdutoHierarquia.Quantidade
	                                    , tbSegment.Text								AS 'Segmento'
	                                    , tbFamily.Text									AS 'Familia'
	                                    , tbClass.Text									AS 'Classe'
	                                    , tbBrick.Text									AS 'brick'
	                                    , ty.Text										AS 'AtributoBrick'
	                                    , vl.Text										AS 'ValorAtributoBrick'
                                    FROM [Produto]
	                                    LEFT JOIN [Associado] ON (Produto.CodigoAssociado = Associado.Codigo)
	                                    LEFT JOIN [PrefixoLicencaAssociado] AS prefixo ON prefixo.NumeroPrefixo = produto.NrPrefixo
	                                    LEFT JOIN [TipoGTIN] ON TipoGTIN.Codigo = Produto.CodigoTipoGTIN
	                                    LEFT JOIN [StatusGTIN] ON Produto.CodigoStatusGTIN = StatusGTIN.Codigo
	                                    LEFT JOIN [StatusPrefixo] ON prefixo.CodigoStatusPrefixo = StatusPrefixo.Codigo
	                                    LEFT JOIN [TipoProduto] ON TipoProduto.Codigo = Produto.CodigoTipoProduto
	                                    LEFT JOIN [Lingua] ON Lingua.Codigo = Produto.CodigoLingua
	                                    LEFT JOIN [ProdutoURL] ON ProdutoURL.CodigoProduto = Produto.CodigoProduto
	                                    LEFT JOIN [TipoURL] ON TipoURL.Codigo = ProdutoURL.CodigoTipoURL
	                                    LEFT JOIN [ProdutoImagem] ON ProdutoImagem.CodigoProduto = Produto.CodigoProduto
	                                    LEFT JOIN [ProdutoHierarquia] ON ProdutoHierarquia.CodigoProdutoSuperior = Produto.CodigoProduto
	                                    LEFT JOIN [Produto] AS ProdutoInferior ON ProdutoInferior.CodigoProduto = ProdutoHierarquia.CodigoProdutoInferior
	                                    LEFT JOIN [tbSegment] ON (Produto.CodeSegment = [tbSegment].CodeSegment)
	                                    LEFT JOIN [tbFamily] ON (Produto.CodeFamily = [tbFamily].CodeFamily)
	                                    LEFT JOIN [tbClass] ON (Produto.CodeClass = [tbClass].CodeClass)
	                                    LEFT JOIN [tbBrick] ON (Produto.CodeBrick = [tbBrick].CodeBrick)
	                                    LEFT JOIN ProdutoAgenciaReguladora par ON (par.codigoproduto = Produto.codigoproduto)
	                                    LEFT JOIN AgenciaReguladora ON (par.CodigoAgencia = AgenciaReguladora.Codigo)
	                                    LEFT JOIN Usuario AS UsuarioCriacao ON (Produto.CodigoUsuarioCriacao = UsuarioCriacao.Codigo)
	                                    left join tbBrickType t ON
					                                    Produto.CodeBrick = t.CodeBrick
				                                    AND Produto.CodeClass = t.CodeClass
				                                    AND Produto.CodeFamily = t.CodeFamily
				                                    AND Produto.CodeSegment = t.CodeSegment
	                                    left join tbTType ty ON
					                                    t.CodeType = ty.CodeType
	                                    left join ProdutoBrickTypeValue v ON
					                                    Produto.CodigoProduto = v.CodigoProduto
				                                    AND	ty.CodeType = v.CodeType
				                                    AND	Produto.CodeBrick = v.CodeBrick
				                                    AND Produto.CodeClass = v.CodeClass
				                                    AND Produto.CodeFamily = v.CodeFamily
				                                    AND Produto.CodeSegment = v.CodeSegment
	                                    left join tbTValue vl ON
					                                    v.CodeValue = vl.CodeValue
                                    WHERE (Associado.Codigo = @associado OR @associado IS NULL)
                                        AND StatusGTIN.Codigo <> 7
                                        AND (StatusPrefixo.Codigo <> 4 OR StatusPrefixo.Codigo IS NULL) 
                                        AND (Produto.globalTradeItemNumber = @gtin OR @gtin IS NULL)
	                                    AND Produto.productDescription LIKE ('%' + ISNULL(@produto, '') + '%')
	                                    AND (Produto.DataInclusao >= @inicio OR @inicio IS NULL)
                                        AND (Produto.DataInclusao < @fim OR @fim IS NULL)
	                                    AND (Produto.CodigoStatusGTIN = @statusgtin OR @statusgtin IS NULL)
	                                    AND (TipoGTIN.Codigo = @tipogtin OR @tipogtin IS NULL)
                                    ORDER BY Associado.Nome,
	                                    Produto.productDescription ASC";

                    if (parametro != null && parametro.where != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    if (param != null)
                    {
                        param.inicio = Core.Util.ChangeDateFormat(param.inicio.ToString());
                        param.fim = Core.Util.ChangeDateFormat(param.fim.ToString());

                        if (!string.IsNullOrEmpty(param.fim))
                        {
                            param.fim += " 23:59:59";
                        }
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
