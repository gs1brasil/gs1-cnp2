﻿using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("StatusAssociadoBO")]
    class StatusAssociadoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "StatusAssociado"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        [GS1CNPAttribute(false)]
        public object buscarStatusAssociado(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO, STATUS FROM STATUSASSOCIADO WITH (NOLOCK)";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> consulta = db.Select(sql, param);

                return JsonConvert.SerializeObject(consulta);
            }
        }
    }
}
