﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("StatusGTINBO")]
    public class StatusGTINBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "STATUSGTIN"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarStatusGTINAtivos(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM STATUSGTIN WITH (NOLOCK) 
                            WHERE STATUS = 1";

            //Restringir o Status do GTIN em Ativo, Cancelado, Em elaboração e Transferido (apenas quando possuir CNAE médico)
            if (parametro != null && parametro.campos != null && parametro.campos.indicadorcnaerestritivo != null)
            {
                if (parametro.campos.indicadorcnaerestritivo == 1)
                    sql += " AND codigo IN (1,4,5,7) ";
            }

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarStatusGTINAtivosFiltrado(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM STATUSGTIN WITH (NOLOCK) 
                            WHERE STATUS = 1
                            AND CODIGO <> 5";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarStatusGTINEtiquetas(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM STATUSGTIN WITH (NOLOCK) 
                            WHERE STATUS = 1
                            AND codigo IN (1,3,6) ";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
