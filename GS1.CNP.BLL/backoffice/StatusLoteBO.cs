﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("StatusLoteBO")]
    public class StatusLoteBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "STATUSLOTE"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarStatusLoteAtivo(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM STATUSLOTE WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
