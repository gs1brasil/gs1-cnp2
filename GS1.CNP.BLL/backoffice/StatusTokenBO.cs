﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("StatusTokenBO")]
    public class StatusTokenBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "STATUSTOKEN"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarStatusTokenAtivo(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM STATUSTOKEN WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
