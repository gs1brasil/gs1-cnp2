﻿using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("StatusUsuarioBO")]
    class StatusUsuarioBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "StatusUsuario"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        [GS1CNPAttribute(false)]
        public object buscarStatusUsuario(dynamic parametro)
        {

//            string sql = @"SELECT A.*, B.NOME, B.DESCRICAO, B.STATUS FROM TIPOUSUARIOSTATUSUSUARIO A WITH (NOLOCK)
//                            INNER JOIN STATUSUSUARIO B WITH (NOLOCK) ON B.CODIGO = A.CODIGOSTATUSUSUARIO
//                             WHERE A.CODIGOTIPOUSUARIO = @codigo";
            string sql = @"SELECT CODIGO, NOME, DESCRICAO, STATUS FROM STATUSUSUARIO WITH (NOLOCK)";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> consulta = db.Select(sql, param);

                return JsonConvert.SerializeObject(consulta);
            }
        }

        [GS1CNPAttribute(false)]
        public object StatusUsuario(dynamic parametro)
        {

            string sql = @"SELECT * FROM STATUSUSUARIO WITH (NOLOCK)";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> consulta = db.Select(sql, param);

                return JsonConvert.SerializeObject(consulta);
            }
        }
    }
}
