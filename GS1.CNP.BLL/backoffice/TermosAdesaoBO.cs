﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Data;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("TermosAdesaoBO")]
    public class TermosAdesaoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TERMOADESAO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }   

        [GS1CNPAttribute(false)]
        public object BuscaTermosAdesao(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"SELECT T.CODIGO,
	                            T.VERSAO,
	                            T.DATACRIACAO,
	                            T.DATAVENCIMENTO,
	                            TI.HTML,
								TI.CODIGOIDIOMA,
								I.Nome AS idioma,
	                            STA.CODIGO AS CODIGOSTATUSTERMOADESAO,
	                            STA.NOME AS NOMESTATUSTERMOADESAO,
								T.CODIGOTIPOACEITE,
	                            T.CODIGOUSUARIOCRIACAO,
	                            U.NOME AS USUARIOCRIACAO,
	                            T.CODIGOUSUARIOALTERACAO
                            FROM TERMOADESAO AS T WITH (NOLOCK)
							INNER JOIN TERMOADESAOIDIOMA AS TI WITH (NOLOCK) ON TI.CODIGOTERMOADESAO = T.CODIGO
							INNER JOIN IDIOMA AS I WITH (NOLOCK) ON I.Codigo = TI.CodigoIdioma
                            INNER JOIN STATUSTERMOADESAO AS STA WITH (NOLOCK) ON STA.CODIGO = T.CODIGOSTATUSTERMOADESAO
                            INNER JOIN USUARIO AS U WITH (NOLOCK) ON U.CODIGO = T.CODIGOUSUARIOCRIACAO";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    List<dynamic> retorno = db.Select(sql);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarTermoAdesaoAtivo(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"select ta.Codigo, 
	                            ta.Versao, 
	                            ti.html 
                            from TermoAdesao as ta WITH (NOLOCK) 
                            INNER JOIN TermoAdesaoIdioma as ti WITH (NOLOCK) on ti.CodigoTermoAdesao = ta.Codigo
                            WHERE CodigoStatusTermoAdesao = 2";
                            //AND ti.CodigoIdioma = @codigoidioma";

                using (Database db = new Database("BD"))
                {
                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Open();
                    
                    dynamic retorno = ((List<dynamic>)db.Select(sql, param)).FirstOrDefault();


                    //if (retorno.Count <= 0)
                    //{
                    //    param.codigoidioma = 1;
                    //    retorno = db.Select(sql, param);
                        
                    //}
                    
                    return retorno;

                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

        [GS1CNPAttribute("CadastroTermosAdesao")]
        public object CadastrarTermoAdesao(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null && parametro.campos != null)
            {
                using (Database db = new Database("BD"))
                {
                    //Recupera o número inteiro da versão do último registro inserido
                    string sqlVersao = parametro.campos.codigostatustermoadesao == null || parametro.campos.codigostatustermoadesao == 1 ?
                        @"SELECT concat(SUBSTRING(versao, 1,CHARINDEX('.', versao) -1)+1, '.0') as versao FROM TermoAdesao where codigo = (SELECT max(Codigo) FROM TermoAdesao)"
                        : @"SELECT concat(SUBSTRING(versao,1,CHARINDEX('.', versao) -1),'.', (SUBSTRING(versao, CHARINDEX('.', versao) +1,1)+1)) as versao FROM TermoAdesao where codigo = @codigo";

                    string sql = @"INSERT INTO TermoAdesao (Versao, DataCriacao, CodigoStatusTermoAdesao, CodigoTipoAceite, CodigoUsuarioCriacao)
                                    VALUES(@versao, GETDATE(), @codigostatustermoadesao, @codigotipoaceite, @CodigoUsuarioCriacao)";

                    db.Open();

                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultVersao = ((List<dynamic>)db.Select(sqlVersao, paramCheck)).FirstOrDefault();

                    string versao = resultVersao != null ? resultVersao.versao : "1.0";

                    parametro.campos.CodigoUsuarioCriacao = user.id.ToString();
                    parametro.campos.versao = versao;
                    parametro.campos.codigostatustermoadesao = 1; //Novo registro será sempre Em Elaboração

                    dynamic param = null;

                    if (parametro.campos != null)
                    
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    long result = db.Insert(sql, param);
                   
                    if (result > 0)
                    {
                        Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, result);

                        string sqlIdioma = @"INSERT INTO TermoAdesaoIdioma (CodigoTermoAdesao, CodigoIdioma, html, DataAlteracao, CodigoUsuarioAlteracao)
                                            VALUES (@codigoTermoAdesao, @codigoidioma, @html, GETDATE(), @CodigoUsuarioCriacao)";

                        parametro.campos.codigoTermoAdesao = Convert.ToInt32(result);
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                        db.Select(sqlIdioma, param);
                    }
                }
                return "";
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar o cadastro.");
            }
        }

        [GS1CNPAttribute(true, true, "EditarTermosAdesao", "VisualizarTermosAdesao")]
        public object AtualizarTermoAdesao(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null && parametro.campos.codigo != null)
            {
                using (Database db = new Database("BD"))
                {
                    //incremente o numero decimal da versao
                    string sqlVersao = @"SELECT concat(SUBSTRING(versao,1,CHARINDEX('.', versao) -1),'.', (SUBSTRING(versao, CHARINDEX('.', versao) +1,1)+1)) as versao FROM TermoAdesao where codigo = @codigo";

                    string sql = @"UPDATE TermoAdesao SET CodigoTipoAceite = @codigotipoaceite, CodigoUsuarioAlteracao = @codigousuarioalteracao, Versao = @versao
                                    WHERE Codigo = @codigo";

                    string sqlIdioma = @"UPDATE TermoAdesaoIdioma SET CodigoIdioma = @codigoidioma, HTML = @html, DataAlteracao = GETDATE(), CodigoUsuarioAlteracao = @codigousuarioalteracao
                                        WHERE CodigoTermoAdesao = @codigo";

                    db.Open();
                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultVersao = ((List<dynamic>)db.Select(sqlVersao, paramCheck)).FirstOrDefault();
                    string versao = resultVersao.versao;

                    parametro.campos.versao = versao;
                    parametro.campos.codigousuarioalteracao = user.id.ToString(); ;

                    dynamic param = null;

                    if (parametro.campos != null)

                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    object result = db.Select(sql, param);
                    db.Select(sqlIdioma, param);

                    return result;
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar o cadastro.");
            }
        }

        [GS1CNPAttribute(false)]
        public object VerificaTermoAtivo(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    try
                    {
                        //Verifica se existe outro termo ativo
                        string sql = @"SELECT 1 AS RESULTADO FROM TERMOADESAO WHERE CODIGOSTATUSTERMOADESAO = 2";

                        db.Open();
                        dynamic result = ((List<dynamic>)db.Select(sql, null)).FirstOrDefault();

                        if (result != null && result.resultado == 1)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível realizar a operação. " + ex.Message);
                    }
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a operação.");
            }
        }

        [GS1CNPAttribute(false)]
        public object VerificaTermoEmElaboracao(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    try
                    {
                        //Verifica se existe outro termo Em Elaboração
                        string sql = @"SELECT 1 AS RESULTADO FROM TERMOADESAO WHERE CODIGOSTATUSTERMOADESAO = 1";

                        db.Open();
                        dynamic result = ((List<dynamic>)db.Select(sql, null)).FirstOrDefault();

                        if (result != null && result.resultado == 1)
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível realizar a operação. " + ex.Message);
                    }
                }

            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a operação.");
            }
        }

        [GS1CNPAttribute("EditarTermosAdesao")]
        public void AtivarTermo(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            //O Termo só poderá ser ativado caso esteja com status de Em Elaboração (codigo 1)
            if (user != null && parametro.campos.codigostatustermoadesao == 1)
            {
                using (Database db = new Database("BD"))
                {
                    try
                    {
                        parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                        //SQL para setar Termo Atual Ativo com Status de Histórico
                        string sqlSetHistorico = @"UPDATE TermoAdesao SET CodigoStatusTermoAdesao = 3, DataVencimento = GETDATE(), CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE CodigoStatusTermoAdesao = 2";
                        
                        //SQL para setar o novo termo Atual do sistema
                        string sql = @"UPDATE TermoAdesao SET CodigoStatusTermoAdesao = 2, CodigoUsuarioAlteracao = @CodigoUsuarioAlteracao WHERE Codigo = @codigo";

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);


                        db.Open();

                        db.Select(sqlSetHistorico, param);

                        db.Select(sql, param);

                    }
                    catch (Exception ex)
                    {
                        db.Rollback();
                        throw new GS1TradeException("Não foi possível realizar a operação. "+ex.Message);
                    }
                }
                
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a operação.");
            }
        }

        [GS1CNPAttribute(false,true)]
        public object AceitarTermoAdesao(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"INSERT INTO TermoAdesaoUsuario (CodigoTermoAdesao, CodigoUsuario, DataAceite)
                                VALUES (@codigo, @codigoUsuario, GETDATE())";

                using (Database db = new Database("BD"))
                {
                    parametro.campos.codigoUsuario = user.id.ToString();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Open();

                    object result = db.Select(sql, param);

                    //Recarrega os dados da sessão
                    GS1.CNP.DAL.Database db2 = new GS1.CNP.DAL.Database("BD");
                    db2.Open();

                    string sqluser = @"SELECT CODIGO, CODIGOPERFIL, CODIGOTIPOUSUARIO, NOME, QUANTIDADETENTATIVA, SENHA, CODIGOSTATUSUSUARIO, EMAIL
                                                           FROM USUARIO WITH (NOLOCK)
                                                           WHERE CODIGO =" + user.id.ToString() + " AND CodigoTipoUsuario in (1,2)"; ;
                    
                    Func<IDataRecord, Login> binder = r => new Login()
                    {
                        id = r.GetInt64(0),
                        id_perfil = r.GetInt32(1),
                        id_tipousuario = r.GetInt32(2),
                        nome = r.GetString(3),
                        quantidade_tentativa = r.GetInt32(4),
                        senha = r.GetString(5),
                        id_statususuario = r.GetInt32(6),
                        email = r.GetString(7)
                    };
                    List<Login> usuarios = db2.Select<Login>(sqluser, null, binder);

                    LoginBO login = new LoginBO();
                    var perfil = login.BuscarModulos(db2, Convert.ToInt32(user.id));
                    this.setSession("PERFIL_USUARIO_LOGADO", perfil);

                    List<string> permissoes = login.BuscarPermissoes(db2, Convert.ToInt32(user.id));
                    this.setSession("PERMISSOES_USUARIO_LOGADO", permissoes);

                    if (usuarios != null && usuarios.Count() > 0)
                    {
                        dynamic usuarioRelogado = usuarios.FirstOrDefault();
                        usuarioRelogado.permissoes = permissoes;

                        this.setSession("USUARIO_LOGADO", usuarioRelogado);
                    }

                    return result;

                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a operação.");
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarTermoAceitoUsuario(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"select * from TermoAdesaoUsuario where CodigoTermoAdesao = @codigo and CodigoUsuario = @codigoUsuario";

                using (Database db = new Database("BD"))
                {
                    parametro.campos.codigoUsuario = user.id.ToString();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Open();

                    object retorno = db.Select(sql, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarTipoAceite(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.All("TIPOACEITE");
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscarIdiomas(dynamic parametro)
        {
            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.All("IDIOMA");
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public object BuscaStatusTermoAdesaoAtivos(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"SELECT CODIGO, NOME, DESCRICAO
                                FROM STATUSTERMOADESAO
                                WHERE STATUS = 1";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    List<dynamic> retorno = db.Select(sql);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }

        [GS1CNPAttribute(false)]
        public object PesquisarTermosAdesao(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = @"SELECT T.CODIGO,
	                            T.VERSAO,
	                            T.DATACRIACAO,
	                            T.DATAVENCIMENTO,
	                            TI.HTML,
								TI.CODIGOIDIOMA,
	                            STA.CODIGO AS CODIGOSTATUSTERMOADESAO,
	                            STA.NOME AS NOMESTATUSTERMOADESAO,
								T.CODIGOTIPOACEITE,
	                            T.CODIGOUSUARIOCRIACAO,
	                            U.EMAIL AS USUARIOCRIACAO,
	                            T.CODIGOUSUARIOALTERACAO
                            FROM TERMOADESAO AS T WITH (NOLOCK)
							INNER JOIN TERMOADESAOIDIOMA AS TI WITH (NOLOCK) ON TI.CODIGOTERMOADESAO = T.CODIGO
                            INNER JOIN STATUSTERMOADESAO AS STA WITH (NOLOCK) ON STA.CODIGO = T.CODIGOSTATUSTERMOADESAO
                            INNER JOIN USUARIO AS U WITH (NOLOCK) ON U.CODIGO = T.CODIGOUSUARIOCRIACAO
                            WHERE 1 = 1 ";

                if (parametro != null && parametro.campos != null)
                {
                    if (parametro.campos.codigotipoaceite != null)
                    {
                        sql += " AND T.CODIGOTIPOACEITE = " + parametro.campos.codigotipoaceite;
                    }

                    if (parametro.campos.codigoidioma != null)
                    {
                        sql += " AND TI.CODIGOIDIOMA = " + parametro.campos.codigoidioma;
                    }

                    if (parametro.campos.codigostatustermoadesao != null)
                    {
                        sql += " AND STA.CODIGO = " + parametro.campos.codigostatustermoadesao;
                    }
                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    List<dynamic> retorno = db.Select(sql);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeException("Não foi possível realizar a consulta");
            }
        }
            
    }
}
