﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("TipoFiltroBO")]
    public class TipoFiltroBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TIPOFILTRO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTipoFiltrosAtivos(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME, DESCRICAO
                            FROM TIPOFILTRO WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
