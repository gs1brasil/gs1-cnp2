﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.IO;
using System.Web;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("TipoProdutoBO")]
    public class TipoProdutoBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TIPOPRODUTO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTipoProduto(dynamic parametro)
        {

            string sql = @"SELECT T.CODIGO, T.NOME, T.DESCRICAO, T.STATUS, T.NOMEIMAGEM, T.CAMPOOBRIGATORIO, TI.CODIGO AS CODIGOTRADEITEMUNITDESCRIPTORCODES,
                            TI.NOME AS NOMETRADEITEMUNITDESCRIPTORCODES, TI.DESCRICAO AS DESCRICAOTRADEITEMUNITDESCRIPTORCODES 
                            FROM TIPOPRODUTO T WITH (NOLOCK)
                            INNER JOIN TRADEITEMUNITDESCRIPTORCODES TI ON (TI.CODIGO = T.CODIGOTRADEITEMUNITDESCRIPTORCODES)";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTipoProdutoAtivo(dynamic parametro)
        {

            string sql = @"SELECT T.CODIGO, T.NOME, T.DESCRICAO, T.STATUS, T.NOMEIMAGEM, T.CAMPOOBRIGATORIO, TI.CODIGO AS CODIGOTRADEITEMUNITDESCRIPTORCODES,
                            TI.NOME AS NOMETRADEITEMUNITDESCRIPTORCODES
                            FROM TIPOPRODUTO T WITH (NOLOCK)
                            INNER JOIN TRADEITEMUNITDESCRIPTORCODES TI ON (TI.CODIGO = T.CODIGOTRADEITEMUNITDESCRIPTORCODES)
                            WHERE T.STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("CadastrarTipoProduto", true, true)]
        public object CadastrarTipoProduto(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    //Verifica se o nome do Tipo Produto já foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM TIPOPRODUTO P WHERE P.NOME = @nome";

                    db.Open();
                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                    {
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {

                        string sql = @"INSERT INTO TIPOPRODUTO (nome, status, descricao, nomeimagem, dataalteracao, codigousuarioalteracao, codigotradeitemunitdescriptorcodes, campoobrigatorio) 
                                        VALUES (@nome, @status, @descricao, @nomeimagem, GETDATE(), @CodigoUsuarioAlteracao, @codigotradeitemunitdescriptorcodes, @campoobrigatorio)";

                        dynamic param = null;

                        if (parametro.campos != null) {

                            if (parametro.campos.descricao == null) {
                                parametro.campos.descricao = "";
                            }

                            if (parametro.campos.nomeimagem == null) {
                                parametro.campos.nomeimagem = "";
                            }

                            if (parametro.campos.campoobrigatorio != null)
                                parametro.campos.campoobrigatorio = parametro.campos.campoobrigatorio == true ? 1 : 0;
                            else
                                parametro.campos.campoobrigatorio = 1;

                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                        }

                        long result = db.Insert(sql, param);

                        if (result > 0)
                        {
                            Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, result);
                        }
                    }
                    else
                    {
                        throw new GS1TradeException("Tipo de Produto já cadastrado.");
                    }
                }

                return "";
            }
            else
            {
                return "Não foi possível realizar o cadastro.";
            }

        }

        [GS1CNPAttribute(true, true, "EditarTipoProduto", "VisualizarTipoProduto")]
        public object EditarTipoProduto(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                using (Database db = new Database("BD"))
                {

                    db.Open(true);
                    db.BeginTransaction();

                    if (parametro != null && parametro.campos != null && parametro.campos.nomeimagem == null)
                    {
                        parametro.campos.nomeimagem = "";
                    }

                    parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                    if (parametro.campos.campoobrigatorio != null)
                        parametro.campos.campoobrigatorio = parametro.campos.campoobrigatorio == true ? 1 : 0;
                    else
                        parametro.campos.campoobrigatorio = 1;

                    //Inserção na tabela TIPOPRODUTOHISTORICO
                    string sql_historico = @"INSERT INTO TIPOPRODUTOHISTORICO (CODIGO, NOME, STATUS, NOMEIMAGEM, DATAALTERACAO, CODIGOUSUARIOALTERACAO, DESCRICAO, CODIGOTRADEITEMUNITDESCRIPTORCODES, CAMPOOBRIGATORIO,
                                                DATAHISTORICO)
	                                        SELECT CODIGO, NOME, STATUS, NOMEIMAGEM, DATAALTERACAO, CODIGOUSUARIOALTERACAO, DESCRICAO, CODIGOTRADEITEMUNITDESCRIPTORCODES, CAMPOOBRIGATORIO, GETDATE()
				                                            FROM TIPOPRODUTO WITH (NOLOCK)
				                                            WHERE TIPOPRODUTO.CODIGO = @codigo";

                    dynamic param1 = null;

                    if (parametro.campos != null)
                        param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql_historico, param1);

                    //Verifica se o nome Tipo de Produto já foi cadastrado
                    string check = "SELECT COUNT(1) AS QUANTIDADE FROM TIPOPRODUTO P WHERE P.NOME = @nome AND P.CODIGO <> @codigo";
                    dynamic paramCheck = null;

                    if (parametro.campos != null)
                        paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                    if (resultCheck.quantidade == 0)
                    {
                        //Atualiza tabela TIPOPRODUTO
                        string sql = @"UPDATE TIPOPRODUTO SET NOME = @nome, STATUS = @status, descricao = @descricao, NOMEIMAGEM = @nomeimagem, CODIGOUSUARIOALTERACAO = @CodigoUsuarioAlteracao, 
                                        CODIGOTRADEITEMUNITDESCRIPTORCODES = @codigotradeitemunitdescriptorcodes, DATAALTERACAO = GETDATE(), CAMPOOBRIGATORIO = @campoobrigatorio WHERE CODIGO = @codigo";
                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        db.Commit();
                        return result;
                    }
                    else
                    {
                        db.Rollback();
                        throw new GS1TradeException("Tipo de Produto já cadastrado.");
                    }
                }

            }
            else
            {
                return "Não foi possível realizar a atualização.";
            }

        }

        [GS1CNPAttribute("ExcluirTipoProduto", true, true)]
        public object ExcluirTipoProduto(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                try
                {
                    string sql = "DELETE FROM TIPOPRODUTO WHERE CODIGO = @codigo";

                    using (Database db = new Database("BD"))
                    {
                        db.Open();

                        dynamic param = null;

                        if (parametro.campos != null)
                            param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        object result = db.Select(sql, param);

                        return result;
                    }
                }
                catch (Exception e)
                {
                    throw new GS1TradeException("Exclusão não é permitida. O Tipo de Produto possui dados vinculados.", e);
                }
            }
            else
            {
                return "Não foi possível remover o item selecionado.";
            }

        }

        [GS1CNPAttribute(false)]
        public object PesquisaTipoProduto(dynamic parametro)
        {

            string sql = @"SELECT T.CODIGO, T.NOME, T.DESCRICAO, T.STATUS, T.NOMEIMAGEM, T.CAMPOOBRIGATORIO, TI.CODIGO AS CODIGOTRADEITEMUNITDESCRIPTORCODES,
                            TI.NOME AS NOMETRADEITEMUNITDESCRIPTORCODES, TI.DESCRICAO AS DESCRICAOTRADEITEMUNITDESCRIPTORCODES
                            FROM TIPOPRODUTO T WITH (NOLOCK)
                            INNER JOIN TRADEITEMUNITDESCRIPTORCODES TI ON (TI.CODIGO = T.CODIGOTRADEITEMUNITDESCRIPTORCODES) ";
            
            if(parametro != null && parametro.campos != null) {

                sql += " WHERE 1 = 1 ";

                if(parametro.campos.nome != null) {
                    sql += " AND T.NOME like '%' + @nome + '%'";
                }

                if (parametro.campos.status != null) {
                    sql += " AND T.STATUS = @status";
                }

                if (parametro.campos.descricao != null)
                {
                    sql += " AND T.DESCRICAO like '%' + @descricao + '%'";
                }

                if (parametro.campos.codigotradeitemunitdescriptorcodes != null)
                {
                    sql += " AND T.CODIGOTRADEITEMUNITDESCRIPTORCODES = @codigotradeitemunitdescriptorcodes";
                }

                if (parametro.campos.campoobrigatorio != null)
                {
                    sql += " AND T.CAMPOOBRIGATORIO = @campoobrigatorio";
                }
            }

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> fornecedor = db.Select(sql, param);

                return JsonConvert.SerializeObject(fornecedor);
            }
        }

        [GS1CNPAttribute(false, true)]
        public override void Update(dynamic parametro)
        {
            base.Update((object)parametro);
        }

        [GS1CNPAttribute(false, true)]
        public void DeletarImagem(dynamic parametro)
        {

            if (parametro != null && parametro.campos != null)
            {
                if (parametro.campos.nomeimagem.Value.Contains(System.Configuration.ConfigurationManager.AppSettings["URLBlobAzure"]))
                {
                    GS1.CNP.BLL.Core.Util.deleteBlobAzure(parametro.campos.nomeimagem.Value);
                }
                else if (parametro.campos.nomeimagem.Value.Contains("/upload"))
                {
                    //string folder = parametro.campos.nomeimagem;
                    //folder = folder.Replace(folder.Split('/').Last(), "");
                    //Directory.Delete(HttpContext.Current.Server.MapPath(folder));
                    if (File.Exists(HttpContext.Current.Server.MapPath(parametro.campos.nomeimagem.Value))) {
                        File.Delete(HttpContext.Current.Server.MapPath(parametro.campos.nomeimagem.Value));
                    }
                }

                string sql = "UPDATE TIPOPRODUTO SET NOMEIMAGEM = NULL WHERE CODIGO = @codigo";

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                    db.Select(sql, param);
                }
            }
            
        }
    }


}
