﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("TipoURLBO")]
    public class TipoURLBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TIPOURL"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTipoURLAtivos(dynamic parametro)
        {

            string sql = @"SELECT T.CODIGO, T.NOME + ' (' + (SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'midia.nuurl.' + LOWER(T.NOME)) + ')' AS NOME
                            , T.DESCRICAO
                            FROM TIPOURL T WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
