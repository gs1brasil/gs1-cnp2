﻿using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;


namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("TipoUsuarioBO")]
    class TipoUsuarioBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TipoUsuario"; }
        }

        public override string nomePK
        {
            get { return "Codigo"; }
        }

        [GS1CNPAttribute(false)]
        public object BuscarTipoUsuario(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, NOME FROM TIPOUSUARIO WITH (NOLOCK)";

            using (Database db = new Database("BD"))
            {
                db.Open();
                List<dynamic> consulta = db.Select(sql, null);
                return JsonConvert.SerializeObject(consulta);
            }
        }
    }
}
