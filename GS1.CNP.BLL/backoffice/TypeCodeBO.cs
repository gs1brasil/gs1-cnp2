﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("TypeCodeBO")]
    public class TypeCodeBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TYPECODE"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarPackagingTypeCode(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, CODEVALUE, NAME, DEFINITION 
                            FROM PACKAGINGTYPECODE WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarPalletTypeCode(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, CODE, CODEDESCRIPTION 
                            FROM PALLETTYPECODE WITH (NOLOCK) 
                            WHERE STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }
    }

}
