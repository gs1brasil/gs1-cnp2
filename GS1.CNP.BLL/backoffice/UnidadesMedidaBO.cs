﻿using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("UnidadesMedidaBO")]
    public class UnidadesMedidaBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return ""; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarMedidasVolume(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, COMMONCODE, NOME, SIGLA, FATORCONVERSAO, INDICADORPRINCIPAL
                            FROM UNECEREC20 WITH (NOLOCK) 
                            WHERE TIPO = 1
                            ORDER BY INDICADORPRINCIPAL DESC, CODIGO ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarMedidasPeso(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, COMMONCODE, NOME, SIGLA, FATORCONVERSAO, INDICADORPRINCIPAL
                            FROM UNECEREC20 WITH (NOLOCK) 
                            WHERE TIPO = 2
                            ORDER BY INDICADORPRINCIPAL DESC, CODIGO ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarMedidasComprimento(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, COMMONCODE, NOME, SIGLA, FATORCONVERSAO, INDICADORPRINCIPAL
                            FROM UNECEREC20 WITH (NOLOCK) 
                            WHERE TIPO = 3
                            ORDER BY INDICADORPRINCIPAL DESC, CODIGO ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarMedidasTemperatura(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, COMMONCODE, NOME, SIGLA, FATORCONVERSAO, INDICADORPRINCIPAL
                            FROM UNECEREC20 WITH (NOLOCK) 
                            WHERE TIPO = 4
                            ORDER BY INDICADORPRINCIPAL DESC, CODIGO ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarMedidasPedido(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, CODEVALUE, NAME, DEFINITION
                            FROM GDSNUNITOFMEASURECODELIST WITH (NOLOCK) 
                            WHERE STATUS = 1 
                            ORDER BY CODIGO ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarMedidasPorTipo(dynamic parametro)
        {

            string sql = @"SELECT CODIGO, COMMONCODE, NOME, SIGLA, FATORCONVERSAO, INDICADORPRINCIPAL
                            FROM UNECEREC20 WITH (NOLOCK) 
                            WHERE TIPO = @tipo
                            ORDER BY SIGLA ASC";

            using (Database db = new Database("BD"))
            {
                db.Open();
                dynamic param = ((JToken)parametro.where).ToExpando();
                object retorno = db.Select(sql, param);
                return JsonConvert.SerializeObject(retorno);
            }
        }

    }

}
