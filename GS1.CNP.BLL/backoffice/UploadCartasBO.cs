﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.BLL.Model;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("UploadCartasBO")]
    public class UploadCartasBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "TIPOPRODUTO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute(false)]
        public string BuscarTipoCarta(dynamic parametro)
        {

            string sql = @"SELECT C.CODIGO, C.NOME, C.STATUS, TC.CAMINHOTEMPLATE, TC.CODIGOUSUARIOALTERACAO, U.NOME AS USUARIO, TC.DATAALTERACAO 
	                        FROM LICENCA C WITH (NOLOCK) 
	                        LEFT JOIN TEMPLATECARTA TC ON (C.CODIGO = TC.CODIGOLICENCA)
	                        LEFT JOIN USUARIO U ON (TC.CODIGOUSUARIOALTERACAO = U.CODIGO)";

            using (Database db = new Database("BD"))
            {
                db.Open();
                object retorno = db.Select(sql, null);
                return JsonConvert.SerializeObject(retorno);
            }
        }

        [GS1CNPAttribute("UploadCartas")]
        public object AtualizarTemplate(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {

                    using (Database db = new Database("BD"))
                    {

                        db.Open();

                        parametro.campos.codigousuarioalteracao = ((GS1.CNP.BLL.Model.Login)(user)).id.ToString();

                        //Verifica se já existe um Template de Carta com o mesmo ID cadastrado
                        string check = "SELECT COUNT(1) AS QUANTIDADE, CODIGO FROM TEMPLATECARTA T WHERE T.CODIGOLICENCA = @codigo GROUP BY CODIGO";
                        dynamic paramCheck = null;

                        if (parametro.campos != null)
                            paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                        dynamic resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();

                        if (resultCheck != null && resultCheck.quantidade != null && resultCheck.quantidade > 0)
                        {

                            db.Open(true);
                            db.BeginTransaction();

                            try
                            {
                                //Inserção na tabela TEMPLATECARTAHISTORICO
                                string sql_historico = @"INSERT INTO TEMPLATECARTAHISTORICO (CODIGO, CODIGOLICENCA, CAMINHOTEMPLATE, CODIGOUSUARIOALTERACAO, DATAALTERACAO, DATAHISTORICO)
	                                            SELECT CODIGO, CODIGOLICENCA, CAMINHOTEMPLATE, CODIGOUSUARIOALTERACAO, DATAALTERACAO, GETDATE()
				                                                FROM TEMPLATECARTA WITH (NOLOCK)
				                                                WHERE TEMPLATECARTA.CODIGOLICENCA = @codigo";

                                dynamic param1 = null;

                                if (parametro.campos != null)
                                    param1 = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                                db.Select(sql_historico, param1);

                                //Atualiza tabela TEMPLATECARTA
                                parametro.campos.codigotipocarta = resultCheck.codigo;

                                string sql = @"UPDATE TEMPLATECARTA SET codigolicenca = @codigo, caminhotemplate = @caminhotemplate, CODIGOUSUARIOALTERACAO = @codigousuarioalteracao, 
                                            DATAALTERACAO = GETDATE() WHERE CODIGO = @codigotipocarta; 
                                            UPDATE LICENCA SET STATUS = @status WHERE CODIGO = @codigo";

                                dynamic param = null;

                                if (parametro.campos != null)
                                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                                object result = db.Select(sql, param);
                                db.Commit();

                                return result;
                            }
                            catch (Exception e)
                            {
                                db.Rollback();
                                throw new GS1TradeException("Não foi possível inserir o Template da Carta");
                            }
                        }
                        else
                        {
                            db.Open();

                            //Insere na tabela TEMPLATECARTA
                            string sql = @"INSERT INTO TEMPLATECARTA (codigolicenca, caminhotemplate, codigousuarioalteracao, dataalteracao) 
                                                               VALUES (@codigo, @caminhotemplate, @codigousuarioalteracao, GETDATE());
                                            UPDATE LICENCA SET STATUS = @status WHERE CODIGO = @codigo";

                            dynamic param = null;

                            if (parametro.campos != null)
                                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                            object result = db.Select(sql, param);

                            return result;
                        }
                    }
                //}
                //else
                //{
                //    throw new GS1TradeException("Falha ao atualizar carta. Por favor, entre em contato com a GS1.");
                //}
            }
            else
            {
                return "Não foi possível realizar a atualização.";
            }

        }

        [GS1CNPAttribute(false)]
        public object PesquisaTipoCarta(dynamic parametro)
        {

            string sql = @"SELECT C.CODIGO, C.NOME, C.STATUS, TC.CAMINHOTEMPLATE, TC.CODIGOUSUARIOALTERACAO, U.NOME AS USUARIO, TC.DATAALTERACAO 
	                        FROM LICENCA C WITH (NOLOCK) 
	                        LEFT JOIN TEMPLATECARTA TC ON (C.CODIGO = TC.CODIGOLICENCA)
	                        LEFT JOIN USUARIO U ON (TC.CODIGOUSUARIOALTERACAO = U.CODIGO) ";

            if (parametro != null && parametro.campos != null)
            {

                sql += " WHERE 1 = 1 ";

                if (parametro.campos.nome != null)
                {
                    sql += " AND C.NOME like '%' + @nome + '%'";
                }

                if (parametro.campos.status != null)
                {
                    sql += " AND status = @status";
                }

            }

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.campos != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);

                List<dynamic> fornecedor = db.Select(sql, param);

                return JsonConvert.SerializeObject(fornecedor);
            }
        }
    }


}
