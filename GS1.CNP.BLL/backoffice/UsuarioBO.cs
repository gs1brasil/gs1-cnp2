﻿using System;
using System.Collections.Generic;
using System.Linq;
using GS1.CNP.BLL.Core;
using System.Data;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using System.Configuration;
using GS1.CNP.BLL.Model;
using System.Web.Script.Serialization;
using System.Reflection;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("UsuarioBO")]
    public class UsuarioBO : CrudBO
    {
        public override string nomeTabela
        {
            get { return "USUARIO"; }
        }

        public override string nomePK
        {
            get { return "CODIGO"; }
        }

        [GS1CNPAttribute("PesquisarUsuario")]
        public string BuscarUsuarios(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            string sql = @"SELECT DISTINCT A.CODIGO, A.NOME, A.CODIGOSTATUSUSUARIO, A.CODIGOPERFIL, A.CODIGOTIPOUSUARIO
                        , A.EMAIL, A.SENHA, A.CODIGOUSUARIOALTERACAO, A.MENSAGEMOBSERVACAO                          
                        , C.NOME AS NOMETIPOUSUARIO, D.NOME AS NOMEPERFIL, E.NOME AS NOMESTATUSUSUARIO
                        , A.CPFCNPJ, A.TELEFONERESIDENCIAL, A.TELEFONECELULAR, A.TELEFONECOMERCIAL, A.RAMAL
                        , A.DEPARTAMENTO, A.CARGO
                        , A.DATAALTERACAOSENHA, A.DATAALTERACAO, A.DATAULTIMOACESSO, A.DATACADASTRO


                        FROM USUARIO A
                        LEFT JOIN ASSOCIADOUSUARIO B ON B.CODIGOUSUARIO = A.CODIGO
                        INNER JOIN TIPOUSUARIO C ON C.CODIGO = A.CODIGOTIPOUSUARIO
                        INNER JOIN PERFIL D ON D.CODIGO = A.CODIGOPERFIL
                        INNER JOIN STATUSUSUARIO E ON E.CODIGO = A.CODIGOSTATUSUSUARIO";

            Func<IDataRecord, object> obj = r => new
            {
                codigo = r.GetValue(r.GetOrdinal("CODIGO")),
                nome = r.GetValue(r.GetOrdinal("NOME")),
                codigostatususuario = r.GetValue(r.GetOrdinal("CODIGOSTATUSUSUARIO")),
                codigoperfil = r.GetValue(r.GetOrdinal("CODIGOPERFIL")),
                codigotipousuario = r.GetValue(r.GetOrdinal("CODIGOTIPOUSUARIO")),
                email = r.GetValue(r.GetOrdinal("EMAIL")),
                senha = r.GetValue(r.GetOrdinal("SENHA")),
                codigousuarioalteracao = r.GetValue(r.GetOrdinal("CODIGOUSUARIOALTERACAO")),
                nometipousuario = r.GetValue(r.GetOrdinal("NOMETIPOUSUARIO")),
                nomeperfil = r.GetValue(r.GetOrdinal("NOMEPERFIL")),
                nomestatususuario = r.GetValue(r.GetOrdinal("NOMESTATUSUSUARIO")),
                mensagemobservacao = r.GetValue(r.GetOrdinal("MENSAGEMOBSERVACAO")),
                cpfcnpj = r.GetValue(r.GetOrdinal("CPFCNPJ")),
                telefone = r.GetValue(r.GetOrdinal("TelefoneResidencial")),
                celular = r.GetValue(r.GetOrdinal("TelefoneCelular")),
                comercial = r.GetValue(r.GetOrdinal("TelefoneComercial")),
                ramal = r.GetValue(r.GetOrdinal("Ramal")),
                departamento = r.GetValue(r.GetOrdinal("Departamento")),
                cargo = r.GetValue(r.GetOrdinal("Cargo")),
                totalregistros = r.GetValue(r.GetOrdinal("TOTALREGISTROS")),

                dataalteracaosenha = r.GetValue(r.GetOrdinal("DATAALTERACAOSENHA")),
                dataalteracao = r.GetValue(r.GetOrdinal("DATAALTERACAO")),
                dataultimoacesso = r.GetValue(r.GetOrdinal("DATAULTIMOACESSO")),
                datacadastro = r.GetValue(r.GetOrdinal("DATACADASTRO"))
            };

            using (Database db = new Database("BD"))
            {

                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                {
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                    sql += " WHERE 1 = 1 ";

                    if (parametro.where.NOME.Value != null && parametro.where.NOME.Value != "")
                    {
                        sql += " AND a.nome like '%' + @NOME + '%'";
                    }
                    if (parametro.where.EMAIL.Value != null && parametro.where.EMAIL.Value != "")
                    {
                        sql += " AND a.email like '%'+ @EMAIL + '%'";
                    }
                    if (parametro.where.CODIGOTIPOUSUARIO != null && parametro.where.CODIGOTIPOUSUARIO.Value != null && Convert.ToString(parametro.where.CODIGOTIPOUSUARIO) != "")
                    {
                        sql += " AND c.codigo = @CODIGOTIPOUSUARIO";
                    }
                    if (parametro.where.NOMETIPOUSUARIO != null && parametro.where.NOMETIPOUSUARIO.Value != null && parametro.where.NOMETIPOUSUARIO.Value != "")
                    {
                        sql += " AND c.nome like '%' + @NOMETIPOUSUARIO + '%'";
                    }
                    if (parametro.where.CODIGOPERFIL.Value != null && Convert.ToString(parametro.where.CODIGOPERFIL) != "")
                    {
                        sql += " AND d.codigo = @CODIGOPERFIL";
                    }
                    if (parametro.where.CODIGOSTATUSUSUARIO.Value != null && Convert.ToString(parametro.where.CODIGOSTATUSUSUARIO) != "")
                    {
                        sql += " AND e.codigo = @CODIGOSTATUSUSUARIO";
                    }
                    if (parametro.where.EMPRESAASSOCIADA.Value != null && Convert.ToString(parametro.where.EMPRESAASSOCIADA) != "")
                    {
                        sql += " AND b.codigoassociado = @EMPRESAASSOCIADA";
                    }
                }

                Login user = this.getSession("USUARIO_LOGADO") as Login;

                //TODO - Verificar erro de session
                if (user != null && user.id_tipousuario != 1)
                {
                    if (parametro.where == null)
                        sql += " WHERE ";
                    else
                        sql += " AND ";

                    sql += string.Format(" (A.CODIGOTIPOUSUARIO = {0} ", user.id_tipousuario);


                    sql += ")";
                    sql += @"AND B.CODIGOASSOCIADO IN (
	                            SELECT Z.CODIGOASSOCIADO FROM ASSOCIADOUSUARIO Z
	                            WHERE Z.CODIGOUSUARIO = " + user.id.ToString() + @" )";
                }

                sql += "";


                string registroPorPagina = "10";
                if (parametro.registroPorPagina != null && parametro.registroPorPagina > 0)
                    registroPorPagina = parametro.registroPorPagina.ToString();

                string numeropagina = "1";
                if (parametro.paginaAtual > 0)
                    numeropagina = parametro.paginaAtual.ToString();

                string ordenacao = "Codigo ASC";
                if (parametro.ordenacao != string.Empty)
                    ordenacao = parametro.ordenacao;


                sql = @";WITH DADOS AS ( " + sql;
                sql = sql + @")
                        SELECT * 
                        ,TOTALREGISTROS = COUNT(*) OVER()
                        FROM DADOS WITH (NOLOCK)
                        ORDER BY " + ordenacao + @"
                        OFFSET " + registroPorPagina + @" * (" + numeropagina + @"-1) ROWS FETCH NEXT " + registroPorPagina + @" ROWS ONLY";


                List<dynamic> usuarios = db.Select(sql, param, obj);

                return JsonConvert.SerializeObject(usuarios);
            }
        }

        [GS1CNPAttribute(false)]
        public string BuscarUsuarioEspecifico(dynamic parametro)
        {

            string sql = @"SELECT A.* FROM ASSOCIADO A WITH(NOLOCK)
                           INNER JOIN ASSOCIADOUSUARIO B WITH(NOLOCK) ON A.CODIGO = B.CODIGOASSOCIADO
                           WHERE B.CODIGOUSUARIO=@Codigo";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                if (parametro.where != null)
                    param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                var result = db.Select(sql, param);

                return JsonConvert.SerializeObject(result);
            }
        }

        [GS1CNPAttribute(false, false)]
        public string BuscarUsuarioLogado(dynamic parametro)
        {
            //TODO - Verificar Session
            return JsonConvert.SerializeObject(base.getSession("USUARIO_LOGADO"));
        }

        [GS1CNPAttribute(false)]
        public string ValidarExistenciaUsuario(dynamic parametro)
        {
            JavaScriptSerializer oJS = new JavaScriptSerializer();

            if (parametro != null && parametro.where != null && parametro.where.Email != null && !string.IsNullOrEmpty(parametro.where.Email.ToString()))
            {
                string sql = string.Format("SELECT COUNT(0) FROM {0} WITH (NOLOCK) WHERE Email = @Email", this.nomeTabela);

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = new ExpandoObject();

                    param.Email = parametro.where.Email.ToString();

                    var result = db.Count(this.nomeTabela, param);

                    return oJS.Serialize(result > 0);
                }
            }

            return oJS.Serialize(false);
        }

        [GS1CNPAttribute(true, true, "EditarUsuario", "VisualizarUsuario")]
        public override void Update(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            if (parametro != null)
                Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, long.Parse(parametro.where.ToString()));

            Login user = this.getSession("USUARIO_LOGADO") as Login;

            using (Database db = new Database("BD"))
            {
                if (parametro != null && parametro.where != null && Convert.ToInt64(parametro.where.ToString()) > 0)
                {

                    db.Open(true);
                    db.BeginTransaction();

                    dynamic param = new ExpandoObject();
                    param.codigo = Convert.ToInt64(parametro.where.ToString());

                        try
                        {

                            //Inserção na tabela UsuarioHistorico
                            string inserthistorico = @"INSERT INTO USUARIOHISTORICO (CODIGO, NOME, CODIGOSTATUSUSUARIO, CODIGOPERFIL, CODIGOTIPOUSUARIO, EMAIL,
                                                            TELEFONERESIDENCIAL, SENHA, CODIGOUSUARIOALTERACAO, DATAALTERACAO, MENSAGEMOBSERVACAO, DATAALTERACAOSENHA,
                                                            QUANTIDADETENTATIVA, DATAULTIMOACESSO, DATACADASTRO, TELEFONECELULAR, TELEFONECOMERCIAL, RAMAL, DEPARTAMENTO,
                                                            CARGO, CPFCNPJ, DATAHISTORICO)
                                                            SELECT CODIGO, NOME,CODIGOSTATUSUSUARIO,CODIGOPERFIL,CODIGOTIPOUSUARIO,EMAIL,TELEFONERESIDENCIAL,SENHA,
                                                            CODIGOUSUARIOALTERACAO,DATAALTERACAO,MENSAGEMOBSERVACAO,DATAALTERACAOSENHA,QUANTIDADETENTATIVA,
                                                            DATAULTIMOACESSO,DATACADASTRO,TELEFONECELULAR,TELEFONECOMERCIAL,RAMAL,DEPARTAMENTO,CARGO,CPFCNPJ,GETDATE()
                                                            FROM USUARIO WITH (NOLOCK)
                                                            WHERE USUARIO.CODIGO = @codigo";

                            db.Insert(inserthistorico, param, false);

                            var usuario = db.Find(this.nomeTabela, param);

                            if (usuario != null)
                            {
                                if (usuario.codigostatususuario == 4 && parametro.campos.CodigoStatusUsuario == 1)
                                    parametro.campos.QuantidadeTentativa = 0;
                            }

                            parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                            parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();

                            dynamic arrayAssociados = parametro.campos.arrayAssociados;
                            parametro.campos.Remove("arrayAssociados");

                            //Verificação para não permitir que o usuário altere suas próprias permissões de perfil
                            if (user.id.ToString() == Convert.ToString(parametro.where))
                            {
                                parametro.campos.Remove("CodigoPerfil");
                            }

                            parametro.campos.Remove("CodigoTipoUsuario");

                            base.Update((object)parametro);

                            if (param.codigo.ToString() != user.id.ToString())
                            {
                                string sql_historico = @"INSERT INTO ASSOCIADOUSUARIOHISTORICO (CODIGOUSUARIO, CODIGOASSOCIADO, DATACADASTRO, DATACONFIRMACAO, DATAHISTORICO, CODIGOUSUARIOALTERACAO)
                                                         SELECT CODIGOUSUARIO, CODIGOASSOCIADO, DATACADASTRO, DATACONFIRMACAO, GETDATE() AS DATAHISTORICO, CODIGOUSUARIOALTERACAO 
                                                         FROM ASSOCIADOUSUARIO WHERE CODIGOUSUARIO = " + param.codigo;
                                db.Select(sql_historico, null);

                                string sql_delete = "DELETE FROM ASSOCIADOUSUARIO WHERE CODIGOUSUARIO = " + param.codigo;
                                db.Select(sql_delete, null);

                                //Inserção na tabela UsuárioAssociado
                                if (arrayAssociados != null)
                                {
                                    for (int i = 0; i < (((Newtonsoft.Json.Linq.JContainer)((arrayAssociados)))).Count; i++)
                                    {
                                        string sql = "INSERT INTO AssociadoUsuario VALUES (" + param.codigo + ", " + (((Newtonsoft.Json.Linq.JContainer)((arrayAssociados))))[i] + ", GETDATE(), NULL, " + user.id.ToString() + ", 0)";
                                        db.Select(sql, null);
                                    }
                                }
                            }

                            db.Commit();
                        }
                        catch (Exception ex)
                        {
                            db.Rollback();
                            throw new GS1TradeException("Não foi possível realizar a atualização.", ex);
                        }
                    //}
                    //else
                    //{
                    //    throw new GS1TradeException("CPF/CNPJ já cadastrado.");
                    //}
                }
            }
        }

        [GS1CNPAttribute(false)]
        public List<dynamic> CarregarParametrosLoginBO(string parametro)
        {
            string sql = @"SELECT CHAVE, VALOR FROM PARAMETRO WITH (NOLOCK) WHERE CHAVE = '" + parametro + "' AND STATUS = 1";

            using (Database db = new Database("BD"))
            {
                db.Open();

                dynamic param = null;

                List<dynamic> parametros = db.Select(sql, param);

                return parametros;
            }
        }

        [GS1CNPAttribute("CadastrarUsuario",true,true)]
        public override string Insert(dynamic parametro)
        {
            this.validaPermissaoBO(MethodBase.GetCurrentMethod().GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute);

            if (parametro != null)
            {

                using (Database db = new Database("BD"))
                {
                    db.Open(true);
                    db.BeginTransaction();

                    /*dynamic resultCheck = null;

                    if (parametro.campos != null && parametro.campos.CPFCNPJ != null && parametro.campos.CPFCNPJ != "")
                    {
                        //Verifica se o CPF já foi cadastrado
                        string check = "SELECT COUNT(1) AS QUANTIDADE FROM USUARIO U WHERE U.CPFCNPJ = @CPFCNPJ";

                        dynamic paramCheck = null;

                        if (parametro.campos != null)
                        {
                            paramCheck = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                        }

                        resultCheck = ((List<dynamic>)db.Select(check, paramCheck)).FirstOrDefault();
                    }*/

                    //if (resultCheck != null && resultCheck.quantidade == 0 && parametro.campos != null || parametro.campos.CPFCNPJ == null || parametro.campos.CPFCNPJ == "")
                    //if (parametro.campos != null)
                    //{

                        Login user = this.getSession("USUARIO_LOGADO") as Login;

                        Core.Util util = new Core.Util();

                        string senha = util.GeraSenha();

                        parametro.campos.senha = util.GeraSHA1(senha);
                        parametro.campos.DataAlteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                        parametro.campos.CodigoUsuarioAlteracao = user.id.ToString();
                        parametro.campos.CodigoStatusUsuario = 5;

                        dynamic arrayAssociados = parametro.campos.arrayAssociados;
                        parametro.campos.Remove("arrayAssociados");

                        dynamic paramInsert = null;

                        if (parametro.campos != null)
                        {
                            paramInsert = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                        }

                        int idInserido = db.Insert(nomeTabela, nomePK, paramInsert, true);

                        dynamic result = ((List<dynamic>)db.Select("SELECT CODIGO, NOME, EMAIL FROM USUARIO WHERE CODIGO = " + idInserido, null)).FirstOrDefault();
                        //string result = base.Insert((object)parametro);

                        //dynamic objResult = JsonConvert.DeserializeObject(result.ToString());

                        //dynamic param = ((JToken)objResult).ToExpando();

                        if (result != null)
                        {
                            Log.WriteInfo(parametro.nomeBO, parametro.nomeMetodo, long.Parse(result.codigo.ToString()));
                        }

                        if (result != null)
                        {
                            try
                            {
                                //Inserção na tabela UsuárioAssociado
                                if (result.codigo.ToString() != null && arrayAssociados != null)
                                {
                                    for (int i = 0; i < (((Newtonsoft.Json.Linq.JContainer)((arrayAssociados)))).Count; i++)
                                    {
                                        string sql = "INSERT INTO AssociadoUsuario VALUES (" + result.codigo.ToString() + ", " + (((Newtonsoft.Json.Linq.JContainer)((arrayAssociados))))[i] + ", GETDATE(), NULL, " + user.id.ToString() + ", 0)";
                                        db.Select(sql, null);
                                    }
                                }

                                EnviarEmail(result.email, senha);
                                db.Commit();

                            }
                            catch (Exception ex)
                            {
                                db.Rollback();

                                if (ex.Message == "Erro ao enviar email. Falha de comunicação com o servidor.")
                                {
                                    throw new GS1TradeException("Erro ao enviar email. Falha de comunicação com o servidor.");
                                }
                                else
                                {
                                    string telefoneGs1 = (string)(CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);
                                    throw new GS1TradeException("Não foi possível realizar o cadastro. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.", ex);
                                }
                            }
                        }

                    //}
                    //else
                    //{
                    //    db.Commit();
                    //    throw new GS1TradeException("CPF/CNPJ já cadastrado.");
                    //}
                }
            }

            return null;

        }

        private void EnviarEmail(string email, string novaSenha)
        {

            String url = ConfigurationManager.AppSettings["URLCaminhoImagemEmail"];
            String urlTrade = ConfigurationManager.AppSettings["URLCaminhoCNPEmail"];
            String urlGS1 = ConfigurationManager.AppSettings["URLCaminhoGS1Email"];
            String urlFacebook = ConfigurationManager.AppSettings["URLCaminhoFacebookEmail"];
            String urlLinkedin = ConfigurationManager.AppSettings["URLCaminhoLinkedinEmail"];
            String urlTwitter = ConfigurationManager.AppSettings["URLCaminhoTwitterEmail"];
            String urlYoutube = ConfigurationManager.AppSettings["URLCaminhoYoutubeEmail"];

            string mensagem = @"<div id='principal' style='text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; color:#000000; background-color:#FFFFFF; font-family: verdana, arial, helvetica; '>
		                            <table align='center' width='600' border='0' cellpadding='0' cellspacing='0' style='border-width: 0px; background-color: #ffffff;'>
			                            <tr valign='top'>
				                            <td><p style=' text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;'>
                                                <img width='600' height='134' alt='' hspace=1 vspace=1 src='" + url + @"/header_email.png'></p>
                                            </td>
			                            </tr>
			                            <tr style='font-size: 14pt; font-weight:bold; color:#F48332;'>
				                            <td><p style='padding:0px 10px 0px 10px;'> Caro usuário, </p> </td>
			                            </tr>
			                            <tr>
				                            <td>
                                                <p style='padding:10px 10px 0px 10px;'> 
                                                    <span style='font-size: 11pt; font-family: verdana, arial, helvetica;'>
                                                        Este é um e-mail automático. Por favor não responda. <br><br>
                                                        Você foi cadastrado(a) no sistema <span style='color: #F48332'>CNP</span>.<br><br>
                                                        Seja bem-vindo(a)!<br><br>
                                                        Login: " + email + @"<br>
                                                        Senha temporária: " + novaSenha + @"<br><br>
                                                        Após o login, troque a senha temporária:<br>
                                                        1. Clique <a href='" + urlTrade + @"'>aqui</a> para fazer o login<br>
                                                        2. Clique no seu nome de usuário e escolha 'Trocar senha'<br> <img alt='' hspace=1 vspace=1 src='" + url + @"/area_superior_direita.png'>                                                        
                                                        <br><br>
                                                        Troque sua senha no próximo acesso ao <span style='color: #F48332'>CNP</span> (a troca de senha é obrigatória).<br>
                                                        Para acessar o <span style='color: #F48332'>CNP</span>, clique <a href='" + urlTrade + @"'>aqui</a><br><br>
                                                        Equipe <b> GS1 </b><span style='color: red'>Brasil</span><br>
                                                    </span>
                                                    <div style='font-size: 8pt; text-align:left; font-family: verdana, arial, helvetica;'>
                                                        Acompanhe: <a href='" + urlFacebook + @"'><img src='" + url + @"/fb.png'></a> <a href='" + urlLinkedin + @"'><img src='" + url + @"/linkedin.png'></a> <a href='" + urlTwitter + @"'><img src='" + url + @"/twitter.png'></a> <a href='" + urlYoutube + @"'><img src='" + url + @"/youtube.png'></a> 
                                                    </div>
                                                </p>
                                            </td>
			                            </tr>
			                            <tr valign='top'>
				                            <td><p style='text-align: left; text-indent: 0px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;'><img width='600' height='134' alt='' hspace=1 vspace=1 src='" + url + @"/footer_email.png'></p></td>
			                            </tr>
		                            </table>
	                            </div>";


            GS1.CNP.BLL.Core.Util.EnviarEmail(email, "[CNP] Dados de Acesso", mensagem, true);

        }

        public UsuarioCRM ConsultarCRM(string CPFCNPJ, string email)
        {
            try
            {
                dynamic param = new ExpandoObject();
                param.cpfcnpj = CPFCNPJ;
                param.email = email;

                Func<IDataRecord, UsuarioCRM> usuarioCRM = r => new UsuarioCRM()
                {
                    id = r.GetGuid(0)
                    ,nome = r.GetString(1)
                    ,nomefantasia = (r.IsDBNull(2) ? string.Empty : r.GetString(2))
                    ,relacionamento = (r.IsDBNull(3) ? string.Empty : r.GetInt32(3).ToString()) 
                    ,cad = (r.IsDBNull(4) ? string.Empty : r.GetString(4)) 
                    ,cpfcnpj = r.GetString(5)
                    ,email = r.GetString(6)                    
                    ,bl_adimplente = Convert.ToBoolean(r.GetInt32(7))
                    ,bl_premium  = Convert.ToBoolean(r.GetInt32(8))
                    ,inscricaoestadual = (r.IsDBNull(9) ? string.Empty : r.GetString(9)) 
                    ,estadoname = (r.IsDBNull(10) ? string.Empty : r.GetString(10))
                    ,numero = (r.IsDBNull(11) ? string.Empty : r.GetString(11))
                    ,address1_city = (r.IsDBNull(12) ? string.Empty : r.GetString(12))
                    ,logradouroprincipal = (r.IsDBNull(13) ? string.Empty : r.GetString(13))
                    ,complementoprincipal = (r.IsDBNull(14) ? string.Empty : r.GetString(14))
                    ,address1_postalcode = (r.IsDBNull(15) ? string.Empty : r.GetString(15))
                    ,bairroprincipal = (r.IsDBNull(16) ? string.Empty : r.GetString(16))
                    ,representante_smart_pessoaname = (r.IsDBNull(17) ? string.Empty : r.GetString(17))
                    ,representante_smart_cpf = (r.IsDBNull(18) ? string.Empty : r.GetString(18))
                    ,representante_telephone1 = (r.IsDBNull(19) ? string.Empty : r.GetString(19))
                    ,representante_telephone2 = (r.IsDBNull(20) ? string.Empty : r.GetString(20))
                    ,representante_telephone3 = (r.IsDBNull(21) ? string.Empty : r.GetString(21))
                    ,representante_emailaddress1 = (r.IsDBNull(22) ? string.Empty : r.GetString(22))
                    ,representante_emailaddress2 = (r.IsDBNull(23) ? string.Empty : r.GetString(23))
                    ,glninformativo = (r.IsDBNull(24) ? string.Empty : r.GetString(24))
                    ,pais = (r.IsDBNull(25) ? string.Empty : r.GetString(25))
                    ,telephone1 = (r.IsDBNull(26) ? string.Empty : r.GetString(26))
                    ,smart_ddidddtelefoneprincipal = (r.IsDBNull(27) ? string.Empty : r.GetString(27))
                    ,gs1br_cnp20 = (r.IsDBNull(28) ? false : r.GetBoolean(28))

                              
                };

                string sql = @"SELECT TOP 1 A.ACCOUNTID
		                            ,A.NAME
		                            ,A.SMART_NOMEFANTASIA		
		                            ,A.SMART_CONDICAODERELACIONAMENTO
		                            ,A.ACCOUNTNUMBER
		                            ,A.SMART_CNPJCPF
		                            ,A.EMAILADDRESS1
		                            ,CASE WHEN EXISTS (
						                            SELECT	Z.SMART_GESTAODELICENCASEPRODUTOID,
								                            Z.SMART_NAME,
								                            Z.SMART_FINANCEIROSTATUS,
								                            Z.SMART_ASSOCIADO                  
						                            FROM SMART_GESTAODELICENCASEPRODUTO Z WITH(NOLOCK)
						                            WHERE Z.SMART_ASSOCIADO = A.ACCOUNTID 
						                            AND Z.SMART_FINANCEIROSTATUS NOT IN (1,3)
					                            ) THEN 1 ELSE 0
		                            END AS STATUS_ADIMPLENTE
		                            , COALESCE ((
						                            SELECT TOP 1 1                                 
						                            FROM SMART_LICENCA X (NOLOCK)
						                            WHERE X.SMART_EMPRESA = A.ACCOUNTID
						                            AND X.SMART_NEGOCIONAME LIKE '%EPC%'
						                            AND X.SMART_PAP_STATUS = 2
					                            ),0) AS STATUS_PREMIUM
                                    ,A.SMART_INSCRICAOESTADUAL
                                    ,A.SMART_ESTADONAME
                                    ,A.SMART_NUMERO
                                    ,A.ADDRESS1_CITY
                                    ,A.SMART_LOGRADOUROPRINCIPAL
                                    ,A.SMART_COMPLEMENTOPRINCIPAL
                                    ,A.ADDRESS1_POSTALCODE
                                    ,A.SMART_BAIRROPRINCIPAL
                                    ,B.SMART_PESSOANAME
                                    ,B.SMART_CPF
                                    ,B.TELEPHONE1
                                    ,B.TELEPHONE2
                                    ,B.TELEPHONE3
                                    ,B.EMAILADDRESS1
                                    ,B.EMAILADDRESS2
                                    ,A.SMART_CODIGOGLN
	                                ,A.SMART_PAISNAME
	                                ,A.TELEPHONE1
	                                ,A.SMART_DDIDDDTELEFONEPRINCIPAL
	                                ,A.gs1br_cnp20

                             FROM ACCOUNT A (NOLOCK)
                             LEFT JOIN CONTACT B ON B.ACCOUNTID = A.ACCOUNTID AND B.SMART_REPRESENTANTELEGAL = 1
                             WHERE SMART_CNPJCPF = @cpfcnpj
                             ORDER BY CASE A.SMART_CONDICAODERELACIONAMENTO WHEN 2 THEN 0 ELSE 1 END ASC
			                 ,CASE 
					                CASE WHEN EXISTS (
						                SELECT	Z.SMART_GESTAODELICENCASEPRODUTOID,
								                Z.SMART_NAME,
								                Z.SMART_FINANCEIROSTATUS,
								                Z.SMART_ASSOCIADO                  
						                FROM SMART_GESTAODELICENCASEPRODUTO Z WITH(NOLOCK)
						                WHERE Z.SMART_ASSOCIADO = A.ACCOUNTID 
						                AND Z.SMART_FINANCEIROSTATUS NOT IN (1,3)
					                ) THEN 1 ELSE 0
		                END
				                 WHEN 1 THEN 0 ELSE 1 END ASC
			                 ,CASE A.gs1br_cnp20 WHEN 1 THEN 0 ELSE 1 END ASC";

                using (Database db = new Database("BDBASEUNICACRM"))
                {
                    db.Open();
                    List<UsuarioCRM> retorno = db.Select(sql, param, usuarioCRM);
                    if(retorno.Count > 0){
                        return retorno[0];
                    }else{
                        return null;
                    }
                   
                }                
            }
            catch (Exception ex)
            {
                //TODO - remover ex
                //throw new Exception("Erro ao buscar informações do usuário." + ex.ToString());
                throw new Exception("Erro ao buscar informações do usuário.",ex);
            }

        }

        [GS1CNPAttribute(false)]
        public string BuscarAssociados(dynamic parametro)
        {
            Login user = (Login)this.getSession("USUARIO_LOGADO");

            if (user != null)
            {
                string sql = String.Empty;

                if (parametro != null && parametro.campos != null && parametro.campos.codigo != null)
                {
                    if (((GS1.CNP.BLL.Model.Login)(user)).id == 1)
                    {
                        sql = @"SELECT C.CODIGO, C.NOME, B.INDICADORPRINCIPAL,
                                (
	                                SELECT COUNT(*) FROM ASSOCIADOUSUARIO Z
	                                WHERE (Z.CODIGOUSUARIO = @codigo)
	                                AND Z.CODIGOASSOCIADO = C.CODIGO
                                ) AS CHECKED
                                FROM USUARIO A
                                    INNER JOIN ASSOCIADOUSUARIO B ON B.CODIGOUSUARIO = A.CODIGO
                                    INNER JOIN ASSOCIADO C ON C.CODIGO = B.CODIGOASSOCIADO
                                    WHERE A.CODIGO = " + parametro.campos.codigo;
                    }
                    else
                    {
                        sql = @"SELECT C.CODIGO, C.NOME, B.INDICADORPRINCIPAL,
                                (
	                                SELECT COUNT(*) FROM ASSOCIADOUSUARIO Z
	                                WHERE (Z.CODIGOUSUARIO = @codigo)
	                                AND Z.CODIGOASSOCIADO = C.CODIGO
                                ) AS CHECKED
                                FROM USUARIO A
                                    INNER JOIN ASSOCIADOUSUARIO B ON B.CODIGOUSUARIO = A.CODIGO
                                    INNER JOIN ASSOCIADO C ON C.CODIGO = B.CODIGOASSOCIADO
                                    WHERE A.CODIGO = @codigousuariologado";
                    }
                }
                else
                {
                    sql = @"SELECT C.CODIGO, C.NOME,
                            0 AS CHECKED
                            FROM USUARIO A
                                INNER JOIN ASSOCIADOUSUARIO B ON B.CODIGOUSUARIO = A.CODIGO
                                INNER JOIN ASSOCIADO C ON C.CODIGO = B.CODIGOASSOCIADO
                                WHERE A.CODIGO = @codigousuariologado";

                }

                using (Database db = new Database("BD"))
                {
                    db.Open();

                    dynamic param = null;

                    if (parametro.campos != null)
                    {
                        parametro.campos.codigousuariologado = user.id.ToString();
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.campos);
                    }

                    object retorno = db.Select(sql, param);
                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                return "Não foi possível buscar os associados.";
            }
        }
    }

    public class UsuarioCRM
    {     
        public Guid id { get; set; }
        public string nome { get; set; }
        public string nomefantasia { get; set; }
        public string email { get; set; }
        public string cpfcnpj { get; set; }
        public string cad { get; set; }
        public string relacionamento { get; set; }
        public bool bl_adimplente { get; set; }
        public bool bl_premium { get; set; }
        public bool bl_acessoTrade { get; set; }
        public string senha { get; set; }
        public string inscricaoestadual { get; set; }
        public string estadoname  { get; set; }
        public string numero { get; set; }
        public string address1_city { get; set; }
        public string logradouroprincipal { get; set; }
        public string complementoprincipal { get; set; }
        public string address1_postalcode { get; set; }
        public string bairroprincipal { get; set; }
        public string representante_smart_pessoaname { get; set; }
        public string representante_smart_cpf { get; set; }
        public string representante_telephone1 { get; set; }
        public string representante_telephone2 { get; set; }
        public string representante_telephone3 { get; set; }
        public string representante_emailaddress1 { get; set; }
        public string representante_emailaddress2 { get; set; }
        public string glninformativo { get; set; }
        public string pais { get; set; }
        public string telephone1 { get; set; }
        public string smart_ddidddtelefoneprincipal { get; set; }
        public bool gs1br_cnp20 { get; set; }
        public DateTime? datamodificacao { get; set; }
        
    }
}
