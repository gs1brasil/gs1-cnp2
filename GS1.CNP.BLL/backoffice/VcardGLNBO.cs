﻿using System;
using System.Text;
using System.IO;
using System.Web;
using System.Net;

namespace GS1.CNP.BLL
{
    public class VcardGLNBO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Organization { get; set; }
        public string JobTitle { get; set; }
        public string StreetAddress { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }        
        public byte[] Image { get; set; }
        public string Rev { get; set; }        
        public string Geo { get; set; }
        public string Note { get; set; }        
        


        public void DownloadVcard(string codigolocalizacaofisica)
        {
            LocalizacoesFisicasBO localizacaoBO = new LocalizacoesFisicasBO();
            LocalizacaoFisica localizacaofisica = localizacaoBO.BuscarLocalizacoesFisicasPK(codigolocalizacaofisica);


            FullName = localizacaofisica.nome;
            Organization = localizacaofisica.razaosocial;
            JobTitle = localizacaofisica.nomepapelgln;
            StreetAddress = localizacaofisica.endereco + ", " + localizacaofisica.numero + " " + localizacaofisica.bairro + " " + localizacaofisica.complemento + " ";
            Zip = localizacaofisica.cep;
            City = localizacaofisica.cidade;
            State = localizacaofisica.estado;
            CountryName = localizacaofisica.pais;
            Phone = localizacaofisica.telefone;
            Email = localizacaofisica.email;
            Rev = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            Geo = localizacaofisica.latitude + "," + localizacaofisica.longitude;
            Note = "GLN:" + localizacaofisica.gln;

            if (localizacaofisica.nomeimagem != string.Empty){
                LoadImage(localizacaofisica.nomeimagem);
            }

            string sFileName = FullName.Replace(" ","") + Rev +  ".vcf";
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "text/vcard";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + sFileName);
            var cardString = this.ToString();
            var inputEncoding = Encoding.Default;
            var outputEncoding = Encoding.GetEncoding("iso-8859-1");
            var cardBytes = inputEncoding.GetBytes(cardString);
            var outputBytes = Encoding.Convert(inputEncoding,
                                    outputEncoding, cardBytes);
            HttpContext.Current.Response.OutputStream.Write(outputBytes, 0, outputBytes.Length);
            HttpContext.Current.Response.End();

        }

        public void LoadImage(string FilePath) 
        {

            try 
	        {
                if (FilePath.StartsWith("http"))
                {
                    WebClient client = new WebClient();
                    Image = client.DownloadData(FilePath);                    
                }else{
                    Image = File.ReadAllBytes(FilePath);
                }               		
	        }
	        catch (Exception)
	        {
		
		        //throw;
                //Não causar erro caso não abra a imagem
	        }                     
        }


        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine("BEGIN:VCARD");
            builder.AppendLine("VERSION:2.1");
            // Name
            builder.AppendLine("N:" + LastName + ";" + FirstName);
            // Full name
            builder.AppendLine("FN:" + FullName);
            // Address
            builder.Append("ADR;WORK;PREF:;;");
            builder.Append(StreetAddress + ";");
            builder.Append(City + ";");
            builder.Append(State + ";");
            builder.Append(Zip + ";");
            builder.AppendLine(CountryName);
            // Other data
            builder.AppendLine("ORG:" + Organization);
            builder.AppendLine("TITLE:" + JobTitle);
            builder.AppendLine("TEL;WORK;VOICE:" + Phone);            
            builder.AppendLine("EMAIL;PREF;INTERNET:" + Email);
            if (Image != null)
            {
                builder.AppendLine("PHOTO;ENCODING=BASE64;TYPE=JPEG:");
                builder.AppendLine(Convert.ToBase64String(Image));
                builder.AppendLine(string.Empty);
            }
            builder.AppendLine("GEO:" + Geo);
            builder.AppendLine("NOTE:" + Note);
            builder.AppendLine("REV:" + Rev + ";");
            builder.AppendLine("END:VCARD");
            return builder.ToString();
        }

    }
}
