using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Globalization;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("relatorioAssociadoTermoAdesaoBO")]
    public class relatorioAssociadoTermoAdesaoBO : CrudBO
    {
        public override string nomeTabela { get { return ""; } }
        public override string nomePK { get { return ""; } }

        [GS1CNPAttribute("AcessarRelatorioAssociadoTermoAdesao", true, true)]
        public string relatorio(dynamic parametro)
        {
            return BuscarRelatorio(parametro, "2");
        }

        [GS1CNPAttribute("AcessarRelatorioAssociadoTermoAdesaoHistorico", true, true)]
        public string relatorioHistorico(dynamic parametro)
        {
            return BuscarRelatorio(parametro, "2,3");
        }

        private string BuscarRelatorio(dynamic parametro, string status)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    string query = @";WITH Relatorio_CTE (CODIGOASSOCIADO, RAZAOSOCIAL, CPFCNPJ, CAD, CODIGOTERMOADESAO, VERSAO, DATAACEITE) AS  
                                     (SELECT A.CODIGO,
			                                 A.NOME,
			                                 A.CPFCNPJ,
			                                 A.CAD,
			                                 E.CODIGO,
			                                 E.VERSAO,
			                                 MAX(D.DATAACEITE)
	                                  FROM   ASSOCIADO A WITH(NOLOCK)
	                                  INNER  JOIN ASSOCIADOUSUARIO B WITH(NOLOCK) ON A.CODIGO = B.CODIGOASSOCIADO
	                                  INNER  JOIN USUARIO C WITH(NOLOCK) ON B.CODIGOUSUARIO = C.CODIGO
	                                  INNER  JOIN TERMOADESAOUSUARIO D WITH(NOLOCK) ON C.CODIGO = D.CODIGOUSUARIO
	                                  INNER  JOIN TERMOADESAO E WITH(NOLOCK) ON D.CODIGOTERMOADESAO = E.CODIGO
	                                  WHERE  E.CODIGOSTATUSTERMOADESAO IN (" + status + @")
	                                  AND    D.DATAACEITE BETWEEN @periodoinicial AND @periodofinal
	                                  AND    (@razaosocial IS NULL OR A.NOME LIKE '%' + @razaosocial + '%')
	                                  AND    (@versao IS NULL OR E.CODIGO = @versao)
	                                  GROUP  BY A.CODIGO, A.NOME, A.CPFCNPJ, A.CAD, E.CODIGO, E.VERSAO)  
                                      SELECT *,
	                                         (SELECT A.NOME
		                                      FROM USUARIO A WITH(NOLOCK)
		                                      INNER JOIN TERMOADESAOUSUARIO B WITH(NOLOCK) ON A.CODIGO = B.CODIGOUSUARIO
		                                      WHERE B.CODIGOTERMOADESAO = R.CODIGOTERMOADESAO
		                                      AND B.DATAACEITE = R.DATAACEITE) AS NOMEUSUARIO
                                      FROM   Relatorio_CTE R
                                      ORDER  BY RAZAOSOCIAL";

                    dynamic param = null;

                    if (parametro != null && parametro.where != null)
                    {
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                        param.periodoinicial = DateTime.ParseExact(param.periodoinicial + " 00:00:00", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        param.periodofinal = DateTime.ParseExact(param.periodofinal + " 23:59:59", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }

        [GS1CNPAttribute(true, true, "AcessarRelatorioAssociadoTermoAdesao", "AcessarRelatorioAssociadoTermoAdesaoHistorico")]
        public string BuscarVersoes(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    return JsonConvert.SerializeObject(db.Select("SELECT CODIGO, VERSAO AS NOME FROM TERMOADESAO WHERE CODIGOSTATUSTERMOADESAO = 2"));
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}   
