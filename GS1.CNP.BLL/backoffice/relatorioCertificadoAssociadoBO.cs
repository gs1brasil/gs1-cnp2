using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("relatorioCertificadoAssociadoBO")]
    public class relatorioCertificadoAssociadoBO : CrudBO
    {
        public override string nomeTabela { get { return ""; } }
        public override string nomePK { get { return ""; } }

        [GS1CNPAttribute("AcessarRelatorioCertificadoAssociado", true, true)]
        public string relatorio(dynamic parametro)
        {
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            return BuscarRelatorio(param);
        }

        [GS1CNPAttribute("AcessarRelatorioCertificadoAssociadoHistorico", true, true)]
        public string relatorioHistorico(dynamic parametro)
        {
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            return BuscarRelatorio(param);
        }

        private string BuscarRelatorio(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                using (Database db = new Database("BDBASEUNICACNP"))
                {
                    db.Open();

                    string query = @"SELECT [AccountNumber], [smart_cnpjcpf], [Empresa], [Produto], [smart_SaldoAmostras], [Data de libera��o], [Status]
                                     FROM [vw_saldocertificacao]
                                     WHERE (@status IS NULL OR [STATUS] = @status)
                                     AND (@dataliberacao IS NULL OR [Data de libera��o] = @dataliberacao)
                                     AND (@empresa IS NULL OR ACCOUNTNUMBER = @empresa OR SMART_CNPJCPF = @empresa OR EMPRESA like '%' + @empresa + '%')
                                     ORDER BY PRODUTO ASC";

                    List<dynamic> retorno = db.Select(query, parametro);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}   
