using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("relatorioCertificadoAssociadoBO")]
    public class relatorioCertificadoBO : CrudBO
    {
        public override string nomeTabela { get { return ""; } }
        public override string nomePK { get { return ""; } }

        //[GS1CNPAttribute("AcessarRelatorioCertificadoAssociado", true, true)]
        [GS1CNPAttribute(false)]
        public string relatorioAssociado(dynamic parametro)
        {
            dynamic param = null;
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            if (parametro != null && parametro.where != null)
            {
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);
                param.empresa = associado.cad;
            }

            return BuscarRelatorio(param);
        }

        //[GS1CNPAttribute("AcessarRelatorioCertificadoAssociadoHistorico", true, true)]
        [GS1CNPAttribute(false)]
        public string relatorioGS1(dynamic parametro)
        {
            dynamic param = null;

            if (parametro != null && parametro.where != null)
                param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

            return BuscarRelatorio(param);
        }

        private string BuscarRelatorio(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                using (Database db = new Database("BDBASEUNICACNP"))
                {
                    db.Open();

                    string query = @"SELECT [AccountNumber] as CAD, [smart_cnpjcpf] as CPFCNPJ, [Empresa], [Produto], [smart_SaldoAmostras] as SALDO, [Data de libera��o] as DATALIBERACAO, [Status]
                                     FROM [vw_saldocertificacao]
                                     WHERE (@status IS NULL OR [STATUS] = @status)
                                     AND (@dataliberacao IS NULL OR [Data de libera��o] = @dataliberacao)
                                     AND (@empresa IS NULL OR ACCOUNTNUMBER = @empresa OR SMART_CNPJCPF = @empresa OR ACCOUNTNUMBER + ' - ' + EMPRESA like '%' + @empresa + '%')
                                     ORDER BY PRODUTO ASC";

                    List<dynamic> retorno = db.Select(query, parametro);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}   
