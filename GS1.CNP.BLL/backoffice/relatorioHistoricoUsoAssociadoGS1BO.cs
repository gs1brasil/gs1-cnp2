using System.Collections.Generic;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Globalization;

namespace GS1.CNP.BLL
{
    [GS1CNPAttribute("RelatorioHistoricoUsoAssociadoGS1BO")]
    public class RelatorioHistoricoUsoAssociadoGS1BO : CrudBO
    {
        public override string nomeTabela { get { return ""; } }
        public override string nomePK { get { return ""; } }

        [GS1CNPAttribute("AcessarRelatorioHistoricoUsoAssociadoGS1", true, true)]
        public string relatorio(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;
            Associado associado = this.getSession("ASSOCIADO_SELECIONADO") as Associado;

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    string query = @"SELECT	A.CODIGO AS CODIGOLOG,
	                                        D.NOME AS NOMEUSUARIO,
	                                        D.EMAIL AS EMAILUSUARIO,
	                                        F.NOME AS NOMEASSOCIADO,
	                                        F.CPFCNPJ,
	                                        B.NOME AS NOMEACAO,
	                                        A.DATAALTERACAO AS DATAACAO,
	                                        C.NOME AS NOMEFUNCIONALIDADE,
	                                        'Nome da Tela: ' + B.NOMETELA + 
	                                        '<br/> A��o: ' + B.NOME + 
	                                        '<br/> Usu�rio: ' + D.NOME + 
	                                        '<br/> Data/Hora: ' + CONVERT(VARCHAR, A.DATAALTERACAO, 103) + ' ' + CONVERT(VARCHAR, A.DATAALTERACAO, 108) +
                                            '<br/> Entidade: ' + 
		                                    CASE
			                                    WHEN A.CLASSE = 'AgenciasReguladorasBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM AgenciaReguladora WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'BannerBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM Banner WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'EspacoBannerBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM EspacoBanner WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'ParametroBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT CHAVE FROM Parametro WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'PerfilBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM Perfil WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'PesquisaSatisfacaoBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM PesquisaSatisfacao WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'ProdutoBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT PRODUCTDESCRIPTION FROM Produto WHERE CODIGOPRODUTO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'TermosAdesaoBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT VERSAO FROM TermoAdesao WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'TipoProdutoBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM TipoProduto WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'UsuarioBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM USUARIO WHERE CODIGO=A.CODIGOENTIDADE)
			                                    ELSE ''
		                                    END AS DESCRICAO
                                     FROM LOG A WITH(NOLOCK)
                                     INNER JOIN FUNCIONALIDADERELATORIOLOG B WITH(NOLOCK) ON A.CLASSE = B.CLASSE AND A.METODO = B.METODO
                                     INNER JOIN FUNCIONALIDADERELATORIO C WITH(NOLOCK) ON B.CODIGOFUNCIONALIDADERELATORIO = C.CODIGO
                                     INNER JOIN USUARIO D WITH(NOLOCK) ON A.CODIGOUSUARIO = D.CODIGO
                                     INNER JOIN ASSOCIADOUSUARIO E WITH(NOLOCK) ON D.CODIGO = E.CODIGOUSUARIO
                                     INNER JOIN ASSOCIADO F WITH(NOLOCK) ON E.CODIGOASSOCIADO = F.CODIGO
                                     WHERE (@cpfcnpj IS NULL OR F.CPFCNPJ = @cpfcnpj)
                                     AND (@razaosocial IS NULL OR F.NOME LIKE @razaosocial + '%')
                                     AND (@nomeusuario IS NULL OR D.NOME LIKE @nomeusuario + '%')
                                     AND (@login IS NULL OR D.EMAIL = @login)
                                     AND (@funcionalidade IS NULL OR C.CODIGO = @funcionalidade)
                                     AND A.DATAALTERACAO BETWEEN @periodoinicial AND @periodofinal
                                     UNION
                                     SELECT	A.CODIGO AS CODIGOLOG,
                                            D.NOME AS NOMEUSUARIO,
                                            D.EMAIL AS EMAILUSUARIO,
                                            F.NOME AS NOMEASSOCIADO,
                                            F.CPFCNPJ,
                                            B.NOME NOMEACAO,
                                            A.DATAALTERACAO AS DATAACAO,
                                            C.NOME AS NOMEFUNCIONALIDADE,
                                            'Nome da Tela: ' + B.NOMETELA + 
                                            '<br/> A��o: ' + B.NOME + 
                                            '<br/> Usu�rio: ' + D.NOME + 
                                            '<br/> Data/Hora: ' + CONVERT(VARCHAR, A.DATAALTERACAO, 103) + ' ' + CONVERT(VARCHAR, A.DATAALTERACAO, 108) +
                                            '<br/> Entidade: ' + 
		                                    CASE
			                                    WHEN A.CLASSE = 'AgenciasReguladorasBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM AgenciaReguladora WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'BannerBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM Banner WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'EspacoBannerBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM EspacoBanner WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'ParametroBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT CHAVE FROM Parametro WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'PerfilBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM Perfil WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'PesquisaSatisfacaoBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM PesquisaSatisfacao WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'ProdutoBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT PRODUCTDESCRIPTION FROM Produto WHERE CODIGOPRODUTO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'TermosAdesaoBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT VERSAO FROM TermoAdesao WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'TipoProdutoBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM TipoProduto WHERE CODIGO=A.CODIGOENTIDADE)
			                                    WHEN A.CLASSE = 'UsuarioBO' AND NOT A.CODIGOENTIDADE IS NULL THEN (SELECT NOME FROM USUARIO WHERE CODIGO=A.CODIGOENTIDADE)
			                                    ELSE ''
		                                    END AS DESCRICAO
                                     FROM [LOG] A WITH(NOLOCK)
                                     INNER JOIN FUNCIONALIDADERELATORIOLOG B WITH(NOLOCK) ON A.CLASSE = B.CLASSE AND A.METODO = B.METODO
                                     INNER JOIN FUNCIONALIDADERELATORIO C WITH(NOLOCK) ON B.CODIGOFUNCIONALIDADERELATORIO = C.CODIGO
                                     INNER JOIN USUARIO D WITH(NOLOCK) ON A.CODIGOUSUARIO = D.CODIGO
                                     INNER JOIN ASSOCIADO F WITH(NOLOCK) ON A.CODIGOASSOCIADO = F.CODIGO
                                     WHERE (@cpfcnpj IS NULL OR F.CPFCNPJ = @cpfcnpj)
                                     AND (@razaosocial IS NULL OR F.NOME LIKE @razaosocial + '%')
                                     AND (@nomeusuario IS NULL OR D.NOME LIKE @nomeusuario + '%')
                                     AND (@login IS NULL OR D.EMAIL = @login)
                                     AND (@funcionalidade IS NULL OR C.CODIGO = @funcionalidade)
                                     AND A.DATAALTERACAO BETWEEN @periodoinicial AND @periodofinal
                                     ORDER BY A.DATAALTERACAO DESC, F.NOME ASC";

                    dynamic param = null;

                    if (parametro != null && parametro.where != null)
                    {
                        param = GS1.CNP.BLL.Core.Util.ConvertJTokenToObject((JToken)parametro.where);

                        if (param.funcionalidade == "0")
                            param.funcionalidade = null;

                        param.periodoinicial = DateTime.ParseExact(param.periodoinicial + " 00:00:00", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        param.periodofinal = DateTime.ParseExact(param.periodofinal + " 23:59:59", "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    }

                    List<dynamic> retorno = db.Select(query, param);

                    return JsonConvert.SerializeObject(retorno);
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }

        [GS1CNPAttribute("AcessarRelatorioHistoricoUsoAssociadoGS1", true, true)]
        public string retornarFuncionalidade(dynamic parametro)
        {
            Login user = this.getSession("USUARIO_LOGADO") as Login;

            if (user != null)
            {
                using (Database db = new Database("BD"))
                {
                    db.Open();

                    return JsonConvert.SerializeObject(db.Select("SELECT CODIGO, NOME FROM FUNCIONALIDADERELATORIO"));
                }
            }
            else
            {
                throw new GS1TradeSessionException();
            }
        }
    }
}
