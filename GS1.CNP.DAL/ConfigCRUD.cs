﻿namespace GS1.CNP.DAL
{
    public class ConfigCRUD
    {
        public string nomeTabela { get; set; }
        public string nomePK { get; set; }
        public bool isIdentity { get; set; }

        /// <summary>
        /// Utilizado na paginação. Quantidade de registros que mostra no GRID
        /// </summary>
        public int registroPorPagina { get; set; }

        /// <summary>
        /// Utilizado na paginação. Número da página atual
        /// </summary>
        public int paginaAtual { get; set; }

        /// <summary>
        /// Comando SQL
        /// </summary>
        public string sql { get; set; }


        //public dynamic  { get; set; }
        //public dynamic where { get; set; }
    }
}
