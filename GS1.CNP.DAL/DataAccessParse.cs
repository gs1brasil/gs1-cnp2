﻿// -----------------------------------------------------------------------
// <copyright file="DataAccessParse.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace GS1.CNP.DAL
{
    using System;
    using Newtonsoft.Json.Schema;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DataAccessParse
    {
        #region Propriedades e Construtor

        public string nomeTabela { get; set; }

        /// <summary>
        /// Schema do JSON de insert, para uso do CRUD genérico
        /// </summary>
        private JsonSchema schemaInsert
        {
            get
            {
                return JsonSchema.Parse(@"{
                        'type': 'object'
                       }");
            }
        }

        /// <summary>
        /// Schema do JSON de Update, para uso do CRUD genérico
        /// </summary>
        private JsonSchema schemaUpdate
        {
            get
            {
                return JsonSchema.Parse(@"{
                      'type': 'object',
                      'type': 'object'
                    }");
            }
        }

        /// <summary>
        /// Schema do JSON de Delete, para uso do CRUD genérico
        /// </summary>
        private JsonSchema schemaDelete
        {
            get
            {
                return JsonSchema.Parse(@"{
                      'type': 'object'
                    }");
            }
        }

        private JsonSchema schemaSelectFiltro
        {
            get
            {
                return JsonSchema.Parse(@"{
                      'type': 'object',
                      'type': 'object'
                    }");
            }
        }

        private JsonSchema schemaSelectFiltroPaging
        {
            get
            {
                return JsonSchema.Parse(@"{
                      'type': 'object',
                      'type': 'object'
                    }");
            }
        }

        private string _comandoJson { get; set; }

        private ConfigCRUD _config { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonComando"></param>
        public DataAccessParse(string jsonComando, ConfigCRUD config)
        {
            this._comandoJson = jsonComando;
            this._config = config;

            if (config == null)
                throw new Exception("A classe [ConfigCRUD] deve ser instanciada.");

        }

        #endregion

        public string GeraComando(EnumComandos comando, bool stringLike = false)
        {
            switch (comando)
            {
                case EnumComandos.INSERT:
                    return Insert();
                case EnumComandos.UPDATE:
                    return Update();
                case EnumComandos.DELETE:
                    return Delete();
                case EnumComandos.SELECTALL:
                    return SelectAll();
                //case EnumComandos.SELECTALLPAGING:
                //    return SelectAllPaging();
                //case EnumComandos.SELECTFILTROPAGING:
                //    return SelectFiltroPaging();
                case EnumComandos.SELECTFILTRO:
                    return SelectFiltro();
                case EnumComandos.DELETEIN:
                    return DeleteIn();
                case EnumComandos.SELECT:
                    return Select(false, stringLike);
                case EnumComandos.SELECTPARAMETER:
                    return SelectParameter(false);
                case EnumComandos.INSERTMULTIPLOS:
                    return InsertMultiplos();
                default:
                    throw new Exception(string.Format("Comando não implementado {0}", comando.ToString()));
            }
        }

        #region Comandos

        private string InsertMultiplos()
        {
            string sql = "";

            //Faz o parse para jSON
            JObject j = JObject.Parse(this._comandoJson);

            //Valida sobre o schema de insert
            bool valid = j.IsValid(this.schemaInsert);

            if (valid)
            {
                objetoCRUD m = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);
                if (m != null)
                {
                    sql += string.Format("INSERT INTO {0} VALUES ", this._config.nomeTabela);

                    foreach (var item in m.campos.permissoes)
                    {
                        sql += "( '" + m.campos.codigo + "', '" + item.Value + "', '" + m.campos.DataAlteracao + "', '" + m.campos.CodigoUsuarioAlteracao + "'),";
                    }
                    sql = sql.Replace("[", string.Empty).Replace("]", string.Empty).Replace("\"", string.Empty);
                    sql = sql.Remove(sql.Length - 1);

                    if (this._config.isIdentity)
                    {
                        sql += string.Format(" SELECT * FROM {0} WHERE {1} = SCOPE_IDENTITY() ", this._config.nomeTabela, this._config.nomePK);
                    }
                    else
                    {
                        sql += string.Format(" SELECT * FROM {0} WHERE {1} = (SELECT MAX({2}) FROM {3}) ", this._config.nomeTabela, this._config.nomePK, this._config.nomePK, this._config.nomeTabela);
                    }

                    //TODO: Passar em alguma rotina para tirar o SQL INJECTION
                    return sql;
                }
                else
                {
                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
                }
            }
            else
            {
                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaInsert.ToString()));
            }
        }

        private string Insert()
        {
            string sql = "";

            //Faz o parse para jSON
            JObject j = JObject.Parse(this._comandoJson);

            //Valida sobre o schema de insert
            bool valid = j.IsValid(this.schemaInsert);

            if (valid)
            {
                objetoCRUD m = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);

                if (m != null)
                {
                    sql += string.Format("INSERT INTO {0} (", this._config.nomeTabela);

                    string values = "";

                    foreach (var item in m.campos)
                    {
                        if ((string)item.Value != "" && item.Name.ToLower() != "codigo")
                        {
                            sql += (string)item.Name + ",";
                            values += "'" + LimparTexto((string)item.Value) + "',";
                        }
                    }

                    sql = sql.Remove(sql.Length - 1);
                    values = values.Remove(values.Length - 1);

                    sql += ") VALUES (";
                    sql += values + ")";

                    if (this._config.isIdentity)
                    {
                        sql += string.Format(" SELECT * FROM {0} WHERE {1} = SCOPE_IDENTITY() ", this._config.nomeTabela, this._config.nomePK);
                    }
                    else
                    {
                        sql += string.Format(" SELECT * FROM {0} WHERE {1} = (SELECT MAX({2}) FROM {3}) ", this._config.nomeTabela, this._config.nomePK, this._config.nomePK, this._config.nomeTabela);
                    }

                    //TODO: Passar em alguma rotina para tirar o SQL INJECTION
                    return sql;
                }
                else
                {
                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
                }
            }
            else
            {
                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaInsert.ToString()));
            }
        }

        public string Update()
        {
            string sql = "";

            JObject j = JObject.Parse(this._comandoJson);

            bool valid = j.IsValid(this.schemaUpdate);

            if (valid)
            {
                objetoCRUD m = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);

                if (m != null)
                {
                    if (m.where != null)
                    {
                        sql += string.Format("UPDATE {0} SET ", this._config.nomeTabela);

                        foreach (var item in m.campos)
                        {
                            if (item.Value.Type.ToString() == "String" && item.Value == "")
                            {
                                sql += (string)item.Name + " = null ,";
                            }
                            else
                            {
                                sql += (string)item.Name + " = '" + LimparTexto((string)item.Value) + "',";
                            }
                        }
                        sql = sql.Remove(sql.Length - 1);

                        sql += " WHERE ";
                        sql += this._config.nomePK + " = " + LimparTexto(m.where.ToString());

                        //foreach (var item in m.where)
                        //{
                        //    sql += (string)item.Name + " = '" + (string)item.Value + "' AND ";
                        //}

                        //Retira o AND
                        //sql = sql.Remove(sql.Length - 5);

                        return sql;
                    }
                    else
                        throw new Exception(string.Format("Não é possível executar um comando de update sem a clausula where: [{0}]", _comandoJson));
                }
                else
                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
            }
            else
                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaUpdate.ToString()));
        }

        public string Delete(bool deleteIn = false)
        {
            string sql = "";

            JObject j = JObject.Parse(this._comandoJson);

            bool valid = j.IsValid(this.schemaDelete);

            if (valid)
            {
                objetoCRUD m = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);

                if (m != null)
                {
                    if (m.where != null)
                    {
                        sql += string.Format("DELETE FROM {0} ", this._config.nomeTabela);

                        if (deleteIn)
                        {
                            sql += string.Format(" WHERE {0} IN ({1})", _config.nomePK, LimparTexto(m.where.whereIn));
                        }
                        else
                        {
                            sql += " WHERE Codigo = " + m.where;
                            //GetCondition(m.where);
                        }

                        return sql;
                    }
                    else
                    {
                        throw new Exception(string.Format("Não é possível executar um comando de update sem a clausula where: [{0}]", _comandoJson));
                    }
                }
                else
                {
                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
                }
            }
            else
            {
                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaDelete.ToString()));
            }
        }

        public string DeleteIn()
        {
            string sql = "";

            JObject j = JObject.Parse(this._comandoJson);

            bool valid = j.IsValid(this.schemaDelete);

            if (valid)
            {
                objetoCRUD m = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);
                if (m != null)
                {
                    if (m.where != null)
                    {
                        sql += string.Format("DELETE FROM {0} ", this._config.nomeTabela);

                        sql += string.Format(" WHERE {0} IN (", _config.nomePK);

                        foreach (var item in m.where.whereIn)
                        {
                            sql += LimparTexto((string)item) + " ,";
                        }

                        //Retira o AND
                        sql = sql.Remove(sql.Length - 1);
                        sql += ")";

                        return sql;
                    }
                    else
                    {
                        throw new Exception(string.Format("Não é possível executar um comando de update sem a clausula where: [{0}]", _comandoJson));
                    }
                }
                else
                {
                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
                }
            }
            else
            {
                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaDelete.ToString()));
            }
        }

        public string SelectAll()
        {
            return string.Format("SELECT * FROM {0}", this._config.nomeTabela);
        }

        public string SelectAllPaging()
        {
            string sql = @"DECLARE @RowsPerPage INT = {0}, @PageNumber INT = {1}
                                SELECT *
                                FROM (
                                SELECT 
                                ROW_NUMBER() OVER (ORDER BY {2}) AS RowNum, *
                                FROM {3} )as RowConstrainedResult
                                WHERE RowNum BETWEEN ((@PageNumber-1)*@RowsPerPage)+1
                                AND @RowsPerPage*(@PageNumber)";

            return string.Format(sql, this._config.registroPorPagina, this._config.paginaAtual, this._config.nomePK, this._config.nomeTabela);
        }

        //        public string SelectFiltroPaging()
        //        {

        //            string sql = "";

        //            //Faz o parse para jSON
        //            JObject j = JObject.Parse(this._comandoJson);

        //            //Valida sobre o schema de insert
        //            bool valid = j.IsValid(this.schemaInsert);

        //            if (valid)
        //            {
        //                objetoCRUD m = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);

        //                if (m != null)
        //                {

        //                    if (m.where != null)
        //                    {
        //                        sql += "SELECT * ";

        //                        sql += string.Format("FROM (");
        //                        sql += string.Format(" SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS RowNum, * FROM {1} )as RowConstrainedResult WHERE RowNum BETWEEN (({3}-1)*{2})+1 AND {2}*({3})", this._config.nomePK, this._config.nomeTabela, this._config.registroPorPagina, this._config.paginaAtual);

        //                        sql += " AND ";

        //                        foreach (var item in m.where)
        //                        {
        //                            sql += (string)item.Name + " = '" + (string)item.Value + "' AND ";
        //                        }

        //                        //Retira o AND
        //                        sql = sql.Remove(sql.Length - 5);
        //                    }

        //                    return sql;
        //                }
        //                else
        //                {
        //                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
        //                }
        //            }
        //            else
        //            {
        //                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaInsert.ToString()));
        //            }

        //        }

        public string SelectFiltro()
        {

            string sql = "";

            //Faz o parse para jSON
            JObject j = JObject.Parse(this._comandoJson);

            //Valida sobre o schema de insert
            bool valid = j.IsValid(this.schemaInsert);

            if (valid)
            {
                objetoCRUD objCrud = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);

                if (objCrud != null)
                {
                    if (objCrud.where != null)
                    {
                        sql += "SELECT * ";

                        sql += string.Format("FROM {0}", this._config.nomeTabela);

                        sql += GetCondition(objCrud.where);
                    }

                    //TODO: Passar em alguma rotina para tirar o SQL INJECTION
                    return sql;
                }
                else
                {
                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
                }
            }
            else
            {
                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaInsert.ToString()));
            }
        }

        public string Select(bool paging = false, bool stringLike = false)
        {
            string sql = "";

            //Faz o parse para jSON
            JObject j = JObject.Parse(this._comandoJson);

            //Valida sobre o schema de insert
            bool valid = j.IsValid(this.schemaInsert);

            if (valid)
            {
                objetoCRUD objCrud = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);

                if (objCrud != null)
                {
                    if (objCrud.sql != null)
                    {
                        sql += objCrud.sql;

                        if (paging)
                        {
                            sql = sql.Replace("SELECT", string.Format("SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS RowNum, ", this._config.nomePK));
                        }
                    }
                    else
                    {
                        sql = "SELECT *";

                        if (paging)
                        {
                            sql += string.Format(", ROW_NUMBER() OVER (ORDER BY {0}) AS RowNum", this._config.nomePK);
                        }

                        sql += string.Format(" FROM {0}", this._config.nomeTabela);
                    }

                    sql += GetCondition(objCrud.where, stringLike);

                    if (paging)
                    {
                        sql = string.Format(@"DECLARE @RowsPerPage INT = {0}, @PageNumber INT = {1}
                                SELECT *
                                FROM ({2})as RowConstrainedResult
                                WHERE RowNum BETWEEN ((@PageNumber - 1) * @RowsPerPage) + 1
                                AND (@RowsPerPage * @PageNumber)", this._config.registroPorPagina, this._config.paginaAtual, sql);
                    }

                    //TODO: Passar em alguma rotina para tirar o SQL INJECTION
                    return sql;
                }
                else
                {
                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
                }
            }
            else
            {
                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaInsert.ToString()));
            }
        }

        public string SelectParameter(bool paging = false)
        {
            string sql = "";

            //Faz o parse para jSON
            JObject j = JObject.Parse(this._comandoJson);

            //Valida sobre o schema de insert
            bool valid = j.IsValid(this.schemaInsert);

            if (valid)
            {
                objetoCRUD objCrud = JsonConvert.DeserializeObject<objetoCRUD>(this._comandoJson);

                if (objCrud != null)
                {
                    if (objCrud.sql != null)
                    {
                        sql += objCrud.sql;

                        foreach (var item in objCrud.where)
                        {
                            //if (item.Value != null && !item.Value.ToString().Equals("null"))
                            //{
                            string valor = item.Value == null ? "null" : LimparTexto(item.Value.ToString());

                            if (Util.IsInteger(valor))
                            {
                                sql = sql.Replace(":" + (string)item.Name, valor);
                                //condition += string.Format("{0}{1}{2} = {3}", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                            }
                            else if (Util.IsDate(valor))
                            {
                                sql = sql.Replace(":" + (string)item.Name, valor);
                                //condition += string.Format("{0}{1}{2} = '{3}'", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                            }
                            else if (Util.IsBoolean(valor))
                            {
                                sql = sql.Replace(":" + (string)item.Name, valor);
                                valor = (Boolean.Parse(valor) ? "1" : "0");
                                //condition += string.Format("{0}{1}{2} = {3}", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                            }
                            else
                            {
                                sql = sql.Replace(":" + (string)item.Name, valor);
                                /*if (stringLike)
                                {
                                    condition += string.Format("{0}{1}{2} LIKE '%{3}%'", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                                }
                                else
                                {
                                    condition += string.Format("{0}{1}{2} = '{3}'", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                                }*/
                            }
                            //}
                        }

                        if (paging)
                        {
                            sql = sql.Replace("SELECT", string.Format("SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS RowNum, ", this._config.nomePK));
                        }
                    }
                    else
                    {
                        throw new Exception(string.Format("Select não informado", _comandoJson));
                    }

                    if (paging)
                    {
                        sql = string.Format(@"DECLARE @RowsPerPage INT = {0}, @PageNumber INT = {1}
                                SELECT *
                                FROM ({2})as RowConstrainedResult
                                WHERE RowNum BETWEEN ((@PageNumber - 1) * @RowsPerPage) + 1
                                AND (@RowsPerPage * @PageNumber)", this._config.registroPorPagina, this._config.paginaAtual, sql);
                    }

                    //TODO: Passar em alguma rotina para tirar o SQL INJECTION
                    return sql;
                }
                else
                {
                    throw new Exception(string.Format("Não foi possível converter a string [{0}] no objetoCRUD!", _comandoJson));
                }
            }
            else
            {
                throw new Exception(string.Format("Json [{0}] inválido de acordo com o schema [{1}]", _comandoJson, this.schemaInsert.ToString()));
            }
        }

        private string GetCondition(dynamic where, bool stringLike = false)
        {
            if (where != null)
            {
                string separador = " AND ";
                string condition = string.Empty;

                foreach (var item in where)
                {
                    if (item.Value != null && !item.Value.ToString().Equals("null"))
                    {
                        string valor = LimparTexto(item.Value.ToString());

                        if (Util.IsInteger(valor))
                        {
                            condition += string.Format("{0}{1}{2} = {3}", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                        }
                        else if (Util.IsDate(valor))
                        {
                            condition += string.Format("{0}{1}{2} = '{3}'", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                        }
                        else if (Util.IsBoolean(valor))
                        {
                            valor = (Boolean.Parse(valor) ? "1" : "0");
                            condition += string.Format("{0}{1}{2} = {3}", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                        }
                        else
                        {
                            if (stringLike)
                            {
                                condition += string.Format("{0}{1}{2} LIKE '%{3}%'", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                            }
                            else
                            {
                                condition += string.Format("{0}{1}{2} = '{3}'", (string.IsNullOrEmpty(condition) ? string.Empty : separador), this.nomeTabela, (string)item.Name, valor);
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(condition))
                {
                    return " WHERE " + condition;
                }
            }

            return string.Empty;
        }

        private string LimparTexto(string str)
        {
            if (str != null)
            {
                str = str.Replace("=", "")
                .Replace("'", "''")
                .Replace("\"\"", "")
                .Replace(" or ", "")
                .Replace(" and ", "")
                .Replace("(", "")
                .Replace(")", "")
                .Replace("<", "[")
                .Replace(">", "]")
                .Replace("update", "")
                .Replace("-shutdown", "")
                .Replace("--", "")
                .Replace("#", "")
                .Replace("$", "")
                .Replace("%", "")
                .Replace("¨", "")
                .Replace("&", "")
                .Replace("'or'1'='1'", "")
                .Replace("--", "")
                .Replace("insert", "")
                .Replace("drop", "")
                .Replace("delet", "")
                .Replace("xp_", "")
                .Replace("select", "");
            }
            return str;

        }
    }

        #endregion
}
