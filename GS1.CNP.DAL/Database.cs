﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Data.Common;

namespace GS1.CNP.DAL
{
    public sealed class Database : IDisposable
    {
        #region Variables

        private IDbConnection Connection;
        private IDbCommand Command;
        private IDbTransaction Transaction;
        private bool UseTransaction;
        private int commandTimeout = 600;

        public int CommandTimeout
        {
            get
            {
                return this.commandTimeout;
            }
            set
            {
                this.commandTimeout = value;
            }
        }

        private static Regex rxParams = new Regex(@"(?<!@)@\w+", RegexOptions.Compiled);
        private static Func<IDataRecord, dynamic> binderDefault = x =>
        {
            dynamic resultBinder = new ExpandoObject();

            for (int i = 0; i < x.FieldCount; i++)
                ((IDictionary<string, object>)resultBinder).Add(x.GetName(i).ToLower(), x.IsDBNull(i) ? string.Empty : x.GetValue(i));

            return resultBinder;
        };


        #endregion

        #region Constructor e Dispose

        public Database(string key)
        {
            this.Connection = this.CreateDbConnection(key);

            if (this.Connection == null)
                new InvalidOperationException("Cannot open a connection without specifying a data source or server.");
        }

        public void Dispose()
        {
            this.Close();
        }

        #endregion

        #region Conection

        public void Open(bool useTransaction = false)
        {
            if (this.Connection.State != ConnectionState.Open)
                this.Connection.Open();

            this.UseTransaction = useTransaction;
        }

        public void Close()
        {
            if (this.Connection.State != ConnectionState.Closed)
            {
                if (this.Command != null)
                {
                    this.Command.Dispose();
                    this.Command = null;
                }

                //TODO - Adicionado por Eduardo 04/11/2014 - VALIDAR
                if (this.Transaction != null && this.Transaction.Connection != null)
                {
                    this.Transaction.Rollback();
                    this.Transaction.Dispose();
                    this.Transaction = null;
                }

                if (this.Connection != null)
                {
                    this.Connection.Close();
                    this.Connection.Dispose();
                    this.Connection = null;
                }
            }
        }

        #endregion

        #region Transaction

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            if (this.Transaction == null)
                this.Transaction = this.Connection.BeginTransaction(isolationLevel);

            this.UseTransaction = true;
        }

        public void Commit()
        {
            if (this.Transaction != null)
            {
                this.Transaction.Commit();
                this.Transaction = null;
                this.UseTransaction = false;
            }
        }

        public void Rollback()
        {
            if (this.Transaction != null)
            {
                this.Transaction.Rollback();
                this.Transaction = null;
                this.UseTransaction = false;
            }
        }

        #endregion

        #region Public Methods

        public dynamic All(string tableName)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            string sql = string.Format("SELECT * FROM {0} (Nolock) ", tableName);

            IEnumerable<dynamic> result = SelectMethod(sql, null, binderDefault);

            return result.ToList();
        }

        public List<T> All<T>(string tableName, Func<IDataRecord, T> binder = null)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            string sql = string.Format("SELECT * FROM {0} (nolock)", tableName);

            IEnumerable<T> result = SelectMethod<T>(sql, null, binder);

            return result.ToList();
        }

        public int Count(string tableName, dynamic dynamicObject = null)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            string sql = string.Format("SELECT COUNT(0) FROM {0} (Nolock) ", tableName);

            if (dynamicObject != null && ((IDictionary<string, object>)dynamicObject).Count > 0)
            {
                sql += this.GenerateWhere(dynamicObject);

                this.PrepareCommand(sql);

                IEnumerable<Match> matches = rxParams.Matches(sql).Cast<Match>().Distinct(new MatchComparer());

                if (matches != null && matches.Count() > 0)
                {
                    foreach (Match match in matches)
                        this.AddParameter(match.Value, dynamicObject);
                }
            }
            else
                this.PrepareCommand(sql);

            int rowsCount;

            rowsCount = Convert.ToInt32(this.Command.ExecuteScalar());

            this.Command.Dispose();

            return rowsCount;
        }

        public int Count(string tableName, string condition, dynamic dynamicObject = null)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            if (string.IsNullOrEmpty(condition))
                throw new ArgumentNullException("condition");

            string sql = string.Format("SELECT COUNT(0) FROM {0} (Nolock) WHERE {1}", tableName, condition);

            this.PrepareCommand(sql);

            IEnumerable<Match> matches = rxParams.Matches(sql).Cast<Match>().Distinct(new MatchComparer());

            if (matches != null && matches.Count() > 0)
            {
                if (dynamicObject != null && ((IDictionary<string, object>)dynamicObject).Count == 0)
                    throw new ArgumentNullException("dynamicObject");

                foreach (Match match in matches)
                    this.AddParameter(match.Value, dynamicObject);
            }

            int rowsCount;

            rowsCount = Convert.ToInt32(this.Command.ExecuteScalar());

            this.Command.Dispose();
            this.Command = null;

            return rowsCount;
        }

        public int Count(string query, string tableName, string condition, dynamic dynamicObject = null)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            if (string.IsNullOrEmpty(condition))
                throw new ArgumentNullException("condition");

            string sql = string.Format(query, tableName, condition);

            this.PrepareCommand(sql);

            IEnumerable<Match> matches = rxParams.Matches(sql).Cast<Match>().Distinct(new MatchComparer());

            if (matches != null && matches.Count() > 0)
            {
                if (dynamicObject != null && ((IDictionary<string, object>)dynamicObject).Count == 0)
                    throw new ArgumentNullException("dynamicObject");

                foreach (Match match in matches)
                    this.AddParameter(match.Value, dynamicObject);
            }

            int rowsCount;

            rowsCount = Convert.ToInt32(this.Command.ExecuteScalar());

            this.Command.Dispose();
            this.Command = null;

            return rowsCount;
        }

        public dynamic Find(string tableName, dynamic dynamicObject, string columns = "*")
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            string sql = string.Format("SELECT {1} FROM {0} ", tableName, columns);

            //Comentado por Eduardo 2016-11-10
            //if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
            //    throw new ArgumentNullException("dynamicObject");

            sql += this.GenerateWhere(dynamicObject);

            IEnumerable<dynamic> result = SelectMethod(sql, dynamicObject, binderDefault);

            return result.FirstOrDefault();
        }

        public dynamic Find(string tableName, string condition, dynamic dynamicObject, string columns = "*")
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("condition");

            string sql = string.Format("SELECT {2} FROM {0} WHERE {1}", tableName, condition, columns);

            //Comentado por Eduardo 2016-11-10
            //if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
            //    throw new ArgumentNullException("dynamicObject");

            IEnumerable<dynamic> result = SelectMethod(sql, dynamicObject, binderDefault);

            return result.FirstOrDefault();
        }

        public T Find<T>(string tableName, dynamic dynamicObject, string columns = "*", Func<IDataRecord, T> binder = null)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            string sql = string.Format("SELECT {1} FROM {0} ", tableName, columns);

            //Comentado por Eduardo 2016-11-10
            //if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
            //    throw new ArgumentNullException("dynamicObject");

            sql += this.GenerateWhere(dynamicObject);

            IEnumerable<dynamic> result = SelectMethod(sql, dynamicObject, binder);

            return result.FirstOrDefault();
        }

        public T Find<T>(string tableName, string condition, dynamic dynamicObject, string columns = "*", Func<IDataRecord, T> binder = null)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("condition");

            string sql = string.Format("SELECT {2} FROM {0} WHERE {1}", tableName, condition, columns);

            //Comentado por Eduardo 2016-11-10
            //if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
            //    throw new ArgumentNullException("dynamicObject");

            IEnumerable<dynamic> result = SelectMethod(sql, dynamicObject, binder);

            return result.FirstOrDefault();
        }

        public dynamic Select(string commandText, dynamic dynamicObject = null)
        {
            IEnumerable<dynamic> result = SelectMethod(commandText, dynamicObject, binderDefault);

            return result.ToList();
        }

        public List<T> Select<T>(string commandText, dynamic dynamicObject = null, Func<IDataRecord, T> binder = null)
        {
            IEnumerable<T> result = SelectMethod(commandText, dynamicObject, binder);

            return result.ToList();
        }

        public dynamic ExecuteProcedure(string commandText, dynamic dynamicObject = null)
        {
            IEnumerable<dynamic> result = ExecuteProcMethod(commandText, dynamicObject, binderDefault);

            return result.ToList();
        }

        public dynamic ExecuteProcedure(string commandText, dynamic dynamicObject = null, dynamic dynamicObjectOutput = null)
        {
            dynamic result = ExecuteProcMethod(commandText, dynamicObject, dynamicObjectOutput);

            return result;
        }

        public List<T> ExecuteProcedure<T>(string commandText, dynamic dynamicObject = null, Func<IDataRecord, T> binder = null)
        {
            IEnumerable<T> result = ExecuteProcMethod<T>(commandText, dynamicObject, binder);

            return result.ToList();
        }

        public int ExecuteCommand(string command, dynamic dynamicObject = null)
        {
            return this.Execute(command, dynamicObject, false);
        }

        public int Insert(string commandText, dynamic dynamicObject, bool returnIdentity = true)
        {
            return this.Execute(commandText, dynamicObject, returnIdentity);
        }

        public int Insert(string tableName, string keyName, dynamic dynamicObject, bool returnIdentity = true)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
                throw new ArgumentNullException("dynamicObject");

            string commandText = "INSERT INTO {0} ({1}) VALUES ({2});",
                columns = string.Empty,
                values = string.Empty;

            foreach (KeyValuePair<string, object> item in (IDictionary<string, object>)dynamicObject)
            {
                if (!item.Key.ToLower().Equals(keyName.ToLower()))
                {
                    columns += string.IsNullOrEmpty(columns) ? item.Key : "," + item.Key;
                    values += string.IsNullOrEmpty(values) ? "@" + item.Key : ",@" + item.Key;
                }
            }

            return this.Execute(string.Format(commandText, tableName, columns, values), dynamicObject, returnIdentity);
        }

        public int Update(string commandText, dynamic dynamicObject)
        {
            return this.Execute(commandText, dynamicObject, false);
        }

        public int Update(string tableName, string keyName, dynamic dynamicObject)
        {
            if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
                throw new ArgumentNullException("dynamicObject");

            string commandText = "UPDATE {0} SET {1} WHERE {2} = @{2};",
                columns = string.Empty;

            foreach (KeyValuePair<string, object> item in (IDictionary<string, object>)dynamicObject)
            {
                if (!item.Key.ToLower().Equals(keyName.ToLower()))
                {
                    columns += string.IsNullOrEmpty(columns) ? string.Format("{0} = @{0}", item.Key) : string.Format(",{0} = @{0}", item.Key);
                }
            }

            return this.Execute(string.Format(commandText, tableName, columns, keyName), dynamicObject, false);
        }

        public int Delete(string commandText, dynamic dynamicObject = null)
        {
            return this.Execute(commandText, dynamicObject, false);
        }

        public int Delete(string tableName, string keyName, dynamic dynamicObject)
        {
            if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
                throw new ArgumentNullException("dynamicObject");

            string commandText = "DELETE FROM {0} WHERE {1} = @{1};";

            return this.Execute(string.Format(commandText, tableName, keyName), dynamicObject, false);
        }

        #endregion

        #region Private Methods

        public DbConnection CreateDbConnection(string key)
        {
            DbConnection connection = null;

            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("key");

            if (ConfigurationManager.ConnectionStrings[key] != null)
            {
                try
                {
                    DbProviderFactory factory = DbProviderFactories.GetFactory(ConfigurationManager.ConnectionStrings[key].ProviderName);

                    connection = factory.CreateConnection();
                    connection.ConnectionString = ConfigurationManager.ConnectionStrings[key].ConnectionString;
                }
                catch
                {
                    if (connection != null)
                        connection = null;
                }
            }
            else
                throw new ArgumentOutOfRangeException("key");

            return connection;
        }

        private IEnumerable<T> SelectMethod<T>(string commandText, dynamic dynamicObject, Func<IDataRecord, T> binder)
        {
            if (string.IsNullOrEmpty(commandText))
                throw new ArgumentNullException("commandText");

            IEnumerable<Match> matches = rxParams.Matches(commandText).Cast<Match>().Distinct(new MatchComparer());

            this.PrepareCommand(commandText);

            if (matches != null && matches.Count() > 0)
            {
                //Comentado por Eduardo 19/09/2016
                //if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
                //    throw new ArgumentNullException("dynamicObject");

                this.Command.Parameters.Clear();

                foreach (Match match in matches)
                    this.AddParameter(match.Value, dynamicObject);
            }

            IDataReader reader = this.Command.ExecuteReader();

            if (!reader.IsClosed)
            {
                try
                {
                    while (reader.Read())
                        yield return binder(reader);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;

                    this.Command.Dispose();
                    this.Command = null;
                }
            }
        }

        private int Execute(string commandText, dynamic dynamicObject = null, bool returnIdentity = false)
        {
            if (string.IsNullOrEmpty(commandText))
                throw new ArgumentNullException("commandText");

            IEnumerable<Match> matches = rxParams.Matches(commandText).Cast<Match>().Distinct(new MatchComparer());

            if (returnIdentity)
                commandText += " SELECT @@IDENTITY";

            this.PrepareCommand(commandText);

            if (matches != null && matches.Count() > 0)
            {
                if (dynamicObject == null || ((IDictionary<string, object>)dynamicObject).Count == 0)
                    throw new ArgumentNullException("dynamicObject");

                foreach (Match match in matches)
                    this.AddParameter(match.Value, dynamicObject);
            }

            int rowsAffected;

            if (returnIdentity)
                rowsAffected = Convert.ToInt32(this.Command.ExecuteScalar());
            else
                rowsAffected = this.Command.ExecuteNonQuery();

            this.Command.Dispose();
            this.Command = null;

            return rowsAffected;
        }

        private IEnumerable<T> ExecuteProcMethod<T>(string commandText, dynamic dynamicObject, Func<IDataRecord, T> binder)
        {
            if (string.IsNullOrEmpty(commandText))
                throw new ArgumentNullException("commandText");

            IDictionary<string, object> parameters = (IDictionary<string, object>)dynamicObject;

            this.PrepareCommand(commandText, CommandType.StoredProcedure);

            if (parameters != null && parameters.Count() > 0)
            {
                this.Command.Parameters.Clear();

                foreach (KeyValuePair<string, object> match in parameters)
                    this.AddParameterValue("@" + match.Key, match.Value);
            }

            IDataReader reader = this.Command.ExecuteReader();

            if (!reader.IsClosed)
            {
                try
                {
                    while (reader.Read())
                        yield return binder(reader);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;

                    this.Command.Dispose();
                    this.Command = null;
                }
            }

        }

        private dynamic ExecuteProcMethod(string commandText, dynamic dynamicObject, dynamic dynamicObjectOut)
        {
            if (string.IsNullOrEmpty(commandText))
                throw new ArgumentNullException("commandText");

            IDictionary<string, object> parameters = (IDictionary<string, object>)dynamicObject;
            IDictionary<string, object> parametersOut = (IDictionary<string, object>)dynamicObjectOut;

            this.PrepareCommand(commandText, CommandType.StoredProcedure);

            if (parameters != null && parameters.Count() > 0)
            {
                this.Command.Parameters.Clear();

                foreach (KeyValuePair<string, object> match in parameters)
                    this.AddParameterValue("@" + match.Key, match.Value);
            }

            if (parametersOut != null && parametersOut.Count() > 0)
            {
                foreach (KeyValuePair<string, object> matchOut in parametersOut)
                {
                    IDictionary<string, object> parametersOut2 = (IDictionary<string, object>)matchOut.Value;
                    this.AddParameterValueOutuput("@" + matchOut.Key, (System.Data.DbType)parametersOut2["type"], (int)parametersOut2["size"]);
                }
            }

            IDataReader reader = this.Command.ExecuteReader();

            if (!reader.IsClosed)
            {
                try
                {
                    //while (reader.Read())
                    //    yield return binder(reader);
                    Dictionary<string, dynamic> paramReturn = new Dictionary<string, dynamic>();
                    foreach (IDataParameter p in this.Command.Parameters)
                    {
                        paramReturn.Add(p.ParameterName.Replace("@", ""), p.Value);
                    }
                    return paramReturn;
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;

                    this.Command.Dispose();
                    this.Command = null;
                }
            }
            return null;

        }

        private void PrepareCommand(string commandText, CommandType commandType = CommandType.Text)
        {
            if (string.IsNullOrEmpty(commandText))
                throw new ArgumentNullException("commandText");

            this.Command = this.Connection.CreateCommand();

            this.Command.CommandText = commandText;
            this.Command.CommandType = commandType;
            this.Command.CommandTimeout = this.commandTimeout;

            if (this.UseTransaction)
                this.Command.Transaction = this.Transaction;
        }

        private void AddParameter(string parameterName, dynamic dynamicObject)
        {
            IDictionary<string, object> values = (IDictionary<string, object>)dynamicObject;
            object value;

            if (values.ContainsKey(parameterName.Substring(1)))
                value = values[parameterName.Substring(1)];
            else
                throw new ArgumentOutOfRangeException(parameterName);

            IDbDataParameter parameter = this.Command.CreateParameter();
            parameter.ParameterName = parameterName;

            //Alterado por Eduardo 29/10/2015
            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(value.ToString()))
                    parameter.Value = value;
                else
                    parameter.Value = DBNull.Value;
            }

            this.Command.Parameters.Add(parameter);
        }

        private void AddParameterValue(string parameterName, object value)
        {
            IDbDataParameter parameter = this.Command.CreateParameter();
            parameter.ParameterName = parameterName;

            if (value == null)
            {
                parameter.Value = DBNull.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(value.ToString()))
                    parameter.Value = value;
                else
                    parameter.Value = DBNull.Value;
            }

            this.Command.Parameters.Add(parameter);
        }

        private void AddParameterValueOutuput(string parameterName, DbType dbtype, int size)
        {

            //TODO - Criado por Eduardo 17/09/2015 - validar!
            IDbDataParameter parameter = this.Command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.Direction = ParameterDirection.Output;
            parameter.Size = size;
            parameter.DbType = dbtype;

            this.Command.Parameters.Add(parameter);

        }

        private string GenerateWhere(dynamic dynamicObject)
        {
            if (dynamicObject != null && ((IDictionary<string, object>)dynamicObject).Count > 0)
            {
                StringBuilder sbWhere = new StringBuilder();

                foreach (KeyValuePair<string, object> item in dynamicObject)
                    sbWhere.Append((sbWhere.Length == 0 ? string.Format("{0} = @{0}", item.Key) : string.Format(" AND {0} = @{0}", item.Key)));

                if (sbWhere.Length > 0)
                    sbWhere.Insert(0, "WHERE ");

                return sbWhere.ToString();
            }

            return string.Empty;
        }

        #endregion
    }

    class MatchComparer : IEqualityComparer<Match>
    {
        public bool Equals(Match a, Match b)
        {
            return a.Value == b.Value;
        }

        public int GetHashCode(Match match)
        {
            return match.Value.GetHashCode();
        }
    }
}