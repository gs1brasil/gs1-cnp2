﻿using System.Collections.Generic;
using System.Data.Common;

namespace GS1.CNP.DAL
{
    public static class DbCommandExtension
    {
        static Dictionary<string, object> parameters = new Dictionary<string, object>();

        public static void AddParameterWithValue(this DbCommand command, string parameterName, object parameterValue)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.Value = parameterValue;
            command.Parameters.Add(parameter);
        }

        public static void AddParameterWithValue(this Database database, string parameterName, object parameterValue)
        {
            parameters.Add(parameterName, parameterValue);
        }
    }
}
