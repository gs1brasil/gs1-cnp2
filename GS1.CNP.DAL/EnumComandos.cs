﻿// -----------------------------------------------------------------------
// <copyright file="EnumComandos.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace GS1.CNP.DAL
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public enum EnumComandos
    {
        INSERT = 0,
        DELETE = 1,
        UPDATE = 2,
        SELECTALL = 3,
        //SELECTALLPAGING = 4,
        SELECTFILTRO = 5,
        //SELECTFILTROPAGING = 6,
        DELETEIN = 7,
        SELECT = 8,
        SELECTPARAMETER = 10,
        INSERTMULTIPLOS = 9
    }
}
