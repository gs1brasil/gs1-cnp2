﻿using System;

namespace GS1.CNP.DAL
{
    public class Util
    {
        public static bool IsDate(Object obj)
        {
            string strDate = obj.ToString();

            try
            {
                DateTime dt = DateTime.Parse(strDate);

                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                {
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsInteger(string value)
        {
            try
            {
                Int64.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsDecimal(string value)
        {
            try
            {
                Decimal.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsBoolean(string value)
        {
            try
            {
                Boolean.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
