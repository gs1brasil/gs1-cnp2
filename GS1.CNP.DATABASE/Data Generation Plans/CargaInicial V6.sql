	Begin tran 
--commit
INSERT INTO Campo (Codigo, Nome, Status)
SELECT 935, 'MENSAGEM_EmpresaNaoPossuiChaveAutenticacao', 1
UNION SELECT 936, 'ABA_Usuarios', 1
UNION SELECT 937, 'ABA_Principal', 1

--Produtos
UNION SELECT 938, 'LBL_AgenciasReguladoraCodIdentificacao', 1
UNION SELECT 939, 'GRID_NumeroIdentificacaoAlternativa', 1
UNION SELECT 940, 'MENSAGEM_AgenciaReguladoraExiste', 1
GO

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'A empresa n�o possui chave de autentica��o gerada. Por favor, entre em contato com a GS1 Brasil: Tel.', 'A empresa n�o possui chave de autentica��o gerada. Por favor, entre em contato com a GS1 Brasil: Tel.', 1, 935, 1
UNION SELECT 'Usu�rios', 'Usu�rios', 1, 936, 1
UNION SELECT 'Principal', 'Principal', 1, 937, 1

--Produtos
UNION SELECT 'Ag�ncias Reguladora / C�digo Identifica��o Interno', 'Ag�ncias Reguladora / C�digo Identifica��o Interno', 1, 938, 1
UNION SELECT 'C�d. Id. Alternativa', 'C�d. Id. Alternativa', 1, 939, 1
UNION SELECT 'J� existe uma ag�ncia reguladora com os dados informados.', 'J� existe uma ag�ncia reguladora com os dados informados.', 1, 940, 1
GO

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 12, 935

--Dados do Associado
UNION SELECT 11, 26
UNION SELECT 11, 936
UNION SELECT 11, 937

--Produtos
UNION SELECT 15, 685
UNION SELECT 15, 938
UNION SELECT 15, 939
UNION SELECT 15, 940
GO

UPDATE CampoIdioma set Texto = 'ou atrav�s do nosso chat no portal www.gs1br.org e solicite a gera��o das chaves de autentica��o.', Comentario = 'ou atrav�s do nosso chat no portal www.gs1br.org e solicite a gera��o das chaves de autentica��o.' WHERE CodigoCampo = 428 AND CodigoIdioma = 1
GO

--Drop Index da tabela de Produto
DROP INDEX [IX1_ProdutoB568A] ON [dbo].[Produto]
DROP INDEX [IX1_ProdutoF3B16] ON [dbo].[Produto]
DROP INDEX [IX1_Produto0CE9F] ON [dbo].[Produto]
DROP INDEX [IX1_Produto731B6] ON [dbo].[Produto]
GO

-- ProdutoAgenciaReguladora
CREATE TABLE ProdutoAgenciaReguladora
(
	Codigo bigint IDENTITY(1,1) NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoAgencia bigint NOT NULL,
	alternateItemIdentificationId VARCHAR(50) NOT NULL,

	CONSTRAINT [PK_ProdutoAgenciaReguladora] PRIMARY KEY CLUSTERED (Codigo ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
);

ALTER TABLE ProdutoAgenciaReguladora  WITH CHECK ADD  CONSTRAINT FK_ProdutoAgenciaReguladora_Agencia FOREIGN KEY(CodigoAgencia)
REFERENCES AgenciaReguladora (Codigo)
GO

ALTER TABLE ProdutoAgenciaReguladora  WITH CHECK ADD  CONSTRAINT FK_ProdutoAgenciaReguladora_Produto FOREIGN KEY(CodigoProduto)
REFERENCES Produto (CodigoProduto)
GO

UPDATE PRODUTO SET ALTERNATEITEMIDENTIFICATIONAGENCY = NULL WHERE ALTERNATEITEMIDENTIFICATIONAGENCY = 0
GO

--Script para migrar agencias cadastradas na tebela produto para tabela produto agencia reguladora
INSERT INTO ProdutoAgenciaReguladora (CodigoProduto, CodigoAgencia, alternateItemIdentificationId)
SELECT CodigoProduto, alternateitemidentificationagency, alternateItemIdentificationId
FROM Produto 
WHERE alternateItemIdentificationId IS NOT NULL
AND alternateitemidentificationagency IS NOT NULL
GO

--Tabela Produto Ag�ncia Reguladora historico
CREATE TABLE ProdutoAgenciaReguladoraHistorico(
	Codigo bigint NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoAgencia bigint NOT NULL,
	AlternateItemIdentificationId varchar(50) NOT NULL,
	DataHistorico datetime NOT NULL
 CONSTRAINT PK_ProdutoAgenciaReguladoraHistorico PRIMARY KEY CLUSTERED 
(
	DataHistorico ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE ProdutoAgenciaReguladoraHistorico  WITH CHECK ADD  CONSTRAINT FK_ProdutoAgenciaReguladoraHistorico_Produto FOREIGN KEY(CodigoProduto)
REFERENCES dbo.Produto (CodigoProduto)
GO

ALTER TABLE ProdutoAgenciaReguladoraHistorico CHECK CONSTRAINT FK_ProdutoAgenciaReguladoraHistorico_Produto
GO

ALTER TABLE ProdutoAgenciaReguladoraHistorico  WITH CHECK ADD  CONSTRAINT FK_ProdutoAgenciaReguladoraHistorico_Agencia FOREIGN KEY(CodigoAgencia)
REFERENCES AgenciaReguladora (Codigo)
GO

ALTER TABLE ProdutoAgenciaReguladoraHistorico CHECK CONSTRAINT FK_ProdutoAgenciaReguladoraHistorico_Agencia
GO

ALTER TABLE produto DROP COLUMN alternateItemIdentificationId;
ALTER TABLE produto DROP COLUMN alternateitemidentificationagency;
GO

--ALTER TABLE produtohistorico DROP COLUMN alternateitemidentificationid;
--ALTER TABLE produtohistorico DROP COLUMN alternateitemidentificationagency;
--GO

UPDATE FORMULARIO SET CODIGOMODULO = 2 WHERE NOME = 'Gest�o de Lotes'
GO

ALTER TABLE TIPOPRODUTO ADD CampoObrigatorio int
GO


INSERT INTO PERFILFUNCIONALIDADE VALUES (1,102,GETDATE(),1)
GO

UPDATE FORMULARIO SET ICONE = 'icon-archive', ORDEM = 12 WHERE CODIGO = 47
GO

INSERT INTO FUNCIONALIDADE VALUES (170,'PesquisarLotes','PesquisarLotes',1,1,47)
INSERT INTO FUNCIONALIDADE VALUES (171,'CadastrarLotes','CadastrarLotes',1,1,47)
INSERT INTO FUNCIONALIDADE VALUES (172,'EditarLotes','EditarLotes',1,1,47)
INSERT INTO FUNCIONALIDADE VALUES (173,'PublicarLotes','PublicarLotes',1,1,47)
INSERT INTO FUNCIONALIDADE VALUES (174,'VisualizarLotes','VisualizarLotes',1,1,47)
GO

INSERT INTO PERFILFUNCIONALIDADE VALUES (1,170,GETDATE(),1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1,171,GETDATE(),1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1,172,GETDATE(),1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1,173,GETDATE(),1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1,174,GETDATE(),1)
GO

ALTER TABLE TIPOPRODUTOHISTORICO
ADD CampoObrigatorio int null
go

INSERT INTO FUNCIONALIDADE VALUES (175,'AcessarRelatorioHistoricoUsoAssociadoGS1','AcessarRelatorioHistoricoUsoAssociadoGS1',1,1,54)

INSERT INTO PERFILFUNCIONALIDADE VALUES (1,175,GETDATE(),1)
go

INSERT INTO FORMULARIO VALUES (54, 'Hist�rico de Uso', 'Hist�rico de Uso', 1, 4, '/backoffice/relatorioHistoricoUsoAssociadoGS1', null, null, 0)

--Mensagens
UPDATE CAMPOIDIOMA SET TEXTO = 'Esta Ag�ncia Reguladora j� foi informada.', COMENTARIO = 'Esta Ag�ncia Reguladora j� foi informada.'
WHERE CODIGOCAMPO = (SELECT CODIGO FROM CAMPO WHERE NOME = 'MENSAGEM_AgenciaReguladoraExiste') AND CAMPOIDIOMA.CODIGOIDIOMA = 1
GO

UPDATE CAMPOIDIOMA SET TEXTO = 'N�mero de Ident. Alternativa / C�d. Interno', COMENTARIO = 'N�mero de Ident. Alternativa / C�d. Interno'
WHERE CODIGOCAMPO = (SELECT CODIGO FROM CAMPO WHERE NOME = 'LBL_NumeroIdentificacaoAlternativa') AND CAMPOIDIOMA.CODIGOIDIOMA = 1
GO

UPDATE CAMPOIDIOMA SET TEXTO = 'Ag�ncia Controla Ident. Alternativa / C�d. Interno', COMENTARIO = 'Selecione uma ag�ncia reguladora ou selecione "C�digo de Identifica��o Interno", quando aplic�vel.'
WHERE CODIGOCAMPO = (SELECT CODIGO FROM CAMPO WHERE NOME = 'LBL_CodAgenciaControla') AND CAMPOIDIOMA.CODIGOIDIOMA = 1
GO

UPDATE CAMPOIDIOMA SET TEXTO = 'C�d. Id. Alternativa / C�d. Interno', COMENTARIO = 'C�d. Id. Alternativa / C�d. Interno'
WHERE CODIGOCAMPO = (SELECT CODIGO FROM CAMPO WHERE NOME = 'GRID_NumeroIdentificacaoAlternativa') AND CAMPOIDIOMA.CODIGOIDIOMA = 1
GO

UPDATE CAMPOIDIOMA SET TEXTO = 'Ag�ncia Reguladora / C�d. Interno', COMENTARIO = 'Ag�ncia Reguladora / C�d. Interno'
WHERE CODIGOCAMPO = (SELECT CODIGO FROM CAMPO WHERE NOME = 'GRID_AgenciaRegistro') AND CAMPOIDIOMA.CODIGOIDIOMA = 1
GO

--Tipo de Produto
INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 941, 'LBL_CampoObrigatorioTipoProduto', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Exigir obrigatoriedade dos campos de medidas no cadastro de produtos', 'Exigir obrigatoriedade dos campos de medidas no cadastro de produtos', 1, 941, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 7, 941

--Idiomas
INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 942, 'GRID_ComentarioTooltip', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Coment�rio/Tooltip', 'Coment�rio/Tooltip', 1, 942, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 49, 942

INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 943, 'LBL_ComentarioTooltip', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Coment�rio/Tooltip', 'Coment�rio/Tooltip', 1, 943, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 49, 943

--Hist�rico de Uso
INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 944, 'LBL_TitleRelatorioHistoricoUsoAssociadoGS1', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Relat�rio de Hist�rico de Uso', 'Relat�rio de Hist�rico de Uso', 1, 944, 1

INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 945, 'MENSAGEM_LoginNaoEncontrado', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Login n�o encontrado.', 'Login n�o encontrado.', 1, 945, 1

INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 946, 'MENSAGEM_RazaoSocialNaoEncontrado', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Raz�o Social n�o encontrada.', 'Raz�o Social n�o encontrada.', 1, 946, 1

INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 947, 'PLACEHOLDER_DigiteRazaoSocial', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Digite a raz�o social', 'Digite a raz�o social', 1, 947, 1

INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 948, 'PLACEHOLDER_DigiteLogin', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Digite o login', 'Digite o login', 1, 948, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 54, 90
UNION SELECT 54, 93
UNION SELECT 54, 108
UNION SELECT 54, 112
UNION SELECT 54, 345
UNION SELECT 54, 363
UNION SELECT 54, 404
UNION SELECT 54, 543
UNION SELECT 54, 544
UNION SELECT 54, 546
UNION SELECT 54, 638
UNION SELECT 54, 659
UNION SELECT 54, 660
UNION SELECT 54, 673
UNION SELECT 54, 771
UNION SELECT 54, 724
UNION SELECT 54, 725
UNION SELECT 54, 806
UNION SELECT 54, 932
UNION SELECT 54, 933
UNION SELECT 54, 934
UNION SELECT 54, 944
UNION SELECT 54, 945
UNION SELECT 54, 946
UNION SELECT 54, 947
UNION SELECT 54, 948
GO

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 53, 93
UNION SELECT 53, 112
UNION SELECT 53, 404
UNION SELECT 53, 533
UNION SELECT 53, 534
UNION SELECT 53, 536
UNION SELECT 53, 771
UNION SELECT 53, 806
UNION SELECT 53, 944
UNION SELECT 53, 945
UNION SELECT 53, 946
UNION SELECT 53, 947
UNION SELECT 53, 948
GO

UPDATE FUNCIONALIDADERELATORIOLOG SET CODIGOFUNCIONALIDADERELATORIO = 5 WHERE NOME = 'Logar no Sistema'
GO

INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 169, GETDATE(), 1)
GO



INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 550


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 949, 'BTN_SelecionarLote', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Selecionar Lote', 'Selecione um lote do Produto', 1, 949, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 949


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 950, 'BTN_SelecionarLote', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'N�mero do Lote', 'N�mero do Lote do Produto', 1, 950, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 950


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 951, 'GRID_ModeloCodigoBarras', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Modelo', 'Modelo do tipo de c�digo de baras', 1, 951, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 951



INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 952, 'BTN_Alterarlote', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Alterar Lote', 'Selecione um outro lote do Produto', 1, 952, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 952


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 953, 'TOOLTIP_PesquisarLote', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Pesquisar Lote', 'Pesquisar Lote', 1, 953, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 953



INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 954, 'GRID_CodigoHomologacao', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'C�digo Homologa��o', 'C�digo homologa��o na ag�ncia reguladora ANATEL', 1, 954, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 954


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 955, 'GRID_RegistroAnvisa', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Registro ANVISA', 'C�digo registro na ag�ncia reguladora ANVISA', 1, 955, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 955


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 956, 'MENSAGEM_DataMatrix', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Selecione um lote e o c�digo de registro.', 'Selecione um lote e o c�digo de registro.', 1, 956, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 956

INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 957, 'MENSAGEM_SerialZero', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Serial  pode ser zero.', 'Serial  pode ser zero.', 1, 957, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 957


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 958, 'MENSAGEM_InformeAnatel', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Selecione um c�digo de homologa��o Anatel.', 'Selecione um c�digo de homologa��o Anatel.', 1, 958, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 958


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 959, 'MENSAGEM_Modelo128', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Selecione um modelo.', 'Selecione um modelo.', 1, 959, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 17, 959


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 960, 'LBL_NenhumRegistroEncontradoAgencia', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Nenhum N�mero de Identifica��o Alternativa inserido', 'Nenhum N�mero de Identifica��o Alternativa inserido', 1, 960, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 15, 960


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 961, 'LBL_NenhumRegistroEncontradoURL', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Nenhuma URL inserida', 'Nenhuma URL inserida', 1, 961, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 15, 961


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 962, 'LBL_AgenciaReguladoraCodigoInterno', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Ag�ncia Reguladora / C�digo Interno', 'Ag�ncia Reguladora / C�digo Interno', 1, 962, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 15, 962

-- Inclus�o da coluna C�digo do Associado na tabela de log da a��o
ALTER TABLE [LOG] ADD CodigoAssociado BIGINT NULL
GO
ALTER TABLE [LOG] ADD CONSTRAINT FK_Log_Associado FOREIGN KEY (CodigoAssociado) REFERENCES dbo.Associado (Codigo)
GO

UPDATE ESPACOBANNER SET NOME = 'Home - Banner principal', DESCRICAO = 'Banner principal na tela de Dashboard', ALTURA = 125 WHERE CODIGO = 1
--UPDATE ESPACOBANNER SET STATUS = 0 WHERE CODIGO = 2
DELETE FROM BANNERESPACOBANNER WHERE CODIGOESPACOBANNER = 2;
DELETE FROM ESPACOBANNER WHERE CODIGO = 2
GO

UPDATE FORMULARIO SET ORDEM = 7 WHERE NOME = 'Gest�o de Lotes'
UPDATE FORMULARIO SET ORDEM = 8 WHERE NOME = 'Gera��o de Etiquetas'
UPDATE FORMULARIO SET ORDEM = 9 WHERE NOME = 'Gera��o EPC/RFID'
UPDATE FORMULARIO SET ORDEM = 10 WHERE NOME = 'Importa��o de Produtos'
UPDATE FORMULARIO SET ORDEM = 11 WHERE NOME = 'Cartas de Filia��o'
GO

INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 963, 'LBL_DataMigracao', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Data de Migra��o', 'Data de Migra��o', 1, 963, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 11, 963

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 50, 360

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 47, 208

UPDATE FORMULARIO SET URL = '/backoffice/cartascertificadasgs1' WHERE CODIGO = 48 AND NOME = 'Cartas de Filia��o GS1'

INSERT INTO FORMULARIO VALUES(53, 'Hist�rico de Uso - Associado', 'Hist�rico de Uso - Associado',	1,	3,	'/backoffice/relatorioHistoricoUsoAssociado',	NULL,	12,	0)
INSERT INTO FUNCIONALIDADE VALUES (169,	'AcessarRelatorioHistoricoUsoAssociado', 'Gerar Relat�rio de Hist�rico de Uso - Associado', 1, 2, 53)
GO

UPDATE BIZSTEP SET STATUS = 0 WHERE CODIGO <> 5
GO

UPDATE CAMPOIDIOMA SET TEXTO = 'Informa��es do Lote', COMENTARIO = 'Informa��es do Lote'
WHERE CODIGOCAMPO = (SELECT CODIGO FROM CAMPO WHERE NOME = 'LBL_TitleAlterarLotes') AND CAMPOIDIOMA.CODIGOIDIOMA = 1
GO


INSERT INTO CAMPO (Codigo, Nome, Status)
SELECT 976, 'MENSAGEM_DataProcessamentoMaiorVencimento', 1

INSERT INTO CAMPOIDIOMA (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'A data de processamento n�o pode ser maior que a data de vencimento.', 'A data de processamento n�o pode ser maior que a data de vencimento.', 1, 976, 1

INSERT INTO FORMULARIOCAMPO (CodigoFormulario, CodigoCampo)
SELECT 47, 976
GO

UPDATE AGENCIAREGULADORA SET NOME =	'C�DIGO INTERNO' WHERE NOME = 'C�digo Interno'
GO


INSERT INTO FORMULARIOCAMPO VALUES (47,303)
INSERT INTO FORMULARIOCAMPO VALUES (47,308)

UPDATE BIZSTEP SET Nome = 'comissionamento', descricao = 'comissionamento' where codigo = 5
UPDATE DISPOSITIONS SET Nome = 'ativo', descricao = 'ativo' where codigo = 6
