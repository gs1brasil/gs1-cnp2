﻿------------------------------------------------------------------------------------------------------------------------------------------------------------	
--------------------------------------------------------------------- CARGAS ENTREGA 1 ---------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO [dbo].[StatusUsuario] ([Codigo],[Nome],[Descricao],[Status])
SELECT 1,'Ativo','Usuário Ativo',1
UNION SELECT 2,'Inativo','Usuário Inativo',1
UNION SELECT 3,'Bloqueado','Usuário com Bloqueio',1
UNION SELECT 4,'Bloqueio de Senha','Usuário com Bloqueio por Erro de Senha',1
UNION SELECT 5,'Recuperação de Senha','Usuário em Recuperação de Senha',1
GO


INSERT INTO [dbo].[StatusAssociado] ([Codigo],[Nome],[Descricao],[Status])
SELECT 1,'Ativo','Associado Ativo',1
UNION SELECT 2,'Inativo','Associado Inativo',1
UNION SELECT 3,'Cancelado','Associado Cancelado',1
UNION SELECT 4,'Sem acesso CNP 2.0','Sem acesso CNP 2.0',1
UNION SELECT 5,'Bloqueado','Usuário com Bloqueio',1
UNION SELECT 6,'Empresa não Associada','Empresa não Associada',1
GO


INSERT INTO [dbo].[TipoUsuario] ([Codigo],[Nome],[Descricao],[Status])
SELECT 1,'GS1','Usuário Tipo GS1',1B
UNION SELECT 2, 'Associado', 'Usuário Associado', 1
GO

INSERT INTO [dbo].[SituacaoFinanceira]
           ([Codigo]
           ,[Nome]
           ,[Descricao]
           ,[Status])
     VALUES
           (1
           ,'Adimplente'
           ,'Associado com Situração Adimplente'
           ,1)
GO


INSERT INTO [dbo].[SituacaoFinanceira]
           ([Codigo]
           ,[Nome]
           ,[Descricao]
           ,[Status])
     VALUES
           (2
           ,'Inadimplente'
           ,'Associado com Situração Adimplente'
           ,1)
GO


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Usuario_Perfil') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Usuario DROP CONSTRAINT FK_Usuario_Perfil


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_Usuario_Usuario') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE Usuario DROP CONSTRAINT FK_Usuario_Usuario



INSERT INTO [dbo].[Usuario]
           ([Nome]
           ,[CodigoStatusUsuario]
           ,[CodigoPerfil]
           ,[CodigoTipoUsuario]
           ,[Email]
           ,[TelefoneResidencial]
           ,[Senha]
           ,[CodigoUsuarioAlteracao]
           ,[DataAlteracao]
           ,[MensagemObservacao]
           ,[DataAlteracaoSenha]
           ,[QuantidadeTentativa]
           ,[DataUltimoAcesso]
           ,[DataCadastro]
           ,[TelefoneCelular]
           ,[TelefoneComercial]
           ,[Ramal]
           ,[Departamento]
           ,[Cargo]
           ,[CPFCNPJ])
     VALUES
           ('Administrador'
           ,1
           ,1
           ,1
           ,'cnp@gs1br.org'
           ,null
           ,'383E4FCF7C6757B4A12B320BBAF7AE0B79402529'
           ,1
           ,getdate()
           ,null
           ,getdate()
           ,0
           ,null
           ,getdate()
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null)
GO






SET IDENTITY_INSERT perfil OFF


INSERT INTO [dbo].[Perfil]
           ([Nome]
           ,[Descricao]
           ,[Status]
           ,[CodigoTipoUsuario]
           ,[DataAlteracao]
           ,[CodigoUsuarioAlteracao])
     VALUES
           ('GS1 Administrador'
           ,'Perfil do Administrador Geral do Sistema'
           ,1
           ,1
           ,getdate()
           ,1)
GO


INSERT INTO [dbo].[Perfil]
           ([Nome]
           ,[Descricao]
           ,[Status]
           ,[CodigoTipoUsuario]
           ,[DataAlteracao]
           ,[CodigoUsuarioAlteracao])
     VALUES
           ('Associado Administrador'
           ,'Perfil do Administrador do Associado'
           ,1
           ,2
           ,getdate()
           ,1)
GO


INSERT INTO [dbo].[Perfil]
           ([Nome]
           ,[Descricao]
           ,[Status]
           ,[CodigoTipoUsuario]
           ,[DataAlteracao]
           ,[CodigoUsuarioAlteracao])
     VALUES
           ('Associado Moderador'
           ,'Perfil do Moderador do Associado'
           ,1
           ,2
           ,getdate()
           ,1)
GO


INSERT INTO [dbo].[Perfil]
           ([Nome]
           ,[Descricao]
           ,[Status]
           ,[CodigoTipoUsuario]
           ,[DataAlteracao]
           ,[CodigoUsuarioAlteracao])
     VALUES
           ('GS1 Moderador'
           ,'Perfil do Moderador/Atendimento GS1'
           ,1
           ,1
           ,getdate()
           ,1)
GO


ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_Perfil 
	FOREIGN KEY (CodigoPerfil) REFERENCES Perfil (Codigo)
;

ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
;
GO


SET IDENTITY_INSERT perfil ON
GO


INSERT INTO [dbo].[Usuario]
           ([Nome]
           ,[CodigoStatusUsuario]
           ,[CodigoPerfil]
           ,[CodigoTipoUsuario]
           ,[Email]
           ,[TelefoneResidencial]
           ,[Senha]
           ,[CodigoUsuarioAlteracao]
           ,[DataAlteracao]
           ,[MensagemObservacao]
           ,[DataAlteracaoSenha]
           ,[QuantidadeTentativa]
           ,[DataUltimoAcesso]
           ,[DataCadastro]
           ,[TelefoneCelular]
           ,[TelefoneComercial]
           ,[Ramal]
           ,[Departamento]
           ,[Cargo]
           ,[CPFCNPJ])
     VALUES
           ('Associado'
           ,1
           ,2
           ,2
           ,'associado@gs1br.org'
           ,null
           ,'383E4FCF7C6757B4A12B320BBAF7AE0B79402529'
           ,1
           ,getdate()
           ,null
           ,getdate()
           ,0
           ,null
           ,getdate()
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null)
GO




INSERT INTO [dbo].[UsuarioHistorico]
           ([Codigo]
		   ,[Nome]
           ,[CodigoStatusUsuario]
           ,[CodigoPerfil]
           ,[CodigoTipoUsuario]
           ,[Email]
           ,[TelefoneResidencial]
           ,[Senha]
           ,[CodigoUsuarioAlteracao]
           ,[DataAlteracao]
           ,[MensagemObservacao]
           ,[DataAlteracaoSenha]
           ,[QuantidadeTentativa]
           ,[DataUltimoAcesso]
           ,[DataCadastro]
           ,[TelefoneCelular]
           ,[TelefoneComercial]
           ,[Ramal]
           ,[Departamento]
           ,[Cargo]
           ,[CPFCNPJ]
		   ,DataHistorico)
     VALUES
           (1
		   ,'Administrador'
           ,1
           ,1
           ,2
           ,'cnp@gs1br.org'
           ,null
           ,'383E4FCF7C6757B4A12B320BBAF7AE0B79402529'
           ,1
           ,getdate()
           ,null
           ,getdate()
           ,0
           ,null
           ,getdate()
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null
		   ,GETDATE())
GO



INSERT INTO [dbo].[UsuarioHistorico]
           ([Codigo]
		   ,[Nome]
           ,[CodigoStatusUsuario]
           ,[CodigoPerfil]
           ,[CodigoTipoUsuario]
           ,[Email]
           ,[TelefoneResidencial]
           ,[Senha]
           ,[CodigoUsuarioAlteracao]
           ,[DataAlteracao]
           ,[MensagemObservacao]
           ,[DataAlteracaoSenha]
           ,[QuantidadeTentativa]
           ,[DataUltimoAcesso]
           ,[DataCadastro]
           ,[TelefoneCelular]
           ,[TelefoneComercial]
           ,[Ramal]
           ,[Departamento]
           ,[Cargo]
           ,[CPFCNPJ]
		   ,DataHistorico)
     VALUES
           (2
		   ,'Associado'
           ,1
           ,1
           ,1
           ,'associado@gs1br.org'
           ,null
           ,'383E4FCF7C6757B4A12B320BBAF7AE0B79402529'
           ,1
           ,getdate()
           ,null
           ,getdate()
           ,0
           ,null
           ,getdate()
           ,null
           ,null
           ,null
           ,null
           ,null
           ,null
		   ,GETDATE())
GO


INSERT INTO Parametro VALUES ('autenticacao.senha.diferente.n.ultimas','5', 1,1,getdate());
INSERT INTO Parametro VALUES ('autenticacao.senha.nudias.avisoexpiracao','10', 1,1,getdate());
INSERT INTO Parametro VALUES ('bo.nu.telefone','(34)3232-3656', 1,1,getdate());
INSERT INTO Parametro VALUES ('autenticacao.nudias.bloqueio.inadimplente.premium','10', 1,1,getdate());
INSERT INTO Parametro VALUES ('autenticacao.nudias.bloqueio.inadimplente.standard','7', 1,1,getdate());
INSERT INTO Parametro VALUES ('autenticacao.senha.nutentativas','5', 1,1,getdate());
INSERT INTO PARAMETRO VALUES ('autenticacao.senha.nudias.validade', '90', 1, 1, getDate());
INSERT INTO PARAMETRO VALUES ('midia.nuurl.foto', '4', 1, 1, getDate());
INSERT INTO PARAMETRO VALUES ('midia.nuurl.youtube', '1', 1, 1, getDate());
INSERT INTO PARAMETRO VALUES ('midia.nuurl.linkeddata', '1', 1, 1, getDate());
INSERT INTO PARAMETRO VALUES ('midia.nuurl.reserva', '4', 1, 1, getDate());
INSERT INTO PARAMETRO VALUES ('midia.nuurl.produto', '1', 1, 1, getDate());
INSERT INTO PARAMETRO VALUES ('midia.numax.foto', '5', 1, 1, getDate());
INSERT INTO PARAMETRO VALUES ('grid.resultados.por.pagina', '10', 1, 1, getDate());
INSERT INTO PARAMETRO VALUES ('ajuda.urlchat', 'http://chat.gs1br.org/code/', 1, 1, getDate());
INSERT INTO PARAMETRO (Chave, Valor, Status, CodigoUsuarioAlteracao, DataAlteracao) VALUES ('produto.default.nucod.classificacao.comercio.ext', '6', 1, 1, GETDATE());
INSERT INTO Parametro VALUES ('autenticacao.senha.nutentativasprerecaptcha','5', 1,1,getdate());
GO


INSERT [Modulo] ([Codigo], [Nome], [Descricao], [Status], [Icone], [Ordem], [Url]) VALUES (1, N'Administação', N'Administação', 1, N'icon-cogs', 1, NULL)
INSERT [Modulo] ([Codigo], [Nome], [Descricao], [Status], [Icone], [Ordem], [Url]) VALUES (2, N'Associado', N'Associado', 1, N'icon-user', 2, NULL)
INSERT [Modulo] ([Codigo], [Nome], [Descricao], [Status], [Icone], [Ordem], [Url]) VALUES (3, N'Relatórios', N'Relatórios', 1, N'icon-file', 3, N'/backoffice/relatoriosAssociado')
INSERT [Modulo] ([Codigo], [Nome], [Descricao], [Status], [Icone], [Ordem], [Url]) VALUES (4, N'Relatórios GS1', N'Relatórios GS1', 1, N'icon-file', 4, N'/backoffice/relatoriosGS1')
INSERT [Modulo] ([Codigo], [Nome], [Descricao], [Status], [Icone], [Ordem], [Url]) VALUES (5, N'Acesso', N'Acesso', 1, NULL, 5, NULL)
GO

INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (01, N'Perfis de Acesso', N'Perfil', 1, 1, N'/backoffice/perfil', N'icon-user', 1, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (02, N'Parâmetros', N'Parâmetros', 1, 1, N'/backoffice/parametrosSistema', N'icon-user', 2, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (03, N'Agências Reguladoras', N'Agências', 1, 1, N'/backoffice/agenciasReguladoras', N'icon-user', 3, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (04, N'Papel GLN', N'Papel GLN', 1, 1, N'/backoffice/papelgln', N'icon-user', 4, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (05, N'Upload de Cartas', N'Upload de Cartas', 1, 1, N'/backoffice/uploadcartas', N'icon-user', 5, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (06, N'Meus Dados', N'Meus Dados', 1, 1, N'/backoffice/meusdados', N'icon-user', 6, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (07, N'Tipo de Produto', N'Tipo Produto', 1, 1, N'/backoffice/tipoproduto', N'icon-user', 7, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (08, N'Administração – GPC´s', N'GPC''s', 1, 1, N'/backoffice/importargpc', N'icon-cogs', 8, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (09, N'Termos de Adesão', N'Termos de Adesão', 1, 1, N'/backoffice/termosAdesao', N'icon-user', 9, N'1')

INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (10, N'Usuários', N'Usuários', 1, 2, N'/backoffice/usuario', N'icon-user', 1, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (11, N'Dados do Associado', N'Dados do Associado', 1, 2, N'/backoffice/dadosassociado', N'icon-user', 2, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (12, N'Cartas de Filiação', N'Cartas de Filiação', 1, 2, N'/backoffice/cartascertificadas', N'icon-user', 3, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (13, N'Gestão de Chaves', N'Gestão de Chaves', 1, 2, N'/backoffice/gerirChaves', N'icon-key', 4, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (14, N'Localizações Físicas', N'Localizações Físicas', 1, 2, N'/backoffice/localizacoesfisicas', N'icon-user', 5, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (15, N'Cadastro de Produtos', N'Cadastro de Produtos', 1, 2, N'/backoffice/produtos', N'icon-list-alt', 6, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (16, N'Geração EPC/RFID', N'Geração EPC/RFID', 1, 2, N'/backoffice/geracaoEpcRfid', N'icon-tags', 7, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (17, N'Geração de Etiquetas', N'Geração de Etiquetas', 1, 2, N'/backoffice/geracaoEtiquetas', N'icon-barcode', 8, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (18, N'Importação de Produtos', N'Importação de Produtos', 1, 2, N'/backoffice/importarProdutos', N'icon-stackexchange', 9, N'1')

INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (19, N'Relatórios', N'Relatórios', 1, 3, N'/backoffice/relatoriosAssociado', NULL, 1, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (20, N'Relatório de Certificado', N'Relatório de Certificado', 0, 3, N'/backoffice/relatorioCertificadoAssociado', NULL, 2, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (21, N'Relatório de Histórico Status do Produto', N'Relatório de Histórico Status do Produto', 1, 3, N'/backoffice/relatorioHistoricoStatusProdutoAssociado', NULL, 3, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (22, N'Relatório de Log de Monitoramento de Ação', N'Relatório de Log de Monitoramento de Ação', 1, 3, N'/backoffice/relatorioLogMonitoramento.aspx', NULL, 4, 0)
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (23, N'Relatório de Localização Física', N'Relatório de Localização Física', 1, 3, N'/backoffice/relatorioLocalizacaoFisicaAssociado', NULL, 5, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (24, N'Relatório de Login de Acesso', N'Relatório de Login de Acesso', 1, 3, N'/backoffice/relatorioAcessoAssociado', NULL, 6, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (25, N'Relatório de Produto', N'Relatório de Produto', 1, 3, N'/backoffice/relatorioProdutoAssociado', NULL, 7, N'0')

INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (26, N'Relatórios GS1', N'Relatórios GS1', 1, 4, N'/backoffice/relatoriosGS1', NULL, 1, N'1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (27, N'Relatório de Certificado', N'Relatório de Certificado', 0, 4, N'/backoffice/relatorioCertificadoGS1', NULL, 2, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (28, N'Relatório de Associado', N'Relatório de Associado', 1, 4, N'/backoffice/relatorioAssociadoGS1', NULL, 3, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (29, N'Relatório de Histórico Status do Produto', N'Relatório de Histórico Status do Produto', 1, 4, N'/backoffice/relatorioHistoricoStatusProdutoGS1', NULL, 4, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (30, N'Relatório de Log de Monitoramento de Ação', N'Relatório de Log de Monitoramento de Ação', 1, 4, N'/backoffice/relatorioLogMonitoramento.aspx', NULL, 5, 0)
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (31, N'Relatório de Log de Erros', N'Relatório de Log de Erros', 1, 4, N'/backoffice/relatorioLogErros.aspx', NULL, 6, 0)
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (32, N'Relatório de Impressão', N'Relatório de Impressão', 1, 4, N'/backoffice/relatorioImpressaoGS1', NULL, 7, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (33, N'Relatório de Localização Física', N'Relatório de Localização Física', 1, 4, N'/backoffice/relatorioLocalizacaoFisicaGS1', NULL, 8, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (34, N'Relatório de Login de Acesso', N'Relatório de Login de Acesso', 1, 4, N'/backoffice/relatorioAcessoGS1', NULL, 9, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (35, N'Relatório de Produto', N'Relatório de Produto', 1, 4, N'/backoffice/relatorioProdutoGS1', NULL, 10, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (43, N'Relatório Totalizador - CNP', N'Relatório Totalizador - CNP', 1, 4, N'/backoffice/relatorioMensalCNP', NULL, 11, N'0')

INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (36, N'Login', N'Login', 1, 5, N'/', NULL, NULL, '0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (37, N'Alteração de Senha', N'Alteração de Senha', 1, 5, N'/backoffice/troca_senha', NULL, NULL, N'0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (38, N'Dashboard', N'Dashboard', 1, 5, N'/backoffice/home', NULL, NULL, '0')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (39, N'Gestão de Ajuda', N'Gestão de Ajuda', 1, 1, N'/backoffice/ajudas', NULL, NULL, '1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (40, N'Cadastro de FAQ', N'Cadastro de FAQ', 1, 1, N'/backoffice/cadastroFAQ', NULL, NULL, '1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (41, N'FAQ', N'Perguntas Frequentes - FAQ', 1, 1, N'/backoffice/FAQ', NULL, NULL, '1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (42, N'Pesquisa de Satisfação', N'Pesquisa de Satisfação', 1, 1, N'/backoffice/pesquisaSatisfacao', NULL, NULL, '1')
INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (44, N'Ajuda Passo a Passo', N'Gestão de Ajuda Passo a Passo', 1, 1, N'/backoffice/cadastrarAjudaPassoaPasso', NULL, NULL, '1')
GO

--- Funcionalidades Tipo de Usuário GS1
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (01, N'PesquisarPerfil', N'Acessar Perfil', 1, 1, 1)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (02, N'CadastrarPerfil', N'Cadastrar Perfil', 1, 1, 1)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (03, N'EditarPerfil', N'Editar Perfil', 1, 1, 1)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (04, N'ExcluirPerfil', N'Excluir Perfil', 1, 1, 1)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (05, N'PesquisarParametro', N'Pesquisar Parâmentros', 1, 1, 2)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (06, N'EditarParametro', N'Editar Parâmetros', 1, 1, 2)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (07, N'PesquisarAgencia', N'Pesquisar Agência', 1, 1, 3)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (08, N'CadastrarAgencia', N'Cadastrar Agência', 1, 1, 3)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (09, N'EditarAgencia', N'Editar Agência', 1, 1, 3)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (10, N'ExcluirAgencia', N'Excluir Agência', 1, 1, 3)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (11, N'PesquisarPapelGLN', N'Pesquisar Papel GLN', 1, 1, 4)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (12, N'CadastrarPapelGLN', N'Cadastrar Papel GLN', 1, 1, 4)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (13, N'EditarPapelGLN', N'Editar Papel GLN', 1, 1, 4)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (14, N'ExcluirPapelGLN', N'Excluir Papel GLN', 1, 1, 4)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (15, N'UploadCartas', N'Upload de Templates de Cartas Certificadas', 1, 1, 5)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (16, N'PesquisarTipoProduto', N'Pesquisar Tipo Produto', 1, 1, 7)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (17, N'CadastrarTipoProduto', N'Cadastrar Tipo Produto', 1, 1, 7)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (18, N'EditarTipoProduto', N'Editar Tipo Produto', 1, 1, 7)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (19, N'ExcluirTipoProduto', N'Excluir Tipo Produto', 1, 1, 7)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (20, N'ListarGPC', N'Listar GPC', 1, 1, 8)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (21, N'PesquisarHIstoricoGPC', N'Pesquisar Histórico de GPC', 1, 1, 8)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (22, N'ImportarGPC', N'Importar GPC', 1, 1, 8)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (23, N'TermosAdesao', N'Termos de Adesão', 1, 1, 9)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (24, N'PesquisarUsuario', N'Pesquisar Usuários', 1, 1, 10)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (25, N'CadastrarUsuario', N'Cadastrar Usuários', 1, 1, 10)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (26, N'EditarUsuario', N'Editar Usuários', 1, 1, 10)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (27, N'DadosAssociado', N'Visualizar Dados do Associado', 1, 1, 11)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (28, N'EditarDadosAssociado', N'Editar Dados do Associado', 1, 1, 11)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (29, N'CartasFiliacao', N'Cartas de Filiação', 1, 1, 12)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (30, N'CadastrarChave', N'Gerar Chaves', 1, 1, 13)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (31, N'PesquisarLocalizacaoFisica', N'Pesquisar Localização Física', 1, 1, 14)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (32, N'CadastrarLocalizacaoFisica', N'Cadastrar Localização Física', 1, 1, 14)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (33, N'EditarLocalizacaoFisica', N'Editar Localização Física', 1, 1, 14)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (34, N'ExcluirLocalizacaoFisica', N'Excluir Localização Física', 1, 1, 14)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (35, N'PesquisarProduto', N'Pesquisar Produtos', 1, 1, 15)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (36, N'CadastrarProduto', N'Cadastrar Produtos', 1, 1, 15)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (37, N'EditarProduto', N'Editar Produtos', 1, 1, 15)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (38, N'PesquisarEPCRFID', N'Acessar EPC/RFID', 1, 1, 16)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (39, N'PesquisarEtiqueta', N'Pesquisar Etiquetas', 1, 1, 17)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (40, N'CadastrarEtiqueta', N'Cadastrar Etiquetas', 1, 1, 17)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (41, N'ImportarProduto', N'Importação de Produtos', 1, 1, 18)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (42, N'PesquisarRelatorioAssociado', N'Acessar Relatórios', 1, 1, 19)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (43, N'AcessarRelatorioCertificadoAssociado', N'Gerar Relatório de Certificado', 0, 1, 20)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (44, N'AcessarRelatorioHistoricoStatusProdutoAssociado', N'Gerar Relatório de Histórico Status do Produto', 1, 1, 21)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (45, N'RelatorioLogMonitoramentoAcao', N'Gerar Relatório de Log de Monitoramento de Ação', 1, 1, 22)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (46, N'AcessarRelatorioLocalizacaoFisicaAssociado', N'Gerar Relatório de Localização Física', 1, 1, 23)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (47, N'AcessarRelatorioAcessoAssociado', N'Gerar Relatório de Login de Acesso', 1, 1, 24)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (48, N'AcessarRelatorioProdutoAssociado', N'Gerar Relatório de Produto', 1, 1, 25)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (49, N'PesquisarRelatorioGS1', N'Acessar Relatórios GS1', 1, 1, 26)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (50, N'AcessarRelatorioCertificadoGS1', N'Gerar Relatório de Certificado', 0, 1, 27)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (51, N'AcessarRelatorioAssociadoGS1', N'Gerar Relatório de Associado', 1, 1, 28)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (52, N'AcessarRelatorioHistoricoStatusProdutoGS1', N'Gerar Relatório de Histórico Status do Produto', 1, 1, 29)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (53, N'RelatorioLogMonitoramentoAcao', N'Gerar Relatório de Log de Monitoramento de Ação', 1, 1, 30)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (54, N'RelatorioLogErro', N'Gerar Relatório de Log de Erros', 1, 1, 31)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (55, N'AcessarRelatorioImpressaoGS1', N'Gerar Relatório de Impressão', 1, 1, 32)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (56, N'AcessarRelatorioLocalizacaoFisicaGS1', N'Gerar Relatório de Localização Física', 1, 1, 33)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (57, N'AcessarRelatorioAcessoGS1', N'Gerar Relatório de Login de Acesso', 1, 1, 34)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (58, N'AcessarRelatorioProdutoGS1', N'Gerar Relatório de Produto', 1, 1, 35)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (92, N'CadastrarAjuda', N'Cadastro de Ajuda WYSIWYG', 1, 1, 39)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (93, N'CadastrarFAQ', N'Cadastrar FAQ', 1, 1, 40)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (94, N'EditarFAQ', N'Editar FAQ', 1, 1, 40)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (95, N'PesquisarFAQ', N'Pesquisar FAQ', 1, 1, 40)
GO

--- Funcionalidades Tipo de Usuário Associado
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (59, N'PesquisarUsuario', N'Pesquisar Usuários', 1, 2, 10)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (60, N'CadastrarUsuario', N'Cadastrar Usuários', 1, 2, 10)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (61, N'EditarUsuario', N'Editar Usuários', 1, 2, 10)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (62, 'DadosAssociado', 'Visualizar Dados do Associado', 1, 2, 11)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (63, 'EditarDadosAssociado', 'Editar Dados do Associado', 1, 2, 11)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (64, N'CartasFiliacao', N'Cartas de Filiação', 1, 2, 12)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (65, N'PesquisarLocalizacaoFisica', N'Pesquisar Localização Física', 1, 2, 14)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (66, N'CadastrarLocalizacaoFisica', N'Cadastrar Localização Física', 1, 2, 14)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (67, N'EditarLocalizacaoFisica', N'Editar Localização Física', 1, 2, 14)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (68, N'ExcluirLocalizacaoFisica', N'Excluir Localização Física', 1, 2, 14)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (69, N'PesquisarProduto', N'Pesquisar Produtos', 1, 2, 15)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (70, N'CadastrarProduto', N'Cadastrar Produtos', 1, 2, 15)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (71, N'EditarProduto', N'Editar Produtos', 1, 2, 15)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (72, N'PesquisarEPCRFID', N'Acessar EPC/RFID', 1, 2, 16)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (73, N'PesquisarEtiqueta', N'Pesquisar Etiquetas', 1, 2, 17)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (74, N'CadastrarEtiqueta', N'Cadastrar Etiquetas', 1, 2, 17)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (75, N'ImportarProduto', N'Importação de Produtos', 1, 2, 18)

INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (76, N'PesquisarRelatorioAssociado', N'Acessar Relatórios', 1, 2, 19)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (77, N'AcessarRelatorioCertificadoAssociado', N'Gerar Relatório de Certificado', 0, 2, 20)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (78, N'AcessarRelatorioHistoricoStatusProdutoAssociado', N'Gerar Relatório de Histórico Status do Produto', 1, 2, 21)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (79, N'RelatorioLogMonitoramentoAcao', N'Gerar Relatório de Log de Monitoramento de Ação', 1, 2, 22)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (80, N'AcessarRelatorioLocalizacaoFisicaAssociado', N'Gerar Relatório de Localização Física', 1, 2, 23)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (81, N'AcessarRelatorioAcessoAssociado', N'Gerar Relatório de Login de Acesso', 1, 2, 24)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (82, N'AcessarRelatorioProdutoAssociado', N'Gerar Relatório de Produto', 1, 2, 25)

GO


-- Grant de todas funcionalidades para o perfil 1 - GS1 Administrador
INSERT INTO [dbo].[PerfilFuncionalidade] ([CodigoPerfil], [CodigoFuncionalidade], [DataAlteracao], [CodigoUsuarioAlteracao])
SELECT 1, F.Codigo, GETDATE(), 1
FROM Funcionalidade F
WHERE F.CodigoTipoUsuario = 1
	AND F.CodigoFormulario <> 14
GO

-- Grant de todas funcionalidades para o perfil 1 - GS1 Administrador
INSERT INTO [dbo].[PerfilFuncionalidade] ([CodigoPerfil], [CodigoFuncionalidade], [DataAlteracao], [CodigoUsuarioAlteracao])
SELECT 2, F.Codigo, GETDATE(), 1
FROM Funcionalidade F
WHERE F.CodigoTipoUsuario = 2
	AND F.CodigoFormulario <> 14
GO

INSERT INTO [dbo].[PerfilFuncionalidade]
SELECT 2,23,GETDATE(),1
UNION SELECT 2,24,GETDATE(),1
UNION SELECT 2,25,GETDATE(),1
UNION SELECT 2,19,GETDATE(),1
UNION SELECT 2,34,GETDATE(),1
UNION SELECT 2,35,GETDATE(),1
UNION SELECT 2,36,GETDATE(),1
UNION SELECT 2,37,GETDATE(),1
UNION SELECT 2,38,GETDATE(),1
UNION SELECT 2,39,GETDATE(),1
UNION SELECT 2,40,GETDATE(),1
UNION SELECT 2,41,GETDATE(),1
UNION SELECT 2,42,GETDATE(),1
UNION SELECT 2,43,GETDATE(),1
UNION SELECT 2,44,GETDATE(),1
UNION SELECT 2,45,GETDATE(),1
UNION SELECT 2,46,GETDATE(),1
UNION SELECT 2,50, GETDATE(),1
UNION SELECT 2,51, GETDATE(),1
GO


INSERT INTO [dbo].[Licenca] ([Codigo],[Nome],[Descricao],[Status],[DataAlteracao])   
SELECT 1,'GTIN-13','GTIN-13 ADC, GTIN-13 ADC4, GTIN-13 CPL, GTIN-13 TRF, GTIN-13 TRF-A, GTIN-13-A',1, GETDATE()
UNION SELECT 2,'GTIN-8','GTIN-8',1, GETDATE()
UNION SELECT 3,'UCC','UCC, UCC ASSOC',1, GETDATE()
UNION SELECT 4,'GLN','GLN, GLN ADCIONAL, GLN ADCIONAL-A, GLN-A',1, GETDATE()
UNION SELECT 5,'CONF. PM','CONF. PM ADC10, CONF. PM ADC20, CONF. PM ADC30',1, GETDATE()
UNION SELECT 6,'CERTIF. CB','CERTIF.CB ADC 1, CERTIF.CB ADC10, CERTIF.CB ADC20, CERTIF.CB ADC30, CERTIF. CB T10',1, GETDATE()
UNION SELECT 7,'EPC','EPC, EPC-A',1, GETDATE()
GO


INSERT INTO TIPOASSOCIADO (CODIGO, NOME, DESCRICAO, STATUS)
SELECT 1, 'Premium','Premium', 1
UNION SELECT 2, 'Standard','Standard',1
GO


INSERT INTO [dbo].[TipoCompartilhamento]([Codigo],[Nome],[Descricao],[Status])
SELECT 1, 'Nunca','Nunca', 1
UNION SELECT 2, 'Sempre','Sempre',1
UNION SELECT 3, 'Personalizado','Personalizado',1
GO

--SET IDENTITY_INSERT [Associado] ON

--INSERT INTO [dbo].[Associado]
--           ([Codigo]
--		   ,[CAD]
--           ,[Nome]
--           ,[Descricao]
--           ,[Email]
--           ,[CPFCNPJ]
--           ,[InscricaoEstadual]
--           ,[CodigoTipoAssociado]           
--           ,[MensagemObservacao]
--           ,[DataAlteracao]
--           ,[DataAtualizacaoCRM]
--           ,[DataStatusInadimplente]
--           ,[CodigoUsuarioAlteracao]
--           ,[Endereco]
--           ,[Numero]
--           ,[Complemento]
--           ,[Bairro]
--           ,[Cidade]
--           ,[UF]
--           ,[CEP]
--           ,[CodigoTipoCompartilhamento]
--           ,[CodigoStatusAssociado]
--		   ,[CodigoSituacaoFinanceira])
--     VALUES
--           (1
--		   ,1
--           ,'GS1 BRASIL ASSOCIACAO BRASILEIRA DE AUTOMACAO'
--           ,'GS1 BRASIL ASSOCIACAO BRASILEIRA DE AUTOMACAO'
--           ,'TESTE@TESTE.COM.BR'
--           ,'53197141000102'
--           ,NULL
--           ,1           
--           ,'Associado cadastrado via carga inicial'
--           ,GETDATE()
--           ,NULL
--           ,NULL
--           ,1
--           ,'​​Rua Henrique Monteiro'
--           ,'79'
--           ,NULL
--           ,'Pinheiros'
--           ,'São Paulo'
--           ,'SP'
--           ,'05423020'
--           ,2
--           ,1
--		   ,1)
--GO

--INSERT INTO [dbo].[Associado]
--           ([Codigo]
--		   ,[CAD]
--           ,[Nome]
--           ,[Descricao]
--           ,[Email]
--           ,[CPFCNPJ]
--           ,[InscricaoEstadual]
--           ,[CodigoTipoAssociado]           
--           ,[MensagemObservacao]
--           ,[DataAlteracao]
--           ,[DataAtualizacaoCRM]
--           ,[DataStatusInadimplente]
--           ,[CodigoUsuarioAlteracao]
--           ,[Endereco]
--           ,[Numero]
--           ,[Complemento]
--           ,[Bairro]
--           ,[Cidade]
--           ,[UF]
--           ,[CEP]
--           ,[CodigoTipoCompartilhamento]
--           ,[CodigoStatusAssociado]
--		   ,[CodigoSituacaoFinanceira])
--     VALUES
--           (2
--		   ,2
--           ,'SOFTBOX SISTEMAS DE INFORMAÇÃO LTDA'
--           ,'GS1 BRASIL ASSOCIACAO BRASILEIRA DE AUTOMACAO'
--           ,'TESTE@SOFTBOX.COM.BR'
--           ,'07623088000147'
--           ,NULL
--           ,1           
--           ,'Associado cadastrado via carga inicial'
--           ,GETDATE()
--           ,NULL
--           ,NULL
--           ,1
--           ,'​​Rua Cesário Alvin'
--           ,'3345'
--           ,NULL
--           ,'Brasil'
--           ,'Uberlândia'
--           ,'MG'
--           ,'38400696'
--           ,2
--           ,1
--		   ,1)
--GO


--SET IDENTITY_INSERT [Associado] OFF

--INSERT INTO [dbo].[LicencaAssociado]([CodigoLicenca],[CodigoAssociado],[Status],[DataAniversario],[DataAlteracao])
--SELECT 1, 1, 1, (SELECT dateadd(year, +1, getdate())),GETDATE()     
--UNION SELECT 2, 1, 1, (SELECT dateadd(year, +1, getdate())),GETDATE()
--UNION SELECT 4, 1, 1, (SELECT dateadd(year, +1, getdate())),GETDATE()	 
--GO

--INSERT INTO [dbo].[LicencaAssociado]([CodigoLicenca],[CodigoAssociado],[Status],[DataAniversario],[DataAlteracao])
--SELECT 1, 2, 1, (SELECT dateadd(year, +1, getdate())),GETDATE()     
--UNION SELECT 4, 2, 1, (SELECT dateadd(year, +1, getdate())),GETDATE()	 
--GO





--INSERT INTO [dbo].[AssociadoUsuario]
--           ([CodigoUsuario]
--           ,[CodigoAssociado]
--           ,[DataCadastro]
--           ,[DataConfirmacao]
--           ,[CodigoUsuarioAlteracao]
--		   ,[IndicadorPrincipal])
--     VALUES
--           (2
--           ,1
--           ,GETDATE()
--           ,NULL
--           ,1
--		   ,1)
--GO


--INSERT INTO [dbo].[AssociadoUsuario]
--           ([CodigoUsuario]
--           ,[CodigoAssociado]
--           ,[DataCadastro]
--           ,[DataConfirmacao]
--           ,[CodigoUsuarioAlteracao]
--		   ,[IndicadorPrincipal])
--     VALUES
--           (2
--           ,2
--           ,GETDATE()
--           ,NULL
--           ,1
--		   ,0)
--GO



--INSERT INTO [dbo].[PrefixoLicencaAssociado] ([CodigoLicenca],[CodigoAssociado],[NumeroPrefixo],[Status],[DataAlteracao])
--SELECT 1,1,789835741,1,GETDATE()
--UNION SELECT 1,1,7898901000,1,GETDATE()
--UNION SELECT 4,1,7890007,1,GETDATE()
--UNION SELECT 4,1,7890037,1,GETDATE()
--UNION SELECT 4,1,7890076,1,GETDATE()
--GO

--INSERT INTO [dbo].[PrefixoLicencaAssociado] ([CodigoLicenca],[CodigoAssociado],[NumeroPrefixo],[Status],[DataAlteracao])
--SELECT 1,2,789802439,1,GETDATE()
--UNION SELECT 4,2,7890090,1,GETDATE()
--GO



INSERT INTO [dbo].[TipoContato]
           ([Codigo]
           ,[Nome]
           ,[Descricao]
           ,[Status])
     VALUES
           (1
           ,'Representante Legal'
           ,'Representante Legal'
           ,1)
GO




--INSERT INTO [dbo].[ContatoAssociado]
--           ([CodigoAssociado]
--           ,[Nome]
--           ,[CPFCNPJ]
--           ,[CodigoTipoContato]           
--           ,[Telefone1]           
--           ,[Telefone2]           
--           ,[Telefone3]
--           ,[Email]
--           ,[Email2]
--           ,[Status]
--           ,[Observacao]
--           ,[IndicadorPrincipal])
--     VALUES
--           (1
--           ,'Contato da GS1'
--           ,'99999999999'
--           ,1           
--           ,'+551130686229'
--           ,NULL
--           ,NULL                     
--           ,'gs1@gs1br.org'
--           ,NULL
--           ,1
--           ,NULL
--           ,'1')
--GO


--INSERT INTO [dbo].[ContatoAssociado]
--           ([CodigoAssociado]
--           ,[Nome]
--           ,[CPFCNPJ]
--           ,[CodigoTipoContato]           
--           ,[Telefone1]           
--           ,[Telefone2]           
--           ,[Telefone3]
--           ,[Email]
--           ,[Email2]
--           ,[Status]
--           ,[Observacao]
--           ,[IndicadorPrincipal])
--     VALUES
--           (2
--           ,'Contato da Softbox'
--           ,'88888888888'
--           ,1           
--           ,'+553432218838'
--           ,NULL
--           ,NULL
--           ,'soft@softbox.org'
--           ,NULL
--           ,1
--           ,NULL
--           ,'1')
--GO


INSERT INTO [dbo].[TIPOHASH] ([CODIGO], [NOME], [DESCRICAO], [STATUS], [QUANTIDADEMAXIMACARACTERES]) 
SELECT 1,'96 bits','Tipo Hash 96 bits',1,8
UNION SELECT 2,'198 bits','Tipo Hash 198 bits',1,16
GO


INSERT INTO [dbo].[TradeItemUnitDescriptorCodes] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) 
SELECT 1,'TL','Transport Load',1
UNION SELECT 2,'PL','Pallet',1
UNION SELECT 3,'CA','Case',1
UNION SELECT 4,'PK','Pack/Innerpack',1
UNION SELECT 5,'EA','Each',1
UNION SELECT 6,'MX','Mixed Module',1
UNION SELECT 7,'DS','Display/Shipper',1
UNION SELECT 8,'AP','Assorted Pack/Setpac',1
GO

INSERT INTO ImportClassificationTypeList(Codigo, CodeValue, Name)
SELECT 1,'CUSTOMS_TARIFF_NUMBER','CUSTOMS_TARIFF_NUMBER'
UNION SELECT 2,'HARMONIZED_COMMODITY_DESCRIPTION_AND_CODING_SYSTEM','HARMONIZED_COMMODITY_DESCRIPTION_AND_CODING_SYSTEM'
UNION SELECT 3,'HARMONIZED_TARIFF_SCHEDULE_OF_THE_US','HARMONIZED_TARIFF_SCHEDULE_OF_THE_US'
UNION SELECT 4,'INTRASTAT','INTRASTAT'
UNION SELECT 5,'INTRASTAT_COMBINED_NOMENCLATURE','INTRASTAT_COMBINED_NOMENCLATURE'
UNION SELECT 6,'MERCOSUR','MERCOSUR'
UNION SELECT 7,'NETHERLANDS','NETHERLANDS'
UNION SELECT 8,'TARIF_INTEGRE_DE_LA_COMMUNAUTE','TARIF_INTEGRE_DE_LA_COMMUNAUTE'
UNION SELECT 9,'TN_VED','TN_VED'
GO



INSERT INTO BarCodeTypeList
SELECT       1,'GS1_128','GS1 128','A subset of Code 128 that is utilised exclusively for GS1 System data structures.'
UNION SELECT 2,'EAN_13','EAN 13','A bar code of the EAN/UPC Symbology that encodes GTIN-13, Coupon-13, RCN-13, and VMN-13.'
UNION SELECT 3,'EAN_13_COMPOSITE','EAN 13 Composite','An EAN-13 symbol with an additional CC-A or CC-B Composite symbol included at the top of the EAN-13 symbol'
UNION SELECT 4,'EAN_13_WITH_FIVE_DIGIT_ADD_ON','EAN 13 With Five Digit Add On','An EAN-13 with an additional five-digit symbol, called an Add-On Symbol that can be included on the item just to the right of the main bar code.'
UNION SELECT 5,'EAN_13_WITH_TWO_DIGIT_ADD_ON','EAN 13 With Two Digit Add On','An EAN-13 with an additional two-digit symbol, called an Add-On Symbol that can be included on the item just to the right of the main bar code.'
UNION SELECT 6,'EAN_8','EAN 8','A bar code of the EAN/UPC Symbology that encodes GTIN-8 or RCN-8.'
UNION SELECT 7,'EAN_8_COMPOSITE','EAN 8 Composite','An EAN-8 symbol with an additional CC-A or CC-B symbol included at the top of the EAN-8 symbol'
UNION SELECT 8,'GS1_128_COMPOSITE','GS1 128 Composite','A GS1-128 symbol with an additional CC-A, CC-B or CC-C Composite symbol included at the top of the GS1-128 symbol'
UNION SELECT 9,'GS1_DATA_MATRIX','GS1 Data Matrix','GS1 implementation specification for use of Data Matrix'
UNION SELECT 10,'GS1_DATABAR_EXPANDED','GS1 Databar Expanded','A bar code with a variable width (from 4 to 22 symbol characters, or a minimum of 102X wide and a maximum of 534X wide) and is 34X high (where X is the width of a module).'
UNION SELECT 11,'GS1_DATABAR_EXPANDED_COMPOSITE','GS1 Databar Expanded Composite','A GS1 DataBar Expanded symbol with an additional CC-A or CC-B Composite symbol included at the top of theGS1 DataBar Expanded symbol'
UNION SELECT 12,'GS1_DATABAR_EXPANDED_STACKED','GS1 Databar Expanded Stacked','A multi-row stacked version of GS1 DataBar Expanded.'
UNION SELECT 13,'GS1_DATABAR_EXPANDED_STACKED_COMPOSITE','GS1 Databar Expanded Stacked Composite','A GS1 DataBar Expanded Stacked symbol with an additional CC-A or CC-B Composite symbol included at the top of the GS1 DataBar Expanded Stacked symbol'
UNION SELECT 14,'GS1_DATABAR_LIMITED','GS1 Databar Limited','A bar code designed for small items that will not need to be read by omnidirectional Point-of-Sale (POS) scanners. Its dimensions are 74X wide, starting with a 1X space and ending with a 1X bar, by 10X high (where X is the width of a module).'
UNION SELECT 15,'GS1_DATABAR_LIMITED_COMPOSITE','GS1 Databar Limited Composite','A GS1 DataBar Limited Symbol symbol with an additional CC-A or CC-B Composite symbol included at the top of theGS1 DataBar Limited symbol'
UNION SELECT 16,'GS1_DATABAR_OMNIDIRECTIONAL','GS1 Databar Omnidirectional','A full height, two-row version of the GS1 DataBar Omnidirectional Bar Code that is designed to be read by an omnidirectional scanner, such as a retail slot scanner.'
UNION SELECT 17,'GS1_DATABAR_OMNIDIRECTIONAL_COMPOSITE','GS1 Databar Omnidirectional Composite','A GS1 DataBar Omnidirectional symbol with an additional CC-A or CC-B Composite symbol included at the top of the GS1 DataBar Omnidirectional Stacked symbol'
UNION SELECT 18,'GS1_DATABAR_STACKED','GS1 Databar Stacked','The GS1 DataBar Stacked Bar Code is a reduced height two-row version of the GS1 DataBar Omnidirectional Bar Code that is designed for small items that will not need to be read by omnidirectional scanners.'
UNION SELECT 19,'GS1_DATABAR_STACKED_COMPOSITE','GS1 Databar Stacked Composite','A GS1 DataBar Stacked symbol with an additional CC-A or CC-B Composite symbol included at the top of the GS1 DataBar Stacked symbol'
UNION SELECT 20,'GS1_DATABAR_STACKED_OMNIDIRECTIONAL','GS1 Databar Stacked Omnidirectional','A full height, two-row version of the GS1 DataBar Omnidirectional Bar Code that is designed to be read by an omnidirectional scanner, such as a retail slot scanner.'
UNION SELECT 21,'GS1_DATABAR_STACKED_OMNIDIRECTIONAL_COMPOSITE','GS1 Databar Stacked Omnidirectional Composite','A GS1 DataBar Stacked Omnidirectional symbol with an additional CC-A or CC-B Composite symbol included at the top of the GS1 DataBar Stacked Omnidirectional symbol'
UNION SELECT 22,'GS1_DATABAR_TRUNCATED','GS1 Databar Truncated','A reduced height version of the GS1 DataBar Omnidirectional Bar Code that is designed for small items that will not need to be read by omnidirectional scanners.'
UNION SELECT 23,'GS1_DATABAR_TRUNCATED_COMPOSITE','GS1 Databar Truncated Composite','A GS1 DataBar Truncated symbol with an additional CC-A or CC-B Composite symbol included at the top of the GS1 DataBar Truncated symbol'
UNION SELECT 24,'ITF_14','ITF 14','ITF-14 (A subset of Interleaved 2-of-5) Bar Codes carry GTINs only on trade items that are not expected to pass through the Point-of-Sale.'
UNION SELECT 25,'NO_BARCODE','No Barcode','Item packaging does not contain bar code.'
UNION SELECT 26,'UPC_A','UPC A','A bar code of the EAN/UPC Symbology that encodes GTIN-12, Coupon-12, RCN-12, and VMN-12.'
UNION SELECT 27,'UPC_A_COMPOSITE','UPC A Composite','A UPC-A symbol with an additional CC-A or CC-B Composite symbol included at the top of the UPC-A symbol'
UNION SELECT 28,'UPC_A_WITH_FIVE_DIGIT_ADD_ON','UPC A With Five Digit Add On','A UPC-A with an additional five-digit symbol, called an Add-On Symbol that can be included on the item just to the right of the main bar code.'
UNION SELECT 29,'UPC_A_WITH_TWO_DIGIT_ADD_ON','UPC A With Two Digit Add On','A UPC-A Symbol with with an additional two-digit symbol, called an Add-On Symbol that can be included on the item just to the right of the main bar code.'
UNION SELECT 30,'UPC_E','UPC E ','A bar code of the EAN/UPC Symbology representing a GTIN-12 in six explicitly encoded digits using zero-suppression techniques.'
UNION SELECT 31,'UPC_E_COMPOSITE','UPC E Composite','A UPC-E symbol with an additional CC-A or CC-B Composite symbol included at the top of the UPC-E symbol'
UNION SELECT 32,'UPC_E_FIVE_DIGIT_ADD_ON','UPC E Five Digit Add On','A UPC-E with an additional five-digit symbol, called an Add-On Symbol that can be included on the item just to the right of the main bar code.'
UNION SELECT 33,'UPC_E_WITH_TWO_DIGIT_ADD_ON','UPC E Two Digit Add On','A UPC-E Symbol with with an additional two-digit symbol, called an Add-On Symbol that can be included on the item just to the right of the main bar code.'
GO


INSERT INTO [dbo].[StatusGTIN] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) VALUES (1, 'Ativo', 'GTIN Ativo',1)
INSERT INTO [dbo].[StatusGTIN] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) VALUES (2, 'Suspenso', 'GTIN Suspenso',1)
INSERT INTO [dbo].[StatusGTIN] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) VALUES (3, 'Reativado', 'GTIN Reativado',1)
INSERT INTO [dbo].[StatusGTIN] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) VALUES (4, 'Cancelado', 'GTIN Cancelado',1)
INSERT INTO [dbo].[StatusGTIN] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) VALUES (5, 'Em elaboração', 'GTIN em Elaboração',1)
INSERT INTO [dbo].[StatusGTIN] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) VALUES (6, 'Reutilizado', 'GTIN Reutilizado', 1)
INSERT INTO [dbo].[StatusGTIN] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) VALUES (7, 'Transferido', 'GTIN Transferido', 1)
GO


INSERT INTO [dbo].[TIPOFILTRO] ([CODIGO], [NOME], [DESCRICAO], [STATUS], [CODIGOUSUARIOALTERACAO]) 
SELECT 0,'Outros','Outros',1,1
UNION SELECT 1,'Item em Ponto de Venda','Item em Ponto de Venda',1,1
UNION SELECT 2,'Caixa','Caixa',1,1
UNION SELECT 3,'Reservado','Reservado',1,1
UNION SELECT 4,'Pacote Interno','Pacote Interno',1,1
UNION SELECT 5,'Reservado','Reservado',1,1
UNION SELECT 6,'Unidade de Carregamento','Unidade de Carregamento',1 ,1
UNION SELECT 7,'Componente','Componente',1,1
GO


INSERT INTO [dbo].[TipoURL] ([CODIGO], [NOME], [DESCRICAO], [STATUS]) 
SELECT 1,'Foto','Link Foto URL externa',1
UNION SELECT 2,'Reserva','Link Reserva',1
UNION SELECT 3,'Linkeddata','Link Linkeddata',1
UNION SELECT 4,'Youtube','Link do Youtube',1
UNION SELECT 5,'Produto','Link do Produto',1
GO



INSERT INTO UNECEREC20
SELECT 1,'barrel (UK petroleum)','J57','bbl (UK liq.)',CONVERT(DECIMAL(25,12),0.15911315),1,1,0
UNION SELECT 2,'barrel (US)','BLL','barrel (US)',CONVERT(DECIMAL(25,12),0.1589873),1,1,0
UNION SELECT 3,'bushel (UK)','BUI','bushel (UK)',CONVERT(DECIMAL(25,12),0.03636872),1,1,0
UNION SELECT 4,'bushel (US)','BUA','bu (US)',CONVERT(DECIMAL(25,12),0.03523907),1,1,0
UNION SELECT 5,'centilitre','CLT','cl',CONVERT(DECIMAL(25,12),0.00001),1,1,0
UNION SELECT 6,'cubic centimetre','CMQ','cm³',CONVERT(DECIMAL(25,12),0.000001),1,1,0
UNION SELECT 7,'cubic decametre','DMA','dam³',CONVERT(DECIMAL(25,12),1000),1,1,0
UNION SELECT 8,'cubic decimetre','DMQ','dm³',CONVERT(DECIMAL(25,12),0.001),1,1,0
UNION SELECT 9,'cubic foot','FTQ','ft³',CONVERT(DECIMAL(25,12),0.02831685),1,1,0
UNION SELECT 10,'cubic hectometre','H19','hm³',CONVERT(DECIMAL(25,12),1000000),1,1,0
UNION SELECT 11,'cubic inch','INQ','in³',CONVERT(DECIMAL(25,12),0.000016387064),1,1,0
UNION SELECT 12,'cubic kilometre','H20','km³',CONVERT(DECIMAL(25,12),1000000000),1,1,0
UNION SELECT 13,'cubic metre','K6','m³',CONVERT(DECIMAL(25,12),1),1,1,0
UNION SELECT 14,'cubic millimetre','MMQ','mm³',CONVERT(DECIMAL(25,12),0.000000001),1,1,0
UNION SELECT 15,'cubic yard','YDQ','yd³',CONVERT(DECIMAL(25,12),0.764555),1,1,0
UNION SELECT 16,'cup [unit of volume]','G21','cup (US)',CONVERT(DECIMAL(25,12),0.0002365882),1,1,0
UNION SELECT 17,'decalitre','A44','dal',CONVERT(DECIMAL(25,12),0.01),1,1,0
UNION SELECT 18,'decilitre','DLT','dl',CONVERT(DECIMAL(25,12),0.0001),1,1,0
UNION SELECT 19,'dry barrel (US)','BLD','bbl (US)',CONVERT(DECIMAL(25,12),0.115627),1,1,0
UNION SELECT 20,'dry gallon (US)','GLD','dry gal (US)',CONVERT(DECIMAL(25,12),0.004404884),1,1,0
UNION SELECT 21,'dry pint (US)','PTD','dry pt (US)',CONVERT(DECIMAL(25,12),0.0005506105),1,1,0
UNION SELECT 22,'dry quart (US)','QTD','dry qt (US)',CONVERT(DECIMAL(25,12),0.001101221),1,1,0
UNION SELECT 23,'fluid ounce (UK)','OZI','fl oz (UK)',CONVERT(DECIMAL(25,12),0.00002841306),1,1,0
UNION SELECT 24,'fluid ounce (US)','OZA','fl oz (US)',CONVERT(DECIMAL(25,12),0.00002957353),1,1,0
UNION SELECT 25,'gallon (UK)','GLI','gal (UK)',CONVERT(DECIMAL(25,12),0.004546092),1,1,0
UNION SELECT 26,'gallon (US)','GLL','gal (US)',CONVERT(DECIMAL(25,12),0.003785412),1,1,0
UNION SELECT 27,'hectolitre','HLT','hl',CONVERT(DECIMAL(25,12),0.1),1,1,0
UNION SELECT 28,'kilolitre','K6','kl',CONVERT(DECIMAL(25,12),1),1,1,0
UNION SELECT 29,'liquid pint (US)','PTL','liq pt (US)',CONVERT(DECIMAL(25,12),0.0004731765),1,1,0
UNION SELECT 30,'liquid quart (US)','QTL','liq qt (US)',CONVERT(DECIMAL(25,12),0.0009463529),1,1,0
UNION SELECT 31,'litre','LTR','l',CONVERT(DECIMAL(25,12),0.001),1,1,0
UNION SELECT 32,'megalitre','MLT','Ml',CONVERT(DECIMAL(25,12),100),1,1,0
UNION SELECT 33,'microlitre','4G','µl',CONVERT(DECIMAL(25,12),0.000000001),1,1,0
UNION SELECT 34,'millilitre','MAL','ml',CONVERT(DECIMAL(25,12),0.000001),1,1,1
UNION SELECT 35,'peck','G23','pk (US)',CONVERT(DECIMAL(25,12),0.008809768),1,1,0
UNION SELECT 36,'peck (UK)','L43','pk (UK)',CONVERT(DECIMAL(25,12),0.009092181),1,1,0
UNION SELECT 37,'pint (UK)','PTI','pt (UK)',CONVERT(DECIMAL(25,12),0.000568261),1,1,0
UNION SELECT 38,'pint (US dry)','L61','pt (US dry)',CONVERT(DECIMAL(25,12),0.0005506105),1,1,0
UNION SELECT 39,'pint (US)','PT','pt (US)',CONVERT(DECIMAL(25,12),0.000473176),1,1,0
UNION SELECT 40,'quart (UK)','QTI','qt (UK)',CONVERT(DECIMAL(25,12),0.0011365225),1,1,0
UNION SELECT 41,'quart (US dry)','L62','qt (US dry)',CONVERT(DECIMAL(25,12),0.001101221),1,1,0
UNION SELECT 42,'quart (US)','QT','qt (US)',CONVERT(DECIMAL(25,12),0.0009463529),1,1,0
UNION SELECT 43,'standard cubic foot','5I','std',CONVERT(DECIMAL(25,12),4672),1,1,0
UNION SELECT 44,'stere','G26','st',CONVERT(DECIMAL(25,12),1),1,1,0
UNION SELECT 45,'tablespoon (US)','G24','tablespoon (US)',CONVERT(DECIMAL(25,12),0.00001478676),1,1,0
UNION SELECT 46,'teaspoon (US)','G25','teaspoon (US)',CONVERT(DECIMAL(25,12),0.000004928922),1,1,0
UNION SELECT 47,'kilogram','KGM','kg',CONVERT(DECIMAL(25,12),1),2,1,1
UNION SELECT 48,'microgram','MC','µg',CONVERT(DECIMAL(25,12),0.000000001),2,1,0
UNION SELECT 49,'decagram','DJ','dag',CONVERT(DECIMAL(25,12),0.01),2,1,0
UNION SELECT 50,'decigram','DG','dg',CONVERT(DECIMAL(25,12),0.0001),2,1,0
UNION SELECT 51,'gram','GRM','g',CONVERT(DECIMAL(25,12),0.001),2,1,0
UNION SELECT 52,'centigram','CGM','cg',CONVERT(DECIMAL(25,12),0.00001),2,1,0
UNION SELECT 53,'tonne (metric ton)','TNE','t',CONVERT(DECIMAL(25,12),1000),2,1,0
UNION SELECT 54,'decitonne','DTN','dt or dtn',CONVERT(DECIMAL(25,12),100),2,1,0
UNION SELECT 55,'milligram','MGM','mg',CONVERT(DECIMAL(25,12),0.000001),2,1,0
UNION SELECT 56,'hectogram','HGM','hg',CONVERT(DECIMAL(25,12),0.1),2,1,0
UNION SELECT 57,'kilotonne','KTN','kt',CONVERT(DECIMAL(25,12),1000000),2,1,0
UNION SELECT 58,'megagram','2U','Mg',CONVERT(DECIMAL(25,12),1000),2,1,0
UNION SELECT 59,'pound','LBR','lb',CONVERT(DECIMAL(25,12),0.45359237),2,1,0
UNION SELECT 60,'grain','GRN','gr',CONVERT(DECIMAL(25,12),0.00006479891),2,1,0
UNION SELECT 61,'ounce (avoirdupois)','ONZ','oz',CONVERT(DECIMAL(25,12),0.02834952),2,1,0
UNION SELECT 62,'stone (UK)','STI','st',CONVERT(DECIMAL(25,12),6350293),2,1,0
UNION SELECT 63,'metre','MTR','m',CONVERT(DECIMAL(25,12),1),3,1,0
UNION SELECT 64,'decimetre','DMT','dm',CONVERT(DECIMAL(25,12),0.1),3,1,0
UNION SELECT 65,'centimetre','CMT','cm',CONVERT(DECIMAL(25,12),0.01),3,1,1
UNION SELECT 66,'micrometre (micron)','4H','µm',CONVERT(DECIMAL(25,12),0.000001),3,1,0
UNION SELECT 67,'millimetre','MMT','mm',CONVERT(DECIMAL(25,12),0.001),3,1,0
UNION SELECT 68,'hectometre','HMT','hm',CONVERT(DECIMAL(25,12),100),3,1,0
UNION SELECT 69,'kilometre','KTM','km',CONVERT(DECIMAL(25,12),1000),3,1,0
UNION SELECT 70,'nanometre','C45','nm',CONVERT(DECIMAL(25,12),0.000000001),3,1,0
UNION SELECT 71,'picometre','C52','pm',CONVERT(DECIMAL(25,12),0.000000000001),3,1,0
UNION SELECT 72,'femtometre','A71','fm',CONVERT(DECIMAL(25,12),0.000000000000001),3,1,0
UNION SELECT 73,'decametre','A45','dam',CONVERT(DECIMAL(25,12),10),3,1,0
UNION SELECT 74,'angstrom','A11','Å',CONVERT(DECIMAL(25,12),0.0000000001),3,1,0
UNION SELECT 75,'French gauge','H79','Fg',CONVERT(DECIMAL(25,12),0.000333333333),3,1,0
UNION SELECT 76,'fathom','AK','fth',CONVERT(DECIMAL(25,12),18288),3,1,0
UNION SELECT 77,'inch','INH','in',CONVERT(DECIMAL(25,12),0.0254),3,1,0
UNION SELECT 78,'micro-inch','M7','µin',CONVERT(DECIMAL(25,12),0.0000000254),3,1,0
UNION SELECT 79,'foot','FOT','ft',CONVERT(DECIMAL(25,12),0.3048),3,1,0
UNION SELECT 80,'yard','YRD','yd',CONVERT(DECIMAL(25,12),0.9144),3,1,0
UNION SELECT 81,'mile (statute mile)','SMI','mile',CONVERT(DECIMAL(25,12),1609344),3,1,0
UNION SELECT 82,'milli-inch','77','mil',CONVERT(DECIMAL(25,12),0.0000254),3,1,0
UNION SELECT 83,'megametre','MAM','Mm',CONVERT(DECIMAL(25,12),1000000),3,1,0
UNION SELECT 84,'american wire gauge','AWG','AWG',CONVERT(DECIMAL(25,12),0),3,1,0
UNION SELECT 85, 'degreeCelsius', 'CEL', 'ºC', '00.0', 4, 1, 0
UNION SELECT 86, 'degree Fahrenheit', 'FAH', 'ºF', '00.0', 4, 1, 0
UNION SELECT 87, 'Kelvin', 'KEL', 'K', '00.0', 4, 1, 1
GO

INSERT INTO AWG
SELECT		 1,'6/0',CONVERT(DECIMAL(25,12),0.01473),1
UNION SELECT 2,'5/0',CONVERT(DECIMAL(25,12),0.01312),1
UNION SELECT 3,'4/0',CONVERT(DECIMAL(25,12),0.0117),1
UNION SELECT 4,'3/0',CONVERT(DECIMAL(25,12),0.0104),1
UNION SELECT 5,'2/0',CONVERT(DECIMAL(25,12),0.00926),1
UNION SELECT 6,'1/0',CONVERT(DECIMAL(25,12),0.00825),1
UNION SELECT 7,'1',CONVERT(DECIMAL(25,12),0.00735),1
UNION SELECT 8,'2',CONVERT(DECIMAL(25,12),0.00654),1
UNION SELECT 9,'3',CONVERT(DECIMAL(25,12),0.00583),1
UNION SELECT 10,'4',CONVERT(DECIMAL(25,12),0.00519),1
UNION SELECT 11,'5',CONVERT(DECIMAL(25,12),0.00462),1
UNION SELECT 12,'6',CONVERT(DECIMAL(25,12),0.00411),1
UNION SELECT 13,'7',CONVERT(DECIMAL(25,12),0.00366),1
UNION SELECT 14,'8',CONVERT(DECIMAL(25,12),0.00326),1
UNION SELECT 15,'9',CONVERT(DECIMAL(25,12),0.00291),1
UNION SELECT 16,'10',CONVERT(DECIMAL(25,12),0.00259),1
UNION SELECT 17,'11',CONVERT(DECIMAL(25,12),0.0023),1
UNION SELECT 18,'12',CONVERT(DECIMAL(25,12),0.00205),1
UNION SELECT 19,'13',CONVERT(DECIMAL(25,12),0.00183),1
UNION SELECT 20,'14',CONVERT(DECIMAL(25,12),0.00163),1
UNION SELECT 21,'15',CONVERT(DECIMAL(25,12),0.00145),1
UNION SELECT 22,'16',CONVERT(DECIMAL(25,12),0.00129),1
UNION SELECT 23,'17',CONVERT(DECIMAL(25,12),0.00115),1
UNION SELECT 24,'18',CONVERT(DECIMAL(25,12),0.00102),1
UNION SELECT 25,'19',CONVERT(DECIMAL(25,12),0.000912),1
UNION SELECT 26,'20',CONVERT(DECIMAL(25,12),0.000812),1
UNION SELECT 27,'21',CONVERT(DECIMAL(25,12),0.000723),1
UNION SELECT 28,'22',CONVERT(DECIMAL(25,12),0.000644),1
UNION SELECT 29,'23',CONVERT(DECIMAL(25,12),0.000573),1
UNION SELECT 30,'24',CONVERT(DECIMAL(25,12),0.000511),1
UNION SELECT 31,'25',CONVERT(DECIMAL(25,12),0.000455),1
UNION SELECT 32,'26',CONVERT(DECIMAL(25,12),0.000405),1
UNION SELECT 33,'27',CONVERT(DECIMAL(25,12),0.000361),1
UNION SELECT 34,'28',CONVERT(DECIMAL(25,12),0.000321),1
UNION SELECT 35,'29',CONVERT(DECIMAL(25,12),0.000286),1
UNION SELECT 36,'30',CONVERT(DECIMAL(25,12),0.000255),1
UNION SELECT 37,'31',CONVERT(DECIMAL(25,12),0.000227),1
UNION SELECT 38,'32',CONVERT(DECIMAL(25,12),0.000202),1
UNION SELECT 39,'33',CONVERT(DECIMAL(25,12),0.00018),1
UNION SELECT 40,'34',CONVERT(DECIMAL(25,12),0.00016),1
UNION SELECT 41,'35',CONVERT(DECIMAL(25,12),0.000143),1
UNION SELECT 42,'36',CONVERT(DECIMAL(25,12),0.000127),1
UNION SELECT 43,'37',CONVERT(DECIMAL(25,12),0.000113),1
UNION SELECT 44,'38',CONVERT(DECIMAL(25,12),0.000101),1
UNION SELECT 45,'39',CONVERT(DECIMAL(25,12),0.0000897),1
UNION SELECT 46,'40',CONVERT(DECIMAL(25,12),0.0000799),1
GO


INSERT INTO ImportacaoResultado
SELECT 0, 'Pendente'
UNION SELECT 1, 'Importado com Sucesso'
UNION SELECT 2, 'Não Importado'

GO



INSERT INTO TipoGTIN
SELECT 1,'GTIN-8','GTIN-8',1
UNION SELECT 2,'GTIN-12','GTIN-12',1
UNION SELECT 3,'GTIN-13','GTIN-13',1
UNION SELECT 4,'GTIN-14','GTIN-14',1
GO


--INSERT INTO TipoGTINBarcodeType VALUES(1,1)
INSERT INTO TipoGTINBarcodeType VALUES(1,6)
--INSERT INTO TipoGTINBarcodeType VALUES(1,9)
--INSERT INTO TipoGTINBarcodeType VALUES(1,10)
--INSERT INTO TipoGTINBarcodeType VALUES(1,12)
--INSERT INTO TipoGTINBarcodeType VALUES(1,16)
--INSERT INTO TipoGTINBarcodeType VALUES(1,20)
--INSERT INTO TipoGTINBarcodeType VALUES(2,1)
INSERT INTO TipoGTINBarcodeType VALUES(2,26)
--INSERT INTO TipoGTINBarcodeType VALUES(2,9)
--INSERT INTO TipoGTINBarcodeType VALUES(2,10)
--INSERT INTO TipoGTINBarcodeType VALUES(2,12)
--INSERT INTO TipoGTINBarcodeType VALUES(2,16)
--INSERT INTO TipoGTINBarcodeType VALUES(2,20)
--INSERT INTO TipoGTINBarcodeType VALUES(3,1)
INSERT INTO TipoGTINBarcodeType VALUES(3,2)
--INSERT INTO TipoGTINBarcodeType VALUES(3,9)
--INSERT INTO TipoGTINBarcodeType VALUES(3,10)
--INSERT INTO TipoGTINBarcodeType VALUES(3,12)
--INSERT INTO TipoGTINBarcodeType VALUES(3,16)
--INSERT INTO TipoGTINBarcodeType VALUES(3,20)
--INSERT INTO TipoGTINBarcodeType VALUES(4,1)
INSERT INTO TipoGTINBarcodeType VALUES(4,24)
--INSERT INTO TipoGTINBarcodeType VALUES(4,9)
--INSERT INTO TipoGTINBarcodeType VALUES(4,10)
--INSERT INTO TipoGTINBarcodeType VALUES(4,12)
--INSERT INTO TipoGTINBarcodeType VALUES(4,16)
--INSERT INTO TipoGTINBarcodeType VALUES(4,20)
INSERT INTO TipoGTINBarcodeType VALUES(2,30)
GO


INSERT INTO [dbo].[TipoGTINLicenca] ([CodigoTipoGTIN],[CodigoLicenca])
SELECT 1,2
UNION SELECT 2, 3
UNION SELECT 3, 1
UNION SELECT 4, 1
GO


INSERT INTO [dbo].[TipoAceite]([Codigo],[Nome],[Descricao],[Status])
SELECT 1, 'Novos', 'Novos', 1
UNION SELECT 2, 'Todos', 'Todos', 1     
GO


INSERT INTO [dbo].[StatusTermoAdesao]([Codigo],[Nome],[Descricao],[Status])
SELECT 1, 'Em Elaboração', 'Em Elaboração', 1
UNION SELECT 2, 'Atual', 'Atual', 1
UNION SELECT 3, 'Histórico', 'Histórico', 1
GO


INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (1, N'Afrikaans', N'af', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (2, N'Albanian', N'sq', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (3, N'Arabic', N'ar', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (4, N'Armenian', N'hy', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (5, N'Azerbaijani', N'az', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (6, N'Basque', N'eu', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (7, N'Belarusian', N'be', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (8, N'Bengali', N'bn', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (9, N'Bosnian', N'bs', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (10, N'Bulgarian', N'bg', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (11, N'Catalan', N'ca', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (12, N'Cebuano', N'ceb', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (13, N'Chichewa', N'ny', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (14, N'Chinese Simplified', N'zh-CN', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (15, N'Chinese Traditional', N'zh-TW', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (16, N'Croatian', N'hr', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (17, N'Czech', N'cs', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (18, N'Danish', N'da', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (19, N'Dutch', N'nl', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (20, N'English', N'en', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (21, N'Esperanto', N'eo', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (22, N'Estonian', N'et', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (23, N'Filipino', N'tl', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (24, N'Finnish', N'fi', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (25, N'French', N'fr', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (26, N'Galician', N'gl', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (27, N'Georgian', N'ka', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (28, N'German', N'de', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (29, N'Greek', N'el', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (30, N'Gujarati', N'gu', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (31, N'Haitian Creole', N'ht', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (32, N'Hausa', N'ha', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (33, N'Hebrew', N'iw', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (34, N'Hindi', N'hi', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (35, N'Hmong', N'hmn', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (36, N'Hungarian', N'hu', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (37, N'Icelandic', N'is', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (38, N'Igbo', N'ig', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (39, N'Indonesian', N'id', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (40, N'Irish', N'ga', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (41, N'Italian', N'it', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (42, N'Japanese', N'ja', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (43, N'Javanese', N'jw', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (44, N'Kannada', N'kn', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (45, N'Kazakh', N'kk', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (46, N'Khmer', N'km', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (47, N'Korean', N'ko', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (48, N'Lao', N'lo', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (49, N'Latin', N'la', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (50, N'Latvian', N'lv', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (51, N'Lithuanian', N'lt', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (52, N'Macedonian', N'mk', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (53, N'Malagasy', N'mg', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (54, N'Malay', N'ms', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (55, N'Malayalam', N'ml', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (56, N'Maltese', N'mt', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (57, N'Maori', N'mi', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (58, N'Marathi', N'mr', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (59, N'Mongolian', N'mn', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (60, N'Myanmar (Burmese)', N'my', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (61, N'Nepali', N'ne', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (62, N'Norwegian', N'no', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (63, N'Persian', N'fa', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (64, N'Polish', N'pl', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (65, N'Portuguese', N'pt', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (66, N'Punjabi', N'ma', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (67, N'Romanian', N'ro', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (68, N'Russian', N'ru', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (69, N'Serbian', N'sr', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (70, N'Sesotho', N'st', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (71, N'Sinhala', N'si', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (72, N'Slovak', N'sk', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (73, N'Slovenian', N'sl', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (74, N'Somali', N'so', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (75, N'Spanish', N'es', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (76, N'Sudanese', N'su', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (77, N'Swahili', N'sw', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (78, N'Swedish', N'sv', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (79, N'Tajik', N'tg', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (80, N'Tamil', N'ta', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (81, N'Telugu', N'te', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (82, N'Thai', N'th', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (83, N'Turkish', N'tr', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (84, N'Ukrainian', N'uk', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (85, N'Urdu', N'ur', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (86, N'Uzbek', N'uz', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (87, N'Vietnamese', N'vi', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (88, N'Welsh', N'cy', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (89, N'Yiddish', N'yi', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (90, N'Yoruba', N'yo', 1)
INSERT [dbo].[IdiomaTraducao] ([Codigo], [Nome], [Sigla], [Status]) VALUES (91, N'Zulu', N'zu', 1)



INSERT INTO [dbo].[Idioma]
           (
           [Nome]
           ,[Descricao]
           ,[Status]
           ,[DataAlteracao]
           ,[CodigoUsuarioAlteracao]
		   ,CodigoIdiomaTraducao)
     VALUES
           (
           'Português'
           ,'pt-br'
           ,1
           ,getdate()
           ,1
		   ,65)
go


INSERT INTO TIPOGERACAO
SELECT 1, 'GTIN', 'Quando possui somente o campo “GTIN” preenchido (Padrão)',1
UNION SELECT 2, 'GTIN + Descrição', 'Quando possui o campo “GTIN” e Descrição preenchidos',1
UNION SELECT 3, 'GTIN + Descrição + Campo Livre', 'Quando possui o campo “GTIN”, “Descrição” e “Campo Livre” preenchidos',1
UNION SELECT 4, 'Baixar Imagem', 'Quando somente a imagem é baixada',0
GO

INSERT INTO POSICAOTEXTO
SELECT 1, 'À Direita', 'À Direita',1
UNION SELECT 2, 'À Esquerda', 'À Esquerda',1
UNION SELECT 3, 'Abaixo', 'Abaixo',1
UNION SELECT 4, 'Acima', 'Acima',1
GO



INSERT [dbo].[Fabricante] ([Codigo], [Nome], [Descricao], [Status]) VALUES (1, N'Avery', N'Avery', 1)
GO
INSERT [dbo].[Fabricante] ([Codigo], [Nome], [Descricao], [Status]) VALUES (2, N'Colacril', N'Colacril', 1)
GO
INSERT [dbo].[Fabricante] ([Codigo], [Nome], [Descricao], [Status]) VALUES (3, N'Pimaco', N'Pimaco', 1)
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (1, N'L7163', N'L7163', 1, 1, CAST(90.10 AS Decimal(10, 2)), CAST(30.10 AS Decimal(10, 2)), N'A4', 2, 7, CAST(15.10 AS Decimal(10, 2)), CAST(13.00 AS Decimal(10, 2)), CAST(6.10 AS Decimal(10, 2)), CAST(7.90 AS Decimal(10, 2)), CAST(10.00 AS Decimal(10, 2)), CAST(10.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (2, N'8096', N'8096', 1, 3, CAST(69.85 AS Decimal(10, 2)), CAST(69.85 AS Decimal(10, 2)), N'CARTA', 3, 3, CAST(1.27 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), CAST(0.32 AS Decimal(10, 2)), CAST(0.32 AS Decimal(10, 2)), CAST(0.32 AS Decimal(10, 2)), CAST(0.32 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (3, N'5160', N'5160', 1, 1, CAST(63.85 AS Decimal(10, 2)), CAST(22.65 AS Decimal(10, 2)), N'A4', 3, 10, CAST(21.50 AS Decimal(10, 2)), CAST(0.25 AS Decimal(10, 2)), CAST(1.50 AS Decimal(10, 2)), CAST(1.00 AS Decimal(10, 2)), CAST(2.95 AS Decimal(10, 2)), CAST(5.25 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (4, N'A4256', N'A4256', 1, 3, CAST(63.50 AS Decimal(10, 2)), CAST(20.40 AS Decimal(10, 2)), N'A4', 3, 11, CAST(6.80 AS Decimal(10, 2)), CAST(1.80 AS Decimal(10, 2)), CAST(5.00 AS Decimal(10, 2)), CAST(0.50 AS Decimal(10, 2)), CAST(6.30 AS Decimal(10, 2)), CAST(5.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (5, N'A4262', N'A4262', 1, 3, CAST(99.00 AS Decimal(10, 2)), CAST(30.00 AS Decimal(10, 2)), N'A4', 2, 8, CAST(10.90 AS Decimal(10, 2)), CAST(1.80 AS Decimal(10, 2)), CAST(2.70 AS Decimal(10, 2)), CAST(0.25 AS Decimal(10, 2)), CAST(6.30 AS Decimal(10, 2)), CAST(5.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (6, N'6283', N'6283', 1, 3, CAST(99.00 AS Decimal(10, 2)), CAST(50.80 AS Decimal(10, 2)), N'CARTA', 2, 5, CAST(12.27 AS Decimal(10, 2)), CAST(1.80 AS Decimal(10, 2)), CAST(0.05 AS Decimal(10, 2)), CAST(0.05 AS Decimal(10, 2)), CAST(6.30 AS Decimal(10, 2)), CAST(5.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (7, N'L7160', N'L7160', 1, 1, CAST(63.50 AS Decimal(10, 2)), CAST(34.00 AS Decimal(10, 2)), N'A4', 3, 7, CAST(12.50 AS Decimal(10, 2)), CAST(1.80 AS Decimal(10, 2)), CAST(5.20 AS Decimal(10, 2)), CAST(0.50 AS Decimal(10, 2)), CAST(6.30 AS Decimal(10, 2)), CAST(4.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (8, N'CA4255', N'CA4255', 1, 2, CAST(63.50 AS Decimal(10, 2)), CAST(29.00 AS Decimal(10, 2)), N'A4', 3, 9, CAST(12.50 AS Decimal(10, 2)), CAST(0.25 AS Decimal(10, 2)), CAST(1.50 AS Decimal(10, 2)), CAST(1.00 AS Decimal(10, 2)), CAST(2.50 AS Decimal(10, 2)), CAST(5.25 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (9, N'CA4262', N'CA4262', 1, 2, CAST(99.00 AS Decimal(10, 2)), CAST(30.00 AS Decimal(10, 2)), N'A4', 2, 8, CAST(10.90 AS Decimal(10, 2)), CAST(1.80 AS Decimal(10, 2)), CAST(2.70 AS Decimal(10, 2)), CAST(0.25 AS Decimal(10, 2)), CAST(6.30 AS Decimal(10, 2)), CAST(5.00 AS Decimal(10, 2)))
GO
INSERT [dbo].[ModeloEtiqueta] ([Codigo], [Nome], [Descricao], [Status], [CodigoFabricante], [Largura], [Altura], [TamanhoFolha], [QuantidadeColunas], [QuantidadeLinhas], [MargemSuperior], [MargemInferior], [MargemEsquerda], [MargemDireita], [EspacamentoVertical], [EspacamentoHorizontal]) VALUES (10, N'CC283', N'CC283', 1, 2, CAST(99.00 AS Decimal(10, 2)), CAST(50.80 AS Decimal(10, 2)), N'CARTA', 2, 5, CAST(12.27 AS Decimal(10, 2)), CAST(1.80 AS Decimal(10, 2)), CAST(0.05 AS Decimal(10, 2)), CAST(0.05 AS Decimal(10, 2)), CAST(6.30 AS Decimal(10, 2)), CAST(5.00 AS Decimal(10, 2)))
GO


INSERT INTO TEMPLATECARTA 
SELECT 1, NULL, GETDATE(), 1
UNION SELECT 2, NULL, GETDATE(), 1
UNION SELECT 4, NULL, GETDATE(), 1
UNION SELECT 7, NULL, GETDATE(), 1
GO


INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (1, N'AE', N'Aerosol', N'A gas-tight, pressure-resistant container with a valve and propellant. When the valve is opened, propellant forces the product from the container in a fine or coarse spray pattern or stream. (e.g., a spray can dispensing paint, furniture polish, etc, under pressure). It does not include atomizers, because atomizers do not rely on a pressurised container to propel product from the container.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (2, N'AM', N'Ampoule', N'A relatively small container made from glass or plastic tubing, the end of which is drawn into a stem and closed by fusion after filling. The bottom may be flat, convex, or drawn out. An ampule is opened by breaking the stem.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (3, N'BA', N'Barrel', N'A cylindrical packaging whose bottom end is permanently fixed to the body and top end (head) is either removable or non-removable.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (4, N'BBG', N'Bag in Box', N'Bag-In-Box or BIB is a type of container for the storage and transportation of liquids. It consists of a strong bladder, usually made of aluminium PET film or other plastics seated inside a corrugated fibreboard box. The box and internal bag can be fused together. In most cases there is nozzle or valve fixed to the bag. The nozzle can be connected easily to a dispensing installation or the valve allows for convenient dispensing.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (5, N'BG', N'Bag', N'A preformed, flexible container, generally enclosed on all but one side, which forms an opening that may or may not be sealed after filling.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (6, N'BJ', N'Bucket', N'A container, usually cylindrical, can be equipped with a lid and a handle. (e.g., a pail made of metal, plastic, or other appropriate material).', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (7, N'BK', N'Basket', N'A semi rigid container usually open at the top traditionally used for gathering, shipping and marketing agricultural products.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (8, N'X11', N'Banded package', N'Something used to bind, tie, or encircle the item or its packaging to secure and maintain unit integrity.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (9, N'BO', N'Bottle', N'A container having a round neck of relatively smaller diameter than the body and an opening capable of holding a closure for retention of the contents. Specifically, a narrow-necked container as compared with a jar or wide-mouth container. The cross section of the bottle may be round, oval, square, oblong, or a combination of these. Bottles generally are made of glass or plastics, but can also be earthenware or metal. Bottle may be disposable, recyclable, returnable, or reusable.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (10, N'BPG', N'Blister pack', N'A type of packaging in which the item is secured between a preformed (usually transparent plastic) dome or “bubble” and a paperboard surface or “carrier.” Attachment may be by stapling, heat- sealing, gluing, or other means. In other instances, the blister folds over the product in clam-shell fashion to form an enclosing container. Blisters are most usually thermoformed from polyvinyl chloride; however, almost any thermoplastic can be thermoformed into a blister.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (11, N'BRI', N'Brick', N'A rectangular-shaped, stackable package designed primarily for liquids such as juice or milk', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (12, N'BX', N'Box', N'A non-specific term used to refer to a rigid, three- dimensional container with closed faces that completely enclose its contents and may be made out of any material. Even though some boxes might be reused or become resealed they could also be disposable depending on the product hierarchy.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (13, N'CG', N'Cage', N'A container enclosed on at least one side by a grating of wires or bars that lets in air and light.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (14, N'CM', N'Card', N'A flat package to which the product is hung or attached for display.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (15, N'CNG', N'Can', N'A metallic and generally cylindrical container of unspecified size which can be used for items of consumer and institutional sizes.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (16, N'CR', N'Crate', N'A non-specific term usually referring to a rigid three- dimensional container with semi-closed faces that enclose its contents for shipment or storage. Crates could have an open or closed top and may have internal divers. Even though some crates might be reused or become resealed they could also be disposable depending on the product hierarchy.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (17, N'CS', N'Case', N'A non-specific term for a container designed to hold, house, and sheath or encase its content while protecting it during distribution, storage and/or exhibition. Cases are mostly intended to store and preserve its contents during the product''s entire lifetime.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (18, N'CT', N'Carton', N'A non-specific term for an open or re-closable container used mostly for perishable foods (e.g. eggs, or fruit).', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (19, N'CU', N'Cup\Tub', N'A flat-bottomed container that has a base of any shape and which may or not be closed with a lid. Usually made of paper, plastic or other materials these containers are typically used to contain mostly (but not exclusively) foods such as ice cream, margarine, yogurt, sour cream, confections, etc.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (20, N'CY', N'Cylinder', N'A rigid cylindrical container with straight sides and circular ends of equal size.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (21, N'EN', N'Envelope', N'A predominantly flat container of flexible material having only two faces, and joined at three edges to form an enclosure. The non-joined edge provides a filling opening, which may later be closed by a gummed or adhesive flap, heat seal, tie string, metal clasp, or other methods.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (22, N'GTG', N'Gable Top', N'A rectangular-shaped, non-stackable package designed primarily for liquids such as juice or milk', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (23, N'JG', N'Jug', N'A container, normally cylindrical, with a handle and/or a lid or spout for holding and pouring liquids', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (24, N'JR', N'Jar', N'A rigid container made of glass, stone, earthenware, plastic or other appropriate material with a large opening, which is used to store products, (e.g., jams, cosmetics).', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (25, N'MPG', N'Multipack', N'A bundle of products held together for ease of carriage by the consumer. A multipack is always a consumer unit.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (26, N'NE', N'Not packed', N'The item is provided without packaging.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (27, N'NT', N'Net', N'A container of meshwork material made from threads or strips twisted or woven to form a regular pattern with spaces between the threads that is used for holding, carrying, trapping, or confining something.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (28, N'PB', N'Pallet Box', N'A three-dimensional container which either has a pallet platform permanently attached at its base or alternatively requires a platform for its handling and storage as due to its constitution it cannot be handled without it. The characteristics of the platform should be specified using the pallet type code list', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (29, N'PO', N'Pouch', N'A preformed, flexible container, generally enclosed with a gusset seal at the bottom of the pack can be shaped/arranged to allow the pack to stand on shelf.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (30, N'PLP', N'Peel Pack', N'A package used for sterile products which may be torn open without touching the product inside.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (31, N'PT', N'Pot', N'A flat-bottomed container that has a base of any shape and which may or not be closed with a lid. Pots are usually made of cardboard, plastic, ceramic, metal or other materials and may be used for a wide array of products such as cosmetics, food/liquids, dairy products, plants.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (32, N'PU', N'Tray', N'A shallow container, which may or may not have a cover, used for displaying or carrying items.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (33, N'PUG', N'Packed, unspecified', N'Packaging of the product (or products) is currently not on the list. Use this code when no suitable options are available and only while a Change Request is approved for the proper packaging type.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (34, N'PX', N'Pallet', N'A platform used to hold or transport unit loads.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (35, N'RK', N'Rack', N'A non specific term identifying a framework or stand for carrying, holding, or storing items. Commonly on wheels and primarily used in the logistical functions to deliver items such as hanging garments, or items on shelves such as dairy products and bakery items and flowers.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (36, N'RL', N'Reel', N'A spool on which thread, wire, film, etc, is wound. Any device on which a material may be wound. Usually has flanged ends and is used for shipping or processing purposes.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (37, N'STR', N'Stretchwrapped', N'In packaging, a high-tensile plastic film, stretched and wrapped repeatedly around an item or group of items to secure and maintain unit integrity. The use of stretch film to tightly wrap a package or a unit load in order to bind, protect and immobilize it for further handling or shipping.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (38, N'SW', N'Shrinkwrapped', N'In packaging, a plastic film around an item or group of items which is heated causing the film to shrink, securing the unit integrity. The use of shrunken film to tightly wrap a package or a unit load in order to bind, protect and immobilize it for further handling or shipping.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (39, N'SY', N'Sleeve', N'A non-rigid container usually made of paper, cardboard or plastic, that is open-ended and is slid over the contents for protection or presentation.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (40, N'TU', N'Tube', N'A cylindrical container sealed on one end that could be closed with a cap or dispenser on the other end.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (41, N'WRP', N'Wrapper', N'The process of enclosing all or part of an item with layers of flexible wrapping material (e.g., for an individually packed ice cream). Does not include items which are shrink-wrapped or vacuum-packed.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (42, N'ZU', N'Flexible Intermediate Bulk Container', N'A non-rigid container used for transport and storage of fluids and other bulk materials. The construction of the IBC container and the materials used are chosen depending on the application.', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (43, N'AA', N'Intermediate Bulk Container, rigid plastic', N'A Rigid Intermediate Bulk Container (RIBC) that is attached to a pallet or has the pallet integrated into the RIBC. The container is used for the transport and storage of fluids and other bulk materials', 1)
INSERT [dbo].[PackagingTypeCode] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (44, N'CMS', N'Clam Shell', N'A one-piece container consisting of two halves joined by a hinge area which allows the structure to come together to close. Clamshells get their name from their appearance to the shell of a clam, which it resembles both in form and function.', 1)
GO

INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (1, N'1', N'Soft Wood', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (2, N'10', N'Pallet ISO 0 - 1/2 EURO Pallet Standard pallet with dimensions 80 X 60 cm.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (3, N'11', N'Pallet ISO 1 - 1/1 EURO Pallet Standard pallet with dimensions 80 X 120 cm.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (4, N'12', N'Pallet ISO 2 Standard pallet with dimensions 100 X 120 cm.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (5, N'13', N'1/4 EURO Pallet Standard pallet with dimensions 60 X 40 cm.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (6, N'14', N'1/8 EURO Pallet Standard pallet with dimensions 40 X 30 cm.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (7, N'15', N'Synthetic pallet ISO 1 A standard pallet with standard dimensions 80*120cm made of a synthetic material for hygienic reasons.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (8, N'16', N'Synthetic pallet ISO 2 A standard pallet with standard dimensions 100*120cm made of a synthetic material for hygienic reasons.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (9, N'17', N'Wholesaler pallet Pallet provided by the wholesaler', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (10, N'18', N'Pallet 80 X 100 cm Pallet with dimensions 80 X 100 cm.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (11, N'19', N'Pallet 60 X 100 cm Pallet with dimensions 60 X 100 cm.', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (12, N'2', N'Aluminum', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (13, N'20', N'Pallet, modular, collars 80cms * 100cms', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (14, N'21', N'Pallet, modular, collars 80cms * 120cms', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (15, N'22', N'CHEP Pallet 40 X 60 cm', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (16, N'23', N'CHEP Pallet 80 X 120 cm', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (17, N'24', N'CHEP Pallet 100 X 120 cm', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (18, N'25', N'AS 4068-1993 Australian Pallet with dimension 115.5 X 116.5 cm', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (19, N'3', N'As Specified by the Department of Transportation (DOT)', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (20, N'4', N'Hard Wood', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (21, N'5', N'Metal', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (22, N'6', N'Standard', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (23, N'7', N'Steel', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (24, N'8', N'Wood', 1)
INSERT [dbo].[palletTypeCode] ([Codigo], [Code], [CodeDescription], [Status]) VALUES (25, N'9', N'Slip sheet Typically cardboard or plastic sheets used to hold product for storage or transportation', 1)
GO


INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (1, N'15', N'Stick', N'Stick', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (2, N'1N', N'Count', N'Count', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (3, N'23', N'Grams Per Cubic Centimetre', N'Grams Per Cubic Centimetre', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (4, N'26', N'Actual Ton', N'Actual Ton', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (5, N'28', N'Kilogram per square metre', N'Kilogram per square metre: a unit of pressure equal to 9.80665*10-05 bar.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (6, N'2N', N'Decibel', N'Decibel: a measurement for sound in air and other gases, relative to 20 micropascals (?Pa) = 2×10?5 Pa, the quietest sound a human can hear. This is roughly the sound of a mosquito flying 3 metres away. This is often abbreviated to just "dB"; however the correct abbreviation is dB(SPL), indicating decibel for Sound Pressure Level.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (7, N'2P', N'Kilobyte', N'Kilobyte: a unit of information equal to 10³ (1000) bytes.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (8, N'2Q', N'Kilo Becquerel', N'Kilo Becquerel', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (9, N'4G', N'Microlitre', N'Microlitre', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (10, N'4H', N'Micrometre', N'Micrometre: a millionth of a metre, also termed Micron.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (11, N'4L', N'Megabyte', N'Megabyte: a unit of information equal to 10? (1000000) bytes.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (12, N'4N', N'Megabecquerel', N'Megabecquerel: 106 Bq1 Bq is defined as the activity of a quantity of radioactive material in which one nucleus decays per second.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (13, N'58', N'Net Kilograms', N'Net Kilograms', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (14, N'59', N'Parts per Million', N'Parts per Million: One ppm is equivalent to 1 milligram of something per litre of water (mg/l) or 1 milligram of something per kilogram soil (mg/kg).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (15, N'5B', N'Batch', N'A unit of count defining the number of batches (batch: quantity of material produced in one operation or number of animals or persons coming at once).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (16, N'64', N'Pounds per square inch gauge', N'Pounds per square inch gauge: at sea level, Earth''s atmosphere actually exerts a pressure of 14.7 psi. Humans do not feel this pressure because internal pressure of liquid in their bodies matches the external pressure. If a pressure gauge is calibrated to read zero in space, then at sea level on Earth it would read 14.7 psi. Thus a reading of 30 psig, on Earth, on a tire gauge represents an absolute pressure of 44.7 psi (lb/in²).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (17, N'A24', N'Candela per Square Meter', N'Candela per Square Meter is the SI base unit of luminous intensity; that is, power emitted by a light source in a particular direction, weighted by the luminosity function in square meters. This is also known as nit in some markets.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (18, N'A86', N'Gigahertz', N'Gigahertz: a unit of frequency equal to 109 Hertz', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (19, N'AD', N'Byte', N'Byte: a unit of information equal to 8 bits.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (20, N'AF', N'Centigram', N'Centigram', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (21, N'AIU', N'Anti XA Unit (International Units)', N'Anti XA Unit (International Units): A unit of measure for blood potency. International units for the anti XA activity which is a measure to the anti coagulating effect at low molecular heparins. A unit of measure for blood potency.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (22, N'ANN', N'Year', N'Year: A unit of time comprising twelve contiguous months.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (23, N'AS', N'Assortment', N'Assortment: a unit of count defining the number of assortments (assortment: set of items grouped in a mixed collection).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (24, N'AXU', N'Anti XA Unit', N'Anti XA Unit: A unit of measure for blood potency. Units for the anti XA activity which is a measure to the anti coagulating effect at low molecular heparins.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (25, N'B10', N'Bit Per Second', N'In telecommunications and computing, bitrate (sometimes written bit rate, data rate or as a variable R or fb) is the number of bits that are conveyed or processed per unit of time.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (26, N'B60', N'Lumens per Square Meter', N'Lumens per Square Meter is the SI derived unit of luminous flux, a measure of the total "amount" of visible light emitted by a source in square meters.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (27, N'B8', N'Board', N'Board', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (28, N'BA', N'Bale', N'Bale', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (29, N'BB', N'Base Box', N'Base Box: a unit of area of 112 sheets of tin mil products (tin plate, tin free steel or black plate) 14 by 20 inches, or 31,360 square inches.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (30, N'BD', N'Bundle', N'Bundle', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (31, N'BF', N'Board Foot', N'A specialized unit of measure for the volume of rough lumber (before drying and planing with no adjustments) or planed/surfaced lumber. It is the volume of a one-foot length of a board one foot wide and one inch thick. Some countries utilize the synonym super foot or superficial foot.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (32, N'BG', N'Bag', N'Bag', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (33, N'BI', N'Bar', N'Bar: the bar is widely used in descriptions of pressure; 1 bar = 100 kilopascals 0.987 atmospheres.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (34, N'BL', N'Block', N'Block', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (35, N'BN', N'Bulk', N'Bulk', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (36, N'BO', N'Bottle', N'Bottle', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (37, N'BP', N'A unit of volume equal to one hundred board foot', N'A unit of volume equal to one hundred board foot.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (38, N'BQ', N'Becquerel', N'The becquerel (symbol Bq) is the SI derived unit of radioactivity. One Bq is defined as the activity of a quantity of radioactive material in which one nucleus decays per second. SI uses the becquerel rather than the second for the unit of activity measure to avoid dangerous mistakes: a measurement in becquerels is proportional to activity, and thus a more dangerous source of radiation gives a higher reading. A measurement in seconds is inversely proportional.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (39, N'BR', N'Barrel', N'Barrel', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (40, N'BTU', N'British Thermal Unit', N'British thermal unit: the British thermal unit (BTU or Btu) is a traditional unit of energy. It is approximately the amount of energy needed to heat one pound of water one degree Fahrenheit. One Btu is equal to about 1.06 kilojoules. It is used in the power, steam generation, heating and air conditioning industries.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (41, N'BU', N'Bushel', N'Bushel', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (42, N'BX', N'Box', N'Box', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (43, N'C18', N'Millimole', N'Millimole:1/1000 part of a mole (measure of the concentration of a solute).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (44, N'C26', N'Millisecond', N'Millisecond: a measurement of time.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (45, N'C3', N'Centilitre', N'Centilitre', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (46, N'C34', N'Mole', N'Mole: a mole will possess mass exactly equal to the substance''s molecular or atomic weight in grams. That is to say, a substance''s atomic or molecular mass in atomic mass units is the same as its molar mass in grams. Because of this, one can measure the number of moles in a pure substance by weighing it and comparing the result to its molecular or atomic weight"', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (47, N'C8', N'Cubic Decimetre', N'Cubic Decimetre: a cubic decimetre is the volume of a cube of side length one decimetre (0.1 m).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (48, N'CA', N'Case', N'Case', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (49, N'CC', N'Cubic Centimetre', N'Cubic Centimetre: a cubic centimetre is the volume of a cube of side length one centimetre (0.01 m) equal to a millilitre.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (50, N'CE', N'Degrees Celsius', N'Degrees Celsius: celsius (also historically known as centigrade) is a temperature scale, the freezing point of water is 0 degrees Celsius (°C) and the boiling point 100 °C (at standard atmospheric pressure), placing the boiling and freezing points of water exactly 100 degrees apart.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (51, N'CF', N'Cubic Foot', N'Cubic Foot: a cubic foot is the volume of a cube of side length one foot (0.3048 m).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (52, N'CG', N'Card', N'Card: a unit of count defining the number of units of card (card: thick stiff paper or cardboard).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (53, N'CHD', N'Centisimal Hahnemannian Dilution', N'Centisimal Hahnemannian Dilution (CH): "CH Centesimal Scale Attenuation - One millilitre (1.0 ml) of the first centesimal liquid attenuation (1C), or one gram (1.0 g) of the first centesimal trituration (1C) represents 0.01 gram (10.0 mg) of the dry crude medicinal substance. Subsequent liquid or solid attenuations are made by serial progression, succussing or triturating one (1) part of the preceding attenuation to 99 parts of the vehicle, and represent the following proportions of active principle (i.e., dried medicinal substance): 2CH = 10-4, 3CH = 10-6.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (54, N'CI', N'Cubic Inch', N'Cubic Inch: A cubic inch is the volume of a cube of side length one inch (0.254 m).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (55, N'CM', N'Centimetre', N'Centimetre: A centimetre is equal to one hundredth of a metre.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (56, N'CMK', N'Square Centimetre', N'Square Centimetre: an area of a square whose sides are exactly 1 centimetre in length.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (57, N'CN', N'Can', N'Can', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (58, N'CO', N'Cubic Meters', N'Cubic Meters: a cubic metre is the volume of a cube of side length one metre.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (59, N'CQ', N'Cartridge', N'Cartridge', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (60, N'CR', N'Cubic Meter', N'Cubic Meter', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (61, N'CT', N'Carton', N'Carton', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (62, N'CU', N'Cup', N'Cup', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (63, N'CV', N'Cover', N'Cover', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (64, N'CW', N'Hundred Pounds', N'Hundred Pounds (CWT)', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (65, N'CWI', N'A unit of weight in the British Imperial System equal to 112 pounds (50.80 kilograms); also called quintal.', N'A unit of weight in the British Imperial System equal to 112 pounds (50.80 kilograms); also called quintal.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (66, N'D43', N'Atomic Mass Units (AMU)', N'Atomic Mass Units (AMU)', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (67, N'D5', N'Kilogram-force Per Square Centimetre', N'A kilogram-force per square centimetre (kgf/cm2), often just kilogram per square centimetre (kg/cm2), or kilopond per square centimetre is a unit of pressure using metric units. Its use is now deprecated; it is not a part of the International System of Units (SI), the modern metric system. The unit is similar to the English unit psi (lbf/in2).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (68, N'D63', N'A unit of count defining the number of books (book: set of items bound together or written document of a material whole).', N'A unit of count defining the number of books (book: set of items bound together or written document of a material whole).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (69, N'D70', N'International Table Calorie', N'International Table Calorie: calorie is 1/100 of the amount of energy required to warm one gram of air-free water from 0 °C to 100 °C at standard atmospheric pressure; this is about 4.190 J. Its use is archaic, having been replaced by the SI unit of energy, the joule. However, in many countries it remains in common use as a unit of food energy. In the context of nutrition, and especially food labelling, the calorie is approximately equal to 4.1868 joules (J), and energy values are normally quoted in kilojoules (kJ) and kilocalories (kcal).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (70, N'DA', N'Days', N'Days: a day is one three hundreds and sixty fifth (1/365) of a year', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (71, N'DD', N'A measurement of plane angle, representing 1?360 of a full rotation; one degree is equivalent to ?/180 radians.', N'A measurement of plane angle, representing 1?360 of a full rotation; one degree is equivalent to ?/180 radians.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (72, N'DG', N'Decigram', N'Decigram: one tenth (1/10) of a gram.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (73, N'DK', N'Kilometre', N'Kilometre: a kilometre is one thousand (1000) metres.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (74, N'DLT', N'Decilitre', N'Decilitre: A measure of capacity or volume in the metric system; one tenth of a litre, equal to 6.1022 cubic inches, or 3.38 fluid ounces..', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (75, N'DMK', N'Square Decimetre', N'Square Decimetre: a square decimetre is an area of a square whose sides are exactly 1 decimetre in length.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (76, N'DMT', N'Decimetre', N'Decimetre: one tenth of a metre.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (77, N'DO', N'Dollars, U.S.', N'Dollars, U.S.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (78, N'DR', N'Drum', N'Drum', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (79, N'DRA', N'Dram (US)', N'dram (US): The dram (archaic spelling drachm) was historically both a coin and a weight. Currently it is both a small mass in the Apothecaries'' system of weights and a small unit of volume. This unit is called more correctly fluid dram or in contraction also fluidram.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (80, N'DRI', N'Dram (UK)', N'dram (UK): the dram (archaic spelling drachm) was historically both a coin and a weight. Currently it is both a small mass in the Apothecaries'' system of weights and a small unit of volume. This unit is called more correctly fluid dram or in contraction also fluidram. The fluid dram is defined as 1?8 of a fluid ounce, which means it is exactly equal to 3.551 632 812 500 0 mL in the Commonwealth and Ireland. In England dram came to mean a small draught of cordial or alcohol; hence the term dram-house for the taverns where one could purchase a dram.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (81, N'DS', N'Display', N'Display', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (82, N'DZ', N'Dozen', N'Dozen: a unit of count defining the number of units in multiples of 12.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (83, N'E14', N'Kilocalorie (international table)', N'Kilocalorie (international table): A unit of energy equal to 1000 calories.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (84, N'E27', N'Dose', N'Dose: a unit of count defining the number of doses (dose: a definite quantity of a medicine or drug).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (85, N'E34', N'Gigabyte', N'Gigabyte: a unit of information equal to 109 bytes.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (86, N'E35', N'Terabyte', N'Terabyte: a unit of information equal to 10¹² bytes.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (87, N'E37', N'Pixel', N'Pixel: a unit of count defining the number of pixels (pixel: picture element).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (88, N'E39', N'Dots Per Inch', N'Dots Per Inch', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (89, N'EA', N'Each', N'Each: a unit of count defining the number of items regarded as separate units.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (90, N'ELU', N'ELISA Units', N'ELISA Units: Enzyme-linked immunosorbent assay unit, is always associated with a product and a method.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (91, N'EV', N'Envelope', N'Envelope', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (92, N'FA', N'Degrees Fahrenheit', N'Degrees Fahrenheit: the Fahrenheit temperature scale, the freezing point of water is 32 degrees Fahrenheit (°F) and the boiling point 212 °F (at standard atmospheric pressure), placing the boiling and freezing points of water exactly 180 degrees apart.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (93, N'FH', N'Micromole', N'Micromole', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (94, N'FJ', N'Sizing Factor', N'Sizing Factor', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (95, N'FO', N'Fluid Ounce', N'Fluid Ounce', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (96, N'FP', N'Pound per square foot', N'Pound per square foot: a non SI unit of Pressure approximately equal to 47.88025 PASCAL''s.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (97, N'FT', N'Feet', N'Feet', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (98, N'G24', N'Tablespoon', N'Tablespoon. 1/2 fluid ounces, 3 teaspoons, 15 millilitres', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (99, N'G25', N'Teaspoon', N'Teaspoon. 1/6 fluid ounces or 5 millilitres', 1)
GO
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (100, N'GA', N'Gallon', N'Gallon', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (101, N'GBQ', N'Gigabecquerel', N'Gigabecquerel: gigabecquerel, 109 Bq. 1 Bq is defined as the activity of a quantity of radioactive material in which one nucleus decays per second.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (102, N'GM', N'Gram per square metre', N'Gram per square metre: The paper density of a type of paper or cardboard is the mass of the product per unit of area. The term density here is used somewhat incorrectly, as density is mass by volume. More precisely, ""paper density"" is a measure of the area density. Expressed in grams per square metre (g/m²), paper density is also known as grammag. This is the measure used in most parts of the world.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (103, N'GR', N'Gram', N'Gram: one one-thousandth of the kilogram (1×10-3 kg).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (104, N'GRN', N'Grain or Troy Grain', N'A grain or troy grain is precisely 64.79891 milligrams. Exactly 7,000 grains per avoirdupois pound.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (105, N'GRO', N'Number of units in multiples of 144 (12 x 12)', N'A unit of count defining the number of units in multiples of 144 (12 x 12).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (106, N'GT', N'Gross Kilogram', N'Gross Kilogram: A unit of mass defining the total number of kilograms before deductions.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (107, N'GWH', N'Gigawatt Hour', N'A gigawatt hour is 109 kilowatt hour or 3.6 terajoules.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (108, N'H79', N'French Gauge', N'French Gauge: the unit of measure of the diameter. i.e. the diameter of a syringe (FR)', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (109, N'HC', N'Hundred Count', N'Hundred Count: a unit of count defining the number of units counted in multiples of 100.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (110, N'HD', N'Half Dozen', N'Half Dozen: a unit of count defining the number of units in multiple of six (6).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (111, N'HEP', N'Histamine Equivalent Prick', N'Histamine Equivalent Prick: Histamine equivalent prick testing for allergen.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (112, N'HGM', N'Hectogram', N'Hectogram: one hundred (100) grams', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (113, N'HL', N'Hundred Feet', N'Hundred Feet', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (114, N'HLT', N'Hectolitre', N'Hectolitre: one hundred (100) litres.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (115, N'HTZ', N'Hertz', N'Hertz: a unit of frequency defined as the number of complete cycles per second; it is the basic unit of frequency in the International System of Units (SI).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (116, N'HUR', N'Hour', N'Hour: A period of time equal to 1/8th of a working day while an hour equals 1/24th of a calendar day.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (117, N'IN', N'Inch', N'Inch: an international inch is defined to be equal to 25.4 millimetres.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (118, N'INK', N'Square Inch', N'Square Inch: an area of a square whose sides are exactly 1 inch in length.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (119, N'JOU', N'Joule', N'Joule: the unit of work or energy, defined to be the work done by a force of one newton acting to move an object through a distance of one meter in the direction in which the force is applied.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (120, N'JR', N'Jar', N'Jar', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (121, N'K6', N'Kilolitre', N'Kilolitre: a metric unit of volume. The kilolitre is identical to the cubic meter.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (122, N'KE', N'Keg', N'Keg', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (123, N'KEL', N'Kelvin', N'Kelvin: a unit of absolute temperature equal to 1/273.16 of the absolute temperature of the triple point of water. One kelvin degree is equal to one Celsius degree.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (124, N'KG', N'Kilogram', N'Kilogram: a unit of mass equal to one thousand grams.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (125, N'KHZ', N'Kilohertz', N'Kilohertz: a unit of frenquecy equal to 103 Hertz', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (126, N'KIU', N'Kallikrein inactivator unit.', N'Kallikrein inactivator unit. Kallikrein Inactivator Unit per Milliliter definition: An arbitrary unit of a kallikrein inactivator concentration equal to the concentration at which one milliliter of the mixture contains one unit of the kallikrein inactivator.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (127, N'KJO', N'Kilojoule', N'Kilojoule: a kilojoule is 1000 joules.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (128, N'KT', N'Kit', N'Kit: a unit of count defining the number of kits (kit: tub, barrel or pail).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (129, N'KVN', N'Korsakovian (K)', N'Korsakovian (K): K Centesimal Scale of Attenuation - One millilitre (1.0 ml) of the first centesimal liquid attenuation (1C), or one gram (1.0 g) of the first centesimal trituration (1C) represents 0.01 gram (10.0 mg) of the dry crude medicinal substance. Subsequent liquid or solid attenuations are made by serial progression, succussing or triturating one (1) part of the preceding attenuation to 99 parts of the vehicle, and represent the following proportions of active principle (i.e., dried medicinal substance): 2CH = 10-4, 3CH = 10-6.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (130, N'KWH', N'Kilowatt hour', N'Kilowatt hour:” a commercial unit of electric energy. One kilowatt hour represents the amount of energy delivered at a rate of 1000 watts over a period of one hour.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (131, N'KWT', N'Kilowatt', N'Kilowatt: a unit of power, equivalent to 1000 watts.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (132, N'LB', N'Pound', N'Pound: the international avoirdupois pound of exactly 0.45359237 kilogram.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (133, N'LF', N'Linear Foot', N'Linear Foot: a unit of count defining the number of feet (12-inch) in length of a uniform width object.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (134, N'LK', N'Link', N'Link: a unit of distance equal to 0.01 chain.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (135, N'LM', N'Linear Meter', N'Linear Meter: a unit of count defining the number of metres in length of a uniform width object.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (136, N'LR', N'Layer', N'Layer: a unit of count defining the number of layers.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (137, N'LT', N'Litre', N'Litre: a litre is defined as a special name for a cubic decimetre (1 L = 1 dm3 = 103 cm3).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (138, N'LUX', N'Lux', N'Lux is the SI unit of illuminance and luminous emittance, measuring luminous flux per unit area.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (139, N'MAW', N'Megawatt', N'Megawatt: a unit of power defining the rate of energy transferred or consumed when a current of 1000 amperes flows due to a potential of 1000 volts at unity power factor.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (140, N'MC', N'Microgram', N'Microgram: a microgram is one millionth of a gram (0.000001).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (141, N'ME', N'Milligram', N'Milligram: a milligram is one thousandth of a gram (0.001).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (142, N'MHZ', N'Megahertz', N'Megahertz: A unit of frenquecy equal to 106 Hertz', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (143, N'MIK', N'Square Mile', N'Square Mile: a unit of area. One square mile is equal to 640 acres, 3 097 600 square yards, 258.9988 hectares.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (144, N'MIN', N'Minute', N'Minute: a unit of time equal to 1/60th of an hour or 60 seconds', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (145, N'MIU', N'Million International Unit (NIE)', N'Million International Unit (NIE): A unit of count defining the number of international units in multiples of 106.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (146, N'ML', N'Millilitre', N'Millilitre: one thousandth of a litre (0.001)', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (147, N'MLM', N'Millesimal (LM)', N'Millesimal (LM): LM - Fifty Millesimal Scale Of Attenuation One millilitre (1.0 ml) of the first fifty millesimal attenuation (1LM) represents 6.20 x 10-11 of dry crude medicinal substance. Impregnate the lactose in a proportion of 1 to 100 beginning with the liquid substance (mother tincture), then triturate. The second and third triturations are carried out in the same way as when starting with solid products.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (148, N'MM', N'Millimetre', N'Millimetre', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (149, N'MMK', N'Square Millimetre', N'Square Millimetre: an area of a square whose sides are exactly 1 millimetre in length.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (150, N'MMQ', N'Cubic Millimetre', N'Cubic Millimetre: a cubic millimetre is the volume of a cube of side length one millimetre (0.001 m).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (151, N'MON', N'Month', N'Month: a unit of time equal to 1/12 of a year of 365,25 days.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (152, N'MP', N'Metric Ton', N'Metric Ton', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (153, N'MR', N'Metre', N'Metre: The metre is the basic unit of length in the International System of Units (SI).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (154, N'MTC', N'Mother Tincture', N'Mother Tincture: A count of a dry crude medicinal substance. Mother tincture when used for homeopathic preparations are liquid preparations obtained by the solvent action of a suitable vehicle upon raw materials. The raw materials are usually in the fresh form but may be dried. Mother tinctures for homeopathic preparations may also be obtained from plant juices, with, or without the addition of a vehicle.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (155, N'MWH', N'Megawatt Hour', N'Megawatt hour (1000 kW.h): A unit of energy defining the total amount of bulk energy transferred or consumed.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (156, N'MX', N'Mod Pallet (Mixed)', N'Mod Pallet (Mixed)', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (157, N'NGM', N'Nanogram', N'Nanogram: one billionth (1/1,000,000,000) of a gram.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (158, N'NIU', N'Number of International Unit', N'Number of International Unit: A unit of count defining the number of international units. The International Unit is a unit of measurement for the amount of a substance, based on measured biological activity or effect. The unit is used for vitamins, hormones, some medications, vaccines, blood products, and similar biologically active substances.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (159, N'NT', N'Trailer', N'Trailer', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (160, N'ON', N'Ounces Per Square Yard', N'Ounces per square yard : The weight of one square yard of the material expressed in ounces. Commonly used to express the density or weight of all types of paper, paperboard, and fabric, e.g. 20 OZ or 20 Weight denim has an area density of 20 oz/yd2. The term density here is used somewhat incorrectly, as density is mass by volume. More precisely, it is a measure of the area density, areal density, or surface density.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (161, N'OZ', N'Ounce', N'Ounce: A unit of mass with several definitions, the most commonly used of which are equal to approximately 30 grams.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (162, N'P1', N'Percent', N'Percent', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (163, N'PA', N'Pail', N'Pail', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (164, N'PAL', N'Pascal', N'Pascal: The pascal (symbol: Pa) is the SI derived unit of pressure, stress, Young''s modulus and tensile strength. It is a measure of force per unit area, defined as one newton per square metre.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (165, N'PC', N'Piece', N'Piece: A unit of count defining the number of pieces (piece: a single item, article or exemplar).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (166, N'PD', N'Pad', N'Pad: A unit of count defining the number of pads (pad: block of paper sheets fastened together at one end).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (167, N'PE', N'Pounds Equivalent', N'Pounds Equivalent', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (168, N'PFU', N'Plaque Forming unit(s)', N'Plaque Forming unit(s)', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (169, N'PG', N'Pound Gross', N'Pound Gross', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (170, N'PH', N'Pack', N'Pack', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (171, N'PK', N'Package', N'Package', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (172, N'PL', N'Pallet', N'Pallet', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (173, N'PN', N'Pounds Net', N'Pounds Net', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (174, N'PNT', N'Point', N'Point: A single unit on a scale of measurement as part of an incentive program or pricing structure used as a means of making a quantitative evaluation.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (175, N'PPC', N'Pixels Per Centimetre', N'Pixels Per Centimetre: A unit of count defining the number of pixels per linear centimetre as a measurement of the resolution of devices in various contexts; typically computer displays, image scanners or digital camera image sensors.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (176, N'PPI', N'Pixels Per Inch', N'Pixels Per Inch: A unit of count defining the number of pixels per linear inch (PPI) as a measurement of the resolution of devices in various contexts; typically computer displays, image scanners or digital camera image sensors.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (177, N'PR', N'Pair', N'Pair: A unit of count defining the number of pairs (pair: item described by two''s).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (178, N'PS', N'Pounds force per square inch', N'Pounds force per square inch: The pound-force per square inch (symbol: psi or lbf/in2 or lbf/in2) is a unit of pressure or of stress based on avoirdupois units. It is the pressure resulting from a force of one pound-force applied to an area of one square inch. Other abbreviations are used that append a modifier to "psi". However, the US National Institute of Standards and Technology recommends that, to avoid confusion, any modifiers be instead applied to the quantity being measured rather than the unit of measure[1] For example, "Pg = 100 psi" rather than "P = 100 psig.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (179, N'PT', N'Pint', N'Pint', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (180, N'PTD', N'United States Dry Pint', N'The United States dry pint is equal one eighth of a US dry gallon or one half US dry quarts. It is used in the United States but is not as common as the liquid pint.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (181, N'PTN', N'Portion', N'Portion', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (182, N'PY', N'Peck, Dry U.S.', N'Peck, Dry U.S.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (183, N'QB', N'Page - hardcopy', N'Page - hardcopy: A unit of count defining the number of hardcopy pages (hardcopy page: a page rendered as printed or written output on paper, film, or other permanent medium).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (184, N'QS', N'Quart Dry', N'Quart Dry: A US dry quart is equal to 1/32 of a US bushel, exactly 1.101220942715 litres.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (185, N'QT', N'Quart', N'Quart', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (186, N'RL', N'Roll', N'Roll', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (187, N'SEC', N'Second', N'Second: a unit of time equal to 1/60th of a minute.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (188, N'SF', N'Square Foot', N'Square Foot: an area of a square whose sides are exactly 1 foot in length.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (189, N'SH', N'Sheet', N'Sheet', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (190, N'SM', N'Square Metre', N'Square Metre: A square metre is an area of a square whose sides are exactly 1 metre in length.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (191, N'SMI', N'Mile (statute mile)', N'Mile (statute mile): A statute mile of 5,280 feet (exactly 1,609.344 meters).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (192, N'SPS', N'Sample Per Second', N'Sample Per Second', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (193, N'SQE', N'SQ-E: Number of allergens based on the SQ-E unit.', N'SQ-E: Number of allergens based on the SQ-E unit.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (194, N'ST', N'Set', N'Set: A unit of count defining the number of sets (set: a number of objects grouped together).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (195, N'SX', N'Shipment', N'Shipment: A unit of count defining the number of shipments (shipment: an amount of goods shipped or transported).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (196, N'SY', N'Square Yard', N'Square Yard: the area of a square with sides of one yard (three feet, thirty-six inches, 0.9144 metres) in length.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (197, N'TE', N'Tote', N'Tote', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (198, N'TK', N'Tank', N'Tank', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (199, N'TM', N'Thousand Feet', N'Thousand Feet', 1)
GO
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (200, N'TY', N'Tray', N'Tray', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (201, N'U2', N'Tablet', N'Tablet: A unit of count defining the number of tablets (tablet: a small flat or compressed solid object).', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (202, N'UN', N'Unit', N'Unit', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (203, N'UY', N'Fifty Square Feet', N'Fifty Square Feet', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (204, N'UZ', N'Fifty Count', N'Fifty Count', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (205, N'V2', N'Pouch', N'Pouch', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (206, N'WEE', N'Week', N'Week: a unit of time equal to a seven day period.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (207, N'WTT', N'Watts', N'Watts: a unit of power. One watt is equal to a power rate of one joule of work per second of time.', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (208, N'YD', N'Yard', N'Yard:', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (209, N'Z51', N'Application', N'Application (e.g. in hair colorant, 6 applications)', 1)
INSERT [dbo].[GDSNUnitOfMeasureCodeList] ([Codigo], [CodeValue], [Name], [Definition], [Status]) VALUES (210, N'Z52', N'Usage', N'Usage (e.g. in laundry, 24 usage)', 1)




INSERT INTO ImportacaoTipoItem
SELECT 1,'GTIN-12 (Itens Comerciais USA/CANADA)'
UNION SELECT 2,'GTIN-13 (Itens Comerciais)'
UNION SELECT 3,'GTIN-14 (Itens Logísticos)'
UNION SELECT 5,'GLN (Localizações Físicas)'
GO


INSERT INTO ImportacaoTipo
SELECT 1,'Template'
UNION SELECT 2,'Tela'
UNION SELECT 3,'Serviço'
GO

INSERT INTO ImportacaoStatusMotivos
SELECT 1,'Itens validados. Pronto para inicar a importação.'
UNION SELECT 2,'Não foi possível gravar o GTIN/GLN porque o GTIN já existe.'
UNION SELECT 3,'Não foi possível gravar o GTIN/GLN porque o digito verificar esta incorreto.'
UNION SELECT 4,'Não foi possível gravar o GTIN/GLN porque o prefixo GS1 da empresa informado não confere.'
UNION SELECT 5,'Não foi possível gravar o GTIN/GLN porque a empresa esta com pendencias financeiras.'
UNION SELECT 6,'Não foi possível gravar o GTIN/GLN porque a licença não esta ativa.'
UNION SELECT 7,'Não foi possível autenticar o acesso.'
GO

INSERT INTO ImportacaoModeloTipo
SELECT 1,'Excel'
UNION SELECT 2,'Txt'
GO

INSERT INTO ImportacaoModelos
SELECT 2, 1, 1, '2015-08-10', 'GTIN-12.xlsx', 'GTIN-12.xls'
UNION SELECT 3, 1, 2, '2015-08-10', 'GTIN-13.xlsx', 'GTIN-13.xls'
UNION SELECT 4, 1, 3, '2015-08-10', 'GTIN-14.xlsx', 'GTIN-14.xls'
UNION SELECT 5, 1, 5, '2015-08-10', 'GLN.xlsx', 'GLN.xls'
UNION SELECT 7, 2, 1, '2015-08-10', 'GTIN-12.csv', 'GTIN-12.txt'
UNION SELECT 8, 2, 2, '2015-08-10', 'GTIN-13.csv', 'GTIN-13.txt'
UNION SELECT 9, 2, 3, '2015-08-10', 'GTIN-14.csv', 'GTIN-14.txt'
UNION SELECT 10, 2, 5, '2015-08-10', 'GLN.csv', 'GLN.txt'
GO


INSERT INTO StatusPrefixo
SELECT 1,'Ativo','Ativo',1
UNION SELECT 2,'Inativo','Inativo',1
UNION SELECT 3,'Cancelado','Cancelado',1
UNION SELECT 4,'Transferido','Transferido',1
UNION SELECT 5,'Em Cadastro','Em Cadastro',1
UNION SELECT 6,'Em Cancelamento','Em Cancelamento',1
UNION SELECT 7,'Em Reativação','Em Reativação',1
UNION SELECT 8,'Reativado','Reativado',1
GO

INSERT INTO PAPELGLN
SELECT 'GLN Informativo','GLN Informativo',1,GETDATE(),1
GO



INSERT INTO [dbo].[BarCodeTypeListMagnification]
           ([MagnificationFactor]
           ,[IdealModuleWidth]
           ,[Width]
           ,[Height]
           ,[LeftGuard]
           ,[RightGuard]
           ,[CodigoBarCodeTypeList])
    
SELECT       0.80,0.264,29.83,18.28,11,7,2
UNION SELECT 0.85,0.281,31.70,19.42,11,7,2
UNION SELECT 0.90,0.297,33.56,20.57,11,7,2
UNION SELECT 0.95,0.313,35.43,21.71,11,7,2
UNION SELECT 1.00,0.330,37.29,22.85,11,7,2
UNION SELECT 1.05,0.346,39.15,23.99,11,7,2
UNION SELECT 1.10,0.363,41.02,25.14,11,7,2
UNION SELECT 1.15,0.379,42.88,26.28,11,7,2
UNION SELECT 1.20,0.396,44.75,27.42,11,7,2
UNION SELECT 1.25,0.412,46.61,28.56,11,7,2
UNION SELECT 1.30,0.429,48.48,29.71,11,7,2
UNION SELECT 1.35,0.445,50.34,30.85,11,7,2
UNION SELECT 1.40,0.462,52.21,31.99,11,7,2
UNION SELECT 1.45,0.478,54.07,33.13,11,7,2
UNION SELECT 1.50,0.495,55.94,34.28,11,7,2
UNION SELECT 1.55,0.511,57.80,35.42,11,7,2
UNION SELECT 1.60,0.528,59.66,36.56,11,7,2
UNION SELECT 1.65,0.544,61.53,37.70,11,7,2
UNION SELECT 1.70,0.561,63.39,38.85,11,7,2
UNION SELECT 1.75,0.577,65.26,39.99,11,7,2
UNION SELECT 1.80,0.594,67.12,41.13,11,7,2
UNION SELECT 1.85,0.610,68.99,42.27,11,7,2
UNION SELECT 1.90,0.627,70.85,43.42,11,7,2
UNION SELECT 1.95,0.643,72.72,44.56,11,7,2
UNION SELECT 2.00,0.660,74.58,45.70,11,7,2

UNION SELECT 0.80,0.264,29.83,18.28,11,7,26
UNION SELECT 0.85,0.281,31.70,19.42,11,7,26
UNION SELECT 0.90,0.297,33.56,20.57,11,7,26
UNION SELECT 0.95,0.313,35.43,21.71,11,7,26
UNION SELECT 1.00,0.330,37.29,22.85,11,7,26
UNION SELECT 1.05,0.346,39.15,23.99,11,7,26
UNION SELECT 1.10,0.363,41.02,25.14,11,7,26
UNION SELECT 1.15,0.379,42.88,26.28,11,7,26
UNION SELECT 1.20,0.396,44.75,27.42,11,7,26
UNION SELECT 1.25,0.412,46.61,28.56,11,7,26
UNION SELECT 1.30,0.429,48.48,29.71,11,7,26
UNION SELECT 1.35,0.445,50.34,30.85,11,7,26
UNION SELECT 1.40,0.462,52.21,31.99,11,7,26
UNION SELECT 1.45,0.478,54.07,33.13,11,7,26
UNION SELECT 1.50,0.495,55.94,34.28,11,7,26
UNION SELECT 1.55,0.511,57.80,35.42,11,7,26
UNION SELECT 1.60,0.528,59.66,36.56,11,7,26
UNION SELECT 1.65,0.544,61.53,37.70,11,7,26
UNION SELECT 1.70,0.561,63.39,38.85,11,7,26
UNION SELECT 1.75,0.577,65.26,39.99,11,7,26
UNION SELECT 1.80,0.594,67.12,41.13,11,7,26
UNION SELECT 1.85,0.610,68.99,42.27,11,7,26
UNION SELECT 1.90,0.627,70.85,43.42,11,7,26
UNION SELECT 1.95,0.643,72.72,44.56,11,7,26
UNION SELECT 2.00,0.660,74.58,45.70,11,7,26

UNION SELECT 0.80,0.264,21.38,14.58,9,9,6
UNION SELECT 0.85,0.281,22.72,15.50,9,9,6
UNION SELECT 0.90,0.297,24.06,16.41,9,9,6
UNION SELECT 0.95,0.313,25.39,17.32,9,9,6
UNION SELECT 1.00,0.330,26.73,18.23,9,9,6
UNION SELECT 1.05,0.346,28.07,19.14,9,9,6
UNION SELECT 1.10,0.363,29.40,20.05,9,9,6
UNION SELECT 1.15,0.379,30.74,20.96,9,9,6
UNION SELECT 1.20,0.396,32.08,21.88,9,9,6
UNION SELECT 1.25,0.412,33.41,22.79,9,9,6
UNION SELECT 1.30,0.429,34.75,23.70,9,9,6
UNION SELECT 1.35,0.445,36.09,24.61,9,9,6
UNION SELECT 1.40,0.462,37.42,25.52,9,9,6
UNION SELECT 1.45,0.478,38.76,26.43,9,9,6
UNION SELECT 1.50,0.495,40.10,27.35,9,9,6
UNION SELECT 1.55,0.511,41.43,28.26,9,9,6
UNION SELECT 1.60,0.528,42.77,29.17,9,9,6
UNION SELECT 1.65,0.544,44.10,30.08,9,9,6
UNION SELECT 1.70,0.561,45.44,30.99,9,9,6
UNION SELECT 1.75,0.577,46.78,31.90,9,9,6
UNION SELECT 1.80,0.594,48.11,32.81,9,9,6
UNION SELECT 1.85,0.610,49.45,33.73,9,9,6
UNION SELECT 1.90,0.627,50.79,34.64,9,9,6
UNION SELECT 1.95,0.643,52.12,35.55,9,9,6
UNION SELECT 2.00,0.660,53.46,36.46,9,9,6

UNION SELECT 0.625,0.635,89.2,32,10,10,24
UNION SELECT 0.7,0.711,99.9,32,10,10,24
UNION SELECT 0.80,0.813,114.2,32,10,10,24
UNION SELECT 0.90,914,128.4,32,10,10,24
UNION SELECT 1.00,1.016,142.7,32,10,10,24
 
GO


INSERT INTO ModeloEtiquetaBarCodeTypeListMagnification (CodigoModeloEtiqueta, CodigoBarCodeTypeList, CodigoBarCodeTypeListMagnification)
 
SELECT 1,2,4
UNION SELECT 2,2,4
UNION SELECT 3,2,4
UNION SELECT 4,2,4
UNION SELECT 5,2,4
UNION SELECT 6,2,4
UNION SELECT 7,2,4
UNION SELECT 8,2,4
UNION SELECT 9,2,4
UNION SELECT 10,2,4

UNION SELECT 1,6,3
UNION SELECT 2,6,3
UNION SELECT 3,6,3
UNION SELECT 4,6,3
UNION SELECT 5,6,3
UNION SELECT 6,6,3
UNION SELECT 7,6,3
UNION SELECT 8,6,3
UNION SELECT 9,6,3
UNION SELECT 10,6,3

UNION SELECT 1,26,5
UNION SELECT 2,26,5
UNION SELECT 3,26,5
UNION SELECT 4,26,5
UNION SELECT 5,26,5
UNION SELECT 6,26,5
UNION SELECT 7,26,5
UNION SELECT 8,26,5
UNION SELECT 9,26,5
UNION SELECT 10,26,5

UNION SELECT 1,24,1
UNION SELECT 2,24,1
UNION SELECT 3,24,1
UNION SELECT 4,24,1
UNION SELECT 5,24,1
UNION SELECT 6,24,1
UNION SELECT 7,24,1
UNION SELECT 8,24,1
UNION SELECT 9,24,1
UNION SELECT 10,24,1

