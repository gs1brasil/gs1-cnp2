begin tran 
--rollback
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------				CARGAS ENTREGA 2 (v2)				----------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--Aceite Termo Ades�o
INSERT INTO Campo (Codigo, Nome, Status)
SELECT 896, 'MENSAGEM_TermoAdesaoAceito', 1
UNION SELECT 897, 'MENSAGEM_NaoExisteTermoEmUso', 1
UNION SELECT 898, 'ABA_Telas', 1
UNION SELECT 899, 'ABA_Menu', 1
UNION SELECT 900, 'LBL_StatusIdioma', 1
UNION SELECT 901, 'LBL_IdiomaBase', 1
UNION SELECT 902, 'LBL_IdiomaNovo', 1
UNION SELECT 903, 'LBL_TitleGestaoIdiomas', 1
UNION SELECT 904, 'LBL_EditandoCampo', 1
UNION SELECT 905, 'LBL_Traducao', 1
UNION SELECT 906, 'LBL_Comentario', 1
UNION SELECT 907, 'GRID_CriadoAlteradoPor', 1
UNION SELECT 908, 'GRID_DataHoraAlteracao', 1
UNION SELECT 909, 'GRID_Traducao', 1
UNION SELECT 910, 'GRID_Comentario', 1
UNION SELECT 911, 'LBL_AlterarIdioma', 1
UNION SELECT 912, 'LBL_CadastrarIdioma', 1
UNION SELECT 913, 'LBL_PesquisarIdioma', 1
UNION SELECT 914, 'GRID_Liberacao', 1
UNION SELECT 915, 'LBL_Certificacao', 1
UNION SELECT 916, 'LBL_DataLiberacao', 1
UNION SELECT 917, 'LBL_QuantidadeCertificacoesDisponiveis', 1
UNION SELECT 918, 'LBL_TotalQuantidadeCertificacoesDisponiveis', 1
UNION SELECT 919, 'MENSAGEM_GravarLocalizacaoFisica', 1
UNION SELECT 920, 'LBL_Telefone', 1
UNION SELECT 921, 'LBL_CodigoPostal', 1

--Localiza��es f�sicas
UNION SELECT 922, 'MENSAGEM_AssociadoSemLicencaParaCadastrarLocalizacaoFisica', 1
UNION SELECT 923, 'MENSAGEM_AtravesDoNossoChatNoPortal', 1
UNION SELECT 924, 'MENSAGEM_AssociadoSemLicencaApropriadaParaCadastrarLocalizacao', 1
UNION SELECT 925, 'MENSAGEM_QuantidadeMaximaProdutoPrefixo', 1
UNION SELECT 926, 'MENSAGEM_Excedida', 1

--Ajudas
UNION SELECT 927, 'LBL_GerarPagina', 1
UNION SELECT 928, 'LBL_ConfiguracoesGerarPaginaAjuda', 1
UNION SELECT 929, 'LBL_ConteudoHtmlPagina', 1
UNION SELECT 930, 'LBL_GestaoAjudas', 1
GO

----INSERT INTO Formulario (Codigo, Nome, Descricao, Status, CodigoModulo, Url, Icone, Ordem, IndicadorMenu)
----SELECT 52, 'Aceite Termo de Ades�o', 'Aceite Termo de Ades�o', 1, 5, '/backoffice/aceiteTermoAdesao', NULL, NULL, 0
----GO

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Termo de ades�o aceito com sucesso.', 'Termo de ades�o aceito com sucesso.', 1, 896, 1
UNION SELECT 'N�o existe termo de ades�o em uso no sistema.', 'N�o existe termo de ades�o em uso no sistema.', 1, 897, 1

--Idiomas
UNION SELECT 'Telas', 'Telas', 1, 898, 1
UNION SELECT 'Menu', 'Menu', 1, 899, 1
UNION SELECT 'Status Idioma', 'Status Idioma', 1, 900, 1
UNION SELECT 'Idioma Base', 'Idioma Base', 1, 901, 1
UNION SELECT 'Idioma Novo', 'Idioma Novo', 1, 902, 1
UNION SELECT 'Gest�o de Idiomas', 'Gest�o de Idiomas', 1, 903, 1
UNION SELECT 'Editando o campo', 'Editando o campo', 1, 904, 1
UNION SELECT 'Tradu��o', 'Tradu��o', 1, 905, 1
UNION SELECT 'Coment�rio', 'Coment�rio', 1, 906, 1
UNION SELECT 'Criado/Alterado Por', 'Criado/Alterado Por', 1, 907, 1
UNION SELECT 'Data/Hora de Altera��o', 'Data/Hora de Altera��o', 1, 908, 1
UNION SELECT 'Tradu��o', 'Tradu��o', 1, 909, 1
UNION SELECT 'Coment�rio', 'Coment�rio', 1, 910, 1
UNION SELECT 'Alterar Idioma', 'Alterar Idioma', 1, 911, 1
UNION SELECT 'Cadastrar Idioma', 'Cadastrar Idioma', 1, 912, 1
UNION SELECT 'Pesquisar Idioma', 'Pesquisar Idioma', 1, 913, 1

--Certifica��o de Produtos
UNION SELECT 'Libera��o', 'Libera��o', 1, 914, 1
UNION SELECT 'Certifica��o', 'Certifica��o', 1, 915, 1
UNION SELECT 'Data de Libera��o', 'Selecionar a data de Libera��o.', 1, 916, 1
UNION SELECT 'Quantidade de Certifica��es Dispon�veis', 'Quantidade de Certifica��es Dispon�veis', 1, 917, 1
UNION SELECT 'Total da Quantidade de Certifica��es Dispon�veis', 'Total da Quantidade de Certifica��es Dispon�veis', 1, 918, 1

--Localiza��o F�sica
UNION SELECT 'Deseja gravar as informa��es da localiza��o f�sica? Ap�s confirma��o, n�o ser� poss�vel alterar as informa��es.', 'Deseja gravar as informa��es da localiza��o f�sica? Ap�s confirma��o, n�o ser� poss�vel alterar as informa��es.', 1, 919, 1 
UNION SELECT 'Telefone', 'Telefone', 1, 920, 1
UNION SELECT 'C�digo Postal', 'C�digo Postal', 1, 921, 1
UNION SELECT 'Este associado n�o possui licen�a apropriada para cadastrar localiza��es f�sicas. Por favor entre em contato com o atendimento GS1 Brasil: Tel.', 'Este associado n�o possui licen�a apropriada para cadastrar localiza��es f�sicas. Por favor entre em contato com o atendimento GS1 Brasil: Tel.', 1, 922, 1
UNION SELECT 'ou atrav�s do nosso chat no portal www.gs1br.org.', 'ou atrav�s do nosso chat no portal www.gs1br.org.', 1, 923, 1
UNION SELECT 'Este associado n�o possui licen�a apropriada para cadastrar localiza��es f�sicas. Por favor entre em contato com o atendimento GS1 Brasil por telefone ou atrav�s do nosso chat no portal www.gs1br.org.', 'Este associado n�o possui licen�a apropriada para cadastrar localiza��es f�sicas. Por favor entre em contato com o atendimento GS1 Brasil por telefone ou atrav�s do nosso chat no portal www.gs1br.org.', 1, 924, 1
UNION SELECT 'Quantidade de produtos m�xima para o prefixo', 'Quantidade de produtos m�xima para o prefixo', 1, 925, 1
UNION SELECT 'Excedida', 'Excedida', 1, 927, 1

--Ajudas
UNION SELECT 'Gerar P�gina', 'Gerar P�gina', 1, 927, 1
UNION SELECT 'Selecione as configura��es para gerar a sua p�gina de ajuda', 'Selecione as configura��es para gerar a sua p�gina de ajuda', 1, 928, 1
UNION SELECT 'Conte�do HTML da P�gina', 'Conte�do HTML da P�gina', 1, 929, 1
UNION SELECT 'Gest�o de Ajudas', 'Gest�o de Ajudas', 1, 930, 1
GO


--select * from FormularioCampo where CodigoFormulario = 45

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
--SELECT 2, 363
--UNION 
--SELECT 6, 363
--UNION SELECT 8, 363
--UNION SELECT 12, 363
--UNION SELECT 13, 363
--UNION SELECT 18, 363
--UNION SELECT 50, 363
--UNION
 SELECT 52, 12
UNION SELECT 52, 24
UNION SELECT 52, 25
UNION SELECT 52, 50
UNION SELECT 52, 164
UNION SELECT 52, 482
UNION SELECT 52, 776
UNION SELECT 52, 896
UNION SELECT 52, 897
--UNION SELECT 45, 70
UNION SELECT 45, 598
UNION SELECT 45, 914
UNION SELECT 45, 915
UNION SELECT 45, 916
UNION SELECT 45, 917
UNION SELECT 45, 918

--Idiomas
UNION SELECT 49, 1
UNION SELECT 49, 2
UNION SELECT 49, 3
UNION SELECT 49, 10
UNION SELECT 49, 26
UNION SELECT 49, 30
UNION SELECT 49, 90
UNION SELECT 49, 97
UNION SELECT 49, 104
UNION SELECT 49, 105
UNION SELECT 49, 107
UNION SELECT 49, 108
UNION SELECT 49, 166
UNION SELECT 49, 360
UNION SELECT 49, 361
UNION SELECT 49, 362
UNION SELECT 49, 363
UNION SELECT 49, 403
UNION SELECT 49, 408
UNION SELECT 49, 519
UNION SELECT 49, 520
UNION SELECT 49, 898
UNION SELECT 49, 899
UNION SELECT 49, 900
UNION SELECT 49, 901
UNION SELECT 49, 902
UNION SELECT 49, 903
UNION SELECT 49, 904
UNION SELECT 49, 905
UNION SELECT 49, 906
UNION SELECT 49, 907
UNION SELECT 49, 908
UNION SELECT 49, 909
UNION SELECT 49, 910
UNION SELECT 49, 911
UNION SELECT 49, 912
UNION SELECT 49, 913

--Localiza��o F�sica
UNION SELECT 14, 179
UNION SELECT 14, 543
UNION SELECT 14, 544
UNION SELECT 14, 919
UNION SELECT 14, 920
UNION SELECT 14, 921
UNION SELECT 14, 922
UNION SELECT 14, 923
UNION SELECT 14, 924
UNION SELECT 14, 925
UNION SELECT 14, 926

--Ajudas
UNION SELECT 39, 1
UNION SELECT 39, 2
UNION SELECT 39, 26
UNION SELECT 39, 26
UNION SELECT 39, 30
UNION SELECT 39, 31
UNION SELECT 39, 35
UNION SELECT 39, 97
UNION SELECT 39, 166
UNION SELECT 39, 308
UNION SELECT 39, 363
UNION SELECT 39, 369
UNION SELECT 39, 403
UNION SELECT 39, 404
UNION SELECT 39, 508
UNION SELECT 39, 514
UNION SELECT 39, 517
UNION SELECT 39, 519
UNION SELECT 39, 520
UNION SELECT 39, 521
UNION SELECT 39, 522
UNION SELECT 39, 524
UNION SELECT 39, 528
UNION SELECT 39, 529
UNION SELECT 39, 530
UNION SELECT 39, 793
UNION SELECT 39, 927
UNION SELECT 39, 928
UNION SELECT 39, 929
UNION SELECT 39, 930
GO

--Altera��es de Certifica��o de Produtos
UPDATE FORMULARIO SET NOME = 'Certifica��o de Produtos', DESCRICAO = 'Certifica��o de Produtos - C�digo de Barras/Pesos e Medidas', URL = '/backoffice/certificacaoProdutos' 
WHERE CODIGO = 45
GO

UPDATE FUNCIONALIDADE SET NOME = 'PesquisarCertificacao', DESCRICAO = 'Acessar Certifica��o de Produtos' WHERE CODIGO = 102
UPDATE FUNCIONALIDADE SET NOME = 'CadastrarCertificacao', DESCRICAO = 'Cadastrar Certifica��o de Produtos' WHERE CODIGO = 103
UPDATE FUNCIONALIDADE SET NOME = 'EditarCertificacao', DESCRICAO = 'Editar Certifica��o de Produtos' WHERE CODIGO = 104
UPDATE FUNCIONALIDADE SET NOME = 'ExcluirCertificacao', DESCRICAO = 'Excluir Certifica��o de Produtos' WHERE CODIGO = 105
GO

UPDATE CAMPO SET NOME = 'LBL_TitleCodigoBarrasPesosMedidas' WHERE CODIGO = 532
GO

UPDATE FORMULARIOIDIOMA SET NOME = 'Certifica��o de Produto' where NOME = 'C�digo de Barras' AND CODIGOIDIOMA = 1
GO

--Deletar permiss�es da tela de Pesos e Medidas. Essa tela agora est� junto com a tela de Certifica��o
--de produtos c�digo de barras
DELETE FROM FORMULARIOAJUDAPASSOAPASSOHISTORICO WHERE CODIGOFORMULARIO = 46
DELETE FROM FORMULARIOAJUDAPASSOAPASSO WHERE CODIGOFORMULARIO = 46
DELETE FROM FORMULARIOCAMPO WHERE CODIGOFORMULARIO = 46
DELETE FROM FORMULARIOIDIOMA WHERE CODIGOFORMULARIO = 46
DELETE FROM FORMULARIOAJUDA WHERE CODIGOFORMULARIO = 46
DELETE FROM PERFILFUNCIONALIDADE WHERE CODIGOFUNCIONALIDADE IN (106, 107, 108, 109)
DELETE FROM PERFILFUNCIONALIDADE WHERE CODIGOFUNCIONALIDADE IN (103, 104, 105)
DELETE FROM FUNCIONALIDADE WHERE CODIGOFORMULARIO = 46
DELETE FROM FORMULARIO WHERE CODIGO = 46
DELETE FROM FUNCIONALIDADE WHERE CODIGO IN (103, 104, 105)
GO

--Funcionalidade de Visualiza��o do Sistema
--GS1
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (123, N'VisualizarParametro', N'Visualizar Par�metros', 1, 1, 2)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (124, N'VisualizarAgencia', N'Visualizar Ag�ncia', 1, 1, 3)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (125, N'VisualizarPapelGLN', N'Visualizar Papel GLN', 1, 1, 4)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (126, N'VisualizarTipoProduto', N'Visualizar Tipo Produto', 1, 1, 7)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (127, N'VisualizarGPC', N'Visualizar GPC', 1, 1, 8)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (128, N'VisualizarUsuario', N'Visualizar Usu�rio', 1, 1, 10)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (129, N'VisualizarDadosAssociado', N'Visualizar Dados do Associado', 1, 1, 11)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (130, N'VisualizarLocalizacaoFisica', N'Visualizar Localiza��o F�sica', 1, 1, 14)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (131, N'VisualizarProduto', N'Visualizar Produto', 1, 1, 15)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (132, N'VisualizarEtiqueta', N'Visualizar Etiquetas', 1, 1, 17)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (133, N'VisualizarImportarProduto', N'Visualizar Importa��o de Produto', 1, 1, 18)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (134, N'VisualizarAjuda', N'Visualizar Ajuda WYSIWYG', 1, 1, 39)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (135, N'VisualizarFAQ', N'Visualizar FAQ', 1, 1, 40)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (136, N'VisualizarPesquisaSatisfacao', N'Visualizar Pesquisa de Satisfa��o', 1, 1, 42)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (137, N'VisualizarAjudaPassoaPasso', N'Visualizar Ajuda Passo a Passo', 1, 1, 44)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (138, N'VisualizarUploadCartas', N'Visualizar Upload de Cartas', 1, 1, 5)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (139, N'VisualizarLotes', N'Visualizar Lotes', 1, 1, 47)
--INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (140, N'VisualizarCartasCertificadasGs1', N'Visualizar Cartas Certificadas GS1', 1, 1, 48)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (141, N'VisualizarIdiomas', N'Visualizar Idiomas', 1, 1, 49)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (142, N'VisualizarBanner', N'Visualizar Banner', 1, 1, 50)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (149, N'VisualizarChave', N'Visualizar Chaves', 1, 1, 13)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (150, N'PesquisarCartasCertificadasGs1', N'Acessar Cartas Certificadas GS1', 1, 1, 48)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (151, N'GerarChave', N'Gerar Chave', 1, 1, 13)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (152, N'ExpirarChave', N'Expirar Chave', 1, 1, 13)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (153, N'EnviarChave', N'Enviar Chave por Email', 1, 1, 13)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (154, N'VisualizarEPCRFID', N'Visualizar EPC/RFID', 1, 1, 16)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (156, N'VisualizarPerfil', N'Visualizar Perfil', 1, 1, 1)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (157, N'PesquisarAjuda', N'Acessar Ajuda WYSIWYG', 1, 1, 39)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (158, N'PesquisarAjudaPassoaPasso', N'Acesso a Ajuda Passo a Passo', 1, 1, 44)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (159, N'PesquisarUploadCartas', N'Acesso a Upload de Templates de Cartas Certificadas', 1, 1, 5)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (160, N'CadastroTermosAdesao', N'Cadastro de Termos de Ades�o', 1, 1, 9)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (161, N'VisualizarTermosAdesao', N'Visualizar Termos de Ades�o', 1, 1, 9)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (162, N'EditarTermosAdesao', N'Editar Termos de Ades�o', 1, 1, 9)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (163, N'PesquisarChave', N'Acesso a Gest�o de Chaves', 1, 1, 13)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (164, N'PesquisarImportacaoProduto', N'Acesso a Importa��o de Produto', 1, 1, 18)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (165, N'CadastrarEPCRFID', N'Cadastrar EPC/RFID', 1, 1, 16)


--Associado
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (143, N'VisualizarUsuario', N'Visualizar Usu�rio', 1, 2, 10)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (144, N'VisualizarDadosAssociado', N'Visualizar Dados do Associado', 1, 2, 11)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (145, N'VisualizarLocalizacaoFisica', N'Visualizar Localiza��o F�sica', 1, 2, 14)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (146, N'VisualizarProduto', N'Visualizar Produto', 1, 2, 15)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (147, N'VisualizarEtiqueta', N'Visualizar Etiqueta', 1, 2, 17)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (148, N'VisualizarImportarProduto', N'Visualizar Importa��o de Produto', 1, 2, 18)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (155, N'VisualizarEPCRFID', N'Visualizar EPC/RFID', 1, 2, 16)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (166, N'CadastrarEPCRFID', N'Cadastrar EPC/RFID', 1, 2, 16)
INSERT [Funcionalidade] ([Codigo], [Nome], [Descricao], [Status], [CodigoTipoUsuario], [CodigoFormulario]) VALUES (167, N'PesquisarImportacaoProduto', N'Acesso a Importa��o de Produto', 1, 2, 18)
GO

INSERT INTO PERFIL VALUES ('GS1	Visualiza��o', 'Perfil do Administrador para Visualiza��o', 1, 1, GETDATE(), 1)
INSERT INTO PERFIL VALUES ('Associado Visualiza��o', 'Perfil do Moderador para Visualiza��o', 1, 1, GETDATE(), 1)
GO

UPDATE FUNCIONALIDADE SET DESCRICAO = 'Acessar Dados do Associado' WHERE NOME = 'DadosAssociado'
UPDATE FUNCIONALIDADE SET DESCRICAO = 'Gerar Cartas Certificadas GS1', NOME = 'GerarCartasCertificadasGs1' WHERE CODIGO = 115
UPDATE FUNCIONALIDADE SET DESCRICAO = 'Acessar Termos de Ades�o' WHERE CODIGO = 23
UPDATE FORMULARIO SET NOME = 'PesquisarChave' WHERE codigo = 30
GO

UPDATE FUNCIONALIDADE SET DESCRICAO = 'Acessar Par�metro' WHERE CODIGO = 5
GO

--Novas funcionalidades de perfil
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 150, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 151, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 152, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 153, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 157, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 158, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 159, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 160, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 162, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 163, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 164, GETDATE(), 1)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 165, GETDATE(), 1)


--INSERT INTO CAMPO
--SELECT 922, 'MENSAGEM_AssociadoSemLicencaParaCadastrarLocalizacaoFisica', 1
--UNION SELECT 923, 'MENSAGEM_AtravesDoNossoChatNoPortal', 1
--UNION SELECT 924, 'MENSAGEM_AssociadoSemLicencaApropriadaParaCadastrarLocalizacao', 1
--UNION SELECT 925, 'MENSAGEM_QuantidadeMaximaProdutoPrefixo', 1
--UNION SELECT 926, 'MENSAGEM_Excedida', 1

--Cartas Certificadas Associado
UNION SELECT 927, 'MENSAGEM_EmpresaNaoPossuiChaveAutenticacao', 1
GO

INSERT INTO CampoIdioma
SELECT 'Este associado n�o possui licen�a apropriada para cadastrar localiza��es f�sicas. Por favor entre em contato com o atendimento GS1 Brasil: Tel.', 'Este associado n�o possui licen�a apropriada para cadastrar localiza��es f�sicas. Por favor entre em contato com o atendimento GS1 Brasil: Tel.', 1, 922, 1
UNION SELECT 'ou atrav�s do nosso chat no portal www.gs1br.org.', 'ou atrav�s do nosso chat no portal www.gs1br.org.', 1, 923, 1
UNION SELECT 'Este associado n�o possui licen�a apropriada para cadastrar localiza��es f�sicas. Por favor entre em contato com o atendimento GS1 Brasil por telefone ou atrav�s do nosso chat no portal www.gs1br.org.', 'Este associado n�o possui licen�a apropriada para cadastrar localiza��es f�sicas. Por favor entre em contato com o atendimento GS1 Brasil por telefone ou atrav�s do nosso chat no portal www.gs1br.org.', 1, 924, 1
UNION SELECT 'Quantidade de produtos m�xima para o prefixo', 'Quantidade de produtos m�xima para o prefixo', 1, 925, 1
UNION SELECT 'Excedida', 'Excedida', 1, 926, 1

--Cartas Certificadas Associado
UNION SELECT 'A empresa n�o possui chave de autentica��o gerada. Por favor, entre em contato com a GS1 Brasil: Tel.', 'A empresa n�o possui chave de autentica��o gerada. Por favor, entre em contato com a GS1 Brasil: Tel.', 1, 927, 1
GO

UPDATE CAMPO SET Nome = 'LBL_Mes' WHERE CODIGO = 753
UPDATE CAMPOIDIOMA SET Texto = 'M�s', Comentario = 'M�s' WHERE CODIGOCAMPO = 753 and codigoidioma = 1
UPDATE CAMPOIDIOMA SET Texto = 'Ag�ncia Reguladora', Comentario = 'Ag�ncia Reguladora' WHERE CODIGOCAMPO = 685 and codigoidioma = 1
UPDATE CAMPOIDIOMA SET Texto = 'C�digo Reguladora', Comentario = 'C�digo Reguladora' WHERE CODIGOCAMPO = 686 and codigoidioma = 1
UPDATE CAMPO SET Nome = 'LBL_Mensal' WHERE CODIGO = 752
UPDATE CAMPOIDIOMA SET Texto = 'Mensal', Comentario = 'Mensal' WHERE CODIGOCAMPO = 752 and codigoidioma = 1


--commit

--Cartas Certificadas Associado
UPDATE CampoIdioma set Texto = 'ou atrav�s do nosso chat no portal www.gs1br.org e solicite a gera��o das chaves de autentica��o.', Comentario = 'ou atrav�s do nosso chat no portal www.gs1br.org e solicite a gera��o das chaves de autentica��o.' WHERE CodigoCampo = 428 AND CodigoIdioma = 1
GO

--Cartas Certificadas Associado
INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 12, 927
GO