﻿USE db_gs1CNP
GO

CREATE TABLE ImportacaoTipoItem
(
	Codigo INT NOT NULL,
	Descricao VARCHAR(500) NOT NULL,
	CONSTRAINT [PK_ImportacaoTipoItem] PRIMARY KEY CLUSTERED ([Codigo] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
);
GO

INSERT INTO ImportacaoTipoItem
SELECT 1,'GTIN-12 (Itens Comerciais USA/CANADA)'
UNION SELECT 2,'GTIN-13 (Itens Comerciais)'
UNION SELECT 3,'GTIN-14 (Itens Logísticos)'
UNION SELECT 5,'GLN (Localizações Físicas)'
GO

CREATE TABLE ImportacaoTipo
(
	Codigo INT NOT NULL,
	Descricao VARCHAR(500) NOT NULL,
	CONSTRAINT [PK_ImportacaoTipo] PRIMARY KEY CLUSTERED ([Codigo] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
);
GO

INSERT INTO ImportacaoTipo
SELECT 1,'Template'
UNION SELECT 2,'Tela'
UNION SELECT 3,'Serviço'
GO

CREATE TABLE ImportacaoStatusMotivos
(
	Codigo INT NOT NULL,
	Descricao VARCHAR(500) NOT NULL,
	CONSTRAINT [PK_ImportacaoStatusMotivos] PRIMARY KEY CLUSTERED ([Codigo] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
);
GO

INSERT INTO ImportacaoStatusMotivos
SELECT 1,'Itens validados. Pronto para inicar a importação.'
UNION SELECT 2,'Não foi possível gravar o GTIN/GLN porque o GTIN já existe.'
UNION SELECT 3,'Não foi possível gravar o GTIN/GLN porque o digito verificar esta incorreto.'
UNION SELECT 4,'Não foi possível gravar o GTIN/GLN porque o prefixo GS1 da empresa informado não confere.'
UNION SELECT 5,'Não foi possível gravar o GTIN/GLN porque a empresa esta com pendencias financeiras.'
UNION SELECT 6,'Não foi possível gravar o GTIN/GLN porque a licença não esta ativa.'
UNION SELECT 7,'Não foi possível autenticar o acesso.'
GO

CREATE TABLE ImportacaoModeloTipo
(
	Codigo INT NOT NULL,
	Descricao VARCHAR(500) NOT NULL,
	CONSTRAINT [PK_ImportaçãoModeloTipo] PRIMARY KEY CLUSTERED ([Codigo] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
);
GO

INSERT INTO ImportacaoModeloTipo
SELECT 1,'Excel'
UNION SELECT 2,'Txt'
GO

CREATE TABLE ImportacaoModelos
(
	Codigo INT NOT NULL,
	CodigoModeloTipo INT NOT NULL,
	CodigoImportacaoTipoItem INT NOT NULL,
	Data DATETIME NOT NULL,
	Arquivo VARCHAR(500) NOT NULL,
	Nome VARCHAR(500) NOT NULL,
	CONSTRAINT [PK_ImportacaoModelos] PRIMARY KEY CLUSTERED ([Codigo] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
);
GO

ALTER TABLE dbo.ImportacaoModelos  WITH CHECK ADD  CONSTRAINT [PK_ImportacaoModelos_ImportacaoModeloTipo] FOREIGN KEY(CodigoModeloTipo)
REFERENCES dbo.ImportacaoModeloTipo (Codigo);
GO

INSERT INTO ImportacaoModelos
SELECT 1, 1, 4, '2015-08-10', 'GTIN-8.xlsx', 'GTIN-8.xls'
UNION SELECT 2, 1, 1, '2015-08-10', 'GTIN-12.xlsx', 'GTIN-12.xls'
UNION SELECT 3, 1, 2, '2015-08-10', 'GTIN-13.xlsx', 'GTIN-13.xls'
UNION SELECT 4, 1, 3, '2015-08-10', 'GTIN-14.xlsx', 'GTIN-14.xls'
UNION SELECT 5, 1, 5, '2015-08-10', 'GLN.xlsx', 'GLN.xls'
UNION SELECT 6, 2, 4, '2015-08-10', 'GTIN-8.csv', 'GTIN-8.txt'
UNION SELECT 7, 2, 1, '2015-08-10', 'GTIN-12.csv', 'GTIN-12.txt'
UNION SELECT 8, 2, 2, '2015-08-10', 'GTIN-13.csv', 'GTIN-13.txt'
UNION SELECT 9, 2, 3, '2015-08-10', 'GTIN-14.csv', 'GTIN-14.txt'
UNION SELECT 10, 2, 5, '2015-08-10', 'GLN.csv', 'GLN.txt'

CREATE TABLE ImportacaoProdutos
(
	Codigo INT IDENTITY(1,1) NOT NULL,
	Arquivo VARCHAR(500) NULL,
	ArquivoRelatorio VARCHAR(500) NULL,
	Data DATETIME NOT NULL,
	ItensImportados INT NOT NULL,
	ItensNaoImportados INT NOT NULL,
	CodigoImportacaoResultado INT NOT NULL,
	CodigoImportacaoTipo INT NOT NULL,
	CodigoImportacaoTipoItem INT NOT NULL,
	TempoImportacao TIME NOT NULL,
	[Status] INT NOT NULL,
	CodigoUsuarioAlteracao BIGINT NOT NULL,
	CONSTRAINT [PK_ImportacaoProdutos] PRIMARY KEY CLUSTERED ([Codigo] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
);
GO

ALTER TABLE dbo.ImportacaoProdutos  WITH CHECK ADD  CONSTRAINT [PK_ImportacaoProdutos_ImportacaoResultado] FOREIGN KEY(CodigoImportacaoResultado)
REFERENCES dbo.ImportacaoResultado (Codigo);
GO

ALTER TABLE dbo.ImportacaoProdutos  WITH CHECK ADD  CONSTRAINT [PK_ImportacaoProdutos_ImportacaoTipo] FOREIGN KEY(CodigoImportacaoTipo)
REFERENCES dbo.ImportacaoTipo (Codigo);
GO

ALTER TABLE dbo.ImportacaoProdutos  WITH CHECK ADD  CONSTRAINT [PK_ImportacaoProdutos_ImportacaoTipoItem] FOREIGN KEY(CodigoImportacaoTipoItem)
REFERENCES dbo.ImportacaoTipoItem (Codigo);
GO

ALTER TABLE dbo.ImportacaoProdutos  WITH CHECK ADD  CONSTRAINT [PK_ImportacaoProdutos_Usuario] FOREIGN KEY(CodigoUsuarioAlteracao)
REFERENCES dbo.Usuario (Codigo);
GO