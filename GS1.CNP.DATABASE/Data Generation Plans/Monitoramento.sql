/****** Object:  Table [dbo].[FuncionalidadeRelatorio]    Script Date: 26/04/2016 14:56:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FuncionalidadeRelatorio](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](500) NOT NULL,
	[Descricao] [varchar](500) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_FuncionalidadeRelatorio] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FuncionalidadeRelatorioLog]    Script Date: 26/04/2016 14:57:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FuncionalidadeRelatorioLog](
	[Codigo] [int] NOT NULL,
	[CodigoFuncionalidadeRelatorio] [int] NOT NULL,
	[Nome] [varchar](500) NOT NULL,
	[Descricao] [varchar](500) NOT NULL,
	[NomeTela] [varchar](500) NOT NULL,
	[Classe] [varchar](255) NOT NULL,
	[Metodo] [varchar](500) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_FuncionalidadeRelatorioLog] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FuncionalidadeRelatorioLog]  WITH CHECK ADD  CONSTRAINT [PK_FuncionalidadeRelatorioLog_FuncionalidadeRelatorio] FOREIGN KEY([CodigoFuncionalidadeRelatorio])
REFERENCES [dbo].[FuncionalidadeRelatorio] ([Codigo])
GO

ALTER TABLE [dbo].[FuncionalidadeRelatorioLog] CHECK CONSTRAINT [PK_FuncionalidadeRelatorioLog_FuncionalidadeRelatorio]
GO

INSERT INTO Funcionalidaderelatorio VALUES (1,'Inclus�o','Incluis�o registro',1)
INSERT INTO Funcionalidaderelatorio VALUES (2,'Edi��o','Edi��o de registro',1)
INSERT INTO Funcionalidaderelatorio VALUES (3,'Exclus�o','Exclus�o de registro',1)
INSERT INTO Funcionalidaderelatorio VALUES (4,'Consulta','Consulta de registro',1)
INSERT INTO Funcionalidaderelatorio VALUES (5,'Outras funcionalidades','Outras funcionalidades',1)
INSERT INTO Funcionalidaderelatorio VALUES (6,'Relat�rios','Relat�rios',1)


INSERT INTO FuncionalidadeRelatorioLog VALUES (1,1,'Cadastrar Ag�ncia','Cadastrar Ag�ncia','Ag�ncia Reguladora','AgenciasReguladorasBO','CadastrarAgencia',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (2,1,'Cadastrar Etiqueta','Cadastrar Etiqueta','Gera��o de Etiquetas','EtiquetaBO','CadastrarEtiqueta',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (3,1,'Cadastrar Pesquisa de Satisfa�','Cadastrar Pesquisa de Satisfa�','Pesquisa de Satisfa��o','PesquisaSatisfacaoBO','CadastrarPesquisaSatisfacao',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (4,1,'Cadastrar Produto','Cadastrar Produto','Cadastro Produto','ProdutoBO','CadastrarProduto',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (5,1,'Cadastrar Termo de Ades�o','Cadastrar Termo de Ades�o','Termo de Ades�o','TermosAdesaoBO','CadastrarTermoAdesao',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (6,1,'Cadastrar Tipo de Produto','Cadastrar Tipo de Produto','Tipo de Produto','TipoProdutoBO','CadastrarTipoProduto',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (7,1,'Cadastrar Hist�rico EPC/RFID','Cadastrar Hist�rico EPC/RFID','Gera��o de EPC/RFID','EPCRFIDBO','InsereHistoricoEPCRFID',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (8,1,'Cadastrar Permiss�o','Cadastrar Permiss�o','Perfis de Acesso','PerfilBO','InserePermissao',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (9,1,'Cadastrar Banner','Cadastrar Banner','Banners','BannerBO','Insert',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (10,1,'Cadastrar Usu�rio','Cadastrar Usu�rio','Usu�rios','UsuarioBO','Insert',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (11,1,'Cadastrar Par�metro','Cadastrar Par�metro','Par�metros','ParametroBO','SalvarParametros',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (12,1,'Cadastrar Status do Log','Cadastrar Status do Log','Cadastro Produto','ProdutoBO','SalvarStatusLog',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (13,2,'Editar Template','Editar Template','Upload de Cartas','UploadCartasBO','AtualizarTemplate',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (14,2,'Editar Termo de Ades�o','Editar Termo de Ades�o','Termo de Ades�o','TermosAdesaoBO','AtualizarTermoAdesao',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (15,2,'Editar Tipo de Compartilhament','Editar Tipo de Compartilhament','Dados do Associado','DadosDoAssociadoBO','AtualizarTipoCompartilhamento',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (16,2,'Editar Produto','Editar Produto','Cadastro Produto','ProdutoBO','EditarProduto',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (17,2,'Editar Banner','Editar Banner','Banners','BannerBO','Update',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (18,2,'Editar Espa�o do Banner','Editar Espa�o do Banner','Banners','EspacoBannerBO','Update',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (19,2,'Editar Perfil','Editar Perfil','Perfis','PerfilBO','Update',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (20,2,'Editar Banner do Espa�o Banner','Editar Banner do Espa�o Banner','Banners','BannerBO','UpdateBannerEspacoBanner',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (21,3,'Excluir Pesquisa de Satisfa��o','Excluir Pesquisa de Satisfa��o','Pesquisa de Satisfa��o','PesquisaSatisfacaoBO','ExcluirPesquisaSatisfacao',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (22,3,'Excluir Ag�ncia','Excluir Ag�ncia','Ag�ncias Reguladoras','AgenciasReguladorasBO','RemoverAgencia',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (23,5,'Logar no Sistema','Logar no Sistema','Login','LoginBO','Logar',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (24,5,'Logar no Sistema','Logar no Sistema','Login','','Login',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (25,5,'Aceitar Termo de Ades�o','Aceitar Termo de Ades�o','Termo de Ades�o','TermosAdesaoBO','AceitarTermoAdesao',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (26,5,'Ativar Termo de Ades�o','Ativar Termo de Ades�o','Termo de Ades�o','TermosAdesaoBO','AtivarTermo',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (27,5,'Enviar Chave','Enviar Chave','Gest�o de Chaves','ChaveBO','EnviarChave',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (28,5,'Gerar Chave','Gerar Chave','Gest�o de Chaves','ChaveBO','GerarChave',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (29,5,'Importar Produtos','Importar Produtos','Importar Produtos','ImportarProdutosBO','Importar',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (30,5,'Ler Arquivo de Template','Ler Arquivo de Template','Importar Produtos','ImportarProdutosBO','LerArquivo',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (31,6,'Relat�rio Associado GS1','Relat�rio Associado GS1','Relat�rio Associado GS1','RelatorioAssociadoGS1BO','relatorioAssociadoGS1',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (32,6,'Relat�rio de Hist�rico de Stat','Relat�rio de Hist�rico de Stat','Relat�rio de Hist�rico de Stat','RelatorioHistoricoStatusProdut','relatorioHistoricoStatusProdut',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (33,6,'Relat�rio de Hist�rico de Stat','Relat�rio de Hist�rico de Stat','Relat�rio de Hist�rico de Stat','RelatorioHistoricoStatusProdut','relatorioHistoricoStatusProdut',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (34,6,'Relat�rio de Hist�rico de Uso ','Relat�rio de Hist�rico de Uso ','Relat�rio de Hist�rico de Uso ','RelatorioHistoricoUsoAssociado','relatorioHistoricoUsoAssociado',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (35,6,'Relat�rio de Impress�o','Relat�rio de Impress�o','Relat�rio de Impress�o','RelatorioImpressaoGS1BO','relatorioImpressaoGS1',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (36,6,'Relat�rio de Localiza��o F�sic','Relat�rio de Localiza��o F�sic','Relat�rio de Localiza��o F�sic','RelatorioLocalizacaoFisicaAsso','relatorioLocalizacaoFisicaAsso',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (37,6,'Relat�rio de Localiza��o F�sic','Relat�rio de Localiza��o F�sic','Relat�rio de Localiza��o F�sic','RelatorioLocalizacaoFisicaGS1B','relatorioLocalizacaoFisicaGS1',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (38,6,'Relat�rio de Login de Acesso -','Relat�rio de Login de Acesso -','Relat�rio de Login de Acesso -','RelatorioLoginAcessoAssociadoB','relatorioLoginAcessoAssociado',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (39,6,'Relat�rio de Login de Acesso -','Relat�rio de Login de Acesso -','Relat�rio de Login de Acesso -','RelatorioLoginAcessoGS1BO','relatorioLoginAcessoGS1',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (40,6,'Relat�rio Totalizador - CNP','Relat�rio Totalizador - CNP','Relat�rio Totalizador - CNP','RelatorioMensalCNPBO','relatorioMensalCNP',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (41,6,'Relat�rio de Produto - Associa','Relat�rio de Produto - Associa','Relat�rio de Produto - Associa','RelatorioProdutoAssociadoBO','relatorioProdutoAssociado',1)
INSERT INTO FuncionalidadeRelatorioLog VALUES (42,6,'Relat�rio de Produto - GS1','Relat�rio de Produto - GS1','Relat�rio de Produto - GS1','RelatorioProdutoGS1BO','relatorioProdutoGS1',1)


-- Inclui coluna CodigoAssociado a tabela de log
ALTER TABLE [LOG] ADD CodigoAssociado BIGINT NULL

ALTER TABLE [LOG] ADD CONSTRAINT FK_Log_Associado FOREIGN KEY (CodigoAssociado) REFERENCES dbo.Associado (Codigo)
GO

INSERT INTO FORMULARIO VALUES (56, 'Relat�rio de Hist�rico de Uso GS1', 'Relat�rio de Hist�rico de Uso GS1', 1, 4, '/backoffice/relatorioHistoricoUsoAssociadoGS1', NULL, 12, 0)
INSERT INTO FUNCIONALIDADE VALUES (171, 'AcessarRelatorioHistoricoUsoAssociadoGS1', 'Gerar Relat�rio de Hist�rico de Uso GS1', 1, 1, 56)
INSERT INTO PERFILFUNCIONALIDADE VALUES (1, 171, GETDATE(), 1)
