﻿INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1011, 'LBL_InformacoesDonoGLN', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Informações sobre o dono do GLN', 'Informações sobre o dono do GLN.', 1, 1011, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 61, 1011


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 996, 'LBL_DetalhesGLN', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Detalhes do GLN', 'Detalhes do GLN.', 1, 996, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 61, 996


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 997, 'LBL_InformacoesDonoItem', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Informações sobre o dono do item', 'Informações sobre o dono do item.', 1, 997, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 61, 997


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 998, 'LBL_DetalhesItem', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Detalhes do item', 'Detalhes do item.', 1, 998, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 61, 998



INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 66, 998

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 66, 997


INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 68, 996

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 68, 1011