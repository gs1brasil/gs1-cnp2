INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1 + (SELECT TOP 1 codigo FROM campo ORDER BY codigo DESC), 'MSG_alertaGerarImagemEtiqueta', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'N�o recomendamos, ao copiar uma imagem, ampli�-la ou reduzi-la, pois isso a afetar� tecnicamente possibilitando problemas na leitura.', 'N�o recomendamos, ao copiar uma imagem, ampli�-la ou reduzi-la, pois isso a afetar� tecnicamente possibilitando problemas na leitura.', 1, (SELECT 1 + (SELECT TOP 1 codigo FROM campo ORDER BY codigo DESC)), 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 17, (SELECT 1 + (SELECT TOP 1 codigo FROM campo ORDER BY codigo DESC))