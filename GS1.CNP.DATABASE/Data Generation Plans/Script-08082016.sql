INSERT INTO Campo (Codigo, Nome, Status)
SELECT 999, 'LBL_InformacoesNutricionais_titulo', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Informa��es Nutricionais', 'Informa��es Nutricionais.', 1, 999, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 999


INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 626


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1000, 'LBL_ServingSize', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Descri��o de Por��o ou Medida', 'Descri��o de Por��o ou Medida do produto.', 1, 1000, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1000


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1001, 'LBL_NutrientTypeCode', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Item para Informa��o Nutricional', 'Selecione um item para a informa��o nutricional.', 1, 1001, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1001


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1002, 'LBL_dailyValueIntakePercent', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT '% VD', 'Informe uma porcentagem de valor di�rio.', 1, 1002, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1002


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1003, 'GRID_unidadeMedida', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Unidade de Medida', 'Unidade de Medida', 1, 1003, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1003


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1004, 'GRID_dailyValueIntakePercent', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT '% VD', 'Informe uma porcentagem de valor di�rio.', 1, 1004, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1004


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1005, 'LBL_NenhumItemInserido', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Nenhum item inserido', 'Nenhum item inserido.', 1, 1005, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1005


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1006, 'LBL_dailyValueIntakeReference', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Refer�ncia Di�ria', 'Informe a refer�ncia di�ria do produto', 1, 1006, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1006


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1007, 'LBL_Alergenos', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Al�rgenos', 'Al�rgenos', 1, 1007, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1007


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1008, 'LBL_AllergenTypeCode', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'C�digo do Al�rgeno', 'C�digo do Al�rgeno', 1, 1008, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1008


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1009, 'LBL_AllergenContainmentCode', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Presen�a do Al�rgeno', 'Presen�a do Al�rgeno', 1, 1009, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1009


INSERT INTO Campo (Codigo, Nome, Status)
SELECT 1010, 'GRID_AllergenLevelOfContainment', 1

INSERT INTO CampoIdioma (Texto, Comentario, Status, CodigoCampo, CodigoIdioma)
SELECT 'Presen�a do Al�rgeno', 'Presen�a do Al�rgeno', 1, 1010, 1

INSERT INTO FormularioCampo (CodigoFormulario, CodigoCampo)
SELECT 15, 1010

