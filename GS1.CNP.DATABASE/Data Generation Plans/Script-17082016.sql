BEGIN TRANSACTION "Alergeno"

--NONBINARYLOGICENUMERATION
INSERT INTO NonBinaryLogicEnumeration (Nome, Status)
SELECT 'FALSE', 1
UNION SELECT 'NOT_APPLICABLE', 1
UNION SELECT 'TRUE', 1
UNION SELECT 'UNSPECIFIED', 1


--ALLERGEN RELATED INFORMATION
INSERT INTO ALLERGEN (CODIGOALLERGENRELATEDINFORMATION, CODIGOALLERGENTYPECODE) 
SELECT CODIGO AS CODIGOALLERGENRELATEDINFORMATION, CODIGOALLERGENTYPECODE FROM ALLERGENRELATEDINFORMATION

ALTER TABLE ALLERGENRELATEDINFORMATION DROP COLUMN CODIGOALLERGENTYPECODE
ALTER TABLE ALLERGENRELATEDINFORMATION ADD CodigoProduto BIGINT NULL

ALTER TABLE AllergenRelatedInformation ADD CONSTRAINT FK_AllergenRelatedInformation_Produto
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);


--LEVELOFCONTAINMENT
INSERT INTO LevelOfContainment (CodeListVersionField, ValueField, Name, Definition)
SELECT '2', '30', 'A subst�ncia n�o � inclu�da intencionalmente no produto.', 'A subst�ncia n�o � inclu�da intencionalmente ou inerentemente no produto, contudo, como n�o � mandat�ria a declara��o de contato cruzado, testes de confirma��o podem n�o ter sido realizados para confirma��o. Por exemplo, leite n�o inerentemente contendo mariscos; ou amendoim ou palitos de queijo sem declara��o de mariscos.'
UNION SELECT '2', '50', 'Derivado', 'Derivado: O produto � derivado de ingredientes com a condi��o especificada, como definido pela ag�ncia reguladora do mercado de destino.'
UNION SELECT '2', '60', 'N�o derivado', 'N�o derivado: O produto n�o � derivado de ingredientes com a condi��o especificada, como definido pela ag�ncia reguladora do mercado de destino.'
UNION SELECT '1', 'CONTAINS', 'Cont�m', 'Intencionalmente inclu�do no produto.'
UNION SELECT '1', 'FREE_FROM', 'N�o Cont�m', 'O produto n�o cont�m a subst�ncia indicada.'
UNION SELECT '1', 'MAY_CONTAIN', 'Pode conter', 'A subst�ncia n�o � inclu�da intencionalmente. No entanto, devido �s instala��es de produ��o compartilhadas ou por outros motivos, o produto pode conter a subst�ncia.'
UNION SELECT '1', 'UNDECLARED', 'N�o declarado', 'N�o inclu�do intencionalmente no produto, sem declara��o de contato cruzado mandat�ria.'


--PASSANDO CAMPO LEVELOFCONTAINMENT DA TABELA PRODUTOALLERGEN PARA A TABELA ALLERGEN
UPDATE ALLERGEN SET ALLERGEN.CODIGOLEVELOFCONTAINMENT = L.CODIGO
FROM ALLERGEN A 
LEFT JOIN PRODUTOALLERGEN P ON A.CODIGOALLERGENRELATEDINFORMATION = P.CODIGOALLERGENRELATEDINFORMATION
LEFT JOIN LEVELOFCONTAINMENT L ON L.VALUEFIELD = P.LEVELOFCONTAINMENTCODE


--PASSANDO O CODIGOPRODUTO DA TABELA DE PRODUTOALLERGEN PARA A TABELA ALLERGENRELATEDINFORMATION
UPDATE ALLERGENRELATEDINFORMATION SET CODIGOPRODUTO = P.CODIGOPRODUTO
FROM ALLERGENRELATEDINFORMATION A
LEFT JOIN PRODUTOALLERGEN P ON P.CODIGOALLERGENRELATEDINFORMATION = A.CODIGO

DROP TABLE PRODUTOALLERGEN

COMMIT TRANSACTION "Alergeno"; 

