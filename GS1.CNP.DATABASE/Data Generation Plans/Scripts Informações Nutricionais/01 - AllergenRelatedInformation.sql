USE [db_gs1CNP_E9]
GO

ALTER TABLE [dbo].[AllergenRelatedInformation] DROP CONSTRAINT [FK_AllergenRelatedInformation_AllergenTypeCode]
GO

ALTER TABLE ProdutoAllergen DROP FK_ProdutoAllergen_AllergenRelatedInformation;

/****** Object:  Table [dbo].[AllergenRelatedInformation]    Script Date: 07/08/2016 13:57:54 ******/
DROP TABLE [dbo].[AllergenRelatedInformation]
GO

/****** Object:  Table [dbo].[AllergenRelatedInformation]    Script Date: 07/08/2016 13:57:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO



CREATE TABLE [dbo].[AllergenRelatedInformation](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[allergenSpecificationAgency] [varchar](255)  NULL,
	[allergenSpecificationName] [varchar](255)  NULL,
	[allergenStatement] [varchar](100)  NULL,
	[CodigoAllergenTypeCode] varchar(2) NOT NULL,
 CONSTRAINT [PK_AllergenRelatedInformation] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[AllergenRelatedInformation]  WITH CHECK ADD  CONSTRAINT [FK_AllergenRelatedInformation_AllergenTypeCode] FOREIGN KEY([CodigoAllergenTypeCode])
REFERENCES [dbo].[AllergenTypeCode] ([Codigo])
GO

ALTER TABLE [dbo].[AllergenRelatedInformation] CHECK CONSTRAINT [FK_AllergenRelatedInformation_AllergenTypeCode]
GO


ALTER TABLE ProdutoAllergen 
	ADD CONSTRAINT FK_ProdutoAllergen_AllergenRelatedInformation FOREIGN KEY (CodigoAllergenRelatedInformation) REFERENCES AllergenRelatedInformation(CODIGO)
GO