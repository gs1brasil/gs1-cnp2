IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'tipoUnidadeMedida' AND OBJECT_ID = OBJECT_ID(N'NutrientTypeCodeINFOODS'))
BEGIN
    PRINT 'Removendo coluna tipoUnidadeMedida da tabela NutrientTypeCodeINFOODS';
	ALTER TABLE NutrientTypeCodeINFOODS 
		DROP COLUMN tipoUnidadeMedida
END
PRINT 'Adicionando coluna tipoUnidadeMedida na tabela NutrientTypeCodeINFOODS';
ALTER TABLE NutrientTypeCodeINFOODS 
	ADD tipoUnidadeMedida int

IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'sigla' AND OBJECT_ID = OBJECT_ID(N'AllergenTypeCode'))
BEGIN
    PRINT 'Removendo coluna sigla da tabela AllergenTypeCode';
	ALTER TABLE AllergenTypeCode 
		DROP COLUMN sigla
END
PRINT 'Adicionando coluna sigla na tabela AllergenTypeCode';
ALTER TABLE AllergenTypeCode 
	ADD sigla varchar(2) NOT NULL;

IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'descricao' AND OBJECT_ID = OBJECT_ID(N'AllergenTypeCode'))
BEGIN
    PRINT 'Removendo coluna descricao da tabela AllergenTypeCode';
	ALTER TABLE AllergenTypeCode 
		DROP COLUMN descricao
END
PRINT 'Adicionando coluna descricao na tabela AllergenTypeCode';
ALTER TABLE AllergenTypeCode 
	ADD descricao varchar(255) NOT NULL;