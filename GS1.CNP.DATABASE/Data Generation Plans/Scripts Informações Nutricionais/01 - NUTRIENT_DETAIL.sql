USE [db_gs1CNP_E9]
GO

ALTER TABLE [dbo].[ProdutoNutrientDetail] DROP CONSTRAINT [FK_ProdutoNutrientDetail_Produto]
GO

/****** Object:  Table [dbo].[ProdutoNutrientDetail]    Script Date: 07/08/2016 13:41:08 ******/
DROP TABLE [dbo].[ProdutoNutrientDetail]
GO

/****** Object:  Table [dbo].[ProdutoNutrientDetail]    Script Date: 07/08/2016 13:41:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ProdutoNutrientDetail](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[dailyValueIntakePercent] varchar(100) NOT NULL,
	[measurementPrecisionCode] [varchar](255) NOT NULL,
	[nutrientTypeCode] [varchar](255) NOT NULL,
	[CodigoProdutoNutrientDetailavpList] [bigint] NULL,
	[quantityContained] [varchar](255) NOT NULL,
 CONSTRAINT [PK_ProdutoNutrientDetail] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ProdutoNutrientDetail]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoNutrientDetail_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[ProdutoNutrientDetail] CHECK CONSTRAINT [FK_ProdutoNutrientDetail_Produto]
GO


