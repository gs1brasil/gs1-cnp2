BEGIN TRANSACTION
BEGIN TRY

	-----------------------------------------------------------------------------------------------
	-- UNECEREC20
	-----------------------------------------------------------------------------------------------
	PRINT 'CARGA UNECEREC20';

	SELECT * 
	FROM UNECEREC20 
	WHERE CommonCode IN (
		'KJO',
		'C15',
		'JOU',
		'J75',
		'K51'
	)

	DELETE 
	FROM UNECEREC20 
	WHERE CommonCode IN (
		'KJO',
		'C15',
		'JOU',
		'J75',
		'K51'
	)

	DECLARE @ID INT;
	DECLARE @TIPO INT;
	SELECT @ID = COALESCE(MAX(Codigo), 0) + 1 
		, @TIPO = COALESCE(MAX(Tipo), 0) + 1
	FROM UNECEREC20;


	INSERT INTO UNECEREC20 (Codigo, Nome, CommonCode, Sigla, FatorConversao, Tipo, Status, IndicadorPrincipal) VALUES (@ID, 'kilojoule', 'KJO', 'kJ', 1, @TIPO, 1, 0);
	SET @ID = @ID + 1;
	INSERT INTO UNECEREC20 (Codigo, Nome, CommonCode, Sigla, FatorConversao, Tipo, Status, IndicadorPrincipal) VALUES (@ID, 'millijoule', 'C15', 'mJ', '1', @TIPO, '1', '0');
	SET @ID = @ID + 1;
	INSERT INTO UNECEREC20 (Codigo, Nome, CommonCode, Sigla, FatorConversao, Tipo, Status, IndicadorPrincipal) VALUES (@ID, 'joule', 'JOU', 'J', '1', @TIPO, '1', '0');
	SET @ID = @ID + 1;
	INSERT INTO UNECEREC20 (Codigo, Nome, CommonCode, Sigla, FatorConversao, Tipo, Status, IndicadorPrincipal) VALUES (@ID, 'calorie (mean)', 'J75', 'cal', '1', @TIPO, '1', '0');
	SET @ID = @ID + 1;
	INSERT INTO UNECEREC20 (Codigo, Nome, CommonCode, Sigla, FatorConversao, Tipo, Status, IndicadorPrincipal) VALUES (@ID, 'kilocalorie (mean)', 'K51', 'kcal', '1', @TIPO, '1', '0');


	select * from UNECEREC20 WHERE TIPO = @TIPO;

	-----------------------------------------------------------------------------------------------
	-- AllergenTypeCode
	-----------------------------------------------------------------------------------------------
	PRINT 'CARGA AllergenTypeCode';

	SELECT * FROM AllergenTypeCode;

	DELETE FROM AllergenTypeCode;

	DECLARE @CODIGO INT;
	SET @CODIGO = 1;

	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AC', 'Refere-se à presença de Crustáceos ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlérgeno e NomeEspecificaçãoAlérgeno.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AE', 'Refere-se à presença de ovos ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlérgeno e NomeEspecificaçãoAlérgeno.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AF', 'Refere-se à presença de peixe ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlérgeno e NomeEspecificaçãoAlérgeno.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AM', 'Refere-se à presença de leite ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlérgeno e NomeEspecificaçãoAlérgeno.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AN', 'Refere-se à presença de nozes ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlérgeno e NomeEspecificaçãoAlérgeno.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AP', 'Refere-se à presença de amendoim ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlérgeno e NomeEspecificaçãoAlérgeno.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AS', 'Refere-se à presença de sementes de gergelim ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlérgeno e NomeEspecificaçãoAlérgeno.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AU', 'Refere-se à presença de Dióxido de enxofre e Sulfitos produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlérgeno e NomeEspecificaçãoAlérgeno.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AW', 'Refere-se à presença de Cereais contendo glúten ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'AY', 'Refere-se à presença de grãos de soja ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'BC', 'Refere-se à presença de salsão ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'BM', 'Refere-se à presença de mostarda ou seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'NC', 'Refere-se à presença de cacau e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'NK', 'Refere-se à presença de coentro e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'NL', 'Refere-se à presença de tremoço e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'NM', 'Refere-se à presença de milho e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'NP', 'Refere-se à presença de vagem e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'NR', 'Refere-se à presença de centeio e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'NW', 'Refere-se à presença de cenoura e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'UM', 'Refere-se à presença de moluscos e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');
	SET @CODIGO = @CODIGO + 1;
	INSERT INTO AllergenTypeCode(Codigo, sigla, descricao) VALUES (@CODIGO, 'UW', 'Refere-se à presença de trigo e seus derivados no produto, conforme elencado nos regulamentos especificados pela AgênciaEspecificaçãoAlergênico e NomeEspecificaçãoAlergênico.');

	SELECT * FROM AllergenTypeCode;

	PRINT 'Carga realizada com sucesso.';
	COMMIT;

END TRY
BEGIN CATCH
	PRINT 'Erro ao realizar carga.';
	ROLLBACK;
END CATCH