USE [db_gs1CNP_E9]
GO

DELETE FROM nutrientTypeCodeINFOODS;

INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 1' , 'N1', 1, 2)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 2' , 'N2', 1, 2)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 3' , 'N3', 1, 2)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 4' , 'N4', 1, 2)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 5' , 'N5', 1, 2)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 6' , 'N6', 1, 5)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 7' , 'N7', 1, 5)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 8' , 'N8', 1, 5)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 9' , 'N9', 1, 5)
INSERT INTO [nutrientTypeCodeINFOODS]([Nome],[Sigla],[Status],[tipoUnidadeMedida]) VALUES ('Nutriente 10' , 'N10', 1, 5)
GO


