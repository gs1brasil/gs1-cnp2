USE [db_gs1CNP_E9]
GO

/****** Object:  Table [dbo].[ProdutoNutrientHeader]    Script Date: 07/08/2016 13:25:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP TABLE ProdutoNutrientHeader
GO

CREATE TABLE [dbo].[ProdutoNutrientHeader](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[dailyValueIntakeReference] [varchar](70) NOT NULL,
	[nutrientBasisQuantity] [varchar](255) NULL,
	[nutrientBasisQuantitymeasurementUnitCode] [int] NULL,
	[CodigoNutrientBasisQuantityTypeCode] [image] NULL,
	[CodigoPreparationTypeCode] [int] NULL,
	[servingSize] [varchar](50) NULL,
	[servingSizemeasurementUnitCode] [int] NULL,
	[servingSizeDescription] [varchar](70) NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
 CONSTRAINT [PK_ProdutoNutrientHeader] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoNutrientHeader_NutrientBasisQuantityTypeCode] FOREIGN KEY([CodigoPreparationTypeCode])
REFERENCES [dbo].[NutrientBasisQuantityTypeCode] ([Codigo])
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader] CHECK CONSTRAINT [FK_ProdutoNutrientHeader_NutrientBasisQuantityTypeCode]
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoNutrientHeader_PreparationTypeCode] FOREIGN KEY([CodigoPreparationTypeCode])
REFERENCES [dbo].[PreparationTypeCode] ([Codigo])
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader] CHECK CONSTRAINT [FK_ProdutoNutrientHeader_PreparationTypeCode]
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoNutrientHeader_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader] CHECK CONSTRAINT [FK_ProdutoNutrientHeader_Produto]
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoNutrientHeader_UNECEREC20_nutrientBasisQuantity] FOREIGN KEY([nutrientBasisQuantitymeasurementUnitCode])
REFERENCES [dbo].[UNECEREC20] ([Codigo])
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader] CHECK CONSTRAINT [FK_ProdutoNutrientHeader_UNECEREC20_nutrientBasisQuantity]
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoNutrientHeader_UNECEREC20_servingSize] FOREIGN KEY([servingSizemeasurementUnitCode])
REFERENCES [dbo].[UNECEREC20] ([Codigo])
GO

ALTER TABLE [dbo].[ProdutoNutrientHeader] CHECK CONSTRAINT [FK_ProdutoNutrientHeader_UNECEREC20_servingSize]
GO


