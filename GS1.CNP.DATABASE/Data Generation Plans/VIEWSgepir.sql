
create view tbProdutoGtinInferior as 

SELECT 
CodigoProdutoSuperior as Gtin,
CodigoProdutoInferior as GtinInferior,
Quantidade as QtdeItens 
from PRODUTOHIERARQUIA
GO



create view [dbo].[tBAssociado] as
SELECT TOP 1
		A.[Codigo] as IdAssociado
		,A.[CPFCNPJ] as CpfCnpj
		,A.[Nome] as RazaoSocial

		,CASE [CodigoStatusAssociado] WHEN 1 THEN 'A'
							  WHEN 3 THEN 'C'
							  WHEN 2 THEN 'S' 
							  END as Status
		,CASE [CodigoSituacaoFinanceira] WHEN 1 THEN 'A' ELSE '1' END AS StatusPagto
		,B.NOME as ContatoMaster
		,B.EMAIL AS EmailContato
		,null as DtVencTaxa
		,A.[CEP] as CEP
		,A.[Endereco] as Endereco
		,CAST(A.[Numero] AS decimal) as Numero
		,A.[Complemento] as Complemento
		,A.[Bairro] as Bairro
		,A.[Cidade] as Cidade
		,A.[UF] as UF
		,A.[Pais] as Pais
		,A.[DataAtualizacaoCRM] as DtUltimaSinc
		,A.[InscricaoEstadual] as InscricaoEstadual		
		,CASE [CodigoTipoCompartilhamento] WHEN 1 THEN 'N'
										 WHEN 2 THEN 'S'
										ELSE NULL END as CompartilhaDado
		FROM [dbo].[Associado] A
		LEFT JOIN CONTATOASSOCIADO B ON B.CODIGOASSOCIADO = A.CODIGO AND B.INDICADORPRINCIPAL = 1
GO


create view [dbo].[vwRelProdutoItem] as
select
	B.CODIGO AS IdAssociado, 
	B.CpfCnpj, 
	B.NOME AS RazaoSocial, 
	CAST(A.globalTradeItemNumber AS VARCHAR)  as Gtin, 
	CAST(A.NrPrefixo AS VARCHAR) as NrPrefixo, 
	A.CodigoTipoGTIN as TipoGtin,
	i.Nome as Contato, 
	D.Nome as DescTipoGtin, 
	A.brandName as MarcaProduto, 
	A.alternateItemIdentificationID  as CodigoInterno, 
	A.PRODUCTDESCRIPTION as Descricao, 
	A.PRODUCTDESCRIPTION as DescricaoImpressao, 
	A.DataInclusao as DtInclusao, 
	A.DataSuspensao as DtSuspensao, 
	A.DataReativacao as DtReativacao, 
	A.DataCancelamento as DtCancelamento, 
	A.DataReutilizacao as DtReutilizacao,
	E.Sigla3 as Pais, 
	A.CodigoTipoProduto as IdTipoProduto, 
	F.Nome as NomeTipoProduto, 
	G.Nome as Lingua, 
	A.Estado as Estado, 
	A.ipiPerc as TIPI, 
	A.CodItem as CodigoRegistro, 
	null as IdAgencia, 
	null as NomeAgencia, 
	A.width as Largura, 
	A.depth as Profundidade, 
	A.height as Altura, 
	A.netWeight as PesoLiquido,
	a.grossWeight as PesoBruto, 
	L.URL AS UrlMobilecom,
	M.URL as UrlFoto1, 
	N.URL as UrlFoto2, 
	O.URL as UrlFoto3, 

	CASE [CodigoTipoCompartilhamento] WHEN 1 THEN 'N'
											 WHEN 2 THEN 'S'
											ELSE NULL END as CompartilhaDado, 
	S.globalTradeItemNumber as GtinOrigem,


 
	CASE [CodigoStatusAssociado] WHEN 1 THEN 'A'
								  WHEN 3 THEN 'C'
								  WHEN 2 THEN 'S' 
								  END as Status,
	H.Nome as DescStatus, 
	a.[Observacoes] as Observacoes, 
	a.importClassificationValue as NCM, 
	J.CodigoProdutoInferior as GtinInferior, 
	J.Quantidade as QtdeItens, 
	J.productDescription as DescricaoInferior, 
	A.CodeSegment, 
	A.CodeFamily, 
	A.CodeClass, 
	A.CodeBrick, 
	K.CodeType, 
	K.CodeValue,
	
	
	CASE a.CodigoStatusGTIN	WHEN 1 THEN	'A'
							WHEN 2 THEN	'S'
							WHEN 3 THEN	'R'
							WHEN 4 THEN	'C'
							WHEN 5 THEN	'E'
							WHEN 6 THEN	'R'
							END AS StatusPrefixo,

	
	P.URL as Imagem1, 
	Q.URL as Imagem2, 
	R.URL as Imagem3

FROM PRODUTO A
INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado
INNER JOIN TipoGTIN D ON D.Codigo = A.CodigoTipoGTIN
LEFT JOIN CountryOfOriginISO3166 E ON E.CODIGO = A.COUNTRYCODE
LEFT JOIN TipoProduto F ON F.codigo = A.CodigoTipoProduto
LEFT JOIN Idioma G ON G.Codigo = A.CodigoIdioma
LEFT JOIN StatusGtin H ON H.Codigo = A.CodigoStatusGTIN
OUTER APPLY (SELECT TOP 1 K.NOME, K.EMAIL FROM ContatoAssociado K WHERE K.CodigoAssociado = A.CodigoAssociado) AS I
OUTER APPLY (SELECT TOP 1 L.CodigoProdutoInferior, L.Quantidade, V.productDescription FROM ProdutoHierarquia L INNER JOIN PRODUTO V ON V.CODIGOPRODUTO = L.CodigoProdutoInferior WHERE L.CodigoProdutoSuperior = A.CodigoProduto) AS J
OUTER APPLY (SELECT TOP 1 R.CODETYPE, R.CODEVALUE FROM ProdutoBrickTypeValue R WHERE R.CODIGOPRODUTO = A.CODIGOPRODUTO) K
OUTER APPLY (SELECT TOP 1 A1.URL, A1.Codigo FROM ProdutoURL A1 WHERE A1.CodigoProduto = A.CodigoProduto AND CODIGOTIPOURL = 5) AS L
OUTER APPLY (SELECT TOP 1 A2.URL, A2.Codigo FROM ProdutoURL A2 WHERE A2.CodigoProduto = A.CodigoProduto AND CODIGOTIPOURL = 1) AS M
OUTER APPLY (SELECT TOP 1 A3.URL, A3.Codigo FROM ProdutoURL A3 WHERE A3.CodigoProduto = A.CodigoProduto AND CODIGOTIPOURL = 1 AND (A3.CODIGO <> M.CODIGO)) AS N
OUTER APPLY (SELECT TOP 1 A4.URL, A4.Codigo FROM ProdutoURL A4 WHERE A4.CodigoProduto = A.CodigoProduto AND CODIGOTIPOURL = 1 AND (A4.CODIGO <> M.CODIGO AND A4.CODIGO <> N.CODIGO)) AS O
OUTER APPLY (SELECT TOP 1 A5.URL, A5.Codigo FROM ProdutoURL A5 WHERE A5.CodigoProduto = A.CodigoProduto AND CODIGOTIPOURL = 1 AND (A5.CODIGO <> M.CODIGO AND A5.CODIGO <> N.CODIGO AND A5.CODIGO <> O.CODIGO)) AS P
OUTER APPLY (SELECT TOP 1 A6.URL, A6.Codigo FROM ProdutoURL A6 WHERE A6.CodigoProduto = A.CodigoProduto AND CODIGOTIPOURL = 1 AND (A6.CODIGO <> M.CODIGO AND A6.CODIGO <> N.CODIGO AND A6.CODIGO <> O.CODIGO AND A6.CODIGO <> P.CODIGO)) AS Q
OUTER APPLY (SELECT TOP 1 A7.URL, A7.Codigo FROM ProdutoURL A7 WHERE A7.CodigoProduto = A.CodigoProduto AND CODIGOTIPOURL = 1 AND (A7.CODIGO <> M.CODIGO AND A7.CODIGO <> N.CODIGO AND A7.CODIGO <> O.CODIGO AND A7.CODIGO <> P.CODIGO AND A7.CODIGO <> Q.CODIGO)) AS R
LEFT JOIN Produto S ON S.CodigoProduto = A.CodigoProdutoOrigem
GO


create view [dbo].[vwRelCertificado] as 
SELECT a.IdAssociado,
			   a.CpfCnpj,
			   a.RazaoSocial,
			   CAST(ap.globalTradeItemNumber AS VARCHAR)  as Gtin, 
			   ap.productDescription as DescricaoProduto,
			   NULL as IdCertificado,
			   NULL as NomeCertificado,
			   NULL AS DataAtribuicao,
			   NULL AS DataExpiracao,
			   NULL AS Status,
			   'SEM CERTIFICADO' as DescricaoStatus,
			   NULL DtHistorico
		  FROM Produto ap
		  INNER JOIN tbAssociado a ON a.IdAssociado = ap.CodigoAssociado


GO


create View [dbo].[vwRelLocalizacaoFisica] as
SELECT a.IdAssociado
				,a.CpfCnpj
				,a.RazaoSocial
				,CAST(GLN AS VARCHAR) AS nrGln
				,ag.NOME AS Contato  
				,ag.CEP 
				,ag.Endereco 
				,ag.Numero 
				,ag.Complemento  
				,ag.Bairro 
				,ag.Cidade 
				,ag.Estado as UF 
				,ag.Pais
				,ag.CodigoPapelGLN as IdPapel
				
				, COALESCE((SELECT MIN(x.DATAALTERACAO) FROM LocalizacaoFisicaHistorico X WHERE X.Codigo = AG.CODIGO),ag.DATAALTERACAO)  as DtInclusao
				,p.Nome AS NomePapel
				,CAST(ag.Status AS VARCHAR) as Status 
				,CASE ag.Status
				  WHEN '1' THEN 'ATIVO'
				  WHEN '2' THEN 'INATIVO'
				  ELSE 'CANCELADO' END DescStatus
				,ag.Observacao AS Observacoes 
				,ag.IndicadorPrincipal as FlagPrincipal
		   FROM LocalizacaoFisica ag
	 left JOIN PapelGLN p ON (ag.CodigoPapelGLN = p.Codigo)
	 INNER JOIN tbAssociado a ON (ag.CodigoAssociado = a.IdAssociado)
GO
