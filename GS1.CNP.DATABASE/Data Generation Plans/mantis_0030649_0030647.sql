INSERT INTO Funcionalidade(Codigo, Nome, Descricao, Status, CodigoTipoUsuario, CodigoFormulario)
SELECT 168,	'BaixarChave', 'Baixar Chave', 1, 1, 13 
GO

INSERT INTO PerfilFuncionalidade(CodigoPerfil, CodigoFuncionalidade, DataAlteracao, CodigoUsuarioAlteracao)
SELECT 1, 168, GETDATE(), 1
GO

DELETE FROM PerfilFuncionalidade WHERE CodigoFuncionalidade = 30
GO

DELETE FROM Funcionalidade WHERE Codigo = 30
GO