CREATE NONCLUSTERED INDEX [nci_wi_Produto_A2ECDC003962D02332AB] ON [dbo].[Produto] ([CodigoTipoGTIN], [NrPrefixo]) INCLUDE ([CodItem]) WITH (ONLINE = ON)
13:09
CREATE NONCLUSTERED INDEX [nci_wi_ProdutoAgenciaReguladora_2E7F78633CF48E0C27A0] ON [dbo].[ProdutoAgenciaReguladora] ([CodigoProduto]) INCLUDE ([alternateItemIdentificationId]) WITH (ONLINE = ON)
3:27
CREATE NONCLUSTERED INDEX [nci_wi_Associado_96DE5D4433929CFE33D7] ON [dbo].[Associado] ([CPFCNPJ]) INCLUDE ([Codigo]) WITH (ONLINE = ON)
1:44
CREATE NONCLUSTERED INDEX [nci_wi_ProdutoImagem_2E7F78633CF48E0C27A0] ON [dbo].[ProdutoImagem] ([CodigoProduto]) INCLUDE ([URL]) WITH (ONLINE = ON)
0:08
