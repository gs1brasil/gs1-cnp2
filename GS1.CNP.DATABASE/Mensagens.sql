$38.LBL_TituloMensagens -> 38.LBL_TituloNoticias
MENSAGEM_SemMensagemDisponivel - > MENSAGEM_SemNoticiaDisponivel

38.LBL_Mensagem
38.LBL_Mensagens
38.LBL_SemMensagens
38.LBL_VejaTodasMensagens
38.LBL_Naolidas

59.LBL_Mensagens
59.GRID_DataMensagem
59.GRID_Mensagem
59.GRID_Assunto

59.MENSAGEM_MensagemNaoEncontrada

*Ajuda


/****** Object:  Table [dbo].[Mensagem]    Script Date: 28/06/2016 14:24:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Mensagem](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CAD] [varchar] (20) NULL,
	[CodigoAssociado] [bigint] NULL,
	[DataCriacao] [datetime] NOT NULL,
	[DataSincronizacao] [datetime] NOT NULL,
	[DataValidacao] [datetime] NOT NULL,	
	[Assunto] [varchar](100) NOT NULL,
	[Mensagem] [text] NOT NULL,
	[gs1br_cnpinformativoId] [uniqueidentifier] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Mensagem] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Mensagem]  WITH CHECK ADD  CONSTRAINT [FK_Mensagem_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO

ALTER TABLE [dbo].[Mensagem] CHECK CONSTRAINT [FK_Mensagem_Associado]
GO




/****** Object:  Table [dbo].[MensagemLeitura]    Script Date: 28/06/2016 14:24:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MensagemLeitura](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CodigoMensagem] [bigint] NOT NULL,
	[CodigoUsuario] [bigint] NOT NULL,
	[DataLeitura] [datetime] NOT NULL,
 CONSTRAINT [PK_MensagemLeitura] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[MensagemLeitura]  WITH CHECK ADD  CONSTRAINT [FK_MensagemLeitura_Mensagem] FOREIGN KEY([CodigoMensagem])
REFERENCES [dbo].[Mensagem] ([Codigo])
GO

ALTER TABLE [dbo].[MensagemLeitura] CHECK CONSTRAINT [FK_MensagemLeitura_Mensagem]
GO

ALTER TABLE [dbo].[MensagemLeitura]  WITH CHECK ADD  CONSTRAINT [FK_MensagemLeitura_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[MensagemLeitura] CHECK CONSTRAINT [FK_MensagemLeitura_Usuario]
GO

ALTER TABLE dbo.MensagemLeitura ADD CONSTRAINT
	UN_MensagemLeitura_Usuario UNIQUE NONCLUSTERED 
	(
	CodigoMensagem,
	CodigoUsuario
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO


SELECT * FROM FORMULARIO

INSERT [Formulario] ([Codigo], [Nome], [Descricao], [Status], [CodigoModulo], [Url], [Icone], [Ordem], [IndicadorMenu]) VALUES (59, N'Mensagens', N'Mensagens', 1, 2, N'/backoffice/mensagem', N'icon-user', 1, N'1')



USE CRM

ALTER TABLE gs1br_cnpinformativo ADD DataSincronizacao DATETIME NULL

ALTER TABLE MENSAGEM ADD CAD VARCHAR(20) NULL

/*

SELECT  

B.ACCOUNTNUMBER
,A.CreatedOn 
,getdate()
,a.gs1br_Datadavalidacao
,a.gs1br_name
--,a.gs1br_mesagem 
,a.gs1br_cnpinformativoId
,1

FROM gs1br_cnpinformativo a
LEFT JOIN ACCOUNT B ON B.AccountId = A.gs1br_Empresa

INSERT INTO [dbo].[Mensagem]
           ([CodigoAssociado]
           ,[DataCriacao]
           ,[DataSincronizacao]
           ,[DataValidacao]
		   ,[Assunto]
           ,[Mensagem]
           ,[gs1br_cnpinformativoId]
           ,[Status])
     VALUES
           (7
           ,GETDATE()
           ,GETDATE()
           ,GETDATE()
           ,'Teste mensagem YUKS 4'
		   ,'Corpo da mensagem YUKS 4'
           ,'81822C8E-9C3C-E611-941F-00155D476D1F'
           ,1)
GO

USE [cnp20-DB-Aceite]
GO

INSERT INTO [dbo].[MensagemLeitura]
           ([CodigoMensagem]
           ,[CodigoUsuario]
           ,[DataLeitura])
     VALUES
           (13
           ,1
           ,GETDATE())
GO





SELECT A.DATASINCRONIZACAO, A.MENSAGEM, B.DATALEITURA 
FROM MENSAGEM A
LEFT JOIN MENSAGEMLEITURA B ON B.CODIGOMENSAGEM = A.CODIGO AND B.CODIGOUSUARIO = 1
WHERE STATUS = 1 
AND (CODIGOASSOCIADO = 1 or CODIGOASSOCIADO IS NULL)


;WITH DADOS AS ( SELECT A.CODIGO
                                ,CONVERT(VARCHAR(10),CONVERT(DATETIME, A.DATASINCRONIZACAO,106),103) + ' '  + CONVERT(VARCHAR(8), CONVERT(DATETIME, A.DATASINCRONIZACAO,113), 14) AS DATASINCRONIZACAO 
                                , A.MENSAGEM, B.DATALEITURA  
                                FROM MENSAGEM A
                                LEFT JOIN MENSAGEMLEITURA B ON B.CODIGOMENSAGEM = A.CODIGO AND B.CODIGOUSUARIO = 1
                                WHERE STATUS = 1 
                                AND (CODIGOASSOCIADO = 7 or CODIGOASSOCIADO IS NULL))
                        SELECT * 
                        ,TOTALREGISTROS = COUNT(*) OVER()
						, (SELECT COUNT(*) 
							FROM MENSAGEM A 
							LEFT JOIN MENSAGEMLEITURA B ON B.CODIGOMENSAGEM = A.CODIGO AND B.CODIGOUSUARIO = 1
							WHERE STATUS = 1 
                            AND (CODIGOASSOCIADO = 7 or CODIGOASSOCIADO IS NULL)
							AND B.CODIGO IS NULL) AS TOTALNAOLIDAS
                        FROM DADOS WITH (NOLOCK)
                        ORDER BY DATASINCRONIZACAO DESC
                        OFFSET 10 * (1-1) ROWS FETCH NEXT 10 ROWS ONLY




--DELETE FROM [MensagemLeitura]
delete FROM Mensagem

select * from associado where cad = '119011'

SELECT * FROM Mensagem

update mensagem set codigoassociado = 24107, mensagem = 'Teste mensagem GS1 <a href="alert(''ok'')">OK</a>' where codigo = 1
update mensagem set codigoassociado = 7 where codigo = 2


*/