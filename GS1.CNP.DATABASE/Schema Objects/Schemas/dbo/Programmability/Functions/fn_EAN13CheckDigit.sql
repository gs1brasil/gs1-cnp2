--select dbo.fn_EAN13CheckDigit('789859353083')
CREATE FUNCTION [fn_EAN13CheckDigit] (@Barcode nvarchar(12)) 
RETURNS nvarchar(13) 
AS 
BEGIN 
    DECLARE @SUM int , @COUNTER int, @RETURN varchar(13), @Val1 int, @Val2 int 
    SET @COUNTER = 1 SET @SUM = 0 

    WHILE @Counter < 13 
    BEGIN 
        SET @VAL1 = SUBSTRING(@Barcode,@counter,1) * 1 
        SET @VAL2 = SUBSTRING(@Barcode,@counter + 1,1) * 3 
        SET @SUM = @VAL1 + @SUM 
        SET @SUM = @VAL2 + @SUM 
        SET @Counter = @Counter + 2; 
    END 
    SET @SUM=(10-(@SUM%10))%10

    SET @Return = @BARCODE + CONVERT(varchar,((@SUM))) 
    RETURN @Return 
END