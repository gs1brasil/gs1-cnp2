﻿CREATE PROCEDURE PROC_IMPORTAR_SEGMENTO
AS
BEGIN
	DECLARE @SEGMENT_ID INT,
			@SEGMENT_CODE VARCHAR(10),
			@SEGMENT_TEXT VARCHAR(1500),
			@SEGMENT_DEFINITION VARCHAR(2000);

	DECLARE CUR_SEGMENTO CURSOR FOR
	SELECT SEGMENT_ID, CODE, [TEXT], [DEFINITION]
	FROM #SEGMENT;

	OPEN CUR_SEGMENTO;

	FETCH NEXT FROM CUR_SEGMENTO
	INTO @SEGMENT_ID, @SEGMENT_CODE, @SEGMENT_TEXT, @SEGMENT_DEFINITION;

	-- SEGMENTO
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (NOT EXISTS(SELECT CODESEGMENT
					   FROM TBSEGMENT
					   WHERE CODESEGMENT = @SEGMENT_CODE
					   AND IDIOMA = 'pt-br'))
		BEGIN
			INSERT INTO TBSEGMENT VALUES (@SEGMENT_CODE, 'pt-br', @SEGMENT_TEXT, @SEGMENT_DEFINITION);
		END
		ELSE
			UPDATE TBSEGMENT SET [TEXT] = @SEGMENT_TEXT, [DEFINITION] = @SEGMENT_DEFINITION WHERE CODESEGMENT = @SEGMENT_CODE AND IDIOMA = 'pt-br';

		-- FAMILIA
		EXEC PROC_IMPORTAR_FAMILIA @SEGMENT_ID, @SEGMENT_CODE;

		FETCH NEXT FROM CUR_SEGMENTO
		INTO @SEGMENT_ID, @SEGMENT_CODE, @SEGMENT_TEXT, @SEGMENT_DEFINITION;
	END;

	CLOSE CUR_SEGMENTO;
	DEALLOCATE CUR_SEGMENTO;
END;
GO