
/****** Object:  StoredProcedure [dbo].[PROC_JOB_SINCRONIZACAO_CRM]    Script Date: 13/05/2016 09:32:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
ALTER PROCEDURE [dbo].[PROC_JOB_SINCRONIZACAO_CRM](  
 @XMLASSOCIADOS XML  
 , @XMLLICENCAS XML  
)  
AS BEGIN  
  
  --INSERT INTO LOGJOBSINCRONIZACAO VALUES (GETDATE(), @XMLASSOCIADOS, @XMLLICENCAS)


  DECLARE @TEMPTBLLICENCA TABLE (prefixo varchar(max),data_aniversario varchar(max),flagprincipal int,licenca varchar(max),negocio varchar(max),qtde_digitos int,stcadastral int,stfinanceiro int  
        ,datamodificacao  datetime, associado varchar(max), status_da_licenca varchar(max), cad varchar(max),id varchar(max), descricaogtin8 varchar(max))  
  
  ;WITH XMLNAMESPACES(default 'http://schemas.datacontract.org/2004/07/GS1.CNP.BLL.Backoffice')    
  INSERT INTO @TEMPTBLLICENCA  
   SELECT         
    Tbl.Col.value('(prefixo)[1]', 'varchar(max)') as prefixo  
   ,Tbl.Col.value('(data_aniversario)[1]', 'varchar(max)') as data_aniversario  
   ,Tbl.Col.value('(flagprincipal)[1]', 'int') as flagprincipal  
   ,Tbl.Col.value('(licenca)[1]', 'varchar(max)') as licenca  
   ,Tbl.Col.value('(negocio)[1]', 'varchar(max)') as negocio  
   ,Tbl.Col.value('(qtde_digitos)[1]', 'int') as qtde_digitos  
   ,Tbl.Col.value('(stcadastral)[1]', 'int') as stcadastral  
   ,Tbl.Col.value('(stfinanceiro)[1]', 'int') as stfinanceiro  
   ,Tbl.Col.value('(datamodificacao)[1]', 'datetime') as data_modificacao  
   ,Tbl.Col.value('(associado)[1]', 'varchar(max)') as associado  
   ,Tbl.Col.value('(status_da_licenca)[1]', 'varchar(max)') as status_da_licenca  
   ,Tbl.Col.value('(CAD)[1]', 'varchar(max)') as cad  
   ,Tbl.Col.value('(id)[1]', 'varchar(max)') as id  
   ,Tbl.Col.value('(descricaogtin8)[1]', 'varchar(max)') as descricaogtin8  
    
   FROM   @XMLLICENCAS.nodes('//ArrayOfIntegracaoCrmBO.PrefixoLicencaAssociadoCRM//IntegracaoCrmBO.PrefixoLicencaAssociadoCRM') Tbl(Col)  
    
  
 DECLARE @TEMPTBLDADOASSOCIADO TABLE (id varchar(max),nome varchar(max),nomefantasia varchar(max),email varchar(max),cpfcnpj varchar(max),cad varchar(max),relacionamento int,bl_adimplente bit,bl_premium bit,bl_acessoTrade bit,senha varchar(max),  
          inscricaoestadual varchar(max),estadoname  varchar(max),numero varchar(max),address1_city varchar(max),logradouroprincipal varchar(max),complementoprincipal varchar(max),address1_postalcode varchar(max),  
          bairroprincipal varchar(max),representante_smart_pessoaname varchar(max),representante_smart_cpf varchar(max),representante_telephone1 varchar(max),representante_telephone2 varchar(max),  
          representante_telephone3 varchar(max),representante_emailaddress1 varchar(max),representante_emailaddress2 varchar(max),glninformativo bigint,pais varchar(max),telephone1 varchar(max),  
          smart_ddidddtelefoneprincipal varchar(max), gs1br_cnp20 bit, datamodificacao  datetime)  
  
  
  
 ;WITH XMLNAMESPACES(default 'http://schemas.datacontract.org/2004/07/GS1.CNP.BLL')    
  INSERT INTO @TEMPTBLDADOASSOCIADO  
  SELECT           
   Tbl.Col.value('(id)[1]', 'varchar(max)') as id  
   ,Tbl.Col.value('(nome)[1]', 'varchar(max)') as nome  
   ,Tbl.Col.value('(nomefantasia)[1]', 'varchar(max)') as nomefantasia  
   ,Tbl.Col.value('(email)[1]', 'varchar(max)') as email  
   ,Tbl.Col.value('(cpfcnpj)[1]', 'varchar(max)') as cpfcnpj  
   ,Tbl.Col.value('(cad)[1]', 'varchar(max)') as cad  
   ,Tbl.Col.value('(relacionamento)[1]', 'int') as relacionamento  
   ,Tbl.Col.value('(bl_adimplente)[1]', 'bit') as bl_adimplente  
   ,Tbl.Col.value('(bl_premium)[1]', 'bit ') as bl_premium  
   ,Tbl.Col.value('(bl_acessoTrade)[1]', 'bit ') as bl_acessoTrade  
   ,Tbl.Col.value('(senha)[1]', 'varchar(max)') as senha  
   ,Tbl.Col.value('(inscricaoestadual)[1]', 'varchar(max)') as inscricaoestadual  
   ,Tbl.Col.value('(estadoname )[1]', 'varchar(max)') as estadoname  
   ,Tbl.Col.value('(numero)[1]', 'varchar(max)') as numero  
   ,Tbl.Col.value('(address1_city)[1]', 'varchar(max)') as address1_city  
   ,Tbl.Col.value('(logradouroprincipal)[1]', 'varchar(max)') as logradouroprincipal  
   ,Tbl.Col.value('(complementoprincipal)[1]', 'varchar(max)') as complementoprincipal  
   ,Tbl.Col.value('(address1_postalcode)[1]', 'varchar(max)') as address1_postalcode  
   ,Tbl.Col.value('(bairroprincipal)[1]', 'varchar(max)') as bairroprincipal  
   ,Tbl.Col.value('(representante_smart_pessoaname)[1]', 'varchar(max)') as representante_smart_pessoaname  
   ,Tbl.Col.value('(representante_smart_cpf)[1]', 'varchar(max)') as representante_smart_cpf  
   ,Tbl.Col.value('(representante_telephone1)[1]', 'varchar(max)') as representante_telephone1  
   ,Tbl.Col.value('(representante_telephone2)[1]', 'varchar(max)') as representante_telephone2  
   ,Tbl.Col.value('(representante_telephone3)[1]', 'varchar(max)') as representante_telephone3  
   ,Tbl.Col.value('(representante_emailaddress1)[1]', 'varchar(max)') as representante_emailaddress1  
   ,Tbl.Col.value('(representante_emailaddress2)[1]', 'varchar(max)') as representante_emailaddress2  
   ,Tbl.Col.value('(glninformativo)[1]', 'bigint') as glninformativo  
   ,Tbl.Col.value('(pais)[1]', 'varchar(max)') as pais  
   ,Tbl.Col.value('(telephone1)[1]', 'varchar(max)') as telephone1  
   ,Tbl.Col.value('(smart_ddidddtelefoneprincipal)[1]', 'varchar(max)') as smart_ddidddtelefoneprincipal  
   ,Tbl.Col.value('(gs1br_cnp20)[1]', 'bit') as gs1br_cnp20  
   ,Tbl.Col.value('(datamodificacao)[1]', 'datetime') as data_modificacao  
    
   FROM   @XMLASSOCIADOS.nodes('ArrayOfUsuarioCRM/UsuarioCRM') Tbl(Col)  
   
  BEGIN TRANSACTION  
    
   --GRAVA O HISTORICO DO ASSOCIADO  
   INSERT INTO [dbo].[AssociadoHistorico] ([CodigoAssociado],[Nome],[Descricao],[Email],[CPFCNPJ],[InscricaoEstadual],[CodigoTipoAssociado],[MensagemObservacao],[DataAlteracao],  
                       [DataAtualizacaoCRM],[DataStatusInadimplente],[CodigoUsuarioAlteracao],[Endereco],[Numero],[Complemento],[Bairro],[Cidade],[UF],[CEP],  
                       [CodigoTipoCompartilhamento],[DataHistorico],[CodigoStatusAssociado],[CodigoSituacaoFinanceira],[CAD])  
  
   SELECT A.[Codigo],A.[Nome],A.[Descricao],A.[Email],A.[CPFCNPJ],A.[InscricaoEstadual],A.[CodigoTipoAssociado],A.[MensagemObservacao],A.[DataAlteracao],  
            A.[DataAtualizacaoCRM],A.[DataStatusInadimplente],A.[CodigoUsuarioAlteracao],A.[Endereco],A.[Numero],A.[Complemento],A.[Bairro],A.[Cidade],A.[UF],A.[CEP],  
            A.[CodigoTipoCompartilhamento],GETDATE(),A.[CodigoStatusAssociado],A.[CodigoSituacaoFinanceira],A.[CAD]  
   FROM ASSOCIADO A  
   --WHERE ASSOCIADO.CODIGOSTATUSASSOCIADO = 1  
   INNER JOIN @TEMPTBLDADOASSOCIADO B ON B.CAD = A.CAD  
   WHERE (DATEADD(HOUR, -3, B.DATAMODIFICACAO)  > A.DATAATUALIZACAOCRM OR A.DATAATUALIZACAOCRM IS NULL)
  
   --ATUALIZA OS ASSOCIADOS ALTERADOS  
   UPDATE [dbo].[Associado]  
   SET  ASSOCIADO.[Nome] = B.NOME  
    ,ASSOCIADO.[Descricao] = B.NOMEFANTASIA  
    ,ASSOCIADO.[Email] = B.EMAIL  
    ,ASSOCIADO.[CPFCNPJ] = REPLACE(REPLACE(REPLACE(B.CPFCNPJ,'.',''),'/',''),'-','')  
    ,ASSOCIADO.[InscricaoEstadual] = B.InscricaoEstadual      
    ,ASSOCIADO.[CodigoTipoAssociado] = CASE B.bl_premium WHEN 1 THEN 1 ELSE 2 END          
    ,ASSOCIADO.[DataAtualizacaoCRM] = GETDATE()      
    ,ASSOCIADO.[DataStatusInadimplente] = CASE B.bl_adimplente WHEN 1 THEN NULL ELSE GETDATE() END      
    ,ASSOCIADO.[CodigoUsuarioAlteracao] = 1  
    ,ASSOCIADO.[Endereco] = B.logradouroprincipal  
    ,ASSOCIADO.[Numero] = B.Numero  
    ,ASSOCIADO.[Complemento] = B.Complementoprincipal  
    ,ASSOCIADO.[Bairro] = B.Bairroprincipal  
    ,ASSOCIADO.[Cidade] = B.address1_city  
    ,ASSOCIADO.[UF] = B.estadoname  
    ,ASSOCIADO.[CEP] = B.address1_postalcode      
    ,ASSOCIADO.[CodigoStatusAssociado] = CASE B.gs1br_cnp20   
              WHEN 1 THEN   
               CASE B.bl_adimplente WHEN 1 THEN  
                CASE B.relacionamento   
                 WHEN 2 THEN 1   
                 ELSE 6 --Não Associado  
                END  
                ELSE 5 END --Inadimplente  
              ELSE  
               4 -- Sem acesso CNP 2.0  
              END  
  
    ,ASSOCIADO.[CodigoSituacaoFinanceira] = CASE B.bl_adimplente WHEN 1 THEN 1 ELSE 2 END  
    ,ASSOCIADO.[CAD] = B.CAD  
    ,ASSOCIADO.[Pais] = B.pais  
   FROM ASSOCIADO  
   INNER JOIN @TEMPTBLDADOASSOCIADO B ON B.CAD = ASSOCIADO.CAD  
   WHERE (DATEADD(HOUR, -3, B.DATAMODIFICACAO) > ASSOCIADO.DATAATUALIZACAOCRM OR ASSOCIADO.DATAATUALIZACAOCRM IS NULL)
     
  
   --ATUALIZA O CONTATO PRINCIPAL    
   UPDATE   
    CONTATOASSOCIADO  
   SET     
     [Nome] = representante_smart_pessoaname  
    ,[CPFCNPJ] = representante_smart_cpf     
    ,[Telefone1] = representante_telephone1  
    ,[Telefone2] = representante_telephone2  
    ,[Telefone3] = representante_telephone3  
    ,[Email] = representante_emailaddress1  
    ,[Email2] = representante_emailaddress2     
    ,[DataAtualizacaoCRM] = GETDATE()     
   FROM CONTATOASSOCIADO A  
   INNER JOIN ASSOCIADO B ON B.Codigo = A.CodigoAssociado  
   INNER JOIN @TEMPTBLDADOASSOCIADO C ON C.CAD = B.CAD   
   AND REPLACE(REPLACE(REPLACE(C.representante_smart_cpf,'.',''),'/',''),'-','') = REPLACE(REPLACE(REPLACE(A.CPFCNPJ,'.',''),'/',''),'-','')  
   --AND (DATEADD(HOUR, -3, C.DATAMODIFICACAO) > B.DATAATUALIZACAOCRM OR B.DATAATUALIZACAOCRM IS NULL)
  
  
   --INSERE CONTATO PRINCIPAL NAO EXISTENTE  
   INSERT INTO [dbo].[ContatoAssociado]  
   ([CodigoAssociado],[Nome],[CPFCNPJ],[CodigoTipoContato]  
   ,[Telefone1],[Telefone2],[Telefone3]  
   ,[Email],[Email2],[Status],[Observacao],[IndicadorPrincipal],[DataAtualizacaoCRM])  
   SELECT B.Codigo, A.representante_smart_pessoaname, A.representante_smart_cpf, 1  
       ,A.representante_telephone1, A.representante_telephone2, A.representante_telephone3  
       ,A.representante_emailaddress1, A.representante_emailaddress2,1,NULL,1,GETDATE()  
   FROM @TEMPTBLDADOASSOCIADO A  
   INNER JOIN ASSOCIADO B ON B.CAD = A.CAD --AND (DATEADD(HOUR, -3, A.DATAMODIFICACAO) > B.DATAATUALIZACAOCRM OR B.DATAATUALIZACAOCRM IS NULL)
   LEFT JOIN ContatoAssociado C ON C.CodigoAssociado = B.Codigo   
   AND REPLACE(REPLACE(REPLACE(a.representante_smart_cpf,'.',''),'/',''),'-','') = REPLACE(REPLACE(REPLACE(C.CPFCNPJ,'.',''),'/',''),'-','')  
   WHERE C.Codigo IS NULL  
  
     
   --INSERE GLN INFORMATIVO NAO EXISTENTE    
   INSERT INTO [dbo].[LocalizacaoFisica] ([Nome],[Descricao],[Status],[Prefixo],[GLN],[NumeroItem],[CodigoPapelGLN]  
       ,[Endereco],[Numero],[Complemento],[CEP],[Cidade],[Estado],[Pais],[Bairro]  
       ,[NomeImagem],[Observacao],[DataAlteracao],[CodigoUsuarioAlteracao],[IndicadorPrincipal]  
       ,[DataCancelamento],[DataSuspensao],[DataReutilizacao],[CodigoAssociado],[Latitude],[Longitude],[Telefone],[Email])  
      
  
    
   SELECT DISTINCT a.nome, a.nome, 1
   
   
    , COALESCE(

   
			(SELECT Y.NUMEROPREFIXO FROM PREFIXOLICENCAASSOCIADO Y WHERE Y.CODIGOASSOCIADO = B.Codigo
			AND CHARINDEX(CAST(Y.NUMEROPREFIXO AS VARCHAR(12)),a.glninformativo) > 0)
		, SUBSTRING(CAST(a.glninformativo AS VARCHAR(50)),1,12)
		)
   
   
   , a.glninformativo
   
	, CASE WHEN ((SELECT Y.NUMEROPREFIXO FROM PREFIXOLICENCAASSOCIADO Y WHERE Y.CODIGOASSOCIADO = B.Codigo
			AND CHARINDEX(CAST(Y.NUMEROPREFIXO AS VARCHAR(12)),a.glninformativo) > 0)
				IS NOT NULL ) THEN
				REPLACE(	SUBSTRING(CAST(a.glninformativo AS VARCHAR(50)),1,12),
							(SELECT Y.NUMEROPREFIXO FROM PREFIXOLICENCAASSOCIADO Y WHERE Y.CODIGOASSOCIADO = B.Codigo
							AND CHARINDEX(CAST(Y.NUMEROPREFIXO AS VARCHAR(12)),a.glninformativo) > 0)
							,'' 
							
						)
			ELSE a.glninformativo
		END
   
   
   , 1   
    ,a.logradouroprincipal, a.numero, a.complementoprincipal, a.address1_postalcode, a.address1_city, a.estadoname, a.pais, a.bairroprincipal  
    ,NULL, NULL, GETDATE(), 1, 1, NULL, NULL, NULL, B.Codigo, NULL, NULL, a.telephone1, a.email    
   FROM @TEMPTBLDADOASSOCIADO A  
   INNER JOIN ASSOCIADO B ON B.CAD = A.CAD  
   LEFT JOIN LocalizacaoFisica C ON  C.gln = A.glninformativo
   
   WHERE C.Codigo IS NULL  
   AND A.GLNINFORMATIVO > 0  
  
    

	--HISTORICO DAS LICENCAS EXISTENTES
	INSERT INTO LICENCAASSOCIADOHISTORICO
	SELECT A.CODIGOLICENCA, A.CODIGOASSOCIADO, A.STATUS, A.DATAANIVERSARIO, A.DATAALTERACAO, GETDATE()
	   FROM LicencaAssociado A  
     INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
     INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD --AND (DATEADD(HOUR, -3, C.DATAMODIFICACAO) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
     INNER JOIN Licenca D ON D.NOME = C.licenca  
     WHERE D.Codigo = A.CodigoLicenca  

   --ATUALIZA LICENCAS EXISTENTES    
   UPDATE [dbo].[LicencaAssociado]  
     SET [DataAniversario] = CASE C.data_aniversario WHEN '0001-01-01T00:00:00' THEN NULL WHEN NULL THEN NULL ELSE CONVERT(datetime,C.data_aniversario) END  
      ,[DataAlteracao] = GETDATE()  
   FROM LicencaAssociado A  
     INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
     INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD --AND (DATEADD(HOUR, -3, C.DATAMODIFICACAO) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
     INNER JOIN Licenca D ON D.NOME = C.licenca  
     WHERE D.Codigo = A.CodigoLicenca  
  
  
   --INSERE LICENCAS NOVAS  
   INSERT INTO [dbo].[LicencaAssociado]  
        ([CodigoLicenca]  
        ,[CodigoAssociado]  
        ,[Status]  
        ,[DataAniversario]  
        ,[DataAlteracao])  
  
   SELECT DISTINCT B.CODIGO, C.Codigo,   
     CASE A.status_da_licenca WHEN 1 THEN 5  
         WHEN 2 THEN 1  
         WHEN 3 THEN 6  
         WHEN 4 THEN 3  
         WHEN 5 THEN 7  
         WHEN 6 THEN 8  
         WHEN 7 THEN 4 END  
     ,CASE A.data_aniversario WHEN '0001-01-01T00:00:00' THEN NULL WHEN NULL THEN NULL ELSE CONVERT(datetime,A.data_aniversario) END  
     ,GETDATE()  
   FROM @TEMPTBLLICENCA A  
   INNER JOIN LICENCA B ON B.NOME = A.LICENCA  
   INNER JOIN Associado C ON C.CAD = A.cad  
   LEFT JOIN LicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo  
   where D.CodigoAssociado is null  
   AND A.status_da_licenca IS NOT NULL  
  
  
  --HISTORICO DOS PREFIXOS EXISTENTES
	INSERT INTO PrefixoLicencaAssociadoHistorico
	SELECT A.CODIGOLICENCA, A.CODIGOASSOCIADO, A.NUMEROPREFIXO, A.CODIGOSTATUSPREFIXO, A.DATAALTERACAO, GETDATE()
	FROM PrefixoLicencaAssociado A  
	INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
	INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD --AND (DATEADD(HOUR, -3, C.DATAMODIFICACAO) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL) 
	INNER JOIN Licenca D ON D.NOME = C.licenca  
	WHERE D.Codigo = A.CodigoLicenca  
	AND C.prefixo = A.NumeroPrefixo  
	AND C.status_da_licenca IS NOT NULL  
  
      
   --ATUALIZA PREFIXOS EXISTENTES   
   UPDATE [dbo].[PrefixoLicencaAssociado]  
      SET   
      [DataAlteracao] = GETDATE()  
      ,[CodigoStatusPrefixo] = CASE c.status_da_licenca WHEN 1 THEN 5  
           WHEN 2 THEN 1  
           WHEN 3 THEN 6  
           WHEN 4 THEN 3  
           WHEN 5 THEN 7  
           WHEN 6 THEN 8  
           WHEN 7 THEN 4 END  
    FROM PrefixoLicencaAssociado A  
   INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
   INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD --AND (DATEADD(HOUR, -3, C.DATAMODIFICACAO) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
   INNER JOIN Licenca D ON D.NOME = C.licenca  
   WHERE D.Codigo = A.CodigoLicenca  
   AND C.prefixo = A.NumeroPrefixo  
   AND C.status_da_licenca IS NOT NULL  
  
  
   --INSERE PREFIXOS NOVOS    
   INSERT INTO [dbo].[PrefixoLicencaAssociado]  
        ([CodigoLicenca]  
        ,[CodigoAssociado]  
        ,[NumeroPrefixo]  
        ,[DataAlteracao]  
        ,[CodigoStatusPrefixo])  
  
   SELECT DISTINCT B.Codigo, C.Codigo, A.PREFIXO, GETDATE()   
    ,CASE A.status_da_licenca WHEN 1 THEN 5  
           WHEN 2 THEN 1  
           WHEN 3 THEN 6  
           WHEN 4 THEN 3  
           WHEN 5 THEN 7  
           WHEN 6 THEN 8  
           WHEN 7 THEN 4 END  
   FROM @TEMPTBLLICENCA A  
   INNER JOIN LICENCA B ON B.NOME = A.LICENCA  
   INNER JOIN Associado C ON C.CAD = A.cad  
   LEFT JOIN PrefixoLicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo AND D.NumeroPrefixo = A.prefixo  
   WHERE D.CodigoAssociado IS NULL  
   AND A.PREFIXO <> '0'  
   AND A.status_da_licenca IS NOT NULL  
  
   --ATUALIZA PRODUTOS GTIN-8  
   INSERT INTO [dbo].[ProdutoHistorico]  
       ([CodigoProduto],[globalTradeItemNumber],[productDescription],[CodigoTipoGTIN],[NrPrefixo],[CodItem],[CodigoAssociado],[CodigoUsuarioCriacao],[CodigoUsuarioAlteracao]  
       ,[CodigoUsuarioExclusao],[tradeItemCountryOfOrigin],[CodigoLingua],[CodigoStatusGTIN],[CodeSegment],[CodeFamily],[CodeClass],[CodeBrick],[countryCode],[CodeBrickAttribute]  
       ,[IndicadorCompartilhaDados],[Observacoes],[Estado],[informationProvider],[brandName],[DataInclusao],[DataAlteracao]  
       ,[DataSuspensao],[DataReativacao],[DataCancelamento],[DataReutilizacao],[modelNumber],[importClassificationType],[importClassificationValue]  
       ,[alternateItemIdentificationId],[alternateItemIdentificationAgency],[minimumTradeItemLifespanFromTimeOfProduction],[startAvailabilityDateTime],[endAvailabilityDateTime]  
       ,[depth],[depthMeasurementUnitCode],[height],[heightMeasurementUnitCode],[width],[widthMeasurementUnitCode],[netContent],[netContentMeasurementUnitCode]  
       ,[grossWeight],[grossWeightMeasurementUnitCode],[netWeight],[netWeightMeasurementUnitCode],[CodigoTipoProduto],[isTradeItemABaseUnit],[isTradeItemAConsumerUnit]  
       ,[isTradeItemAModel],[isTradeItemAnInvoiceUnit],[packagingTypeCode],[PalletTypeCode],[totalQuantityOfNextLowerLevelTradeItem],[StackingFactor]  
       ,[quantityOfTradeItemContainedInACompleteLayer],[quantityOfTradeItemsPerPalletLayer],[quantityOfCompleteLayersContainedInATradeItem],[quantityOfLayersPerPallet]  
       ,[CodigoProdutoOrigem],[deliveryToDistributionCenterTemperatureMinimum],[storageHandlingTempMinimumUOM]  
       ,[storageHandlingTemperatureMaximum],[storageHandlingTemperatureMaximumunitOfMeasure],[isDangerousSubstanceIndicated],[ipiPerc],[isTradeItemAnOrderableUnit]  
       ,[isTradeItemADespatchUnit],[orderSizingFactor],[orderQuantityMultiple],[orderQuantityMinimum],[barcodeCertified],[dataQualityCertified],[dataQualityCertifiedAgencyCode]  
       ,[allGDSNAttributes],[CanalComunicacaoDados],[ProprioDonoInformacao],[ValidadoDonoDaInformacao],[IndicadorGDSN],[VarianteLogistica]  
       ,[DataHistorico])  
  
    SELECT A.[CodigoProduto],A.[globalTradeItemNumber],A.[productDescription],A.[CodigoTipoGTIN],A.[NrPrefixo],A.[CodItem],A.[CodigoAssociado],A.[CodigoUsuarioCriacao],A.[CodigoUsuarioAlteracao]  
     ,A.[CodigoUsuarioExclusao],A.[tradeItemCountryOfOrigin],A.[CodigoLingua],A.[CodigoStatusGTIN],A.[CodeSegment],A.[CodeFamily],A.[CodeClass],A.[CodeBrick],A.[countryCode],A.[CodeBrickAttribute]  
     ,A.[IndicadorCompartilhaDados],A.[Observacoes],A.[Estado],A.[informationProvider],A.[brandName],A.[DataInclusao],A.[DataAlteracao]  
     ,A.[DataSuspensao],A.[DataReativacao],A.[DataCancelamento],A.[DataReutilizacao],A.[modelNumber],A.[importClassificationType],A.[importClassificationValue]  
     ,A.[alternateItemIdentificationId],A.[alternateItemIdentificationAgency],A.[minimumTradeItemLifespanFromTimeOfProduction],A.[startAvailabilityDateTime],A.[endAvailabilityDateTime]  
     ,A.[depth],A.[depthMeasurementUnitCode],A.[height],A.[heightMeasurementUnitCode],A.[width],A.[widthMeasurementUnitCode],A.[netContent],A.[netContentMeasurementUnitCode]  
     ,A.[grossWeight],A.[grossWeightMeasurementUnitCode],A.[netWeight],A.[netWeightMeasurementUnitCode],A.[CodigoTipoProduto],A.[isTradeItemABaseUnit],A.[isTradeItemAConsumerUnit]  
     ,A.[isTradeItemAModel],A.[isTradeItemAnInvoiceUnit],A.[packagingTypeCode],A.[PalletTypeCode],A.[totalQuantityOfNextLowerLevelTradeItem],A.[StackingFactor]  
     ,A.[quantityOfTradeItemContainedInACompleteLayer],A.[quantityOfTradeItemsPerPalletLayer],A.[quantityOfCompleteLayersContainedInATradeItem],A.[quantityOfLayersPerPallet]  
     ,A.[CodigoProdutoOrigem],A.[deliveryToDistributionCenterTemperatureMinimum],A.[storageHandlingTempMinimumUOM]  
     ,A.[storageHandlingTemperatureMaximum],A.[storageHandlingTemperatureMaximumunitOfMeasure],A.[isDangerousSubstanceIndicated],A.[ipiPerc],A.[isTradeItemAnOrderableUnit]  
     ,A.[isTradeItemADespatchUnit],A.[orderSizingFactor],A.[orderQuantityMultiple],A.[orderQuantityMinimum],A.[barcodeCertified],A.[dataQualityCertified],A.[dataQualityCertifiedAgencyCode]  
     ,A.[allGDSNAttributes],A.[CanalComunicacaoDados],A.[ProprioDonoInformacao],A.[ValidadoDonoDaInformacao],A.[IndicadorGDSN],A.[VarianteLogistica]       
    , GETDATE()  
    FROM Produto A  
   INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
   INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD  --AND (DATEADD(HOUR, -3, C.DATAMODIFICACAO) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
   INNER JOIN Licenca D ON D.NOME = C.licenca  
   WHERE C.prefixo = globalTradeItemNumber  
   AND A.CodigoStatusGTIN = 1  
   AND a.productdescription <> c.descricaogtin8  
  
  
   UPDATE [dbo].[Produto]  
   SET productdescription = c.descricaogtin8  
   FROM Produto A  
   INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
   INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD  --AND (DATEADD(HOUR, -3, C.DATAMODIFICACAO) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
   INNER JOIN Licenca D ON D.NOME = C.licenca  
   WHERE C.prefixo = globalTradeItemNumber  
   AND A.CodigoStatusGTIN = 1  
   AND a.productdescription <> c.descricaogtin8  
  
  
   --INSERE NOVOS PRODUTOS GTIN-8  
   INSERT INTO [dbo].[Produto]  
      ([globalTradeItemNumber]  
      ,[productDescription]  
      ,[CodigoTipoGTIN]  
      ,[NrPrefixo]  
      ,[CodItem]  
      ,[CodigoAssociado]  
      ,[CodigoUsuarioCriacao]            
      ,[DataInclusao]  
      ,[CodigoStatusGTIN]  
      ,indicadorgdsn)  
  
   SELECT DISTINCT A.PREFIXO  
       ,a.descricaogtin8  
       ,1  
       ,A.PREFIXO  
       ,SUBSTRING(A.PREFIXO,4,4)  
       ,C.CODIGO  
       ,1  
       ,GETDATE()  
       ,1  
       ,0  
    FROM @TEMPTBLLICENCA A  
    INNER JOIN LICENCA B ON B.NOME = A.LICENCA  
    INNER JOIN Associado C ON C.CAD = A.cad  
    INNER JOIN PrefixoLicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo AND D.NumeroPrefixo = A.prefixo  
    where B.Codigo = 2  
    and not exists (select top 1 x.[globalTradeItemNumber] from produto x where x.[globalTradeItemNumber] = A.PREFIXO)  
    and a.descricaogtin8 is not null  
  
   
   -- Grava histórico e atualiza o usuário principal  
   INSERT INTO [dbo].[UsuarioHistorico]  
        ([Codigo],[Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email],  
        [TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao],[MensagemObservacao],  
        [DataAlteracaoSenha],[QuantidadeTentativa],[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],  
       [TelefoneComercial],[Ramal],[Departamento],[Cargo],[CPFCNPJ],[DataHistorico])  
     
   SELECT DISTINCT C.Codigo,C.Nome,C.CodigoStatusUsuario,C.CodigoPerfil,C.CodigoTipoUsuario,C.Email,  
    C.TelefoneResidencial,C.Senha,C.CodigoUsuarioAlteracao,C.DataAlteracao,C.MensagemObservacao,  
    C.DataAlteracaoSenha,C.QuantidadeTentativa,C.DataUltimoAcesso,C.DataCadastro,C.TelefoneCelular,  
    C.TelefoneComercial,C.Ramal,C.Departamento,C.Cargo,C.CPFCNPJ, GETDATE()  
   FROM Associado  
    INNER JOIN AssociadoUsuario B ON B.CodigoAssociado = ASSOCIADO.Codigo  
    INNER JOIN Usuario C ON C.Codigo = B.CodigoUsuario  
    INNER JOIN @TEMPTBLDADOASSOCIADO D ON D.CAD = ASSOCIADO.CAD --AND (DATEADD(HOUR, -3, D.DATAMODIFICACAO) > ASSOCIADO.DATAATUALIZACAOCRM OR ASSOCIADO.DATAATUALIZACAOCRM IS NULL)
    WHERE B.IndicadorPrincipal = 1  
    

   -- TIRA O USUÁRIO PRINCIPAL DOS USUÁRIOS QUE SÃO DIFERENTES DOS DADOS DO CRM
   --UPDATE AssociadoUsuario
   --REPLACE(REPLACE(REPLACE(B.CPFCNPJ,'.',''),'/',''),'-','')  

   --UPDATE [dbo].[Usuario]       
   --    SET Email = CONVERT(VARCHAR(255),d.email),  
   --     Nome = CONVERT(VARCHAR(255),d.nome),  
   --     CPFCNPJ = CONVERT(VARCHAR(14),d.cpfcnpj)  
    
   --FROM Associado  
   --INNER JOIN AssociadoUsuario B ON B.CodigoAssociado = ASSOCIADO.Codigo  
   --INNER JOIN Usuario C ON C.Codigo = B.CodigoUsuario  
   --INNER JOIN @TEMPTBLDADOASSOCIADO D ON D.CAD = ASSOCIADO.CAD  
   --WHERE B.IndicadorPrincipal = 1  
   --AND NOT EXISTS (SELECT Q.EMAIL FROM USUARIO Q WHERE Q.EMAIL = CONVERT(VARCHAR(255),d.email)) 
  
  
   -- Desbloqueia os usuarios principais de associados desbloqueados  
   UPDATE [dbo].[Usuario]  
      SET codigoStatusUsuario = 1  
   --SELECT C.*  
   FROM Associado  
   INNER JOIN AssociadoUsuario B ON B.CodigoAssociado = ASSOCIADO.Codigo  
   INNER JOIN Usuario C ON C.Codigo = B.CodigoUsuario  
   INNER JOIN @TEMPTBLDADOASSOCIADO D ON D.CAD = ASSOCIADO.CAD --AND (DATEADD(HOUR, -3, D.DATAMODIFICACAO) > ASSOCIADO.DATAATUALIZACAOCRM OR ASSOCIADO.DATAATUALIZACAOCRM IS NULL) 
   WHERE B.IndicadorPrincipal = 1  
   AND C.CodigoStatusUsuario = 3  
   AND D.bl_adimplente = 1   
   AND D.relacionamento =2  


   --INSERE NOVO USUARIO SE EMAIL PRINCIPAL FOI ALTERADO
   
   INSERT INTO [dbo].[Usuario]([Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email]
									,[TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao]
									,[MensagemObservacao],[DataAlteracaoSenha],[QuantidadeTentativa]
									,[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],[TelefoneComercial]
									,[Ramal],[Departamento],[Cargo],[CPFCNPJ])
   SELECT TOP 1 A.NOME,1,2,2,D.EMAIL
        ,NULL,A.SENHA,1,GETDATE()
        ,NULL,NULL,0
        ,NULL,GETDATE(),NULL,NULL
        ,NULL,NULL,NULL,A.CPFCNPJ
	FROM USUARIO A
	INNER JOIN ASSOCIADOUSUARIO B ON B.CODIGOUSUARIO = A.CODIGO
	INNER JOIN ASSOCIADO C ON C.CODIGO =  B.CODIGOASSOCIADO
	INNER JOIN @TEMPTBLDADOASSOCIADO D ON D.CAD = C.CAD
	LEFT JOIN USUARIO E ON E.EMAIL = D.EMAIL
	WHERE E.CODIGO IS NULL
	AND B.INDICADORPRINCIPAL = 1
	ORDER BY A.DATACADASTRO DESC
  	

	INSERT INTO ASSOCIADOUSUARIO (CODIGOASSOCIADO, CODIGOUSUARIO, DATACADASTRO, CODIGOUSUARIOALTERACAO, INDICADORPRINCIPAL)	
	SELECT C.CODIGO, A.CODIGO, getdate(), 1,1
	FROM USUARIO A
	INNER JOIN @TEMPTBLDADOASSOCIADO B ON B.EMAIL = A.EMAIL
	INNER JOIN ASSOCIADO C ON C.CAD =  B.CAD
	LEFT JOIN ASSOCIADOUSUARIO D ON D.CODIGOUSUARIO = A.CODIGO
	WHERE D.CODIGOUSUARIO IS NULL
  
     
  
   --ATUALIZA DATA DE ULTIMA SINCRONIZACAO  
   UPDATE PARAMETRO SET VALOR = CONVERT(VARCHAR(50),GETDATE(),113) WHERE CHAVE = 'parametros.integracoes.ultimaSincronizacaoCrmRgc'  
  
    
  IF @@ERROR <> 0  
  BEGIN     
   ROLLBACK     
   RAISERROR ('Erro ao gravar sincronizar dados CRM.', 16, 1)  
   RETURN  
  END  
  
  
  COMMIT  
  
END