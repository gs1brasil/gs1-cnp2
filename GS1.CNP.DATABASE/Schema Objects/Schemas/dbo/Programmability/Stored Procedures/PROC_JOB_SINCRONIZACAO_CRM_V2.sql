
/****** Object:  StoredProcedure [dbo].[PROC_JOB_SINCRONIZACAO_CRM_V2]    Script Date: 16/08/2016 14:02:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
ALTER PROCEDURE [dbo].[PROC_JOB_SINCRONIZACAO_CRM_V2]

AS BEGIN  
   
 BEGIN TRANSACTION  
    
  --GRAVA O HISTORICO DO ASSOCIADO  
   INSERT INTO [dbo].[AssociadoHistorico] ([CodigoAssociado],[Nome],[Descricao],[Email],[CPFCNPJ],[InscricaoEstadual],[CodigoTipoAssociado],[MensagemObservacao],[DataAlteracao],  
                       [DataAtualizacaoCRM],[DataStatusInadimplente],[CodigoUsuarioAlteracao],[Endereco],[Numero],[Complemento],[Bairro],[Cidade],[UF],[CEP],  
                       [CodigoTipoCompartilhamento],[DataHistorico],[CodigoStatusAssociado],[CodigoSituacaoFinanceira],[CAD])  
  
   SELECT A.[Codigo],A.[Nome],A.[Descricao],A.[Email],A.[CPFCNPJ],A.[InscricaoEstadual],A.[CodigoTipoAssociado],A.[MensagemObservacao],A.[DataAlteracao],  
            A.[DataAtualizacaoCRM],A.[DataStatusInadimplente],A.[CodigoUsuarioAlteracao],A.[Endereco],A.[Numero],A.[Complemento],A.[Bairro],A.[Cidade],A.[UF],A.[CEP],  
            A.[CodigoTipoCompartilhamento],GETDATE(),A.[CodigoStatusAssociado],A.[CodigoSituacaoFinanceira],A.[CAD]  
   FROM ASSOCIADO A  
   --WHERE ASSOCIADO.CODIGOSTATUSASSOCIADO = 1  
   WHERE A.[Codigo] IN (
	   SELECT DISTINCT A.[Codigo]
	   FROM ASSOCIADO A
	   INNER JOIN vw_apenas_associados_atualizados B ON B.ACCOUNTNUMBER = A.CAD  
	   WHERE (DATEADD(HOUR, -3, B.[DATA DE MODIFICAÇÃO])  > A.DATAATUALIZACAOCRM OR A.DATAATUALIZACAOCRM IS NULL)
   )
  
   --ATUALIZA OS ASSOCIADOS ALTERADOS  
   UPDATE [dbo].[Associado]  
   SET  ASSOCIADO.[Nome] = B.[NAME]  
    ,ASSOCIADO.[Descricao] = B.[SMART_NOMEFANTASIA]  
    ,ASSOCIADO.[Email] = B.EMAIL  
    ,ASSOCIADO.[CPFCNPJ] = REPLACE(REPLACE(REPLACE(B.[SMART_CNPJCPF],'.',''),'/',''),'-','')  
    ,ASSOCIADO.[InscricaoEstadual] = B.[SMART_INSCRICAOESTADUAL]      
    ,ASSOCIADO.[CodigoTipoAssociado] = CASE B.[STATUS_ADIMPLENTE] WHEN 1 THEN 1 ELSE 2 END          
    ,ASSOCIADO.[DataAtualizacaoCRM] = GETDATE()      
    ,ASSOCIADO.[DataStatusInadimplente] = CASE B.[STATUS_ADIMPLENTE] WHEN 1 THEN NULL ELSE GETDATE() END      
    ,ASSOCIADO.[CodigoUsuarioAlteracao] = 1  
    ,ASSOCIADO.[Endereco] = B.[SMART_LOGRADOUROPRINCIPAL]  
    ,ASSOCIADO.[Numero] = B.[SMART_NUMERO]  
    ,ASSOCIADO.[Complemento] = B.[SMART_COMPLEMENTOPRINCIPAL]  
    ,ASSOCIADO.[Bairro] = B.[SMART_BAIRROPRINCIPAL]  
    ,ASSOCIADO.[Cidade] = B.[ADDRESS1_CITY]  
    ,ASSOCIADO.[UF] = B.[SMART_ESTADONAME]  
    ,ASSOCIADO.[CEP] = B.[ADDRESS1_POSTALCODE]      
    ,ASSOCIADO.[CodigoStatusAssociado] = CASE B.[gs1br_cnp20]   
              WHEN 1 THEN   
               CASE B.[STATUS_ADIMPLENTE] WHEN 1 THEN  
                CASE B.[SMART_CONDICAODERELACIONAMENTO]   
                 WHEN 2 THEN 1   
                 ELSE 6 --Não Associado  
                END  
                ELSE 5 END --Inadimplente  
              ELSE  
               4 -- Sem acesso CNP 2.0  
              END  
  
    ,ASSOCIADO.[CodigoSituacaoFinanceira] = CASE B.[STATUS_ADIMPLENTE] WHEN 1 THEN 1 ELSE 2 END  
    ,ASSOCIADO.[CAD] = B.[ACCOUNTNUMBER]  
    ,ASSOCIADO.[Pais] = B.[SMART_PAISNAME]  
   FROM ASSOCIADO  
   INNER JOIN vw_apenas_associados_atualizados B ON B.[ACCOUNTNUMBER] = ASSOCIADO.CAD  
   WHERE (DATEADD(HOUR, -3, B.[DATA DE MODIFICAÇÃO]) > ASSOCIADO.DATAATUALIZACAOCRM OR ASSOCIADO.DATAATUALIZACAOCRM IS NULL)
     
  
   --ATUALIZA O CONTATO PRINCIPAL    
   UPDATE   
    CONTATOASSOCIADO  
   SET     
     [Nome] = [SMART_PESSOANAME]  
    ,[CPFCNPJ] = [SMART_CPF]     
    ,[Telefone1] = [representante_telephone1]  
    ,[Telefone2] = [representante_telephone2]  
    ,[Telefone3] = [representante_telephone3]  
    ,[Email] = [EMAILADDRESS1]  
    ,[Email2] = [EMAILADDRESS2]     
    ,[DataAtualizacaoCRM] = GETDATE()     
   FROM CONTATOASSOCIADO A  
   INNER JOIN ASSOCIADO B ON B.Codigo = A.CodigoAssociado  
   INNER JOIN vw_apenas_associados_atualizados C ON C.[ACCOUNTNUMBER] = B.CAD   
   AND REPLACE(REPLACE(REPLACE(C.[SMART_CPF],'.',''),'/',''),'-','') = REPLACE(REPLACE(REPLACE(A.CPFCNPJ,'.',''),'/',''),'-','')  
   AND (DATEADD(HOUR, -3, C.[DATA DE MODIFICAÇÃO]) > B.DATAATUALIZACAOCRM OR B.DATAATUALIZACAOCRM IS NULL)
  
  
   --INSERE CONTATO PRINCIPAL NAO EXISTENTE  
   INSERT INTO [dbo].[ContatoAssociado]  
   ([CodigoAssociado],[Nome],[CPFCNPJ],[CodigoTipoContato]  
   ,[Telefone1],[Telefone2],[Telefone3]  
   ,[Email],[Email2],[Status],[Observacao],[IndicadorPrincipal],[DataAtualizacaoCRM])  
   SELECT B.Codigo, A.SMART_PESSOANAME, A.SMART_CPF, 1  
       ,A.representante_telephone1, A.representante_telephone2, A.representante_telephone3  
       ,A.[EMAILADDRESS1], A.[EMAILADDRESS2],1,NULL,1,GETDATE()  
   FROM vw_apenas_associados_atualizados A  
   INNER JOIN ASSOCIADO B ON B.CAD = A.[ACCOUNTNUMBER] AND (DATEADD(HOUR, -3, A.[DATA DE MODIFICAÇÃO]) > B.DATAATUALIZACAOCRM OR B.DATAATUALIZACAOCRM IS NULL)
   LEFT JOIN ContatoAssociado C ON C.CodigoAssociado = B.Codigo   
   AND REPLACE(REPLACE(REPLACE(a.SMART_CPF,'.',''),'/',''),'-','') = REPLACE(REPLACE(REPLACE(C.CPFCNPJ,'.',''),'/',''),'-','')  
   WHERE C.Codigo IS NULL  
  
     
--INSERE GLN INFORMATIVO NAO EXISTENTE    
	INSERT INTO [dbo].[LocalizacaoFisica] ([Nome],[Descricao],[Status],[Prefixo],[GLN],[NumeroItem],[CodigoPapelGLN]  
    ,[Endereco],[Numero],[Complemento],[CEP],[Cidade],[Estado],[Pais],[Bairro]  
    ,[NomeImagem],[Observacao],[DataAlteracao],[CodigoUsuarioAlteracao],[IndicadorPrincipal]  
    ,[DataCancelamento],[DataSuspensao],[DataReutilizacao],[CodigoAssociado],[Latitude],[Longitude],[Telefone],[Email])  
      
  
    
   SELECT DISTINCT a.[NAME], a.[NAME], 1
   
   
    , COALESCE(

   
			(SELECT Y.NUMEROPREFIXO FROM PREFIXOLICENCAASSOCIADO Y WHERE Y.CODIGOASSOCIADO = B.Codigo
			AND CHARINDEX(CAST(Y.NUMEROPREFIXO AS VARCHAR(12)),a.[SMART_CODIGOGLN]) > 0)
		, SUBSTRING(CAST(a.[SMART_CODIGOGLN] AS VARCHAR(50)),1,12)
		)
   
   
   , a.[SMART_CODIGOGLN]
   
	, CASE WHEN ((SELECT Y.NUMEROPREFIXO FROM PREFIXOLICENCAASSOCIADO Y WHERE Y.CODIGOASSOCIADO = B.Codigo
			AND CHARINDEX(CAST(Y.NUMEROPREFIXO AS VARCHAR(12)),a.[SMART_CODIGOGLN]) > 0)
				IS NOT NULL ) THEN
				REPLACE(	SUBSTRING(CAST(a.[SMART_CODIGOGLN] AS VARCHAR(50)),1,12),
							(SELECT Y.NUMEROPREFIXO FROM PREFIXOLICENCAASSOCIADO Y WHERE Y.CODIGOASSOCIADO = B.Codigo
							AND CHARINDEX(CAST(Y.NUMEROPREFIXO AS VARCHAR(12)),a.[SMART_CODIGOGLN]) > 0)
							,'' 
							
						)
			ELSE a.[SMART_CODIGOGLN]
		END      
   , 1   
    ,a.[SMART_LOGRADOUROPRINCIPAL], a.[SMART_NUMERO], a.[SMART_COMPLEMENTOPRINCIPAL], a.[ADDRESS1_POSTALCODE], a.[ADDRESS1_CITY], a.[SMART_ESTADONAME], a.[SMART_PAISNAME], a.[SMART_BAIRROPRINCIPAL]  
    ,NULL, NULL, GETDATE(), 1, 1, NULL, NULL, NULL, B.Codigo, NULL, NULL, a.[TELEPHONE1], a.[EMAIL]    
   FROM vw_apenas_associados_atualizados A  
   INNER JOIN ASSOCIADO B ON B.CAD = A.[ACCOUNTNUMBER]
   LEFT JOIN LocalizacaoFisica C ON  C.gln =  CONVERT(BIGINT,A.[SMART_CODIGOGLN])
   
   WHERE C.Codigo IS NULL  
   AND  CONVERT(BIGINT,A.[SMART_CODIGOGLN]) > 0  
   and a.[SMART_NUMERO] is not null
  
    

	----HISTORICO DAS LICENCAS EXISTENTES
	INSERT INTO LICENCAASSOCIADOHISTORICO
	SELECT DISTINCT A.CODIGOLICENCA, A.CODIGOASSOCIADO, A.STATUS, A.DATAANIVERSARIO, A.DATAALTERACAO, GETDATE()
	   FROM LicencaAssociado A  
     INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
     INNER JOIN vw_LICENCAS_atualizadas C ON C.CAD = B.CAD AND (DATEADD(HOUR, -3, C.[DATA DE MODIFICAÇÃO]) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
     INNER JOIN Licenca D ON D.NOME = C.licenca  
     WHERE D.Codigo = A.CodigoLicenca  

   --ATUALIZA LICENCAS EXISTENTES    
   UPDATE [dbo].[LicencaAssociado]  
     SET [DataAniversario] = C.data_aniversario --CASE C.data_aniversario WHEN '0001-01-01T00:00:00' THEN NULL WHEN NULL THEN NULL ELSE CONVERT(datetime,C.data_aniversario) END  
      ,[DataAlteracao] = GETDATE()  
   FROM LicencaAssociado A  
     INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
     INNER JOIN vw_LICENCAS_atualizadas C ON C.CAD = B.CAD AND (DATEADD(HOUR, -3, C.[DATA DE MODIFICAÇÃO]) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
     INNER JOIN Licenca D ON D.NOME = C.licenca  
     WHERE D.Codigo = A.CodigoLicenca  
  
  
   --INSERE LICENCAS NOVAS  
   INSERT INTO [dbo].[LicencaAssociado]  
        ([CodigoLicenca]  
        ,[CodigoAssociado]  
        ,[Status]  
        ,[DataAniversario]  
        ,[DataAlteracao])  
  
   SELECT DISTINCT B.CODIGO, C.Codigo,   
     CASE A.[STATUS_DA_LICENÇA] WHEN 1 THEN 5  
         WHEN 2 THEN 1  
         WHEN 3 THEN 6  
         WHEN 4 THEN 3  
         WHEN 5 THEN 7  
         WHEN 6 THEN 8  
         WHEN 7 THEN 4 END  
     , CONVERT(datetime,A.data_aniversario) --CASE A.data_aniversario WHEN '0001-01-01T00:00:00' THEN NULL WHEN NULL THEN NULL ELSE CONVERT(datetime,A.data_aniversario) END  
     ,GETDATE()  
   FROM vw_LICENCAS_atualizadas A  
   INNER JOIN LICENCA B ON B.NOME = A.LICENCA  
   INNER JOIN Associado C ON C.CAD = A.cad  
   LEFT JOIN LicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo  
   where D.CodigoAssociado is null  
   AND A.[STATUS_DA_LICENÇA] IS NOT NULL  
  
  
  --HISTORICO DOS PREFIXOS EXISTENTES
	INSERT INTO PrefixoLicencaAssociadoHistorico
	SELECT A.CODIGOLICENCA, A.CODIGOASSOCIADO, A.NUMEROPREFIXO, A.CODIGOSTATUSPREFIXO, A.DATAALTERACAO, GETDATE()
	FROM PrefixoLicencaAssociado A  
	INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
	INNER JOIN vw_LICENCAS_atualizadas C ON C.CAD = B.CAD AND (DATEADD(HOUR, -3, C.[DATA DE MODIFICAÇÃO]) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL) 
	INNER JOIN Licenca D ON D.NOME = C.licenca  
	WHERE D.Codigo = A.CodigoLicenca  
	AND C.prefixo = A.NumeroPrefixo  
	AND C.[STATUS_DA_LICENÇA] IS NOT NULL  
  
      
   --ATUALIZA PREFIXOS EXISTENTES   
   UPDATE [dbo].[PrefixoLicencaAssociado]  
      SET   
      [DataAlteracao] = GETDATE()  
      ,[CodigoStatusPrefixo] = CASE c.[STATUS_DA_LICENÇA] WHEN 1 THEN 5  
           WHEN 2 THEN 1  
           WHEN 3 THEN 6  
           WHEN 4 THEN 3  
           WHEN 5 THEN 7  
           WHEN 6 THEN 8  
           WHEN 7 THEN 4 END  
    FROM PrefixoLicencaAssociado A  
   INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
   INNER JOIN vw_LICENCAS_atualizadas C ON C.CAD = B.CAD AND (DATEADD(HOUR, -3, C.[DATA DE MODIFICAÇÃO]) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
   INNER JOIN Licenca D ON D.NOME = C.licenca  
   WHERE D.Codigo = A.CodigoLicenca  
   AND C.prefixo = A.NumeroPrefixo  
   AND C.[STATUS_DA_LICENÇA] IS NOT NULL  
  
  
   --INSERE PREFIXOS NOVOS    
   INSERT INTO [dbo].[PrefixoLicencaAssociado]  
        ([CodigoLicenca]  
        ,[CodigoAssociado]  
        ,[NumeroPrefixo]  
        ,[DataAlteracao]  
        ,[CodigoStatusPrefixo])  
  
   SELECT DISTINCT B.Codigo, C.Codigo, A.PREFIXO, GETDATE()   
    ,CASE A.[STATUS_DA_LICENÇA] WHEN 1 THEN 5  
           WHEN 2 THEN 1  
           WHEN 3 THEN 6  
           WHEN 4 THEN 3  
           WHEN 5 THEN 7  
           WHEN 6 THEN 8  
           WHEN 7 THEN 4 END  
   FROM vw_LICENCAS_atualizadas A  
   INNER JOIN LICENCA B ON B.NOME = A.LICENCA  
   INNER JOIN Associado C ON C.CAD = A.cad  
   LEFT JOIN PrefixoLicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo AND D.NumeroPrefixo = A.prefixo  
   WHERE D.CodigoAssociado IS NULL  
   AND A.PREFIXO <> '0'  
   AND A.[STATUS_DA_LICENÇA] IS NOT NULL  
  
   --ATUALIZA PRODUTOS GTIN-8  
   INSERT INTO [dbo].[ProdutoHistorico]  
       ([CodigoProduto],[globalTradeItemNumber],[productDescription],[CodigoTipoGTIN],[NrPrefixo],[CodItem],[CodigoAssociado],[CodigoUsuarioCriacao],[CodigoUsuarioAlteracao]  
       ,[CodigoUsuarioExclusao],[tradeItemCountryOfOrigin],[CodigoLingua],[CodigoStatusGTIN],[CodeSegment],[CodeFamily],[CodeClass],[CodeBrick],[countryCode],[CodeBrickAttribute]  
       ,[IndicadorCompartilhaDados],[Observacoes],[Estado],[informationProvider],[brandName],[DataInclusao],[DataAlteracao]  
       ,[DataSuspensao],[DataReativacao],[DataCancelamento],[DataReutilizacao],[modelNumber],[importClassificationType],[importClassificationValue]  
       ,[alternateItemIdentificationId],[alternateItemIdentificationAgency],[minimumTradeItemLifespanFromTimeOfProduction],[startAvailabilityDateTime],[endAvailabilityDateTime]  
       ,[depth],[depthMeasurementUnitCode],[height],[heightMeasurementUnitCode],[width],[widthMeasurementUnitCode],[netContent],[netContentMeasurementUnitCode]  
       ,[grossWeight],[grossWeightMeasurementUnitCode],[netWeight],[netWeightMeasurementUnitCode],[CodigoTipoProduto],[isTradeItemABaseUnit],[isTradeItemAConsumerUnit]  
       ,[isTradeItemAModel],[isTradeItemAnInvoiceUnit],[packagingTypeCode],[PalletTypeCode],[totalQuantityOfNextLowerLevelTradeItem],[StackingFactor]  
       ,[quantityOfTradeItemContainedInACompleteLayer],[quantityOfTradeItemsPerPalletLayer],[quantityOfCompleteLayersContainedInATradeItem],[quantityOfLayersPerPallet]  
       ,[CodigoProdutoOrigem],[deliveryToDistributionCenterTemperatureMinimum],[storageHandlingTempMinimumUOM]  
       ,[storageHandlingTemperatureMaximum],[storageHandlingTemperatureMaximumunitOfMeasure],[isDangerousSubstanceIndicated],[ipiPerc],[isTradeItemAnOrderableUnit]  
       ,[isTradeItemADespatchUnit],[orderSizingFactor],[orderQuantityMultiple],[orderQuantityMinimum],[barcodeCertified],[dataQualityCertified],[dataQualityCertifiedAgencyCode]  
       ,[allGDSNAttributes],[CanalComunicacaoDados],[ProprioDonoInformacao],[ValidadoDonoDaInformacao],[IndicadorGDSN],[VarianteLogistica]  
       ,[DataHistorico])  
  
    SELECT A.[CodigoProduto],A.[globalTradeItemNumber],A.[productDescription],A.[CodigoTipoGTIN],A.[NrPrefixo],A.[CodItem],A.[CodigoAssociado],A.[CodigoUsuarioCriacao],A.[CodigoUsuarioAlteracao]  
     ,A.[CodigoUsuarioExclusao],A.[tradeItemCountryOfOrigin],A.[CodigoLingua],A.[CodigoStatusGTIN],A.[CodeSegment],A.[CodeFamily],A.[CodeClass],A.[CodeBrick],A.[countryCode],A.[CodeBrickAttribute]  
     ,A.[IndicadorCompartilhaDados],A.[Observacoes],A.[Estado],A.[informationProvider],A.[brandName],A.[DataInclusao],A.[DataAlteracao]  
     ,A.[DataSuspensao],A.[DataReativacao],A.[DataCancelamento],A.[DataReutilizacao],A.[modelNumber],A.[importClassificationType],A.[importClassificationValue]  
     ,A.[alternateItemIdentificationId],A.[alternateItemIdentificationAgency],A.[minimumTradeItemLifespanFromTimeOfProduction],A.[startAvailabilityDateTime],A.[endAvailabilityDateTime]  
     ,A.[depth],A.[depthMeasurementUnitCode],A.[height],A.[heightMeasurementUnitCode],A.[width],A.[widthMeasurementUnitCode],A.[netContent],A.[netContentMeasurementUnitCode]  
     ,A.[grossWeight],A.[grossWeightMeasurementUnitCode],A.[netWeight],A.[netWeightMeasurementUnitCode],A.[CodigoTipoProduto],A.[isTradeItemABaseUnit],A.[isTradeItemAConsumerUnit]  
     ,A.[isTradeItemAModel],A.[isTradeItemAnInvoiceUnit],A.[packagingTypeCode],A.[PalletTypeCode],A.[totalQuantityOfNextLowerLevelTradeItem],A.[StackingFactor]  
     ,A.[quantityOfTradeItemContainedInACompleteLayer],A.[quantityOfTradeItemsPerPalletLayer],A.[quantityOfCompleteLayersContainedInATradeItem],A.[quantityOfLayersPerPallet]  
     ,A.[CodigoProdutoOrigem],A.[deliveryToDistributionCenterTemperatureMinimum],A.[storageHandlingTempMinimumUOM]  
     ,A.[storageHandlingTemperatureMaximum],A.[storageHandlingTemperatureMaximumunitOfMeasure],A.[isDangerousSubstanceIndicated],A.[ipiPerc],A.[isTradeItemAnOrderableUnit]  
     ,A.[isTradeItemADespatchUnit],A.[orderSizingFactor],A.[orderQuantityMultiple],A.[orderQuantityMinimum],A.[barcodeCertified],A.[dataQualityCertified],A.[dataQualityCertifiedAgencyCode]  
     ,A.[allGDSNAttributes],A.[CanalComunicacaoDados],A.[ProprioDonoInformacao],A.[ValidadoDonoDaInformacao],A.[IndicadorGDSN],A.[VarianteLogistica]       
    , GETDATE()  
    -- FROM Produto A  
   --INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
   --INNER JOIN vw_LICENCAS_atualizadas C ON C.CAD = B.CAD  AND (DATEADD(HOUR, -3, C.[DATA DE MODIFICAÇÃO]) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
   --INNER JOIN Licenca D ON D.NOME = C.licenca  
   --WHERE C.prefixo = globalTradeItemNumber  
   --AND A.CodigoStatusGTIN = 1  
   --AND a.productdescription <> c.[DESCRIÇÃO_GTIN8]
   FROM vw_LICENCAS_atualizadas C
	INNER JOIN ASSOCIADO B ON B.CAD = C.CAD  
	INNER JOIN PRODUTO A ON A.globalTradeItemNumber = C.prefixo
	WHERE A.CODIGOSTATUSGTIN = 1  
	AND A.CODIGOTIPOGTIN = 1
	AND A.PRODUCTDESCRIPTION <> c.[DESCRIÇÃO_GTIN8] 
  
  
   UPDATE [dbo].[Produto]  
   SET productdescription = c.[DESCRIÇÃO_GTIN8]  
    --FROM Produto A  
   --INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado  
   --INNER JOIN vw_LICENCAS_atualizadas C ON C.CAD = B.CAD  AND (DATEADD(HOUR, -3, C.[DATA DE MODIFICAÇÃO]) > A.DATAALTERACAO OR A.DATAALTERACAO IS NULL)
   --INNER JOIN Licenca D ON D.NOME = C.licenca  
   --WHERE C.prefixo = globalTradeItemNumber  
   --AND A.CodigoStatusGTIN = 1  
   --AND a.productdescription <> c.[DESCRIÇÃO_GTIN8]  
   
   FROM vw_LICENCAS_atualizadas C
	INNER JOIN ASSOCIADO B ON B.CAD = C.CAD  
	INNER JOIN PRODUTO A ON A.globalTradeItemNumber = C.prefixo

	WHERE A.globalTradeItemNumber = globalTradeItemNumber 
	 AND A.CODIGOSTATUSGTIN = 1  
	 AND A.CODIGOTIPOGTIN = 1
	 AND A.PRODUCTDESCRIPTION <> c.[DESCRIÇÃO_GTIN8] 
  
  
   --INSERE NOVOS PRODUTOS GTIN-8  
   INSERT INTO [dbo].[Produto]  
      ([globalTradeItemNumber]  
      ,[productDescription]  
      ,[CodigoTipoGTIN]  
      ,[NrPrefixo]  
      ,[CodItem]  
      ,[CodigoAssociado]  
      ,[CodigoUsuarioCriacao]            
      ,[DataInclusao]  
      ,[CodigoStatusGTIN]  
      ,indicadorgdsn)  
  
   SELECT DISTINCT A.PREFIXO  
       ,a.[DESCRIÇÃO_GTIN8]  
       ,1  
       ,A.PREFIXO  
       ,SUBSTRING(A.PREFIXO,4,4)  
       ,C.CODIGO  
       ,1  
       ,GETDATE()  
       ,1  
       ,0  
    FROM vw_LICENCAS_atualizadas A  
    INNER JOIN LICENCA B ON B.NOME = A.LICENCA  
    INNER JOIN Associado C ON C.CAD = A.cad  
    INNER JOIN PrefixoLicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo AND D.NumeroPrefixo = A.prefixo  
    where B.Codigo = 2  
    and not exists (select top 1 x.[globalTradeItemNumber] from produto x where x.[globalTradeItemNumber] = A.PREFIXO)  
    and a.[DESCRIÇÃO_GTIN8] is not null  
  
   
   -- Grava histórico e atualiza o usuário principal  
   INSERT INTO [dbo].[UsuarioHistorico]  
        ([Codigo],[Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email],  
        [TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao],[MensagemObservacao],  
        [DataAlteracaoSenha],[QuantidadeTentativa],[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],  
       [TelefoneComercial],[Ramal],[Departamento],[Cargo],[CPFCNPJ],[DataHistorico])  
     
   SELECT DISTINCT C.Codigo,C.Nome,C.CodigoStatusUsuario,C.CodigoPerfil,C.CodigoTipoUsuario,C.Email,  
    C.TelefoneResidencial,C.Senha,C.CodigoUsuarioAlteracao,C.DataAlteracao,C.MensagemObservacao,  
    C.DataAlteracaoSenha,C.QuantidadeTentativa,C.DataUltimoAcesso,C.DataCadastro,C.TelefoneCelular,  
    C.TelefoneComercial,C.Ramal,C.Departamento,C.Cargo,C.CPFCNPJ, GETDATE()  
   FROM Associado  
    INNER JOIN AssociadoUsuario B ON B.CodigoAssociado = ASSOCIADO.Codigo  
    INNER JOIN Usuario C ON C.Codigo = B.CodigoUsuario  
    INNER JOIN vw_apenas_associados_atualizados D ON D.[ACCOUNTNUMBER] = ASSOCIADO.CAD AND (DATEADD(HOUR, -3, D.[DATA DE MODIFICAÇÃO]) > ASSOCIADO.DATAATUALIZACAOCRM OR ASSOCIADO.DATAATUALIZACAOCRM IS NULL)
    WHERE B.IndicadorPrincipal = 1  
    
   -- Desbloqueia os usuarios principais de associados desbloqueados  
   UPDATE [dbo].[Usuario]  
      SET codigoStatusUsuario = 1  
   --SELECT C.*  
   FROM Associado  
   INNER JOIN AssociadoUsuario B ON B.CodigoAssociado = ASSOCIADO.Codigo  
   INNER JOIN Usuario C ON C.Codigo = B.CodigoUsuario  
   INNER JOIN vw_apenas_associados_atualizados D ON D.[ACCOUNTNUMBER] = ASSOCIADO.CAD AND (DATEADD(HOUR, -3, D.[DATA DE MODIFICAÇÃO]) > ASSOCIADO.DATAATUALIZACAOCRM OR ASSOCIADO.DATAATUALIZACAOCRM IS NULL) 
   WHERE B.IndicadorPrincipal = 1  
   AND C.CodigoStatusUsuario = 3  
   AND D.[STATUS_ADIMPLENTE] = 1   
   AND D.[SMART_CONDICAODERELACIONAMENTO] =2  


   --INSERE NOVO USUARIO SE EMAIL PRINCIPAL FOI ALTERADO
   
   INSERT INTO [dbo].[Usuario]([Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email]
									,[TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao]
									,[MensagemObservacao],[DataAlteracaoSenha],[QuantidadeTentativa]
									,[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],[TelefoneComercial]
									,[Ramal],[Departamento],[Cargo],[CPFCNPJ])
   SELECT TOP 1 A.NOME,1,2,2,D.EMAIL
        ,NULL,A.SENHA,1,GETDATE()
        ,NULL,NULL,0
        ,NULL,GETDATE(),NULL,NULL
        ,NULL,NULL,NULL,A.CPFCNPJ
	FROM USUARIO A
	INNER JOIN ASSOCIADOUSUARIO B ON B.CODIGOUSUARIO = A.CODIGO
	INNER JOIN ASSOCIADO C ON C.CODIGO =  B.CODIGOASSOCIADO
	INNER JOIN vw_apenas_associados_atualizados D ON D.[ACCOUNTNUMBER] = C.CAD
	LEFT JOIN USUARIO E ON E.EMAIL = D.EMAIL
	WHERE E.CODIGO IS NULL
	AND B.INDICADORPRINCIPAL = 1
	ORDER BY A.DATACADASTRO DESC
  	

	INSERT INTO ASSOCIADOUSUARIO (CODIGOASSOCIADO, CODIGOUSUARIO, DATACADASTRO, CODIGOUSUARIOALTERACAO, INDICADORPRINCIPAL)	
	SELECT DISTINCT C.CODIGO, A.CODIGO, getdate(), 1,1
	FROM USUARIO A
	INNER JOIN vw_apenas_associados_atualizados B ON B.EMAIL = A.EMAIL
	INNER JOIN ASSOCIADO C ON C.CAD =  B.[ACCOUNTNUMBER]
	LEFT JOIN ASSOCIADOUSUARIO D ON D.CODIGOUSUARIO = A.CODIGO
	WHERE D.CODIGOUSUARIO IS NULL



	-- Transferencia


	  INSERT INTO [dbo].[ProdutoHistorico]
	([CodigoProduto],[globalTradeItemNumber],[productDescription],[CodigoTipoGTIN],[NrPrefixo]
	,[CodItem],[VarianteLogistica],[CodigoAssociado],[CodigoUsuarioCriacao],[CodigoUsuarioAlteracao],[CodigoUsuarioExclusao],[tradeItemCountryOfOrigin]
	,[CodigoStatusGTIN],[CodeSegment],[CodeFamily],[CodeClass],[CodeBrick],[countryCode],[CodeBrickAttribute],[IndicadorCompartilhaDados],[Observacoes],[Estado]
	,[informationProvider],[brandName],[DataInclusao],[DataAlteracao],[DataSuspensao],[DataReativacao],[DataCancelamento]
	,[DataReutilizacao],[modelNumber],[importClassificationType],[importClassificationValue],[alternateItemIdentificationId],[alternateItemIdentificationAgency]
	,[minimumTradeItemLifespanFromTimeOfProduction],[startAvailabilityDateTime],[endAvailabilityDateTime],[depth],[depthMeasurementUnitCode],[height]
	,[heightMeasurementUnitCode],[width],[widthMeasurementUnitCode],[netContent],[netContentMeasurementUnitCode],[grossWeight],[grossWeightMeasurementUnitCode],[netWeight]
	,[netWeightMeasurementUnitCode],[CodigoTipoProduto],[isTradeItemABaseUnit],[isTradeItemAConsumerUnit],[isTradeItemAModel],[isTradeItemAnInvoiceUnit],[packagingTypeCode]
	,[PalletTypeCode],[totalQuantityOfNextLowerLevelTradeItem],[StackingFactor],[quantityOfTradeItemContainedInACompleteLayer],[quantityOfTradeItemsPerPalletLayer]
	,[quantityOfCompleteLayersContainedInATradeItem],[quantityOfLayersPerPallet],[CodigoProdutoOrigem],[deliveryToDistributionCenterTemperatureMinimum]
	,[storageHandlingTempMinimumUOM],[storageHandlingTemperatureMaximum],[storageHandlingTemperatureMaximumunitOfMeasure],[isDangerousSubstanceIndicated]
	,[ipiPerc],[isTradeItemAnOrderableUnit],[isTradeItemADespatchUnit],[orderSizingFactor],[orderQuantityMultiple],[orderQuantityMinimum],[barcodeCertified]
	,[dataQualityCertified],[dataQualityCertifiedAgencyCode],[allGDSNAttributes],[CanalComunicacaoDados],[ProprioDonoInformacao],[ValidadoDonoDaInformacao],[IndicadorGDSN],[CodigoLingua]
	,[DataHistorico])
   

	SELECT A.[CodigoProduto]
	,A.[globalTradeItemNumber],A.[productDescription],A.[CodigoTipoGTIN],A.[NrPrefixo]
	,A.[CodItem],A.[VarianteLogistica],A.[CodigoAssociado],A.[CodigoUsuarioCriacao],A.[CodigoUsuarioAlteracao],A.[CodigoUsuarioExclusao],A.[tradeItemCountryOfOrigin]
	,A.[CodigoStatusGTIN],A.[CodeSegment],A.[CodeFamily],A.[CodeClass],A.[CodeBrick],A.[countryCode],A.[CodeBrickAttribute],A.[IndicadorCompartilhaDados],A.[Observacoes],A.[Estado]
	,A.[informationProvider],A.[brandName],A.[DataInclusao],A.[DataAlteracao],A.[DataSuspensao],A.[DataReativacao],A.[DataCancelamento]
	,A.[DataReutilizacao],A.[modelNumber],A.[importClassificationType],A.[importClassificationValue],A.[alternateItemIdentificationId],A.[alternateItemIdentificationAgency]
	,A.[minimumTradeItemLifespanFromTimeOfProduction],A.[startAvailabilityDateTime],A.[endAvailabilityDateTime],A.[depth],A.[depthMeasurementUnitCode],A.[height]
	,A.[heightMeasurementUnitCode],A.[width],A.[widthMeasurementUnitCode],A.[netContent],A.[netContentMeasurementUnitCode],A.[grossWeight],A.[grossWeightMeasurementUnitCode],A.[netWeight]
	,A.[netWeightMeasurementUnitCode],A.[CodigoTipoProduto],A.[isTradeItemABaseUnit],A.[isTradeItemAConsumerUnit],A.[isTradeItemAModel],A.[isTradeItemAnInvoiceUnit],A.[packagingTypeCode]
	,A.[PalletTypeCode],A.[totalQuantityOfNextLowerLevelTradeItem],A.[StackingFactor],A.[quantityOfTradeItemContainedInACompleteLayer],A.[quantityOfTradeItemsPerPalletLayer]
	,A.[quantityOfCompleteLayersContainedInATradeItem],A.[quantityOfLayersPerPallet],A.[CodigoProdutoOrigem],A.[deliveryToDistributionCenterTemperatureMinimum]
	,A.[storageHandlingTempMinimumUOM],A.[storageHandlingTemperatureMaximum],A.[storageHandlingTemperatureMaximumunitOfMeasure],A.[isDangerousSubstanceIndicated]
	,A.[ipiPerc],A.[isTradeItemAnOrderableUnit],A.[isTradeItemADespatchUnit],A.[orderSizingFactor],A.[orderQuantityMultiple],A.[orderQuantityMinimum],A.[barcodeCertified]
	,A.[dataQualityCertified],A.[dataQualityCertifiedAgencyCode],A.[allGDSNAttributes],A.[CanalComunicacaoDados],A.[ProprioDonoInformacao],A.[ValidadoDonoDaInformacao],A.[IndicadorGDSN],A.[CodigoLingua]
	,GETDATE()
	FROM PRODUTO A
	WHERE CODIGOPRODUTO IN (SELECT DISTINCT A.CODIGOPRODUTO
							FROM PRODUTO A
							INNER JOIN PREFIXOLICENCAASSOCIADO B ON B.CODIGOASSOCIADO = A.CODIGOASSOCIADO AND B.NUMEROPREFIXO = A.NRPREFIXO
								AND CASE A.CODIGOTIPOGTIN WHEN 1 THEN 3
										  WHEN 2 THEN 1
										  WHEN 3 THEN 1
										  WHEN 4 THEN 1 END = B.CODIGOLICENCA
							INNER JOIN ASSOCIADO C ON C.CODIGO = A.CODIGOASSOCIADO
							INNER JOIN STATUSASSOCIADO D ON D.CODIGO = C.CODIGOSTATUSASSOCIADO
							INNER JOIN STATUSGTIN E ON E.CODIGO = A.CODIGOSTATUSGTIN
							WHERE B.CODIGOSTATUSPREFIXO = 4
							AND A.CODIGOSTATUSGTIN <> 7)




	UPDATE PRODUTO 
	SET CODIGOSTATUSGTIN = 7
	WHERE CODIGOPRODUTO IN (SELECT  DISTINCT A.CODIGOPRODUTO
							FROM PRODUTO A
							INNER JOIN PREFIXOLICENCAASSOCIADO B ON B.CODIGOASSOCIADO = A.CODIGOASSOCIADO AND B.NUMEROPREFIXO = A.NRPREFIXO
								AND CASE A.CODIGOTIPOGTIN WHEN 1 THEN 3
										  WHEN 2 THEN 1
										  WHEN 3 THEN 1
										  WHEN 4 THEN 1 END = B.CODIGOLICENCA
							INNER JOIN ASSOCIADO C ON C.CODIGO = A.CODIGOASSOCIADO
							INNER JOIN STATUSASSOCIADO D ON D.CODIGO = C.CODIGOSTATUSASSOCIADO
							INNER JOIN STATUSGTIN E ON E.CODIGO = A.CODIGOSTATUSGTIN
							WHERE B.CODIGOSTATUSPREFIXO = 4
							AND A.CODIGOSTATUSGTIN <> 7)       
  
   --ATUALIZA DATA DE ULTIMA SINCRONIZACAO  
   UPDATE PARAMETRO SET VALOR = CONVERT(VARCHAR(50),GETDATE(),113) WHERE CHAVE = 'parametros.integracoes.ultimaSincronizacaoCrmRgc'  
  
    
  IF @@ERROR <> 0  
  BEGIN     
   ROLLBACK     
   RAISERROR ('Erro ao gravar sincronizar dados CRM.', 16, 1)  
   RETURN  
  END  
  
	COMMIT
END