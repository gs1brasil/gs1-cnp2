IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AgenciaReguladora') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AgenciaReguladora;

CREATE TABLE [dbo].[AgenciaReguladora](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL
);

ALTER TABLE AgenciaReguladora ADD CONSTRAINT PK_AgenciaReguladora 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE AgenciaReguladora ADD CONSTRAINT FK_AgenciaReguladora_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
