IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AgenciaReguladoraHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AgenciaReguladoraHistorico;

CREATE TABLE AgenciaReguladoraHistorico ( 
	Codigo bigint NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE AgenciaReguladoraHistorico ADD CONSTRAINT PK_AgenciaReguladoraHistorico 
	PRIMARY KEY CLUSTERED (DataHistorico);

ALTER TABLE AgenciaReguladoraHistorico ADD CONSTRAINT FK_AgenciaReguladoraHistorico_AgenciaReguladora 
	FOREIGN KEY (Codigo) REFERENCES AgenciaReguladora (Codigo);

ALTER TABLE AgenciaReguladoraHistorico ADD CONSTRAINT FK_AgenciaReguladoraHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);








