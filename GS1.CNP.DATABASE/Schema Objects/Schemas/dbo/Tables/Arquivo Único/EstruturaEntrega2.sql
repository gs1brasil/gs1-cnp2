﻿ALTER TABLE ASSOCIADO ADD [IndicadorCNAERestritivo] [varchar](1)

ALTER TABLE ASSOCIADOHISTORICO ADD [IndicadorCNAERestritivo] [varchar](1)


ALTER TABLE ProdutoBrickTypeValue ALTER COLUMN [CodeSegment] [varchar](10) NOT NULL


ALTER TABLE StatusPublicacao ALTER COLUMN [Status] [int] NOT NULL
ALTER TABLE TipoAceite ALTER COLUMN [Status] [int] NOT NULL

ALTER TABLE TipoAjuda ALTER COLUMN [Nome] [varchar](100) NOT NULL
ALTER TABLE TipoAjuda ALTER COLUMN [Status] [int] NOT NULL

ALTER TABLE TipoAssociado ALTER COLUMN [Nome] [varchar](100) NOT NULL
ALTER TABLE TipoAssociado ALTER COLUMN [Status] [int] NOT NULL

ALTER TABLE TipoGeracao ALTER COLUMN [Nome] [varchar](100) NOT NULL
ALTER TABLE TipoGeracao ALTER COLUMN [Status] [int] NOT NULL

ALTER TABLE TipoGTIN ALTER COLUMN [Nome] [varchar](100) NOT NULL
ALTER TABLE TipoGTIN ALTER COLUMN [Status] [int] NOT NULL


/*******************************************************/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Banner](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
	[NomeImagem] [varchar](255) NOT NULL,
	[DataInicioPublicacao] [datetime] NOT NULL,
	[DataFimPublicacao] [datetime] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[Link] [varchar](255) NULL,
	[CorFundo] [varchar](50) NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Banner]  WITH CHECK ADD  CONSTRAINT [FK_Banner_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[Banner] CHECK CONSTRAINT [FK_Banner_Usuario]
GO



/****** Object:  Table [dbo].[EspacoBanner]    Script Date: 23/02/2016 10:00:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EspacoBanner](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[Altura] [varchar](50) NOT NULL,
	[Largura] [varchar](50) NOT NULL,
	[UrlPublicacao] [varchar](255) NOT NULL,
	[QuantidadeMaxima] [int] NOT NULL,
	[CodigoTipoEspacoBanner] [int] NOT NULL,
 CONSTRAINT [PK_EspacoBanner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[EspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_EspacoBanner_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[EspacoBanner] CHECK CONSTRAINT [FK_EspacoBanner_Usuario]
GO


/****** Object:  Table [dbo].[BannerEspacoBanner]    Script Date: 23/02/2016 09:58:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BannerEspacoBanner](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CodigoEspacoBanner] [int] NULL,
	[CodigoBanner] [bigint] NULL,
	[Ordem] [int] NULL,
 CONSTRAINT [PK_BannerEspacoBanner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BannerEspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_BannerEspacoBanner_Banner] FOREIGN KEY([CodigoBanner])
REFERENCES [dbo].[Banner] ([Codigo])
GO

ALTER TABLE [dbo].[BannerEspacoBanner] CHECK CONSTRAINT [FK_BannerEspacoBanner_Banner]
GO

ALTER TABLE [dbo].[BannerEspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_BannerEspacoBanner_EspacoBanner] FOREIGN KEY([CodigoEspacoBanner])
REFERENCES [dbo].[EspacoBanner] ([Codigo])
GO

ALTER TABLE [dbo].[BannerEspacoBanner] CHECK CONSTRAINT [FK_BannerEspacoBanner_EspacoBanner]
GO






GO

/****** Object:  Table [dbo].[BizStep]    Script Date: 23/02/2016 09:58:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BizStep](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_BizStep] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO

/****** Object:  Table [dbo].[CertificacaoProdutosCodigoBarras]    Script Date: 23/02/2016 09:59:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CertificacaoProdutosCodigoBarras](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosCodigoBarras] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Associado]
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Produto]
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Usuario]
GO


GO

/****** Object:  Table [dbo].[CertificacaoProdutosCodigoBarrasHistorico]    Script Date: 23/02/2016 09:59:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico](
	[Codigo] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosCodigoBarrasHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Associado]
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_CertificacaoProdutosCodigoBarras] FOREIGN KEY([Codigo])
REFERENCES [dbo].[CertificacaoProdutosCodigoBarras] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_CertificacaoProdutosCodigoBarras]
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Produto]
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Usuario]
GO



GO

/****** Object:  Table [dbo].[CertificacaoProdutosPesosMedidas]    Script Date: 23/02/2016 09:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CertificacaoProdutosPesosMedidas](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosPesosMedidas] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Associado]
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Produto]
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Usuario]
GO


GO

/****** Object:  Table [dbo].[CertificacaoProdutosPesosMedidasHistorico]    Script Date: 23/02/2016 09:59:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico](
	[Codigo] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosPesosMedidasHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Associado]
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_CertificacaoProdutosCodigoBarras] FOREIGN KEY([Codigo])
REFERENCES [dbo].[CertificacaoProdutosCodigoBarras] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_CertificacaoProdutosCodigoBarras]
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Produto]
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Usuario]
GO


GO

/****** Object:  Table [dbo].[Dispositions]    Script Date: 23/02/2016 10:00:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Dispositions](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[CodigoBizStep] [int] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Dispositions] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Dispositions]  WITH CHECK ADD  CONSTRAINT [FK_Dispositions_BizStep] FOREIGN KEY([CodigoBizStep])
REFERENCES [dbo].[BizStep] ([Codigo])
GO

ALTER TABLE [dbo].[Dispositions] CHECK CONSTRAINT [FK_Dispositions_BizStep]
GO



/****** Object:  Table [dbo].[FormularioAjudaPassoAPasso]    Script Date: 23/02/2016 10:00:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FormularioAjudaPassoAPasso](
	[Codigo] [int] NOT NULL,
	[CodigoFormulario] [int] NOT NULL,
	[CodigoTipoAjuda] [int] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Ajuda] [varchar](max) NULL,
	[Ordem] [int] NULL,
	[UrlVideo] [varchar](max) NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
 CONSTRAINT [PK_FormularioAjudaPassoAPasso] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPasso]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPasso_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPasso] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPasso_Formulario]
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPasso]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPasso_TipoAjuda] FOREIGN KEY([CodigoTipoAjuda])
REFERENCES [dbo].[TipoAjuda] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPasso] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPasso_TipoAjuda]
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPasso]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPasso_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPasso] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPasso_Usuario]
GO


GO

/****** Object:  Table [dbo].[FormularioAjudaPassoAPassoHistorico]    Script Date: 23/02/2016 10:01:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FormularioAjudaPassoAPassoHistorico](
	[CodigoFormularioAjudaPassoAPasso] [int] NOT NULL,
	[CodigoFormulario] [int] NOT NULL,
	[CodigoTipoAjuda] [int] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Ajuda] [varchar](max) NULL,
	[Ordem] [int] NULL,
	[UrlVideo] [varchar](max) NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_FormularioAjudaPassoAPassoHistorico] PRIMARY KEY CLUSTERED 
(
	[CodigoFormularioAjudaPassoAPasso] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_Formulario]
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_FormularioAjuda] FOREIGN KEY([CodigoFormularioAjudaPassoAPasso])
REFERENCES [dbo].[FormularioAjudaPassoAPasso] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_FormularioAjuda]
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_TipoAjuda] FOREIGN KEY([CodigoTipoAjuda])
REFERENCES [dbo].[TipoAjuda] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_TipoAjuda]
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_Usuario]
GO



GO

/****** Object:  Table [dbo].[FormularioIdioma]    Script Date: 23/02/2016 10:01:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FormularioIdioma](
	[CodigoFormulario] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Descricao] [varchar](255) NOT NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_FormularioIdioma] PRIMARY KEY CLUSTERED 
(
	[CodigoFormulario] ASC,
	[CodigoIdioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FormularioIdioma]  WITH CHECK ADD  CONSTRAINT [FK_FormularioIdioma_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioIdioma] CHECK CONSTRAINT [FK_FormularioIdioma_Formulario]
GO

ALTER TABLE [dbo].[FormularioIdioma]  WITH CHECK ADD  CONSTRAINT [FK_FormularioIdioma_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO

ALTER TABLE [dbo].[FormularioIdioma] CHECK CONSTRAINT [FK_FormularioIdioma_Idioma]
GO




/****** Object:  Table [dbo].[StatusLote]    Script Date: 23/02/2016 10:04:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StatusLote](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NULL,
	[Descricao] [varchar](500) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_StatusLote] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO


/****** Object:  Table [dbo].[Lote]    Script Date: 23/02/2016 10:01:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Lote](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CodigoStatusLote] [int] NOT NULL,
	[CodigoProduto] [bigint] NULL,
	[Nrlote] [varchar](20) NULL,
	[DataProcessamento] [datetime] NULL,
	[DataVencimento] [datetime] NULL,
	[TipoProcessamentoBizStep] [varchar](500) NULL,
	[StatusProdutoDispositions] [varchar](500) NULL,
	[InformacoesAdicionais] [varchar](500) NULL,
	[DataAlteracao] [datetime] NULL,
	[CodigoUsuarioAlteracao] [bigint] NULL,
	[CodigoAssociado] [bigint] NULL,
	[CodigoBizStep] [int] NULL,
	[CodigoDispositions] [int] NULL,
	[quantidadeItens] [int] NULL,
 CONSTRAINT [PK_Lote] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Lote]  WITH CHECK ADD  CONSTRAINT [FK_Lote_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO

ALTER TABLE [dbo].[Lote] CHECK CONSTRAINT [FK_Lote_Associado]
GO

ALTER TABLE [dbo].[Lote]  WITH CHECK ADD  CONSTRAINT [FK_Lote_BizStep] FOREIGN KEY([CodigoBizStep])
REFERENCES [dbo].[BizStep] ([Codigo])
GO

ALTER TABLE [dbo].[Lote] CHECK CONSTRAINT [FK_Lote_BizStep]
GO

ALTER TABLE [dbo].[Lote]  WITH CHECK ADD  CONSTRAINT [FK_Lote_Dispositions] FOREIGN KEY([CodigoDispositions])
REFERENCES [dbo].[Dispositions] ([Codigo])
GO

ALTER TABLE [dbo].[Lote] CHECK CONSTRAINT [FK_Lote_Dispositions]
GO

ALTER TABLE [dbo].[Lote]  WITH CHECK ADD  CONSTRAINT [FK_Lote_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[Lote] CHECK CONSTRAINT [FK_Lote_Produto]
GO

ALTER TABLE [dbo].[Lote]  WITH CHECK ADD  CONSTRAINT [FK_Lote_StatusLote] FOREIGN KEY([CodigoStatusLote])
REFERENCES [dbo].[StatusLote] ([Codigo])
GO

ALTER TABLE [dbo].[Lote] CHECK CONSTRAINT [FK_Lote_StatusLote]
GO

ALTER TABLE [dbo].[Lote]  WITH CHECK ADD  CONSTRAINT [FK_Lote_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[Lote] CHECK CONSTRAINT [FK_Lote_Usuario]
GO



GO

/****** Object:  Table [dbo].[LoteHistorico]    Script Date: 23/02/2016 10:02:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LoteHistorico](
	[CodigoLote] [bigint] NOT NULL,
	[CodigoStatusLote] [int] NOT NULL,
	[CodigoProduto] [bigint] NULL,
	[Nrlote] [varchar](20) NULL,
	[DataProcessamento] [datetime] NULL,
	[DataVencimento] [datetime] NULL,
	[TipoProcessamentoBizStep] [varchar](500) NULL,
	[StatusProdutoDispositions] [varchar](500) NULL,
	[InformacoesAdicionais] [varchar](500) NULL,
	[DataAlteracao] [datetime] NULL,
	[CodigoUsuarioAlteracao] [bigint] NULL,
	[CodigoAssociado] [bigint] NULL,
	[CodigoBizStep] [int] NULL,
	[CodigoDispositions] [int] NULL,
	[DataHistorico] [datetime] NOT NULL,
	[quantidadeItens] [int] NULL,
 CONSTRAINT [PK_LoteHistorico] PRIMARY KEY CLUSTERED 
(
	[CodigoLote] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LoteHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LoteHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO

ALTER TABLE [dbo].[LoteHistorico] CHECK CONSTRAINT [FK_LoteHistorico_Associado]
GO

ALTER TABLE [dbo].[LoteHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LoteHistorico_BizStep] FOREIGN KEY([CodigoBizStep])
REFERENCES [dbo].[BizStep] ([Codigo])
GO

ALTER TABLE [dbo].[LoteHistorico] CHECK CONSTRAINT [FK_LoteHistorico_BizStep]
GO

ALTER TABLE [dbo].[LoteHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LoteHistorico_Dispositions] FOREIGN KEY([CodigoDispositions])
REFERENCES [dbo].[Dispositions] ([Codigo])
GO

ALTER TABLE [dbo].[LoteHistorico] CHECK CONSTRAINT [FK_LoteHistorico_Dispositions]
GO

ALTER TABLE [dbo].[LoteHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LoteHistorico_Lote] FOREIGN KEY([CodigoLote])
REFERENCES [dbo].[Lote] ([Codigo])
GO

ALTER TABLE [dbo].[LoteHistorico] CHECK CONSTRAINT [FK_LoteHistorico_Lote]
GO

ALTER TABLE [dbo].[LoteHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LoteHistorico_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[LoteHistorico] CHECK CONSTRAINT [FK_LoteHistorico_Produto]
GO

ALTER TABLE [dbo].[LoteHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LoteHistorico_StatusLote] FOREIGN KEY([CodigoStatusLote])
REFERENCES [dbo].[StatusLote] ([Codigo])
GO

ALTER TABLE [dbo].[LoteHistorico] CHECK CONSTRAINT [FK_LoteHistorico_StatusLote]
GO

ALTER TABLE [dbo].[LoteHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LoteHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[LoteHistorico] CHECK CONSTRAINT [FK_LoteHistorico_Usuario]
GO



GO

/****** Object:  Table [dbo].[LoteURL]    Script Date: 23/02/2016 10:02:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LoteURL](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[URL] [varchar](max) NOT NULL,
	[Status] [int] NOT NULL,
	[CodigoLote] [bigint] NOT NULL,
 CONSTRAINT [PK_LoteURL] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


GO


/****** Object:  Table [dbo].[LoteURLHistorico]    Script Date: 23/02/2016 10:02:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LoteURLHistorico](
	[Codigo] [bigint] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[URL] [varchar](max) NOT NULL,
	[Status] [int] NOT NULL,
	[CodigoLote] [bigint] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_LoteURLHistorico] PRIMARY KEY CLUSTERED 
(
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LoteURLHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LoteURLHistorico_Lote] FOREIGN KEY([CodigoLote])
REFERENCES [dbo].[Lote] ([Codigo])
GO

ALTER TABLE [dbo].[LoteURLHistorico] CHECK CONSTRAINT [FK_LoteURLHistorico_Lote]
GO



GO

/****** Object:  Table [dbo].[ModuloIdioma]    Script Date: 23/02/2016 10:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ModuloIdioma](
	[CodigoModulo] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_ModuloIdioma] PRIMARY KEY CLUSTERED 
(
	[CodigoModulo] ASC,
	[CodigoIdioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ModuloIdioma]  WITH CHECK ADD  CONSTRAINT [FK_ModuloIdioma_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO

ALTER TABLE [dbo].[ModuloIdioma] CHECK CONSTRAINT [FK_ModuloIdioma_Idioma]
GO

ALTER TABLE [dbo].[ModuloIdioma]  WITH CHECK ADD  CONSTRAINT [FK_ModuloIdioma_Modulo] FOREIGN KEY([CodigoModulo])
REFERENCES [dbo].[Modulo] ([Codigo])
GO

ALTER TABLE [dbo].[ModuloIdioma] CHECK CONSTRAINT [FK_ModuloIdioma_Modulo]
GO



GO

/****** Object:  Table [dbo].[StatusAjuda]    Script Date: 23/02/2016 10:03:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StatusAjuda](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_StatusAjuda] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



GO



/****** Object:  Table [dbo].[StatusProduto]    Script Date: 23/02/2016 10:04:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[StatusProduto](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_StatusProduto] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO




























/****** Object:  Table [dbo].[PublicoAlvo]    Script Date: 23/02/2016 11:16:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PublicoAlvo](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Descricao] [varchar](255) NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
 CONSTRAINT [PK_PublicoAlvo] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[StatusPesquisaSatisfacaoUsuario]    Script Date: 23/02/2016 11:17:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StatusPesquisaSatisfacaoUsuario](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
 CONSTRAINT [PK_StatusPesquisaSatisfacaoUsuario] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO







/****** Object:  Table [dbo].[PesquisaSatisfacao]    Script Date: 23/02/2016 11:16:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PesquisaSatisfacao](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Descricao] [varchar](400) NOT NULL,
	[Status] [int] NOT NULL,
	[Inicio] [datetime] NOT NULL,
	[Fim] [datetime] NOT NULL,
	[Url] [varchar](max) NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoPublicoAlvo] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
 CONSTRAINT [PK_PesquisaSatisfacao] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PesquisaSatisfacao]  WITH CHECK ADD  CONSTRAINT [FK_PesquisaSatisfacao_PublicoAlvo] FOREIGN KEY([CodigoPublicoAlvo])
REFERENCES [dbo].[PublicoAlvo] ([Codigo])
GO

ALTER TABLE [dbo].[PesquisaSatisfacao] CHECK CONSTRAINT [FK_PesquisaSatisfacao_PublicoAlvo]
GO

ALTER TABLE [dbo].[PesquisaSatisfacao]  WITH CHECK ADD  CONSTRAINT [FK_PesquisaSatisfacao_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[PesquisaSatisfacao] CHECK CONSTRAINT [FK_PesquisaSatisfacao_Usuario]
GO






/****** Object:  Table [dbo].[PesquisaSatisfacaoHistorico]    Script Date: 23/02/2016 11:16:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PesquisaSatisfacaoHistorico](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Descricao] [varchar](400) NOT NULL,
	[Status] [int] NOT NULL,
	[Inicio] [datetime] NOT NULL,
	[Fim] [datetime] NOT NULL,
	[Url] [varchar](max) NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoPublicoAlvo] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_PesquisaSatisfacaoHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_PesquisaSatisfacaoHistorico_PesquisaSatisfacao] FOREIGN KEY([Codigo])
REFERENCES [dbo].[PesquisaSatisfacao] ([Codigo])
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoHistorico] CHECK CONSTRAINT [FK_PesquisaSatisfacaoHistorico_PesquisaSatisfacao]
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_PesquisaSatisfacaoHistorico_PublicoAlvo] FOREIGN KEY([CodigoPublicoAlvo])
REFERENCES [dbo].[PublicoAlvo] ([Codigo])
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoHistorico] CHECK CONSTRAINT [FK_PesquisaSatisfacaoHistorico_PublicoAlvo]
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_PesquisaSatisfacaoHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoHistorico] CHECK CONSTRAINT [FK_PesquisaSatisfacaoHistorico_Usuario]
GO



/****** Object:  Table [dbo].[PesquisaSatisfacaoUsuario]    Script Date: 23/02/2016 11:16:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PesquisaSatisfacaoUsuario](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[CodigoPesquisaSatisfacao] [int] NOT NULL,
	[CodigoUsuario] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoStatusPesquisaSatisfacaoUsuario] [int] NOT NULL,
 CONSTRAINT [PK_PesquisaSatisfacaoUsuario] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PesquisaSatisfacaoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_PesquisaSatisfacaoUsuario_PesquisaSatisfacao] FOREIGN KEY([CodigoPesquisaSatisfacao])
REFERENCES [dbo].[PesquisaSatisfacao] ([Codigo])
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoUsuario] CHECK CONSTRAINT [FK_PesquisaSatisfacaoUsuario_PesquisaSatisfacao]
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_PesquisaSatisfacaoUsuario_StatusPesquisaSatisfacaoUsuario] FOREIGN KEY([CodigoStatusPesquisaSatisfacaoUsuario])
REFERENCES [dbo].[StatusPesquisaSatisfacaoUsuario] ([Codigo])
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoUsuario] CHECK CONSTRAINT [FK_PesquisaSatisfacaoUsuario_StatusPesquisaSatisfacaoUsuario]
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_PesquisaSatisfacaoUsuario_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])
GO

ALTER TABLE [dbo].[PesquisaSatisfacaoUsuario] CHECK CONSTRAINT [FK_PesquisaSatisfacaoUsuario_Usuario]
GO


ALTER TABLE PRODUTO
ADD AKGTINASSOCIADO AS (ISNULL(GLOBALTRADEITEMNUMBER,-[CODIGOPRODUTO]))

ALTER TABLE PRODUTO
ADD CONSTRAINT AK_GTINASSOCIADO UNIQUE (AKGTINASSOCIADO, CODIGOASSOCIADO)




