-----------------------------------------
---------	SITUA��O FINANCEIRA	---------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('SituacaoFinanceira') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE SituacaoFinanceira;

CREATE TABLE SituacaoFinanceira ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE SituacaoFinanceira ADD CONSTRAINT PK_SituacaoFinanceira 
	PRIMARY KEY CLUSTERED (Codigo);
GO


-------------------------------------------------
---------		STATUS ASSOCIADO		---------
-------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusAssociado;

CREATE TABLE StatusAssociado ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE StatusAssociado ADD CONSTRAINT PK_StatusAssociado 
	PRIMARY KEY CLUSTERED (Codigo);


-----------------------------------------------------
------------		TIPO ASSOCIADO		-------------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoAssociado;

CREATE TABLE TipoAssociado ( 
	Codigo int NOT NULL,
	Nome varchar(50),
	Descricao varchar(255),
	Status int
);

ALTER TABLE TipoAssociado ADD CONSTRAINT PK_TipoAssociado 
	PRIMARY KEY CLUSTERED (Codigo);


-----------------------------------------------------
---------		TIPO COMPARTILHAMENTO		---------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoCompartilhamento') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoCompartilhamento;

CREATE TABLE TipoCompartilhamento ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE TipoCompartilhamento ADD CONSTRAINT PK_TipoCompartilhamento 
	PRIMARY KEY CLUSTERED (Codigo);


-----------------------------------------------------
-------------		 TIPO USU�RIO	 ----------------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoUsuario;

CREATE TABLE TipoUsuario ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE TipoUsuario ADD CONSTRAINT PK_TipoUsuario 
	PRIMARY KEY CLUSTERED (Codigo);


-----------------------------------------------------
------------		STATUS USU�RIO		-------------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusUsuario;

CREATE TABLE StatusUsuario ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	CodigoTipoUsuario int
);

ALTER TABLE StatusUsuario ADD CONSTRAINT PK_StatusUsuario 
	PRIMARY KEY CLUSTERED (Codigo);

GO


-----------------------------------------------------
----------------		USU�RIO		-----------------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Usuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Usuario;

CREATE TABLE Usuario ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Nome varchar(255) NOT NULL,
	CodigoStatusUsuario int NOT NULL,
	CodigoPerfil int NOT NULL,
	CodigoTipoUsuario int NOT NULL,
	Email varchar(255) NOT NULL,
	TelefoneResidencial varchar(50),
	Senha varchar(255),
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	MensagemObservacao varchar(500),
	DataAlteracaoSenha datetime,
	QuantidadeTentativa int DEFAULT ((0)) NOT NULL,
	DataUltimoAcesso datetime,
	DataCadastro datetime DEFAULT (getdate()) NOT NULL,
	TelefoneCelular varchar(50),
	TelefoneComercial varchar(50),
	Ramal varchar(50),
	Departamento varchar(255),
	Cargo varchar(255),
	CPFCNPJ varchar(14)
);

ALTER TABLE Usuario
	ADD CONSTRAINT UQ_Usuario_Email UNIQUE (Email);

CREATE INDEX IDX_CPFCNPJ
ON Usuario (CPFCNPJ ASC);

ALTER TABLE Usuario ADD CONSTRAINT PK_Usuario 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_StatusUsuario 
	FOREIGN KEY (CodigoStatusUsuario) REFERENCES StatusUsuario (Codigo)
	
ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo)

ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)



-----------------------------------------------------
----------------		PERFIL		-----------------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Perfil') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Perfil;

CREATE TABLE Perfil ( 
	Codigo int identity(1,1)  NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	CodigoTipoUsuario int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE Perfil ADD CONSTRAINT PK_Perfil 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Perfil ADD CONSTRAINT FK_Perfil_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo)

ALTER TABLE Perfil ADD CONSTRAINT FK_Perfil_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)


ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_Perfil 
	FOREIGN KEY (CodigoPerfil) REFERENCES Perfil (Codigo)

-----------------------------------------------------
---------------		 ASSOCIADO		 ----------------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Associado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Associado;

CREATE TABLE [dbo].[Associado](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CAD] [varchar](50) NOT NULL,
	[Nome] [varchar](255) NULL,
	[Descricao] [varchar](5000) NULL,
	[Email] [varchar](255) NULL,
	[CPFCNPJ] [varchar](14) NULL,
	[InscricaoEstadual] [varchar](50) NULL,
	[CodigoTipoAssociado] [int] NOT NULL,
	[MensagemObservacao] [text] NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataAtualizacaoCRM] [datetime] NULL,
	[DataStatusInadimplente] [datetime] NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[Endereco] [varchar](1000) NULL,
	[Numero] [varchar](50) NULL,
	[Complemento] [varchar](100) NULL,
	[Bairro] [varchar](100) NULL,
	[Cidade] [varchar](255) NULL,
	[UF] [varchar](2) NULL,
	[CEP] [varchar](50) NULL,
	[CodigoTipoCompartilhamento] [int] NOT NULL,
	[CodigoStatusAssociado] [int] NOT NULL,
	[CodigoSituacaoFinanceira] [int] NOT NULL,
	[Pais] [varchar](50) NULL,
	[IndicadorCNAERestritivo] [varchar](1) NULL,
 CONSTRAINT [PK_Associado] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_SituacaoFinanceira] FOREIGN KEY([CodigoSituacaoFinanceira])
REFERENCES [dbo].[SituacaoFinanceira] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_SituacaoFinanceira]
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_StatusAssociado] FOREIGN KEY([CodigoStatusAssociado])
REFERENCES [dbo].[StatusAssociado] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_StatusAssociado]
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_TipoAssociado] FOREIGN KEY([CodigoTipoAssociado])
REFERENCES [dbo].[TipoAssociado] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_TipoAssociado]
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_TipoCompartilhamento] FOREIGN KEY([CodigoTipoCompartilhamento])
REFERENCES [dbo].[TipoCompartilhamento] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_TipoCompartilhamento]
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_Usuario]
GO


-----------------------------------------
---------	ASSOCIADO HIST�RICO	---------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AssociadoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AssociadoHistorico;

CREATE TABLE [dbo].[AssociadoHistorico](
	[CodigoAssociado] [bigint] NOT NULL,
	[CAD] [varchar](50) NULL,
	[Nome] [varchar](255) NULL,
	[Descricao] [varchar](5000) NULL,
	[Email] [varchar](255) NULL,
	[CPFCNPJ] [varchar](14) NULL,
	[InscricaoEstadual] [varchar](50) NULL,
	[CodigoTipoAssociado] [int] NOT NULL,
	[MensagemObservacao] [text] NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataAtualizacaoCRM] [datetime] NULL,
	[DataStatusInadimplente] [datetime] NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[Endereco] [varchar](1000) NULL,
	[Numero] [varchar](50) NULL,
	[Complemento] [varchar](100) NULL,
	[Bairro] [varchar](100) NULL,
	[Cidade] [varchar](255) NULL,
	[UF] [varchar](2) NULL,
	[CEP] [varchar](50) NULL,
	[CodigoTipoCompartilhamento] [int] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
	[CodigoStatusAssociado] [int] NOT NULL,
	[CodigoSituacaoFinanceira] [int] NOT NULL,
	[IndicadorCNAERestritivo] [varchar](1) NULL,
 CONSTRAINT [PK_AssociadoHistorico] PRIMARY KEY CLUSTERED 
(
	[CodigoAssociado] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AssociadoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoHistorico_SituacaoFinanceira] FOREIGN KEY([CodigoSituacaoFinanceira])
REFERENCES [dbo].[SituacaoFinanceira] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoHistorico] CHECK CONSTRAINT [FK_AssociadoHistorico_SituacaoFinanceira]
GO
ALTER TABLE [dbo].[AssociadoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoHistorico_StatusAssociado] FOREIGN KEY([CodigoStatusAssociado])
REFERENCES [dbo].[StatusAssociado] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoHistorico] CHECK CONSTRAINT [FK_AssociadoHistorico_StatusAssociado]
GO
ALTER TABLE [dbo].[AssociadoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoHistorico_TipoAssociado] FOREIGN KEY([CodigoTipoAssociado])
REFERENCES [dbo].[TipoAssociado] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoHistorico] CHECK CONSTRAINT [FK_AssociadoHistorico_TipoAssociado]
GO
ALTER TABLE [dbo].[AssociadoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoHistorico_TipoCompartilhamento] FOREIGN KEY([CodigoTipoCompartilhamento])
REFERENCES [dbo].[TipoCompartilhamento] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoHistorico] CHECK CONSTRAINT [FK_AssociadoHistorico_TipoCompartilhamento]
GO
ALTER TABLE [dbo].[AssociadoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoHistorico] CHECK CONSTRAINT [FK_AssociadoHistorico_Usuario]
GO


-----------------------------------------
---------	ASSOCIADO USU�RIO	---------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AssociadoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AssociadoUsuario;

CREATE TABLE [dbo].[AssociadoUsuario](
	[CodigoUsuario] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[DataConfirmacao] [datetime] NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[IndicadorPrincipal] [varchar](1) NOT NULL,
 CONSTRAINT [PK_AssociadoUsuario] PRIMARY KEY CLUSTERED 
(
	[CodigoUsuario] ASC,
	[CodigoAssociado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AssociadoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoUsuario_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoUsuario] CHECK CONSTRAINT [FK_AssociadoUsuario_Associado]
GO
ALTER TABLE [dbo].[AssociadoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoUsuario_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoUsuario] CHECK CONSTRAINT [FK_AssociadoUsuario_Usuario]
GO


-----------------------------------------
-------ASSOCIADO USU�RIO HIST�RICO-------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AssociadoUsuarioHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AssociadoUsuarioHistorico;

CREATE TABLE [dbo].[AssociadoUsuarioHistorico](
	[CodigoUsuario] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[DataConfirmacao] [datetime] NULL,
	[DataHistorico] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
 CONSTRAINT [PK_AssociadoUsuarioHistorico] PRIMARY KEY CLUSTERED 
(
	[CodigoUsuario] ASC,
	[CodigoAssociado] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[AssociadoUsuarioHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoUsuarioHistorico_AssociadoUsuario] FOREIGN KEY([CodigoUsuario], [CodigoAssociado])
REFERENCES [dbo].[AssociadoUsuario] ([CodigoUsuario], [CodigoAssociado])
GO
ALTER TABLE [dbo].[AssociadoUsuarioHistorico] CHECK CONSTRAINT [FK_AssociadoUsuarioHistorico_AssociadoUsuario]
GO
ALTER TABLE [dbo].[AssociadoUsuarioHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoUsuarioHistorico_UsuarioAlteracao] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoUsuarioHistorico] CHECK CONSTRAINT [FK_AssociadoUsuarioHistorico_UsuarioAlteracao]
GO


-----------------------------------------
---------			AWG			---------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AWG') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AWG;

CREATE TABLE [dbo].[AWG](
	[Codigo] [int] NOT NULL,
	[AWG] [varchar](50) NOT NULL,
	[FatorConversao] [decimal](25, 12) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_AWG] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



-----------------------------------------
---------	BARCODE TYPE LIST	---------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('BarCodeTypeList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE BarCodeTypeList;

CREATE TABLE [dbo].[BarCodeTypeList](
	[Codigo] [int] NOT NULL,
	[CodeValue] [varchar](100) NOT NULL,
	[Name] [varchar](500) NOT NULL,
	[Definition] [text] NULL,
 CONSTRAINT [PK_BarCodeTypeList] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]



-----------------------------------------
---- BARCODE TYPE LIST MAGNIFICATION ----
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('BarCodeTypeListMagnification') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE BarCodeTypeListMagnification;

CREATE TABLE [dbo].[BarCodeTypeListMagnification](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[MagnificationFactor] [decimal](10, 3) NOT NULL,
	[IdealModuleWidth] [decimal](10, 3) NOT NULL,
	[Width] [decimal](10, 3) NOT NULL,
	[Height] [decimal](10, 3) NOT NULL,
	[LeftGuard] [int] NOT NULL,
	[RightGuard] [int] NULL,
	[CodigoBarCodeTypeList] [int] NULL,
 CONSTRAINT [PK_BarCodeTypeListMagnification] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[BarCodeTypeListMagnification]  WITH CHECK ADD  CONSTRAINT [FK_BarCodeTypeListMagnification_BarCodeTypeList] FOREIGN KEY([CodigoBarCodeTypeList])
REFERENCES [dbo].[BarCodeTypeList] ([Codigo])
GO
ALTER TABLE [dbo].[BarCodeTypeListMagnification] CHECK CONSTRAINT [FK_BarCodeTypeListMagnification_BarCodeTypeList]
GO


-----------------------------------------
-----------		BIZSTEP		-------------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('BizStep') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE BizStep;

CREATE TABLE [dbo].[BizStep](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_BizStep] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO


-----------------------------------------
---------- 		 CAMPO		   ----------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Campo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Campo;

CREATE TABLE [dbo].[Campo](
	[Codigo] [bigint] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Campo] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


------------------------------------------
----------	 IDIOMA TRADU��O	----------
------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('IdiomaTraducao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE IdiomaTraducao;

CREATE TABLE IdiomaTraducao ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Sigla varchar(20) NOT NULL,
	Status int NOT NULL
);

ALTER TABLE IdiomaTraducao ADD CONSTRAINT PK_IdiomaTraducao 
	PRIMARY KEY CLUSTERED (Codigo);


-----------------------------------------
---------- 		 IDIOMA		   ----------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Idioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Idioma;

CREATE TABLE Idioma ( 
	Codigo int identity(1,1)  NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	CodigoIdiomaTraducao int NOT NULL
);

ALTER TABLE Idioma ADD CONSTRAINT PK_Idioma 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Idioma ADD CONSTRAINT FK_Idioma_IdiomaTraducao 
	FOREIGN KEY (CodigoIdiomaTraducao) REFERENCES IdiomaTraducao (Codigo);


-----------------------------------------
---------	  CAMPO IDIOMA	    ---------
-----------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CampoIdioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CampoIdioma;

CREATE TABLE [dbo].[CampoIdioma](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Texto] [varchar](8000) NOT NULL,
	[Comentario] [varchar](8000) NULL,
	[Status] [int] NOT NULL,
	[CodigoCampo] [bigint] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
 CONSTRAINT [PK_CampoIdioma] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CampoIdioma]  WITH CHECK ADD  CONSTRAINT [FK_CampoIdioma_Campo] FOREIGN KEY([CodigoCampo])
REFERENCES [dbo].[Campo] ([Codigo])
GO
ALTER TABLE [dbo].[CampoIdioma] CHECK CONSTRAINT [FK_CampoIdioma_Campo]
GO
ALTER TABLE [dbo].[CampoIdioma]  WITH CHECK ADD  CONSTRAINT [FK_CampoIdioma_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[CampoIdioma] CHECK CONSTRAINT [FK_CampoIdioma_Idioma]
GO


-------------------------------------------
------------	  TIPO GTIN	    -----------
-------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoGTIN') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoGTIN;

CREATE TABLE TipoGTIN ( 
	Codigo int NOT NULL,
	Nome varchar(255),
	Descricao varchar(1000),
	Status int
);

ALTER TABLE TipoGTIN ADD CONSTRAINT PK_TipoGTIN 
	PRIMARY KEY CLUSTERED (Codigo);


-----------------------------------------------
------------	  STATUS GTIN	    -----------
-----------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusGTIN') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusGTIN;

CREATE TABLE StatusGTIN ( 
	Codigo int NOT NULL,
	Nome varchar(100),
	Descricao varchar(500),
	Status int
);

ALTER TABLE StatusGTIN ADD CONSTRAINT PK_StatusGTIN 
	PRIMARY KEY CLUSTERED (Codigo);



-------------------------------------------
------------	  PRODUTO	    -----------
-------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Produto') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Produto;

CREATE TABLE Produto ( 
	CodigoProduto bigint identity(1,1)  NOT NULL,
	globalTradeItemNumber bigint,
	productDescription varchar(300) NOT NULL,
	CodigoTipoGTIN int NOT NULL,
	NrPrefixo bigint,
	CodItem varchar(5),
	VarianteLogistica int,
	CodigoAssociado bigint NOT NULL,
	CodigoUsuarioCriacao bigint NOT NULL,
	CodigoUsuarioAlteracao bigint,
	CodigoUsuarioExclusao bigint,
	tradeItemCountryOfOrigin int,
	CodigoIdioma int,
	CodigoStatusGTIN int NOT NULL,
	CodeSegment varchar(10),
	CodeFamily varchar(10),
	CodeClass varchar(10),
	CodeBrick varchar(10),
	countryCode varchar(3),
	CodeBrickAttribute bigint,
	IndicadorCompartilhaDados varchar(1),
	Observacoes text,
	Estado varchar(50),
	informationProvider bigint,
	brandName varchar(50),
	DataInclusao datetime NOT NULL,
	DataAlteracao datetime,
	DataSuspensao datetime,
	DataReativacao datetime,
	DataCancelamento datetime,
	DataReutilizacao datetime,
	modelNumber varchar(70),
	importClassificationType varchar(20),
	importClassificationValue varchar(100),
	alternateItemIdentificationId varchar(50),
	alternateItemIdentificationAgency int,
	minimumTradeItemLifespanFromTimeOfProduction int,
	startAvailabilityDateTime datetime,
	endAvailabilityDateTime datetime,
	depth decimal(5,2),
	depthMeasurementUnitCode varchar(3),
	height decimal(5,2),
	heightMeasurementUnitCode varchar(3),
	width decimal(5,2),
	widthMeasurementUnitCode varchar(3),
	netContent decimal(5,2),
	netContentMeasurementUnitCode varchar(3),
	grossWeight decimal(5,2),
	grossWeightMeasurementUnitCode varchar(3),
	netWeight decimal(5,2),
	netWeightMeasurementUnitCode varchar(3),
	CodigoTipoProduto int,
	isTradeItemABaseUnit int,
	isTradeItemAConsumerUnit int,
	isTradeItemAModel int,
	isTradeItemAnInvoiceUnit varchar(1),
	packagingTypeCode varchar(25),
	PalletTypeCode varchar(3),
	totalQuantityOfNextLowerLevelTradeItem varchar(10),
	StackingFactor int,
	quantityOfTradeItemContainedInACompleteLayer int,
	quantityOfTradeItemsPerPalletLayer int,
	quantityOfCompleteLayersContainedInATradeItem int,
	quantityOfLayersPerPallet int,
	CodigoProdutoOrigem bigint,
	deliveryToDistributionCenterTemperatureMinimum decimal(5,2),
	storageHandlingTempMinimumUOM varchar(3),
	storageHandlingTemperatureMaximum decimal(5,2),
	storageHandlingTemperatureMaximumunitOfMeasure varchar(3),
	isDangerousSubstanceIndicated int,
	ipiPerc decimal(5,2),
	isTradeItemAnOrderableUnit int,
	isTradeItemADespatchUnit int,
	orderSizingFactor varchar(3),
	orderQuantityMultiple int,
	orderQuantityMinimum int,
	barcodeCertified int,
	dataQualityCertified int,
	dataQualityCertifiedAgencyCode int,
	allGDSNAttributes int,
	CanalComunicacaoDados varchar(1),
	ProprioDonoInformacao int,
	ValidadoDonoDaInformacao int,
	IndicadorGDSN int
);

CREATE INDEX IDX_globalTradeItemNumber
ON Produto (globalTradeItemNumber ASC);

ALTER TABLE Produto ADD CONSTRAINT PK_Produto 
	PRIMARY KEY CLUSTERED (CodigoProduto);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_TipoGTIN 
	FOREIGN KEY (CodigoTipoGTIN) REFERENCES TipoGTIN (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_Produto 
	FOREIGN KEY (CodigoProdutoOrigem) REFERENCES Produto (CodigoProduto);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_StatusGTIN 
	FOREIGN KEY (CodigoStatusGTIN) REFERENCES StatusGTIN (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_Usuario 
	FOREIGN KEY (CodigoUsuarioCriacao) REFERENCES Usuario (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_UsuarioAlteracao 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_UsuarioExclusao 
	FOREIGN KEY (CodigoUsuarioExclusao) REFERENCES Usuario (Codigo);


---------------------------------------------------------
---------	CERTIFICA��O PRODUTOS CODIGO BARRAS	---------
---------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CertificacaoProdutosCodigoBarras') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CertificacaoProdutosCodigoBarras;

CREATE TABLE [dbo].[CertificacaoProdutosCodigoBarras](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosCodigoBarras] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Associado]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Produto]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Usuario]
GO

---------------------------------------------------------------------
---------	CERTIFICA��O PRODUTOS CODIGO BARRAS HIST�RICO	---------
---------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CertificacaoProdutosCodigoBarrasHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CertificacaoProdutosCodigoBarrasHistorico;

CREATE TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico](
	[Codigo] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosCodigoBarrasHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Associado]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_CertificacaoProdutosCodigoBarras] FOREIGN KEY([Codigo])
REFERENCES [dbo].[CertificacaoProdutosCodigoBarras] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_CertificacaoProdutosCodigoBarras]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Produto]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Usuario]
GO

-------------------------------------------------------------
---------	CERTIFICA��O PRODUTOS PESOS E MEDIDAS	---------
-------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CertificacaoProdutosPesosMedidas') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CertificacaoProdutosPesosMedidas;

CREATE TABLE [dbo].[CertificacaoProdutosPesosMedidas](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosPesosMedidas] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Associado]
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Produto]
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidas] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidas_Usuario]
GO

---------------------------------------------------------------------
---------	CERTIFICA��O PRODUTOS PESOS MEDIDAS HISTORICO	---------
---------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CertificacaoProdutosPesosMedidasHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CertificacaoProdutosPesosMedidasHistorico;

CREATE TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico](
	[Codigo] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosPesosMedidasHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Associado]
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_CertificacaoProdutosCodigoBarras] FOREIGN KEY([Codigo])
REFERENCES [dbo].[CertificacaoProdutosCodigoBarras] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_CertificacaoProdutosCodigoBarras]
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Produto]
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Usuario]
GO


-------------------------------------------------
---------			CERTIFICADO			---------
-------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Certificado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Certificado;

CREATE TABLE Certificado ( 
	Codigo int NOT NULL,
	Sigla varchar(15),
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Vigencia varchar(1),
	ValorVigencia int
);

ALTER TABLE Certificado ADD CONSTRAINT PK_Certificado 
	PRIMARY KEY CLUSTERED (Codigo);



----------------------------------------------------
----------------	STATUS PREFIXO	----------------
----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusPrefixo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusPrefixo;

CREATE TABLE StatusPrefixo ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE StatusPrefixo ADD CONSTRAINT PK_StatusPrefixo 
	PRIMARY KEY CLUSTERED (Codigo);


--------------------------------------------------
----------------   	LICENCA	   -------------------
--------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Licenca') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Licenca;

CREATE TABLE Licenca ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(500),
	Status int,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE Licenca ADD CONSTRAINT PK_Licenca 
	PRIMARY KEY CLUSTERED (Codigo);



--------------------------------------------------
--------------	LICENCA ASSOCIADO	--------------
--------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LicencaAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LicencaAssociado;

CREATE TABLE LicencaAssociado ( 
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	Status int NOT NULL,
	DataAniversario datetime,
	DataAlteracao datetime
);

ALTER TABLE LicencaAssociado ADD CONSTRAINT PK_LicencaAssociado 
	PRIMARY KEY CLUSTERED (CodigoLicenca, CodigoAssociado);

ALTER TABLE LicencaAssociado ADD CONSTRAINT FK_LicencaAssociado_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE LicencaAssociado ADD CONSTRAINT FK_LicencaAssociado_Licenca 
	FOREIGN KEY (CodigoLicenca) REFERENCES Licenca (Codigo);

ALTER TABLE LicencaAssociado ADD CONSTRAINT FK_LicencaAssociado_StatusPrefixo 
	FOREIGN KEY (Status) REFERENCES StatusPrefixo (Codigo);



-------------------------------------------------
---------	PREFIXO LICENCA ASSOCIADO	---------
-------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PrefixoLicencaAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PrefixoLicencaAssociado;

CREATE TABLE PrefixoLicencaAssociado ( 
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	NumeroPrefixo bigint NOT NULL,
	CodigoStatusPrefixo int,
	DataAlteracao datetime
);

ALTER TABLE PrefixoLicencaAssociado ADD CONSTRAINT PK_PrefixoLicencaUsuario 
	PRIMARY KEY CLUSTERED (CodigoLicenca, CodigoAssociado, NumeroPrefixo);

ALTER TABLE PrefixoLicencaAssociado ADD CONSTRAINT FK_PrefixoLicencaAssociado_StatusPrefixo 
	FOREIGN KEY (CodigoStatusPrefixo) REFERENCES StatusPrefixo (Codigo);

ALTER TABLE PrefixoLicencaAssociado ADD CONSTRAINT FK_PrefixoLicencaUsuario_LicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado) REFERENCES LicencaAssociado (CodigoLicenca, CodigoAssociado);



-------------------------------------------------
-----------			CHAVE			-------------
-------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Chave') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Chave;

CREATE TABLE Chave ( 
	CodigoChave bigint identity(1,1)  NOT NULL,
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	NumeroPrefixo bigint NOT NULL,
	Chave varchar(max) NOT NULL,
	DataCriacao datetime NOT NULL,
	DataExpiracao datetime,
	DataExpiracaoOriginal datetime NOT NULL,
	CodigoUsuarioCriacao bigint NOT NULL,
	CodigoUsuarioExpiracao bigint
);

ALTER TABLE Chave ADD CONSTRAINT PK_Chave 
	PRIMARY KEY CLUSTERED (CodigoChave);

ALTER TABLE Chave ADD CONSTRAINT FK_Chave_PrefixoLicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado, NumeroPrefixo) REFERENCES PrefixoLicencaAssociado (CodigoLicenca, CodigoAssociado, NumeroPrefixo);

ALTER TABLE Chave ADD CONSTRAINT FK_Chave_UsuarioExpiracao 
	FOREIGN KEY (CodigoUsuarioExpiracao) REFERENCES Usuario (Codigo);

ALTER TABLE Chave ADD CONSTRAINT FK_Chave_UsuarioCriacao 
	FOREIGN KEY (CodigoUsuarioCriacao) REFERENCES Usuario (Codigo);


----------------------------------------------------
------------		TIPO CONTATO		------------
----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoContato') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoContato;

CREATE TABLE TipoContato ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status varchar(1) NOT NULL
);

ALTER TABLE TipoContato ADD CONSTRAINT PK_TipoContato 
	PRIMARY KEY CLUSTERED (Codigo);



-------------------------------------------------
---------		CONTATO ASSOCIADO		---------
-------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ContatoAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ContatoAssociado;

CREATE TABLE [dbo].[ContatoAssociado](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[CodigoAssociado] [bigint] NULL,
	[Nome] [varchar](255) NOT NULL,
	[CPFCNPJ] [varchar](14) NOT NULL,
	[CodigoTipoContato] [int] NOT NULL,
	[Telefone1] [varchar](50) NULL,
	[Telefone2] [varchar](50) NULL,
	[Telefone3] [varchar](50) NULL,
	[Email] [varchar](255) NULL,
	[Email2] [varchar](255) NULL,
	[Status] [varchar](1) NULL,
	[DataAtualizacaoCRM] [datetime] NULL,
	[Observacao] [text] NULL,
	[IndicadorPrincipal] [varchar](1) NULL,
 CONSTRAINT [PK_ContatoAssociado] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ContatoAssociado]  WITH CHECK ADD  CONSTRAINT [FK_ContatoAssociado_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[ContatoAssociado] CHECK CONSTRAINT [FK_ContatoAssociado_Associado]
GO
ALTER TABLE [dbo].[ContatoAssociado]  WITH CHECK ADD  CONSTRAINT [FK_ContatoAssociado_TipoContato] FOREIGN KEY([CodigoTipoContato])
REFERENCES [dbo].[TipoContato] ([Codigo])
GO
ALTER TABLE [dbo].[ContatoAssociado] CHECK CONSTRAINT [FK_ContatoAssociado_TipoContato]
GO


---------------------------------------------------------
---------		CONTATO ASSOCIADO HIST�RICO		---------
---------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ContatoAssociadoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ContatoAssociadoHistorico;

CREATE TABLE [dbo].[ContatoAssociadoHistorico](
	[Codigo] [int] NOT NULL,
	[CodigoAssociado] [bigint] NULL,
	[Nome] [varchar](255) NOT NULL,
	[CPFCNPJ] [varchar](14) NOT NULL,
	[CodigoTipoContato] [int] NOT NULL,
	[Telefone1] [varchar](50) NULL,
	[Telefone2] [varchar](50) NULL,
	[Telefone3] [varchar](50) NULL,
	[Email] [varchar](255) NULL,
	[Email2] [varchar](255) NULL,
	[Status] [varchar](1) NULL,
	[Observacao] [text] NULL,
	[DataHistorico] [datetime] NOT NULL,
	[IndicadorPrincipal] [varchar](1) NULL,
 CONSTRAINT [PK_ContatoAssociadoHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ContatoAssociadoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_ContatoAssociadoHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[ContatoAssociadoHistorico] CHECK CONSTRAINT [FK_ContatoAssociadoHistorico_Associado]
GO
ALTER TABLE [dbo].[ContatoAssociadoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_ContatoAssociadoHistorico_ContatoAssociado] FOREIGN KEY([Codigo])
REFERENCES [dbo].[ContatoAssociado] ([Codigo])
GO
ALTER TABLE [dbo].[ContatoAssociadoHistorico] CHECK CONSTRAINT [FK_ContatoAssociadoHistorico_ContatoAssociado]
GO
ALTER TABLE [dbo].[ContatoAssociadoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_ContatoAssociadoHistorico_TipoContato] FOREIGN KEY([CodigoTipoContato])
REFERENCES [dbo].[TipoContato] ([Codigo])
GO
ALTER TABLE [dbo].[ContatoAssociadoHistorico] CHECK CONSTRAINT [FK_ContatoAssociadoHistorico_TipoContato]
GO


-----------------------------------------------------
---------		COUNTRYOFORIGINISO3166		---------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CountryOfOriginISO3166') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CountryOfOriginISO3166;

CREATE TABLE [dbo].[CountryOfOriginISO3166](
	[Codigo] [int] NOT NULL,
	[CodigoPais] [varchar](50) NULL,
	[Nome] [varchar](255) NULL,
	[Sigla2] [varchar](2) NOT NULL,
	[Sigla3] [varchar](3) NULL,
	[CodigoTelefonico] [varchar](100) NULL,
	[SiglaIANA] [varchar](100) NULL,
 CONSTRAINT [PK_CountryOfOriginISO3166] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


-----------------------------------------------------
------------		DISPOSITIONS		-------------
-----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Dispositions') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Dispositions;

CREATE TABLE Dispositions ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL, 
	Descricao varchar(255),
	CodigoBizStep int NOT NULL,
	Status int NOT NULL
);

ALTER TABLE Dispositions ADD CONSTRAINT PK_Dispositions
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Dispositions ADD CONSTRAINT FK_Dispositions_BizStep
	FOREIGN KEY (CodigoBizStep) REFERENCES BizStep (Codigo);
	


----------------------------------------------
--------------	TIPO FILTRO		--------------
----------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoFiltro') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoFiltro;

CREATE TABLE TipoFiltro ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE TipoFiltro ADD CONSTRAINT PK_TipoFiltro 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TipoFiltro ADD CONSTRAINT FK_TipoFiltro_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)



------------------------------------------------
------------		TIPO HASH		------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoHash') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoHash;

CREATE TABLE TipoHash ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	QuantidadeMaximaCaracteres int NOT NULL
);

ALTER TABLE TipoHash ADD CONSTRAINT PK_TipoHash 
	PRIMARY KEY CLUSTERED (Codigo);



----------------------------------------------
--------------		EPCRFID		--------------
----------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('EPCRFID') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE EPCRFID;

CREATE TABLE EPCRFID ( 
	Codigo bigint identity(1,1)  NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoTipoHash int NOT NULL,
	SerialInicio varchar(16) NOT NULL,
	SerialFim varchar(16),
	CodigoUsuarioAlteracao bigint NOT NULL,
	CodigoTipoFiltro int NOT NULL
);

ALTER TABLE EPCRFID ADD CONSTRAINT PK_EPCRFID 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE EPCRFID ADD CONSTRAINT FK_EPCRFID_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto)
	

ALTER TABLE EPCRFID ADD CONSTRAINT FK_EPCRFID_TipoFiltro 
	FOREIGN KEY (CodigoTipoFiltro) REFERENCES TipoFiltro (Codigo)
	

ALTER TABLE EPCRFID ADD CONSTRAINT FK_EPCRFID_TipoHash 
	FOREIGN KEY (CodigoTipoHash) REFERENCES TipoHash (Codigo)
	

ALTER TABLE EPCRFID ADD CONSTRAINT FK_EPCRFID_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	

------------------------------------------------
------------		FABRICANTE		------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Fabricante') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Fabricante;

CREATE TABLE Fabricante ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE Fabricante ADD CONSTRAINT PK_Fabricante 
	PRIMARY KEY CLUSTERED (Codigo);


------------------------------------------------
------------	STATUS PUBLICA��O	------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusPublicacao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusPublicacao;

CREATE TABLE [dbo].[StatusPublicacao](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NULL
 );

ALTER TABLE StatusPublicacao ADD CONSTRAINT PK_StatusPublicacao 
	PRIMARY KEY CLUSTERED (Codigo);


-------------------------------------------------------
------------	STATUS VISUALIZA��O FAQ	   ------------
-------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusVisualizacaoFAQ') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusVisualizacaoFAQ;
CREATE TABLE StatusVisualizacaoFAQ(
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_StatusVisualizacaoFAQ] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


-------------------------------------------
---------------		FAQ		---------------
-------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FAQ') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FAQ;
CREATE TABLE FAQ(
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Descricao] [text] NOT NULL,
	[Url] [varchar](max) NULL,
	[Ordem] [int] NULL,
	[DataCadastro] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[CodigoStatusVisualizacaoFAQ] [int] NOT NULL,
 CONSTRAINT [PK_FAQ] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_Idioma]
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_StatusPublicacao]
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_StatusVisualizacaoFAQ] FOREIGN KEY([CodigoStatusVisualizacaoFAQ])
REFERENCES [dbo].[StatusVisualizacaoFAQ] ([Codigo])
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_StatusVisualizacaoFAQ]
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_Usuario]
GO


----------------------------------------------------
------------		FAQ HIST�RICO		------------
----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FAQHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FAQHistorico;
CREATE TABLE FAQHistorico(
	[Codigo] [bigint] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Descricao] [text] NOT NULL,
	[Url] [varchar](max) NULL,
	[Ordem] [int] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[CodigoStatusVisualizacaoFAQ] [int] NOT NULL,
 CONSTRAINT [PK_FAQHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_FAQ] FOREIGN KEY([Codigo])
REFERENCES [dbo].[FAQ] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_FAQ]
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_Idioma]
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_StatusPublicacao]
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_StatusVisualizacaoFAQ] FOREIGN KEY([CodigoStatusVisualizacaoFAQ])
REFERENCES [dbo].[StatusVisualizacaoFAQ] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_StatusVisualizacaoFAQ]
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_Usuario]
GO


------------------------------------------------
------------	MODELO ETIQUETA		------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ModeloEtiqueta') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ModeloEtiqueta;

CREATE TABLE ModeloEtiqueta ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	CodigoFabricante int,
	Largura decimal(10,2),
	Altura decimal(10,2),
	TamanhoFolha varchar(50),
	QuantidadeColunas int,
	QuantidadeLinhas int,
	MargemSuperior decimal(10,2),
	MargemInferior decimal(10,2),
	MargemEsquerda decimal(10,2),
	MargemDireita decimal(10,2),
	EspacamentoVertical decimal(10,2),
	EspacamentoHorizontal decimal(10,2)
);

ALTER TABLE ModeloEtiqueta ADD CONSTRAINT PK_ModeloEtiqueta 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ModeloEtiqueta ADD CONSTRAINT FK_ModelEtiqueta_Fabricante 
	FOREIGN KEY (CodigoFabricante) REFERENCES Fabricante (Codigo);



---------------------------------------------------
----------------		M�DULO		---------------
---------------------------------------------------	
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Modulo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Modulo;

CREATE TABLE Modulo ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int,
	Icone varchar(100),
	Ordem int,
	Url varchar(255)
);

ALTER TABLE Modulo ADD CONSTRAINT PK_Modulo 
	PRIMARY KEY CLUSTERED (Codigo);



--------------------------------------------------
---------------		FORMUL�RIO		--------------
--------------------------------------------------	
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Formulario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Formulario;

CREATE TABLE Formulario ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int,
	CodigoModulo int NOT NULL,
	Url varchar(255) NOT NULL,
	Icone varchar(100),
	Ordem int,
	IndicadorMenu varchar(1) NOT NULL
);

ALTER TABLE Formulario ADD CONSTRAINT PK_Formulario 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Formulario ADD CONSTRAINT FK_Formulario_Modulo 
	FOREIGN KEY (CodigoModulo) REFERENCES Modulo (Codigo);


---------------------------------------------------
---------------		STATUS AJUDA	---------------
---------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusAjuda') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusAjuda;

CREATE TABLE StatusAjuda (
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255) NULL,
	Status int NOT NULL,
 CONSTRAINT [PK_StatusAjuda] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



------------------------------------------------
---------------		TIPO AJUDA	 ---------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoAjuda') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoAjuda;

CREATE TABLE TipoAjuda ( 
	Codigo int NOT NULL,
	Nome varchar(100),
	Descricao varchar(255),
	Status int
);

ALTER TABLE TipoAjuda ADD CONSTRAINT PK_TipoAjuda 
	PRIMARY KEY CLUSTERED (Codigo);



------------------------------------------------
------------	FORMUL�RIO AJUDA	------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioAjuda') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioAjuda;

CREATE TABLE [dbo].[FormularioAjuda](
	[Codigo] [int] NOT NULL,
	[CodigoFormulario] [int] NOT NULL,
	[CodigoTipoAjuda] [int] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Ajuda] [text] NULL,
	[UrlVideo] [varchar](max) NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
 CONSTRAINT [PK_FormularioAjuda] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])

ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_Formulario]
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])

ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_Idioma]
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_StatusAjuda] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusAjuda] ([Codigo])

ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_StatusAjuda]
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])

ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_StatusPublicacao]
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_TipoAjuda] FOREIGN KEY([CodigoTipoAjuda])
REFERENCES [dbo].[TipoAjuda] ([Codigo])

ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_TipoAjuda]
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])

ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_Usuario]



------------------------------------------------------------
------------	FORMUL�RIO AJUDA HIST�RICO  	------------
------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioAjudaHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioAjudaHistorico;

CREATE TABLE [dbo].[FormularioAjudaHistorico](
	[CodigoFormularioAjuda] [int] NOT NULL,
	[CodigoFormulario] [int] NOT NULL,
	[CodigoTipoAjuda] [int] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Ajuda] [text] NULL,
	[UrlVideo] [varchar](max) NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
 CONSTRAINT [PK_FormularioAjudaHistorico] PRIMARY KEY CLUSTERED 
(
	[CodigoFormularioAjuda] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])

ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_Formulario]
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_FormularioAjuda] FOREIGN KEY([CodigoFormularioAjuda])
REFERENCES [dbo].[FormularioAjuda] ([Codigo])

ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_FormularioAjuda]
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])

ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_Idioma]
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_StatusAjuda] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusAjuda] ([Codigo])

ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_StatusAjuda]
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])

ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_StatusPublicacao]
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_TipoAjuda] FOREIGN KEY([CodigoTipoAjuda])
REFERENCES [dbo].[TipoAjuda] ([Codigo])

ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_TipoAjuda]
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])

ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_Usuario]
GO

----------------------------------------------------------------
------------	FORMUL�RIO AJUDA PASSO A PASSO  	------------
----------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioAjudaPassoAPasso') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioAjudaPassoAPasso;

CREATE TABLE FormularioAjudaPassoAPasso (
	[Codigo] [int] NOT NULL,
	[CodigoFormulario] [int] NOT NULL,
	[CodigoTipoAjuda] [int] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Ajuda] [varchar](max) NULL,
	[Ordem] [int] NULL,
	[UrlVideo] [varchar](max) NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
 CONSTRAINT [PK_FormularioAjudaPassoAPasso] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[FormularioAjudaPassoAPasso]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPasso_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
ALTER TABLE [dbo].[FormularioAjudaPassoAPasso] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPasso_Formulario]

ALTER TABLE [dbo].[FormularioAjudaPassoAPasso]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPasso_TipoAjuda] FOREIGN KEY([CodigoTipoAjuda])
REFERENCES [dbo].[TipoAjuda] ([Codigo])
ALTER TABLE [dbo].[FormularioAjudaPassoAPasso] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPasso_TipoAjuda]

ALTER TABLE [dbo].[FormularioAjudaPassoAPasso]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPasso_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
ALTER TABLE [dbo].[FormularioAjudaPassoAPasso] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPasso_Usuario]



------------------------------------------------------------------------
------------	FORMUL�RIO AJUDA PASSO A PASSO HIST�RICO	------------
------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioAjudaPassoAPassoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioAjudaPassoAPassoHistorico;

CREATE TABLE FormularioAjudaPassoAPassoHistorico (
	CodigoFormularioAjudaPassoAPasso [int] NOT NULL,
	CodigoFormulario [int] NOT NULL,
	CodigoTipoAjuda [int] NOT NULL,
	Nome [varchar](255) NOT NULL,
	Ajuda [varchar](max) NULL,
	Ordem [int] NULL,
	UrlVideo [varchar](max) NULL,
	CodigoStatusPublicacao [int] NOT NULL,
	CodigoIdioma [int] NOT NULL,
	CodigoUsuarioAlteracao [bigint] NOT NULL,
	DataAlteracao [datetime] NOT NULL,
	DataHistorico [datetime] NOT NULL,
 CONSTRAINT [PK_FormularioAjudaPassoAPassoHistorico] PRIMARY KEY CLUSTERED 
(
	[CodigoFormularioAjudaPassoAPasso] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_Formulario]

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_FormularioAjuda] FOREIGN KEY([CodigoFormularioAjudaPassoAPasso])
REFERENCES [dbo].[FormularioAjudaPassoAPasso] ([Codigo])
ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_FormularioAjuda]

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_TipoAjuda] FOREIGN KEY([CodigoTipoAjuda])
REFERENCES [dbo].[TipoAjuda] ([Codigo])
ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_TipoAjuda]

ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
ALTER TABLE [dbo].[FormularioAjudaPassoAPassoHistorico] CHECK CONSTRAINT [FK_FormularioAjudaPassoAPassoHistorico_Usuario]



------------------------------------------------
------------	FORMUL�RIO CAMPO	------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioCampo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioCampo;

CREATE TABLE FormularioCampo ( 
	CodigoFormulario int NOT NULL,
	CodigoCampo bigint NOT NULL
);

ALTER TABLE FormularioCampo ADD CONSTRAINT PK_FormularioCampo 
	PRIMARY KEY CLUSTERED (CodigoFormulario, CodigoCampo);

ALTER TABLE FormularioCampo ADD CONSTRAINT FK_FormularioCampo_Campo 
	FOREIGN KEY (CodigoCampo) REFERENCES Campo (Codigo);

ALTER TABLE FormularioCampo ADD CONSTRAINT FK_FormularioCampo_Formulario 
	FOREIGN KEY (CodigoFormulario) REFERENCES Formulario (Codigo);



------------------------------------------------
------------	FORMUL�RIO IDIOMA	------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioIdioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioIdioma;
CREATE TABLE FormularioIdioma (
	CodigoFormulario [int] NOT NULL,
	CodigoIdioma [int] NOT NULL,
	Nome [varchar](50) NOT NULL,
	Descricao [varchar](255) NOT NULL,
	Status [int] NULL,
 CONSTRAINT [PK_FormularioIdioma] PRIMARY KEY CLUSTERED 
(
	[CodigoFormulario] ASC,
	[CodigoIdioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[FormularioIdioma]  WITH CHECK ADD  CONSTRAINT [FK_FormularioIdioma_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
ALTER TABLE [dbo].[FormularioIdioma] CHECK CONSTRAINT [FK_FormularioIdioma_Formulario]

ALTER TABLE [dbo].[FormularioIdioma]  WITH CHECK ADD  CONSTRAINT [FK_FormularioIdioma_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
ALTER TABLE [dbo].[FormularioIdioma] CHECK CONSTRAINT [FK_FormularioIdioma_Idioma]



----------------------------------------------------
--------------	  FUNCIONALIDADE	  --------------
----------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Funcionalidade') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Funcionalidade;

CREATE TABLE Funcionalidade ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	CodigoTipoUsuario int NOT NULL,
	CodigoFormulario int
);

ALTER TABLE Funcionalidade ADD CONSTRAINT PK_Funcionalidade 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Funcionalidade ADD CONSTRAINT FK_Funcionalidade_Formulario 
	FOREIGN KEY (CodigoFormulario) REFERENCES Formulario (Codigo);

ALTER TABLE Funcionalidade ADD CONSTRAINT FK_Funcionalidade_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo)
	


--------------------------------------------------------
--------------	  FUNCIONALIDADE IDIOMA	  --------------
--------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FuncionalidadeIdioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FuncionalidadeIdioma;

CREATE TABLE FuncionalidadeIdioma ( 
	CodigoFuncionalidade int NOT NULL,
	CodigoIdioma int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE FuncionalidadeIdioma ADD CONSTRAINT PK_FuncionalidadeIdioma 
	PRIMARY KEY CLUSTERED (CodigoFuncionalidade, CodigoIdioma);

ALTER TABLE FuncionalidadeIdioma ADD CONSTRAINT FK_FuncionalidadeIdioma_Funcionalidade 
	FOREIGN KEY (CodigoFuncionalidade) REFERENCES Funcionalidade (Codigo);

ALTER TABLE FuncionalidadeIdioma ADD CONSTRAINT FK_FuncionalidadeIdioma_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);



------------------------------------------------------------
--------------	  GDSNUNITOFMEASURECODELIST	  --------------
------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('GDSNUnitOfMeasureCodeList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE GDSNUnitOfMeasureCodeList;

CREATE TABLE GDSNUnitOfMeasureCodeList ( 
	Codigo int NOT NULL,
	CodeValue varchar(3) NOT NULL,
	Name varchar(300) NOT NULL,
	Definition text,
	Status int NOT NULL
);

ALTER TABLE GDSNUnitOfMeasureCodeList ADD CONSTRAINT PK_GDSNUnitOfMeasureCodeList 
	PRIMARY KEY CLUSTERED (Codigo);



------------------------------------------------
--------------	  POSI��O TEXTO	  --------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PosicaoTexto') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PosicaoTexto;

CREATE TABLE PosicaoTexto ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE PosicaoTexto ADD CONSTRAINT PK_PosicaoTexto 
	PRIMARY KEY CLUSTERED (Codigo);



------------------------------------------------
--------------	  TIPO GERA��O 	  --------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoGeracao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoGeracao;

CREATE TABLE TipoGeracao ( 
	Codigo int NOT NULL,
	Nome varchar(100),
	Descricao varchar(255),
	Status int
);

ALTER TABLE TipoGeracao ADD CONSTRAINT PK_TipoGeracao 
	PRIMARY KEY CLUSTERED (Codigo);


------------------------------------------------------------------------------------
--------------	  MODELO ETIQUETA BARCODE TYPELIST MAGNIFICATION	  --------------
------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ModeloEtiquetaBarCodeTypeListMagnification') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ModeloEtiquetaBarCodeTypeListMagnification;

CREATE TABLE ModeloEtiquetaBarCodeTypeListMagnification ( 
	CodigoModeloEtiqueta int NOT NULL,
	CodigoBarCodeTypeList int NOT NULL,
	CodigoBarCodeTypeListMagnification int NOT NULL
);

ALTER TABLE ModeloEtiquetaBarCodeTypeListMagnification ADD CONSTRAINT PK_ModeloEtiquetaBarCodeTypeListMagnification 
	PRIMARY KEY CLUSTERED (CodigoModeloEtiqueta, CodigoBarCodeTypeList);

ALTER TABLE ModeloEtiquetaBarCodeTypeListMagnification ADD CONSTRAINT FK_ModeloEtiquetaBarCodeTypeListMagnification_BarCodeTypeList 
	FOREIGN KEY (CodigoBarCodeTypeList) REFERENCES BarCodeTypeList (Codigo);

ALTER TABLE ModeloEtiquetaBarCodeTypeListMagnification ADD CONSTRAINT FK_ModeloEtiquetaBarCodeTypeListMagnification_BarCodeTypeListMagnification 
	FOREIGN KEY (CodigoBarCodeTypeListMagnification) REFERENCES BarCodeTypeListMagnification (Codigo);

ALTER TABLE ModeloEtiquetaBarCodeTypeListMagnification ADD CONSTRAINT FK_ModeloEtiquetaBarCodeTypeListMagnification_ModeloEtiqueta 
	FOREIGN KEY (CodigoModeloEtiqueta) REFERENCES ModeloEtiqueta (Codigo);



----------------------------------------------------------------
--------------	  HIST�RICO GERA��O ETIQUETA	  --------------
----------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('HistoricoGeracaoEtiqueta') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE HistoricoGeracaoEtiqueta;

CREATE TABLE HistoricoGeracaoEtiqueta ( 
	Codigo bigint identity(1,1)  NOT NULL,
	CodigoUsuario bigint NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoTipoGeracao int,
	DataImpressao datetime NOT NULL,
	Texto varchar(300),
	CodigoFabricante bigint,
	CodigoModeloEtiqueta int,
	Quantidade int,
	Descricao varchar(255),
	CodigoPosicaoTexto int,
	CodigoBarCodeTypeList int NOT NULL,
	FonteNomeDescricao varchar(100),
	FonteTamanhoDescricao int,
	AlinhamentoDescricao varchar(50),
	FonteNomeTextoLivre varchar(100),
	FonteTamanhoTextoLivre int,
	AlinhamentoTextoLivre varchar(50)
);

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT PK_HistoricoGeracaoEtiqueta 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT FK_HistoricoGeracaoEtiqueta_BarCodeTypeList 
	FOREIGN KEY (CodigoBarCodeTypeList) REFERENCES BarCodeTypeList (Codigo);

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT FK_HistoricoGeracaoEtiqueta_PosicaoTexto 
	FOREIGN KEY (CodigoPosicaoTexto) REFERENCES PosicaoTexto (Codigo);

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT FK_HistoricoGeracaoEtiqueta_TipoGeracao 
	FOREIGN KEY (CodigoTipoGeracao) REFERENCES TipoGeracao (Codigo);

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT FK_HistoricoGeracaoEtiqueta_ModeloEtiqueta 
	FOREIGN KEY (CodigoModeloEtiqueta) REFERENCES ModeloEtiqueta (Codigo)



--------------------------------------------------------
--------------	  IMPORTA��O RESULTADO	  --------------
--------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoResultado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoResultado;

CREATE TABLE ImportacaoResultado ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoResultado ADD CONSTRAINT PK_ImportacaoResultado 
	PRIMARY KEY CLUSTERED (Codigo);



------------------------------------------------
--------------	  IMPORTA��O	  --------------
------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Importacao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Importacao;

CREATE TABLE Importacao ( 
	Codigo int identity(1,1)  NOT NULL,
	Arquivo varchar(500) NOT NULL,
	Data datetime NOT NULL,
	ItensImportados int NOT NULL,
	ItensNaoImportados int NOT NULL,
	CodigoImportacaoResultado int NOT NULL,
	TempoImportacao datetime NOT NULL,
	Status int NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE Importacao ADD CONSTRAINT PK_Importacao 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Importacao ADD CONSTRAINT PK_Importacao_ImportacaoResultado 
	FOREIGN KEY (CodigoImportacaoResultado) REFERENCES ImportacaoResultado (Codigo)
	

ALTER TABLE Importacao ADD CONSTRAINT PK_Importacao_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	


------------------------------------------------------------
--------------	  IMPORTA��O MODELO TIPO	  --------------
------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoModeloTipo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoModeloTipo;

CREATE TABLE ImportacaoModeloTipo ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoModeloTipo ADD CONSTRAINT PK_Importa��oModeloTipo 
	PRIMARY KEY CLUSTERED (Codigo);



--------------------------------------------------------
--------------	  IMPORTA��O MODELOS	  --------------
--------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoModelos') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoModelos;

CREATE TABLE ImportacaoModelos ( 
	Codigo int NOT NULL,
	CodigoModeloTipo int NOT NULL,
	CodigoImportacaoTipoItem int NOT NULL,
	Data datetime NOT NULL,
	Arquivo varchar(500) NOT NULL,
	Nome varchar(500) NOT NULL
);

ALTER TABLE ImportacaoModelos ADD CONSTRAINT PK_ImportacaoModelos 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ImportacaoModelos ADD CONSTRAINT PK_ImportacaoModelos_ImportacaoModeloTipo 
	FOREIGN KEY (CodigoModeloTipo) REFERENCES ImportacaoModeloTipo (Codigo)
	


---------------------------------------------------------
--------------- 	  IMPORTA��O TIPO	  ---------------
---------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoTipo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoTipo;

CREATE TABLE ImportacaoTipo ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoTipo ADD CONSTRAINT PK_ImportacaoTipo 
	PRIMARY KEY CLUSTERED (Codigo);



--------------------------------------------------------
--------------	  IMPORTA��O TIPO ITEM	  --------------
--------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoTipoItem') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoTipoItem;

CREATE TABLE ImportacaoTipoItem ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoTipoItem ADD CONSTRAINT PK_ImportacaoTipoItem 
	PRIMARY KEY CLUSTERED (Codigo);



--------------------------------------------------------
--------------	  IMPORTA��O PRODUTOS	  --------------
--------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoProdutos') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoProdutos;

CREATE TABLE ImportacaoProdutos (
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Arquivo] [varchar](500) NULL,
	[Data] [datetime] NOT NULL,
	[ItensImportados] [int] NOT NULL,
	[ItensNaoImportados] [int] NOT NULL,
	[CodigoImportacaoResultado] [int] NOT NULL,
	[CodigoImportacaoTipo] [int] NOT NULL,
	[CodigoImportacaoTipoItem] [int] NOT NULL,
	[TempoImportacao] [time](7) NOT NULL,
	[Status] [int] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[ArquivoRelatorio] [varchar](500) NULL,
	[CodigoAssociado] [bigint] NOT NULL
);

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos_ImportacaoResultado 
	FOREIGN KEY (CodigoImportacaoResultado) REFERENCES ImportacaoResultado (Codigo)

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos_ImportacaoTipo 
	FOREIGN KEY (CodigoImportacaoTipo) REFERENCES ImportacaoTipo (Codigo)

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos_ImportacaoTipoItem 
	FOREIGN KEY (CodigoImportacaoTipoItem) REFERENCES ImportacaoTipoItem (Codigo)

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	

------------------------------------------------------------
--------------	  IMPORTA��O STATUS MOTIVOS	  --------------
------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoStatusMotivos') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoStatusMotivos;

CREATE TABLE ImportacaoStatusMotivos ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoStatusMotivos ADD CONSTRAINT PK_ImportacaoStatusMotivos 
	PRIMARY KEY CLUSTERED (Codigo);



--------------------------------------------------------------------
--------------	  IMPORT CLASSIFICATION TYPELIST	  --------------
--------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportClassificationTypeList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportClassificationTypeList;

CREATE TABLE ImportClassificationTypeList ( 
	Codigo int NOT NULL,
	CodeValue varchar(100) NOT NULL,
	Name varchar(500) NOT NULL
);

ALTER TABLE ImportClassificationTypeList ADD CONSTRAINT PK_ImportClassificationTypeList 
	PRIMARY KEY CLUSTERED (Codigo);



----------------------------------------------------------------
--------------	  LICEN�A ASSOCIADO HIST�RICO	  --------------
----------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LicencaAssociadoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LicencaAssociadoHistorico;

CREATE TABLE LicencaAssociadoHistorico ( 
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	Status int NOT NULL,
	DataAniversario datetime,
	DataAlteracao datetime,
	DataHistorico datetime NOT NULL
);

ALTER TABLE LicencaAssociadoHistorico ADD CONSTRAINT PK_LicencaAssociadoHistorico 
	PRIMARY KEY CLUSTERED (CodigoLicenca, CodigoAssociado, DataHistorico);

ALTER TABLE LicencaAssociadoHistorico ADD CONSTRAINT FK_LicencaAssociadoHistorico_LicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado) REFERENCES LicencaAssociado (CodigoLicenca, CodigoAssociado);



---------------------------------------------------------------
-----------------	   LICEN�A HISTORICO	  -----------------
---------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LicencaHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LicencaHistorico;

CREATE TABLE LicencaHistorico ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(500),
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE LicencaHistorico ADD CONSTRAINT PK_LicencaHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE LicencaHistorico ADD CONSTRAINT FK_LicencaHistorico_Licenca 
	FOREIGN KEY (Codigo) REFERENCES Licenca (Codigo);



------------------------------------------------------------------
---------------------	   PAPEL GLN	     ---------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PapelGLN') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PapelGLN;

CREATE TABLE PapelGLN ( 
	Codigo int identity(1,1)  NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE PapelGLN ADD CONSTRAINT PK_PapelGLN 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE PapelGLN ADD CONSTRAINT FK_PapelGLN_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)



--------------------------------------------------------------
----------------	  LOCALIZA��O F�SICA	  ----------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LocalizacaoFisica') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LocalizacaoFisica;

CREATE TABLE [dbo].[LocalizacaoFisica](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](500) NULL,
	[Status] [int] NOT NULL,
	[Prefixo] [varchar](12) NOT NULL,
	[GLN] [bigint] NOT NULL,
	[NumeroItem] [decimal](16, 2) NOT NULL,
	[CodigoPapelGLN] [int] NOT NULL,
	[Endereco] [varchar](100) NOT NULL,
	[Numero] [varchar](50) NOT NULL,
	[Complemento] [varchar](100) NULL,
	[CEP] [varchar](20) NOT NULL,
	[Cidade] [varchar](255) NOT NULL,
	[Estado] [varchar](255) NOT NULL,
	[Pais] [varchar](255) NULL,
	[Bairro] [varchar](50) NOT NULL,
	[NomeImagem] [varchar](255) NULL,
	[Observacao] [varchar](200) NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[IndicadorPrincipal] [varchar](1) NULL,
	[DataCancelamento] [datetime] NULL,
	[DataSuspensao] [datetime] NULL,
	[DataReutilizacao] [datetime] NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[Telefone] [varchar](50) NULL,
	[Email] [varchar](255) NULL,
 CONSTRAINT [PK_LocalizacaoFisica] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_LocalizacaoFisica_GLN] UNIQUE NONCLUSTERED 
(
	[GLN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[LocalizacaoFisica]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisica_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
ALTER TABLE [dbo].[LocalizacaoFisica] CHECK CONSTRAINT [FK_LocalizacaoFisica_Associado]

ALTER TABLE [dbo].[LocalizacaoFisica]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisica_PapelGLN1] FOREIGN KEY([CodigoPapelGLN])
REFERENCES [dbo].[PapelGLN] ([Codigo])
ALTER TABLE [dbo].[LocalizacaoFisica] CHECK CONSTRAINT [FK_LocalizacaoFisica_PapelGLN1]

ALTER TABLE [dbo].[LocalizacaoFisica]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisica_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
ALTER TABLE [dbo].[LocalizacaoFisica] CHECK CONSTRAINT [FK_LocalizacaoFisica_Usuario]



----------------------------------------------------------------------
----------------	  LOCALIZA��O F�SICA HISTORICO 	  ----------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LocalizacaoFisicaHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LocalizacaoFisicaHistorico;

CREATE TABLE [dbo].[LocalizacaoFisicaHistorico](
	[Codigo] [bigint] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](500) NULL,
	[Status] [int] NOT NULL,
	[Prefixo] [varchar](12) NOT NULL,
	[GLN] [bigint] NOT NULL,
	[NumeroItem] [decimal](16, 2) NOT NULL,
	[CodigoPapelGLN] [int] NOT NULL,
	[Endereco] [varchar](100) NOT NULL,
	[Numero] [varchar](50) NOT NULL,
	[Complemento] [varchar](100) NULL,
	[CEP] [varchar](20) NOT NULL,
	[Cidade] [varchar](255) NOT NULL,
	[Estado] [varchar](255) NOT NULL,
	[Pais] [varchar](255) NULL,
	[Bairro] [varchar](50) NOT NULL,
	[NomeImagem] [varchar](255) NULL,
	[Observacao] [varchar](200) NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[IndicadorPrincipal] [varchar](1) NULL,
	[DataCancelamento] [datetime] NULL,
	[DataSuspensao] [datetime] NULL,
	[DataReutilizacao] [datetime] NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[Telefone] [varchar](50) NULL,
	[Email] [varchar](255) NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_LocalizacaoFisicaHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[LocalizacaoFisicaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisicaHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico] CHECK CONSTRAINT [FK_LocalizacaoFisicaHistorico_Associado]

ALTER TABLE [dbo].[LocalizacaoFisicaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisicaHistorico_LocalizacaoFisica] FOREIGN KEY([Codigo])
REFERENCES [dbo].[LocalizacaoFisica] ([Codigo])
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico] CHECK CONSTRAINT [FK_LocalizacaoFisicaHistorico_LocalizacaoFisica]

ALTER TABLE [dbo].[LocalizacaoFisicaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisicaHistorico_PapelGLN] FOREIGN KEY([CodigoPapelGLN])
REFERENCES [dbo].[PapelGLN] ([Codigo])
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico] CHECK CONSTRAINT [FK_LocalizacaoFisicaHistorico_PapelGLN]

ALTER TABLE [dbo].[LocalizacaoFisicaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisicaHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico] CHECK CONSTRAINT [FK_LocalizacaoFisicaHistorico_Usuario]



----------------------------------------------
----------------	  LOG	  ----------------
----------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Log') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Log;

CREATE TABLE Log ( 
	Codigo bigint identity(1,1)  NOT NULL,
	DataAlteracao datetime,
	CodigoUsuario bigint,
	Classe varchar(255),
	Metodo varchar(500),
	Requisicao text,
	CodigoEntidade bigint,
	URL varchar(500)
);

ALTER TABLE Log ADD CONSTRAINT PK_Log 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Log ADD CONSTRAINT FK_Log_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo)
	


--------------------------------------------------
----------------	  LOG ERRO	  ----------------
--------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LogErro') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LogErro;

CREATE TABLE LogErro ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Mensagem varchar(1000) NOT NULL,
	Descricao text,
	DataLog datetime,
	CodigoUsuario bigint,
	URL varchar(500),
	IP varchar(50),
	Navegador varchar(500),
	VersaoNavegador varchar(500),
	Email varchar(255),
	Requisicao text,
	Servidor varchar(500)
);

ALTER TABLE LogErro ADD CONSTRAINT PK_LogErro 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LogErro ADD CONSTRAINT FK_LogErro_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo)
	

----------------------------------------------------------
----------------	  LOG ERRO EXPURGO	  ----------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LogErroExpurgo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LogErroExpurgo;

CREATE TABLE LogErroExpurgo ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Mensagem varchar(1000) NOT NULL,
	Descricao text,
	DataLog datetime,
	CodigoUsuario bigint,
	URL varchar(500),
	IP varchar(50),
	Navegador varchar(500),
	VersaoNavegador varchar(500),
	Email varchar(255),
	Requisicao text,
	Servidor varchar(500),
	DataExpurgo datetime NOT NULL
);

ALTER TABLE LogErroExpurgo ADD CONSTRAINT PK_LogErroExpurgo 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LogErroExpurgo ADD CONSTRAINT FK_LogErroExpurgo_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo)



--------------------------------------------------------------
----------------	  LOG EXPURGO	  ----------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LogExpurgo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LogExpurgo;

CREATE TABLE LogExpurgo ( 
	Codigo bigint NOT NULL,
	DataAlteracao datetime,
	CodigoUsuarioAlteracao bigint,
	Classe varchar(255),
	Metodo varchar(500),
	Requisicao text,
	CodigoEntidade bigint,
	URL bigint,
	DataExpurgo datetime NOT NULL
);

ALTER TABLE LogExpurgo ADD CONSTRAINT PK_LogExpurgo 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LogExpurgo ADD CONSTRAINT FK_LogExpurgo_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);



------------------------------------------------------
----------------	  STATUS LOTE	  ----------------
------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusLote') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusLote;

CREATE TABLE StatusLote ( 
	Codigo int NOT NULL,
	Nome varchar(100),
	Descricao varchar(500),
	Status int
);

ALTER TABLE StatusLote ADD CONSTRAINT PK_StatusLote 
	PRIMARY KEY CLUSTERED (Codigo);



----------------------------------------------
----------------	  LOTE	  ----------------
----------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Lote') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Lote;

CREATE TABLE Lote ( 
	Codigo bigint identity(1,1)  NOT NULL,
	CodigoStatusLote int NOT NULL,
	CodigoProduto bigint NULL,
	Nrlote varchar(20) NULL,
	DataProcessamento Datetime NULL,
	DataVencimento Datetime NULL,
	TipoProcessamentoBizStep varchar(500) NULL,
	StatusProdutoDispositions varchar(500) NULL,
	InformacoesAdicionais varchar(500) NULL,
	DataAlteracao Datetime NULL,
	CodigoUsuarioAlteracao bigint NULL,
	CodigoAssociado bigint NULL,
	CodigoBizStep int NULL,
	CodigoDispositions int NULL,
	QuantidadeItens int NULL
);

ALTER TABLE Lote ADD CONSTRAINT PK_Lote
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Lote ADD CONSTRAINT FK_Lote_Produto
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE Lote ADD CONSTRAINT FK_Lote_Usuario
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);
	
ALTER TABLE Lote ADD CONSTRAINT FK_Lote_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);
	
ALTER TABLE Lote ADD CONSTRAINT FK_Lote_BizStep
	FOREIGN KEY (CodigoBizStep) REFERENCES BizStep (Codigo);
	
ALTER TABLE Lote ADD CONSTRAINT FK_Lote_Dispositions
	FOREIGN KEY (CodigoDispositions) REFERENCES Dispositions (Codigo);

ALTER TABLE Lote ADD CONSTRAINT FK_Lote_StatusLote
	FOREIGN KEY (CodigoStatusLote) REFERENCES StatusLote (Codigo);



----------------------------------------------------------
----------------	  LOTE HIST�RICO	  ----------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LoteHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LoteHistorico;

CREATE TABLE LoteHistorico ( 
	CodigoLote bigint NOT NULL,
	CodigoStatusLote int NOT NULL,
	CodigoProduto bigint NULL,
	Nrlote varchar(20) NULL,
	DataProcessamento Datetime NULL,
	DataVencimento Datetime NULL,
	TipoProcessamentoBizStep varchar(500) NULL,
	StatusProdutoDispositions varchar(500) NULL,
	InformacoesAdicionais varchar(500) NULL,
	DataAlteracao Datetime NULL,
	CodigoUsuarioAlteracao bigint NULL, 
	CodigoAssociado bigint NULL,
	CodigoBizStep int NULL,
	CodigoDispositions int NULL,
	QuantidadeItens int NULL, 
	DataHistorico Datetime NOT NULL
);

ALTER TABLE LoteHistorico ADD CONSTRAINT PK_LoteHistorico
	PRIMARY KEY CLUSTERED (CodigoLote, DataHistorico);

ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Lote
	FOREIGN KEY (CodigoLote) REFERENCES Lote (Codigo);	
	
ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Produto
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Usuario
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);
	
ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_BizStep
	FOREIGN KEY (CodigoBizStep) REFERENCES BizStep (Codigo);
	
ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Dispositions
	FOREIGN KEY (CodigoDispositions) REFERENCES Dispositions (Codigo);
	
ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_StatusLote
	FOREIGN KEY (CodigoStatusLote) REFERENCES StatusLote (Codigo);
	

--------------------------------------------------
----------------	  LOTE URL	  ----------------
--------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LoteURL') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LoteURL;

CREATE TABLE LoteURL ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Nome varchar(255) NOT NULL,
	URL varchar(max) NOT NULL,
	Status int NOT NULL,
	CodigoLote bigint NOT NULL
);

ALTER TABLE LoteURL ADD CONSTRAINT PK_LoteURL
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LoteURL ADD CONSTRAINT FK_LoteURL_Lote
	FOREIGN KEY (CodigoLote) REFERENCES Lote (Codigo);


--------------------------------------------------------------
----------------	  LOTE URL HISTORICO	  ----------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LoteURLHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LoteURLHistorico;

CREATE TABLE LoteURLHistorico ( 
	Codigo bigint NOT NULL,
	Nome varchar(255) NOT NULL,
	URL varchar(max) NOT NULL,
	Status int NOT NULL,
	CodigoLote bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE LoteURLHistorico ADD CONSTRAINT PK_LoteURLHistorico 
	PRIMARY KEY CLUSTERED (DataHistorico);

ALTER TABLE LoteURLHistorico ADD CONSTRAINT FK_LoteURLHistorico_Lote
	FOREIGN KEY (CodigoLote) REFERENCES Lote (Codigo);


--------------------------------------------------------------
-----------------	   M�DULO IDIOMA	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ModuloIdioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ModuloIdioma;

CREATE TABLE ModuloIdioma (
	CodigoModulo [int] NOT NULL,
	CodigoIdioma [int] NOT NULL,
	Nome [varchar](50) NOT NULL,
	Descricao [varchar](255) NULL,
	Status [int] NULL,
 CONSTRAINT [PK_ModuloIdioma] PRIMARY KEY CLUSTERED 
(
	[CodigoModulo] ASC,
	[CodigoIdioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[ModuloIdioma]  WITH CHECK ADD  CONSTRAINT [FK_ModuloIdioma_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
ALTER TABLE [dbo].[ModuloIdioma] CHECK CONSTRAINT [FK_ModuloIdioma_Idioma]

ALTER TABLE [dbo].[ModuloIdioma]  WITH CHECK ADD  CONSTRAINT [FK_ModuloIdioma_Modulo] FOREIGN KEY([CodigoModulo])
REFERENCES [dbo].[Modulo] ([Codigo])
ALTER TABLE [dbo].[ModuloIdioma] CHECK CONSTRAINT [FK_ModuloIdioma_Modulo]


--------------------------------------------------------------
-----------------	   M�DULO LINGUAGEM	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ModuloLinguagem') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ModuloLinguagem;

CREATE TABLE ModuloLinguagem ( 
	CodigoModulo int NOT NULL,
	CodigoIdioma int NOT NULL,
	Nome varbinary(50) NOT NULL,
	Descricao varchar(255),
	Status int
);

ALTER TABLE ModuloLinguagem ADD CONSTRAINT PK_ModuloLinguagem 
	PRIMARY KEY CLUSTERED (CodigoModulo, CodigoIdioma);

ALTER TABLE ModuloLinguagem ADD CONSTRAINT FK_ModuloLinguagem_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);

ALTER TABLE ModuloLinguagem ADD CONSTRAINT FK_ModuloLinguagem_Modulo 
	FOREIGN KEY (CodigoModulo) REFERENCES Modulo (Codigo);


--------------------------------------------------
-----------------	   NCM	    ------------------
--------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('NCM') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE NCM;

CREATE TABLE NCM ( 
	Codigo bigint NOT NULL,
	NCM varchar(10) NOT NULL,
	Descricao varchar(5000) NOT NULL,
	Status int
);

ALTER TABLE NCM ADD CONSTRAINT PK_NCM 
	PRIMARY KEY CLUSTERED (Codigo);


------------------------------------------------------------------
-----------------	   PACKAGING TYPE CODE	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PackagingTypeCode') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PackagingTypeCode;

CREATE TABLE PackagingTypeCode ( 
	Codigo int NOT NULL,
	CodeValue varchar(3) NOT NULL,
	Name varchar(300) NOT NULL,
	Definition text,
	Status int NOT NULL
);

ALTER TABLE PackagingTypeCode ADD CONSTRAINT PK_PackagingTypeCode 
	PRIMARY KEY CLUSTERED (Codigo);


--------------------------------------------------------------
-----------------	   PALLET TYPE CODE	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('palletTypeCode') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE palletTypeCode;

CREATE TABLE palletTypeCode ( 
	Codigo int NOT NULL,
	Code varchar(3) NOT NULL,
	CodeDescription varchar(3000) NOT NULL,
	Status int NOT NULL
);

ALTER TABLE palletTypeCode ADD CONSTRAINT PK_palletTypeCode 
	PRIMARY KEY CLUSTERED (Codigo);



------------------------------------------------------------------
-----------------	   PAPEL GLN HIST�RICO	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PapelGLNHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PapelGLNHistorico;

CREATE TABLE PapelGLNHistorico ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE PapelGLNHistorico ADD CONSTRAINT PK_PapelGLNHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE PapelGLNHistorico ADD CONSTRAINT FK_PapelGLNHistorico_PapelGLN 
	FOREIGN KEY (Codigo) REFERENCES PapelGLN (Codigo);

ALTER TABLE PapelGLNHistorico ADD CONSTRAINT FK_PapelGLNHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);



----------------------------------------------------------
-----------------	   PAR�METRO	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Parametro') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Parametro;

CREATE TABLE Parametro ( 
	Codigo int identity(1,1)  NOT NULL,
	Chave varchar(50) NOT NULL,
	Valor varchar(500),
	Status int NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE Parametro ADD CONSTRAINT PK_Parametro 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Parametro ADD CONSTRAINT FK_Parametro_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	

------------------------------------------------------------------
-----------------	   PAR�METRO HIST�RICO	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ParametroHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ParametroHistorico;

CREATE TABLE ParametroHistorico ( 
	CodigoParametro int NOT NULL,
	Chave varchar(50) NOT NULL,
	Valor varchar(500),
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE ParametroHistorico ADD CONSTRAINT PK_ParametroHistorico 
	PRIMARY KEY CLUSTERED (CodigoParametro, DataHistorico);

ALTER TABLE ParametroHistorico ADD CONSTRAINT FK_ParametroHistorico_Parametro 
	FOREIGN KEY (CodigoParametro) REFERENCES Parametro (Codigo)
	

ALTER TABLE ParametroHistorico ADD CONSTRAINT FK_ParametroHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	
	
----------------------------------------------------------------------
-----------------	   PERFIL FUNCIONALIDADE	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PerfilFuncionalidade') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PerfilFuncionalidade;

CREATE TABLE PerfilFuncionalidade ( 
	CodigoPerfil int NOT NULL,
	CodigoFuncionalidade int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE PerfilFuncionalidade ADD CONSTRAINT PK_PerfilFuncionalidade 
	PRIMARY KEY CLUSTERED (CodigoPerfil, CodigoFuncionalidade);

ALTER TABLE PerfilFuncionalidade ADD CONSTRAINT FK_PerfilFuncionalidade_Funcionalidade 
	FOREIGN KEY (CodigoFuncionalidade) REFERENCES Funcionalidade (Codigo)
	

ALTER TABLE PerfilFuncionalidade ADD CONSTRAINT FK_PerfilFuncionalidade_Perfil 
	FOREIGN KEY (CodigoPerfil) REFERENCES Perfil (Codigo)
	

ALTER TABLE PerfilFuncionalidade ADD CONSTRAINT FK_PerfilFuncionalidade_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)


----------------------------------------------------------
-----------------	   P�BLICO ALVO	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PublicoAlvo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PublicoAlvo;

CREATE TABLE PublicoAlvo ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255) NOT NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE PublicoAlvo ADD CONSTRAINT PK_PublicoAlvo
	PRIMARY KEY CLUSTERED (Codigo);


-----------------------------------------------------------------
-----------------	   PESQUISA SATISFA��O	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PesquisaSatisfacao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PesquisaSatisfacao;

CREATE TABLE PesquisaSatisfacao ( 
	Codigo int identity(1,1) NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	Inicio datetime NOT NULL,
	Fim datetime NOT NULL,
	Url varchar(max) NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	CodigoPublicoAlvo int NOT NULL, 
	DataAlteracao datetime NOT NULL
);

ALTER TABLE PesquisaSatisfacao ADD CONSTRAINT PK_PesquisaSatisfacao 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE PesquisaSatisfacao ADD CONSTRAINT FK_PesquisaSatisfacao_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE PesquisaSatisfacao ADD CONSTRAINT FK_PesquisaSatisfacao_PublicoAlvo
	FOREIGN KEY (CodigoPublicoAlvo) REFERENCES PublicoAlvo (Codigo);
	

------------------------------------------------------------------------------
-----------------	   PESQUISA SATISFA��O HIST�RICO	    ------------------
------------------------------------------------------------------------------	
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PesquisaSatisfacaoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PesquisaSatisfacaoHistorico;

CREATE TABLE PesquisaSatisfacaoHistorico ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	Inicio datetime NOT NULL,
	Fim datetime NOT NULL,
	Url varchar(max) NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	CodigoPublicoAlvo int NOT NULL, 
	DataAlteracao datetime NOT NULL, 
	DataHistorico datetime NOT NULL 
);

ALTER TABLE PesquisaSatisfacaoHistorico ADD CONSTRAINT PK_PesquisaSatisfacaoHistorico
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);
	
ALTER TABLE PesquisaSatisfacaoHistorico ADD CONSTRAINT FK_PesquisaSatisfacaoHistorico_PesquisaSatisfacao
	FOREIGN KEY (Codigo) REFERENCES PesquisaSatisfacao (Codigo);

ALTER TABLE PesquisaSatisfacaoHistorico ADD CONSTRAINT FK_PesquisaSatisfacaoHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE PesquisaSatisfacaoHistorico ADD CONSTRAINT FK_PesquisaSatisfacaoHistorico_PublicoAlvo
	FOREIGN KEY (CodigoPublicoAlvo) REFERENCES PublicoAlvo (Codigo);
	

---------------------------------------------------------------------------------
-----------------	   STATUS PESQUISA SATISFA��O USU�RIO	    ------------------
----------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusPesquisaSatisfacaoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusPesquisaSatisfacaoUsuario;

CREATE TABLE StatusPesquisaSatisfacaoUsuario ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255) NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL 
);

ALTER TABLE StatusPesquisaSatisfacaoUsuario ADD CONSTRAINT PK_StatusPesquisaSatisfacaoUsuario
	PRIMARY KEY CLUSTERED (Codigo);


--------------------------------------------------------------------------
-----------------	   PESQUISA SATISFA��O USU�RIO	    ------------------
--------------------------------------------------------------------------	
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PesquisaSatisfacaoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PesquisaSatisfacaoUsuario;

CREATE TABLE PesquisaSatisfacaoUsuario ( 
	Codigo int identity (1,1) NOT NULL,
	CodigoPesquisaSatisfacao int NOT NULL,
	CodigoUsuario bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoStatusPesquisaSatisfacaoUsuario int NOT NULL
);

ALTER TABLE PesquisaSatisfacaoUsuario ADD CONSTRAINT PK_PesquisaSatisfacaoUsuario
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE PesquisaSatisfacaoUsuario ADD CONSTRAINT FK_PesquisaSatisfacaoUsuario_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo);

ALTER TABLE PesquisaSatisfacaoUsuario ADD CONSTRAINT FK_PesquisaSatisfacaoUsuario_PesquisaSatisfacao
	FOREIGN KEY (CodigoPesquisaSatisfacao) REFERENCES PesquisaSatisfacao (Codigo);
	
ALTER TABLE PesquisaSatisfacaoUsuario ADD CONSTRAINT FK_PesquisaSatisfacaoUsuario_StatusPesquisaSatisfacaoUsuario
	FOREIGN KEY (CodigoStatusPesquisaSatisfacaoUsuario) REFERENCES StatusPesquisaSatisfacaoUsuario (Codigo);
	

---------------------------------------------------------------------------------
-----------------	   PREFIXO LICEN�A ASSOCIADO HIST�RICO	    ------------------
----------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PrefixoLicencaAssociadoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PrefixoLicencaAssociadoHistorico;

CREATE TABLE PrefixoLicencaAssociadoHistorico ( 
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	NumeroPrefixo bigint NOT NULL,
	Status int,
	DataAlteracao datetime,
	DataHistorico datetime NOT NULL
);

ALTER TABLE PrefixoLicencaAssociadoHistorico ADD CONSTRAINT PK_PrefixoLicencaAssociadoHistorico 
	PRIMARY KEY CLUSTERED (CodigoLicenca, CodigoAssociado, NumeroPrefixo, DataHistorico);

ALTER TABLE PrefixoLicencaAssociadoHistorico ADD CONSTRAINT FK_PrefixoLicencaAssociadoHistorico_LicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado) REFERENCES LicencaAssociado (CodigoLicenca, CodigoAssociado);

ALTER TABLE PrefixoLicencaAssociadoHistorico ADD CONSTRAINT FK_PrefixoLicencaAssociadoHistorico_PrefixoLicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado, NumeroPrefixo) REFERENCES PrefixoLicencaAssociado (CodigoLicenca, CodigoAssociado, NumeroPrefixo);



----------------------------------------------------------------------
-----------------	   PRODUTO BRICK TYPE VALUE	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoBrickTypeValue') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoBrickTypeValue;

CREATE TABLE [dbo].[ProdutoBrickTypeValue](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodeBrick] [varchar](10) NOT NULL,
	[CodeClass] [varchar](10) NOT NULL,
	[CodeFamily] [varchar](10) NOT NULL,
	[CodeSegment] [varchar](10) NOT NULL,
	[Idioma] [varchar](5) NOT NULL,
	[CodeType] [varchar](10) NOT NULL,
	[CodeValue] [varchar](10) NOT NULL
);

ALTER TABLE ProdutoBrickTypeValue ADD CONSTRAINT PK_ProdutoBrickTypeValue 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ProdutoBrickTypeValue ADD CONSTRAINT FK_ProdutoBrickTypeValue_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);


------------------------------------------------------------------
-----------------	   PRODUTO HIERARQUIA	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoHierarquia') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoHierarquia;

CREATE TABLE ProdutoHierarquia ( 
	CodigoProdutoSuperior bigint NOT NULL,
	CodigoProdutoInferior bigint NOT NULL,
	Quantidade int NOT NULL
);

ALTER TABLE ProdutoHierarquia ADD CONSTRAINT PK_ProdutoHierarquia 
	PRIMARY KEY CLUSTERED (CodigoProdutoSuperior, CodigoProdutoInferior);

ALTER TABLE ProdutoHierarquia ADD CONSTRAINT FK_ProdutoHierarquia_ProdutoInferior 
	FOREIGN KEY (CodigoProdutoInferior) REFERENCES Produto (CodigoProduto);

ALTER TABLE ProdutoHierarquia ADD CONSTRAINT FK_ProdutoHierarquia_ProdutoSuperior 
	FOREIGN KEY (CodigoProdutoSuperior) REFERENCES Produto (CodigoProduto);


------------------------------------------------------------------
-----------------	   PRODUTO HIST�RICO	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoHistorico;

CREATE TABLE ProdutoHistorico ( 
	CodigoProduto bigint NOT NULL,
	globalTradeItemNumber bigint,
	productDescription varchar(300) NOT NULL,
	CodigoTipoGTIN int NOT NULL,
	NrPrefixo bigint,
	CodItem varchar(5),
	VarianteLogistica int,
	CodigoAssociado bigint NOT NULL,
	CodigoUsuarioCriacao bigint NOT NULL,
	CodigoUsuarioAlteracao bigint,
	CodigoUsuarioExclusao bigint,
	tradeItemCountryOfOrigin int,
	CodigoIdioma int,
	CodigoStatusGTIN int NOT NULL,
	CodeSegment varchar(10),
	CodeFamily varchar(10),
	CodeClass varchar(10),
	CodeBrick varchar(10),
	countryCode varchar(3),
	CodeBrickAttribute bigint,
	IndicadorCompartilhaDados varchar(1),
	Observacoes text,
	Estado varchar(50),
	informationProvider bigint,
	brandName varchar(50),
	barCodeType int,
	DescricaoImpressao varchar(300),
	DataInclusao datetime NOT NULL,
	DataAlteracao datetime,
	DataSuspensao datetime,
	DataReativacao datetime,
	DataCancelamento datetime,
	DataReutilizacao datetime,
	modelNumber varchar(70),
	importClassificationType varchar(20),
	importClassificationValue varchar(100),
	alternateItemIdentificationId varchar(50),
	alternateItemIdentificationAgency int,
	minimumTradeItemLifespanFromTimeOfProduction int,
	startAvailabilityDateTime datetime,
	endAvailabilityDateTime datetime,
	depth decimal(5,2),
	depthMeasurementUnitCode varchar(3),
	height decimal(5,2),
	heightMeasurementUnitCode varchar(3),
	width decimal(5,2),
	widthMeasurementUnitCode varchar(3),
	netContent decimal(5,2),
	netContentMeasurementUnitCode varchar(3),
	grossWeight decimal(5,2),
	grossWeightMeasurementUnitCode varchar(3),
	netWeight decimal(5,2),
	netWeightMeasurementUnitCode varchar(3),
	CodigoTipoProduto int,
	isTradeItemABaseUnit int,
	isTradeItemAConsumerUnit int,
	isTradeItemAModel int,
	isTradeItemAnInvoiceUnit varchar(1),
	packagingTypeCode varchar(25),
	PalletTypeCode varchar(3),
	totalQuantityOfNextLowerLevelTradeItem varchar(10),
	StackingFactor int,
	quantityOfTradeItemContainedInACompleteLayer int,
	quantityOfTradeItemsPerPalletLayer int,
	quantityOfCompleteLayersContainedInATradeItem int,
	quantityOfLayersPerPallet int,
	CodigoProdutoOrigem bigint,
	deliveryToDistributionCenterTemperatureMinimum decimal(5,2),
	storageHandlingTempMinimumUOM varchar(3),
	storageHandlingTemperatureMaximum decimal(5,2),
	storageHandlingTemperatureMaximumunitOfMeasure varchar(3),
	isDangerousSubstanceIndicated int,
	ipiPerc decimal(5,2),
	isTradeItemAnOrderableUnit int,
	isTradeItemADespatchUnit int,
	orderSizingFactor varchar(3),
	orderQuantityMultiple int,
	orderQuantityMinimum int,
	barcodeCertified int,
	dataQualityCertified int,
	dataQualityCertifiedAgencyCode int,
	allGDSNAttributes int,
	CanalComunicacaoDados varchar(1),
	ProprioDonoInformacao int,
	ValidadoDonoDaInformacao int,
	DataHistorico datetime NOT NULL,
	IndicadorGDSN int
);

CREATE INDEX IDX_globalTradeItemNumberHistorico
ON ProdutoHistorico (globalTradeItemNumber ASC);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT PK_ProdutoHistorico 
	PRIMARY KEY CLUSTERED (CodigoProduto, DataHistorico);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_TipoGTIN 
	FOREIGN KEY (CodigoTipoGTIN) REFERENCES TipoGTIN (Codigo);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_ProdutoOrigem 
	FOREIGN KEY (CodigoProdutoOrigem) REFERENCES Produto (CodigoProduto);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_StatusGTIN 
	FOREIGN KEY (CodigoStatusGTIN) REFERENCES StatusGTIN (Codigo);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_UsuarioAltercao 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_UsuarioCriacao 
	FOREIGN KEY (CodigoUsuarioCriacao) REFERENCES Usuario (Codigo);

ALTER TABLE ProdutoHistorico ADD CONSTRAINT FK_ProdutoHistorico_UsuarioExclusao 
	FOREIGN KEY (CodigoUsuarioExclusao) REFERENCES Usuario (Codigo);



--------------------------------------------------------------
-----------------	   PRODUTO IMAGEM	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoImagem') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoImagem;

CREATE TABLE ProdutoImagem ( 
	Codigo bigint identity(1,1)  NOT NULL,
	URL varchar(max) NOT NULL,
	CodigoProduto bigint NOT NULL,
	Status int NOT NULL
);

ALTER TABLE ProdutoImagem ADD CONSTRAINT PK_ProdutoImagem 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ProdutoImagem ADD CONSTRAINT FK_ProdutoImagem_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);


----------------------------------------------------------------------
-----------------	   PRODUTO IMAGEM HIST�RICO	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoImagemHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoImagemHistorico;

CREATE TABLE ProdutoImagemHistorico ( 
	Codigo bigint NOT NULL,
	URL varchar(max) NOT NULL,
	CodigoProduto bigint NOT NULL,
	Status int NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE ProdutoImagemHistorico ADD CONSTRAINT PK_ProdutoImagemHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE ProdutoImagemHistorico ADD CONSTRAINT FK_ProdutoImagemHistorico_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);


------------------------------------------------------
-----------------	   TIPO URL	    ------------------
------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoURL') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoURL;

CREATE TABLE TipoURL ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE TipoURL ADD CONSTRAINT PK_TipoURL 
	PRIMARY KEY CLUSTERED (Codigo);


----------------------------------------------------------
-----------------	   PRODUTO URL	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoURL') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoURL;

CREATE TABLE ProdutoURL ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Nome varchar(255) NOT NULL,
	URL varchar(max) NOT NULL,
	Status int NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoTipoURL int NOT NULL
);

ALTER TABLE ProdutoURL ADD CONSTRAINT PK_ProdutoURL 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ProdutoURL ADD CONSTRAINT FK_ProdutoURL_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE ProdutoURL ADD CONSTRAINT FK_ProdutoURL_TipoURL 
	FOREIGN KEY (CodigoTipoURL) REFERENCES TipoURL (Codigo);


----------------------------------------------------------------------
-----------------	   PRODUTO URL HIST�RICO	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoURLHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoURLHistorico;

CREATE TABLE ProdutoURLHistorico ( 
	Codigo bigint NOT NULL,
	Nome varchar(255) NOT NULL,
	URL varchar(max) NOT NULL,
	Status int NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoTipoURL int NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE ProdutoURLHistorico ADD CONSTRAINT PK_ProdutoURLHistorico 
	PRIMARY KEY CLUSTERED (DataHistorico);

ALTER TABLE ProdutoURLHistorico ADD CONSTRAINT FK_ProdutoURLHistorico_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE ProdutoURLHistorico ADD CONSTRAINT FK_ProdutoURLHistorico_TipoURL 
	FOREIGN KEY (CodigoTipoURL) REFERENCES TipoURL (Codigo);


--------------------------------------------------------------
-----------------	   STATUS PRODUTO	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusProduto') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusProduto;

CREATE TABLE StatusProduto ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE StatusProduto ADD CONSTRAINT PK_StatusProduto 
	PRIMARY KEY CLUSTERED (Codigo);



------------------------------------------------------------------
-----------------	   STATUS TERMO ADES�O	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusTermoAdesao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusTermoAdesao;

CREATE TABLE StatusTermoAdesao ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE StatusTermoAdesao ADD CONSTRAINT PK_StatusTermoAdesao 
	PRIMARY KEY CLUSTERED (Codigo);


----------------------------------------------------------
-----------------	   TB SEGMENT	    ------------------
----------------------------------------------------------	
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbSegment') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbSegment;

CREATE TABLE tbSegment ( 
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(1500) NOT NULL,
	Definition varchar(2000)
);

ALTER TABLE tbSegment ADD CONSTRAINT PK_tbSeguimento 
	PRIMARY KEY CLUSTERED (Idioma, CodeSegment);


----------------------------------------------------------
-----------------	   TB FAMILY	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbFamily') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbFamily;

CREATE TABLE tbFamily ( 
	CodeFamily varchar(10) NOT NULL,
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(1500) NOT NULL,
	Definition varchar(2000)
);

ALTER TABLE tbFamily ADD CONSTRAINT PK_tbFamily 
	PRIMARY KEY CLUSTERED (CodeFamily, Idioma, CodeSegment);

ALTER TABLE tbFamily ADD CONSTRAINT FK_tbFamily_tbSegment 
	FOREIGN KEY (Idioma, CodeSegment) REFERENCES tbSegment (Idioma, CodeSegment)



------------------------------------------------------
-----------------	   TB CLASS	    ------------------
------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbClass') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbClass;

CREATE TABLE tbClass ( 
	CodeClass varchar(10) NOT NULL,
	CodeFamily varchar(10) NOT NULL,
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(150) NOT NULL,
	Definition varchar(1000)
);

ALTER TABLE tbClass ADD CONSTRAINT PK_tbClass 
	PRIMARY KEY CLUSTERED (CodeClass, CodeFamily, Idioma, CodeSegment);

ALTER TABLE tbClass ADD CONSTRAINT FK_tbClass_tbFamily 
	FOREIGN KEY (CodeFamily, Idioma, CodeSegment) REFERENCES tbFamily (CodeFamily, Idioma, CodeSegment)
	

------------------------------------------------------
-----------------	   TB BRICK	    ------------------
------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbBrick') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbBrick;

CREATE TABLE tbBrick ( 
	CodeBrick varchar(10) NOT NULL,
	CodeClass varchar(10) NOT NULL,
	CodeFamily varchar(10) NOT NULL,
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(1500) NOT NULL,
	Definition varchar(2000)
);

ALTER TABLE tbBrick ADD CONSTRAINT PK_tbBrick 
	PRIMARY KEY CLUSTERED (CodeClass, CodeBrick, CodeFamily, Idioma, CodeSegment);

ALTER TABLE tbBrick ADD CONSTRAINT FK_tbBrick_tbClass 
	FOREIGN KEY (CodeClass, CodeFamily, Idioma, CodeSegment) REFERENCES tbClass (CodeClass, CodeFamily, Idioma, CodeSegment)


------------------------------------------------------
-----------------	   TB TTYPE	    ------------------
------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbTType') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbTType;
CREATE TABLE tbTType (
	CodeType [varchar](10) NOT NULL,
	Idioma [varchar](5) NOT NULL,
	Text [varchar](1500) NOT NULL,
	Definition [varchar](2000) NULL,
 CONSTRAINT [PK_tbType] PRIMARY KEY CLUSTERED 
(
	[CodeType] ASC,
	[Idioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


----------------------------------------------------------
-----------------	   TB TVALUE	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbTValue') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbTValue;

CREATE TABLE tbTValue ( 
	CodeValue varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(1500) NOT NULL,
	Definition varchar(2000)
);

ALTER TABLE tbTValue ADD CONSTRAINT PK_tbValue 
	PRIMARY KEY CLUSTERED (CodeValue, Idioma);


--------------------------------------------------------------
-----------------	   TB BRICK TYPE	    ------------------
--------------------------------------------------------------	
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbBrickType') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbBrickType;

CREATE TABLE tbBrickType ( 
	CodeBrick varchar(10) NOT NULL,
	CodeClass varchar(10) NOT NULL,
	CodeFamily varchar(10) NOT NULL,
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	CodeType varchar(10) NOT NULL
);

ALTER TABLE tbBrickType ADD CONSTRAINT PK_tbBrickType 
	PRIMARY KEY CLUSTERED (CodeBrick, CodeClass, CodeFamily, Idioma, CodeSegment, CodeType);

ALTER TABLE tbBrickType ADD CONSTRAINT FK_tbBrickType_tbBrick 
	FOREIGN KEY (CodeClass, CodeBrick, CodeFamily, Idioma, CodeSegment) REFERENCES tbBrick (CodeClass, CodeBrick, CodeFamily, Idioma, CodeSegment)
	

ALTER TABLE tbBrickType ADD CONSTRAINT FK_tbBrickType_tbType 
	FOREIGN KEY (CodeType, Idioma) REFERENCES tbTType (CodeType, Idioma)
	

----------------------------------------------------------
-----------------	   TB COUNTRY	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbCountry') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbCountry;

CREATE TABLE tbCountry ( 
	CountryCode varchar(3) NOT NULL,
	CountryName varchar(100) NOT NULL
);

ALTER TABLE tbCountry ADD CONSTRAINT PK_tbCountry 
	PRIMARY KEY CLUSTERED (CountryCode);


--------------------------------------------------------------
-----------------	   TB STATE COUNTRY	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbStateCountry') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbStateCountry;

CREATE TABLE tbStateCountry ( 
	CountryCode varchar(3) NOT NULL,
	StateCode varchar(8) NOT NULL,
	StateName varchar(100) NOT NULL
);

ALTER TABLE tbStateCountry ADD CONSTRAINT PK_tbStateCountry 
	PRIMARY KEY CLUSTERED (CountryCode, StateCode);

ALTER TABLE tbStateCountry ADD CONSTRAINT FK_tbStateCountry_tbCountry 
	FOREIGN KEY (CountryCode) REFERENCES tbCountry (CountryCode)


--------------------------------------------------------------
-----------------	   TB TYPE VALUE	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbTypeValue') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbTypeValue;

CREATE TABLE tbTypeValue ( 
	CodeType varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	CodeValue varchar(10) NOT NULL,
	CodeBrick varchar(10) NOT NULL
);

ALTER TABLE tbTypeValue ADD CONSTRAINT PK_tbTypeValue 
	PRIMARY KEY CLUSTERED (CodeType, Idioma, CodeValue, CodeBrick);

ALTER TABLE tbTypeValue ADD CONSTRAINT FK_tbTypeValue_tbType 
	FOREIGN KEY (CodeType, Idioma) REFERENCES tbTType (CodeType, Idioma)
	

ALTER TABLE tbTypeValue ADD CONSTRAINT FK_tbTypeValue_tbValue 
	FOREIGN KEY (CodeValue, Idioma) REFERENCES tbTValue (CodeValue, Idioma)
	


--------------------------------------------------------------
-----------------	   TEMPLATE CARTA	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TemplateCarta') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TemplateCarta;

CREATE TABLE TemplateCarta ( 
	Codigo int identity(1,1)  NOT NULL,
	CodigoLicenca int NOT NULL,
	CaminhoTemplate varchar(max),
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE TemplateCarta
	ADD CONSTRAINT UQ_TemplateCarta_CodigoLicenca UNIQUE (CodigoLicenca);

ALTER TABLE TemplateCarta ADD CONSTRAINT PK_TemplateCarta 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TemplateCarta ADD CONSTRAINT FK_TemplateCarta_Licenca 
	FOREIGN KEY (CodigoLicenca) REFERENCES Licenca (Codigo);

ALTER TABLE TemplateCarta ADD CONSTRAINT FK_TemplateCarta_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);



----------------------------------------------------------------------
-----------------	   TEMPLATE CARTA HIST�RICO	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TemplateCartaHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TemplateCartaHistorico;

CREATE TABLE TemplateCartaHistorico ( 
	Codigo int NOT NULL,
	CodigoLicenca int NOT NULL,
	CaminhoTemplate varchar(255),
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE TemplateCartaHistorico ADD CONSTRAINT PK_TemplateCartaHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE TemplateCartaHistorico ADD CONSTRAINT FK_TemplateCartaHistorico_Licenca 
	FOREIGN KEY (CodigoLicenca) REFERENCES Licenca (Codigo);

ALTER TABLE TemplateCartaHistorico ADD CONSTRAINT FK_TemplateCartaHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);


----------------------------------------------------------------------
-----------------	   TEMPLATE CARTA USU�RIO	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TemplateCartaUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TemplateCartaUsuario;

CREATE TABLE TemplateCartaUsuario ( 
	Codigo bigint identity(1,1)  NOT NULL,
	CodigoTemplateCarta int NOT NULL,
	CaminhoTemplate varchar(255) NOT NULL,
	CodigoUsuario bigint NOT NULL,
	DataGeracao datetime NOT NULL,
	CodigoAssociado bigint NOT NULL,
	NumeroPrefixo bigint NOT NULL
);

ALTER TABLE TemplateCartaUsuario ADD CONSTRAINT PK_TemplateCartaUsuario 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TemplateCartaUsuario ADD CONSTRAINT FK_TemplateCartaUsuario_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE TemplateCartaUsuario ADD CONSTRAINT FK_TemplateCartaUsuario_TemplateCarta 
	FOREIGN KEY (CodigoTemplateCarta) REFERENCES TemplateCarta (Codigo);

ALTER TABLE TemplateCartaUsuario ADD CONSTRAINT FK_TemplateCartaUsuario_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo);


----------------------------------------------------------
-----------------	   TIPO ACEITE	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoAceite') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoAceite;

CREATE TABLE TipoAceite ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int
);

ALTER TABLE TipoAceite ADD CONSTRAINT PK_TipoAceite 
	PRIMARY KEY CLUSTERED (Codigo);


----------------------------------------------------------
-----------------	   TERMO ADES�O	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TermoAdesao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TermoAdesao;

CREATE TABLE TermoAdesao ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Versao varchar(50) NOT NULL,
	DataCriacao datetime NOT NULL,
	DataVencimento datetime,
	CodigoStatusTermoAdesao int NOT NULL,
	CodigoTipoAceite int NOT NULL,
	CodigoUsuarioCriacao bigint NOT NULL,
	CodigoUsuarioAlteracao bigint
);

ALTER TABLE TermoAdesao ADD CONSTRAINT PK_TermoAdesao 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TermoAdesao ADD CONSTRAINT FK_TermoAdesao_StatusTermoAdesao 
	FOREIGN KEY (CodigoStatusTermoAdesao) REFERENCES StatusTermoAdesao (Codigo);

ALTER TABLE TermoAdesao ADD CONSTRAINT FK_TermoAdesao_TipoAceite 
	FOREIGN KEY (CodigoTipoAceite) REFERENCES TipoAceite (Codigo);

ALTER TABLE TermoAdesao ADD CONSTRAINT FK_TermoAdesao_UsuarioAlteracao 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE TermoAdesao ADD CONSTRAINT FK_TermoAdesao_UsuarioCriacao 
	FOREIGN KEY (CodigoUsuarioCriacao) REFERENCES Usuario (Codigo);


------------------------------------------------------------------
-----------------	   TERMO ADES�O IDIOMA	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TermoAdesaoIdioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TermoAdesaoIdioma;

CREATE TABLE TermoAdesaoIdioma ( 
	CodigoTermoAdesao bigint NOT NULL,
	CodigoIdioma int NOT NULL,
	HTML text NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE TermoAdesaoIdioma ADD CONSTRAINT PK_TermoAdesaoIdioma 
	PRIMARY KEY CLUSTERED (CodigoTermoAdesao, CodigoIdioma);

ALTER TABLE TermoAdesaoIdioma ADD CONSTRAINT FK_TermoAdesaoIdioma_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);

ALTER TABLE TermoAdesaoIdioma ADD CONSTRAINT FK_TermoAdesaoIdioma_TermoAdesao 
	FOREIGN KEY (CodigoTermoAdesao) REFERENCES TermoAdesao (Codigo);

ALTER TABLE TermoAdesaoIdioma ADD CONSTRAINT FK_TermoAdesaoIdioma_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);



------------------------------------------------------------------
-----------------	   TERMO ADES�O USU�RIO	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TermoAdesaoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TermoAdesaoUsuario;

CREATE TABLE TermoAdesaoUsuario ( 
	CodigoTermoAdesao bigint NOT NULL,
	CodigoUsuario bigint NOT NULL,
	DataAceite datetime NOT NULL
);

ALTER TABLE TermoAdesaoUsuario ADD CONSTRAINT PK_TermoAdesaoUsuario 
	PRIMARY KEY CLUSTERED (CodigoTermoAdesao, CodigoUsuario);

ALTER TABLE TermoAdesaoUsuario ADD CONSTRAINT FK_TermoAdesaoUsuario_TermoAdesao 
	FOREIGN KEY (CodigoTermoAdesao) REFERENCES TermoAdesao (Codigo);

ALTER TABLE TermoAdesaoUsuario ADD CONSTRAINT FK_TermoAdesaoUsuario_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo);


----------------------------------------------------------------------
-----------------	   TIPO GTIN BARCODE TYPE	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoGTINBarcodeType') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoGTINBarcodeType;

CREATE TABLE TipoGTINBarcodeType ( 
	CodigoTipoGTIN int NOT NULL,
	CodigoBarCodeTypeList int NOT NULL
);

ALTER TABLE TipoGTINBarcodeType ADD CONSTRAINT PK_TipoGTINBarcodeType 
	PRIMARY KEY CLUSTERED (CodigoTipoGTIN, CodigoBarCodeTypeList);

ALTER TABLE TipoGTINBarcodeType ADD CONSTRAINT FK_TipoGTINBarcodeType_BarCodeTypeList 
	FOREIGN KEY (CodigoBarCodeTypeList) REFERENCES BarCodeTypeList (Codigo);

ALTER TABLE TipoGTINBarcodeType ADD CONSTRAINT FK_TipoGTINBarcodeType_TipoGTIN 
	FOREIGN KEY (CodigoTipoGTIN) REFERENCES TipoGTIN (Codigo);


------------------------------------------------------------------
-----------------	   TIPO GTIN LICEN�A	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoGTINLicenca') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoGTINLicenca;

CREATE TABLE TipoGTINLicenca ( 
	CodigoTipoGTIN int NOT NULL,
	CodigoLicenca int NOT NULL
);

ALTER TABLE TipoGTINLicenca ADD CONSTRAINT PK_TipoGTINLicenca 
	PRIMARY KEY CLUSTERED (CodigoTipoGTIN, CodigoLicenca);

ALTER TABLE TipoGTINLicenca ADD CONSTRAINT FK_TipoGTINLicenca_Licenca 
	FOREIGN KEY (CodigoLicenca) REFERENCES Licenca (Codigo);

ALTER TABLE TipoGTINLicenca ADD CONSTRAINT FK_TipoGTINLicenca_TipoGTIN 
	FOREIGN KEY (CodigoTipoGTIN) REFERENCES TipoGTIN (Codigo);


------------------------------------------------------------------------------
-----------------	   TRADE ITEM UNIT DESCRIPTOR CODES	    ------------------
------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemUnitDescriptorCodes') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemUnitDescriptorCodes;

CREATE TABLE TradeItemUnitDescriptorCodes ( 
	Codigo int NOT NULL,
	Nome varchar(6) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL
);

ALTER TABLE TradeItemUnitDescriptorCodes ADD CONSTRAINT PK_TradeItemUnitDescriptorCodes 
	PRIMARY KEY CLUSTERED (Codigo);


----------------------------------------------------------
-----------------	   TIPO PRODUTO	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoProduto') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoProduto;

CREATE TABLE TipoProduto ( 
	Codigo int identity(1,1)  NOT NULL,
	Nome varchar(50),
	Status int NOT NULL,
	NomeImagem varchar(255),
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	Descricao varchar(255),
	CodigoTradeItemUnitDescriptorCodes int NOT NULL
);

ALTER TABLE TipoProduto ADD CONSTRAINT PK_TipoProduto_1 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TipoProduto ADD CONSTRAINT FK_TipoProduto_TradeItemUnitDescriptorCodes 
	FOREIGN KEY (CodigoTradeItemUnitDescriptorCodes) REFERENCES TradeItemUnitDescriptorCodes (Codigo)
	

ALTER TABLE TipoProduto ADD CONSTRAINT FK_TipoProduto_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	

----------------------------------------------------------------------
-----------------	   TIPO PRODUTO HIST�RICO	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoProdutoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoProdutoHistorico;

CREATE TABLE TipoProdutoHistorico ( 
	Codigo int NOT NULL,
	Nome varchar(50),
	Status int NOT NULL,
	NomeImagem varchar(max),
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	Descricao varchar(255),
	DataHistorico datetime NOT NULL,
	CodigoTradeItemUnitDescriptorCodes int NOT NULL
);

ALTER TABLE TipoProdutoHistorico ADD CONSTRAINT PK_TipoProdutoHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE TipoProdutoHistorico ADD CONSTRAINT FK_TipoProdutoHistorico_TradeItemUnitDescriptorCodes 
	FOREIGN KEY (CodigoTradeItemUnitDescriptorCodes) REFERENCES TradeItemUnitDescriptorCodes (Codigo);

ALTER TABLE TipoProdutoHistorico ADD CONSTRAINT FK_TipoProdutoHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);


--------------------------------------------------------------------------
-----------------	   TIPO USU�RIO STATUS USU�RIO	    ------------------
--------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoUsuarioStatusUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoUsuarioStatusUsuario;

CREATE TABLE TipoUsuarioStatusUsuario ( 
	Codigo bigint identity(1,1)  NOT NULL,
	CodigoStatusUsuario int NOT NULL,
	CodigoTipoUsuario int NOT NULL
);

ALTER TABLE TipoUsuarioStatusUsuario ADD CONSTRAINT PK_TipoUsuarioStatusUsuario 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TipoUsuarioStatusUsuario ADD CONSTRAINT FK_TipoUsuarioStatusUsuario_StatusUsuario 
	FOREIGN KEY (CodigoStatusUsuario) REFERENCES StatusUsuario (Codigo)
	
ALTER TABLE TipoUsuarioStatusUsuario ADD CONSTRAINT FK_TipoUsuarioStatusUsuario_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo)


----------------------------------------------------------
-----------------	   UNECEREC20	    ------------------
----------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('UNECEREC20') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE UNECEREC20;

CREATE TABLE UNECEREC20 ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	CommonCode varchar(3),
	Sigla varchar(50) NOT NULL,
	FatorConversao decimal(25,12) NOT NULL,
	Tipo int NOT NULL,
	Status int NOT NULL,
	IndicadorPrincipal varchar(1)
);

ALTER TABLE UNECEREC20 ADD CONSTRAINT PK_UNECEREC20 
	PRIMARY KEY CLUSTERED (Codigo);


------------------------------------------------------------------
-----------------	   USU�RIO HIST�RICO	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('UsuarioHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE UsuarioHistorico;

CREATE TABLE UsuarioHistorico ( 
	Codigo bigint NOT NULL,
	Nome varchar(255) NOT NULL,
	CodigoStatusUsuario int NOT NULL,
	CodigoPerfil int NOT NULL,
	CodigoTipoUsuario int NOT NULL,
	Email varchar(255) NOT NULL,
	TelefoneResidencial varchar(50),
	Senha varchar(255),
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	MensagemObservacao varchar(500),
	DataAlteracaoSenha datetime,
	QuantidadeTentativa int DEFAULT ((0)) NOT NULL,
	DataUltimoAcesso datetime,
	DataCadastro datetime DEFAULT (getdate()) NOT NULL,
	TelefoneCelular varchar(50),
	TelefoneComercial varchar(50),
	Ramal varchar(50),
	Departamento varchar(255),
	Cargo varchar(255),
	CPFCNPJ varchar(14),
	DataHistorico datetime NOT NULL
);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT PK_UsuarioHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT FK_UsuarioHistorico_Perfil 
	FOREIGN KEY (CodigoPerfil) REFERENCES Perfil (Codigo);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT FK_UsuarioHistorico_StatusUsuario 
	FOREIGN KEY (CodigoStatusUsuario) REFERENCES StatusUsuario (Codigo);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT FK_UsuarioHistorico_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT FK_UsuarioHistorico_UsuarioAlteracao 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);


----------------------------------------------------------------------
-----------------	   USU�RIO HIST�RICO SENHA	    ------------------
----------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('UsuarioHistoricoSenha') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE UsuarioHistoricoSenha;

CREATE TABLE UsuarioHistoricoSenha ( 
	CodigoUsuario bigint NOT NULL,
	Senha varchar(255) NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE UsuarioHistoricoSenha ADD CONSTRAINT PK_UsuarioHistoricoSenha 
	PRIMARY KEY CLUSTERED (CodigoUsuario, DataHistorico);

ALTER TABLE UsuarioHistoricoSenha ADD CONSTRAINT FK_UsuarioHistoricoSenha_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo)
	

------------------------------------------------------------------
-----------------	   AG�NCIA REGULADORA	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AgenciaReguladora') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AgenciaReguladora;

CREATE TABLE [dbo].[AgenciaReguladora](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL
);

ALTER TABLE AgenciaReguladora ADD CONSTRAINT PK_AgenciaReguladora 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE AgenciaReguladora ADD CONSTRAINT FK_AgenciaReguladora_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)


--------------------------------------------------------------------------
-----------------	   AG�NCIA REGULADORA HIST�RICO	    ------------------
--------------------------------------------------------------------------	
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AgenciaReguladoraHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AgenciaReguladoraHistorico;

CREATE TABLE AgenciaReguladoraHistorico ( 
	Codigo bigint NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE AgenciaReguladoraHistorico ADD CONSTRAINT PK_AgenciaReguladoraHistorico 
	PRIMARY KEY CLUSTERED (DataHistorico);

ALTER TABLE AgenciaReguladoraHistorico ADD CONSTRAINT FK_AgenciaReguladoraHistorico_AgenciaReguladora 
	FOREIGN KEY (Codigo) REFERENCES AgenciaReguladora (Codigo);

ALTER TABLE AgenciaReguladoraHistorico ADD CONSTRAINT FK_AgenciaReguladoraHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);


--------------------------------------------------------------
-----------------	   ESPA�O BANNER	    ------------------
--------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('EspacoBanner') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE EspacoBanner;

CREATE TABLE EspacoBanner(
	Codigo [int] NOT NULL,
	Nome [varchar](50) NOT NULL,
	Descricao [varchar](255) NULL,
	Status [bigint] NOT NULL,
	DataAlteracao [datetime] NOT NULL,
	CodigoUsuarioAlteracao [bigint] NOT NULL,
	Altura [varchar](50) NOT NULL,
	Largura [varchar](50) NOT NULL,
	UrlPublicacao [varchar](255) NOT NULL,
	QuantidadeMaxima [int] NOT NULL,
	CodigoTipoEspacoBanner [int] NOT NULL,
 CONSTRAINT [PK_EspacoBanner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[EspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_EspacoBanner_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
ALTER TABLE [dbo].[EspacoBanner] CHECK CONSTRAINT [FK_EspacoBanner_Usuario]


------------------------------------------------------
-----------------	   BANNER	    ------------------
------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Banner') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Banner;

CREATE TABLE Banner(
	Codigo [bigint] IDENTITY(1,1) NOT NULL,
	Nome [varchar](100) NOT NULL,
	Descricao [varchar](255) NULL,
	Status [int] NOT NULL,
	NomeImagem [varchar](255) NOT NULL,
	DataInicioPublicacao [datetime] NOT NULL,
	DataFimPublicacao [datetime] NOT NULL,
	DataAlteracao [datetime] NOT NULL,
	CodigoUsuarioAlteracao [bigint] NOT NULL,
	Link [varchar](255) NULL,
	CorFundo [varchar](50) NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[Banner]  WITH CHECK ADD  CONSTRAINT [FK_Banner_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
ALTER TABLE [dbo].[Banner] CHECK CONSTRAINT [FK_Banner_Usuario]


------------------------------------------------------------------
-----------------	   BANNER ESPA�O BANNER	    ------------------
------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('BannerEspacoBanner') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE BannerEspacoBanner;

CREATE TABLE BannerEspacoBanner(
	Codigo [bigint] IDENTITY(1,1) NOT NULL,
	CodigoEspacoBanner [int] NULL,
	CodigoBanner [bigint] NULL,
	Ordem [int] NULL,
 CONSTRAINT [PK_BannerEspacoBanner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[BannerEspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_BannerEspacoBanner_Banner] FOREIGN KEY([CodigoBanner])
REFERENCES [dbo].[Banner] ([Codigo])
ALTER TABLE [dbo].[BannerEspacoBanner] CHECK CONSTRAINT [FK_BannerEspacoBanner_Banner]

ALTER TABLE [dbo].[BannerEspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_BannerEspacoBanner_EspacoBanner] FOREIGN KEY([CodigoEspacoBanner])
REFERENCES [dbo].[EspacoBanner] ([Codigo])
ALTER TABLE [dbo].[BannerEspacoBanner] CHECK CONSTRAINT [FK_BannerEspacoBanner_EspacoBanner]



------------------------------------------------------------------
-----------------	   PROC EXPIRA LICENCAS	    ------------------
------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_EXPIRALICENCAS') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_EXPIRALICENCAS END

public class PROC_EXPIRALICENCAS {

	public PROC_EXPIRALICENCAS(){

	}

	public void finalize() throws Throwable {

	}
}


----------------------------------------------------------------------
-----------------	   PROC IMPORTAR ATRIBUTO	    ------------------
----------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_IMPORTAR_ATRIBUTO') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_IMPORTAR_ATRIBUTO END

CREATE PROCEDURE PROC_IMPORTAR_ATRIBUTO
(
	@BRICK_ID INT,
	@BRICK_CODE VARCHAR(10),
	@CLASS_CODE VARCHAR(10),
	@FAMILY_CODE VARCHAR(10),
	@SEGMENT_CODE VARCHAR(10)
)
AS
BEGIN
	IF (EXISTS(SELECT TOP 1 ATTTYPE_ID FROM #ATTTYPE WHERE [BRICK_ID] = @BRICK_ID))
	BEGIN
		DECLARE @ATTTYPE_ID INT,
				@ATTTYPE_CODE VARCHAR(10),
				@ATTTYPE_TEXT VARCHAR(1500),
				@ATTTYPE_DEFINITION VARCHAR(2000);

		DECLARE CUR_ATRIBUTO CURSOR FOR
		SELECT ATTTYPE_ID, CODE, [TEXT], [DEFINITION]
		FROM #ATTTYPE
		WHERE [BRICK_ID] = @BRICK_ID;

		OPEN CUR_ATRIBUTO;

		FETCH NEXT FROM CUR_ATRIBUTO
		INTO @ATTTYPE_ID, @ATTTYPE_CODE, @ATTTYPE_TEXT, @ATTTYPE_DEFINITION;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (NOT EXISTS(SELECT CODETYPE
						   FROM TBTTYPE
						   WHERE CODETYPE = @ATTTYPE_CODE
						   AND IDIOMA = 'pt-br'))
			BEGIN
				INSERT INTO TBTTYPE VALUES (@ATTTYPE_CODE, 'pt-br', @ATTTYPE_TEXT, @ATTTYPE_DEFINITION);
			END
			ELSE
				UPDATE TBTTYPE SET [TEXT] = @ATTTYPE_TEXT, [DEFINITION] = @ATTTYPE_DEFINITION WHERE CODETYPE = @ATTTYPE_CODE AND IDIOMA = 'pt-br';

			IF (NOT EXISTS(SELECT CODETYPE
						   FROM TBBRICKTYPE
						   WHERE CODETYPE = @ATTTYPE_CODE
						   AND CODEBRICK = @BRICK_CODE
						   AND CODECLASS = @CLASS_CODE
						   AND CODEFAMILY = @FAMILY_CODE
						   AND CODESEGMENT = @SEGMENT_CODE
						   AND IDIOMA = 'pt-br'))
			BEGIN
				INSERT INTO TBBRICKTYPE VALUES (@BRICK_CODE, @CLASS_CODE, @FAMILY_CODE, @SEGMENT_CODE, 'pt-br', @ATTTYPE_CODE);
			END;

			-- VALOR
			EXEC PROC_IMPORTAR_VALOR @ATTTYPE_ID, @ATTTYPE_CODE, @BRICK_CODE;

			FETCH NEXT FROM CUR_ATRIBUTO
			INTO @ATTTYPE_ID, @ATTTYPE_CODE, @ATTTYPE_TEXT, @ATTTYPE_DEFINITION;
		END;

		CLOSE CUR_ATRIBUTO;
		DEALLOCATE CUR_ATRIBUTO;
	END;
END;
GO


------------------------------------------------------------------
-----------------	   PROC IMPORTAR BLOCO	    ------------------
------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_IMPORTAR_BLOCO') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_IMPORTAR_BLOCO END

CREATE PROCEDURE PROC_IMPORTAR_BLOCO
(
	@CLASS_ID INT,
	@CLASS_CODE VARCHAR(10),
	@FAMILY_CODE VARCHAR(10),
	@SEGMENT_CODE VARCHAR(10)
)
AS
BEGIN
	IF (EXISTS(SELECT TOP 1 [BRICK_ID] FROM #BRICK WHERE CLASS_ID = @CLASS_ID))
	BEGIN
		DECLARE @BRICK_ID INT,
				@BRICK_CODE VARCHAR(10),
				@BRICK_TEXT VARCHAR(1500),
				@BRICK_DEFINITION VARCHAR(2000);

		DECLARE CUR_BLOCO CURSOR FOR
		SELECT [BRICK_ID], CODE, [TEXT], [DEFINITION]
		FROM #BRICK
		WHERE CLASS_ID = @CLASS_ID;

		OPEN CUR_BLOCO;

		FETCH NEXT FROM CUR_BLOCO
		INTO @BRICK_ID, @BRICK_CODE, @BRICK_TEXT, @BRICK_DEFINITION;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (NOT EXISTS(SELECT CODEBRICK
						   FROM TBBRICK
						   WHERE CODEBRICK = @BRICK_CODE
						   AND CODECLASS = @CLASS_CODE
						   AND CODEFAMILY = @FAMILY_CODE
						   AND CODESEGMENT = @SEGMENT_CODE
						   AND IDIOMA = 'pt-br'))
			BEGIN
				INSERT INTO TBBRICK VALUES (@BRICK_CODE, @CLASS_CODE, @FAMILY_CODE, @SEGMENT_CODE, 'pt-br', @BRICK_TEXT, @BRICK_DEFINITION);
			END
			ELSE
				UPDATE TBBRICK SET [TEXT] = @BRICK_TEXT, [DEFINITION] = @BRICK_DEFINITION WHERE CODEBRICK = @BRICK_CODE AND CODECLASS = @CLASS_CODE AND CODEFAMILY = @FAMILY_CODE AND CODESEGMENT = @SEGMENT_CODE AND IDIOMA = 'pt-br';

			-- ATRIBUTO
			EXEC PROC_IMPORTAR_ATRIBUTO @BRICK_ID, @BRICK_CODE, @CLASS_CODE, @FAMILY_CODE, @SEGMENT_CODE;
							
			FETCH NEXT FROM CUR_BLOCO
			INTO @BRICK_ID, @BRICK_CODE, @BRICK_TEXT, @BRICK_DEFINITION;
		END;

		CLOSE CUR_BLOCO;
		DEALLOCATE CUR_BLOCO;
	END;
END;
GO


------------------------------------------------------------------
-----------------	   PROC IMPORTAR CLASSE	    ------------------
------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_IMPORTAR_CLASSE') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_IMPORTAR_CLASSE END

CREATE PROCEDURE PROC_IMPORTAR_CLASSE
(
	@FAMILY_ID INT,
	@FAMILY_CODE VARCHAR(10),
	@SEGMENT_CODE VARCHAR(10)
)
AS
BEGIN
	IF (EXISTS(SELECT COUNT(CLASS_ID) FROM #CLASS WHERE FAMILY_ID = @FAMILY_ID))
	BEGIN
		DECLARE @CLASS_ID INT,
				@CLASS_CODE VARCHAR(10),
				@CLASS_TEXT VARCHAR(1500),
				@CLASS_DEFINITION VARCHAR(2000);

		DECLARE CUR_CLASSE CURSOR FOR
		SELECT CLASS_ID, CODE, [TEXT], [DEFINITION]
		FROM #CLASS
		WHERE FAMILY_ID = @FAMILY_ID;

		OPEN CUR_CLASSE;

		FETCH NEXT FROM CUR_CLASSE
		INTO @CLASS_ID, @CLASS_CODE, @CLASS_TEXT, @CLASS_DEFINITION;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (NOT EXISTS(SELECT CODECLASS
						   FROM TBCLASS
						   WHERE CODECLASS = @CLASS_CODE
						   AND CODEFAMILY = @FAMILY_CODE
						   AND CODESEGMENT = @SEGMENT_CODE
						   AND IDIOMA = 'pt-br'))
			BEGIN
				INSERT INTO TBCLASS VALUES (@CLASS_CODE, @FAMILY_CODE, @SEGMENT_CODE, 'pt-br', @CLASS_TEXT, @CLASS_DEFINITION);
			END
			ELSE
				UPDATE TBCLASS SET [TEXT] = @CLASS_TEXT, [DEFINITION] = @CLASS_DEFINITION WHERE CODECLASS = @CLASS_CODE AND CODEFAMILY = @FAMILY_CODE AND CODESEGMENT = @SEGMENT_CODE AND IDIOMA = 'pt-br';

			--BLOCO
			EXEC PROC_IMPORTAR_BLOCO @CLASS_ID, @CLASS_CODE, @FAMILY_CODE, @SEGMENT_CODE;

			FETCH NEXT FROM CUR_CLASSE
			INTO @CLASS_ID, @CLASS_CODE, @CLASS_TEXT, @CLASS_DEFINITION;
		END;

		CLOSE CUR_CLASSE;
		DEALLOCATE CUR_CLASSE;
	END;
END;
GO


----------------------------------------------------------------------
-----------------	   PROC IMPORTAR FAM�LIA	    ------------------
----------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_IMPORTAR_FAMILIA') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_IMPORTAR_FAMILIA END

CREATE PROCEDURE PROC_IMPORTAR_FAMILIA
(
	@SEGMENT_ID INT,
	@SEGMENT_CODE VARCHAR(10)
)
AS
BEGIN
	IF (EXISTS(SELECT TOP 1 FAMILY_ID FROM #FAMILY WHERE SEGMENT_ID = @SEGMENT_ID))
	BEGIN
		DECLARE @FAMILY_ID INT,
				@FAMILY_CODE VARCHAR(10),
				@FAMILY_TEXT VARCHAR(1500),
				@FAMILY_DEFINITION VARCHAR(2000);

		DECLARE CUR_FAMILIA CURSOR FOR
		SELECT FAMILY_ID, CODE, [TEXT], [DEFINITION]
		FROM #FAMILY
		WHERE SEGMENT_ID = @SEGMENT_ID;

		OPEN CUR_FAMILIA;

		FETCH NEXT FROM CUR_FAMILIA
		INTO @FAMILY_ID, @FAMILY_CODE, @FAMILY_TEXT, @FAMILY_DEFINITION;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF (NOT EXISTS(SELECT CODEFAMILY
						   FROM TBFAMILY
						   WHERE CODEFAMILY = @FAMILY_CODE
						   AND CODESEGMENT = @SEGMENT_CODE
						   AND IDIOMA = 'pt-br'))
			BEGIN
				INSERT INTO TBFAMILY VALUES (@FAMILY_CODE, @SEGMENT_CODE, 'pt-br', @FAMILY_TEXT, @FAMILY_DEFINITION);
			END
			ELSE
				UPDATE TBFAMILY SET [TEXT] = @FAMILY_TEXT, [DEFINITION] = @FAMILY_DEFINITION WHERE CODEFAMILY = @FAMILY_CODE AND CODESEGMENT = @SEGMENT_CODE AND IDIOMA = 'pt-br';

			-- CLASSE
			EXEC PROC_IMPORTAR_CLASSE @FAMILY_ID, @FAMILY_CODE, @SEGMENT_CODE;

			FETCH NEXT FROM CUR_FAMILIA
			INTO @FAMILY_ID, @FAMILY_CODE, @FAMILY_TEXT, @FAMILY_DEFINITION;
		END;

		CLOSE CUR_FAMILIA;
		DEALLOCATE CUR_FAMILIA;
	END;
END;
GO


----------------------------------------------------------------------
-----------------	   PROC IMPORTAR SEGMENTO	    ------------------
----------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_IMPORTAR_SEGMENTO') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_IMPORTAR_SEGMENTO END

CREATE PROCEDURE PROC_IMPORTAR_SEGMENTO
AS
BEGIN
	DECLARE @SEGMENT_ID INT,
			@SEGMENT_CODE VARCHAR(10),
			@SEGMENT_TEXT VARCHAR(1500),
			@SEGMENT_DEFINITION VARCHAR(2000);

	DECLARE CUR_SEGMENTO CURSOR FOR
	SELECT SEGMENT_ID, CODE, [TEXT], [DEFINITION]
	FROM #SEGMENT;

	OPEN CUR_SEGMENTO;

	FETCH NEXT FROM CUR_SEGMENTO
	INTO @SEGMENT_ID, @SEGMENT_CODE, @SEGMENT_TEXT, @SEGMENT_DEFINITION;

	-- SEGMENTO
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (NOT EXISTS(SELECT CODESEGMENT
					   FROM TBSEGMENT
					   WHERE CODESEGMENT = @SEGMENT_CODE
					   AND IDIOMA = 'pt-br'))
		BEGIN
			INSERT INTO TBSEGMENT VALUES (@SEGMENT_CODE, 'pt-br', @SEGMENT_TEXT, @SEGMENT_DEFINITION);
		END
		ELSE
			UPDATE TBSEGMENT SET [TEXT] = @SEGMENT_TEXT, [DEFINITION] = @SEGMENT_DEFINITION WHERE CODESEGMENT = @SEGMENT_CODE AND IDIOMA = 'pt-br';

		-- FAMILIA
		EXEC PROC_IMPORTAR_FAMILIA @SEGMENT_ID, @SEGMENT_CODE;

		FETCH NEXT FROM CUR_SEGMENTO
		INTO @SEGMENT_ID, @SEGMENT_CODE, @SEGMENT_TEXT, @SEGMENT_DEFINITION;
	END;

	CLOSE CUR_SEGMENTO;
	DEALLOCATE CUR_SEGMENTO;
END;
GO


------------------------------------------------------------------
-----------------	   PROC IMPORTAR VALOR	    ------------------
------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_IMPORTAR_VALOR') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_IMPORTAR_VALOR END

CREATE PROCEDURE PROC_IMPORTAR_VALOR
(
	@TYPE_ID INT,
	@TYPE_CODE VARCHAR(10),
	@BRICK_CODE VARCHAR(10)
)
AS
BEGIN
	DECLARE @MSG VARCHAR(MAX)
	BEGIN TRY
		IF (EXISTS(SELECT TOP 1 CODE FROM #ATTVALUE WHERE ATTTYPE_ID = @TYPE_ID))
		BEGIN
			DECLARE  @ATTVALUE_CODE VARCHAR(10),
					 @ATTVALUE_TEXT VARCHAR(1500),
					 @ATTVALUE_DEFINITION VARCHAR(2000);

			DECLARE CUR_VALOR CURSOR FOR
			SELECT CODE, [TEXT], [DEFINITION]
			FROM #ATTVALUE
			WHERE ATTTYPE_ID = @TYPE_ID;

			OPEN CUR_VALOR;

			FETCH NEXT FROM CUR_VALOR
			INTO  @ATTVALUE_CODE, @ATTVALUE_TEXT, @ATTVALUE_DEFINITION;

			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF (NOT EXISTS(SELECT CODEVALUE
							   FROM TBTVALUE
							   WHERE CODEVALUE = @ATTVALUE_CODE
							   AND IDIOMA = 'pt-br'))
				BEGIN
					INSERT INTO TBTVALUE
					VALUES (@ATTVALUE_CODE, 'pt-br', @ATTVALUE_TEXT, @ATTVALUE_DEFINITION);
				END
				ELSE
				BEGIN
					UPDATE TBTVALUE
					SET [TEXT] = @ATTVALUE_TEXT,
						[DEFINITION] = @ATTVALUE_DEFINITION
					WHERE CODEVALUE = @ATTVALUE_CODE
					AND IDIOMA = 'pt-br';
				END

				IF (NOT EXISTS(SELECT CODEVALUE
							   FROM TBTYPEVALUE
							   WHERE CODETYPE = @TYPE_CODE
							   AND CODEVALUE = @ATTVALUE_CODE
							   AND IDIOMA = 'pt-br'
							   AND CODEBRICK = @BRICK_CODE))
				BEGIN
					INSERT INTO TBTYPEVALUE (CODETYPE, IDIOMA, CODEVALUE, CODEBRICK)
					VALUES (@TYPE_CODE, 'pt-br', @ATTVALUE_CODE, @BRICK_CODE);
				END;

				FETCH NEXT FROM CUR_VALOR
				INTO @ATTVALUE_CODE, @ATTVALUE_TEXT, @ATTVALUE_DEFINITION;
			END;

			CLOSE CUR_VALOR;
			DEALLOCATE CUR_VALOR;
		END;
	END TRY
	BEGIN CATCH
		SET @MSG = 'Linha: ' + CAST(ERROR_LINE() AS VARCHAR) + '; Procedimento: ' + ERROR_PROCEDURE() + '; Mensagem: ' + ERROR_MESSAGE()

		INSERT INTO LOGERRO (MENSAGEM, DESCRICAO) VALUES (@MSG, @MSG);
	END CATCH
END;
GO


--------------------------------------------------------------------------
-----------------	   PROC JOB SINCRONIZA��O CRM	    ------------------
--------------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_JOB_SINCRONIZACAO_CRM') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_JOB_SINCRONIZACAO_CRM END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[PROC_JOB_SINCRONIZACAO_CRM](
	@XMLASSOCIADOS XML
	, @XMLLICENCAS XML
)
AS BEGIN

	 DECLARE @TEMPTBLLICENCA TABLE (prefixo varchar(max),data_aniversario varchar(max),flagprincipal int,licenca varchar(max),negocio varchar(max),qtde_digitos int,stcadastral int,stfinanceiro int
								,datamodificacao  varchar(max), associado varchar(max), status_da_licenca varchar(max), cad varchar(max),id varchar(max), descricaogtin8 varchar(max))

	 ;WITH XMLNAMESPACES(default 'http://schemas.datacontract.org/2004/07/GS1.CNP.BLL.Backoffice')	 
		INSERT INTO @TEMPTBLLICENCA
		 SELECT       
			 Tbl.Col.value('(prefixo)[1]', 'varchar(max)') as prefixo
			,Tbl.Col.value('(data_aniversario)[1]', 'varchar(max)') as data_aniversario
			,Tbl.Col.value('(flagprincipal)[1]', 'int') as flagprincipal
			,Tbl.Col.value('(licenca)[1]', 'varchar(max)') as licenca
			,Tbl.Col.value('(negocio)[1]', 'varchar(max)') as negocio
			,Tbl.Col.value('(qtde_digitos)[1]', 'int') as qtde_digitos
			,Tbl.Col.value('(stcadastral)[1]', 'int') as stcadastral
			,Tbl.Col.value('(stfinanceiro)[1]', 'int') as stfinanceiro
			,Tbl.Col.value('(datamodificacao)[1]', 'varchar(max)') as data_modificacao
			,Tbl.Col.value('(associado)[1]', 'varchar(max)') as associado
			,Tbl.Col.value('(status_da_licenca)[1]', 'varchar(max)') as status_da_licenca
			,Tbl.Col.value('(CAD)[1]', 'varchar(max)') as cad
			,Tbl.Col.value('(id)[1]', 'varchar(max)') as id
			,Tbl.Col.value('(descricaogtin8)[1]', 'varchar(max)') as descricaogtin8
		
		 FROM   @XMLLICENCAS.nodes('//ArrayOfIntegracaoCrmBO.PrefixoLicencaAssociadoCRM//IntegracaoCrmBO.PrefixoLicencaAssociadoCRM') Tbl(Col)
		

	DECLARE @TEMPTBLDADOASSOCIADO TABLE (id varchar(max),nome varchar(max),nomefantasia varchar(max),email varchar(max),cpfcnpj varchar(max),cad varchar(max),relacionamento int,bl_adimplente bit,bl_premium bit,bl_acessoTrade bit,senha varchar(max),
										inscricaoestadual varchar(max),estadoname  varchar(max),numero varchar(max),address1_city varchar(max),logradouroprincipal varchar(max),complementoprincipal varchar(max),address1_postalcode varchar(max),
										bairroprincipal varchar(max),representante_smart_pessoaname varchar(max),representante_smart_cpf varchar(max),representante_telephone1 varchar(max),representante_telephone2 varchar(max),
										representante_telephone3 varchar(max),representante_emailaddress1 varchar(max),representante_emailaddress2 varchar(max),glninformativo bigint,pais varchar(max),telephone1 varchar(max),
										smart_ddidddtelefoneprincipal varchar(max), gs1br_cnp20 bit)



	;WITH XMLNAMESPACES(default 'http://schemas.datacontract.org/2004/07/GS1.CNP.BLL')	 
		INSERT INTO @TEMPTBLDADOASSOCIADO
		SELECT       		
			Tbl.Col.value('(id)[1]', 'varchar(max)') as id
			,Tbl.Col.value('(nome)[1]', 'varchar(max)') as nome
			,Tbl.Col.value('(nomefantasia)[1]', 'varchar(max)') as nomefantasia
			,Tbl.Col.value('(email)[1]', 'varchar(max)') as email
			,Tbl.Col.value('(cpfcnpj)[1]', 'varchar(max)') as cpfcnpj
			,Tbl.Col.value('(cad)[1]', 'varchar(max)') as cad
			,Tbl.Col.value('(relacionamento)[1]', 'int') as relacionamento
			,Tbl.Col.value('(bl_adimplente)[1]', 'bit') as bl_adimplente
			,Tbl.Col.value('(bl_premium)[1]', 'bit ') as bl_premium
			,Tbl.Col.value('(bl_acessoTrade)[1]', 'bit ') as bl_acessoTrade
			,Tbl.Col.value('(senha)[1]', 'varchar(max)') as senha
			,Tbl.Col.value('(inscricaoestadual)[1]', 'varchar(max)') as inscricaoestadual
			,Tbl.Col.value('(estadoname )[1]', 'varchar(max)') as estadoname
			,Tbl.Col.value('(numero)[1]', 'varchar(max)') as numero
			,Tbl.Col.value('(address1_city)[1]', 'varchar(max)') as address1_city
			,Tbl.Col.value('(logradouroprincipal)[1]', 'varchar(max)') as logradouroprincipal
			,Tbl.Col.value('(complementoprincipal)[1]', 'varchar(max)') as complementoprincipal
			,Tbl.Col.value('(address1_postalcode)[1]', 'varchar(max)') as address1_postalcode
			,Tbl.Col.value('(bairroprincipal)[1]', 'varchar(max)') as bairroprincipal
			,Tbl.Col.value('(representante_smart_pessoaname)[1]', 'varchar(max)') as representante_smart_pessoaname
			,Tbl.Col.value('(representante_smart_cpf)[1]', 'varchar(max)') as representante_smart_cpf
			,Tbl.Col.value('(representante_telephone1)[1]', 'varchar(max)') as representante_telephone1
			,Tbl.Col.value('(representante_telephone2)[1]', 'varchar(max)') as representante_telephone2
			,Tbl.Col.value('(representante_telephone3)[1]', 'varchar(max)') as representante_telephone3
			,Tbl.Col.value('(representante_emailaddress1)[1]', 'varchar(max)') as representante_emailaddress1
			,Tbl.Col.value('(representante_emailaddress2)[1]', 'varchar(max)') as representante_emailaddress2
			,Tbl.Col.value('(glninformativo)[1]', 'bigint') as glninformativo
			,Tbl.Col.value('(pais)[1]', 'varchar(max)') as pais
			,Tbl.Col.value('(telephone1)[1]', 'varchar(max)') as telephone1
			,Tbl.Col.value('(smart_ddidddtelefoneprincipal)[1]', 'varchar(max)') as smart_ddidddtelefoneprincipal
			,Tbl.Col.value('(gs1br_cnp20)[1]', 'bit') as gs1br_cnp20
		
		 FROM   @XMLASSOCIADOS.nodes('ArrayOfUsuarioCRM/UsuarioCRM') Tbl(Col)
 
		BEGIN TRANSACTION
		
			--GRAVA O HISTORICO DO ASSOCIADO
			INSERT INTO [dbo].[AssociadoHistorico] ([CodigoAssociado],[Nome],[Descricao],[Email],[CPFCNPJ],[InscricaoEstadual],[CodigoTipoAssociado],[MensagemObservacao],[DataAlteracao],
																							[DataAtualizacaoCRM],[DataStatusInadimplente],[CodigoUsuarioAlteracao],[Endereco],[Numero],[Complemento],[Bairro],[Cidade],[UF],[CEP],
																							[CodigoTipoCompartilhamento],[DataHistorico],[CodigoStatusAssociado],[CodigoSituacaoFinanceira],[CAD])

			SELECT A.[Codigo],A.[Nome],A.[Descricao],A.[Email],A.[CPFCNPJ],A.[InscricaoEstadual],A.[CodigoTipoAssociado],A.[MensagemObservacao],A.[DataAlteracao],
												A.[DataAtualizacaoCRM],A.[DataStatusInadimplente],A.[CodigoUsuarioAlteracao],A.[Endereco],A.[Numero],A.[Complemento],A.[Bairro],A.[Cidade],A.[UF],A.[CEP],
												A.[CodigoTipoCompartilhamento],GETDATE(),A.[CodigoStatusAssociado],A.[CodigoSituacaoFinanceira],A.[CAD]
			FROM ASSOCIADO A
			--WHERE ASSOCIADO.CODIGOSTATUSASSOCIADO = 1
			--INNER JOIN @TEMPTBLDADOASSOCIADO B ON B.CAD = A.CAD
			WHERE A.CAD IN (SELECT DISTINCT CAD FROM @TEMPTBLDADOASSOCIADO) 


			--ATUALIZA OS ASSOCIADOS ALTERADOS
			UPDATE [dbo].[Associado]
			SET  ASSOCIADO.[Nome] = B.NOME
				,ASSOCIADO.[Descricao] = B.NOMEFANTASIA
				,ASSOCIADO.[Email] = B.EMAIL
				,ASSOCIADO.[CPFCNPJ] = REPLACE(REPLACE(REPLACE(B.CPFCNPJ,'.',''),'/',''),'-','')
				,ASSOCIADO.[InscricaoEstadual] = B.InscricaoEstadual				
				,ASSOCIADO.[CodigoTipoAssociado] = CASE B.bl_premium WHEN 1 THEN 1 ELSE 2 END								
				,ASSOCIADO.[DataAtualizacaoCRM] = GETDATE()				
				,ASSOCIADO.[DataStatusInadimplente] = CASE B.bl_adimplente WHEN 1 THEN NULL ELSE GETDATE() END				
				,ASSOCIADO.[CodigoUsuarioAlteracao] = 1
				,ASSOCIADO.[Endereco] = B.logradouroprincipal
				,ASSOCIADO.[Numero] = B.Numero
				,ASSOCIADO.[Complemento] = B.Complementoprincipal
				,ASSOCIADO.[Bairro] = B.Bairroprincipal
				,ASSOCIADO.[Cidade] = B.address1_city
				,ASSOCIADO.[UF] = B.estadoname
				,ASSOCIADO.[CEP] = B.address1_postalcode				
				,ASSOCIADO.[CodigoStatusAssociado] = CASE B.gs1br_cnp20 
														WHEN 1 THEN 
															CASE B.bl_adimplente WHEN 1 THEN
																CASE B.relacionamento 
																	WHEN 2 THEN 1 
																	ELSE 6 --N�o Associado
																END
															 ELSE 5 END --Inadimplente
														ELSE
															4 -- Sem acesso CNP 2.0
														END

				,ASSOCIADO.[CodigoSituacaoFinanceira] = CASE B.bl_adimplente WHEN 1 THEN 1 ELSE 2 END
				,ASSOCIADO.[CAD] = B.CAD
				,ASSOCIADO.[Pais] = B.pais
			FROM ASSOCIADO
			INNER JOIN @TEMPTBLDADOASSOCIADO B ON B.CAD = ASSOCIADO.CAD
			

			--ATUALIZA O CONTATO PRINCIPAL		
			UPDATE 
				CONTATOASSOCIADO
			SET			
				 [Nome] = representante_smart_pessoaname
				,[CPFCNPJ] = representante_smart_cpf			
				,[Telefone1] = representante_telephone1
				,[Telefone2] = representante_telephone2
				,[Telefone3] = representante_telephone3
				,[Email] = representante_emailaddress1
				,[Email2] = representante_emailaddress2			
				,[DataAtualizacaoCRM] = GETDATE()			
			FROM CONTATOASSOCIADO A
			INNER JOIN ASSOCIADO B ON B.Codigo = A.CodigoAssociado
			INNER JOIN @TEMPTBLDADOASSOCIADO C ON C.CAD = B.CAD 
			AND REPLACE(REPLACE(REPLACE(C.representante_smart_cpf,'.',''),'/',''),'-','') = REPLACE(REPLACE(REPLACE(A.CPFCNPJ,'.',''),'/',''),'-','')


			--INSERE CONTATO PRINCIPAL NAO EXISTENTE
			INSERT INTO [dbo].[ContatoAssociado]
			([CodigoAssociado],[Nome],[CPFCNPJ],[CodigoTipoContato]
			,[Telefone1],[Telefone2],[Telefone3]
			,[Email],[Email2],[Status],[Observacao],[IndicadorPrincipal],[DataAtualizacaoCRM])
			SELECT B.Codigo, A.representante_smart_pessoaname, A.representante_smart_cpf, 1
				   ,A.representante_telephone1, A.representante_telephone2, A.representante_telephone3
				   ,A.representante_emailaddress1, A.representante_emailaddress2,1,NULL,1,GETDATE()
			FROM @TEMPTBLDADOASSOCIADO A
			INNER JOIN ASSOCIADO B ON B.CAD = A.CAD
			LEFT JOIN ContatoAssociado C ON C.CodigoAssociado = B.Codigo 
			AND REPLACE(REPLACE(REPLACE(a.representante_smart_cpf,'.',''),'/',''),'-','') = REPLACE(REPLACE(REPLACE(C.CPFCNPJ,'.',''),'/',''),'-','')
			WHERE C.Codigo IS NULL

			
			--INSERE GLN INFORMATIVO NAO EXISTENTE		
			INSERT INTO [dbo].[LocalizacaoFisica] ([Nome],[Descricao],[Status],[Prefixo],[GLN],[NumeroItem],[CodigoPapelGLN]
							,[Endereco],[Numero],[Complemento],[CEP],[Cidade],[Estado],[Pais],[Bairro]
							,[NomeImagem],[Observacao],[DataAlteracao],[CodigoUsuarioAlteracao],[IndicadorPrincipal]
							,[DataCancelamento],[DataSuspensao],[DataReutilizacao],[CodigoAssociado],[Latitude],[Longitude],[Telefone],[Email])
    

		
			SELECT DISTINCT a.nome, a.nome, 1, a.glninformativo, a.glninformativo, a.glninformativo, 1 
				,a.logradouroprincipal, a.numero, a.complementoprincipal, a.address1_postalcode, a.address1_city, a.estadoname, a.pais, a.bairroprincipal
				,NULL, NULL, GETDATE(), 1, 1, NULL, NULL, NULL, B.Codigo, NULL, NULL, a.telephone1, a.email  
			FROM @TEMPTBLDADOASSOCIADO A
			INNER JOIN ASSOCIADO B ON B.CAD = A.CAD
			LEFT JOIN LocalizacaoFisica C ON C.CodigoAssociado = B.Codigo 
			AND REPLACE(REPLACE(REPLACE(a.glninformativo,'.',''),'/',''),'-','') = REPLACE(REPLACE(REPLACE(C.gln,'.',''),'/',''),'-','')
			WHERE C.Codigo IS NULL
			AND A.GLNINFORMATIVO > 0

		
			--ATUALIZA LICENCAS EXISTENTES		
			UPDATE [dbo].[LicencaAssociado]
					SET [DataAniversario] = CASE C.data_aniversario WHEN '0001-01-01T00:00:00' THEN NULL WHEN NULL THEN NULL ELSE CONVERT(datetime,C.data_aniversario) END
						,[DataAlteracao] = GETDATE()
			FROM LicencaAssociado A
					INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado
					INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD
					INNER JOIN Licenca D ON D.NOME = C.licenca
					WHERE D.Codigo = A.CodigoLicenca


			--INSERE LICENCAS NOVAS
			INSERT INTO [dbo].[LicencaAssociado]
					   ([CodigoLicenca]
					   ,[CodigoAssociado]
					   ,[Status]
					   ,[DataAniversario]
					   ,[DataAlteracao])

			SELECT DISTINCT B.CODIGO, C.Codigo, 
					CASE A.status_da_licenca WHEN 1 THEN 5
									WHEN 2 THEN 1
									WHEN 3 THEN 6
									WHEN 4 THEN 3
									WHEN 5 THEN 7
									WHEN 6 THEN 8
									WHEN 7 THEN 4 END
					,CASE A.data_aniversario WHEN '0001-01-01T00:00:00' THEN NULL WHEN NULL THEN NULL ELSE CONVERT(datetime,A.data_aniversario) END
					,GETDATE()
			FROM @TEMPTBLLICENCA A
			INNER JOIN LICENCA B ON B.NOME = A.LICENCA
			INNER JOIN Associado C ON C.CAD = A.cad
			LEFT JOIN LicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo
			where D.CodigoAssociado is null
			AND A.status_da_licenca IS NOT NULL

				
			--ATUALIZA PREFIXOS EXISTENTES	
			UPDATE [dbo].[PrefixoLicencaAssociado]
			   SET 
				  [DataAlteracao] = GETDATE()
				  ,[CodigoStatusPrefixo] =	CASE c.status_da_licenca WHEN 1 THEN 5
											WHEN 2 THEN 1
											WHEN 3 THEN 6
											WHEN 4 THEN 3
											WHEN 5 THEN 7
											WHEN 6 THEN 8
											WHEN 7 THEN 4 END
			 FROM PrefixoLicencaAssociado A
			INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado
			INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD
			INNER JOIN Licenca D ON D.NOME = C.licenca
			WHERE D.Codigo = A.CodigoLicenca
			AND C.prefixo = A.NumeroPrefixo
			AND C.status_da_licenca IS NOT NULL


			--INSERE PREFIXOS NOVOS		
			INSERT INTO [dbo].[PrefixoLicencaAssociado]
					   ([CodigoLicenca]
					   ,[CodigoAssociado]
					   ,[NumeroPrefixo]
					   ,[DataAlteracao]
					   ,[CodigoStatusPrefixo])

			SELECT DISTINCT B.Codigo, C.Codigo, A.PREFIXO, GETDATE() 
				,CASE A.status_da_licenca WHEN 1 THEN 5
											WHEN 2 THEN 1
											WHEN 3 THEN 6
											WHEN 4 THEN 3
											WHEN 5 THEN 7
											WHEN 6 THEN 8
											WHEN 7 THEN 4 END
			FROM @TEMPTBLLICENCA A
			INNER JOIN LICENCA B ON B.NOME = A.LICENCA
			INNER JOIN Associado C ON C.CAD = A.cad
			LEFT JOIN PrefixoLicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo AND D.NumeroPrefixo = A.prefixo
			WHERE D.CodigoAssociado IS NULL
			AND A.PREFIXO <> '0'
			AND A.status_da_licenca IS NOT NULL

			--ATUALIZA PRODUTOS GTIN-8
			INSERT INTO [dbo].[ProdutoHistorico]
							([CodigoProduto],[globalTradeItemNumber],[productDescription],[CodigoTipoGTIN],[NrPrefixo],[CodItem],[CodigoAssociado],[CodigoUsuarioCriacao],[CodigoUsuarioAlteracao]
							,[CodigoUsuarioExclusao],[tradeItemCountryOfOrigin],[CodigoIdioma],[CodigoStatusGTIN],[CodeSegment],[CodeFamily],[CodeClass],[CodeBrick],[countryCode],[CodeBrickAttribute]
							,[IndicadorCompartilhaDados],[Observacoes],[Estado],[informationProvider],[brandName],[DataInclusao],[DataAlteracao]
							,[DataSuspensao],[DataReativacao],[DataCancelamento],[DataReutilizacao],[modelNumber],[importClassificationType],[importClassificationValue]
							,[alternateItemIdentificationId],[alternateItemIdentificationAgency],[minimumTradeItemLifespanFromTimeOfProduction],[startAvailabilityDateTime],[endAvailabilityDateTime]
							,[depth],[depthMeasurementUnitCode],[height],[heightMeasurementUnitCode],[width],[widthMeasurementUnitCode],[netContent],[netContentMeasurementUnitCode]
							,[grossWeight],[grossWeightMeasurementUnitCode],[netWeight],[netWeightMeasurementUnitCode],[CodigoTipoProduto],[isTradeItemABaseUnit],[isTradeItemAConsumerUnit]
							,[isTradeItemAModel],[isTradeItemAnInvoiceUnit],[packagingTypeCode],[PalletTypeCode],[totalQuantityOfNextLowerLevelTradeItem],[StackingFactor]
							,[quantityOfTradeItemContainedInACompleteLayer],[quantityOfTradeItemsPerPalletLayer],[quantityOfCompleteLayersContainedInATradeItem],[quantityOfLayersPerPallet]
							,[CodigoProdutoOrigem],[deliveryToDistributionCenterTemperatureMinimum],[storageHandlingTempMinimumUOM]
							,[storageHandlingTemperatureMaximum],[storageHandlingTemperatureMaximumunitOfMeasure],[isDangerousSubstanceIndicated],[ipiPerc],[isTradeItemAnOrderableUnit]
							,[isTradeItemADespatchUnit],[orderSizingFactor],[orderQuantityMultiple],[orderQuantityMinimum],[barcodeCertified],[dataQualityCertified],[dataQualityCertifiedAgencyCode]
							,[allGDSNAttributes],[CanalComunicacaoDados],[ProprioDonoInformacao],[ValidadoDonoDaInformacao],[IndicadorGDSN],[VarianteLogistica]
							,[DataHistorico])

			 SELECT A.[CodigoProduto],A.[globalTradeItemNumber],A.[productDescription],A.[CodigoTipoGTIN],A.[NrPrefixo],A.[CodItem],A.[CodigoAssociado],A.[CodigoUsuarioCriacao],A.[CodigoUsuarioAlteracao]
					,A.[CodigoUsuarioExclusao],A.[tradeItemCountryOfOrigin],A.[CodigoIdioma],A.[CodigoStatusGTIN],A.[CodeSegment],A.[CodeFamily],A.[CodeClass],A.[CodeBrick],A.[countryCode],A.[CodeBrickAttribute]
					,A.[IndicadorCompartilhaDados],A.[Observacoes],A.[Estado],A.[informationProvider],A.[brandName],A.[DataInclusao],A.[DataAlteracao]
					,A.[DataSuspensao],A.[DataReativacao],A.[DataCancelamento],A.[DataReutilizacao],A.[modelNumber],A.[importClassificationType],A.[importClassificationValue]
					,A.[alternateItemIdentificationId],A.[alternateItemIdentificationAgency],A.[minimumTradeItemLifespanFromTimeOfProduction],A.[startAvailabilityDateTime],A.[endAvailabilityDateTime]
					,A.[depth],A.[depthMeasurementUnitCode],A.[height],A.[heightMeasurementUnitCode],A.[width],A.[widthMeasurementUnitCode],A.[netContent],A.[netContentMeasurementUnitCode]
					,A.[grossWeight],A.[grossWeightMeasurementUnitCode],A.[netWeight],A.[netWeightMeasurementUnitCode],A.[CodigoTipoProduto],A.[isTradeItemABaseUnit],A.[isTradeItemAConsumerUnit]
					,A.[isTradeItemAModel],A.[isTradeItemAnInvoiceUnit],A.[packagingTypeCode],A.[PalletTypeCode],A.[totalQuantityOfNextLowerLevelTradeItem],A.[StackingFactor]
					,A.[quantityOfTradeItemContainedInACompleteLayer],A.[quantityOfTradeItemsPerPalletLayer],A.[quantityOfCompleteLayersContainedInATradeItem],A.[quantityOfLayersPerPallet]
					,A.[CodigoProdutoOrigem],A.[deliveryToDistributionCenterTemperatureMinimum],A.[storageHandlingTempMinimumUOM]
					,A.[storageHandlingTemperatureMaximum],A.[storageHandlingTemperatureMaximumunitOfMeasure],A.[isDangerousSubstanceIndicated],A.[ipiPerc],A.[isTradeItemAnOrderableUnit]
					,A.[isTradeItemADespatchUnit],A.[orderSizingFactor],A.[orderQuantityMultiple],A.[orderQuantityMinimum],A.[barcodeCertified],A.[dataQualityCertified],A.[dataQualityCertifiedAgencyCode]
					,A.[allGDSNAttributes],A.[CanalComunicacaoDados],A.[ProprioDonoInformacao],A.[ValidadoDonoDaInformacao],A.[IndicadorGDSN],A.[VarianteLogistica]					
			 , GETDATE()
			 FROM Produto A
			INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado
			INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD
			INNER JOIN Licenca D ON D.NOME = C.licenca
			WHERE C.prefixo = globalTradeItemNumber
			AND A.CodigoStatusGTIN = 1
			AND a.productdescription <> c.descricaogtin8


			UPDATE [dbo].[Produto]
			SET productdescription = c.descricaogtin8
			FROM Produto A
			INNER JOIN Associado B ON B.Codigo = A.CodigoAssociado
			INNER JOIN @TEMPTBLLICENCA C ON C.CAD = B.CAD
			INNER JOIN Licenca D ON D.NOME = C.licenca
			WHERE C.prefixo = globalTradeItemNumber
			AND A.CodigoStatusGTIN = 1
			AND a.productdescription <> c.descricaogtin8


			--INSERE NOVOS PRODUTOS GTIN-8
			INSERT INTO [dbo].[Produto]
			   ([globalTradeItemNumber]
			   ,[productDescription]
			   ,[CodigoTipoGTIN]
			   ,[NrPrefixo]
			   ,[CodItem]
			   ,[CodigoAssociado]
			   ,[CodigoUsuarioCriacao]          
			   ,[DataInclusao]
			   ,[CodigoStatusGTIN]
			   ,indicadorgdsn)

			SELECT DISTINCT A.PREFIXO
				   ,a.descricaogtin8
				   ,1
				   ,A.PREFIXO
				   ,SUBSTRING(A.PREFIXO,4,4)
				   ,C.CODIGO
				   ,1
				   ,GETDATE()
				   ,1
				   ,0
				FROM @TEMPTBLLICENCA A
				INNER JOIN LICENCA B ON B.NOME = A.LICENCA
				INNER JOIN Associado C ON C.CAD = A.cad
				INNER JOIN PrefixoLicencaAssociado D ON D.CodigoLicenca = B.Codigo AND D.CodigoAssociado = C.Codigo AND D.NumeroPrefixo = A.prefixo
				where B.Codigo = 2
				and not exists (select top 1 x.[globalTradeItemNumber] from produto x where x.[globalTradeItemNumber] = A.PREFIXO)
				and a.descricaogtin8 is not null

	
			-- Grava hist�rico e atualiza o usu�rio principal
			INSERT INTO [dbo].[UsuarioHistorico]
					   ([Codigo],[Nome],[CodigoStatusUsuario],[CodigoPerfil],[CodigoTipoUsuario],[Email],
					   [TelefoneResidencial],[Senha],[CodigoUsuarioAlteracao],[DataAlteracao],[MensagemObservacao],
					   [DataAlteracaoSenha],[QuantidadeTentativa],[DataUltimoAcesso],[DataCadastro],[TelefoneCelular],
					   [TelefoneComercial],[Ramal],[Departamento],[Cargo],[CPFCNPJ],[DataHistorico])
   
			SELECT	DISTINCT C.Codigo,C.Nome,C.CodigoStatusUsuario,C.CodigoPerfil,C.CodigoTipoUsuario,C.Email,
				C.TelefoneResidencial,C.Senha,C.CodigoUsuarioAlteracao,C.DataAlteracao,C.MensagemObservacao,
				C.DataAlteracaoSenha,C.QuantidadeTentativa,C.DataUltimoAcesso,C.DataCadastro,C.TelefoneCelular,
				C.TelefoneComercial,C.Ramal,C.Departamento,C.Cargo,C.CPFCNPJ, GETDATE()
			FROM Associado
				INNER JOIN AssociadoUsuario B ON B.CodigoAssociado = ASSOCIADO.Codigo
				INNER JOIN Usuario C ON C.Codigo = B.CodigoUsuario
				INNER JOIN @TEMPTBLDADOASSOCIADO D ON D.CAD = ASSOCIADO.CAD
				WHERE B.IndicadorPrincipal = 1
		
			UPDATE [dbo].[Usuario]					
							SET Email = CONVERT(VARCHAR(255),d.email),
								Nome = CONVERT(VARCHAR(255),d.nome),
								CPFCNPJ = CONVERT(VARCHAR(14),d.cpfcnpj)
		
			FROM Associado
			INNER JOIN AssociadoUsuario B ON B.CodigoAssociado = ASSOCIADO.Codigo
			INNER JOIN Usuario C ON C.Codigo = B.CodigoUsuario
			INNER JOIN @TEMPTBLDADOASSOCIADO D ON D.CAD = ASSOCIADO.CAD
			WHERE B.IndicadorPrincipal = 1


			-- Desbloqueia os usuarios principais de associados desbloqueados
			UPDATE [dbo].[Usuario]
						SET codigoStatusUsuario = 1
			--SELECT C.*
			FROM Associado
			INNER JOIN AssociadoUsuario B ON B.CodigoAssociado = ASSOCIADO.Codigo
			INNER JOIN Usuario C ON C.Codigo = B.CodigoUsuario
			INNER JOIN @TEMPTBLDADOASSOCIADO D ON D.CAD = ASSOCIADO.CAD
			WHERE B.IndicadorPrincipal = 1
			AND C.CodigoStatusUsuario = 3
			AND D.bl_adimplente = 1 
			AND D.relacionamento =2


			

			--ATUALIZA DATA DE ULTIMA SINCRONIZACAO
			UPDATE PARAMETRO SET VALOR = CONVERT(VARCHAR(50),GETDATE(),131) WHERE CHAVE = 'parametros.integracoes.ultimaSincronizacaoCrmRgc'

		
		IF @@ERROR <> 0
		BEGIN			
			ROLLBACK			
			RAISERROR ('Erro ao gravar sincronizar dados CRM.', 16, 1)
			RETURN
		END


	 COMMIT
END


--------------------------------------------------------------------------
-----------------	   PROC REL LOG MONITORAMENTO	    ------------------
--------------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_REL_LOG_MONITORAMENTO') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_REL_LOG_MONITORAMENTO END

CREATE PROCEDURE [dbo].[PROC_REL_LOG_MONITORAMENTO](

	@DATAINICIAL DATETIME,

	@DATAFINAL DATETIME,

	@CODIGOUSUARIO BIGINT = NULL,

	@CODIGOENTIDADE BIGINT = NULL,

	@URL VARCHAR(500) = NULL
)

AS

BEGIN
	SELECT A.CODIGO, A.DATAALTERACAO, B.NOME, URL, CLASSE, METODO, REQUISICAO, A.CODIGOENTIDADE, C.NOME AS TIPOUSUARIO, D.NOME AS PERFIL

	FROM LOG A WITH (NOLOCK)

	LEFT JOIN USUARIO B WITH (NOLOCK) ON B.CODIGO = A.CODIGOUSUARIO

	LEFT JOIN TIPOUSUARIO C WITH (NOLOCK) ON C.CODIGO = B.CODIGOTIPOUSUARIO

	LEFT JOIN PERFIL D WITH (NOLOCK) ON D.CODIGO = B.CODIGOPERFIL

	WHERE CONVERT(DATETIME,CONVERT(VARCHAR(10),A.DATAALTERACAO,101)) BETWEEN @DATAINICIAL AND @DATAFINAL + ' 23:59'

	AND (A.CODIGOUSUARIO = @CODIGOUSUARIO OR @CODIGOUSUARIO IS NULL)

	AND (A.CODIGOENTIDADE = @CODIGOENTIDADE OR @CODIGOENTIDADE IS NULL)

	AND (URL LIKE '%'+@URL+'%' OR @URL IS NULL)
END


------------------------------------------------------------------
-----------------	   PROC REL LOG ERRO	    ------------------
------------------------------------------------------------------
IF EXISTS ( SELECT * FROM sysobjects WHERE id = object_id(N'PROC_REL_LOGERRO') AND OBJECTPROPERTY(id, N'IsProcedure') = 1 )
BEGIN DROP PROCEDURE PROC_REL_LOGERRO END

CREATE PROCEDURE [dbo].[PROC_REL_LOGERRO](

	@DATAINICIAL DATETIME,

	@DATAFINAL DATETIME,

	@CODIGOUSUARIO BIGINT = NULL

)

AS

BEGIN

	SELECT A.CODIGO, A.DATALOG, A.MENSAGEM, A.DESCRICAO, B.NOME, A.EMAIL, A.URL, A.IP, A.NAVEGADOR, A.VERSAONAVEGADOR

	FROM LOGERRO A WITH (NOLOCK)

	LEFT JOIN USUARIO B WITH (NOLOCK) ON B.CODIGO = A.CODIGOUSUARIO

	WHERE CONVERT(DATETIME,CONVERT(VARCHAR(10),A.DATALOG,101)) BETWEEN @DATAINICIAL AND @DATAFINAL + ' 23:59'

	AND (A.CODIGOUSUARIO = @CODIGOUSUARIO OR @CODIGOUSUARIO IS NULL)

END
