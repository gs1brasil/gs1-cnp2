IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Associado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Associado;

CREATE TABLE [dbo].[Associado](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CAD] [varchar](50) NOT NULL,
	[Nome] [varchar](255) NULL,
	[Descricao] [varchar](5000) NULL,
	[Email] [varchar](255) NULL,
	[CPFCNPJ] [varchar](14) NULL,
	[InscricaoEstadual] [varchar](50) NULL,
	[CodigoTipoAssociado] [int] NOT NULL,
	[MensagemObservacao] [text] NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataAtualizacaoCRM] [datetime] NULL,
	[DataStatusInadimplente] [datetime] NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[Endereco] [varchar](1000) NULL,
	[Numero] [varchar](50) NULL,
	[Complemento] [varchar](100) NULL,
	[Bairro] [varchar](100) NULL,
	[Cidade] [varchar](255) NULL,
	[UF] [varchar](2) NULL,
	[CEP] [varchar](50) NULL,
	[CodigoTipoCompartilhamento] [int] NOT NULL,
	[CodigoStatusAssociado] [int] NOT NULL,
	[CodigoSituacaoFinanceira] [int] NOT NULL,
	[Pais] [varchar](50) NULL,
	[IndicadorCNAERestritivo] [varchar](1) NULL,
 CONSTRAINT [PK_Associado] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_SituacaoFinanceira] FOREIGN KEY([CodigoSituacaoFinanceira])
REFERENCES [dbo].[SituacaoFinanceira] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_SituacaoFinanceira]
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_StatusAssociado] FOREIGN KEY([CodigoStatusAssociado])
REFERENCES [dbo].[StatusAssociado] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_StatusAssociado]
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_TipoAssociado] FOREIGN KEY([CodigoTipoAssociado])
REFERENCES [dbo].[TipoAssociado] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_TipoAssociado]
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_TipoCompartilhamento] FOREIGN KEY([CodigoTipoCompartilhamento])
REFERENCES [dbo].[TipoCompartilhamento] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_TipoCompartilhamento]
GO
ALTER TABLE [dbo].[Associado]  WITH CHECK ADD  CONSTRAINT [FK_Associado_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[Associado] CHECK CONSTRAINT [FK_Associado_Usuario]
GO
