USE [db_gs1CNP_E5]IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AssociadoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AssociadoUsuario;

CREATE TABLE [dbo].[AssociadoUsuario](
	[CodigoUsuario] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[DataConfirmacao] [datetime] NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[IndicadorPrincipal] [varchar](1) NOT NULL,
 CONSTRAINT [PK_AssociadoUsuario] PRIMARY KEY CLUSTERED 
(
	[CodigoUsuario] ASC,
	[CodigoAssociado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AssociadoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoUsuario_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoUsuario] CHECK CONSTRAINT [FK_AssociadoUsuario_Associado]
GO
ALTER TABLE [dbo].[AssociadoUsuario]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoUsuario_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoUsuario] CHECK CONSTRAINT [FK_AssociadoUsuario_Usuario]
GO
