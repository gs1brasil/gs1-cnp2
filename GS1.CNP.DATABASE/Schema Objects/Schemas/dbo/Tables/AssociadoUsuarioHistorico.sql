USE [db_gs1CNP_E5]IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AssociadoUsuarioHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AssociadoUsuarioHistorico;

CREATE TABLE [dbo].[AssociadoUsuarioHistorico](
	[CodigoUsuario] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[DataConfirmacao] [datetime] NULL,
	[DataHistorico] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
 CONSTRAINT [PK_AssociadoUsuarioHistorico] PRIMARY KEY CLUSTERED 
(
	[CodigoUsuario] ASC,
	[CodigoAssociado] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[AssociadoUsuarioHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoUsuarioHistorico_AssociadoUsuario] FOREIGN KEY([CodigoUsuario], [CodigoAssociado])
REFERENCES [dbo].[AssociadoUsuario] ([CodigoUsuario], [CodigoAssociado])
GO
ALTER TABLE [dbo].[AssociadoUsuarioHistorico] CHECK CONSTRAINT [FK_AssociadoUsuarioHistorico_AssociadoUsuario]
GO
ALTER TABLE [dbo].[AssociadoUsuarioHistorico]  WITH CHECK ADD  CONSTRAINT [FK_AssociadoUsuarioHistorico_UsuarioAlteracao] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[AssociadoUsuarioHistorico] CHECK CONSTRAINT [FK_AssociadoUsuarioHistorico_UsuarioAlteracao]
GO
