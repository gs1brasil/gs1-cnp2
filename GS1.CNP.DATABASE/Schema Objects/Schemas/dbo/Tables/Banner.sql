IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Banner') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Banner;

CREATE TABLE Banner(
	Codigo [bigint] IDENTITY(1,1) NOT NULL,
	Nome [varchar](100) NOT NULL,
	Descricao [varchar](255) NULL,
	Status [int] NOT NULL,
	NomeImagem [varchar](255) NOT NULL,
	DataInicioPublicacao [datetime] NOT NULL,
	DataFimPublicacao [datetime] NOT NULL,
	DataAlteracao [datetime] NOT NULL,
	CodigoUsuarioAlteracao [bigint] NOT NULL,
	Link [varchar](255) NULL,
	CorFundo [varchar](50) NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[Banner]  WITH CHECK ADD  CONSTRAINT [FK_Banner_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
ALTER TABLE [dbo].[Banner] CHECK CONSTRAINT [FK_Banner_Usuario]