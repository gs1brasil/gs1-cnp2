IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('BannerEspacoBanner') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE BannerEspacoBanner;

CREATE TABLE BannerEspacoBanner(
	Codigo [bigint] IDENTITY(1,1) NOT NULL,
	CodigoEspacoBanner [int] NULL,
	CodigoBanner [bigint] NULL,
	Ordem [int] NULL,
 CONSTRAINT [PK_BannerEspacoBanner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[BannerEspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_BannerEspacoBanner_Banner] FOREIGN KEY([CodigoBanner])
REFERENCES [dbo].[Banner] ([Codigo])
ALTER TABLE [dbo].[BannerEspacoBanner] CHECK CONSTRAINT [FK_BannerEspacoBanner_Banner]

ALTER TABLE [dbo].[BannerEspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_BannerEspacoBanner_EspacoBanner] FOREIGN KEY([CodigoEspacoBanner])
REFERENCES [dbo].[EspacoBanner] ([Codigo])
ALTER TABLE [dbo].[BannerEspacoBanner] CHECK CONSTRAINT [FK_BannerEspacoBanner_EspacoBanner]