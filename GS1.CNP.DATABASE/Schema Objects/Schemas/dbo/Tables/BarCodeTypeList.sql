USE [db_gs1CNP_E5]IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('BarCodeTypeList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE BarCodeTypeList;

CREATE TABLE [dbo].[BarCodeTypeList](
	[Codigo] [int] NOT NULL,
	[CodeValue] [varchar](100) NOT NULL,
	[Name] [varchar](500) NOT NULL,
	[Definition] [text] NULL,
 CONSTRAINT [PK_BarCodeTypeList] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

