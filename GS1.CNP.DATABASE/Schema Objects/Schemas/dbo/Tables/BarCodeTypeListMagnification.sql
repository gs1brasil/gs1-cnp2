IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('BarCodeTypeListMagnification') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE BarCodeTypeListMagnification;

CREATE TABLE [dbo].[BarCodeTypeListMagnification](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[MagnificationFactor] [decimal](10, 3) NOT NULL,
	[IdealModuleWidth] [decimal](10, 3) NOT NULL,
	[Width] [decimal](10, 3) NOT NULL,
	[Height] [decimal](10, 3) NOT NULL,
	[LeftGuard] [int] NOT NULL,
	[RightGuard] [int] NULL,
	[CodigoBarCodeTypeList] [int] NULL,
 CONSTRAINT [PK_BarCodeTypeListMagnification] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[BarCodeTypeListMagnification]  WITH CHECK ADD  CONSTRAINT [FK_BarCodeTypeListMagnification_BarCodeTypeList] FOREIGN KEY([CodigoBarCodeTypeList])
REFERENCES [dbo].[BarCodeTypeList] ([Codigo])
GO
ALTER TABLE [dbo].[BarCodeTypeListMagnification] CHECK CONSTRAINT [FK_BarCodeTypeListMagnification_BarCodeTypeList]
GO
