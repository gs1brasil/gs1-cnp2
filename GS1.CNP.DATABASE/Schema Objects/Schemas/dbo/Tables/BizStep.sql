IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('BizStep') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE BizStep;

CREATE TABLE [dbo].[BizStep](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_BizStep] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
