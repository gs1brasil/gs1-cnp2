IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Campo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Campo;

CREATE TABLE [dbo].[Campo](
	[Codigo] [bigint] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Campo] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

