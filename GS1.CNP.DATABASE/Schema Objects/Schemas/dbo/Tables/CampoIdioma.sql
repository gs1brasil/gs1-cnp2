IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CampoIdioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CampoIdioma;

CREATE TABLE [dbo].[CampoIdioma](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Texto] [varchar](8000) NOT NULL,
	[Comentario] [varchar](8000) NULL,
	[Status] [int] NOT NULL,
	[CodigoCampo] [bigint] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
 CONSTRAINT [PK_CampoIdioma] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CampoIdioma]  WITH CHECK ADD  CONSTRAINT [FK_CampoIdioma_Campo] FOREIGN KEY([CodigoCampo])
REFERENCES [dbo].[Campo] ([Codigo])
GO
ALTER TABLE [dbo].[CampoIdioma] CHECK CONSTRAINT [FK_CampoIdioma_Campo]
GO
ALTER TABLE [dbo].[CampoIdioma]  WITH CHECK ADD  CONSTRAINT [FK_CampoIdioma_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[CampoIdioma] CHECK CONSTRAINT [FK_CampoIdioma_Idioma]
GO
