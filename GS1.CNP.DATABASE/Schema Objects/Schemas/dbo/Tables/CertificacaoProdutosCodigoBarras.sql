IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CertificacaoProdutosCodigoBarras') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CertificacaoProdutosCodigoBarras;

CREATE TABLE [dbo].[CertificacaoProdutosCodigoBarras](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosCodigoBarras] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Associado]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Produto]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarras] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarras_Usuario]
GO
