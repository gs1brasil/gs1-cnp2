IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CertificacaoProdutosCodigoBarrasHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CertificacaoProdutosCodigoBarrasHistorico;

CREATE TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico](
	[Codigo] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosCodigoBarrasHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Associado]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_CertificacaoProdutosCodigoBarras] FOREIGN KEY([Codigo])
REFERENCES [dbo].[CertificacaoProdutosCodigoBarras] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_CertificacaoProdutosCodigoBarras]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Produto]
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosCodigoBarrasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosCodigoBarrasHistorico_Usuario]
GO
