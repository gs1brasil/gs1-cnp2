IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CertificacaoProdutosPesosMedidasHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CertificacaoProdutosPesosMedidasHistorico;

CREATE TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico](
	[Codigo] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataCertificado] [datetime] NOT NULL,
	[DataValidade] [datetime] NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_CertificacaoProdutosPesosMedidasHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Associado]
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_CertificacaoProdutosCodigoBarras] FOREIGN KEY([Codigo])
REFERENCES [dbo].[CertificacaoProdutosCodigoBarras] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_CertificacaoProdutosCodigoBarras]
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Produto]
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico]  WITH CHECK ADD  CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[CertificacaoProdutosPesosMedidasHistorico] CHECK CONSTRAINT [FK_CertificacaoProdutosPesosMedidasHistorico_Usuario]
GO
