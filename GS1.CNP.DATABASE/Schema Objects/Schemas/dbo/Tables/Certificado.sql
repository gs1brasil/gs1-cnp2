IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Certificado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Certificado;

CREATE TABLE Certificado ( 
	Codigo int NOT NULL,
	Sigla varchar(15),
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Vigencia varchar(1),
	ValorVigencia int
);

ALTER TABLE Certificado ADD CONSTRAINT PK_Certificado 
	PRIMARY KEY CLUSTERED (Codigo);







