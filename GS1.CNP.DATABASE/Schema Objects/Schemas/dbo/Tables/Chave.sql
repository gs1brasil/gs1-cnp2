IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Chave') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Chave;

CREATE TABLE Chave ( 
	CodigoChave bigint identity(1,1)  NOT NULL,
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	NumeroPrefixo bigint NOT NULL,
	Chave varchar(max) NOT NULL,
	DataCriacao datetime NOT NULL,
	DataExpiracao datetime,
	DataExpiracaoOriginal datetime NOT NULL,
	CodigoUsuarioCriacao bigint NOT NULL,
	CodigoUsuarioExpiracao bigint
);

ALTER TABLE Chave ADD CONSTRAINT PK_Chave 
	PRIMARY KEY CLUSTERED (CodigoChave);

ALTER TABLE Chave ADD CONSTRAINT FK_Chave_PrefixoLicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado, NumeroPrefixo) REFERENCES PrefixoLicencaAssociado (CodigoLicenca, CodigoAssociado, NumeroPrefixo);

ALTER TABLE Chave ADD CONSTRAINT FK_Chave_UsuarioExpiracao 
	FOREIGN KEY (CodigoUsuarioExpiracao) REFERENCES Usuario (Codigo);

ALTER TABLE Chave ADD CONSTRAINT FK_Chave_UsuarioCriacao 
	FOREIGN KEY (CodigoUsuarioCriacao) REFERENCES Usuario (Codigo);
