﻿ALTER TABLE [dbo].[Fornecedor] ADD  DEFAULT ((0)) FOR [QualificacaoVotos]
GO
ALTER TABLE [dbo].[FornecedorHistorico] ADD  DEFAULT ((0)) FOR [QualificacaoVotos]
GO
ALTER TABLE [dbo].[FornecedorHistoricoExpurgo] ADD  DEFAULT ((0)) FOR [QualificacaoVotos]
GO
ALTER TABLE [dbo].[Usuario] ADD  DEFAULT ((0)) FOR [QuantidadeTentativa]
GO
ALTER TABLE [dbo].[Usuario] ADD  DEFAULT (getdate()) FOR [DataCadastro]


