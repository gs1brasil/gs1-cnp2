IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ContatoAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ContatoAssociado;

CREATE TABLE [dbo].[ContatoAssociado](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[CodigoAssociado] [bigint] NULL,
	[Nome] [varchar](255) NOT NULL,
	[CPFCNPJ] [varchar](14) NOT NULL,
	[CodigoTipoContato] [int] NOT NULL,
	[Telefone1] [varchar](50) NULL,
	[Telefone2] [varchar](50) NULL,
	[Telefone3] [varchar](50) NULL,
	[Email] [varchar](255) NULL,
	[Email2] [varchar](255) NULL,
	[Status] [varchar](1) NULL,
	[DataAtualizacaoCRM] [datetime] NULL,
	[Observacao] [text] NULL,
	[IndicadorPrincipal] [varchar](1) NULL,
 CONSTRAINT [PK_ContatoAssociado] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ContatoAssociado]  WITH CHECK ADD  CONSTRAINT [FK_ContatoAssociado_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[ContatoAssociado] CHECK CONSTRAINT [FK_ContatoAssociado_Associado]
GO
ALTER TABLE [dbo].[ContatoAssociado]  WITH CHECK ADD  CONSTRAINT [FK_ContatoAssociado_TipoContato] FOREIGN KEY([CodigoTipoContato])
REFERENCES [dbo].[TipoContato] ([Codigo])
GO
ALTER TABLE [dbo].[ContatoAssociado] CHECK CONSTRAINT [FK_ContatoAssociado_TipoContato]
GO
