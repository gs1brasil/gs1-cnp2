IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CountryOfOriginISO3166') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE CountryOfOriginISO3166;

CREATE TABLE [dbo].[CountryOfOriginISO3166](
	[Codigo] [int] NOT NULL,
	[CodigoPais] [varchar](50) NULL,
	[Nome] [varchar](255) NULL,
	[Sigla2] [varchar](2) NOT NULL,
	[Sigla3] [varchar](3) NULL,
	[CodigoTelefonico] [varchar](100) NULL,
	[SiglaIANA] [varchar](100) NULL,
 CONSTRAINT [PK_CountryOfOriginISO3166] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
