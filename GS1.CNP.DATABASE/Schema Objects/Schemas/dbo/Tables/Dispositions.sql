IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Dispositions') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Dispositions;

CREATE TABLE Dispositions ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL, 
	Descricao varchar(255),
	CodigoBizStep int NOT NULL,
	Status int NOT NULL
);

ALTER TABLE Dispositions ADD CONSTRAINT PK_Dispositions
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Dispositions ADD CONSTRAINT FK_Dispositions_BizStep
	FOREIGN KEY (CodigoBizStep) REFERENCES BizStep (Codigo);