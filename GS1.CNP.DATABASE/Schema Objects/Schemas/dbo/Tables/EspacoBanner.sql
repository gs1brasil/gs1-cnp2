IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('EspacoBanner') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE EspacoBanner;

CREATE TABLE EspacoBanner(
	Codigo [int] NOT NULL,
	Nome [varchar](50) NOT NULL,
	Descricao [varchar](255) NULL,
	Status [bigint] NOT NULL,
	DataAlteracao [datetime] NOT NULL,
	CodigoUsuarioAlteracao [bigint] NOT NULL,
	Altura [varchar](50) NOT NULL,
	Largura [varchar](50) NOT NULL,
	UrlPublicacao [varchar](255) NOT NULL,
	QuantidadeMaxima [int] NOT NULL,
	CodigoTipoEspacoBanner [int] NOT NULL,
 CONSTRAINT [PK_EspacoBanner] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[EspacoBanner]  WITH CHECK ADD  CONSTRAINT [FK_EspacoBanner_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
ALTER TABLE [dbo].[EspacoBanner] CHECK CONSTRAINT [FK_EspacoBanner_Usuario]








