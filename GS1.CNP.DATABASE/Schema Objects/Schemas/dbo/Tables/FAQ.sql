USE [db_gs1CNP_E5]
GO
/****** Object:  Table [dbo].[FAQ]    Script Date: 12/02/2016 17:23:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FAQ](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Descricao] [text] NOT NULL,
	[Url] [varchar](max) NULL,
	[Ordem] [int] NULL,
	[DataCadastro] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[CodigoStatusVisualizacaoFAQ] [int] NOT NULL,
 CONSTRAINT [PK_FAQ] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_Idioma]
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_StatusPublicacao]
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_StatusVisualizacaoFAQ] FOREIGN KEY([CodigoStatusVisualizacaoFAQ])
REFERENCES [dbo].[StatusVisualizacaoFAQ] ([Codigo])
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_StatusVisualizacaoFAQ]
GO
ALTER TABLE [dbo].[FAQ]  WITH CHECK ADD  CONSTRAINT [FK_FAQ_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[FAQ] CHECK CONSTRAINT [FK_FAQ_Usuario]
GO
