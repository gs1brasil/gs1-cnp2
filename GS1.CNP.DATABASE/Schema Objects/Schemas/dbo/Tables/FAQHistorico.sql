USE [db_gs1CNP_E5]
GO
/****** Object:  Table [dbo].[FAQHistorico]    Script Date: 12/02/2016 17:23:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FAQHistorico](
	[Codigo] [bigint] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Descricao] [text] NOT NULL,
	[Url] [varchar](max) NULL,
	[Ordem] [int] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[CodigoStatusVisualizacaoFAQ] [int] NOT NULL,
 CONSTRAINT [PK_FAQHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_FAQ] FOREIGN KEY([Codigo])
REFERENCES [dbo].[FAQ] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_FAQ]
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_Idioma]
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_StatusPublicacao]
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_StatusVisualizacaoFAQ] FOREIGN KEY([CodigoStatusVisualizacaoFAQ])
REFERENCES [dbo].[StatusVisualizacaoFAQ] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_StatusVisualizacaoFAQ]
GO
ALTER TABLE [dbo].[FAQHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FAQHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[FAQHistorico] CHECK CONSTRAINT [FK_FAQHistorico_Usuario]
GO
