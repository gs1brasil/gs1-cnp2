IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Fabricante') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Fabricante;

CREATE TABLE Fabricante ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE Fabricante ADD CONSTRAINT PK_Fabricante 
	PRIMARY KEY CLUSTERED (Codigo);
