IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Formulario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Formulario;

CREATE TABLE Formulario ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int,
	CodigoModulo int NOT NULL,
	Url varchar(255) NOT NULL,
	Icone varchar(100),
	Ordem int,
	IndicadorMenu varchar(1) NOT NULL
);

ALTER TABLE Formulario ADD CONSTRAINT PK_Formulario 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Formulario ADD CONSTRAINT FK_Formulario_Modulo 
	FOREIGN KEY (CodigoModulo) REFERENCES Modulo (Codigo);
