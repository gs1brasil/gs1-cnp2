IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioAjuda') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioAjuda;

CREATE TABLE [dbo].[FormularioAjuda](
	[Codigo] [int] NOT NULL,
	[CodigoFormulario] [int] NOT NULL,
	[CodigoTipoAjuda] [int] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Ajuda] [text] NULL,
	[UrlVideo] [varchar](max) NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
 CONSTRAINT [PK_FormularioAjuda] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_Formulario]
GO
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_Idioma]
GO
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_StatusAjuda] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusAjuda] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_StatusAjuda]
GO
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_StatusPublicacao]
GO
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_TipoAjuda] FOREIGN KEY([CodigoTipoAjuda])
REFERENCES [dbo].[TipoAjuda] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_TipoAjuda]
GO
ALTER TABLE [dbo].[FormularioAjuda]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjuda_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjuda] CHECK CONSTRAINT [FK_FormularioAjuda_Usuario]
GO
