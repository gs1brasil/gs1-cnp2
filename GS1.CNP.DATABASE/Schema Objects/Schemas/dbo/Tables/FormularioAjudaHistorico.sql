IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioAjudaHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioAjudaHistorico;

CREATE TABLE [dbo].[FormularioAjudaHistorico](
	[CodigoFormularioAjuda] [int] NOT NULL,
	[CodigoFormulario] [int] NOT NULL,
	[CodigoTipoAjuda] [int] NOT NULL,
	[Nome] [varchar](255) NOT NULL,
	[Ajuda] [text] NULL,
	[UrlVideo] [varchar](max) NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[DataHistorico] [datetime] NOT NULL,
	[CodigoStatusPublicacao] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
 CONSTRAINT [PK_FormularioAjudaHistorico] PRIMARY KEY CLUSTERED 
(
	[CodigoFormularioAjuda] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_Formulario]
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_FormularioAjuda] FOREIGN KEY([CodigoFormularioAjuda])
REFERENCES [dbo].[FormularioAjuda] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_FormularioAjuda]
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_Idioma]
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_StatusAjuda] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusAjuda] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_StatusAjuda]
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_StatusPublicacao]
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_TipoAjuda] FOREIGN KEY([CodigoTipoAjuda])
REFERENCES [dbo].[TipoAjuda] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_TipoAjuda]
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FormularioAjudaHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioAjudaHistorico] CHECK CONSTRAINT [FK_FormularioAjudaHistorico_Usuario]
GO
