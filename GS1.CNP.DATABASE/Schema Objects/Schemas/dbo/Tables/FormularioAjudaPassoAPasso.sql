IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioAjudaPassoAPasso') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioAjudaPassoAPasso;

CREATE TABLE FormularioAjudaPassoAPasso ( 
	Codigo int NOT NULL,
	CodigoFormulario int NOT NULL,
	CodigoTipoAjuda int NOT NULL,
	Nome varchar(255) NOT NULL,
	Ajuda varchar(max),
	Ordem int NULL, 
	UrlVideo varchar(max) NULL,
	CodigoStatusPublicacao int NOT NULL,
	CodigoIdioma int NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE FormularioAjudaPassoAPasso ADD CONSTRAINT PK_FormularioAjudaPassoAPasso
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE FormularioAjudaPassoAPasso ADD CONSTRAINT FK_FormularioAjudaPassoAPasso_Formulario 
	FOREIGN KEY (CodigoFormulario) REFERENCES Formulario (Codigo);

ALTER TABLE FormularioAjudaPassoAPasso ADD CONSTRAINT FK_FormularioAjudaPassoAPasso_TipoAjuda 
	FOREIGN KEY (CodigoTipoAjuda) REFERENCES TipoAjuda (Codigo);

ALTER TABLE FormularioAjudaPassoAPasso ADD CONSTRAINT FK_FormularioAjudaPassoAPasso_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);