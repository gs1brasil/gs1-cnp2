IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioAjudaPassoAPassoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioAjudaPassoAPassoHistorico
;

CREATE TABLE FormularioAjudaPassoAPassoHistorico ( 
	CodigoFormularioAjudaPassoAPasso int NOT NULL,
	CodigoFormulario int NOT NULL,
	CodigoTipoAjuda int NOT NULL,
	Nome varchar(255) NOT NULL,
	Ajuda varchar(max),
	Ordem int NULL, 
	UrlVideo varchar(max) NULL,
	CodigoStatusPublicacao int NOT NULL,
	CodigoIdioma int NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	DataHistorico datetime NOT NULL
)
;

ALTER TABLE FormularioAjudaPassoAPassoHistorico ADD CONSTRAINT PK_FormularioAjudaPassoAPassoHistorico
	PRIMARY KEY CLUSTERED (CodigoFormularioAjudaPassoAPasso, DataHistorico)
;

ALTER TABLE FormularioAjudaPassoAPassoHistorico ADD CONSTRAINT FK_FormularioAjudaPassoAPassoHistorico_Formulario 
	FOREIGN KEY (CodigoFormulario) REFERENCES Formulario (Codigo)
;

ALTER TABLE FormularioAjudaPassoAPassoHistorico ADD CONSTRAINT FK_FormularioAjudaPassoAPassoHistorico_FormularioAjuda 
	FOREIGN KEY (CodigoFormularioAjudaPassoAPasso) REFERENCES FormularioAjudaPassoAPasso (Codigo)
;

ALTER TABLE FormularioAjudaPassoAPassoHistorico ADD CONSTRAINT FK_FormularioAjudaPassoAPassoHistorico_TipoAjuda 
	FOREIGN KEY (CodigoTipoAjuda) REFERENCES TipoAjuda (Codigo)
;

ALTER TABLE FormularioAjudaPassoAPassoHistorico ADD CONSTRAINT FK_FormularioAjudaPassoAPassoHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
;









