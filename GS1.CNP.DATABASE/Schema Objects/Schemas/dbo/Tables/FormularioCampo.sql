IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FormularioCampo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FormularioCampo;

CREATE TABLE FormularioCampo ( 
	CodigoFormulario int NOT NULL,
	CodigoCampo bigint NOT NULL
);

ALTER TABLE FormularioCampo ADD CONSTRAINT PK_FormularioCampo 
	PRIMARY KEY CLUSTERED (CodigoFormulario, CodigoCampo);

ALTER TABLE FormularioCampo ADD CONSTRAINT FK_FormularioCampo_Campo 
	FOREIGN KEY (CodigoCampo) REFERENCES Campo (Codigo);

ALTER TABLE FormularioCampo ADD CONSTRAINT FK_FormularioCampo_Formulario 
	FOREIGN KEY (CodigoFormulario) REFERENCES Formulario (Codigo);
