USE [db_gs1CNP_E5]
GO
/****** Object:  Table [dbo].[FormularioIdioma]    Script Date: 12/02/2016 17:23:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormularioIdioma](
	[CodigoFormulario] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Descricao] [varchar](255) NOT NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_FormularioIdioma] PRIMARY KEY CLUSTERED 
(
	[CodigoFormulario] ASC,
	[CodigoIdioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FormularioIdioma]  WITH CHECK ADD  CONSTRAINT [FK_FormularioIdioma_Formulario] FOREIGN KEY([CodigoFormulario])
REFERENCES [dbo].[Formulario] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioIdioma] CHECK CONSTRAINT [FK_FormularioIdioma_Formulario]
GO
ALTER TABLE [dbo].[FormularioIdioma]  WITH CHECK ADD  CONSTRAINT [FK_FormularioIdioma_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[FormularioIdioma] CHECK CONSTRAINT [FK_FormularioIdioma_Idioma]
GO
