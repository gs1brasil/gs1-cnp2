IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Funcionalidade') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Funcionalidade;

CREATE TABLE Funcionalidade ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	CodigoTipoUsuario int NOT NULL,
	CodigoFormulario int
);

ALTER TABLE Funcionalidade ADD CONSTRAINT PK_Funcionalidade 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Funcionalidade ADD CONSTRAINT FK_Funcionalidade_Formulario 
	FOREIGN KEY (CodigoFormulario) REFERENCES Formulario (Codigo);

ALTER TABLE Funcionalidade ADD CONSTRAINT FK_Funcionalidade_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
