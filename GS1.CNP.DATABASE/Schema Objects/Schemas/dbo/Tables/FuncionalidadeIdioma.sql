IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FuncionalidadeIdioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FuncionalidadeIdioma;

CREATE TABLE FuncionalidadeIdioma ( 
	CodigoFuncionalidade int NOT NULL,
	CodigoIdioma int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE FuncionalidadeIdioma ADD CONSTRAINT PK_FuncionalidadeIdioma 
	PRIMARY KEY CLUSTERED (CodigoFuncionalidade, CodigoIdioma);

ALTER TABLE FuncionalidadeIdioma ADD CONSTRAINT FK_FuncionalidadeIdioma_Funcionalidade 
	FOREIGN KEY (CodigoFuncionalidade) REFERENCES Funcionalidade (Codigo);

ALTER TABLE FuncionalidadeIdioma ADD CONSTRAINT FK_FuncionalidadeIdioma_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);
