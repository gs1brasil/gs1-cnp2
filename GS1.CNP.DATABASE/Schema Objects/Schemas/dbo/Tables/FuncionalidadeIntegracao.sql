IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FuncionalidadeIntegracao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE FuncionalidadeIntegracao;

CREATE TABLE FuncionalidadeIntegracao ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE FuncionalidadeIntegracao ADD CONSTRAINT PK_FuncionalidadeIntegracao
	PRIMARY KEY CLUSTERED (Codigo);