IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('MeasurementType') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE MeasurementType;

CREATE TABLE MeasurementType (
	Codigo bigint identity(1,1) NOT NULL,
	measurementUnitCodeField varchar(255) NULL,
	codeListVersionField varchar(255) NULL,
	valueField decimal (5,2) NULL	
)

ALTER TABLE MeasurementType ADD CONSTRAINT PK_MeasurementType
	PRIMARY KEY CLUSTERED (Codigo);