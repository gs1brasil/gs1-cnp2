IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('NonBinaryLogicEnumeration') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE NonBinaryLogicEnumeration;

CREATE TABLE NonBinaryLogicEnumeration (
	Codigo bigint identity(1,1) NOT NULL,
	Nome varchar(255) NOT NULL,
	Status int NOT NULL
)

ALTER TABLE NonBinaryLogicEnumeration ADD CONSTRAINT PK_NonBinaryLogicEnumeration
	PRIMARY KEY CLUSTERED (Codigo);