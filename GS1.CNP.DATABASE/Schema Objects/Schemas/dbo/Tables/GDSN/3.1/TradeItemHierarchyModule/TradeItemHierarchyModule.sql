IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemHierarchyModule') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemHierarchyModule;

CREATE TABLE TradeItemHierarchyModule (
	Codigo bigint identity(1,1) NOT NULL,
	CodigoAVPList integer NULL,
	CodigoProduto bigint NULL,
	isNonGTINLogisticsUnitPackedIrregularlyField bigint NULL,
	isNonGTINLogisticsUnitPackedIrregularlyFieldSpecified varchar(1) NULL,
	isTradeItemPackedIrregularlyField bigint NULL,
	isTradeItemPackedIrregularlyFieldSpecified varchar(1) NULL,
	layerHeightField integer NULL,
	quantityOfCompleteLayersContainedInATradeItemField varchar(255) NULL,
	quantityOfInnerPackField varchar(255) NULL,
	quantityOfLayersPerPalletField varchar(255) NULL,
	quantityOfNextLevelTradeItemWithinInnerPackField varchar(255) NULL,
	quantityOfTradeItemsContainedInACompleteLayerField varchar(255) NULL,
	quantityOfTradeItemsPerPalletField varchar(255) NULL,
	quantityOfTradeItemsPerPalletLayerField varchar(255) NULL,
	suggestedConsumerPackField varchar(255) NULL,
	unitsPerTradeItemField integer NULL
)

ALTER TABLE TradeItemHierarchyModule ADD CONSTRAINT PK_TradeItemHierarchyModule
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE TradeItemHierarchyModule ADD CONSTRAINT FK_TradeItemHierarchyModule_Produto
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);
	
ALTER TABLE TradeItemHierarchyModule ADD CONSTRAINT FK_TradeItemHierarchyModule_NonBinaryLogicEnumeration_isNonGTIN
	FOREIGN KEY (isNonGTINLogisticsUnitPackedIrregularlyField) REFERENCES NonBinaryLogicEnumeration (Codigo);

ALTER TABLE TradeItemHierarchyModule ADD CONSTRAINT FK_TradeItemHierarchyModule_NonBinaryLogicEnumeration_isTrade
	FOREIGN KEY (isTradeItemPackedIrregularlyField) REFERENCES NonBinaryLogicEnumeration (Codigo);	
	