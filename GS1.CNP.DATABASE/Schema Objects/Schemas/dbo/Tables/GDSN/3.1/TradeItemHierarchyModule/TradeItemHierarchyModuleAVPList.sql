IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemHierarchyModuleAVPList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemHierarchyModuleAVPList;

CREATE TABLE TradeItemHierarchyModuleAVPList (
	Codigo bigint identity(1,1) NOT NULL,
	CodigoTradeItemHierarchyModule bigint NOT NULL
)

ALTER TABLE TradeItemHierarchyModuleAVPList ADD CONSTRAINT PK_TradeItemHierarchyModuleAVPList
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE TradeItemHierarchyModuleAVPList ADD CONSTRAINT FK_TradeItemHierarchyModuleAVPList_TradeItemHierarchyModule
	FOREIGN KEY (CodigoTradeItemHierarchyModule) REFERENCES TradeItemHierarchyModule (Codigo);
	
