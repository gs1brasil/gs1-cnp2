IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemHierarchyModuleAVPListCompound') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemHierarchyModuleAVPListCompound;

CREATE TABLE TradeItemHierarchyModuleAVPListCompound (
	Codigo bigint identity(1,1) NOT NULL,
	attributeNameField varchar(255) NULL,
    attributeCodeField varchar(255) NULL,
    codeListNameCodeField varchar(255) NULL,
    codeListVersionField varchar(255) NULL,
    valueField varchar(255) NULL,
	CodigoAVPList bigint NOT NULL
)

ALTER TABLE TradeItemHierarchyModuleAVPListCompound ADD CONSTRAINT PK_TradeItemHierarchyModuleAVPListCompound
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE TradeItemHierarchyModuleAVPListCompound ADD CONSTRAINT FK_TradeItemHierarchyModuleAVPListCompound_TradeItemHierarchyModuleAVPList
	FOREIGN KEY (CodigoAVPList) REFERENCES TradeItemHierarchyModuleAVPList (Codigo);
	
