IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemHierarchyModuleAVPListSingle') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemHierarchyModuleAVPListSingle;

CREATE TABLE TradeItemHierarchyModuleAVPListSingle (
	Codigo bigint identity(1,1) NOT NULL,
	attributeNameField varchar(255) NULL,
    valueField varchar(255) NULL,
	CodigoAVPList bigint NOT NULL
)

ALTER TABLE TradeItemHierarchyModuleAVPListSingle ADD CONSTRAINT PK_TradeItemHierarchyModuleAVPListSingle
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE TradeItemHierarchyModuleAVPListSingle ADD CONSTRAINT FK_TradeItemHierarchyModuleAVPListSingle_TradeItemHierarchyModuleAVPList
	FOREIGN KEY (CodigoAVPList) REFERENCES TradeItemHierarchyModuleAVPList (Codigo);
	
