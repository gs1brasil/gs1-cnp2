IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemLifespanModule') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemLifespanModule;

CREATE TABLE TradeItemLifespanModule (
	Codigo bigint identity(1,1) NOT NULL,
	CodigoAVPList bigint NULL,
	CodigoProduto bigint NULL,
	doesTradeItemHaveAutoReaderTrackerField bigint NULL,
	doesTradeItemHaveAutoReaderTrackerFieldSpecified varchar(1) NULL, 
	minimumTradeItemLifespanFromTimeOfArrivalField varchar(255) NULL,
	minimumTradeItemLifespanFromTimeOfProductionField varchar(255) NULL,
	openedTradeItemLifespanField varchar(255) NULL,
	supplierSpecifiedMinimumConsumerStorageDaysField varchar(255) NULL
)

ALTER TABLE TradeItemLifespanModule ADD CONSTRAINT PK_TradeItemLifespanModule
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE TradeItemLifespanModule ADD CONSTRAINT FK_TradeItemLifespanModule_NonBinaryLogicEnumeration
	FOREIGN KEY (doesTradeItemHaveAutoReaderTrackerField) REFERENCES NonBinaryLogicEnumeration (Codigo);
	
ALTER TABLE TradeItemLifespanModule ADD CONSTRAINT FK_TradeItemLifespanModule_Produto
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);