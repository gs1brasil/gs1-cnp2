IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemLifespanModuleAVPList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemLifespanModuleAVPList;

CREATE TABLE TradeItemLifespanModuleAVPList (
	Codigo bigint identity(1,1) NOT NULL,
	CodigoTradeItemLifespanModule bigint NOT NULL
)

ALTER TABLE TradeItemLifespanModuleAVPList ADD CONSTRAINT PK_TradeItemLifespanModuleAVPList
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE TradeItemLifespanModuleAVPList ADD CONSTRAINT FK_TradeItemLifespanModuleAVPList_TradeItemLifespanModule
	FOREIGN KEY (CodigoTradeItemLifespanModule) REFERENCES TradeItemLifespanModule (Codigo);
	
