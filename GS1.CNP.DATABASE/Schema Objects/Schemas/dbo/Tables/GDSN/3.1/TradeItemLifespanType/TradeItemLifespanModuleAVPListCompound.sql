IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemLifespanModuleAVPListCompound') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemLifespanModuleAVPListCompound;

CREATE TABLE TradeItemLifespanModuleAVPListCompound (
	Codigo bigint identity(1,1) NOT NULL,
	attributeNameField varchar(255) NULL,
    attributeCodeField varchar(255) NULL,
    codeListNameCodeField varchar(255) NULL,
    codeListVersionField varchar(255) NULL,
    valueField varchar(255) NULL,
	CodigoAVPList bigint NOT NULL
)

ALTER TABLE TradeItemLifespanModuleAVPListCompound ADD CONSTRAINT PK_TradeItemLifespanModuleAVPListCompound
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE TradeItemLifespanModuleAVPListCompound ADD CONSTRAINT FK_TradeItemLifespanModuleAVPListCompound_TradeItemLifespanModuleAVPList
	FOREIGN KEY (CodigoAVPList) REFERENCES TradeItemLifespanModuleAVPList (Codigo);
	
