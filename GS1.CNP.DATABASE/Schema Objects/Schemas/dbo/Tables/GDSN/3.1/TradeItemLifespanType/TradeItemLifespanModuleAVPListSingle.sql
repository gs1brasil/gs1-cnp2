IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemLifespanModuleAVPListSingle') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemLifespanModuleAVPListSingle;

CREATE TABLE TradeItemLifespanModuleAVPListSingle (
	Codigo bigint identity(1,1) NOT NULL,
	attributeNameField varchar(255) NULL,
    valueField varchar(255) NULL,
	CodigoAVPList bigint NOT NULL
)

ALTER TABLE TradeItemLifespanModuleAVPListSingle ADD CONSTRAINT PK_TradeItemLifespanModuleAVPListSingle
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE TradeItemLifespanModuleAVPListSingle ADD CONSTRAINT FK_TradeItemLifespanModuleAVPListSingle_TradeItemLifespanModuleAVPList
	FOREIGN KEY (CodigoAVPList) REFERENCES TradeItemLifespanModuleAVPList (Codigo);
	
