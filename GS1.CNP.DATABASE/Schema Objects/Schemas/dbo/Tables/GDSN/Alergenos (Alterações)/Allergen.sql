IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Allergen') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Allergen;

CREATE TABLE Allergen (
	Codigo bigint identity(1,1) NOT NULL,
	CodigoAllergenRelatedInformation integer NOT NULL,
	CodigoAllergenTypeCode integer NOT NULL,
	CodigoLevelOfContainment bigint NULL
)

ALTER TABLE Allergen ADD CONSTRAINT PK_Allergen
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE Allergen ADD CONSTRAINT FK_Allergen_AllergenRelatedInformation
	FOREIGN KEY (CodigoAllergenRelatedInformation) REFERENCES AllergenRelatedInformation (Codigo);

ALTER TABLE Allergen ADD CONSTRAINT FK_Allergen_AllergenTypeCode
	FOREIGN KEY (CodigoAllergenTypeCode) REFERENCES AllergenTypeCode (Codigo);
	
ALTER TABLE Allergen ADD CONSTRAINT FK_Allergen_LevelOfContainment
	FOREIGN KEY (CodigoLevelOfContainment) REFERENCES LevelOfContainment (Codigo);
	
