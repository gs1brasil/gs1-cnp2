IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AllergenRelatedInformationAVPList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AllergenRelatedInformationAVPList;

CREATE TABLE AllergenRelatedInformationAVPList (
	Codigo bigint identity(1,1) NOT NULL,
	CodigoAllergenRelatedInformation integer NOT NULL
)

ALTER TABLE AllergenRelatedInformationAVPList ADD CONSTRAINT PK_AllergenRelatedInformationAVPList
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE AllergenRelatedInformationAVPList ADD CONSTRAINT FK_AllergenRelatedInformationAVPList_AllergenRelatedInformation
	FOREIGN KEY (CodigoAllergenRelatedInformation) REFERENCES AllergenRelatedInformation (Codigo);
	
