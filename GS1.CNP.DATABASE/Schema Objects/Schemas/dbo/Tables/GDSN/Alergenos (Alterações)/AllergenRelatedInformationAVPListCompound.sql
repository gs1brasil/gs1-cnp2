IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AllergenRelatedInformationAVPListCompound') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AllergenRelatedInformationAVPListCompound;

CREATE TABLE AllergenRelatedInformationAVPListCompound (
	Codigo bigint identity(1,1) NOT NULL,
	attributeNameField varchar(255) NULL,
    attributeCodeField varchar(255) NULL,
    codeListNameCodeField varchar(255) NULL,
    codeListVersionField varchar(255) NULL,
    valueField varchar(255) NULL,
	CodigoAVPList bigint NOT NULL
)

ALTER TABLE AllergenRelatedInformationAVPListCompound ADD CONSTRAINT PK_AllergenRelatedInformationAVPListCompound
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE AllergenRelatedInformationAVPListCompound ADD CONSTRAINT FK_AllergenRelatedInformationAVPListCompound_AllergenRelatedInformationAVPList
	FOREIGN KEY (CodigoAVPList) REFERENCES AllergenRelatedInformationAVPList (Codigo);
	
