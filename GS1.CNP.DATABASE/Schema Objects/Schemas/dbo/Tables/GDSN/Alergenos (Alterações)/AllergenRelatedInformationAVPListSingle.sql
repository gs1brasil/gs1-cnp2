IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AllergenRelatedInformationAVPListSingle') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AllergenRelatedInformationAVPListSingle;

CREATE TABLE AllergenRelatedInformationAVPListSingle (
	Codigo bigint identity(1,1) NOT NULL,
	attributeNameField varchar(255) NULL,
    valueField varchar(255) NULL,
	CodigoAVPList bigint NOT NULL
)

ALTER TABLE AllergenRelatedInformationAVPListSingle ADD CONSTRAINT PK_AllergenRelatedInformationAVPListSingle
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE AllergenRelatedInformationAVPListSingle ADD CONSTRAINT FK_AllergenRelatedInformationAVPListSingle_AllergenRelatedInformationAVPList
	FOREIGN KEY (CodigoAVPList) REFERENCES AllergenRelatedInformationAVPList (Codigo);
	
