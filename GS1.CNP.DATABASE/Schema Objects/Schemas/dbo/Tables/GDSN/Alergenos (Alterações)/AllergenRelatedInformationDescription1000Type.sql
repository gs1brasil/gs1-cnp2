IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('AllergenRelatedInformationDescription1000Type') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE AllergenRelatedInformationDescription1000Type;

CREATE TABLE AllergenRelatedInformationDescription1000Type (
	Codigo bigint identity(1,1) NOT NULL,
	CodigoAllergenRelatedInformation integer NOT NULL,
	CodigoDescription1000Type integer NOT NULL
)

ALTER TABLE AllergenRelatedInformationDescription1000Type ADD CONSTRAINT PK_AllergenRelatedInformationDescription1000Type
	PRIMARY KEY CLUSTERED (Codigo);
	
ALTER TABLE AllergenRelatedInformationDescription1000Type ADD CONSTRAINT FK_AllergenRelatedInformationDescription1000Type_AllergenRelatedInformation
	FOREIGN KEY (CodigoAllergenRelatedInformation) REFERENCES AllergenRelatedInformation (Codigo);

ALTER TABLE AllergenRelatedInformationDescription1000Type ADD CONSTRAINT FK_AllergenRelatedInformationDescription1000Type_Description1000Type
	FOREIGN KEY (CodigoDescription1000Type) REFERENCES Description1000Type (Codigo);
	
