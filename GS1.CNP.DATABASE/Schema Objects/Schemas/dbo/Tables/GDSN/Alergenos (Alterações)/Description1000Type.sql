IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Description1000Type') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Description1000Type;

CREATE TABLE Description1000Type (
	Codigo integer identity(1,1) NOT NULL,
	languageCodeField varchar(255) NULL,
	codeListVersionField varchar(255) NULL,
	valueField varchar(255) NULL
)

ALTER TABLE Description1000Type ADD CONSTRAINT PK_Description1000Type
	PRIMARY KEY CLUSTERED (Codigo);
	
	
