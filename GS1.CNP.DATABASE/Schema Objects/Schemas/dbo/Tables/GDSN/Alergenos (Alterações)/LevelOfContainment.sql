IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LevelOfContainment') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LevelOfContainment;

CREATE TABLE LevelOfContainment (
	Codigo bigint identity(1,1) NOT NULL,
	CodeListVersionField varchar(255) NULL,
	ValueField varchar(255) NULL,
	Name varchar(255) NULL,
	[Definition] text NULL
)

ALTER TABLE LevelOfContainment ADD CONSTRAINT PK_LevelOfContainment
	PRIMARY KEY CLUSTERED (Codigo);