IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('GDSNUnitOfMeasureCodeList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE GDSNUnitOfMeasureCodeList;

CREATE TABLE GDSNUnitOfMeasureCodeList ( 
	Codigo int NOT NULL,
	CodeValue varchar(3) NOT NULL,
	Name varchar(300) NOT NULL,
	Definition text,
	Status int NOT NULL
);

ALTER TABLE GDSNUnitOfMeasureCodeList ADD CONSTRAINT PK_GDSNUnitOfMeasureCodeList 
	PRIMARY KEY CLUSTERED (Codigo);
