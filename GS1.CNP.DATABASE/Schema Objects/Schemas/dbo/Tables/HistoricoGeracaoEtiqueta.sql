IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('HistoricoGeracaoEtiqueta') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE HistoricoGeracaoEtiqueta;

CREATE TABLE HistoricoGeracaoEtiqueta ( 
	Codigo bigint identity(1,1)  NOT NULL,
	CodigoUsuario bigint NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoTipoGeracao int,
	DataImpressao datetime NOT NULL,
	Texto varchar(300),
	CodigoFabricante bigint,
	CodigoModeloEtiqueta int,
	Quantidade int,
	Descricao varchar(255),
	CodigoPosicaoTexto int,
	CodigoBarCodeTypeList int NOT NULL,
	FonteNomeDescricao varchar(100),
	FonteTamanhoDescricao int,
	AlinhamentoDescricao varchar(50),
	FonteNomeTextoLivre varchar(100),
	FonteTamanhoTextoLivre int,
	AlinhamentoTextoLivre varchar(50)
)
;

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT PK_HistoricoGeracaoEtiqueta 
	PRIMARY KEY CLUSTERED (Codigo)
;

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT FK_HistoricoGeracaoEtiqueta_BarCodeTypeList 
	FOREIGN KEY (CodigoBarCodeTypeList) REFERENCES BarCodeTypeList (Codigo)
;

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT FK_HistoricoGeracaoEtiqueta_PosicaoTexto 
	FOREIGN KEY (CodigoPosicaoTexto) REFERENCES PosicaoTexto (Codigo)
;

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT FK_HistoricoGeracaoEtiqueta_TipoGeracao 
	FOREIGN KEY (CodigoTipoGeracao) REFERENCES TipoGeracao (Codigo)
;

ALTER TABLE HistoricoGeracaoEtiqueta ADD CONSTRAINT FK_HistoricoGeracaoEtiqueta_ModeloEtiqueta 
	FOREIGN KEY (CodigoModeloEtiqueta) REFERENCES ModeloEtiqueta (Codigo)
