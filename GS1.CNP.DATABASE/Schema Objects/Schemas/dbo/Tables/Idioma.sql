IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Idioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Idioma;

CREATE TABLE Idioma ( 
	Codigo int identity(1,1)  NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	CodigoIdiomaTraducao int NOT NULL
);

ALTER TABLE Idioma ADD CONSTRAINT PK_Idioma 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Idioma ADD CONSTRAINT FK_Idioma_IdiomaTraducao 
	FOREIGN KEY (CodigoIdiomaTraducao) REFERENCES IdiomaTraducao (Codigo);
