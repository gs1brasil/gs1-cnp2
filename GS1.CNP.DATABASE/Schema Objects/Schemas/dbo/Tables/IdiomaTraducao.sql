IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('IdiomaTraducao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE IdiomaTraducao;

CREATE TABLE IdiomaTraducao ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Sigla varchar(20) NOT NULL,
	Status int NOT NULL
);

ALTER TABLE IdiomaTraducao ADD CONSTRAINT PK_IdiomaTraducao 
	PRIMARY KEY CLUSTERED (Codigo);
