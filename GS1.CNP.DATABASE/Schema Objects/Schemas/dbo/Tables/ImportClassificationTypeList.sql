IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportClassificationTypeList') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportClassificationTypeList;

CREATE TABLE ImportClassificationTypeList ( 
	Codigo int NOT NULL,
	CodeValue varchar(100) NOT NULL,
	Name varchar(500) NOT NULL
);

ALTER TABLE ImportClassificationTypeList ADD CONSTRAINT PK_ImportClassificationTypeList 
	PRIMARY KEY CLUSTERED (Codigo);

