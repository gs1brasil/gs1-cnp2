IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Importacao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Importacao;

CREATE TABLE Importacao ( 
	Codigo int identity(1,1)  NOT NULL,
	Arquivo varchar(500) NOT NULL,
	Data datetime NOT NULL,
	ItensImportados int NOT NULL,
	ItensNaoImportados int NOT NULL,
	CodigoImportacaoResultado int NOT NULL,
	TempoImportacao datetime NOT NULL,
	Status int NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE Importacao ADD CONSTRAINT PK_Importacao 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Importacao ADD CONSTRAINT PK_Importacao_ImportacaoResultado 
	FOREIGN KEY (CodigoImportacaoResultado) REFERENCES ImportacaoResultado (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Importacao ADD CONSTRAINT PK_Importacao_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
