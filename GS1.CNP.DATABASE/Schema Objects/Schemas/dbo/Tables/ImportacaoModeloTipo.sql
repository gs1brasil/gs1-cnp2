IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoModeloTipo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoModeloTipo;

CREATE TABLE ImportacaoModeloTipo ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoModeloTipo ADD CONSTRAINT PK_ImportaçãoModeloTipo 
	PRIMARY KEY CLUSTERED (Codigo);
