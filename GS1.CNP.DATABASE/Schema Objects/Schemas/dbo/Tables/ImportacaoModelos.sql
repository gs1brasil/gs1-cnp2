IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoModelos') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoModelos;

CREATE TABLE ImportacaoModelos ( 
	Codigo int NOT NULL,
	CodigoModeloTipo int NOT NULL,
	CodigoImportacaoTipoItem int NOT NULL,
	Data datetime NOT NULL,
	Arquivo varchar(500) NOT NULL,
	Nome varchar(500) NOT NULL
);

ALTER TABLE ImportacaoModelos ADD CONSTRAINT PK_ImportacaoModelos 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ImportacaoModelos ADD CONSTRAINT PK_ImportacaoModelos_ImportacaoModeloTipo 
	FOREIGN KEY (CodigoModeloTipo) REFERENCES ImportacaoModeloTipo (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
