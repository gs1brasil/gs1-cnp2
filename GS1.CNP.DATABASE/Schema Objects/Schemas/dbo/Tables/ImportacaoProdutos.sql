IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoProdutos') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoProdutos;

CREATE TABLE [dbo].[ImportacaoProdutos](
	[Codigo] [int] IDENTITY(1,1) NOT NULL,
	[Arquivo] [varchar](500) NULL,
	[Data] [datetime] NOT NULL,
	[ItensImportados] [int] NOT NULL,
	[ItensNaoImportados] [int] NOT NULL,
	[CodigoImportacaoResultado] [int] NOT NULL,
	[CodigoImportacaoTipo] [int] NOT NULL,
	[CodigoImportacaoTipoItem] [int] NOT NULL,
	[TempoImportacao] [time](7) NOT NULL,
	[Status] [int] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[ArquivoRelatorio] [varchar](500) NULL,
	[CodigoAssociado] [bigint] NOT NULL,
 CONSTRAINT [PK_ImportacaoProdutos] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos_ImportacaoResultado 
	FOREIGN KEY (CodigoImportacaoResultado) REFERENCES ImportacaoResultado (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos_ImportacaoTipo 
	FOREIGN KEY (CodigoImportacaoTipo) REFERENCES ImportacaoTipo (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos_ImportacaoTipoItem 
	FOREIGN KEY (CodigoImportacaoTipoItem) REFERENCES ImportacaoTipoItem (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ImportacaoProdutos ADD CONSTRAINT PK_ImportacaoProdutos_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
