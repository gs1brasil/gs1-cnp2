IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoResultado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoResultado;

CREATE TABLE ImportacaoResultado ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoResultado ADD CONSTRAINT PK_ImportacaoResultado 
	PRIMARY KEY CLUSTERED (Codigo);
