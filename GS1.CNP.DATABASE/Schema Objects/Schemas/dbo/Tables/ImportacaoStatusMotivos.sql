IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoStatusMotivos') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoStatusMotivos;

CREATE TABLE ImportacaoStatusMotivos ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoStatusMotivos ADD CONSTRAINT PK_ImportacaoStatusMotivos 
	PRIMARY KEY CLUSTERED (Codigo);
