IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoTipo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoTipo;

CREATE TABLE ImportacaoTipo ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoTipo ADD CONSTRAINT PK_ImportacaoTipo 
	PRIMARY KEY CLUSTERED (Codigo);
