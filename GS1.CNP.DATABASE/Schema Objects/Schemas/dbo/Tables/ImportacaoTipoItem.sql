IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ImportacaoTipoItem') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ImportacaoTipoItem;

CREATE TABLE ImportacaoTipoItem ( 
	Codigo int NOT NULL,
	Descricao varchar(500) NOT NULL
);

ALTER TABLE ImportacaoTipoItem ADD CONSTRAINT PK_ImportacaoTipoItem 
	PRIMARY KEY CLUSTERED (Codigo);
