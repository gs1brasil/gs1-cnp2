﻿CREATE NONCLUSTERED INDEX [IDX_ProdutoRelCertificado]
ON [dbo].[Produto] ([CodigoAssociado])
INCLUDE ([globalTradeItemNumber],[productDescription])
GO