/*
Missing Index Details from ConsultaProdutosPaginado.sql - hdt14ossoe.database.windows.net.cnp20-db-prod (admin_softbox (93))
The Query Processor estimates that implementing the following index could improve the query cost by 75.7422%.
*/

/*
USE [cnp20-DB-Prod]
GO
CREATE NONCLUSTERED INDEX IdxNCMDescricao
ON [dbo].[NCM] ([NCM])
INCLUDE ([Descricao])
GO
*/
