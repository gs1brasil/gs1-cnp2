﻿ALTER TABLE [dbo].[FornecedorHistoricoExpurgo]  WITH CHECK ADD  CONSTRAINT [FK_FornecedorHistoricoExpurgo_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])


GO
ALTER TABLE [dbo].[FornecedorHistoricoExpurgo] CHECK CONSTRAINT [FK_FornecedorHistoricoExpurgo_StatusPublicacao]

