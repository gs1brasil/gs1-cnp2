﻿ALTER TABLE [dbo].[FornecedorHistoricoExpurgo]  WITH CHECK ADD  CONSTRAINT [FK_FornecedorHistoricoExpurgo_TipoPatrocinador] FOREIGN KEY([CodigoTipoPatrocinador])
REFERENCES [dbo].[TipoPatrocinador] ([Codigo])


GO
ALTER TABLE [dbo].[FornecedorHistoricoExpurgo] CHECK CONSTRAINT [FK_FornecedorHistoricoExpurgo_TipoPatrocinador]

