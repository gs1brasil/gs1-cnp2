﻿ALTER TABLE [dbo].[FornecedorHistoricoExpurgo]  WITH CHECK ADD  CONSTRAINT [FK_FornecedorHistoricoExpurgo_UsuarioFornecedor] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[FornecedorHistoricoExpurgo] CHECK CONSTRAINT [FK_FornecedorHistoricoExpurgo_UsuarioFornecedor]

