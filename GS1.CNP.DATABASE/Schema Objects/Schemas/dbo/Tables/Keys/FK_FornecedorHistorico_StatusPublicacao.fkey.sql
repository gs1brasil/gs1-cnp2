﻿ALTER TABLE [dbo].[FornecedorHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FornecedorHistorico_StatusPublicacao] FOREIGN KEY([CodigoStatusPublicacao])
REFERENCES [dbo].[StatusPublicacao] ([Codigo])


GO
ALTER TABLE [dbo].[FornecedorHistorico] CHECK CONSTRAINT [FK_FornecedorHistorico_StatusPublicacao]

