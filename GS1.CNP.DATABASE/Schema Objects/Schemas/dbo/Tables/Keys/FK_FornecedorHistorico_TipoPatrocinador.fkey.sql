﻿ALTER TABLE [dbo].[FornecedorHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FornecedorHistorico_TipoPatrocinador] FOREIGN KEY([CodigoTipoPatrocinador])
REFERENCES [dbo].[TipoPatrocinador] ([Codigo])


GO
ALTER TABLE [dbo].[FornecedorHistorico] CHECK CONSTRAINT [FK_FornecedorHistorico_TipoPatrocinador]

