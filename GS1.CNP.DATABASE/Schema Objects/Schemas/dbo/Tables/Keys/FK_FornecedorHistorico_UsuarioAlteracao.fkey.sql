﻿ALTER TABLE [dbo].[FornecedorHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FornecedorHistorico_UsuarioAlteracao] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[FornecedorHistorico] CHECK CONSTRAINT [FK_FornecedorHistorico_UsuarioAlteracao]

