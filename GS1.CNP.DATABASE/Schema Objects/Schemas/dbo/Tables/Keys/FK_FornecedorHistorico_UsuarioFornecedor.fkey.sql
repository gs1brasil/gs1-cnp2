﻿ALTER TABLE [dbo].[FornecedorHistorico]  WITH CHECK ADD  CONSTRAINT [FK_FornecedorHistorico_UsuarioFornecedor] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[FornecedorHistorico] CHECK CONSTRAINT [FK_FornecedorHistorico_UsuarioFornecedor]

