﻿ALTER TABLE [dbo].[Fornecedor]  WITH CHECK ADD  CONSTRAINT [FK_Fornecedor_TipoAssociado] FOREIGN KEY([CodigoTipoAssociado])
REFERENCES [dbo].[TipoAssociado] ([Codigo])


GO
ALTER TABLE [dbo].[Fornecedor] CHECK CONSTRAINT [FK_Fornecedor_TipoAssociado]

