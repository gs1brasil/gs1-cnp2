﻿ALTER TABLE [dbo].[Funcionalidade]  WITH CHECK ADD  CONSTRAINT [FK_Funcionalidade_Modulo] FOREIGN KEY([CodigoModulo])
REFERENCES [dbo].[Modulo] ([Codigo])


GO
ALTER TABLE [dbo].[Funcionalidade] CHECK CONSTRAINT [FK_Funcionalidade_Modulo]

