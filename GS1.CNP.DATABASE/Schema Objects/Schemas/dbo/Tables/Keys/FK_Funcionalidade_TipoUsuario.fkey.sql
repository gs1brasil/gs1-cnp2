﻿ALTER TABLE [dbo].[Funcionalidade]  WITH CHECK ADD  CONSTRAINT [FK_Funcionalidade_TipoUsuario] FOREIGN KEY([CodigoTipoUsuario])
REFERENCES [dbo].[TipoUsuario] ([Codigo])


GO
ALTER TABLE [dbo].[Funcionalidade] CHECK CONSTRAINT [FK_Funcionalidade_TipoUsuario]

