﻿ALTER TABLE [dbo].[ParametroHistorico]  WITH CHECK ADD  CONSTRAINT [FK_ParametroHistorico_Parametro] FOREIGN KEY([CodigoParametro])
REFERENCES [dbo].[Parametro] ([Codigo])


GO
ALTER TABLE [dbo].[ParametroHistorico] CHECK CONSTRAINT [FK_ParametroHistorico_Parametro]

