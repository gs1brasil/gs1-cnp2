﻿ALTER TABLE [dbo].[ParametroHistorico]  WITH CHECK ADD  CONSTRAINT [FK_ParametroHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[ParametroHistorico] CHECK CONSTRAINT [FK_ParametroHistorico_Usuario]

