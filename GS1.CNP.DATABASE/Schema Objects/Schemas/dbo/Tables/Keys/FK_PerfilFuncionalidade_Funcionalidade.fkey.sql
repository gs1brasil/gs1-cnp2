﻿ALTER TABLE [dbo].[PerfilFuncionalidade]  WITH CHECK ADD  CONSTRAINT [FK_PerfilFuncionalidade_Funcionalidade] FOREIGN KEY([CodigoFuncionalidade])
REFERENCES [dbo].[Funcionalidade] ([Codigo])


GO
ALTER TABLE [dbo].[PerfilFuncionalidade] CHECK CONSTRAINT [FK_PerfilFuncionalidade_Funcionalidade]

