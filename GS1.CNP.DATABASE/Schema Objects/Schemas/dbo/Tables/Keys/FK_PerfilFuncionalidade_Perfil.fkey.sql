﻿ALTER TABLE [dbo].[PerfilFuncionalidade]  WITH CHECK ADD  CONSTRAINT [FK_PerfilFuncionalidade_Perfil] FOREIGN KEY([CodigoPerfil])
REFERENCES [dbo].[Perfil] ([Codigo])


GO
ALTER TABLE [dbo].[PerfilFuncionalidade] CHECK CONSTRAINT [FK_PerfilFuncionalidade_Perfil]

