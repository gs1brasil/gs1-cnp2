﻿ALTER TABLE [dbo].[PerfilFuncionalidade]  WITH CHECK ADD  CONSTRAINT [FK_PerfilFuncionalidade_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[PerfilFuncionalidade] CHECK CONSTRAINT [FK_PerfilFuncionalidade_Usuario]

