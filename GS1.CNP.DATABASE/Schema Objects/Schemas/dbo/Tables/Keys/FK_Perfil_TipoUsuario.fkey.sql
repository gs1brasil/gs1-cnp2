﻿ALTER TABLE [dbo].[Perfil]  WITH CHECK ADD  CONSTRAINT [FK_Perfil_TipoUsuario] FOREIGN KEY([CodigoTipoUsuario])
REFERENCES [dbo].[TipoUsuario] ([Codigo])


GO
ALTER TABLE [dbo].[Perfil] CHECK CONSTRAINT [FK_Perfil_TipoUsuario]

