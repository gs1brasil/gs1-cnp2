﻿ALTER TABLE [dbo].[TipoUsuarioStatusUsuario]  WITH CHECK ADD  CONSTRAINT [FK_TipoUsuarioStatusUsuario_StatusUsuario] FOREIGN KEY([CodigoStatusUsuario])
REFERENCES [dbo].[StatusUsuario] ([Codigo])


GO
ALTER TABLE [dbo].[TipoUsuarioStatusUsuario] CHECK CONSTRAINT [FK_TipoUsuarioStatusUsuario_StatusUsuario]

