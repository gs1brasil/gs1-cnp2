﻿ALTER TABLE [dbo].[TipoUsuarioStatusUsuario]  WITH CHECK ADD  CONSTRAINT [FK_TipoUsuarioStatusUsuario_TipoUsuario] FOREIGN KEY([CodigoTipoUsuario])
REFERENCES [dbo].[TipoUsuario] ([Codigo])


GO
ALTER TABLE [dbo].[TipoUsuarioStatusUsuario] CHECK CONSTRAINT [FK_TipoUsuarioStatusUsuario_TipoUsuario]

