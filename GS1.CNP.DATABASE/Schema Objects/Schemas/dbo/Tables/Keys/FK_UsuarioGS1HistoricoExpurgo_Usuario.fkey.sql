﻿ALTER TABLE [dbo].[UsuarioGS1HistoricoExpurgo]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioGS1HistoricoExpurgo_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioGS1HistoricoExpurgo] CHECK CONSTRAINT [FK_UsuarioGS1HistoricoExpurgo_Usuario]

