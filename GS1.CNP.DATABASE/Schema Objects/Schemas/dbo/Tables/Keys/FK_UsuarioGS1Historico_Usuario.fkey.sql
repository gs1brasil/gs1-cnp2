﻿ALTER TABLE [dbo].[UsuarioGS1Historico]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioGS1Historico_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioGS1Historico] CHECK CONSTRAINT [FK_UsuarioGS1Historico_Usuario]

