﻿ALTER TABLE [dbo].[UsuarioHistoricoExpurgo]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistoricoExpurgo_Perfil] FOREIGN KEY([CodigoPerfil])
REFERENCES [dbo].[Perfil] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistoricoExpurgo] CHECK CONSTRAINT [FK_UsuarioHistoricoExpurgo_Perfil]

