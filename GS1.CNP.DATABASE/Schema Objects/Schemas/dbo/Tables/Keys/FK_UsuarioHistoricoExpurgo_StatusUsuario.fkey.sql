﻿ALTER TABLE [dbo].[UsuarioHistoricoExpurgo]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistoricoExpurgo_StatusUsuario] FOREIGN KEY([CodigoStatusUsuario])
REFERENCES [dbo].[StatusUsuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistoricoExpurgo] CHECK CONSTRAINT [FK_UsuarioHistoricoExpurgo_StatusUsuario]

