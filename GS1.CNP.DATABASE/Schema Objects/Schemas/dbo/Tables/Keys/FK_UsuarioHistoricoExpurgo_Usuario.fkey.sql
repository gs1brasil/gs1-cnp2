﻿ALTER TABLE [dbo].[UsuarioHistoricoExpurgo]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistoricoExpurgo_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistoricoExpurgo] CHECK CONSTRAINT [FK_UsuarioHistoricoExpurgo_Usuario]

