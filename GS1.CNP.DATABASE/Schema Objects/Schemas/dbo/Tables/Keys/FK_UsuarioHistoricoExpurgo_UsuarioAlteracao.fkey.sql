﻿ALTER TABLE [dbo].[UsuarioHistoricoExpurgo]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistoricoExpurgo_UsuarioAlteracao] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistoricoExpurgo] CHECK CONSTRAINT [FK_UsuarioHistoricoExpurgo_UsuarioAlteracao]

