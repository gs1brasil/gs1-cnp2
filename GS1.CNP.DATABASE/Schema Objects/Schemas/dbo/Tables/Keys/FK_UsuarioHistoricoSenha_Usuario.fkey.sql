﻿ALTER TABLE [dbo].[UsuarioHistoricoSenha]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistoricoSenha_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistoricoSenha] CHECK CONSTRAINT [FK_UsuarioHistoricoSenha_Usuario]

