﻿ALTER TABLE [dbo].[UsuarioHistorico]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistorico_Perfil] FOREIGN KEY([CodigoPerfil])
REFERENCES [dbo].[Perfil] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistorico] CHECK CONSTRAINT [FK_UsuarioHistorico_Perfil]

