﻿ALTER TABLE [dbo].[UsuarioHistorico]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistorico_StatusUsuario] FOREIGN KEY([CodigoStatusUsuario])
REFERENCES [dbo].[StatusUsuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistorico] CHECK CONSTRAINT [FK_UsuarioHistorico_StatusUsuario]

