﻿ALTER TABLE [dbo].[UsuarioHistorico]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistorico_Usuario] FOREIGN KEY([CodigoUsuario])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistorico] CHECK CONSTRAINT [FK_UsuarioHistorico_Usuario]

