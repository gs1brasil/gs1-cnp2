﻿ALTER TABLE [dbo].[UsuarioHistorico]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistorico_UsuarioAlteracao] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])


GO
ALTER TABLE [dbo].[UsuarioHistorico] CHECK CONSTRAINT [FK_UsuarioHistorico_UsuarioAlteracao]

