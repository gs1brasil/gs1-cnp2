﻿ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Fornecedor] FOREIGN KEY([CodigoUsuarioFornecedor])
REFERENCES [dbo].[Fornecedor] ([CodigoUsuario])


GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Fornecedor]

