﻿ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_StatusUsuario] FOREIGN KEY([CodigoStatusUsuario])
REFERENCES [dbo].[StatusUsuario] ([Codigo])


GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_StatusUsuario]

