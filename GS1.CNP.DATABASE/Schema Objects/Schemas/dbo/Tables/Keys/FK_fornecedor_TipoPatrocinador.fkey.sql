﻿ALTER TABLE [dbo].[Fornecedor]  WITH CHECK ADD  CONSTRAINT [FK_fornecedor_TipoPatrocinador] FOREIGN KEY([CodigoTipoPatrocinador])
REFERENCES [dbo].[TipoPatrocinador] ([Codigo])


GO
ALTER TABLE [dbo].[Fornecedor] CHECK CONSTRAINT [FK_fornecedor_TipoPatrocinador]

