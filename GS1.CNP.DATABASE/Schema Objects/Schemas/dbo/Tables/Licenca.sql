IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Licenca') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Licenca;

CREATE TABLE Licenca ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(500),
	Status int,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE Licenca ADD CONSTRAINT PK_Licenca 
	PRIMARY KEY CLUSTERED (Codigo);






