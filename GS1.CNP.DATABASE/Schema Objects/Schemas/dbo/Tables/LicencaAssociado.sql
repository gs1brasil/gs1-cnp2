IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LicencaAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LicencaAssociado;

CREATE TABLE LicencaAssociado ( 
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	Status int NOT NULL,
	DataAniversario datetime,
	DataAlteracao datetime
);

ALTER TABLE LicencaAssociado ADD CONSTRAINT PK_LicencaAssociado 
	PRIMARY KEY CLUSTERED (CodigoLicenca, CodigoAssociado);

ALTER TABLE LicencaAssociado ADD CONSTRAINT FK_LicencaAssociado_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE LicencaAssociado ADD CONSTRAINT FK_LicencaAssociado_Licenca 
	FOREIGN KEY (CodigoLicenca) REFERENCES Licenca (Codigo);

ALTER TABLE LicencaAssociado ADD CONSTRAINT FK_LicencaAssociado_StatusPrefixo 
	FOREIGN KEY (Status) REFERENCES StatusPrefixo (Codigo);