IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LicencaAssociadoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LicencaAssociadoHistorico;

CREATE TABLE LicencaAssociadoHistorico ( 
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	Status int NOT NULL,
	DataAniversario datetime,
	DataAlteracao datetime,
	DataHistorico datetime NOT NULL
);

ALTER TABLE LicencaAssociadoHistorico ADD CONSTRAINT PK_LicencaAssociadoHistorico 
	PRIMARY KEY CLUSTERED (CodigoLicenca, CodigoAssociado, DataHistorico);

ALTER TABLE LicencaAssociadoHistorico ADD CONSTRAINT FK_LicencaAssociadoHistorico_LicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado) REFERENCES LicencaAssociado (CodigoLicenca, CodigoAssociado);

