IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LicencaHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LicencaHistorico;

CREATE TABLE LicencaHistorico ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(500),
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE LicencaHistorico ADD CONSTRAINT PK_LicencaHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE LicencaHistorico ADD CONSTRAINT FK_LicencaHistorico_Licenca 
	FOREIGN KEY (Codigo) REFERENCES Licenca (Codigo);
