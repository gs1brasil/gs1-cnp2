IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LocalizacaoFisicaHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LocalizacaoFisicaHistorico;

CREATE TABLE [dbo].[LocalizacaoFisicaHistorico](
	[Codigo] [bigint] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](500) NULL,
	[Status] [int] NOT NULL,
	[Prefixo] [varchar](12) NOT NULL,
	[GLN] [bigint] NOT NULL,
	[NumeroItem] [decimal](16, 2) NOT NULL,
	[CodigoPapelGLN] [int] NOT NULL,
	[Endereco] [varchar](100) NOT NULL,
	[Numero] [varchar](50) NOT NULL,
	[Complemento] [varchar](100) NULL,
	[CEP] [varchar](20) NOT NULL,
	[Cidade] [varchar](255) NOT NULL,
	[Estado] [varchar](255) NOT NULL,
	[Pais] [varchar](255) NULL,
	[Bairro] [varchar](50) NOT NULL,
	[NomeImagem] [varchar](255) NULL,
	[Observacao] [varchar](200) NULL,
	[DataAlteracao] [datetime] NOT NULL,
	[CodigoUsuarioAlteracao] [bigint] NOT NULL,
	[IndicadorPrincipal] [varchar](1) NULL,
	[DataCancelamento] [datetime] NULL,
	[DataSuspensao] [datetime] NULL,
	[DataReutilizacao] [datetime] NULL,
	[CodigoAssociado] [bigint] NOT NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[Telefone] [varchar](50) NULL,
	[Email] [varchar](255) NULL,
	[DataHistorico] [datetime] NOT NULL,
 CONSTRAINT [PK_LocalizacaoFisicaHistorico] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC,
	[DataHistorico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisicaHistorico_Associado] FOREIGN KEY([CodigoAssociado])
REFERENCES [dbo].[Associado] ([Codigo])
GO
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico] CHECK CONSTRAINT [FK_LocalizacaoFisicaHistorico_Associado]
GO
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisicaHistorico_LocalizacaoFisica] FOREIGN KEY([Codigo])
REFERENCES [dbo].[LocalizacaoFisica] ([Codigo])
GO
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico] CHECK CONSTRAINT [FK_LocalizacaoFisicaHistorico_LocalizacaoFisica]
GO
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisicaHistorico_PapelGLN] FOREIGN KEY([CodigoPapelGLN])
REFERENCES [dbo].[PapelGLN] ([Codigo])
GO
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico] CHECK CONSTRAINT [FK_LocalizacaoFisicaHistorico_PapelGLN]
GO
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico]  WITH CHECK ADD  CONSTRAINT [FK_LocalizacaoFisicaHistorico_Usuario] FOREIGN KEY([CodigoUsuarioAlteracao])
REFERENCES [dbo].[Usuario] ([Codigo])
GO
ALTER TABLE [dbo].[LocalizacaoFisicaHistorico] CHECK CONSTRAINT [FK_LocalizacaoFisicaHistorico_Usuario]
GO
