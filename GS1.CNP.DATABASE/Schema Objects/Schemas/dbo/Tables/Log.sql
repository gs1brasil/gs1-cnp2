IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Log') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Log;

CREATE TABLE Log ( 
	Codigo bigint identity(1,1)  NOT NULL,
	DataAlteracao datetime,
	CodigoUsuario bigint,
	Classe varchar(255),
	Metodo varchar(500),
	Requisicao text,
	CodigoEntidade bigint,
	URL varchar(500)
);

ALTER TABLE Log ADD CONSTRAINT PK_Log 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Log ADD CONSTRAINT FK_Log_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;









