IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LogErroExpurgo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LogErroExpurgo;

CREATE TABLE LogErroExpurgo ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Mensagem varchar(1000) NOT NULL,
	Descricao text,
	DataLog datetime,
	CodigoUsuario bigint,
	URL varchar(500),
	IP varchar(50),
	Navegador varchar(500),
	VersaoNavegador varchar(500),
	Email varchar(255),
	Requisicao text,
	Servidor varchar(500),
	DataExpurgo datetime NOT NULL
);

ALTER TABLE LogErroExpurgo ADD CONSTRAINT PK_LogErroExpurgo 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LogErroExpurgo ADD CONSTRAINT FK_LogErroExpurgo_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
