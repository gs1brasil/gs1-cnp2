IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LogExpurgo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LogExpurgo;

CREATE TABLE LogExpurgo ( 
	Codigo bigint NOT NULL,
	DataAlteracao datetime,
	CodigoUsuarioAlteracao bigint,
	Classe varchar(255),
	Metodo varchar(500),
	Requisicao text,
	CodigoEntidade bigint,
	URL bigint,
	DataExpurgo datetime NOT NULL
);

ALTER TABLE LogExpurgo ADD CONSTRAINT PK_LogExpurgo 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LogExpurgo ADD CONSTRAINT FK_LogExpurgo_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);
