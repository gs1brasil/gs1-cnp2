IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LogGepirRouter') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LogGepirRouter;

CREATE TABLE LogGepirRouter ( 
	Codigo int identity(1,1)  NOT NULL,
	CodigoUsuario bigint NULL,
	CodigoAssociado bigint NULL,
	DataConsulta datetime NOT NULL,
	Metodo varchar(50) NULL,
	Campos text NULL,
	Valores text NULL
);

ALTER TABLE LogGepirRouter ADD CONSTRAINT PK_LogGepirRouter
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LogGepirRouter ADD CONSTRAINT FK_LogGepirRouter_Associado
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);