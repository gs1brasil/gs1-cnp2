IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LogGerencialInbar') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LogGerencialInbar;

CREATE TABLE LogGerencialInbar ( 
	Codigo int identity(1,1)  NOT NULL,
	CodigoProduto bigint NULL,
	GlobalTradeItemNumber bigint NOT NULL,
	DataConsulta datetime NOT NULL,
	CompartilhaDadosProduto varchar(1) NULL,
	CompartilhaDadosAssociado varchar(1) NULL
);

ALTER TABLE LogGerencialInbar ADD CONSTRAINT PK_LogGerencialInbar
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LogGerencialInbar ADD CONSTRAINT FK_LogGerencialInbar_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);