SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LogWsProdutoServiceErro](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[DataAlteracao] [datetime] NULL,
	[Mensagem] [varchar](1000) NOT NULL,
	[Descricao] [text] NULL,
	[Requisicao] [text] NULL,	
	[URL] [varchar](500) NULL,	
	[IP] [varchar](50) NULL,
	[Servidor] [varchar](500) NULL,	
		
 CONSTRAINT [PK_LogWsProdutoServiceErro] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO