IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Lote') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Lote;

CREATE TABLE Lote ( 
	Codigo bigint identity(1,1)  NOT NULL,
	CodigoStatusLote int NOT NULL,
	CodigoProduto bigint NULL,
	Nrlote varchar(20) NULL,
	DataProcessamento Datetime NULL,
	DataVencimento Datetime NULL,
	TipoProcessamentoBizStep varchar(500) NULL,
	StatusProdutoDispositions varchar(500) NULL,
	InformacoesAdicionais varchar(500) NULL,
	DataAlteracao Datetime NULL,
	CodigoUsuarioAlteracao bigint NULL,
	CodigoAssociado bigint NULL,
	CodigoBizStep int NULL,
	CodigoDispositions int NULL,
	QuantidadeItens int NULL
);

ALTER TABLE Lote ADD CONSTRAINT PK_Lote
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Lote ADD CONSTRAINT FK_Lote_Produto
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE Lote ADD CONSTRAINT FK_Lote_Usuario
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);
	
ALTER TABLE Lote ADD CONSTRAINT FK_Lote_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);
	
ALTER TABLE Lote ADD CONSTRAINT FK_Lote_BizStep
	FOREIGN KEY (CodigoBizStep) REFERENCES BizStep (Codigo);
	
ALTER TABLE Lote ADD CONSTRAINT FK_Lote_Dispositions
	FOREIGN KEY (CodigoDispositions) REFERENCES Dispositions (Codigo);

ALTER TABLE Lote ADD CONSTRAINT FK_Lote_StatusLote
	FOREIGN KEY (CodigoStatusLote) REFERENCES StatusLote (Codigo);
