IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LoteHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LoteHistorico;

CREATE TABLE LoteHistorico ( 
	CodigoLote bigint NOT NULL,
	CodigoStatusLote int NOT NULL,
	CodigoProduto bigint NULL,
	Nrlote varchar(20) NULL,
	DataProcessamento Datetime NULL,
	DataVencimento Datetime NULL,
	TipoProcessamentoBizStep varchar(500) NULL,
	StatusProdutoDispositions varchar(500) NULL,
	InformacoesAdicionais varchar(500) NULL,
	DataAlteracao Datetime NULL,
	CodigoUsuarioAlteracao bigint NULL, 
	CodigoAssociado bigint NULL,
	CodigoBizStep int NULL,
	CodigoDispositions int NULL,
	QuantidadeItens int NULL, 
	DataHistorico Datetime NOT NULL
);

ALTER TABLE LoteHistorico ADD CONSTRAINT PK_LoteHistorico
	PRIMARY KEY CLUSTERED (CodigoLote, DataHistorico);

ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Lote
	FOREIGN KEY (CodigoLote) REFERENCES Lote (Codigo);	
	
ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Produto
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Usuario
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);
	
ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_BizStep
	FOREIGN KEY (CodigoBizStep) REFERENCES BizStep (Codigo);
	
ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_Dispositions
	FOREIGN KEY (CodigoDispositions) REFERENCES Dispositions (Codigo);
	
ALTER TABLE LoteHistorico ADD CONSTRAINT FK_LoteHistorico_StatusLote
	FOREIGN KEY (CodigoStatusLote) REFERENCES StatusLote (Codigo);