IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LoteURL') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LoteURL;

CREATE TABLE LoteURL ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Nome varchar(255) NOT NULL,
	URL varchar(max) NOT NULL,
	Status int NOT NULL,
	CodigoLote bigint NOT NULL
);

ALTER TABLE LoteURL ADD CONSTRAINT PK_LoteURL
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE LoteURL ADD CONSTRAINT FK_LoteURL_Lote
	FOREIGN KEY (CodigoLote) REFERENCES Lote (Codigo);
