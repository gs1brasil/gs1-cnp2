IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('LoteURLHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE LoteURLHistorico;

CREATE TABLE LoteURLHistorico ( 
	Codigo bigint NOT NULL,
	Nome varchar(255) NOT NULL,
	URL varchar(max) NOT NULL,
	Status int NOT NULL,
	CodigoLote bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE LoteURLHistorico ADD CONSTRAINT PK_LoteURLHistorico 
	PRIMARY KEY CLUSTERED (DataHistorico);

ALTER TABLE LoteURLHistorico ADD CONSTRAINT FK_LoteURLHistorico_Lote
	FOREIGN KEY (CodigoLote) REFERENCES Lote (Codigo);
