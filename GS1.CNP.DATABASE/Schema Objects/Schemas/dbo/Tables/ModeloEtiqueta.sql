USE [db_gs1CNP_E5]
GO
/****** Object:  Table [dbo].[ModeloEtiqueta]    Script Date: 12/02/2016 17:23:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ModeloEtiqueta](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NOT NULL,
	[CodigoFabricante] [int] NULL,
	[Largura] [decimal](10, 2) NULL,
	[Altura] [decimal](10, 2) NULL,
	[TamanhoFolha] [varchar](50) NULL,
	[QuantidadeColunas] [int] NULL,
	[QuantidadeLinhas] [int] NULL,
	[MargemSuperior] [decimal](10, 2) NULL,
	[MargemInferior] [decimal](10, 2) NULL,
	[MargemEsquerda] [decimal](10, 2) NULL,
	[MargemDireita] [decimal](10, 2) NULL,
	[EspacamentoVertical] [decimal](10, 2) NULL,
	[EspacamentoHorizontal] [decimal](10, 2) NULL,
 CONSTRAINT [PK_ModeloEtiqueta] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ModeloEtiqueta]  WITH CHECK ADD  CONSTRAINT [FK_ModelEtiqueta_Fabricante] FOREIGN KEY([CodigoFabricante])
REFERENCES [dbo].[Fabricante] ([Codigo])
GO
ALTER TABLE [dbo].[ModeloEtiqueta] CHECK CONSTRAINT [FK_ModelEtiqueta_Fabricante]
GO
