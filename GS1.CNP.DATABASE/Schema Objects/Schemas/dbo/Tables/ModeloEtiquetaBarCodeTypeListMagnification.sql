IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ModeloEtiquetaBarCodeTypeListMagnification') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ModeloEtiquetaBarCodeTypeListMagnification;

CREATE TABLE ModeloEtiquetaBarCodeTypeListMagnification ( 
	CodigoModeloEtiqueta int NOT NULL,
	CodigoBarCodeTypeList int NOT NULL,
	CodigoBarCodeTypeListMagnification int NOT NULL
);

ALTER TABLE ModeloEtiquetaBarCodeTypeListMagnification ADD CONSTRAINT PK_ModeloEtiquetaBarCodeTypeListMagnification 
	PRIMARY KEY CLUSTERED (CodigoModeloEtiqueta, CodigoBarCodeTypeList);

ALTER TABLE ModeloEtiquetaBarCodeTypeListMagnification ADD CONSTRAINT FK_ModeloEtiquetaBarCodeTypeListMagnification_BarCodeTypeList 
	FOREIGN KEY (CodigoBarCodeTypeList) REFERENCES BarCodeTypeList (Codigo);

ALTER TABLE ModeloEtiquetaBarCodeTypeListMagnification ADD CONSTRAINT FK_ModeloEtiquetaBarCodeTypeListMagnification_BarCodeTypeListMagnification 
	FOREIGN KEY (CodigoBarCodeTypeListMagnification) REFERENCES BarCodeTypeListMagnification (Codigo);

ALTER TABLE ModeloEtiquetaBarCodeTypeListMagnification ADD CONSTRAINT FK_ModeloEtiquetaBarCodeTypeListMagnification_ModeloEtiqueta 
	FOREIGN KEY (CodigoModeloEtiqueta) REFERENCES ModeloEtiqueta (Codigo);
