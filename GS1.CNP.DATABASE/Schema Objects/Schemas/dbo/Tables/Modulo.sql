IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Modulo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Modulo;

CREATE TABLE Modulo ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int,
	Icone varchar(100),
	Ordem int,
	Url varchar(255)
);

ALTER TABLE Modulo ADD CONSTRAINT PK_Modulo 
	PRIMARY KEY CLUSTERED (Codigo);
