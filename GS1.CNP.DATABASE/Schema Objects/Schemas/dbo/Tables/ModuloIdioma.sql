USE [db_gs1CNP_E5]
GO
/****** Object:  Table [dbo].[ModuloIdioma]    Script Date: 12/02/2016 17:23:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ModuloIdioma](
	[CodigoModulo] [int] NOT NULL,
	[CodigoIdioma] [int] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_ModuloIdioma] PRIMARY KEY CLUSTERED 
(
	[CodigoModulo] ASC,
	[CodigoIdioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ModuloIdioma]  WITH CHECK ADD  CONSTRAINT [FK_ModuloIdioma_Idioma] FOREIGN KEY([CodigoIdioma])
REFERENCES [dbo].[Idioma] ([Codigo])
GO
ALTER TABLE [dbo].[ModuloIdioma] CHECK CONSTRAINT [FK_ModuloIdioma_Idioma]
GO
ALTER TABLE [dbo].[ModuloIdioma]  WITH CHECK ADD  CONSTRAINT [FK_ModuloIdioma_Modulo] FOREIGN KEY([CodigoModulo])
REFERENCES [dbo].[Modulo] ([Codigo])
GO
ALTER TABLE [dbo].[ModuloIdioma] CHECK CONSTRAINT [FK_ModuloIdioma_Modulo]
GO
