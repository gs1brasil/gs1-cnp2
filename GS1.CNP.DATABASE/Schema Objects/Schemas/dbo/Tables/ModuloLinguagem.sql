IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ModuloLinguagem') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ModuloLinguagem;

CREATE TABLE ModuloLinguagem ( 
	CodigoModulo int NOT NULL,
	CodigoIdioma int NOT NULL,
	Nome varbinary(50) NOT NULL,
	Descricao varchar(255),
	Status int
);

ALTER TABLE ModuloLinguagem ADD CONSTRAINT PK_ModuloLinguagem 
	PRIMARY KEY CLUSTERED (CodigoModulo, CodigoIdioma);

ALTER TABLE ModuloLinguagem ADD CONSTRAINT FK_ModuloLinguagem_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);

ALTER TABLE ModuloLinguagem ADD CONSTRAINT FK_ModuloLinguagem_Modulo 
	FOREIGN KEY (CodigoModulo) REFERENCES Modulo (Codigo);
