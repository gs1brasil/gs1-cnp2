IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('NCM') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE NCM;

CREATE TABLE NCM ( 
	Codigo bigint NOT NULL,
	NCM varchar(10) NOT NULL,
	Descricao varchar(5000) NOT NULL,
	Status int
);

ALTER TABLE NCM ADD CONSTRAINT PK_NCM 
	PRIMARY KEY CLUSTERED (Codigo);
