IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PackagingTypeCode') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PackagingTypeCode;

CREATE TABLE PackagingTypeCode ( 
	Codigo int NOT NULL,
	CodeValue varchar(3) NOT NULL,
	Name varchar(300) NOT NULL,
	Definition text,
	Status int NOT NULL
);

ALTER TABLE PackagingTypeCode ADD CONSTRAINT PK_PackagingTypeCode 
	PRIMARY KEY CLUSTERED (Codigo);






