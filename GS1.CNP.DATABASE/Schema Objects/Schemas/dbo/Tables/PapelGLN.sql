IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PapelGLN') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PapelGLN;

CREATE TABLE PapelGLN ( 
	Codigo int identity(1,1)  NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE PapelGLN ADD CONSTRAINT PK_PapelGLN 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE PapelGLN ADD CONSTRAINT FK_PapelGLN_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;







