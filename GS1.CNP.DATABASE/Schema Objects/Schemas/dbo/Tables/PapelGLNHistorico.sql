IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PapelGLNHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PapelGLNHistorico;

CREATE TABLE PapelGLNHistorico ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE PapelGLNHistorico ADD CONSTRAINT PK_PapelGLNHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE PapelGLNHistorico ADD CONSTRAINT FK_PapelGLNHistorico_PapelGLN 
	FOREIGN KEY (Codigo) REFERENCES PapelGLN (Codigo);

ALTER TABLE PapelGLNHistorico ADD CONSTRAINT FK_PapelGLNHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);








