IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Parametro') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Parametro;

CREATE TABLE Parametro ( 
	Codigo int identity(1,1)  NOT NULL,
	Chave varchar(50) NOT NULL,
	Valor varchar(500),
	Status int NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE Parametro ADD CONSTRAINT PK_Parametro 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Parametro ADD CONSTRAINT FK_Parametro_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
