IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ParametroHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ParametroHistorico;

CREATE TABLE ParametroHistorico ( 
	CodigoParametro int NOT NULL,
	Chave varchar(50) NOT NULL,
	Valor varchar(500),
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE ParametroHistorico ADD CONSTRAINT PK_ParametroHistorico 
	PRIMARY KEY CLUSTERED (CodigoParametro, DataHistorico);

ALTER TABLE ParametroHistorico ADD CONSTRAINT FK_ParametroHistorico_Parametro 
	FOREIGN KEY (CodigoParametro) REFERENCES Parametro (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE ParametroHistorico ADD CONSTRAINT FK_ParametroHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

