IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Perfil') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Perfil;

CREATE TABLE Perfil ( 
	Codigo int identity(1,1)  NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	CodigoTipoUsuario int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE Perfil ADD CONSTRAINT PK_Perfil 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Perfil ADD CONSTRAINT FK_Perfil_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Perfil ADD CONSTRAINT FK_Perfil_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

