IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PerfilFuncionalidade') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PerfilFuncionalidade;

CREATE TABLE PerfilFuncionalidade ( 
	CodigoPerfil int NOT NULL,
	CodigoFuncionalidade int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE PerfilFuncionalidade ADD CONSTRAINT PK_PerfilFuncionalidade 
	PRIMARY KEY CLUSTERED (CodigoPerfil, CodigoFuncionalidade);

ALTER TABLE PerfilFuncionalidade ADD CONSTRAINT FK_PerfilFuncionalidade_Funcionalidade 
	FOREIGN KEY (CodigoFuncionalidade) REFERENCES Funcionalidade (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PerfilFuncionalidade ADD CONSTRAINT FK_PerfilFuncionalidade_Perfil 
	FOREIGN KEY (CodigoPerfil) REFERENCES Perfil (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE PerfilFuncionalidade ADD CONSTRAINT FK_PerfilFuncionalidade_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
