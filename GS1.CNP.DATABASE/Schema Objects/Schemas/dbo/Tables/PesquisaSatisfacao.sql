IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PesquisaSatisfacao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PesquisaSatisfacao;

CREATE TABLE PesquisaSatisfacao ( 
	Codigo int identity(1,1) NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	Inicio datetime NOT NULL,
	Fim datetime NOT NULL,
	Url varchar(max) NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	CodigoPublicoAlvo int NOT NULL, 
	DataAlteracao datetime NOT NULL
);

ALTER TABLE PesquisaSatisfacao ADD CONSTRAINT PK_PesquisaSatisfacao 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE PesquisaSatisfacao ADD CONSTRAINT FK_PesquisaSatisfacao_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE PesquisaSatisfacao ADD CONSTRAINT FK_PesquisaSatisfacao_PublicoAlvo
	FOREIGN KEY (CodigoPublicoAlvo) REFERENCES PublicoAlvo (Codigo);