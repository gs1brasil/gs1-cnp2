IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PesquisaSatisfacaoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PesquisaSatisfacaoHistorico;

CREATE TABLE PesquisaSatisfacaoHistorico ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	Inicio datetime NOT NULL,
	Fim datetime NOT NULL,
	Url varchar(max) NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	CodigoPublicoAlvo int NOT NULL, 
	DataAlteracao datetime NOT NULL, 
	DataHistorico datetime NOT NULL 
);

ALTER TABLE PesquisaSatisfacaoHistorico ADD CONSTRAINT PK_PesquisaSatisfacaoHistorico
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);
	
ALTER TABLE PesquisaSatisfacaoHistorico ADD CONSTRAINT FK_PesquisaSatisfacaoHistorico_PesquisaSatisfacao
	FOREIGN KEY (Codigo) REFERENCES PesquisaSatisfacao (Codigo);

ALTER TABLE PesquisaSatisfacaoHistorico ADD CONSTRAINT FK_PesquisaSatisfacaoHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE PesquisaSatisfacaoHistorico ADD CONSTRAINT FK_PesquisaSatisfacaoHistorico_PublicoAlvo
	FOREIGN KEY (CodigoPublicoAlvo) REFERENCES PublicoAlvo (Codigo);