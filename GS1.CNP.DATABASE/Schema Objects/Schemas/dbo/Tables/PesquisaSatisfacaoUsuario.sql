IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PesquisaSatisfacaoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PesquisaSatisfacaoUsuario;

CREATE TABLE PesquisaSatisfacaoUsuario ( 
	Codigo int identity (1,1) NOT NULL,
	CodigoPesquisaSatisfacao int NOT NULL,
	CodigoUsuario bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoStatusPesquisaSatisfacaoUsuario int NOT NULL
);

ALTER TABLE PesquisaSatisfacaoUsuario ADD CONSTRAINT PK_PesquisaSatisfacaoUsuario
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE PesquisaSatisfacaoUsuario ADD CONSTRAINT FK_PesquisaSatisfacaoUsuario_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo);

ALTER TABLE PesquisaSatisfacaoUsuario ADD CONSTRAINT FK_PesquisaSatisfacaoUsuario_PesquisaSatisfacao
	FOREIGN KEY (CodigoPesquisaSatisfacao) REFERENCES PesquisaSatisfacao (Codigo);
	
ALTER TABLE PesquisaSatisfacaoUsuario ADD CONSTRAINT FK_PesquisaSatisfacaoUsuario_StatusPesquisaSatisfacaoUsuario
	FOREIGN KEY (CodigoStatusPesquisaSatisfacaoUsuario) REFERENCES StatusPesquisaSatisfacaoUsuario (Codigo);