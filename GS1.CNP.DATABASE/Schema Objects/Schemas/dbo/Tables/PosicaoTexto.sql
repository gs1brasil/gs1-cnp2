IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PosicaoTexto') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PosicaoTexto;

CREATE TABLE PosicaoTexto ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE PosicaoTexto ADD CONSTRAINT PK_PosicaoTexto 
	PRIMARY KEY CLUSTERED (Codigo);
