IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PrefixoLicencaAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PrefixoLicencaAssociado;

CREATE TABLE PrefixoLicencaAssociado ( 
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	NumeroPrefixo bigint NOT NULL,
	CodigoStatusPrefixo int,
	DataAlteracao datetime
);

ALTER TABLE PrefixoLicencaAssociado ADD CONSTRAINT PK_PrefixoLicencaUsuario 
	PRIMARY KEY CLUSTERED (CodigoLicenca, CodigoAssociado, NumeroPrefixo);

ALTER TABLE PrefixoLicencaAssociado ADD CONSTRAINT FK_PrefixoLicencaAssociado_StatusPrefixo 
	FOREIGN KEY (CodigoStatusPrefixo) REFERENCES StatusPrefixo (Codigo);

ALTER TABLE PrefixoLicencaAssociado ADD CONSTRAINT FK_PrefixoLicencaUsuario_LicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado) REFERENCES LicencaAssociado (CodigoLicenca, CodigoAssociado);






