IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PrefixoLicencaAssociadoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PrefixoLicencaAssociadoHistorico;

CREATE TABLE PrefixoLicencaAssociadoHistorico ( 
	CodigoLicenca int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	NumeroPrefixo bigint NOT NULL,
	Status int,
	DataAlteracao datetime,
	DataHistorico datetime NOT NULL
);

ALTER TABLE PrefixoLicencaAssociadoHistorico ADD CONSTRAINT PK_PrefixoLicencaAssociadoHistorico 
	PRIMARY KEY CLUSTERED (CodigoLicenca, CodigoAssociado, NumeroPrefixo, DataHistorico);

ALTER TABLE PrefixoLicencaAssociadoHistorico ADD CONSTRAINT FK_PrefixoLicencaAssociadoHistorico_LicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado) REFERENCES LicencaAssociado (CodigoLicenca, CodigoAssociado);

ALTER TABLE PrefixoLicencaAssociadoHistorico ADD CONSTRAINT FK_PrefixoLicencaAssociadoHistorico_PrefixoLicencaAssociado 
	FOREIGN KEY (CodigoLicenca, CodigoAssociado, NumeroPrefixo) REFERENCES PrefixoLicencaAssociado (CodigoLicenca, CodigoAssociado, NumeroPrefixo);







