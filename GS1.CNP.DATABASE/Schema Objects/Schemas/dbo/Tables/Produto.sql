IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Produto') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Produto;

CREATE TABLE Produto ( 
	CodigoProduto bigint identity(1,1)  NOT NULL,
	globalTradeItemNumber bigint,
	productDescription varchar(300) NOT NULL,
	CodigoTipoGTIN int NOT NULL,
	NrPrefixo bigint,
	CodItem varchar(5),
	VarianteLogistica int,
	CodigoAssociado bigint NOT NULL,
	CodigoUsuarioCriacao bigint NOT NULL,
	CodigoUsuarioAlteracao bigint,
	CodigoUsuarioExclusao bigint,
	tradeItemCountryOfOrigin int,
	CodigoIdioma int,
	CodigoStatusGTIN int NOT NULL,
	CodeSegment varchar(10),
	CodeFamily varchar(10),
	CodeClass varchar(10),
	CodeBrick varchar(10),
	countryCode varchar(3),
	CodeBrickAttribute bigint,
	IndicadorCompartilhaDados varchar(1),
	Observacoes text,
	Estado varchar(50),
	informationProvider bigint,
	brandName varchar(50),
	DataInclusao datetime NOT NULL,
	DataAlteracao datetime,
	DataSuspensao datetime,
	DataReativacao datetime,
	DataCancelamento datetime,
	DataReutilizacao datetime,
	modelNumber varchar(70),
	importClassificationType varchar(20),
	importClassificationValue varchar(100),
	alternateItemIdentificationId varchar(50),
	alternateItemIdentificationAgency int,
	minimumTradeItemLifespanFromTimeOfProduction int,
	startAvailabilityDateTime datetime,
	endAvailabilityDateTime datetime,
	depth decimal(5,2),
	depthMeasurementUnitCode varchar(3),
	height decimal(5,2),
	heightMeasurementUnitCode varchar(3),
	width decimal(5,2),
	widthMeasurementUnitCode varchar(3),
	netContent decimal(5,2),
	netContentMeasurementUnitCode varchar(3),
	grossWeight decimal(5,2),
	grossWeightMeasurementUnitCode varchar(3),
	netWeight decimal(5,2),
	netWeightMeasurementUnitCode varchar(3),
	CodigoTipoProduto int,
	isTradeItemABaseUnit int,
	isTradeItemAConsumerUnit int,
	isTradeItemAModel int,
	isTradeItemAnInvoiceUnit varchar(1),
	packagingTypeCode varchar(25),
	PalletTypeCode varchar(3),
	totalQuantityOfNextLowerLevelTradeItem varchar(10),
	StackingFactor int,
	quantityOfTradeItemContainedInACompleteLayer int,
	quantityOfTradeItemsPerPalletLayer int,
	quantityOfCompleteLayersContainedInATradeItem int,
	quantityOfLayersPerPallet int,
	CodigoProdutoOrigem bigint,
	deliveryToDistributionCenterTemperatureMinimum decimal(5,2),
	storageHandlingTempMinimumUOM varchar(3),
	storageHandlingTemperatureMaximum decimal(5,2),
	storageHandlingTemperatureMaximumunitOfMeasure varchar(3),
	isDangerousSubstanceIndicated int,
	ipiPerc decimal(5,2),
	isTradeItemAnOrderableUnit int,
	isTradeItemADespatchUnit int,
	orderSizingFactor varchar(3),
	orderQuantityMultiple int,
	orderQuantityMinimum int,
	barcodeCertified int,
	dataQualityCertified int,
	dataQualityCertifiedAgencyCode int,
	allGDSNAttributes int,
	CanalComunicacaoDados varchar(1),
	ProprioDonoInformacao int,
	ValidadoDonoDaInformacao int,
	IndicadorGDSN int
);

CREATE INDEX IDX_globalTradeItemNumber
ON Produto (globalTradeItemNumber ASC);

ALTER TABLE Produto ADD CONSTRAINT PK_Produto 
	PRIMARY KEY CLUSTERED (CodigoProduto);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_TipoGTIN 
	FOREIGN KEY (CodigoTipoGTIN) REFERENCES TipoGTIN (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_Produto 
	FOREIGN KEY (CodigoProdutoOrigem) REFERENCES Produto (CodigoProduto);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_StatusGTIN 
	FOREIGN KEY (CodigoStatusGTIN) REFERENCES StatusGTIN (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_Usuario 
	FOREIGN KEY (CodigoUsuarioCriacao) REFERENCES Usuario (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_UsuarioAlteracao 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE Produto ADD CONSTRAINT FK_Produto_UsuarioExclusao 
	FOREIGN KEY (CodigoUsuarioExclusao) REFERENCES Usuario (Codigo);

ALTER TABLE PRODUTO ADD isTradeItemAService INT NULL
GO
ALTER TABLE PRODUTO ADD isTradeItemNonphysical INT NULL
GO
ALTER TABLE PRODUTO ADD isTradeItemRecalled INT NULL
GO

ALTER TABLE PRODUTO ADD additionalTradeItemDescription VARCHAR(500) NULL
GO

ALTER TABLE PRODUTO ADD predominantMaterial VARCHAR(50) NULL
GO

ALTER TABLE PRODUTO ADD contextIdentification VARCHAR(50) NULL
GO

ALTER TABLE PRODUTO ADD descriptionShort VARCHAR(35) NULL
GO


ALTER TABLE PRODUTO ADD orderingUnitOfMeasure VARCHAR(35) NULL
GO