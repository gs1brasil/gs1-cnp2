IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoBrickTypeValue') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoBrickTypeValue;

CREATE TABLE [dbo].[ProdutoBrickTypeValue](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[CodigoProduto] [bigint] NOT NULL,
	[CodeBrick] [varchar](10) NOT NULL,
	[CodeClass] [varchar](10) NOT NULL,
	[CodeFamily] [varchar](10) NOT NULL,
	[CodeSegment] [varchar](10) NOT NULL,
	[Idioma] [varchar](5) NOT NULL,
	[CodeType] [varchar](10) NOT NULL,
	[CodeValue] [varchar](10) NOT NULL
);

ALTER TABLE ProdutoBrickTypeValue ADD CONSTRAINT PK_ProdutoBrickTypeValue 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ProdutoBrickTypeValue ADD CONSTRAINT FK_ProdutoBrickTypeValue_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);
