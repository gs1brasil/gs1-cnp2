CREATE TABLE ProdutoDutyFeeTax (
	Codigo [bigint] IDENTITY(1,1) NOT NULL,
	CodigoProduto [bigint] NOT NULL,
	dutyFeeTaxAmount  VARCHAR(3) NULL,
	dutyFeeTaxBasis  VARCHAR(3) NULL,
	dutyFeeTaxCategoryCode  VARCHAR(200) NULL,
	dutyFeeTaxCountrySubdivisionCode  VARCHAR(5) NULL,
	dutyFeeTaxExemptPartyRoleCode  VARCHAR(200) NULL,
	dutyFeeTaxRate decimal (10, 2) NULL,
	dutyFeeTaxReductionCriteriaDescription  VARCHAR(200) NULL,
	dutyFeeTaxTypeCode VARCHAR(80) NULL,
	dutyFeeTaxTypeDescription  VARCHAR(70) NULL,
CONSTRAINT [PK_ProdutoDutyFeeTax] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ProdutoDutyFeeTax]  WITH NOCHECK ADD  CONSTRAINT [FK_ProdutoDutyFeeTax_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[ProdutoDutyFeeTax] NOCHECK CONSTRAINT [FK_ProdutoDutyFeeTax_Produto]
GO