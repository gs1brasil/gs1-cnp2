IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoHierarquia') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoHierarquia;

CREATE TABLE ProdutoHierarquia ( 
	CodigoProdutoSuperior bigint NOT NULL,
	CodigoProdutoInferior bigint NOT NULL,
	Quantidade int NOT NULL
);

ALTER TABLE ProdutoHierarquia ADD CONSTRAINT PK_ProdutoHierarquia 
	PRIMARY KEY CLUSTERED (CodigoProdutoSuperior, CodigoProdutoInferior);

ALTER TABLE ProdutoHierarquia ADD CONSTRAINT FK_ProdutoHierarquia_ProdutoInferior 
	FOREIGN KEY (CodigoProdutoInferior) REFERENCES Produto (CodigoProduto);

ALTER TABLE ProdutoHierarquia ADD CONSTRAINT FK_ProdutoHierarquia_ProdutoSuperior 
	FOREIGN KEY (CodigoProdutoSuperior) REFERENCES Produto (CodigoProduto);




