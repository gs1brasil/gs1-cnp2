IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoImagem') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoImagem;

CREATE TABLE ProdutoImagem ( 
	Codigo bigint identity(1,1)  NOT NULL,
	URL varchar(max) NOT NULL,
	CodigoProduto bigint NOT NULL,
	Status int NOT NULL
);

ALTER TABLE ProdutoImagem ADD CONSTRAINT PK_ProdutoImagem 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ProdutoImagem ADD CONSTRAINT FK_ProdutoImagem_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);
