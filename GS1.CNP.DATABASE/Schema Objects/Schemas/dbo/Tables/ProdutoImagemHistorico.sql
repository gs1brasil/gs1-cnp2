IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoImagemHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoImagemHistorico;

CREATE TABLE ProdutoImagemHistorico ( 
	Codigo bigint NOT NULL,
	URL varchar(max) NOT NULL,
	CodigoProduto bigint NOT NULL,
	Status int NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE ProdutoImagemHistorico ADD CONSTRAINT PK_ProdutoImagemHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE ProdutoImagemHistorico ADD CONSTRAINT FK_ProdutoImagemHistorico_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);






