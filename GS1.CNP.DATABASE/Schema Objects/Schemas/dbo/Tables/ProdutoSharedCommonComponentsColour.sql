/****** Object:  Table [dbo].[ProdutoSharedCommonComponentsColour]    Script Date: 16/09/2016 18:39:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ProdutoSharedCommonComponentsColour](
	[CodigoProduto] [bigint] NOT NULL,
	[colourCode] [varchar](80) NULL,
	[colourDescription] [varchar](80) NULL,
	[colourCodeAgency] [varchar](80) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ProdutoSharedCommonComponentsColour]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoSharedCommonComponentsColour_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[ProdutoSharedCommonComponentsColour] CHECK CONSTRAINT [FK_ProdutoSharedCommonComponentsColour_Produto]
GO


