/****** Object:  Table [dbo].[ProdutoSharedCommonComponentsSize]    Script Date: 16/09/2016 18:32:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ProdutoSharedCommonComponentsSize](
	[CodigoProduto] [bigint] NOT NULL,
	[descriptiveSize] [varchar](80) NULL,
	[sizeCode] [varchar](80) NULL,
	[sizeSystemCode] [varchar](80) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ProdutoSharedCommonComponentsSize]  WITH CHECK ADD  CONSTRAINT [FK_ProdutoSharedCommonComponentsSize_Produto] FOREIGN KEY([CodigoProduto])
REFERENCES [dbo].[Produto] ([CodigoProduto])
GO

ALTER TABLE [dbo].[ProdutoSharedCommonComponentsSize] CHECK CONSTRAINT [FK_ProdutoSharedCommonComponentsSize_Produto]
GO