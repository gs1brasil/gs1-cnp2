IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoURL') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoURL;

CREATE TABLE ProdutoURL ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Nome varchar(255) NOT NULL,
	URL varchar(max) NOT NULL,
	Status int NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoTipoURL int NOT NULL
);

ALTER TABLE ProdutoURL ADD CONSTRAINT PK_ProdutoURL 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE ProdutoURL ADD CONSTRAINT FK_ProdutoURL_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE ProdutoURL ADD CONSTRAINT FK_ProdutoURL_TipoURL 
	FOREIGN KEY (CodigoTipoURL) REFERENCES TipoURL (Codigo);







