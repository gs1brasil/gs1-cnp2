IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('ProdutoURLHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE ProdutoURLHistorico;

CREATE TABLE ProdutoURLHistorico ( 
	Codigo bigint NOT NULL,
	Nome varchar(255) NOT NULL,
	URL varchar(max) NOT NULL,
	Status int NOT NULL,
	CodigoProduto bigint NOT NULL,
	CodigoTipoURL int NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE ProdutoURLHistorico ADD CONSTRAINT PK_ProdutoURLHistorico 
	PRIMARY KEY CLUSTERED (DataHistorico);

ALTER TABLE ProdutoURLHistorico ADD CONSTRAINT FK_ProdutoURLHistorico_Produto 
	FOREIGN KEY (CodigoProduto) REFERENCES Produto (CodigoProduto);

ALTER TABLE ProdutoURLHistorico ADD CONSTRAINT FK_ProdutoURLHistorico_TipoURL 
	FOREIGN KEY (CodigoTipoURL) REFERENCES TipoURL (Codigo);

