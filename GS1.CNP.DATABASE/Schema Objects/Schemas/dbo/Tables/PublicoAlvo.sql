IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('PublicoAlvo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE PublicoAlvo;

CREATE TABLE PublicoAlvo ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255) NOT NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE PublicoAlvo ADD CONSTRAINT PK_PublicoAlvo
	PRIMARY KEY CLUSTERED (Codigo);
