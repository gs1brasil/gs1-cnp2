IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('SituacaoFinanceira') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE SituacaoFinanceira;

CREATE TABLE SituacaoFinanceira ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE SituacaoFinanceira ADD CONSTRAINT PK_SituacaoFinanceira 
	PRIMARY KEY CLUSTERED (Codigo);





