IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusAssociado;

CREATE TABLE StatusAssociado ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE StatusAssociado ADD CONSTRAINT PK_StatusAssociado 
	PRIMARY KEY CLUSTERED (Codigo);





