IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusFuncionalidadeIntegracao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusFuncionalidadeIntegracao;

CREATE TABLE StatusFuncionalidadeIntegracao ( 
	Codigo int identity(1,1) NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE StatusFuncionalidadeIntegracao ADD CONSTRAINT PK_StatusFuncionalidadeIntegracao
	PRIMARY KEY CLUSTERED (Codigo);