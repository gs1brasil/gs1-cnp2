IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusGTIN') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusGTIN;

CREATE TABLE StatusGTIN ( 
	Codigo int NOT NULL,
	Nome varchar(100),
	Descricao varchar(500),
	Status int
);

ALTER TABLE StatusGTIN ADD CONSTRAINT PK_StatusGTIN 
	PRIMARY KEY CLUSTERED (Codigo);





