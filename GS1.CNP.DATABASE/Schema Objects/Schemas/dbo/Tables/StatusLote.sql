IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusLote') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusLote;

CREATE TABLE StatusLote ( 
	Codigo int NOT NULL,
	Nome varchar(100),
	Descricao varchar(500),
	Status int
);

ALTER TABLE StatusLote ADD CONSTRAINT PK_StatusLote 
	PRIMARY KEY CLUSTERED (Codigo);

