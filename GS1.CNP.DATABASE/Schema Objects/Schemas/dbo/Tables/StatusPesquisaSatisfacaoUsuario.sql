IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusPesquisaSatisfacaoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusPesquisaSatisfacaoUsuario;

CREATE TABLE StatusPesquisaSatisfacaoUsuario ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255) NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL 
);

ALTER TABLE StatusPesquisaSatisfacaoUsuario ADD CONSTRAINT PK_StatusPesquisaSatisfacaoUsuario
	PRIMARY KEY CLUSTERED (Codigo);