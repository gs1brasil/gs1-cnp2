IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusPrefixo') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusPrefixo;

CREATE TABLE StatusPrefixo ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE StatusPrefixo ADD CONSTRAINT PK_StatusPrefixo 
	PRIMARY KEY CLUSTERED (Codigo);





