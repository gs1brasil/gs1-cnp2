IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusProduto') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusProduto;

CREATE TABLE StatusProduto ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE StatusProduto ADD CONSTRAINT PK_StatusProduto 
	PRIMARY KEY CLUSTERED (Codigo);





