IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusPublicacao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusPublicacao;

CREATE TABLE [dbo].[StatusPublicacao](
	[Codigo] [int] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Descricao] [varchar](255) NULL,
	[Status] [int] NULL
 );

ALTER TABLE StatusPublicacao ADD CONSTRAINT PK_StatusPublicacao 
	PRIMARY KEY CLUSTERED (Codigo);
