IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusTermoAdesao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusTermoAdesao;

CREATE TABLE StatusTermoAdesao ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE StatusTermoAdesao ADD CONSTRAINT PK_StatusTermoAdesao 
	PRIMARY KEY CLUSTERED (Codigo);





