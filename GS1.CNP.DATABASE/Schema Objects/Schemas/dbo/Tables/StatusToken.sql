IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusToken') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusToken;

CREATE TABLE StatusToken ( 
	Codigo int identity(1,1) NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	DataAlteracao datetime NOT NULL
);

ALTER TABLE StatusToken ADD CONSTRAINT PK_StatusToken
	PRIMARY KEY CLUSTERED (Codigo);