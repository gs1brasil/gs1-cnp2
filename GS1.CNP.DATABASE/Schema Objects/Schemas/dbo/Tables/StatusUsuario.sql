IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('StatusUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE StatusUsuario;

CREATE TABLE StatusUsuario ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL,
	CodigoTipoUsuario int
);

ALTER TABLE StatusUsuario ADD CONSTRAINT PK_StatusUsuario 
	PRIMARY KEY CLUSTERED (Codigo);
