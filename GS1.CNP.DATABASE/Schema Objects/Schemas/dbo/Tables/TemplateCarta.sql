IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TemplateCarta') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TemplateCarta;

CREATE TABLE TemplateCarta ( 
	Codigo int identity(1,1)  NOT NULL,
	CodigoLicenca int NOT NULL,
	CaminhoTemplate varchar(max),
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE TemplateCarta
	ADD CONSTRAINT UQ_TemplateCarta_CodigoLicenca UNIQUE (CodigoLicenca);

ALTER TABLE TemplateCarta ADD CONSTRAINT PK_TemplateCarta 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TemplateCarta ADD CONSTRAINT FK_TemplateCarta_Licenca 
	FOREIGN KEY (CodigoLicenca) REFERENCES Licenca (Codigo);

ALTER TABLE TemplateCarta ADD CONSTRAINT FK_TemplateCarta_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);






