IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TemplateCartaHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TemplateCartaHistorico;

CREATE TABLE TemplateCartaHistorico ( 
	Codigo int NOT NULL,
	CodigoLicenca int NOT NULL,
	CaminhoTemplate varchar(255),
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE TemplateCartaHistorico ADD CONSTRAINT PK_TemplateCartaHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE TemplateCartaHistorico ADD CONSTRAINT FK_TemplateCartaHistorico_Licenca 
	FOREIGN KEY (CodigoLicenca) REFERENCES Licenca (Codigo);

ALTER TABLE TemplateCartaHistorico ADD CONSTRAINT FK_TemplateCartaHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);
