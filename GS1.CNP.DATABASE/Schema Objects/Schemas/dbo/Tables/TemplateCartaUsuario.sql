IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TemplateCartaUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TemplateCartaUsuario;

CREATE TABLE TemplateCartaUsuario ( 
	Codigo bigint identity(1,1)  NOT NULL,
	CodigoTemplateCarta int NOT NULL,
	CaminhoTemplate varchar(255) NOT NULL,
	CodigoUsuario bigint NOT NULL,
	DataGeracao datetime NOT NULL,
	CodigoAssociado bigint NOT NULL,
	NumeroPrefixo bigint NOT NULL
);

ALTER TABLE TemplateCartaUsuario ADD CONSTRAINT PK_TemplateCartaUsuario 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TemplateCartaUsuario ADD CONSTRAINT FK_TemplateCartaUsuario_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo);

ALTER TABLE TemplateCartaUsuario ADD CONSTRAINT FK_TemplateCartaUsuario_TemplateCarta 
	FOREIGN KEY (CodigoTemplateCarta) REFERENCES TemplateCarta (Codigo);

ALTER TABLE TemplateCartaUsuario ADD CONSTRAINT FK_TemplateCartaUsuario_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo);








