IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TermoAdesao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TermoAdesao;

CREATE TABLE TermoAdesao ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Versao varchar(50) NOT NULL,
	DataCriacao datetime NOT NULL,
	DataVencimento datetime,
	CodigoStatusTermoAdesao int NOT NULL,
	CodigoTipoAceite int NOT NULL,
	CodigoUsuarioCriacao bigint NOT NULL,
	CodigoUsuarioAlteracao bigint
);

ALTER TABLE TermoAdesao ADD CONSTRAINT PK_TermoAdesao 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TermoAdesao ADD CONSTRAINT FK_TermoAdesao_StatusTermoAdesao 
	FOREIGN KEY (CodigoStatusTermoAdesao) REFERENCES StatusTermoAdesao (Codigo);

ALTER TABLE TermoAdesao ADD CONSTRAINT FK_TermoAdesao_TipoAceite 
	FOREIGN KEY (CodigoTipoAceite) REFERENCES TipoAceite (Codigo);

ALTER TABLE TermoAdesao ADD CONSTRAINT FK_TermoAdesao_UsuarioAlteracao 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);

ALTER TABLE TermoAdesao ADD CONSTRAINT FK_TermoAdesao_UsuarioCriacao 
	FOREIGN KEY (CodigoUsuarioCriacao) REFERENCES Usuario (Codigo);









