IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TermoAdesaoIdioma') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TermoAdesaoIdioma;

CREATE TABLE TermoAdesaoIdioma ( 
	CodigoTermoAdesao bigint NOT NULL,
	CodigoIdioma int NOT NULL,
	HTML text NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE TermoAdesaoIdioma ADD CONSTRAINT PK_TermoAdesaoIdioma 
	PRIMARY KEY CLUSTERED (CodigoTermoAdesao, CodigoIdioma);

ALTER TABLE TermoAdesaoIdioma ADD CONSTRAINT FK_TermoAdesaoIdioma_Idioma 
	FOREIGN KEY (CodigoIdioma) REFERENCES Idioma (Codigo);

ALTER TABLE TermoAdesaoIdioma ADD CONSTRAINT FK_TermoAdesaoIdioma_TermoAdesao 
	FOREIGN KEY (CodigoTermoAdesao) REFERENCES TermoAdesao (Codigo);

ALTER TABLE TermoAdesaoIdioma ADD CONSTRAINT FK_TermoAdesaoIdioma_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);






