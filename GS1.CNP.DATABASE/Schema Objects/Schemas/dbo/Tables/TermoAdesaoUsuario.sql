IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TermoAdesaoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TermoAdesaoUsuario;

CREATE TABLE TermoAdesaoUsuario ( 
	CodigoTermoAdesao bigint NOT NULL,
	CodigoUsuario bigint NOT NULL,
	DataAceite datetime NOT NULL
);

ALTER TABLE TermoAdesaoUsuario ADD CONSTRAINT PK_TermoAdesaoUsuario 
	PRIMARY KEY CLUSTERED (CodigoTermoAdesao, CodigoUsuario);

ALTER TABLE TermoAdesaoUsuario ADD CONSTRAINT FK_TermoAdesaoUsuario_TermoAdesao 
	FOREIGN KEY (CodigoTermoAdesao) REFERENCES TermoAdesao (Codigo);

ALTER TABLE TermoAdesaoUsuario ADD CONSTRAINT FK_TermoAdesaoUsuario_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo);




