IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoAceite') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoAceite;

CREATE TABLE TipoAceite ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int
);

ALTER TABLE TipoAceite ADD CONSTRAINT PK_TipoAceite 
	PRIMARY KEY CLUSTERED (Codigo);





