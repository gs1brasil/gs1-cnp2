IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoAjuda') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoAjuda;

CREATE TABLE TipoAjuda ( 
	Codigo int NOT NULL,
	Nome varchar(100),
	Descricao varchar(255),
	Status int
);

ALTER TABLE TipoAjuda ADD CONSTRAINT PK_TipoAjuda 
	PRIMARY KEY CLUSTERED (Codigo);





