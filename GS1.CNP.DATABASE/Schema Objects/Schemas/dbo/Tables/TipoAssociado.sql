IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoAssociado') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoAssociado;

CREATE TABLE TipoAssociado ( 
	Codigo int NOT NULL,
	Nome varchar(50),
	Descricao varchar(255),
	Status int
);

ALTER TABLE TipoAssociado ADD CONSTRAINT PK_TipoAssociado 
	PRIMARY KEY CLUSTERED (Codigo);





