IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoCompartilhamento') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoCompartilhamento;

CREATE TABLE TipoCompartilhamento ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE TipoCompartilhamento ADD CONSTRAINT PK_TipoCompartilhamento 
	PRIMARY KEY CLUSTERED (Codigo);
