IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoContato') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoContato;

CREATE TABLE TipoContato ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status varchar(1) NOT NULL
);

ALTER TABLE TipoContato ADD CONSTRAINT PK_TipoContato 
	PRIMARY KEY CLUSTERED (Codigo);





