IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoFiltro') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoFiltro;

CREATE TABLE TipoFiltro ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE TipoFiltro ADD CONSTRAINT PK_TipoFiltro 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TipoFiltro ADD CONSTRAINT FK_TipoFiltro_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;






