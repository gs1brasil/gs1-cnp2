IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoGTIN') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoGTIN;

CREATE TABLE TipoGTIN ( 
	Codigo int NOT NULL,
	Nome varchar(255),
	Descricao varchar(1000),
	Status int
);

ALTER TABLE TipoGTIN ADD CONSTRAINT PK_TipoGTIN 
	PRIMARY KEY CLUSTERED (Codigo);





