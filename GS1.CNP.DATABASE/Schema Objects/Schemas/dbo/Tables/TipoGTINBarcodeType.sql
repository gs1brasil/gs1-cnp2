IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoGTINBarcodeType') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoGTINBarcodeType;

CREATE TABLE TipoGTINBarcodeType ( 
	CodigoTipoGTIN int NOT NULL,
	CodigoBarCodeTypeList int NOT NULL
);

ALTER TABLE TipoGTINBarcodeType ADD CONSTRAINT PK_TipoGTINBarcodeType 
	PRIMARY KEY CLUSTERED (CodigoTipoGTIN, CodigoBarCodeTypeList);

ALTER TABLE TipoGTINBarcodeType ADD CONSTRAINT FK_TipoGTINBarcodeType_BarCodeTypeList 
	FOREIGN KEY (CodigoBarCodeTypeList) REFERENCES BarCodeTypeList (Codigo);

ALTER TABLE TipoGTINBarcodeType ADD CONSTRAINT FK_TipoGTINBarcodeType_TipoGTIN 
	FOREIGN KEY (CodigoTipoGTIN) REFERENCES TipoGTIN (Codigo);



