IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoGTINLicenca') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoGTINLicenca;

CREATE TABLE TipoGTINLicenca ( 
	CodigoTipoGTIN int NOT NULL,
	CodigoLicenca int NOT NULL
);

ALTER TABLE TipoGTINLicenca ADD CONSTRAINT PK_TipoGTINLicenca 
	PRIMARY KEY CLUSTERED (CodigoTipoGTIN, CodigoLicenca);

ALTER TABLE TipoGTINLicenca ADD CONSTRAINT FK_TipoGTINLicenca_Licenca 
	FOREIGN KEY (CodigoLicenca) REFERENCES Licenca (Codigo);

ALTER TABLE TipoGTINLicenca ADD CONSTRAINT FK_TipoGTINLicenca_TipoGTIN 
	FOREIGN KEY (CodigoTipoGTIN) REFERENCES TipoGTIN (Codigo);



