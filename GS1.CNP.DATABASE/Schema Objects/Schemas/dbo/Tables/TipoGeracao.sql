IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoGeracao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoGeracao;

CREATE TABLE TipoGeracao ( 
	Codigo int NOT NULL,
	Nome varchar(100),
	Descricao varchar(255),
	Status int
);

ALTER TABLE TipoGeracao ADD CONSTRAINT PK_TipoGeracao 
	PRIMARY KEY CLUSTERED (Codigo);





