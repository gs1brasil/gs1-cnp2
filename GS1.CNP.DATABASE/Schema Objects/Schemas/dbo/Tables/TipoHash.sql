IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoHash') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoHash;

CREATE TABLE TipoHash ( 
	Codigo int NOT NULL,
	Nome varchar(255) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL,
	QuantidadeMaximaCaracteres int NOT NULL
);

ALTER TABLE TipoHash ADD CONSTRAINT PK_TipoHash 
	PRIMARY KEY CLUSTERED (Codigo);






