IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoProduto') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoProduto;

CREATE TABLE TipoProduto ( 
	Codigo int identity(1,1)  NOT NULL,
	Nome varchar(50),
	Status int NOT NULL,
	NomeImagem varchar(255),
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	Descricao varchar(255),
	CodigoTradeItemUnitDescriptorCodes int NOT NULL
);

ALTER TABLE TipoProduto ADD CONSTRAINT PK_TipoProduto_1 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TipoProduto ADD CONSTRAINT FK_TipoProduto_TradeItemUnitDescriptorCodes 
	FOREIGN KEY (CodigoTradeItemUnitDescriptorCodes) REFERENCES TradeItemUnitDescriptorCodes (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE TipoProduto ADD CONSTRAINT FK_TipoProduto_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;









