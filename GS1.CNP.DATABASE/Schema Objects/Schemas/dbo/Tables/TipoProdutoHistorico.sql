IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoProdutoHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoProdutoHistorico;

CREATE TABLE TipoProdutoHistorico ( 
	Codigo int NOT NULL,
	Nome varchar(50),
	Status int NOT NULL,
	NomeImagem varchar(max),
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	Descricao varchar(255),
	DataHistorico datetime NOT NULL,
	CodigoTradeItemUnitDescriptorCodes int NOT NULL
);

ALTER TABLE TipoProdutoHistorico ADD CONSTRAINT PK_TipoProdutoHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE TipoProdutoHistorico ADD CONSTRAINT FK_TipoProdutoHistorico_TradeItemUnitDescriptorCodes 
	FOREIGN KEY (CodigoTradeItemUnitDescriptorCodes) REFERENCES TradeItemUnitDescriptorCodes (Codigo);

ALTER TABLE TipoProdutoHistorico ADD CONSTRAINT FK_TipoProdutoHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);










