IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoURL') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoURL;

CREATE TABLE TipoURL ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE TipoURL ADD CONSTRAINT PK_TipoURL 
	PRIMARY KEY CLUSTERED (Codigo);





