IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoUsuario;

CREATE TABLE TipoUsuario ( 
	Codigo int NOT NULL,
	Nome varchar(50) NOT NULL,
	Descricao varchar(255),
	Status int NOT NULL
);

ALTER TABLE TipoUsuario ADD CONSTRAINT PK_TipoUsuario 
	PRIMARY KEY CLUSTERED (Codigo);





