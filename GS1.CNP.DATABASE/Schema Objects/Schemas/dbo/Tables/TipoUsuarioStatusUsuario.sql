IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TipoUsuarioStatusUsuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TipoUsuarioStatusUsuario;

CREATE TABLE TipoUsuarioStatusUsuario ( 
	Codigo bigint identity(1,1)  NOT NULL,
	CodigoStatusUsuario int NOT NULL,
	CodigoTipoUsuario int NOT NULL
);

ALTER TABLE TipoUsuarioStatusUsuario ADD CONSTRAINT PK_TipoUsuarioStatusUsuario 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE TipoUsuarioStatusUsuario ADD CONSTRAINT FK_TipoUsuarioStatusUsuario_StatusUsuario 
	FOREIGN KEY (CodigoStatusUsuario) REFERENCES StatusUsuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE TipoUsuarioStatusUsuario ADD CONSTRAINT FK_TipoUsuarioStatusUsuario_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;




