IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Token') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Token;

CREATE TABLE Token ( 
	Codigo int identity(1,1) NOT NULL,
	CodigoAssociado  bigint NOT NULL,
	Token varchar(255) NOT NULL UNIQUE,
	Descricao varchar(400) NULL,
	Status int NOT NULL,
	CodigoStatusToken int NOT NULL,
	DataAlteracao datetime NOT NULL,
	DataCadastro datetime NOT NULL,
	DataExpiracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE Token ADD CONSTRAINT PK_Token
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Token ADD CONSTRAINT FK_Token_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	
ALTER TABLE Token ADD CONSTRAINT FK_Token_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo)
	
ALTER TABLE Token ADD CONSTRAINT FK_Token_StatusToken
	FOREIGN KEY (CodigoStatusToken) REFERENCES StatusToken (Codigo)
	
