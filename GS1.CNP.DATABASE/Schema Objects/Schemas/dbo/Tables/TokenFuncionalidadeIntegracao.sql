IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TokenFuncionalidadeIntegracao') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TokenFuncionalidadeIntegracao;

CREATE TABLE TokenFuncionalidadeIntegracao ( 
	CodigoToken int NOT NULL,
	CodigoFuncionalidadeIntegracao int NOT NULL,
	DataAlteracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL
);

ALTER TABLE TokenFuncionalidadeIntegracao ADD CONSTRAINT PK_TokenFuncionalidadeIntegracao
	PRIMARY KEY CLUSTERED (CodigoToken, CodigoFuncionalidadeIntegracao);

ALTER TABLE TokenFuncionalidadeIntegracao ADD CONSTRAINT FK_TokenFuncionalidadeIntegracao_Funcionalidade 
	FOREIGN KEY (CodigoFuncionalidadeIntegracao) REFERENCES FuncionalidadeIntegracao (Codigo)

ALTER TABLE TokenFuncionalidadeIntegracao ADD CONSTRAINT FK_TokenFuncionalidadeIntegracao_Token
	FOREIGN KEY (CodigoToken) REFERENCES Token (Codigo)

ALTER TABLE TokenFuncionalidadeIntegracao ADD CONSTRAINT FK_TokenFuncionalidadeIntegracao_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)