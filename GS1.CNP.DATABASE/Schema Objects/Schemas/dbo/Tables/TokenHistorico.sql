IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TokenHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TokenHistorico;

CREATE TABLE TokenHistorico ( 
	Codigo int NOT NULL,
	CodigoAssociado bigint NOT NULL,
	Token varchar(255) NOT NULL,
	Descricao varchar(400) NULL,
	Status int NOT NULL,
	CodigoStatusToken int NOT NULL,
	DataAlteracao datetime NOT NULL,
	DataCadastro datetime NOT NULL,
	DataExpiracao datetime NOT NULL,
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE TokenHistorico ADD CONSTRAINT PK_TokenHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE TokenHistorico ADD CONSTRAINT FK_TokenHistorico_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	
ALTER TABLE TokenHistorico ADD CONSTRAINT FK_TokenHistorico_Associado 
	FOREIGN KEY (CodigoAssociado) REFERENCES Associado (Codigo)
	
ALTER TABLE Token ADD CONSTRAINT FK_TokenHistorico_StatusToken
	FOREIGN KEY (CodigoStatusToken) REFERENCES StatusToken (Codigo)