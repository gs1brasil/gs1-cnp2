IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('TradeItemUnitDescriptorCodes') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE TradeItemUnitDescriptorCodes;

CREATE TABLE TradeItemUnitDescriptorCodes ( 
	Codigo int NOT NULL,
	Nome varchar(6) NOT NULL,
	Descricao varchar(400) NOT NULL,
	Status int NOT NULL
);

ALTER TABLE TradeItemUnitDescriptorCodes ADD CONSTRAINT PK_TradeItemUnitDescriptorCodes 
	PRIMARY KEY CLUSTERED (Codigo);





