IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('UNECEREC20') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE UNECEREC20;

CREATE TABLE UNECEREC20 ( 
	Codigo int NOT NULL,
	Nome varchar(100) NOT NULL,
	CommonCode varchar(3),
	Sigla varchar(50) NOT NULL,
	FatorConversao decimal(25,12) NOT NULL,
	Tipo int NOT NULL,
	Status int NOT NULL,
	IndicadorPrincipal varchar(1)
);

ALTER TABLE UNECEREC20 ADD CONSTRAINT PK_UNECEREC20 
	PRIMARY KEY CLUSTERED (Codigo);









