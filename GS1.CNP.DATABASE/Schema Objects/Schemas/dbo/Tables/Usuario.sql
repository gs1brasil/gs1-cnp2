IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('Usuario') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE Usuario;

CREATE TABLE Usuario ( 
	Codigo bigint identity(1,1)  NOT NULL,
	Nome varchar(255) NOT NULL,
	CodigoStatusUsuario int NOT NULL,
	CodigoPerfil int NOT NULL,
	CodigoTipoUsuario int NOT NULL,
	Email varchar(255) NOT NULL,
	TelefoneResidencial varchar(50),
	Senha varchar(255),
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	MensagemObservacao varchar(500),
	DataAlteracaoSenha datetime,
	QuantidadeTentativa int DEFAULT ((0)) NOT NULL,
	DataUltimoAcesso datetime,
	DataCadastro datetime DEFAULT (getdate()) NOT NULL,
	TelefoneCelular varchar(50),
	TelefoneComercial varchar(50),
	Ramal varchar(50),
	Departamento varchar(255),
	Cargo varchar(255),
	CPFCNPJ varchar(14)
);

ALTER TABLE Usuario
	ADD CONSTRAINT UQ_Usuario_Email UNIQUE (Email);

CREATE INDEX IDX_CPFCNPJ
ON Usuario (CPFCNPJ ASC);

ALTER TABLE Usuario ADD CONSTRAINT PK_Usuario 
	PRIMARY KEY CLUSTERED (Codigo);

ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_Perfil 
	FOREIGN KEY (CodigoPerfil) REFERENCES Perfil (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_StatusUsuario 
	FOREIGN KEY (CodigoStatusUsuario) REFERENCES StatusUsuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Usuario ADD CONSTRAINT FK_Usuario_Usuario 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;






















