IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('UsuarioHistorico') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE UsuarioHistorico;

CREATE TABLE UsuarioHistorico ( 
	Codigo bigint NOT NULL,
	Nome varchar(255) NOT NULL,
	CodigoStatusUsuario int NOT NULL,
	CodigoPerfil int NOT NULL,
	CodigoTipoUsuario int NOT NULL,
	Email varchar(255) NOT NULL,
	TelefoneResidencial varchar(50),
	Senha varchar(255),
	CodigoUsuarioAlteracao bigint NOT NULL,
	DataAlteracao datetime NOT NULL,
	MensagemObservacao varchar(500),
	DataAlteracaoSenha datetime,
	QuantidadeTentativa int DEFAULT ((0)) NOT NULL,
	DataUltimoAcesso datetime,
	DataCadastro datetime DEFAULT (getdate()) NOT NULL,
	TelefoneCelular varchar(50),
	TelefoneComercial varchar(50),
	Ramal varchar(50),
	Departamento varchar(255),
	Cargo varchar(255),
	CPFCNPJ varchar(14),
	DataHistorico datetime NOT NULL
);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT PK_UsuarioHistorico 
	PRIMARY KEY CLUSTERED (Codigo, DataHistorico);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT FK_UsuarioHistorico_Perfil 
	FOREIGN KEY (CodigoPerfil) REFERENCES Perfil (Codigo);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT FK_UsuarioHistorico_StatusUsuario 
	FOREIGN KEY (CodigoStatusUsuario) REFERENCES StatusUsuario (Codigo);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT FK_UsuarioHistorico_TipoUsuario 
	FOREIGN KEY (CodigoTipoUsuario) REFERENCES TipoUsuario (Codigo);

ALTER TABLE UsuarioHistorico ADD CONSTRAINT FK_UsuarioHistorico_UsuarioAlteracao 
	FOREIGN KEY (CodigoUsuarioAlteracao) REFERENCES Usuario (Codigo);























