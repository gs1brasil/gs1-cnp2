IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('UsuarioHistoricoSenha') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE UsuarioHistoricoSenha;

CREATE TABLE UsuarioHistoricoSenha ( 
	CodigoUsuario bigint NOT NULL,
	Senha varchar(255) NOT NULL,
	DataHistorico datetime NOT NULL
);

ALTER TABLE UsuarioHistoricoSenha ADD CONSTRAINT PK_UsuarioHistoricoSenha 
	PRIMARY KEY CLUSTERED (CodigoUsuario, DataHistorico);

ALTER TABLE UsuarioHistoricoSenha ADD CONSTRAINT FK_UsuarioHistoricoSenha_Usuario 
	FOREIGN KEY (CodigoUsuario) REFERENCES Usuario (Codigo)
	ON DELETE RESTRICT ON UPDATE RESTRICT;




