IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('palletTypeCode') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE palletTypeCode;

CREATE TABLE palletTypeCode ( 
	Codigo int NOT NULL,
	Code varchar(3) NOT NULL,
	CodeDescription varchar(3000) NOT NULL,
	Status int NOT NULL
);

ALTER TABLE palletTypeCode ADD CONSTRAINT PK_palletTypeCode 
	PRIMARY KEY CLUSTERED (Codigo);





