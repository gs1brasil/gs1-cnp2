IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbBrick') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbBrick;

CREATE TABLE tbBrick ( 
	CodeBrick varchar(10) NOT NULL,
	CodeClass varchar(10) NOT NULL,
	CodeFamily varchar(10) NOT NULL,
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(1500) NOT NULL,
	Definition varchar(2000)
);

ALTER TABLE tbBrick ADD CONSTRAINT PK_tbBrick 
	PRIMARY KEY CLUSTERED (CodeClass, CodeBrick, CodeFamily, Idioma, CodeSegment);

ALTER TABLE tbBrick ADD CONSTRAINT FK_tbBrick_tbClass 
	FOREIGN KEY (CodeClass, CodeFamily, Idioma, CodeSegment) REFERENCES tbClass (CodeClass, CodeFamily, Idioma, CodeSegment)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
