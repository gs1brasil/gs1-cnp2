IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbBrickType') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbBrickType;

CREATE TABLE tbBrickType ( 
	CodeBrick varchar(10) NOT NULL,
	CodeClass varchar(10) NOT NULL,
	CodeFamily varchar(10) NOT NULL,
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	CodeType varchar(10) NOT NULL
);

ALTER TABLE tbBrickType ADD CONSTRAINT PK_tbBrickType 
	PRIMARY KEY CLUSTERED (CodeBrick, CodeClass, CodeFamily, Idioma, CodeSegment, CodeType);

ALTER TABLE tbBrickType ADD CONSTRAINT FK_tbBrickType_tbBrick 
	FOREIGN KEY (CodeClass, CodeBrick, CodeFamily, Idioma, CodeSegment) REFERENCES tbBrick (CodeClass, CodeBrick, CodeFamily, Idioma, CodeSegment)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE tbBrickType ADD CONSTRAINT FK_tbBrickType_tbType 
	FOREIGN KEY (CodeType, Idioma) REFERENCES tbTType (CodeType, Idioma)
	ON DELETE RESTRICT ON UPDATE RESTRICT;







