IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbClass') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbClass;

CREATE TABLE tbClass ( 
	CodeClass varchar(10) NOT NULL,
	CodeFamily varchar(10) NOT NULL,
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(150) NOT NULL,
	Definition varchar(1000)
);

ALTER TABLE tbClass ADD CONSTRAINT PK_tbClass 
	PRIMARY KEY CLUSTERED (CodeClass, CodeFamily, Idioma, CodeSegment);

ALTER TABLE tbClass ADD CONSTRAINT FK_tbClass_tbFamily 
	FOREIGN KEY (CodeFamily, Idioma, CodeSegment) REFERENCES tbFamily (CodeFamily, Idioma, CodeSegment)
	ON DELETE RESTRICT ON UPDATE RESTRICT;







