IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbCountry') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbCountry;

CREATE TABLE tbCountry ( 
	CountryCode varchar(3) NOT NULL,
	CountryName varchar(100) NOT NULL
);

ALTER TABLE tbCountry ADD CONSTRAINT PK_tbCountry 
	PRIMARY KEY CLUSTERED (CountryCode);
