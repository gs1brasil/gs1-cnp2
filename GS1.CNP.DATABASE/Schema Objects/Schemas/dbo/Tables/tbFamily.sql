IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbFamily') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbFamily;

CREATE TABLE tbFamily ( 
	CodeFamily varchar(10) NOT NULL,
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(1500) NOT NULL,
	Definition varchar(2000)
);

ALTER TABLE tbFamily ADD CONSTRAINT PK_tbFamily 
	PRIMARY KEY CLUSTERED (CodeFamily, Idioma, CodeSegment);

ALTER TABLE tbFamily ADD CONSTRAINT FK_tbFamily_tbSegment 
	FOREIGN KEY (Idioma, CodeSegment) REFERENCES tbSegment (Idioma, CodeSegment)
	ON DELETE RESTRICT ON UPDATE RESTRICT;
