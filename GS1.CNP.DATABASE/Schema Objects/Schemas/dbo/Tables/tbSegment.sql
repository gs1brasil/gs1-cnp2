IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbSegment') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbSegment;

CREATE TABLE tbSegment ( 
	CodeSegment varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(1500) NOT NULL,
	Definition varchar(2000)
);

ALTER TABLE tbSegment ADD CONSTRAINT PK_tbSeguimento 
	PRIMARY KEY CLUSTERED (Idioma, CodeSegment);





