IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbStateCountry') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbStateCountry;

CREATE TABLE tbStateCountry ( 
	CountryCode varchar(3) NOT NULL,
	StateCode varchar(8) NOT NULL,
	StateName varchar(100) NOT NULL
);

ALTER TABLE tbStateCountry ADD CONSTRAINT PK_tbStateCountry 
	PRIMARY KEY CLUSTERED (CountryCode, StateCode);

ALTER TABLE tbStateCountry ADD CONSTRAINT FK_tbStateCountry_tbCountry 
	FOREIGN KEY (CountryCode) REFERENCES tbCountry (CountryCode)
	ON DELETE RESTRICT ON UPDATE RESTRICT;




