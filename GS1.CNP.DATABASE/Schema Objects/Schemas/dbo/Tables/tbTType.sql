USE [db_gs1CNP_E5]
GO
/****** Object:  Table [dbo].[tbTType]    Script Date: 12/02/2016 17:23:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbTType](
	[CodeType] [varchar](10) NOT NULL,
	[Idioma] [varchar](5) NOT NULL,
	[Text] [varchar](1500) NOT NULL,
	[Definition] [varchar](2000) NULL,
 CONSTRAINT [PK_tbType] PRIMARY KEY CLUSTERED 
(
	[CodeType] ASC,
	[Idioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
