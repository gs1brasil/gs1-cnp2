IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbTValue') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbTValue;

CREATE TABLE tbTValue ( 
	CodeValue varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	Text varchar(1500) NOT NULL,
	Definition varchar(2000)
);

ALTER TABLE tbTValue ADD CONSTRAINT PK_tbValue 
	PRIMARY KEY CLUSTERED (CodeValue, Idioma);





