IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('tbTypeValue') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE tbTypeValue;

CREATE TABLE tbTypeValue ( 
	CodeType varchar(10) NOT NULL,
	Idioma varchar(5) NOT NULL,
	CodeValue varchar(10) NOT NULL,
	CodeBrick varchar(10) NOT NULL
);

ALTER TABLE tbTypeValue ADD CONSTRAINT PK_tbTypeValue 
	PRIMARY KEY CLUSTERED (CodeType, Idioma, CodeValue, CodeBrick);

ALTER TABLE tbTypeValue ADD CONSTRAINT FK_tbTypeValue_tbType 
	FOREIGN KEY (CodeType, Idioma) REFERENCES tbTType (CodeType, Idioma)
	ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE tbTypeValue ADD CONSTRAINT FK_tbTypeValue_tbValue 
	FOREIGN KEY (CodeValue, Idioma) REFERENCES tbTValue (CodeValue, Idioma)
	ON DELETE RESTRICT ON UPDATE RESTRICT;





