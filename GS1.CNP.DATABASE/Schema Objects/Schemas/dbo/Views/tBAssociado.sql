USE [db_gs1CNP]
GO

/****** Object:  View [dbo].[tBAssociado]    Script Date: 04/12/2015 17:55:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE view [dbo].[tBAssociado] as
SELECT TOP 1
		A.[Codigo] as IdAssociado
		,A.[CPFCNPJ] as CpfCnpj
		,A.[Nome] as RazaoSocial

		,CASE [CodigoStatusAssociado] WHEN 1 THEN 'A'
							  WHEN 3 THEN 'C'
							  WHEN 2 THEN 'S' 
							  END as Status
		,CASE [CodigoSituacaoFinanceira] WHEN 1 THEN 'A' ELSE '1' END AS StatusPagto
		,B.NOME as ContatoMaster
		,B.EMAIL AS EmailContato
		,null as DtVencTaxa
		,A.[CEP] as CEP
		,A.[Endereco] as Endereco
		,CAST(A.[Numero] AS decimal) as Numero
		,A.[Complemento] as Complemento
		,A.[Bairro] as Bairro
		,A.[Cidade] as Cidade
		,A.[UF] as UF
		,A.[Pais] as Pais
		,A.[DataAtualizacaoCRM] as DtUltimaSinc
		,A.[InscricaoEstadual] as InscricaoEstadual		
		,CASE [CodigoTipoCompartilhamento] WHEN 1 THEN 'N'
										 WHEN 2 THEN 'S'
										ELSE NULL END as CompartilhaDado
		FROM [dbo].[Associado] A
		LEFT JOIN CONTATOASSOCIADO B ON B.CODIGOASSOCIADO = A.CODIGO AND B.INDICADORPRINCIPAL = 1

GO


