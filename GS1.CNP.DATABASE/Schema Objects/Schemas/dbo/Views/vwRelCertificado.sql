USE [db_gs1CNP]
GO

/****** Object:  View [dbo].[vwRelCertificado]    Script Date: 04/12/2015 17:55:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vwRelCertificado] as 
SELECT a.IdAssociado,
			   a.CpfCnpj,
			   a.RazaoSocial,
			   CAST(ap.globalTradeItemNumber AS VARCHAR)  as Gtin, 
			   ap.productDescription as DescricaoProduto,
			   NULL as IdCertificado,
			   NULL as NomeCertificado,
			   NULL AS DataAtribuicao,
			   NULL AS DataExpiracao,
			   NULL AS Status,
			   'SEM CERTIFICADO' as DescricaoStatus,
			   NULL DtHistorico
		  FROM Produto ap
		  INNER JOIN tbAssociado a ON a.IdAssociado = ap.CodigoAssociado



GO


