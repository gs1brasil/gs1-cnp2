

/****** Object:  View [dbo].[vwRelLocalizacaoFisica]    Script Date: 02/09/2016 09:52:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



alter View [dbo].[vwRelLocalizacaoFisica] as
SELECT a.IdAssociado
				,a.CpfCnpj
				,a.RazaoSocial
				,CAST(GLN AS VARCHAR) AS nrGln
				,ag.NOME AS Contato  
				,ag.CEP 
				,ag.Endereco 
				,ag.Numero 
				,ag.Complemento  
				,ag.Bairro 
				,ag.Cidade 
				,ag.Estado as UF 
				,ag.Pais
				,ag.CodigoPapelGLN as IdPapel
				
				, COALESCE((SELECT MIN(x.DATAALTERACAO) FROM LocalizacaoFisicaHistorico X WHERE X.Codigo = AG.CODIGO),ag.DATAALTERACAO)  as DtInclusao
				,p.Nome AS NomePapel
				--,CAST(ag.Status AS VARCHAR) as Status 
				,CASE CAST(ag.Status AS VARCHAR) WHEN 'A' THEN '1' ELSE '0' end as Status
				,CASE ag.Status
				  WHEN '1' THEN 'ATIVO'
				  WHEN '2' THEN 'INATIVO'
				  ELSE 'CANCELADO' END DescStatus
				,ag.Observacao AS Observacoes 
				--,ag.IndicadorPrincipal as FlagPrincipal
				,cast(ag.IndicadorPrincipal as bit)as FlagPrincipal
		   FROM LocalizacaoFisica ag
	 left JOIN PapelGLN p ON (ag.CodigoPapelGLN = p.Codigo)
	 INNER JOIN tbAssociado a ON (ag.CodigoAssociado = a.IdAssociado)
GO


