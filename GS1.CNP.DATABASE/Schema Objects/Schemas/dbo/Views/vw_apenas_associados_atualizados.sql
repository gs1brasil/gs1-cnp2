

/****** Object:  View [dbo].[vw_apenas_associados_atualizados]    Script Date: 06/04/2016 14:06:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[vw_apenas_associados_atualizados] AS
SELECT A.ACCOUNTID
             ,A.NAME
             ,A.SMART_NOMEFANTASIA             
             ,A.SMART_CONDICAODERELACIONAMENTO
             ,A.ACCOUNTNUMBER
             ,A.SMART_CNPJCPF
             ,A.EMAILADDRESS1 AS EMAIL
             ,CASE WHEN EXISTS (
                                        SELECT Z.SMART_GESTAODELICENCASEPRODUTOID,
                                                      Z.SMART_NAME,
                                                      Z.SMART_FINANCEIROSTATUS,
                                                      Z.SMART_ASSOCIADO                  
                                        FROM SMART_GESTAODELICENCASEPRODUTO Z WITH(NOLOCK)
                                        WHERE Z.SMART_ASSOCIADO = A.ACCOUNTID 
                                        AND Z.SMART_FINANCEIROSTATUS NOT IN (1,3)
                                  ) THEN 1 ELSE 0
             END AS STATUS_ADIMPLENTE
             , COALESCE ((
                                        SELECT TOP 1 1                                 
                                        FROM SMART_LICENCA X (NOLOCK)
                                        WHERE X.SMART_EMPRESA = A.ACCOUNTID
                                        AND X.SMART_NEGOCIONAME LIKE '%EPC%'
                                        AND X.SMART_PAP_STATUS = 2
                                  ),0) AS STATUS_PREMIUM
        ,A.SMART_INSCRICAOESTADUAL
        ,A.SMART_ESTADONAME
        ,A.SMART_NUMERO
        ,A.ADDRESS1_CITY
        ,A.SMART_LOGRADOUROPRINCIPAL
        ,A.SMART_COMPLEMENTOPRINCIPAL
        ,A.ADDRESS1_POSTALCODE
        ,A.SMART_BAIRROPRINCIPAL
        ,B.SMART_PESSOANAME
        ,B.SMART_CPF
        ,B.TELEPHONE1 AS representante_telephone1
        ,B.TELEPHONE2 AS representante_telephone2	
        ,B.TELEPHONE3 AS representante_telephone3
        ,B.EMAILADDRESS1
        ,B.EMAILADDRESS2
        ,A.SMART_CODIGOGLN
           ,A.SMART_PAISNAME
           ,A.TELEPHONE1
           ,A.SMART_DDIDDDTELEFONEPRINCIPAL
		   --,replace(convert(NVARCHAR, A.ModifiedOn, 103), ' ', '/')  as [DATA DE MODIFICAÇÃO]
		    ,A.ModifiedOn [DATA DE MODIFICAÇÃO]
       ,gs1br_cnp20

    FROM [dbo].[Account] A (NOLOCK)
INNER JOIN CONTACT B ON B.ACCOUNTID = A.ACCOUNTID AND B.SMART_REPRESENTANTELEGAL = 1
WHERE a.ModifiedOn >= DATEADD(DAY, -1, GETDATE())


GO


