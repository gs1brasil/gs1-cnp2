CREATE NONCLUSTERED INDEX IX1_Produto12A4C  on Produto([NrPrefixo], [CodigoAssociado]) Include ([CodItem])
CREATE NONCLUSTERED INDEX IX1_Produto738C5  on Produto([CodigoProdutoOrigem],[globalTradeItemNumber]) 
CREATE NONCLUSTERED INDEX IX1_ProdutoA1316  on Produto([NrPrefixo],[globalTradeItemNumber], [CodigoStatusGTIN]) Include ([productDescription], [CodigoTipoGTIN])
--CREATE NONCLUSTERED INDEX IX1_Produto12A4S  on Produto([CodigoAssociado]) Include ([CodigoProduto], [productDescription], [CodigoTipoGTIN], [alternateItemIdentificationId])
--CREATE NONCLUSTERED INDEX IX1_ProdutoF3B16  on Produto([CodigoTipoGTIN], [CodigoAssociado],[globalTradeItemNumber], [CodigoStatusGTIN]) Include ([CodigoProduto], [productDescription], [alternateItemIdentificationId])
--drop index IX1_ProdutoF3B16 on Produto
--Alterado 18/03/2016 18:10

CREATE NONCLUSTERED INDEX IX1_ProdutoF3B16  on Produto([CodigoTipoGTIN], [CodigoAssociado],[globalTradeItemNumber]) Include ([CodigoProduto], [productDescription], [alternateItemIdentificationId])



CREATE NONCLUSTERED INDEX IX1_Associado21D47  on Associado([CodigoStatusAssociado]) Include ([Codigo], [CAD], [Nome], [CPFCNPJ], [IndicadorCNAERestritivo])

CREATE NONCLUSTERED INDEX IX1_ContatoAssociado769B0  on ContatoAssociado([CodigoAssociado]) Include ([Codigo], [CPFCNPJ], [Email])

CREATE NONCLUSTERED INDEX IX1_HistoricoGeracaoEtiquetaAAE2E  on HistoricoGeracaoEtiqueta([CodigoProduto]) Include ([Codigo], [CodigoUsuario], [CodigoTipoGeracao], [DataImpressao], [Texto], [CodigoFabricante], [CodigoModeloEtiqueta], [Quantidade], [Descricao], [CodigoPosicaoTexto], [CodigoBarCodeTypeList])
CREATE NONCLUSTERED INDEX IX1_HistoricoGeracaoEtiqueta0AEAC  on HistoricoGeracaoEtiqueta([CodigoProduto], [CodigoBarCodeTypeList]) Include ([Codigo], [CodigoUsuario], [CodigoTipoGeracao], [DataImpressao], [Texto], [CodigoFabricante], [CodigoModeloEtiqueta], [Quantidade], [Descricao], [CodigoPosicaoTexto])


CREATE NONCLUSTERED INDEX IX1_UsuarioC12C1  on Usuario([CodigoPerfil],[IdUsuario],[DataMigracao]) Include ([Codigo], [Nome], [CodigoStatusUsuario], [CodigoTipoUsuario], [Email], [TelefoneResidencial], [Senha], [CodigoUsuarioAlteracao], [DataAlteracao], [MensagemObservacao], [DataAlteracaoSenha], [QuantidadeTentativa], [DataUltimoAcesso], [DataCadastro], [TelefoneCelular], [TelefoneComercial], [Ramal], [Departamento], [Cargo], [CPFCNPJ])

CREATE NONCLUSTERED INDEX IX1_tbBrick2FC58  on tbBrick([CodeBrick]) Include ([Text])