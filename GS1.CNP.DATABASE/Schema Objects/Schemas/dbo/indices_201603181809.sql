/*
Missing Index Details from SQLQuery13.sql - hdt14ossoe.database.windows.net.cnp20-DB-Prod (admin_softbox (53))
The Query Processor estimates that implementing the following index could improve the query cost by 51.1386%.
*/

/*
USE [cnp20-DB-Prod]
GO
CREATE NONCLUSTERED INDEX IdxProdutoCodigoStatusGTIN
ON [dbo].[Produto] ([CodigoStatusGTIN])
INCLUDE ([CodigoAssociado])
GO
*/
