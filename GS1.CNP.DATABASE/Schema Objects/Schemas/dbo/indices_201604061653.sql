/*
Missing Index Details from SQLQuery22.sql - hdt14ossoe.database.windows.net.cnp20-db-prod (admin_softbox (51))
The Query Processor estimates that implementing the following index could improve the query cost by 25.5%.
*/

/*
USE [cnp20-DB-Prod]
GO
CREATE NONCLUSTERED INDEX [idxCodigoStatusUsuario]
ON [dbo].[Usuario] ([CodigoStatusUsuario])
INCLUDE ([Codigo])
GO
*/


/*
Missing Index Details from SQLQuery22.sql - hdt14ossoe.database.windows.net.cnp20-db-prod (admin_softbox (51))
The Query Processor estimates that implementing the following index could improve the query cost by 19.6134%.
*/

/*
USE [cnp20-DB-Prod]
GO
CREATE NONCLUSTERED INDEX [idxPrefixoLicencaAssociado]
ON [dbo].[PrefixoLicencaAssociado] ([CodigoAssociado])

GO
*/


/*
Missing Index Details from SQLQuery22.sql - hdt14ossoe.database.windows.net.cnp20-db-prod (admin_softbox (51))
The Query Processor estimates that implementing the following index could improve the query cost by 16.8128%.
*/

/*
USE [cnp20-DB-Prod]
GO
CREATE NONCLUSTERED INDEX [idxAssociadoCADDATACRM]
ON [dbo].[Associado] ([CAD],[DataAtualizacaoCRM])
INCLUDE ([Codigo])
GO
*/

