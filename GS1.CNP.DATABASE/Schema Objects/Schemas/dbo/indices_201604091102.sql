--CREATE NONCLUSTERED INDEX IX1_tbBrick67FC5  on tbBrick([CodeBrick]) Include ([Text])
--CREATE NONCLUSTERED INDEX IX1_ProdutoBrickTypeValue414AF  on ProdutoBrickTypeValue([CodigoProduto], [CodeBrick], [CodeClass], [CodeFamily], [CodeSegment], [CodeType]) 
--CREATE NONCLUSTERED INDEX IX1_ProdutoBrickTypeValue6DA0E  on ProdutoBrickTypeValue([CodigoProduto], [CodeBrick], [CodeClass], [CodeFamily], [CodeSegment], [CodeType]) Include ([Codigo])
--CREATE NONCLUSTERED INDEX IX1_ProdutoBrickTypeValueC82AB  on ProdutoBrickTypeValue([CodigoProduto]) 
--CREATE NONCLUSTERED INDEX IX1_ProdutoF2C21  on Produto([CodigoAssociado],[CodigoStatusGTIN]) 

--CREATE NONCLUSTERED INDEX IX1_ProdutoBrickTypeValue62E6C  on ProdutoBrickTypeValue([CodigoProduto], [CodeBrick], [CodeClass], [CodeFamily], [CodeSegment], [CodeType]) Include ([CodeValue])


CREATE NONCLUSTERED INDEX IX1_AssociadoUsuario06C26  on AssociadoUsuario([CodigoAssociado]) Include ([CodigoUsuario])
CREATE NONCLUSTERED INDEX IX1_ProdutoImagemB8CD8  on ProdutoImagem([CodigoProduto]) Include ([URL])