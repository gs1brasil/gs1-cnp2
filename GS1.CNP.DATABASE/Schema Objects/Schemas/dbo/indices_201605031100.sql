CREATE NONCLUSTERED INDEX IX1_Produto08475  on Produto([NrPrefixo], [CodigoAssociado]) Include ([CodItem])
CREATE NONCLUSTERED INDEX IX1_Produto0CE9F  on Produto([CodigoTipoGTIN], [CodigoAssociado], [CodigoStatusGTIN],[globalTradeItemNumber]) Include ([CodigoProduto], [productDescription], [alternateItemIdentificationId])

CREATE NONCLUSTERED INDEX IX1_Produto84C4D  on Produto([CodigoAssociado]) Include ([CodigoProduto], [globalTradeItemNumber], [productDescription], [CodigoTipoGTIN], [CodigoStatusGTIN])
CREATE NONCLUSTERED INDEX IX1_ProdutoEB718  on Produto([CodigoAssociado],[globalTradeItemNumber], [CodigoStatusGTIN]) Include ([CodigoProduto], [productDescription], [CodigoTipoGTIN], [NrPrefixo], [CodItem], [VarianteLogistica], [informationProvider])

CREATE NONCLUSTERED INDEX [IX1_UsuarioJOBCRM1]
ON [dbo].[Usuario] ([CodigoStatusUsuario])
INCLUDE ([Codigo])
GO

CREATE NONCLUSTERED INDEX [IX1_UsuarioJOBCRM2>]
ON [dbo].[Usuario] ([Email])
INCLUDE ([Codigo])
GO