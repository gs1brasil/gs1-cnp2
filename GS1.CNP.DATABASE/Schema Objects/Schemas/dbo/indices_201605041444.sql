CREATE NONCLUSTERED INDEX IX1_ProdutoURL5F220  on ProdutoURL([Status], [CodigoProduto]) 
CREATE NONCLUSTERED INDEX IX1_ProdutoBrickTypeValueBC118  on ProdutoBrickTypeValue([CodigoProduto], [CodeBrick], [CodeClass], [CodeFamily], [CodeSegment], [CodeType]) Include ([CodeValue])
CREATE NONCLUSTERED INDEX IX1_PrefixoLicencaAssociado16667  on PrefixoLicencaAssociado([NumeroPrefixo]) Include ([CodigoLicenca], [CodigoAssociado])

CREATE NONCLUSTERED INDEX IX1_Produto731B6  on Produto([CodigoAssociado],[globalTradeItemNumber], [CodigoStatusGTIN]) Include ([CodigoProduto], [productDescription], [CodigoTipoGTIN], [alternateItemIdentificationId])



CREATE NONCLUSTERED INDEX [idx_JOBCRM]
ON [dbo].[Associado] ([DataAtualizacaoCRM])
INCLUDE ([Codigo],[CAD])