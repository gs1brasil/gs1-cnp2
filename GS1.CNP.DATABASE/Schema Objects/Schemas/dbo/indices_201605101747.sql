CREATE NONCLUSTERED INDEX IX1_Produto61942  on Produto([CodigoProdutoOrigem],[globalTradeItemNumber]) 
CREATE NONCLUSTERED INDEX IX1_ProdutoD32E9  on Produto([globalTradeItemNumber], [CodigoStatusGTIN]) 
CREATE NONCLUSTERED INDEX IX1_ProdutoBrickTypeValueAB30D  on ProdutoBrickTypeValue([CodigoProduto], [CodeBrick], [CodeClass], [CodeFamily], [CodeSegment]) Include ([CodeType], [CodeValue])
CREATE NONCLUSTERED INDEX IX1_ProdutoURL237F3  on ProdutoURL([Status], [CodigoProduto]) 


CREATE NONCLUSTERED INDEX IX1_Usuario0B41F  on Usuario([CodigoStatusUsuario]) Include ([Codigo])
CREATE NONCLUSTERED INDEX IX1_UsuarioDAE02  on Usuario([CodigoPerfil]) Include ([Codigo], [Nome], [Email], [DataAlteracao], [DataAlteracaoSenha], [DataUltimoAcesso], [DataCadastro], [CPFCNPJ])

-- Migracao

CREATE NONCLUSTERED INDEX [IDX_MIGRACAO7_IdUsuario]
ON [dbo].[Usuario] ([IdUsuario])
INCLUDE ([Codigo])


CREATE NONCLUSTERED INDEX [IDX_MIGRACAO7_ProdutoURL]
ON [dbo].[ProdutoURL] ([CodigoProduto])
INCLUDE ([Codigo],[URL])


USE [cnp_gs1]
GO
CREATE NONCLUSTERED INDEX [IDX_Migracao7_tbAssociadoProduto]
ON [dbo].[tbAssociadoProduto] ([IdAssociado])
INCLUDE ([Gtin],[NrPrefixo],[TipoGtin],[CodItem],[MarcaProduto],[CodigoInterno],[Descricao],[DtInclusao],[DtSuspensao],[DtReativacao],[DtCancelamento],[DtReutilizacao],[Estado],[TIPI],[Largura],[Profundidade],[Altura],[PesoBruto],[PesoLiquido],[Status],[NCM])
GO