﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using GS1.CNP.DAL;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.Diagnostics;

namespace GS1.CNP.JOB.EXPIRALICENCAS
{
    class Program
    {
        private static String STRCON = ConfigurationManager.ConnectionStrings["BD"].ToString();
        private static String EVENT_SOURCE = "GS1.CNP.JOB.EXPIRALICENCAS";
        private static String EVENT_LOG = "Application";
        private static String listaEmail = ConfigurationManager.AppSettings["listaEmailNotificacaoFalha"];
        private static bool gravarLog = Convert.ToBoolean(ConfigurationManager.AppSettings["gravarLog"]);

        static void Main(string[] args)
        {
            try
            {
                ExpirarLicencas();
            }
            catch (Exception ex)
            {
                if (gravarLog)
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter("GS1.CNP.JOB.EXPIRALICENCAS." + DateTime.Now.ToString("yyyy_MM") + ".txt", true);
                    file.WriteLine(DateTime.Now.ToString() + " - GS1.CNP.JOB.EXPIRALICENCAS - ExpirarLicencas - Erro ao Executar: \n\n" + ex.Message + " Stack: " + ex.StackTrace + "\r");
                    file.Close();
                }
            }
        }

        public static void ExpirarLicencas()
        {
            try
            {
                SqlConnection thisConnection = new SqlConnection(STRCON);
                thisConnection.Open();
                SqlCommand thisCommand = thisConnection.CreateCommand();

                thisCommand.CommandType = CommandType.StoredProcedure;
                thisCommand.CommandText = "PROC_EXPIRALICENCAS";
                SqlDataReader thisReader = thisCommand.ExecuteReader();


                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.JOB.EXPIRALICENCAS - ExpirarLicencas - Executado com sucesso.", EventLogEntryType.Information, 1);
                }
                catch (Exception)
                {

                }

                try
                {
                    if (gravarLog)
                    {
                        System.IO.StreamWriter file = new System.IO.StreamWriter("GS1.CNP.JOB.EXPIRALICENCAS." + DateTime.Now.ToString("yyyy_MM") + ".txt", true);
                        file.WriteLine(DateTime.Now.ToString() + " - GS1.CNP.JOB.EXPIRALICENCAS - ExpirarLicencas - Executado com sucesso.\r");
                        file.Close();
                    }
                    
                }
                catch (Exception)
                {
                }


            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.JOB.EXPIRALICENCAS - ExpirarLicencas - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);
                }
                catch (Exception)
                {
                }


                try
                {
                    if (gravarLog)
                    {
                        System.IO.StreamWriter file = new System.IO.StreamWriter("GS1.CNP.JOB.EXPIRALICENCAS." + DateTime.Now.ToString("yyyy_MM") + ".txt", true);
                        file.WriteLine(DateTime.Now.ToString() + " - GS1.CNP.JOB.EXPIRALICENCAS - ExpirarLicencas - Erro ao Executar: \n\n" + ex.Message + " Stack: " + ex.StackTrace + "\r");
                        file.Close();
                    }
                    
                }
                catch (Exception)
                {
                }


                try
                {
                    EnviarEmail(listaEmail, "GS1.CNP.JOB.EXPIRALICENCAS - ExpirarLicencas - Erro ao Execeutar", "GS1.CNP.JOB.EXPIRALICENCAS - ExpirarLicencas - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, true);
                }
                catch (Exception)
                {
                }

            }
            
        }

        public static void EnviarEmail(string email, string assunto, string mensagem, bool idfHtml)
        {
            try
            {
                //Configurações de envio de email
                MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["remetente"], email);
                SmtpClient client = new SmtpClient();

                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["portSMTP"]);
                client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["enableSSL"]);

                if (ConfigurationManager.AppSettings["pass"] != "")
                {
                    NetworkCredential basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["pass"]);
                    client.UseDefaultCredentials = false;
                    client.Credentials = basicCredential;
                }
                else //Relay
                {
                    client.UseDefaultCredentials = true;
                }

                client.Host = ConfigurationManager.AppSettings["hostSMTP"];

                //Envio de email
                mail.Subject = assunto;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;


                if (idfHtml)
                {
                    AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + mensagem + "</body></html>", null, MediaTypeNames.Text.Html);
                    mail.AlternateViews.Add(av1);
                    mail.IsBodyHtml = true;
                }
                else
                {
                    mail.Body = mensagem;
                }

                client.Send(mail);

            }
            catch (SmtpException smtpex)
            {
                throw new Exception("Erro ao enviar email. Falha de comunicação com o servidor.");

            }
            catch (Exception)
            {
                throw new Exception("Falha ao enviar email. Consulte o Administrador.");
            }

        }
    }
}
