﻿using System;
using GS1.CNP.BLL;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Diagnostics;
using System.Net.Mime;
using System.Dynamic;

namespace GS1.CNP.JOB.RELATORIO
{
    class Program
    {
        private static String EVENT_SOURCE = "GS1.CNP.JOB.RELATORIO";
        private static String EVENT_LOG = "Application";
        private static String STRCON = ConfigurationManager.ConnectionStrings["BD"].ToString();
        private static String listaEmail = ConfigurationManager.AppSettings["listaEmailNotificacaoFalha"];

        static void Main(string[] args)
        {
            try
            {
                RelatorioMensalCNPBO relatorio = new RelatorioMensalCNPBO();

                var resultado = relatorio.BuscarParametro("email.envio.relatorio.mensal");

                if (resultado != null)
                {
                    string html = relatorio.relatorioMensalCNPJOB();

                    if (!string.IsNullOrEmpty(html))
                        EnviarEmail(resultado.valor, "Relatório Mensal - CNP", html, true);
                }

                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, EVENT_SOURCE + " executado com sucesso.", EventLogEntryType.Information, 1);
                }
                catch (Exception)
                {
                }

                try
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter(EVENT_SOURCE + "." + DateTime.Now.ToString("yyyy_MM") + ".txt", true);
                    file.WriteLine(DateTime.Now.ToString() + " - " + EVENT_SOURCE + " - Executado com sucesso.\r");
                    file.Close();
                }
                catch (Exception)
                {
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, EVENT_SOURCE + " - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);
                }
                catch (Exception)
                {
                }

                try
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter(EVENT_SOURCE + "." + DateTime.Now.ToString("yyyy_MM") + ".txt", true);
                    file.WriteLine(DateTime.Now.ToString() + " - " + EVENT_SOURCE + " - Erro ao Executar: \n\n" + ex.Message + " Stack: " + ex.StackTrace + "\r");
                    file.Close();
                }
                catch (Exception)
                {
                }

                try
                {
                    EnviarEmail(listaEmail, EVENT_SOURCE + " - Erro ao Execeutar", EVENT_SOURCE + " - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, true);
                }
                catch (Exception)
                {
                }
            }
        }

        public static void EnviarEmail(string email, string assunto, string mensagem, bool idfHtml)
        {
            try
            {
                //Configurações de envio de email
                MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["remetente"], email);
                SmtpClient client = new SmtpClient();

                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["portSMTP"]);
                client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["enableSSL"]);

                if (ConfigurationManager.AppSettings["pass"] != "")
                {
                    NetworkCredential basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["pass"]);
                    client.UseDefaultCredentials = false;
                    client.Credentials = basicCredential;
                }
                else //Relay
                {
                    client.UseDefaultCredentials = true;
                }

                client.Host = ConfigurationManager.AppSettings["hostSMTP"];

                //Envio de email
                mail.Subject = assunto;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;



                if (idfHtml)
                {
                    AlternateView av1 = AlternateView.CreateAlternateViewFromString("<html><body>" + mensagem + "</body></html>", null, MediaTypeNames.Text.Html);
                    mail.AlternateViews.Add(av1);
                    mail.IsBodyHtml = true;
                }
                else
                {
                    mail.Body = mensagem;
                }

                client.Send(mail);

            }
            catch (SmtpException)
            {
                throw new Exception("Erro ao enviar email. Falha de comunicação com o servidor.");

            }
            catch (Exception)
            {
                throw new Exception("Falha ao enviar email. Consulte o Administrador.");
            }

        }
    }
}
