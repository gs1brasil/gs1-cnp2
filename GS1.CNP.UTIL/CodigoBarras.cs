﻿using System;
using System.Collections.Generic;
using Vintasoft.Barcode;
using Vintasoft.Barcode.SymbologySubsets;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using Newtonsoft.Json.Linq;


namespace GS1.CNP.UTIL
{
    public class CodigoBarras
    {
        public byte[] GeraEtiqueta(dynamic parametros)
        {
            try
            {
                if (parametros != null && parametros.where.codigotipocodigobarras != null)
                {
                    double largura;
                    double altura;
                    double ajustealtura = 0;

                    if (parametros.where.larguracodigobarras == null || parametros.where.alturacodigobarras == null)
                        throw new Exception("Não foi possível exibir a imagem da etiqueta. Dimensões Inválidas.");

                    largura = (double)parametros.where.larguracodigobarras.Value;
                    altura = (double)parametros.where.alturacodigobarras.Value;

                    BarcodeGlobalSettings.RegisterBarcodeWriter("Softbox Sistemas de Inf Ltda", "cnp.gs1br.org","FB4BScaXNeBDIgd2McLqUzTLOWQi4NXwq7TA41nVpU5GpZqxFeb+iJR94AvDmYgZFtb4c4QnkXcp9TnJWDIUg0iAxaPPsiKHZIT+av6L4uZdC6zZnPjKXcBDLJFXx7+FJGoWwP4LCpE+PCbk3hOvw1g2O+KZSXxf52m/Am2X+Tew");
                    //BarcodeGlobalSettings.RegisterBarcodeWriter("Softbox Sistemas de Inf Ltda (For test purposes only)", "piloto.cadastronacionaldeprodutos.com.br", "eBTY/c3uw674fsQOjPVku5YZBKpikjNPaAK+eynFUj47xhkxwQmbLGhd7RSpJipoRCohiVvXuDKJ5yb1llGUVgoixVaVgbCTsjRFS0AxYsE1QCiCaDqxfhQg3vGE8UT81Er+Ws3HlrWItVH4GP02C48Wkv53cFhbEBwQkEeJqcKs");
                    BarcodeWriter barcodeWriter = new BarcodeWriter();
                    BarcodeSymbologySubset barcodeSymbologySubset = null;
                    string data = string.Empty;
                    barcodeWriter.Settings.MinWidth = 2;
                    //barcodeWriter.Settings.SetMinWidth(0.330, UnitOfMeasure.Millimeters);
                    barcodeWriter.Settings.BarsWidthAdjustment = 0;
                    barcodeWriter.Settings.Padding = 0;

                    //barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);                    
                    //barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura), UnitOfMeasure.Millimeters);

                    if (parametros.where.codigotipocodigobarras.Value == 6)
                    {
                        barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.EAN8;
                        data = Convert.ToString(parametros.where.codigogtin.Value).Substring(0, 7);
                        barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura) - (0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                    }

                    if (parametros.where.codigotipocodigobarras.Value == 2)
                    {
                        barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.EAN13;
                        data = Convert.ToString(parametros.where.codigogtin.Value).Substring(0, 12);
                        ajustealtura = 5;
                        barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                        barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura) + (0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                    }

                    if (parametros.where.codigotipocodigobarras.Value == 24)
                    {
                        barcodeSymbologySubset = BarcodeSymbologySubsets.ITF14;
                        data = Convert.ToString(parametros.where.codigogtin.Value);
                    }

                    if (parametros.where.codigotipocodigobarras.Value == 26)
                    {
                        barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.UPCA;
                        data = Convert.ToString(parametros.where.codigogtin.Value);
                        data = "0" + data.Substring(0, data.Length - 1);
                        ajustealtura = 5;
                        barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                    }

                    if (parametros.where.codigotipocodigobarras.Value == 30)
                    {
                        barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.UPCE;
                        string upce = ConvertUPCAtoUPCE("0" + Convert.ToString(parametros.where.codigogtin.Value).Substring(0, data.Length - 1));
                        if (upce == "-1" || upce == "-2")
                            return null;

                        data = upce;
                    }



                    if (parametros.where.codigotipocodigobarras.Value == 1)
                    {

                        //barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.Code128;
                        barcodeSymbologySubset = BarcodeSymbologySubsets.GS1_128;
                        //barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.;


                        if (parametros.where.modelo128 == 1) //Padrão
                        {
                            string dataVencimento = DateTime.ParseExact(Convert.ToString(parametros.where.datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                            if (parametros.where.codigotipogtin == 3)
                            {
                                data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                            }

                            if (parametros.where.codigotipogtin == 4)
                            {
                                //data = "(02)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote + "(37)" + parametros.where.quantidadeporcaixa;
                                //modelo sem SSCC
                                data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                            }

                            ajustealtura = 5;
                            barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                            barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura) + (0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                            barcodeWriter.Settings.ValueFont = new System.Drawing.Font("Arial", 20, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);

                        } else { // Anatel
                            ajustealtura = 5;
                            data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0');
                            barcodeWriter.Settings.ValueFont = new System.Drawing.Font("Arial", 16, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
                            barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                        }                                               
                    }


                    if (parametros.where.codigotipocodigobarras.Value == 10)
                    {                        
                        string dataVencimento = DateTime.ParseExact(Convert.ToString(parametros.where.datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyMMdd", System.Globalization.CultureInfo.InvariantCulture);                        
                        barcodeSymbologySubset = BarcodeSymbologySubsets.GS1DataBarExpanded;
                        barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.RSSExpanded;
                        //data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                        data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(10)" + parametros.where.nrlote;
                        barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                        //barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura) + (0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                    }

                    if (parametros.where.codigotipocodigobarras.Value == 12)
                    {
                        string dataVencimento = DateTime.ParseExact(Convert.ToString(parametros.where.datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        barcodeSymbologySubset = BarcodeSymbologySubsets.GS1DataBarExpandedStacked;
                        barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.RSSExpandedStacked;
                        //data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                        data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(10)" + parametros.where.nrlote;
                        barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                        //barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura) + (0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                        barcodeWriter.Settings.ValueFont = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
                        //zbarcodeWriter.Settings.MinWidth = 3;
                    }

                    if (parametros.where.codigotipocodigobarras.Value == 18)
                    {                        
                        barcodeSymbologySubset = BarcodeSymbologySubsets.GS1DataBarStacked;
                        barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.RSS14Stacked;
                        data = Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0');
                        barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                        barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura) + (0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                        barcodeWriter.Settings.ValueFont = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
                        barcodeWriter.Settings.Padding = 2;
                    }


                    if (parametros.where.codigotipocodigobarras.Value == 9)
                    {
                        string dataVencimento = DateTime.ParseExact(Convert.ToString(parametros.where.datavencimento.Value), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        string registroanisa = "";
                        if (parametros.where.codigohomologacao != null)
                        {
                            registroanisa = parametros.where.codigohomologacao[0].alternateitemidentificationid.Value;
                        }

                        string codigoserial = "";
                        if (parametros.where is JToken)
                        {
                            if (parametros.where.codigoserial != null)
                            {
                                codigoserial = parametros.where.codigoserial.Value;
                                data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(713)" + registroanisa + "(21)" + codigoserial + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                            }
                            else if (parametros.where.serialinicial != null)
                            {
                                data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(713)" + registroanisa + "(21)" + parametros.where.serialinicial.Value + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                            }
                            else
                            {
                                data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(713)" + registroanisa + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                            }
                        }
                        else if (!(parametros.where is JToken))
                        {
                            if (((IDictionary<String, object>)parametros.where).ContainsKey("codigoserial"))
                            {
                                codigoserial = parametros.where.codigoserial.ToString();
                                data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(713)" + registroanisa + "(21)" + codigoserial + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                            }
                            else if (((IDictionary<String, object>)parametros.where).ContainsKey("serialinicial"))
                            {
                                data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(713)" + registroanisa + "(21)" + parametros.where.serialinicial.Value + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                            }
                            else
                            {
                                data = "(01)" + Convert.ToString(parametros.where.codigogtin.Value).PadLeft(14, '0') + "(713)" + registroanisa + "(17)" + dataVencimento + "(10)" + parametros.where.nrlote;
                            }
                        }
                        
                       
                        barcodeSymbologySubset = BarcodeSymbologySubsets.GS1DataMatrix;
                        barcodeWriter.Settings.Barcode = Vintasoft.Barcode.BarcodeType.DataMatrix;
                        //data = "(01)07898357410022(713)A123467891234(21)12345678901234(17)010122(10)asdasd";
                        

                        //barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                        //barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura) + (0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                        barcodeWriter.Settings.ValueFont = new System.Drawing.Font("Arial", 10, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
                        barcodeWriter.Settings.Padding = 2;
                        //barcodeWriter.Settings.SetMinWidth(0.396, UnitOfMeasure.Millimeters);
                    }


                    // set the value of barcode
                    if (barcodeSymbologySubset != null)
                    {
                        barcodeSymbologySubset.Encode(data, barcodeWriter.Settings);
                    }
                    else
                    {
                        barcodeWriter.Settings.Value = data;
                    }


                    //barcodeWriter.Settings.Padding = 0;
                    barcodeWriter.Settings.PixelFormat = Vintasoft.Barcode.BarcodeImagePixelFormat.Bgr24;
                    //barcodeWriter.Settings.SetHeight(Convert.ToDouble(altura) + ajustealtura, UnitOfMeasure.Millimeters);
                    ////barcodeWriter.Settings.SetWidth(Convert.ToInt32(largura) - (int)Math.Ceiling(0.33 * (double)parametros.where.leftguard) - (int)Math.Ceiling(0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                    ////barcodeWriter.Settings.SetWidth(Convert.ToInt32(largura) + (int)Math.Ceiling(0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);
                    //barcodeWriter.Settings.SetWidth(Convert.ToDouble(largura) + (0.33 * (double)parametros.where.rightguard), UnitOfMeasure.Millimeters);

                    //barcodeWriter.Settings.BackColor = Color.Gray;
                    //barcodeWriter.Settings.ForeColor = Color.White;

                    if (parametros.where.fontenome != null && parametros.where.fontetamanho != null)
                    {
                        barcodeWriter.Settings.ValueFont = new System.Drawing.Font(Convert.ToString(parametros.where.fontenome.Value), Convert.ToInt32(parametros.where.fontetamanho.Value), System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
                    }

                    System.Drawing.Image imageWithBarcode = null;

                    if (parametros.where.codigotipocodigobarras.Value == 24)
                    {
                        imageWithBarcode = GenerateITF14WithBearerBars(data, 2);
                    }
                    else
                    {
                        System.Drawing.Image imageWithBarcodeWG = barcodeWriter.GetBarcodeAsBitmap();
                        int quietzoneright = (int)Math.Ceiling(((double)parametros.where.idealmodulewidth * (double)parametros.where.rightguard) * 300 / 254);

                        imageWithBarcode = new Bitmap(imageWithBarcodeWG.Width + quietzoneright, imageWithBarcodeWG.Height);
                        using (Graphics g = Graphics.FromImage(imageWithBarcode))
                        {
                            g.Clear(barcodeWriter.Settings.BackColor);
                            g.DrawImage(imageWithBarcodeWG, new Point(0, 0));
                        }
                    }

                    byte[] byteArray = new byte[0];
                    using (MemoryStream stream = new MemoryStream())
                    {
                        imageWithBarcode.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.Close();
                        return stream.ToArray();
                    }
                }
                else
                {
                    throw new Exception("Não foi possível exibir a imagem da etiqueta.");
                }

            }
            catch (Exception ex)
            {

                throw new Exception("Não foi possível exibir a imagem da etiqueta.", ex);
            }
        }

        public static Bitmap GenerateITF14WithBearerBars(string value, int moduleWidth)
        {

            // create barcode writer
            BarcodeWriter writer = new BarcodeWriter();
            writer.Settings.PixelFormat = BarcodeImagePixelFormat.Bgr24;
            writer.Settings.MinWidth = moduleWidth;
            writer.Settings.Padding = 13; //1X            
            writer.Settings.ValueGap = 20;
            writer.Settings.SetHeight(32, UnitOfMeasure.Millimeters);                        
            writer.Settings.SetWidth(99.9, UnitOfMeasure.Millimeters);            
            //writer.Settings.BackColor = Color.Khaki;
            writer.Settings.ValueFont = new System.Drawing.Font(FontFamily.GenericSansSerif, 20);

            // encode ITF14 barcode
            BarcodeSymbologySubsets.ITF14.Encode(value, writer.Settings);

            // generate barcode without barier bars
            using (Bitmap barcodeWithoutBarierBars = writer.GetBarcodeAsBitmap())
            {

                int dw = 19;            
                int ajustealtura = 15;
                Bitmap result = new Bitmap(barcodeWithoutBarierBars.Width + (dw * 2), barcodeWithoutBarierBars.Height - (dw * 2) - ajustealtura);
                using (Graphics g = Graphics.FromImage(result))
                {
                    g.Clear(writer.Settings.BackColor);
                    //g.Clear(Color.Cornsilk);                    
                    int xbwb = 19;
                    int ybwb = -20;
                    g.DrawImage(barcodeWithoutBarierBars, new Point(xbwb, ybwb));
                    int bw = 19;
                    int bw2 = 19;

                    using (Pen pen = new Pen(writer.Settings.ForeColor, bw))
                    {
                        pen.Alignment = PenAlignment.Inset;                     
                        int xi = 9;
                        int yi = 19;
                        int xf = result.Width - (bw/2);
                        int yf = 150;                        
                        g.DrawLine(pen, new Point(xi, yi), new Point(xi, yf));
                        g.DrawLine(pen, new Point(xf, yi), new Point(xf, yf));
                    }

                    using (Pen pen = new Pen(writer.Settings.ForeColor, bw2))
                    {
                        pen.Alignment = PenAlignment.Inset;
                        int xi = 0;
                        int yi = 9;
                        int xf = 600;
                        int yf = 149;                        
                        g.DrawLine(pen, new Point(xi, yi), new Point(xf, yi));
                        g.DrawLine(pen, new Point(xi, yf), new Point(xf, yf));
                    }
                }
                return result;
            }
        }

        public static Bitmap GenerateITF14WithBearerBars_New(string value, int moduleWidth)
        {
            // 2X barier bar, minimum - 2X
            const int barierBarWidth = 4;
            // 10X quietZone, minimum - 10X
            const int quietZone = 10;
            const float tamanhoBearerBar = 4.8F;

            // create barcode writer
            BarcodeWriter writer = new BarcodeWriter();
            writer.Settings.PixelFormat = BarcodeImagePixelFormat.Bgr24;
            writer.Settings.MinWidth = moduleWidth;
            writer.Settings.Padding = 13; //1X
            //writer.Settings.ValueGap = barierBarWidth * moduleWidth;
            writer.Settings.ValueGap = 45;

            //writer.Settings.SetHeight(90, UnitOfMeasure..Millimeters);
            //writer.Settings.SetWidth(32, UnitOfMeasure.Millimeters);
            writer.Settings.SetHeight(338, UnitOfMeasure.Pixels);
            writer.Settings.SetWidth(121, UnitOfMeasure.Pixels);

            //writer.Settings.BackColor = Color.Khaki;
            writer.Settings.ValueFont = new System.Drawing.Font(FontFamily.GenericSansSerif, 20);

            // encode ITF14 barcode
            BarcodeSymbologySubsets.ITF14.Encode(value, writer.Settings);

            // generate barcode without barier bars
            //using (Bitmap barcodeWithoutBarierBars = writer.GetBarcodeAsBitmap())

            int sizex = 338;
            int sizey = 255; //int sizey = 250;

            using (Bitmap barcodeWithoutBarierBars = new Bitmap(writer.GetBarcodeAsBitmap(), new Size(sizex, sizey)))
            //using (Bitmap barcodeWithoutBarierBars = new Bitmap(writer.GetBarcodeAsBitmap(), new Size(80, 32)))
            {
                //int dw = moduleWidth * (quietZone - 1 + barierBarWidth) * 2;
                int dw = 19; //(int)Math.Floor(tamanhoBearerBar * 2);

                //int dh = moduleWidth * (barierBarWidth - 1) * 2;
                //int dh = 19;  //(int)Math.Floor(tamanhoBearerBar * 2);
                int dh = 5;

                Bitmap result = new Bitmap(barcodeWithoutBarierBars.Width + dw, barcodeWithoutBarierBars.Height + dh);

                using (Graphics g = Graphics.FromImage(result))
                {
                    g.Clear(writer.Settings.BackColor);
                    //g.Clear(Color.Cornsilk);
                    //g.DrawImage(barcodeWithoutBarierBars, new Point(dw / 2, -13));

                    int pointx = 10;
                    int pointy = 5;

                    g.DrawImage(barcodeWithoutBarierBars, new Point(pointx, pointy));
                    //g.DrawImage(barcodeWithoutBarierBars, new Point(10, 5));

                    //float bw = moduleWidth * barierBarWidth;
                    //float bw = (4.8F * 96 )/ 25.4F; //Força Magnitude especificada - transforma em pixel
//                    float bw = 19.14F; //Força Magnitude especificada - transforma em pixel
                    //float bw = 19.44F;
                    float bw = 43F;
                    float posx = -8;//bw / 2f; // 
                    //float posy = -8; //20.2F;
                    float posy = -3;
                    float w = result.Width + 15; //- bw;
                    //float h = result.Height + 15;//writer.Settings.Height + 28;

                    //float h = 220;
                    float h = 224;

                    using (Pen pen = new Pen(writer.Settings.ForeColor, bw))
                    {
                        pen.Alignment = PenAlignment.Center;
                        //g.DrawRectangle(pen, bw / 2f, bw / 2f, result.Width - bw, writer.Settings.Height + 1);
                        g.DrawRectangle(pen, posx, posy, w, h);
                        //g.DrawRectangle(pen, bw / 2f, bw / 2f, result.Width - bw, (((32F) * 96F) / 25.4F) + (bw*2));
                    }

                }
                return result;
            }
        }

        private Rectangle GetConstrainedTextHeight(string textToParse, int quoteAreaWidth, Font font, ref string resultText)
        {
            // create a new bitmap - I don't knowe if the size matters, but just to 
            // be safe, I set it to be larger than the expected height, and the max 
            // area width
            Bitmap bitmap = new Bitmap(100, quoteAreaWidth);

            // create a graphics object from the image. This gives us access to the 
            // GDI drawing functions needed to do what we're here to do.
            Graphics g = Graphics.FromImage(bitmap);

            SizeF sz = g.MeasureString(textToParse, font);

            // Make sure we actually have work to do. If the quote width is smaller 
            // than the desired max width, we can exit right now. This should almost 
            // always happen for author text.
            if (sz.Width <= quoteAreaWidth)
            {
                resultText = textToParse;

                // don't forget to clean up our resources
                g.Dispose();
                bitmap.Dispose();
                return new Rectangle(new Point(0, 0), new Size((int)sz.Width, (int)sz.Height));
            }

            // make sure our resultText is empty
            resultText = "";

            // split the orginal text into separate words
            string[] words = textToParse.Trim().Split(' ');
            string nextLine = "";
            string word = "";

            for (int i = 0; i < words.Length; i++)
            {
                word = words[i];


                // get the size of the current line
                SizeF lineSize = g.MeasureString(nextLine + 1, font);

                // get the size ofthe new word
                SizeF wordSize = g.MeasureString(" " + word, font);

                // if the line including the new word is smaller than our constrained size
                if (lineSize.Width + wordSize.Width < quoteAreaWidth)
                {
                    // add the word to the line
                    nextLine = string.Format("{0} {1}", nextLine, word);

                    // If it's the last word in the original text, add the line 
                    // to the resultText
                    if (i == words.Length - 1)
                    {
                        resultText += nextLine;

                    }
                }
                else
                {
                    // Add the current line to the resultText *without* the new word, 
                    // but with a linefeed
                    resultText += (nextLine + "\n");

                    // Start a new current line
                    nextLine = word;


                    // If it's the last word in the original text, add the line 
                    // to the resultText
                    if (i == words.Length - 1)
                    {
                        resultText += nextLine;

                    }
                }
            }

            // It's time to get a new measurement for the string. The Graphics.MeasureString 
            // method takes into account the linefeed characters we inserted.
            sz = g.MeasureString(resultText, font);

            // Cleanup our resources
            g.Dispose();
            bitmap.Dispose();

            // Return the rectangle to the calling method - Alt satir burada kesiliyor. Dikkat et.
            //test  new Rectangle(new Point(0, 0), new Size((int)sz.Width, (int)sz.Height + 2*FontSize + FontSize/3));
            return new Rectangle(new Point(0, 0), new Size((int)sz.Width, (int)sz.Height));
        }



        public string ConvertUPCAtoUPCE(string UPCA)
        {
            string holdString;
            string UPCE;

            if ((UPCA.Length < 12))
            {
                holdString = ("000000000000" + UPCA);
                UPCA = holdString.Substring((holdString.Length - 12));
            }

            if (((UPCA.Substring(0, 1) != "0")
                        && (UPCA.Substring(0, 1) != "1")))
            {

                return "-1";
            }
            else if (((UPCA.Substring(3, 3) == "000")
                        || ((UPCA.Substring(3, 3) == "100")
                        || (UPCA.Substring(3, 3) == "200"))))
            {
                UPCE = (UPCA.Substring(1, 2)
                            + (UPCA.Substring(8, 3) + UPCA.Substring(3, 1)));
            }
            else if ((UPCA.Substring(4, 2) == "00"))
            {
                UPCE = (UPCA.Substring(1, 3)
                            + (UPCA.Substring(9, 2) + "3"));
            }
            else if ((UPCA.Substring(5, 1) == "0"))
            {
                UPCE = (UPCA.Substring(1, 5)
                            + (UPCA.Substring(10, 1) + "4"));
            }
            else if ((double.Parse(UPCA.Substring(10, 1)) >= 5))
            {
                UPCE = (UPCA.Substring(1, 5) + UPCA.Substring(10, 1));
            }
            else
            {
                return "-2";
            }

            return UPCE;
        }



    }
}

