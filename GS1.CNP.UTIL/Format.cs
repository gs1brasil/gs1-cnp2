﻿using System;

namespace GS1.CNP.UTIL
{
    public class Format
    {
        public static string formataCPFCNPJ(string cpfcnpj)
        {
            try
            {
                cpfcnpj = cpfcnpj.Replace(".", "").Replace("-", "").Replace("/", "");

                Int64 outcpfcnpj;
                if (!(Int64.TryParse(cpfcnpj, out outcpfcnpj)))
                    return string.Empty;


                if (cpfcnpj.Length == 14)
                {
                    return Convert.ToUInt64(cpfcnpj).ToString(@"00\.000\.000\/0000\-00");
                }
                else if (cpfcnpj.Length == 11)
                {
                    return Convert.ToUInt64(cpfcnpj).ToString(@"000\.000\.000\-00");
                }

                return cpfcnpj;
            }
            catch (Exception)
            {

                return string.Empty;
            }
           

        }
    }
}
