﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace GS1.CNP.WEBSERVICE.CNPSERVICE
{
    /// <summary>
    /// Summary description for CNPServices
    /// </summary>
    [WebService(Namespace = "http://www.gs1br.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CNPServices : System.Web.Services.WebService
    {
        public CNPRequestHeader cnpRequestHeader;


        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public string AuthenticateUser()
        {
            if (cnpRequestHeader == null)
                return "Please provide a Username and Password";
            if (string.IsNullOrEmpty(cnpRequestHeader.Username) || string.IsNullOrEmpty(cnpRequestHeader.Password))
                return "Please provide a Username and Password";

            // Are the credentials valid?
            if (!IsUserValid(cnpRequestHeader.Username, cnpRequestHeader.Password))
                return "Invalid Username or Password";

            // Create and store the AuthenticatedToken before returning it
            string token = Guid.NewGuid().ToString();

            HttpRuntime.Cache.Add(
                token,
                cnpRequestHeader.Username,
                null,
                System.Web.Caching.Cache.NoAbsoluteExpiration,
                TimeSpan.FromMinutes(60),
                System.Web.Caching.CacheItemPriority.NotRemovable,
                null);

            return token;
        }

        private bool IsUserValid(string Username, string Password)
        {
            // Ask the SQL Memebership to verify the credentials for us
            //   return System.Web.Security.Membership.ValidateUser(Username, Password);
            return true;
        }

        private bool IsUserValid(CNPRequestHeader SoapHeader)
        {
            if (SoapHeader == null)
                return false;

            // Does the token exists in our Cache?
            if (!string.IsNullOrEmpty(SoapHeader.AuthenticatedToken))
                return (HttpRuntime.Cache[SoapHeader.AuthenticatedToken] != null);

            return false;
        }


    }
}
