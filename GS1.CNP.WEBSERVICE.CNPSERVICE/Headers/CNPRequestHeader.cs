﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GS1.CNP.WEBSERVICE.CNPSERVICE
{
    public class CNPRequestHeader : System.Web.Services.Protocols.SoapHeader
    {
        public string Username;
        public string Password;
        public string AuthenticatedToken;
    }
}