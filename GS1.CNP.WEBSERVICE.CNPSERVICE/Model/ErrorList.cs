﻿

namespace GS1.CNP.WEBSERVICE.CNPSERVICE
{
    public enum ErrorList
    {
        AUTH_FAIL = 01,
        INVALID_TOKEN = 02
    }
}