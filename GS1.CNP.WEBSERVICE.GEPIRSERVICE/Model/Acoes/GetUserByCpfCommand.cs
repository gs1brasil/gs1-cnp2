﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Dynamic;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Acoes
{
    public class GetUserByCpfCommand : IComputationCommand<string>
    {
        public object Execute(string cpf)
        {

            string sql_usuario = @" SELECT CODIGO, NOME, CPFCNPJ, EMAIL, SENHA, CODIGOPERFIL, QUANTIDADETENTATIVA,
	                                        DATAALTERACAO,
	                                        DATEADD(day, CONVERT(INT, (SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.senha.nudias.validade')), DATAALTERACAOSENHA) AS DATAEXPIRASENHA,
	                                        CODIGOSTATUSUSUARIO, TELEFONERESIDENCIAL, TELEFONECELULAR, TELEFONECOMERCIAL,
	                                        RAMAL, DEPARTAMENTO, CARGO 
	                                        FROM USUARIO
                                            WHERE CPFCNPJ = @cpf";

            string sql_associado = @" SELECT CODIGO, CAD, NOME, DESCRICAO, EMAIL, CPFCNPJ, INSCRICAOESTADUAL, 
	                                        CODIGOTIPOASSOCIADO, MENSAGEMOBSERVACAO, DATAALTERACAO, DATAATUALIZACAOCRM,
	                                        DATASTATUSINADIMPLENTE, CODIGOUSUARIOALTERACAO, ENDERECO, NUMERO, COMPLEMENTO,
	                                        BAIRRO, CIDADE, UF, CEP, CODIGOTIPOCOMPARTILHAMENTO 
	                                        FROM ASSOCIADO
                                            WHERE CODIGO = @codigoassociado";

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                dynamic param_usuario = new ExpandoObject();
                param_usuario.cpf = cpf;

                CNP.BLL.Model.Associado associado = new CNP.BLL.Model.Associado();
                associado = (CNP.BLL.Model.Associado)HttpContext.Current.Session["ASSOCIADO_SELECIONADO"];

                db.Open();

                var usuario = ((List<dynamic>)db.Select(sql_usuario, param_usuario)).FirstOrDefault();

                if (usuario == null)
                    return null;

                if (!usuario.IdAssociado.HasValue)
                    return null;

                dynamic param_associado = new ExpandoObject();
                param_associado.codigoassociado = associado.codigo.ToString();

                var result = db.Select(sql_associado, param_associado); 

                usuario.Senha = null;

                var user = new User
                {
                    Usuario = usuario,
                    Associado = result
                };

                return user;

            }
            
        }
    }
}