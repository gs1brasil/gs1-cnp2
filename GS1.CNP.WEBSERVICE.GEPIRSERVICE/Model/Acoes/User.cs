﻿using GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Acoes
{
    public class User
    {
        public Usuario Usuario { get; set; }
        public Associado Associado { get; set; }
    }
}