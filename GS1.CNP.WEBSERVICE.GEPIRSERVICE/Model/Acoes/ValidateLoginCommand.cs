﻿using System;
using System.Collections.Generic;
using GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core;
using System.Data;
using System.Dynamic;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Acoes
{
    public class ValidateLoginCommand : IComputationCommand<ValidateLoginRequest>
    {
        public object Execute(ValidateLoginRequest request)
        {
            if (!(string.IsNullOrEmpty(request.Password) || request.Password.Equals("SENHA")))
            {

                string sql_usuario = @" SELECT CODIGO, NOME, CPFCNPJ, EMAIL, SENHA,  CODIGOPERFIL, 
	                                        QUANTIDADETENTATIVA, DATAALTERACAO,
	                                        DATEADD(day, CONVERT(INT, (SELECT VALOR FROM PARAMETRO WHERE CHAVE = 'autenticacao.senha.nudias.validade')), DATAALTERACAOSENHA) AS DATAEXPIRASENHA,
	                                        CODIGOSTATUSUSUARIO, TELEFONERESIDENCIAL, TELEFONECELULAR,
	                                        TELEFONECOMERCIAL, RAMAL, DEPARTAMENTO, CARGO 
	                                        FROM USUARIO";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    Usuario user = new Usuario();
                    dynamic param_usuario = new ExpandoObject();
                    param_usuario.email = request.Email;
                    param_usuario.senha = PasswordHelper.GetHashedPassword((request.Password.Length > 10) ? request.Password.Substring(0, 10) : request.Password);

                    db.Open();

                    string sql_licenca = "";
                    dynamic param_licenca = new ExpandoObject();
                    param_licenca.idassociado = "";

                    Func<IDataRecord, Usuario> binder = r => new Usuario()
                    {
                        IdUsuario = r.GetInt64(0),
                        NomeCompleto = r.GetString(1),
                        CpfCnpj = r.GetString(2),
                        Email = r.GetString(3),
                        Senha = r.GetString(4),
                        IdPerfil = r.GetInt32(5),
                        TentativaAcesso = r.GetInt32(6),
                        DtExpiraSenha = r.GetDateTime(7),
                        Status = r.GetString(8),
                        TelResidencial = r.GetString(9),
                        TelCelular = r.GetString(10),
                        TelComercial = r.GetString(11),
                        Ramal = r.GetString(12),
                        Departamento = r.GetString(13),
                        Cargo = r.GetString(14)
                        //Licencas = listLicencas
                    };

                    List<Usuario> usuarios = db.Select<Usuario>(sql_usuario, param_usuario, binder);
                    //List<AssociadoLicenca> listLicencas = db.Select<AssociadoLicenca>(sql_usuario, param_usuario, binder);
                    
                    //Licenses = reader1.GetValues(0);

                    //SqlDataReader reader1 = db1.ExecuteReader();
                    //user.IdUsuario = reader.GetInt64(0);
                    //user.NomeCompleto = reader.GetString(1);
                    //user.CpfCnpj = reader.GetString(2);
                    //user.Email = reader.GetString(3);
                    //user.Senha = reader.GetString(4);
                    //user.IdPerfil = reader.GetInt32(5);
                    //user.TentativaAcesso = reader.GetInt32(6);
                    //user.DtExpiraSenha = reader.GetDateTime(7);
                    //user.Status = reader.GetString(8);
                    //user.TelResidencial = reader.GetString(9);
                    //user.TelCelular = reader.GetString(10);
                    //user.TelComercial = reader.GetString(11);
                    //user.Ramal = reader.GetString(12);
                    //user.Departamento = reader.GetString(13);
                    //user.Cargo = reader.GetString(14);

                    //user.Licenses = reader1.GetValues(0);

                    return user;
                }

            }

            return false;
        }
    }

    public class ValidateLoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}