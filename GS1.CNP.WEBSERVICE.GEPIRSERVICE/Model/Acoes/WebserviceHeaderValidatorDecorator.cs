﻿using System;
using System.Linq;
using System.Configuration;
using GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Acoes
{
    public class WebserviceHeaderValidatorDecorator<T> : IComputationCommand<T>
    {
        private readonly IComputationCommand<T> _command;
        private readonly CnpServicesHeader _header;
        private readonly string _allowedGLN;
        private readonly string[] _allowedUsers;

        public WebserviceHeaderValidatorDecorator(IComputationCommand<T> command, CnpServicesHeader header)
        {
            _command = command;
            _header = header;
            _allowedGLN = ConfigurationManager.AppSettings["SearchAllowedGLN"];
            _allowedUsers = ConfigurationManager.AppSettings["WSUSER"].Split(';');
        }

        public object Execute(T request)
        {
            ValidateHeader();
            return _command.Execute(request);
        }

        private void ValidateHeader()
        {
            if (!_header.RequesterGln.Equals(_allowedGLN) || !_allowedUsers.Any(x => x.Equals(_header.WSUSER)))
                throw new UnauthorizedAccessException();
        }
    }
}