﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE
{
    [Serializable]
    public partial class Associado
    {
        [Required]
        [Display(Name = "IdAssociado", Description = "")]
        public virtual long IdAssociado { get; set; }

        [Required]
        [StringLength(14)]
        [Display(Name = "CpfCnpj", Description = "")]
        public virtual string CpfCnpj { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "RazaoSocial", Description = "")]
        public virtual string RazaoSocial { get; set; }

        [Required]
        [StringLength(1)]
        [Display(Name = "Status", Description = "")]
        public virtual string Status { get; set; }

        [StringLength(1)]
        [Display(Name = "StatusPagto", Description = "")]
        public virtual string StatusPagto { get; set; }

        [StringLength(50)]
        [Display(Name = "ContatoMaster", Description = "")]
        public virtual string ContatoMaster { get; set; }

        [StringLength(100)]
        [Display(Name = "EmailContato", Description = "")]
        public virtual string EmailContato { get; set; }

        [Display(Name = "DtVencTaxa", Description = "")]
        public virtual DateTime? DtVencTaxa { get; set; }

        [StringLength(8)]
        [Display(Name = "CEP", Description = "")]
        public virtual string CEP { get; set; }

        [StringLength(50)]
        [Display(Name = "Endereco", Description = "")]
        public virtual string Endereco { get; set; }

        [Display(Name = "Numero", Description = "")]
        public virtual decimal? Numero { get; set; }

        [StringLength(100)]
        [Display(Name = "Complemento", Description = "")]
        public virtual string Complemento { get; set; }

        [StringLength(50)]
        [Display(Name = "Bairro", Description = "")]
        public virtual string Bairro { get; set; }

        [StringLength(50)]
        [Display(Name = "Cidade", Description = "")]
        public virtual string Cidade { get; set; }

        [StringLength(2)]
        [Display(Name = "UF", Description = "")]
        public virtual string UF { get; set; }

        [StringLength(50)]
        [Display(Name = "Pais", Description = "")]
        public virtual string Pais { get; set; }

        [Display(Name = "DtUltimaSinc", Description = "")]
        public virtual DateTime? DtUltimaSinc { get; set; }

        [StringLength(50)]
        [Display(Name = "InscricaoEstadual", Description = "")]
        public virtual string InscricaoEstadual { get; set; }

        [StringLength(1)]
        [Display(Name = "CompartilhaDado", Description = "")]
        public virtual string CompartilhaDado { get; set; }
    }
}