﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    [Serializable]
    public partial class AssociadoLicenca
    {
        [Required]
        [Display(Name = "IdAssociado", Description = "")]
        public virtual long IdAssociado { get; set; }

        [Required]
        [Display(Name = "IdLicenca", Description = "")]
        public virtual int IdLicenca { get; set; }

        [Required]
        [StringLength(1)]
        [Display(Name = "Status", Description = "")]
        public virtual string Status { get; set; }

        [Display(Name = "DataAniversario", Description = "")]
        public virtual DateTime? DataAniversario { get; set; }
    }
}