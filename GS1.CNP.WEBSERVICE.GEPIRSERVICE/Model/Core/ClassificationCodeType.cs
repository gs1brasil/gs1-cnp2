﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class ClassificationCodeType
    {
        public ClassificationCodeTypeClassificationSystemCode classificationSystemCode { get; set; }
        public string Value { get; set; }
    }
}