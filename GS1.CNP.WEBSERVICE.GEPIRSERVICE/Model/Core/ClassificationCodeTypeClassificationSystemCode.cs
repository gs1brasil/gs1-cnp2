﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public enum ClassificationCodeTypeClassificationSystemCode
    {
        UNSPSC, ISIC, GPC
    }
}