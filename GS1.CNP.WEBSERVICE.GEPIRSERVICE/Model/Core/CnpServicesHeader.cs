﻿using System.Web.Services.Protocols;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class CnpServicesHeader : SoapHeader
    {
        public string RequesterGln { get; set; }
        public string WSUSER { get; set; }
    }
}