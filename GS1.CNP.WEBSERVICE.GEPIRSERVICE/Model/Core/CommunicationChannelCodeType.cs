﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public enum CommunicationChannelCodeType
    {
        TELEPHONE,
        CONSUMER_HOTLINE_NUMBER,
        TELEFAX,
        EMAIL,
        WEBSITE,
        MOBILE_WEBSITE
    }
}