﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class CommunicationChannelType
    {
        [XmlAttribute]
        public CommunicationChannelCodeType communicationChannelCode { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}