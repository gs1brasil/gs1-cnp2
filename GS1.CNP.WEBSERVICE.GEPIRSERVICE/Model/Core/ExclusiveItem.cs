﻿using System;
using System.Xml.Serialization;
using System.ComponentModel;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class ExclusiveItem : Item
    {
        public ExclusiveItem()
        {
            lang = "en";
        }

        [XmlAttribute(DataType = "language")]
        [DefaultValue("en")]

        public string CertificateName { get; set; }
        public DateTime? AssignDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Status { get; set; }
    }
}