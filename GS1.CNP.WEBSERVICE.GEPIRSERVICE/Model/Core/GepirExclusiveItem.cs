﻿using System.ComponentModel;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class GepirExclusiveItem
    {
        public GepirExclusiveItem()
        {
            version = 3.10m;
        }

        public ExclusiveItem[] itemDataLine { get; set; }

        [DefaultValue(typeof(decimal), "3.10")]
        public decimal version { get; set; }
    }
}