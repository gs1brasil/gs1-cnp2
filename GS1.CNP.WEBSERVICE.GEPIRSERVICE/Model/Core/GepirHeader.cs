﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class GepirHeader : CnpServicesHeader
    {
        public bool IsExclusive { get; set; }
    }
}