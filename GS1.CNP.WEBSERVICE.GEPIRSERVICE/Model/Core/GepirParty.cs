﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class GepirParty
    {
        [System.Xml.Serialization.XmlElementAttribute("partyDataLine")]
        public Party[] partyDataLine { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal version { get; set; }
    }
}