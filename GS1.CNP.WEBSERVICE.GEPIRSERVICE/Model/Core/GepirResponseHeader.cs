﻿using System.Web.Services.Protocols;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class GepirResponseHeader : SoapHeader
    {
        public string responderGln { get; set; }
        public int numberOfHits { get; set; }
        public int returnCode { get; set; }
    }
}