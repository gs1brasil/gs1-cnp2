﻿using System;
using System.Xml.Serialization;
using System.ComponentModel;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class Item
    {
        public Item()
        {
            lang = "en";
        }

        [XmlAttribute(DataType = "language")]
        [DefaultValue("en")]
        public string lang { get; set; }
        public string gtin { get; set; }
        public string informationProviderGln { get; set; }
        public string manufacturerGln { get; set; }
        public string itemName { get; set; }
        public string brandName { get; set; }
        public string descriptiveSize { get; set; }
        public DateTime? lastChangeDateTime { get; set; }

        public ItemTradeItemUnitDescriptor tradeItemUnitDescriptor { get; set; }
        //public ItemNetContent netContent { get; set; }
        public ItemLinkUri[] linkUri { get; set; }
        public ClassificationCodeType[] classificationCode { get; set; }

        [XmlArrayItem("childItem", IsNullable = false)]
        public ItemChildItem[] childItems { get; set; }

        public string ImageUrl { get; set; }

        //WebSite
        public string UrlMobilecom { get; set; }


        
         public int? totalQuantityOfNextLowerLevelTradeItem  { get; set; }
         public decimal? width{ get; set; }
         public string widthMeasurementUnitCode { get; set; }
         public decimal? height { get; set; }
         public string heightMeasurementUnitCode { get; set; }
         public decimal? depth { get; set; }
         public string depthMeasurementUnitCode { get; set; }
         public decimal? netContent { get; set; }
         public string netContentMeasurementUnitCode { get; set; }        
         public int? quantityOfChildren  { get; set; }

         public string CodeSegment { get; set; }
         public string CodeFamily { get; set; }
         public string CodeClass { get; set; }
         public string CodeBrick { get; set; }



    }
}