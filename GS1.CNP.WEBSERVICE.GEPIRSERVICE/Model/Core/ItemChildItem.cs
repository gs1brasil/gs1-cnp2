﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class ItemChildItem
    {
        public string gtin { get; set; }

        [XmlElement(DataType = "positiveInteger")]
        public string numberContained { get; set; }
    }
}