﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class ItemLinkUri
    {
        public ItemLinkUriFormat format { get; set; }
        [XmlText(DataType = "anyURI")]
        public string Value { get; set; }
    }
}