﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public enum ItemLinkUriFormat
    {
        html, xml, text
    }
}