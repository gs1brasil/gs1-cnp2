﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class ItemNetContent
    {
        public UomType uom { get; set; }
        public string Value { get; set; }
    }
}