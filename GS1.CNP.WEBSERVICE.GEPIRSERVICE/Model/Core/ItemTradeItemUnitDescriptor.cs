﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public enum ItemTradeItemUnitDescriptor
    {
        BASE_UNIT_OR_EACH, PACK_OR_INNER_PACK, CASE, DISPLAY_SHIPPER, PALLET, MIXED_MODULE
    }
}