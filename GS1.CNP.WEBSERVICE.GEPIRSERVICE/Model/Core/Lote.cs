﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class Lote
    {
        public string statuslote { get; set; }
        public string loteai { get; set; }
        public string lote { get; set; }
        public DateTime? dataprocessamento { get; set; }
        public DateTime? datavencimento { get; set; }
        public string tipoprocessamento { get; set; }
        public string url { get; set; }
        public string informacoesadicionais { get; set; }
        public string bizstep { get; set; }
        public string disposition { get; set; }
        public string lgtin { get; set; }
        public string gs1string { get; set; }
        public int quantidade { get; set; }

        [XmlElement("informationProduct")]
        public long gln { get; set; }
        public long gtin { get; set; }
        public string statusproduto { get; set; }

        [XmlElement(DataType = "nonNegativeInteger")]
        public string returnCode { get; set; }
        public string informationProviderGln { get; set; }
        public DateTime? lastChangeDateTime { get; set; }

        [XmlIgnore]
        public bool lastChangeDateTimeSpecified { get; set; }

        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string lang { get; set; }
    }
}