﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class LoteParty
    {
        [System.Xml.Serialization.XmlElementAttribute("Lote")]
        public Lote[] partyLotes { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal version { get; set; }
    }
}