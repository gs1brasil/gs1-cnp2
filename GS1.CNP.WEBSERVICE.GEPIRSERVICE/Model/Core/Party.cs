﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class Party
    {
        public string gln { get; set; }

        [XmlElement(DataType = "nonNegativeInteger")]
        public string returnCode { get; set; }
        public string informationProviderGln { get; set; }
        public DateTime? lastChangeDateTime { get; set; }

        [XmlIgnore]
        public bool lastChangeDateTimeSpecified { get; set; }
        public string gcp { get; set; }

        [XmlElement("additionalPartyId")]
        public string[] additionalPartyId { get; set; }
        public string partyName { get; set; }

        [XmlElement("streetAddress")]
        public string[] streetAddress { get; set; }
        public string pOBoxNumber { get; set; }
        public string subDivision { get; set; }
        public string postalCode { get; set; }
        public string city { get; set; }
        public string countryISOCode { get; set; }

        [XmlElement("contact")]
        public PartyContactType[] contact { get; set; }
        public PartyRoleListType partyRole { get; set; }

        [XmlIgnore]
        public bool partyRoleSpecified { get; set; }

        [XmlArrayItem("partyChild", IsNullable = false)]
        public PartyChildType[] partyContainment { get; set; }

        [XmlAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/XML/1998/namespace")]
        public string lang { get; set; }
    }
}