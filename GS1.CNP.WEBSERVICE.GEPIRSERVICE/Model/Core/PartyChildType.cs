﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class PartyChildType
    {
        [XmlElement("partyChildRole")]
        public PartyRoleListType[] partyChildRole { get; set; }
        public string gln { get; set; }
    }
}