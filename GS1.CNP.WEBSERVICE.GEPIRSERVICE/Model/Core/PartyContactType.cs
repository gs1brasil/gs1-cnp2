﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class PartyContactType
    {
        public string contactName { get; set; }

        [XmlElement("communicationChannel")]
        public CommunicationChannelType[] communicationChannel { get; set; }
    }
}