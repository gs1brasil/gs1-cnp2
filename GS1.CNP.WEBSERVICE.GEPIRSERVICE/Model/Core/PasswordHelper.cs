﻿using System.Web.Security;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public static class PasswordHelper
    {
        public static string GetHashedPassword(string password)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
        }
    }
}