﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    [Serializable]
    public partial class RelCertificadoView
    {
        [Display(Name = "IdAssociado", Description = "")]
        public long IdAssociado { get; set; }

        [Display(Name = "CpfCnpj", Description = "")]
        public string CpfCnpj { get; set; }

        [Display(Name = "RazaoSocial", Description = "")]
        public string RazaoSocial { get; set; }

        [Display(Name = "Gtin", Description = "")]
        public string Gtin { get; set; }

        [Display(Name = "DescricaoProduto", Description = "")]
        public string DescricaoProduto { get; set; }

        [Display(Name = "IdCertificado", Description = "")]
        public int? IdCertificado { get; set; }

        [Display(Name = "NomeCertificado", Description = "")]
        public string NomeCertificado { get; set; }

        [Display(Name = "DataAtribuicao", Description = "")]
        public DateTime? DataAtribuicao { get; set; }

        [Display(Name = "DataExpiracao", Description = "")]
        public DateTime? DataExpiracao { get; set; }

        [Display(Name = "Status", Description = "")]
        public string Status { get; set; }

        [Display(Name = "DescricaoStatus", Description = "")]
        public string DescricaoStatus { get; set; }

        [Display(Name = "DtHistorico", Description = "")]
        public DateTime? DtHistorico { get; set; }
    }
}