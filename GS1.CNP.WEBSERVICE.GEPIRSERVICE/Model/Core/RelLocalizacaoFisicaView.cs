﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    [Serializable]
    public partial class RelLocalizacaoFisicaView
    {
        [Display(Name = "IdAssociado", Description = "")]
        public long IdAssociado { get; set; }

        [Display(Name = "CpfCnpj", Description = "")]
        public string CpfCnpj { get; set; }

        [Display(Name = "RazaoSocial", Description = "")]
        public string RazaoSocial { get; set; }

        [Display(Name = "NrGln", Description = "")]
        public string NrGln { get; set; }

        [Display(Name = "Contato", Description = "")]
        public string Contato { get; set; }

        [Display(Name = "CEP", Description = "")]
        public string CEP { get; set; }

        [Display(Name = "Endereco", Description = "")]
        public string Endereco { get; set; }

        [Display(Name = "Numero", Description = "")]
        public string Numero { get; set; }

        [Display(Name = "Complemento", Description = "")]
        public string Complemento { get; set; }

        [Display(Name = "Bairro", Description = "")]
        public string Bairro { get; set; }

        [Display(Name = "Cidade", Description = "")]
        public string Cidade { get; set; }

        [Display(Name = "UF", Description = "")]
        public string UF { get; set; }

        [Display(Name = "Pais", Description = "")]
        public string Pais { get; set; }

        [Display(Name = "IdPapel", Description = "")]
        public int? IdPapel { get; set; }

        [Display(Name = "DtInclusao", Description = "")]
        public DateTime? DtInclusao { get; set; }

        [Display(Name = "NomePapel", Description = "")]
        public string NomePapel { get; set; }

        [Display(Name = "Status", Description = "")]
        public string Status { get; set; }

        [Display(Name = "DescStatus", Description = "")]
        public string DescStatus { get; set; }

        [Display(Name = "Observacoes", Description = "")]
        public string Observacoes { get; set; }

        [Display(Name = "FlagPrincipal", Description = "")]
        public bool FlagPrincipal { get; set; }
    }
}