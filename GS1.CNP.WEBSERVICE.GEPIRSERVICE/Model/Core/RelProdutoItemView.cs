﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    [Serializable]
    [DataContract]
    public partial class RelProdutoItemView
    {
        [Display(Name = "IdAssociado", Description = "")]
        [DataMember]
        public long IdAssociado { get; set; }

        [Display(Name = "CpfCnpj", Description = "")]
        [DataMember]
        public string CpfCnpj { get; set; }

        [Display(Name = "RazaoSocial", Description = "")]
        [DataMember]
        public string RazaoSocial { get; set; }

        [Display(Name = "Gtin", Description = "")]
        [DataMember]
        public string Gtin { get; set; }

        [Display(Name = "NrPrefixo", Description = "")]
        [DataMember]
        public string NrPrefixo { get; set; }

        [Display(Name = "TipoGtin", Description = "")]
        [DataMember]
        public int? TipoGtin { get; set; }

        [Display(Name = "DescTipoGtin", Description = "")]
        [DataMember]
        public string DescTipoGtin { get; set; }

        [Display(Name = "MarcaProduto", Description = "")]
        [DataMember]
        public string MarcaProduto { get; set; }

        [Display(Name = "CodigoInterno", Description = "")]
        [DataMember]
        public string CodigoInterno { get; set; }

        [Display(Name = "Descricao", Description = "")]
        [DataMember]
        public string Descricao { get; set; }

        [Display(Name = "DescricaoImpressao", Description = "")]
        [DataMember]
        public string DescricaoImpressao { get; set; }

        [Display(Name = "DtInclusao", Description = "")]
        [DataMember]
        public DateTime DtInclusao { get; set; }

        [Display(Name = "DtSuspensao", Description = "")]
        [DataMember]
        public DateTime? DtSuspensao { get; set; }

        [Display(Name = "DtReativacao", Description = "")]
        [DataMember]
        public DateTime? DtReativacao { get; set; }

        [Display(Name = "DtCancelamento", Description = "")]
        [DataMember]
        public DateTime? DtCancelamento { get; set; }

        [Display(Name = "DtReutilizacao", Description = "")]
        [DataMember]
        public DateTime? DtReutilizacao { get; set; }

        [Display(Name = "Pais", Description = "")]
        [DataMember]
        public string Pais { get; set; }

        [Display(Name = "IdTipoProduto", Description = "")]
        [DataMember]
        public int? IdTipoProduto { get; set; }

        [Display(Name = "NomeTipoProduto", Description = "")]
        [DataMember]
        public string NomeTipoProduto { get; set; }

        [Display(Name = "Lingua", Description = "")]
        [DataMember]
        public string Lingua { get; set; }

        [Display(Name = "Estado", Description = "")]
        [DataMember]
        public string Estado { get; set; }

        [Display(Name = "TIPI", Description = "")]
        [DataMember]
        public decimal? TIPI { get; set; }

        [Display(Name = "CodigoRegistro", Description = "")]
        [DataMember]
        public string CodigoRegistro { get; set; }

        [Display(Name = "IdAgencia", Description = "")]
        [DataMember]
        public int? IdAgencia { get; set; }

        [Display(Name = "NomeAgencia", Description = "")]
        [DataMember]
        public string NomeAgencia { get; set; }

        [Display(Name = "Largura", Description = "")]
        [DataMember]
        public decimal? Largura { get; set; }

        [Display(Name = "Profundidade", Description = "")]
        [DataMember]
        public decimal? Profundidade { get; set; }

        [Display(Name = "Altura", Description = "")]
        [DataMember]
        public decimal? Altura { get; set; }

        [Display(Name = "PesoLiquido", Description = "")]
        [DataMember]
        public decimal? PesoLiquido { get; set; }

        [Display(Name = "PesoBruto", Description = "")]
        [DataMember]
        public decimal? PesoBruto { get; set; }

        [Display(Name = "UrlMobilecom", Description = "")]
        [DataMember]
        public string UrlMobilecom { get; set; }

        [Display(Name = "UrlFoto1", Description = "")]
        [DataMember]
        public string UrlFoto1 { get; set; }

        [Display(Name = "UrlFoto2", Description = "")]
        [DataMember]
        public string UrlFoto2 { get; set; }

        [Display(Name = "UrlFoto3", Description = "")]
        [DataMember]
        public string UrlFoto3 { get; set; }

        [Display(Name = "CompartilhaDado", Description = "")]
        [DataMember]
        public string CompartilhaDado { get; set; }

        [Display(Name = "GtinOrigem", Description = "")]
        [DataMember]
        public string GtinOrigem { get; set; }

        [Display(Name = "Status", Description = "")]
        [DataMember]
        public string Status { get; set; }

        [Display(Name = "DescStatus", Description = "")]
        [DataMember]
        public string DescStatus { get; set; }

        [Display(Name = "Observacoes", Description = "")]
        [DataMember]
        public string Observacoes { get; set; }

        [Display(Name = "NCM", Description = "")]
        [DataMember]
        public string NCM { get; set; }

        [Display(Name = "GtinInferior", Description = "")]
        [DataMember]
        public string GtinInferior { get; set; }

        [Display(Name = "QtdeItens", Description = "")]
        [DataMember]
        public int? QtdeItens { get; set; }

        [Display(Name = "DescricaoInferior", Description = "")]
        [DataMember]
        public string DescricaoInferior { get; set; }

        [Display(Name = "CodeSegment", Description = "")]
        [DataMember]
        public string CodeSegment { get; set; }

        [Display(Name = "CodeFamily", Description = "")]
        [DataMember]
        public string CodeFamily { get; set; }

        [Display(Name = "CodeClass", Description = "")]
        [DataMember]
        public string CodeClass { get; set; }

        [Display(Name = "CodeBrick", Description = "")]
        [DataMember]
        public string CodeBrick { get; set; }

        [Display(Name = "CodeType", Description = "")]
        [DataMember]
        public string CodeType { get; set; }

        [Display(Name = "CodeValue", Description = "")]
        [DataMember]
        public string CodeValue { get; set; }

        [Display(Name = "Status do Prefixo")]
        [DataMember]
        public string StatusPrefixo { get; set; }

        [Display(Name = "Imagem 1")]
        [DataMember]
        public string Imagem1 { get; set; }

        [Display(Name = "Imagem 2")]
        [DataMember]
        public string Imagem2 { get; set; }

        [Display(Name = "Imagem 3")]
        [DataMember]
        public string Imagem3 { get; set; }

        [Display(Name = "Contato")]
        [DataMember]
        public string Contato { get; set; }
    }
}