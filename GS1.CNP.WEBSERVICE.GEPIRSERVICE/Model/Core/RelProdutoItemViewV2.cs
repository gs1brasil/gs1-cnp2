﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    [Serializable]
    [DataContract]
    public class RelProdutoItemViewV2
    {

        [Display(Name = "CodigoProduto", Description = "")]
        [DataMember]
        public long CodigoProduto { get; set; }

        [Display(Name = "globalTradeItemNumber", Description = "")]
        [DataMember]
        public long globalTradeItemNumber { get; set; }

        [Display(Name = "productDescription", Description = "")]
        [DataMember]
        public string productDescription { get; set; }

        [Display(Name = "CodigoTipoGTIN", Description = "")]
        [DataMember]
        public int CodigoTipoGTIN { get; set; }

        [Display(Name = "NrPrefixo", Description = "")]
        [DataMember]
        public long NrPrefixo { get; set; }

        [Display(Name = "CodItem", Description = "")]
        [DataMember]
        public string CodItem { get; set; }

        [Display(Name = "VarianteLogistica", Description = "")]
        [DataMember]
        public int?  VarianteLogistica { get; set; }

        [Display(Name = "CodigoAssociado", Description = "")]
        [DataMember]
        public long CodigoAssociado { get; set; }

        [Display(Name = "CodigoUsuarioCriacao", Description = "")]
        [DataMember]
        public long CodigoUsuarioCriacao { get; set; }

        [Display(Name = "CodigoUsuarioAlteracao", Description = "")]
        [DataMember]
        public long? CodigoUsuarioAlteracao { get; set; }

        [Display(Name = "CodigoUsuarioExclusao", Description = "")]
        [DataMember]
        public long? CodigoUsuarioExclusao { get; set; }

        [Display(Name = "tradeItemCountryOfOrigin", Description = "")]
        [DataMember]
        public int? tradeItemCountryOfOrigin { get; set; }

        [Display(Name = "CodigoStatusGTIN", Description = "")]
        [DataMember]
        public int CodigoStatusGTIN { get; set; }

        [Display(Name = "CodeSegment", Description = "")]
        [DataMember]
        public string CodeSegment { get; set; }

        [Display(Name = "CodeFamily", Description = "")]
        [DataMember]
        public string CodeFamily { get; set; }

        [Display(Name = "CodeClass", Description = "")]
        [DataMember]
        public string CodeClass { get; set; }

        [Display(Name = "CodeBrick", Description = "")]
        [DataMember]
        public string CodeBrick { get; set; }

        [Display(Name = "countryCode", Description = "")]
        [DataMember]
        public string countryCode { get; set; }

        [Display(Name = "CodeBrickAttribute", Description = "")]
        [DataMember]
        public long? CodeBrickAttribute { get; set; }

        [Display(Name = "IndicadorCompartilhaDados", Description = "")]
        [DataMember]
        public string IndicadorCompartilhaDados { get; set; }

        [Display(Name = "Observacoes", Description = "")]
        [DataMember]
        public string Observacoes { get; set; }

        [Display(Name = "Estado", Description = "")]
        [DataMember]
        public string Estado { get; set; }

        [Display(Name = "informationProvider", Description = "")]
        [DataMember]
        public long? informationProvider { get; set; }

        [Display(Name = "brandName", Description = "")]
        [DataMember]
        public string brandName { get; set; }

        [Display(Name = "DataInclusao", Description = "")]
        [DataMember]
        public DateTime DataInclusao { get; set; }

        [Display(Name = "DataAlteracao", Description = "")]
        [DataMember]
        public DateTime? DataAlteracao { get; set; }

        [Display(Name = "DataSuspensao", Description = "")]
        [DataMember]
        public DateTime? DataSuspensao { get; set; }

        [Display(Name = "DataReativacao", Description = "")]
        [DataMember]
        public DateTime? DataReativacao { get; set; }

        [Display(Name = "DataCancelamento", Description = "")]
        [DataMember]
        public DateTime? DataCancelamento { get; set; }

        [Display(Name = "DataReutilizacao", Description = "")]
        [DataMember]
        public DateTime? DataReutilizacao { get; set; }

        [Display(Name = "modelNumber", Description = "")]
        [DataMember]
        public string modelNumber { get; set; }

        [Display(Name = "importClassificationType", Description = "")]
        [DataMember]
        public string importClassificationType { get; set; }

        [Display(Name = "importClassificationValue", Description = "")]
        [DataMember]
        public string importClassificationValue { get; set; }

        [Display(Name = "minimumTradeItemLifespanFromTimeOfProduction", Description = "")]
        [DataMember]
        public int? minimumTradeItemLifespanFromTimeOfProduction { get; set; }

        [Display(Name = "startAvailabilityDateTime", Description = "")]
        [DataMember]
        public DateTime? startAvailabilityDateTime { get; set; }

        [Display(Name = "endAvailabilityDateTime", Description = "")]
        [DataMember]
        public DateTime? endAvailabilityDateTime { get; set; }

        [Display(Name = "depth", Description = "")]
        [DataMember]
        public decimal? depth { get; set; }

        [Display(Name = "depthMeasurementUnitCode", Description = "")]
        [DataMember]
        public string depthMeasurementUnitCode { get; set; }

        [Display(Name = "height", Description = "")]
        [DataMember]
        public decimal? height { get; set; }

        [Display(Name = "heightMeasurementUnitCode", Description = "")]
        [DataMember]
        public string heightMeasurementUnitCode { get; set; }

        [Display(Name = "width", Description = "")]
        [DataMember]
        public decimal? width { get; set; }

        [Display(Name = "widthMeasurementUnitCode", Description = "")]
        [DataMember]
        public string widthMeasurementUnitCode { get; set; }

        [Display(Name = "netContent", Description = "")]
        [DataMember]
        public decimal? netContent { get; set; }

        [Display(Name = "netContentMeasurementUnitCode", Description = "")]
        [DataMember]
        public string netContentMeasurementUnitCode { get; set; }

        [Display(Name = "grossWeight", Description = "")]
        [DataMember]
        public decimal? grossWeight { get; set; }

        [Display(Name = "grossWeightMeasurementUnitCode", Description = "")]
        [DataMember]
        public string grossWeightMeasurementUnitCode { get; set; }

        [Display(Name = "netWeight", Description = "")]
        [DataMember]
        public decimal? netWeight { get; set; }

        [Display(Name = "netWeightMeasurementUnitCode", Description = "")]
        [DataMember]
        public string netWeightMeasurementUnitCode { get; set; }

        [Display(Name = "CodigoTipoProduto", Description = "")]
        [DataMember]
        public int? CodigoTipoProduto { get; set; }

        [Display(Name = "isTradeItemABaseUnit", Description = "")]
        [DataMember]
        public int? isTradeItemABaseUnit { get; set; }

        [Display(Name = "isTradeItemAConsumerUnit", Description = "")]
        [DataMember]
        public int? isTradeItemAConsumerUnit { get; set; }

        [Display(Name = "isTradeItemAModel", Description = "")]
        [DataMember]
        public int? isTradeItemAModel { get; set; }

        [Display(Name = "isTradeItemAnInvoiceUnit", Description = "")]
        [DataMember]
        public int? isTradeItemAnInvoiceUnit { get; set; }

        [Display(Name = "packagingTypeCode", Description = "")]
        [DataMember]
        public string packagingTypeCode { get; set; }

        [Display(Name = "PalletTypeCode", Description = "")]
        [DataMember]
        public string PalletTypeCode { get; set; }

        [Display(Name = "totalQuantityOfNextLowerLevelTradeItem", Description = "")]
        [DataMember]
        public int? totalQuantityOfNextLowerLevelTradeItem { get; set; }

        [Display(Name = "StackingFactor", Description = "")]
        [DataMember]
        public int? StackingFactor { get; set; }

        [Display(Name = "[PalletTypeCode]", Description = "")]
        [DataMember]
        public int? quantityOfTradeItemContainedInACompleteLayer { get; set; }

        [Display(Name = "quantityOfTradeItemsPerPalletLayer", Description = "")]
        [DataMember]
        public int? quantityOfTradeItemsPerPalletLayer { get; set; }

        [Display(Name = "quantityOfCompleteLayersContainedInATradeItem", Description = "")]
        [DataMember]
        public int? quantityOfCompleteLayersContainedInATradeItem { get; set; }

        [Display(Name = "quantityOfLayersPerPallet", Description = "")]
        [DataMember]
        public int? quantityOfLayersPerPallet { get; set; }

        [Display(Name = "CodigoProdutoOrigem", Description = "")]
        [DataMember]
        public long? CodigoProdutoOrigem { get; set; }

        [Display(Name = "deliveryToDistributionCenterTemperatureMinimum", Description = "")]
        [DataMember]
        public decimal? deliveryToDistributionCenterTemperatureMinimum { get; set; }

        [Display(Name = "storageHandlingTempMinimumUOM", Description = "")]
        [DataMember]
        public string storageHandlingTempMinimumUOM { get; set; }

        [Display(Name = "storageHandlingTemperatureMaximum", Description = "")]
        [DataMember]
        public decimal? storageHandlingTemperatureMaximum { get; set; }

        [Display(Name = "storageHandlingTemperatureMaximumunitOfMeasure", Description = "")]
        [DataMember]
        public string storageHandlingTemperatureMaximumunitOfMeasure { get; set; }

        [Display(Name = "isDangerousSubstanceIndicated", Description = "")]
        [DataMember]
        public int? isDangerousSubstanceIndicated { get; set; }

        [Display(Name = "ipiPerc", Description = "")]
        [DataMember]
        public decimal? ipiPerc { get; set; }

        [Display(Name = "isTradeItemAnOrderableUnit", Description = "")]
        [DataMember]
        public int? isTradeItemAnOrderableUnit { get; set; }

        [Display(Name = "isTradeItemADespatchUnit", Description = "")]
        [DataMember]
        public int? isTradeItemADespatchUnit { get; set; }

        [Display(Name = "orderSizingFactor", Description = "")]
        [DataMember]
        public string orderSizingFactor { get; set; }

        [Display(Name = "orderQuantityMultiple", Description = "")]
        [DataMember]
        public int orderQuantityMultiple { get; set; }

        [Display(Name = "orderQuantityMinimum", Description = "")]
        [DataMember]
        public int? orderQuantityMinimum { get; set; }

        [Display(Name = "barcodeCertified", Description = "")]
        [DataMember]
        public int? barcodeCertified { get; set; }

        [Display(Name = "dataQualityCertified", Description = "")]
        [DataMember]
        public int? dataQualityCertified { get; set; }

        [Display(Name = "dataQualityCertifiedAgencyCode", Description = "")]
        [DataMember]
        public int? dataQualityCertifiedAgencyCode { get; set; }

        [Display(Name = "allGDSNAttributes", Description = "")]
        [DataMember]
        public int? allGDSNAttributes { get; set; }

        [Display(Name = "CanalComunicacaoDados", Description = "")]
        [DataMember]
        public int? CanalComunicacaoDados { get; set; }

        [Display(Name = "ProprioDonoInformacao", Description = "")]
        [DataMember]
        public int? ProprioDonoInformacao { get; set; }

        [Display(Name = "ValidadoDonoDaInformacao", Description = "")]
        [DataMember]
        public int? ValidadoDonoDaInformacao { get; set; }

        [Display(Name = "IndicadorGDSN", Description = "")]
        [DataMember]
        public int? IndicadorGDSN { get; set; }

        [Display(Name = "CodigoLingua", Description = "")]
        [DataMember]
        public int? CodigoLingua { get; set; }
      

        [Display(Name = "CPFCNPJ", Description = "")]
        [DataMember]
        public string CPFCNPJ { get; set; }

        [Display(Name = "CAD", Description = "")]
        [DataMember]
        public string CAD { get; set; }

        [Display(Name = "NomeAssociado", Description = "")]
        [DataMember]
        public string NomeAssociado { get; set; }

        [Display(Name = "NomeTipoGtin", Description = "")]
        [DataMember]
        public string NomeTipoGtin { get; set; }

        [Display(Name = "NomeUsuarioCriacao", Description = "")]
        [DataMember]
        public string NomeUsuarioCriacao { get; set; }

        [Display(Name = "NomeUsuarioAlteracao", Description = "")]
        [DataMember]
        public string NomeUsuarioAlteracao { get; set; }

        [Display(Name = "NomeUsuarioExclusao", Description = "")]
        [DataMember]
        public string NomeUsuarioExclusao { get; set; }

        [Display(Name = "NometradeItemCountryOfOrigin", Description = "")]
        [DataMember]
        public string NometradeItemCountryOfOrigin { get; set; }

        [Display(Name = "NomeStatusGTIN", Description = "")]
        [DataMember]
        public string NomeStatusGTIN { get; set; }

        [Display(Name = "NomecountryCode", Description = "")]
        [DataMember]
        public string NomecountryCode { get; set; }

        [Display(Name = "NomeLingua", Description = "")]
        [DataMember]
        public string NomeLingua { get; set; }

        [Display(Name = "NomeTipoProduto", Description = "")]
        [DataMember]
        public string NomeTipoProduto { get; set; }


        [Display(Name = "AgenciasReguladoras", Description = "")]
        [DataMember]
        public List<AgenciaReguladora> AgenciasReguladoras { get; set; }


        [Display(Name = "BrickTypeValues", Description = "")]
        [DataMember]
        public List<BrickTypeValue> BrickTypeValues { get; set; }

        [Display(Name = "ProdutoInferior", Description = "")]
        [DataMember]
        public List<ProdutoInferior> ProdutosInferiores { get; set; }


        [Display(Name = "ImagensProduto", Description = "")]
        [DataMember]
        public List<ImagenProduto> ImagensProduto { get; set; }

        [Display(Name = "URLsProduto", Description = "")]
        [DataMember]
        public List<URLProduto> URLsProduto { get; set; }
        
        [Display(Name = "ContatosAssociado", Description = "")]
        [DataMember]
        public List<ContatoAssociado> ContatosAssociado { get; set; }
             
    }

    [Serializable]
    [DataContract]
    public class AgenciaReguladora
    {
        [Display(Name = "CodigoAgenciaReguladora", Description = "")]
        [DataMember]
        public long CodigoAgenciaReguladora { get; set; }

        [Display(Name = "NomeAgenciaReguladora", Description = "")]
        [DataMember]
        public string NomeAgenciaReguladora { get; set; }

        [Display(Name = "alternateItemIdentificationId", Description = "")]
        [DataMember]
        public string alternateItemIdentificationId { get; set; }
        
    }

    [Serializable]
    [DataContract]
    public class BrickTypeValue
    {
        [Display(Name = "CodigoBrickTypeValue", Description = "")]
        [DataMember]
        public long CodigoBrickTypeValue { get; set; }

        [Display(Name = "CodigoProduto", Description = "")]
        [DataMember]
        public long CodigoProduto { get; set; }

        [Display(Name = "CodeBrick", Description = "")]
        [DataMember]
        public string CodeBrick { get; set; }

        [Display(Name = "CodeClass", Description = "")]
        [DataMember]
        public string CodeClass { get; set; }

        [Display(Name = "CodeFamily", Description = "")]
        [DataMember]
        public string CodeFamily { get; set; }

        [Display(Name = "CodeSegment", Description = "")]
        [DataMember]
        public string CodeSegment { get; set; }

        [Display(Name = "Idioma", Description = "")]
        [DataMember]
        public string Idioma { get; set; }

        [Display(Name = "CodeType", Description = "")]
        [DataMember]
        public string CodeType { get; set; }

        [Display(Name = "CodeValue", Description = "")]
        [DataMember]
        public string CodeValue { get; set; }
    }

    [Serializable]
    [DataContract]
    public class ProdutoInferior
    {
        [Display(Name = "CodigoProdutoInferior", Description = "")]
        [DataMember]
        public long CodigoProdutoInferior { get; set; }

        [Display(Name = "globalTradeItemNumber", Description = "")]
        [DataMember]
        public long globalTradeItemNumber { get; set; }

        [Display(Name = "productdescription", Description = "")]
        [DataMember]
        public string productdescription { get; set; }

        [Display(Name = "Quantidade", Description = "")]
        [DataMember]
        public int Quantidade { get; set; }

    }

    [Serializable]
    [DataContract]
    public class ImagenProduto
    {
        [Display(Name = "CodigoImagem", Description = "")]
        [DataMember]
        public long CodigoImagem { get; set; }

        [Display(Name = "Url", Description = "")]
        [DataMember]
        public string Url { get; set; }
       
    }

    [Serializable]
    [DataContract]
    public class URLProduto
    {
        [Display(Name = "CodigoURL", Description = "")]
        [DataMember]
        public long CodigoURL { get; set; }

        [Display(Name = "NomeURL", Description = "")]
        [DataMember]
        public string NomeURL { get; set; }

        [Display(Name = "Url", Description = "")]
        [DataMember]
        public string Url { get; set; }

        [Display(Name = "CodigoTipoURL", Description = "")]
        [DataMember]
        public int CodigoTipoURL { get; set; }

        [Display(Name = "NomeTipoURL", Description = "")]
        [DataMember]
        public string NomeTipoURL { get; set; }

    }

    [Serializable]
    [DataContract]
    public class ContatoAssociado
    {
        [Display(Name = "CodigoContato", Description = "")]
        [DataMember]
        public long CodigoContato { get; set; }

        [Display(Name = "NomeContato", Description = "")]
        [DataMember]
        public string NomeContato { get; set; }
        
    }

}