﻿namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public enum UomType
    {
        PCE, GRM, KGM
    }
}