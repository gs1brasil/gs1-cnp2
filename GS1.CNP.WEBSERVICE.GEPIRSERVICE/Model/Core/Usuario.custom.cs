﻿using System.Collections.Generic;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public partial class Usuario
    {
        public List<AssociadoLicenca> Licenses { get; set; }
        public bool AlreadyProvedDelinquencyStatus { get; set; }
    }
}