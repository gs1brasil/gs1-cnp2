using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core
{
    public class XMLLoteGTIN
    {
        [XmlElement(DataType = "nonNegativeInteger")]
        public string GTIN { get; set; }
        public string Lote { get; set; }
    }
}