﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using GS1.CNP.WEBSERVICE.GEPIRSERVICE.Model.Core;
using System.Dynamic;
using System.Data;
using System.Diagnostics;

namespace GS1.CNP.WEBSERVICE.GEPIRSERVICE
{
    [WebService(Namespace = "http://www.gs1br.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class GepirServices : WebService
    {
        private static String EVENT_SOURCE = "GS1.CNP.WEBSERVICE.GEPIRSERVICE";
        private static String EVENT_LOG = "Application";

        public GepirHeader RequestHeader { get; set; }
        public GepirResponseHeader ResponseHeader { get; set; }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public GepirItem GetItemByGTIN(string requestedGTIN)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            try
            {
                //Armazena o LOG da busca pelo GTIN
                LogGTINCNP20(requestedGTIN);

                if (!ValidateHeader() || !ValidateGTIN(requestedGTIN))
                    return null;

                string query = @"SELECT * 
                                    FROM [dbo].[vwRelProdutoItem]
                                    WHERE GTIN = @gtin";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    dynamic param = new ExpandoObject();
                    param.gtin = requestedGTIN;

                    var results = ((List<dynamic>)db.Select(query, param)).FirstOrDefault(); 

                    ResponseHeader.numberOfHits = 0;
                    if (IsResultEmpty(results))
                        return null;

                    ResponseHeader.returnCode = 0;

                    results = GetFilteredResults(results, requestedGTIN);

                    var gepirItem = BuildGepirItem(results);
                    ResponseHeader.numberOfHits = (results != null) ? 1 : 0;
                    //ResponseHeader.numberOfHits = ((results != null)) ? ((IDictionary<string, object>)results).Count : 0;
                    return gepirItem;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.WEBSERVICE.GEPIRSERVICE - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);


                }
                catch (Exception)
                {
                    
                    
                }
              

                ResponseHeader.numberOfHits = 0;
                ResponseHeader.returnCode = 99;
                return null;
            }
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public RelProdutoItemView GetCNPItemByGTIN(string gtin)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            try
            {
                //Armazena o LOG da busca pelo GTIN
                LogGTINCNP20(gtin);

                if (!ValidateHeader() || !ValidateGTIN(gtin))
                    return null;

                string query = @"SELECT * 
                                    FROM [dbo].[vwRelProdutoItem]
                                    WHERE GTIN = @gtin";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    dynamic param = new ExpandoObject();
                    param.gtin = gtin;

                    var gtinResult = ((List<dynamic>)db.Select(query, param)).FirstOrDefault();

                    if (gtinResult == null)
                    {
                        ResponseHeader.returnCode = 8;
                        return null;
                    }

                    var list = new RelProdutoItemView();

                    if (gtinResult.altura != null && gtinResult.altura.ToString() != "") list.Altura = Convert.ToDecimal(gtinResult.altura.ToString());
                    if (gtinResult.codebrick != null && gtinResult.codebrick.ToString() != "") list.CodeBrick = gtinResult.codebrick;
                    if (gtinResult.codeclass != null && gtinResult.codeclass.ToString() != "") list.CodeClass = gtinResult.codeclass;
                    if (gtinResult.codefamily != null && gtinResult.codefamily.ToString() != "") list.CodeFamily = gtinResult.codefamily;
                    if (gtinResult.codesegment != null && gtinResult.codesegment.ToString() != "") list.CodeSegment = gtinResult.codesegment;
                    if (gtinResult.codetype != null && gtinResult.codetype.ToString() != "") list.CodeType = gtinResult.codetype;
                    if (gtinResult.codevalue != null && gtinResult.codevalue.ToString() != "") list.CodeValue = gtinResult.codevalue;
                    if (gtinResult.codigointerno != null && gtinResult.codigointerno.ToString() != "") list.CodigoInterno = gtinResult.codigointerno;
                    if (gtinResult.codigoregistro != null && gtinResult.codigoregistro.ToString() != "") list.CodigoRegistro = gtinResult.codigoregistro;
                    if (gtinResult.compartilhadado != null && gtinResult.compartilhadado.ToString() != "") list.CompartilhaDado = gtinResult.compartilhadado;
                    if (gtinResult.contato != null && gtinResult.contato.ToString() != "") list.Contato = gtinResult.contato;
                    if (gtinResult.cpfcnpj != null && gtinResult.cpfcnpj.ToString() != "") list.CpfCnpj = gtinResult.cpfcnpj;
                    if (gtinResult.descricao != null && gtinResult.descricao.ToString() != "") list.Descricao = gtinResult.descricao;
                    if (gtinResult.descricaoimpressao != null && gtinResult.descricaoimpressao.ToString() != "") list.DescricaoImpressao = gtinResult.descricaoimpressao;
                    if (gtinResult.descricaoinferior != null && gtinResult.descricaoinferior.ToString() != "") list.DescricaoInferior = gtinResult.descricaoinferior;
                    if (gtinResult.descstatus != null && gtinResult.descstatus.ToString() != "") list.DescStatus = gtinResult.descstatus;
                    if (gtinResult.desctipogtin != null && gtinResult.desctipogtin.ToString() != "") list.DescTipoGtin = gtinResult.desctipogtin;
                    if (gtinResult.dtcancelamento != null && gtinResult.dtcancelamento.ToString() != "") list.DtCancelamento = gtinResult.dtcancelamento;
                    if (gtinResult.dtinclusao != null && gtinResult.dtinclusao.ToString() != "") list.DtInclusao = gtinResult.dtinclusao;
                    if (gtinResult.dtreativacao != null && gtinResult.dtreativacao.ToString() != "") list.DtReativacao = gtinResult.dtreativacao;
                    if (gtinResult.dtreutilizacao != null && gtinResult.dtreutilizacao.ToString() != "") list.DtReutilizacao = gtinResult.dtreutilizacao;
                    if (gtinResult.dtsuspensao != null && gtinResult.dtsuspensao.ToString() != "") list.DtSuspensao = gtinResult.dtsuspensao;
                    if (gtinResult.estado != null && gtinResult.estado.ToString() != "") list.Estado = gtinResult.estado;
                    if (gtinResult.gtin != null && gtinResult.gtin.ToString() != "") list.Gtin = gtinResult.gtin;
                    if (gtinResult.gtininferior != null && gtinResult.gtininferior.ToString() != "") list.GtinInferior = gtinResult.gtininferior.ToString();
                    if (gtinResult.gtinorigem != null && gtinResult.gtinorigem.ToString() != "") list.GtinOrigem = gtinResult.gtinorigem.ToString();
                    if (gtinResult.idagencia != null && gtinResult.idagencia.ToString() != "") list.IdAgencia = Convert.ToInt64(gtinResult.idagencia.ToString());
                    if (gtinResult.idassociado != null && gtinResult.idassociado.ToString() != "") list.IdAssociado = Convert.ToInt64(gtinResult.idassociado.ToString());
                    if (gtinResult.idtipoproduto != null && gtinResult.idtipoproduto.ToString() != "") list.IdTipoProduto = Convert.ToInt32(gtinResult.idtipoproduto.ToString());
                    if (gtinResult.imagem1 != null && gtinResult.imagem1.ToString() != "") list.Imagem1 = gtinResult.imagem1;
                    if (gtinResult.imagem2 != null && gtinResult.imagem2.ToString() != "") list.Imagem2 = gtinResult.imagem2;
                    if (gtinResult.imagem3 != null && gtinResult.imagem3.ToString() != "") list.Imagem3 = gtinResult.imagem3;
                    if (gtinResult.largura != null && gtinResult.largura.ToString() != "") list.Largura = Convert.ToDecimal(gtinResult.largura.ToString());
                    if (gtinResult.lingua != null && gtinResult.lingua.ToString() != "") list.Lingua = gtinResult.lingua;
                    if (gtinResult.marcaproduto != null && gtinResult.marcaproduto.ToString() != "") list.MarcaProduto = gtinResult.marcaproduto;
                    if (gtinResult.ncm != null && gtinResult.ncm.ToString() != "") list.NCM = gtinResult.ncm;
                    if (gtinResult.nomeagencia != null && gtinResult.nomeagencia.ToString() != "") list.NomeAgencia = gtinResult.nomeagencia;
                    if (gtinResult.nometipoproduto != null && gtinResult.nometipoproduto.ToString() != "") list.NomeTipoProduto = gtinResult.nometipoproduto;
                    if (gtinResult.nrprefixo != null && gtinResult.nrprefixo.ToString() != "") list.NrPrefixo = gtinResult.nrprefixo;
                    if (gtinResult.observacoes != null && gtinResult.observacoes.ToString() != "") list.Observacoes = gtinResult.observacoes;
                    if (gtinResult.pais != null && gtinResult.pais.ToString() != "") list.Pais = gtinResult.pais;
                    if (gtinResult.pesobruto != null && gtinResult.pesobruto.ToString() != "") list.PesoBruto = Convert.ToDecimal(gtinResult.pesobruto.ToString());
                    if (gtinResult.pesoliquido != null && gtinResult.pesoliquido.ToString() != "") list.PesoLiquido = Convert.ToDecimal(gtinResult.pesoliquido.ToString());
                    if (gtinResult.profundidade != null && gtinResult.profundidade.ToString() != "") list.Profundidade = Convert.ToDecimal(gtinResult.profundidade.ToString());
                    if (gtinResult.qtdeitens != null && gtinResult.qtdeitens.ToString() != "") list.QtdeItens = (Int32)gtinResult.qtdeitens;
                    if (gtinResult.razaosocial != null && gtinResult.razaosocial.ToString() != "") list.RazaoSocial = gtinResult.razaosocial;
                    if (gtinResult.status != null && gtinResult.status.ToString() != "") list.Status = gtinResult.status;
                    if (gtinResult.statusprefixo != null && gtinResult.statusprefixo.ToString() != "") list.StatusPrefixo = gtinResult.statusprefixo;
                    if (gtinResult.tipi != null && gtinResult.tipi.ToString() != "") list.TIPI = Convert.ToDecimal(gtinResult.tipi.ToString());
                    if (gtinResult.tipogtin != null && gtinResult.tipogtin.ToString() != "") list.TipoGtin = gtinResult.tipogtin;
                    if (gtinResult.urlfoto1 != null && gtinResult.urlfoto1.ToString() != "") list.UrlFoto1 = gtinResult.urlfoto1;
                    if (gtinResult.urlfoto2 != null && gtinResult.urlfoto2.ToString() != "") list.UrlFoto2 = gtinResult.urlfoto2;
                    if (gtinResult.urlfoto3 != null && gtinResult.urlfoto3.ToString() != "") list.UrlFoto3 = gtinResult.urlfoto3;
                    if (gtinResult.urlmobilecom != null && gtinResult.urlmobilecom.ToString() != "") list.UrlMobilecom = gtinResult.urlmobilecom;

                    list = GetFilteredResults(list, gtin);

                    //if (!(gtinResult.Count == 0))
                    if (!(list == null))
                    {
                        //gtinResult = list.First();
                        ResponseHeader.numberOfHits = 1;
                    }
                    else
                        return null;

                    //if (gtinResult.tipogtin != null && gtinResult.tipogtin == 1 && string.IsNullOrWhiteSpace(gtinResult.nrprefixo))
                    if (list != null && list.TipoGtin != null && list.TipoGtin == 1 && string.IsNullOrWhiteSpace(list.NrPrefixo))
                        list.NrPrefixo = list.Gtin;

                    ResponseHeader.returnCode = 0;

                    return list;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.WEBSERVICE.GEPIRSERVICE - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);


                }
                catch (Exception)
                {


                }

                ResponseHeader.numberOfHits = 0;
                ResponseHeader.returnCode = 99;
                return null;
            }
        }

        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("Erro!");
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public GepirExclusiveItem GetExclusiveItemByGTIN(string requestedGTIN)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            try
            {
                //Armazena o LOG da busca pelo GTIN
                LogGTINCNP20(requestedGTIN);

                if (!ValidateHeader() || !ValidateGTIN(requestedGTIN))
                    return null;

                string query = @"SELECT * 
                                    FROM [dbo].[vwRelProdutoItem]
                                    WHERE GTIN = @gtin";

                string query_certificado = @"SELECT IdAssociado, CPFCNPJ, RazaoSocial, GTIN, DescricaoProduto, 
                                                IdCertificado, NomeCertificado, DataAtribuicao, DataExpiracao,
                                                Status, DescricaoStatus, DtHistorico
                                                FROM vwRelCertificado
                                                WHERE GTIN = @gtin";

                string query_associado = @"SELECT IdAssociado, CPFCNPJ, RazaoSocial, Status, StatusPagto, ContatoMaster, EmailContato, DtVencTaxa, CEP,
                                            Endereco, Numero, Complemento, Bairro, Cidade, UF, Pais, DtUltimaSinc, InscricaoEstadual, CompartilhaDado 
                                            FROM tbAssociado
                                            WHERE IdAssociado = @IdAssociado";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    dynamic param = new ExpandoObject();
                    param.gtin = requestedGTIN;

                    var gtinResults = ((List<dynamic>)db.Select(query, param)).FirstOrDefault();
                    var certificadoResults = ((List<dynamic>)db.Select(query_certificado, param)).FirstOrDefault();
                    //var certificadoResults = RelatorioFacade.GetRelCertificadoByCriteria(criteriaGtin);

                    ResponseHeader.numberOfHits = 0;
                    if (IsResultEmpty(gtinResults)) return null;
                    if (!RequestHeader.IsExclusive) return null;

                    ResponseHeader.returnCode = 0;

                    dynamic param_associado = new ExpandoObject();
                    param_associado.IdAssociado = gtinResults.idassociado;
                    var company = ((List<dynamic>)db.Select(query_associado, param_associado)).FirstOrDefault();

                    if (CompanyIsOverdue(company))
                    {
                        // deixar a chamada pronta caso haja necessidade de mudar as informações de registro no RGC
                        //var gln = ContainerFactory.GetDomainContainer().Resolve<AssociadoGLNBiz>().GetAll().FirstOrDefault(x => x.IdAssociado == gtinResults.First().IdAssociado);
                        //try
                        //{
                        //    new RGC.WSGS1004().GS1GRAVACONSULTA("MSALPHA", company.CpfCnpj, string.Empty, "GTIN", requestedGTIN, string.Empty, string.Empty, string.Empty);
                        //}
                        //catch (Exception) { }
                    }

                    var gepirItem = BuildGepirExclusiveItem(gtinResults, certificadoResults);
                    ResponseHeader.numberOfHits = (gtinResults != null) ? 1 : 0;
                    //ResponseHeader.numberOfHits = ((gtinResults != null)) ? ((IDictionary<string, object>)gtinResults).Count : 0;
                    return gepirItem;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.WEBSERVICE.GEPIRSERVICE - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);


                }
                catch (Exception)
                {


                }

                ResponseHeader.numberOfHits = 0;
                ResponseHeader.returnCode = 99;
                return null;
            }
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public GepirParty GetPartyByGLN(string requestedGLN)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            try
            {

                if (!ValidateHeader() || !ValidateGLN(requestedGLN))
                    return null;

                string query_localizacao_fisica = @"SELECT * 
                                FROM [dbo].[vwRelLocalizacaoFisica]
                                WHERE NRGLN = @gln";

                string query_associado = @"SELECT IdAssociado, CPFCNPJ, RazaoSocial, Status, StatusPagto, ContatoMaster, EmailContato, DtVencTaxa, CEP,
                                            Endereco, Numero, Complemento, Bairro, Cidade, UF, Pais, DtUltimaSinc, InscricaoEstadual, CompartilhaDado 
                                            FROM tbAssociado
                                            WHERE IdAssociado = @IdAssociado";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    dynamic param = new ExpandoObject();
                    param.gln = requestedGLN;

                    var results = ((List<dynamic>)db.Select(query_localizacao_fisica, param)).FirstOrDefault();

                    if (IsResultEmpty(results)) return null;

                    ResponseHeader.returnCode = 0;

                    dynamic param_associado = new ExpandoObject();
                    param_associado.IdAssociado = results.idassociado;
                    var company = ((List<dynamic>)db.Select(query_associado, param_associado)).FirstOrDefault();

                    if (CompanyIsOverdue(company))
                    {
                        // deixar a chamada pronta caso haja necessidade de mudar as informações de registro no RGC
                        //var gln = ContainerFactory.GetDomainContainer().Resolve<AssociadoGLNBiz>().GetAll().FirstOrDefault(x => x.IdAssociado == results.First().IdAssociado);

                        //try
                        //{
                        //    new RGC.WSGS1004().GS1GRAVACONSULTA("MSALPHA", company.CpfCnpj, string.Empty, "GLN", requestedGLN, string.Empty, string.Empty, string.Empty);
                        //}
                        //catch (Exception) { }
                    }

                    ResponseHeader.numberOfHits = (results != null) ? 1 : 0;
                    //ResponseHeader.numberOfHits = ((results != null)) ? ((IDictionary<string, object>)results).Count : 0;
                    return BuildParty(results);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.WEBSERVICE.GEPIRSERVICE - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);


                }
                catch (Exception)
                {


                }

                ResponseHeader.returnCode = 99;
                ResponseHeader.numberOfHits = 0;
                return null;
            }
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public LoteParty GetLot(string GTIN, string Lote)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            //Armazena o LOG da busca pelo GTIN
            LogGTINCNP20(GTIN);

            try
            {
                if (!ValidateHeader() || !ValidateGLN(GTIN))
                    return null;

                string query_lotes = @"SELECT SL.NOME AS STATUSLOTE, P.GLOBALTRADEITEMNUMBER, P.INFORMATIONPROVIDER AS GLN, '10' AS LOTEAI, L.NRLOTE AS NRLOTE, L.DATAPROCESSAMENTO, L.DATAVENCIMENTO,
	                                        L.TIPOPROCESSAMENTOBIZSTEP AS TIPOPROCESSAMENTO, SG.NOME AS STATUSPRODUTO, LURL.NOME AS URL, L.INFORMACOESADICIONAIS, B.NOME AS BIZSTEP, D.NOME AS DISPOSITION, 
	                                        CONCAT(P.GLOBALTRADEITEMNUMBER, L.NRLOTE) AS LGTIN, '' AS GS1STRING, L.QUANTIDADEITENS AS QUANTIDADE
	                                        FROM LOTE L
	                                        INNER JOIN PRODUTO P ON P.CODIGOPRODUTO = L.CODIGOPRODUTO
	                                        INNER JOIN STATUSGTIN SG ON SG.CODIGO = P.CODIGOSTATUSGTIN
	                                        INNER JOIN BIZSTEP B ON B.CODIGO = L.CODIGOBIZSTEP
	                                        INNER JOIN DISPOSITIONS D ON D.CODIGOBIZSTEP = B.CODIGO
	                                        LEFT JOIN LOTEURL LURL ON LURL.CODIGOLOTE = L.CODIGO
	                                        LEFT JOIN STATUSLOTE SL ON SL.CODIGO = L.CODIGOSTATUSLOTE
	                                        WHERE P.GLOBALTRADEITEMNUMBER = @gtin
	                                        AND L.NRLOTE = @lote 
                                            AND L.NRLOTE IS NOT NULL";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    dynamic param = new ExpandoObject();
                    param.gtin = GTIN;
                    param.lote = Lote;

                    var results = ((List<dynamic>)db.Select(query_lotes, param));

                    if (IsResultEmpty(results)) return null;

                    ResponseHeader.returnCode = 0;

                    ResponseHeader.numberOfHits = ((results != null)) ? results.Count : 0;
                    return BuildLoteParty(results);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.WEBSERVICE.GEPIRSERVICE - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);


                }
                catch (Exception)
                {


                }

                ResponseHeader.returnCode = 99;
                ResponseHeader.numberOfHits = 0;
                return null;
            }
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public LoteParty GetLotbyList(XMLLoteGTIN[] xmlparam)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            dynamic param = new ExpandoObject();

            try
            {

                string query_lotes = @"SELECT SL.NOME AS STATUSLOTE, P.GLOBALTRADEITEMNUMBER, P.INFORMATIONPROVIDER AS GLN, '10' AS LOTEAI, L.NRLOTE AS NRLOTE, L.DATAPROCESSAMENTO, L.DATAVENCIMENTO,
	                                        L.TIPOPROCESSAMENTOBIZSTEP AS TIPOPROCESSAMENTO, SG.NOME AS STATUSPRODUTO, LURL.NOME AS URL, L.INFORMACOESADICIONAIS, B.NOME AS BIZSTEP, D.NOME AS DISPOSITION, 
	                                        dbo.fn_CalcularLGTIN(P.GLOBALTRADEITEMNUMBER, L.NRLOTE) AS LGTIN, CONCAT('(01)', P.GLOBALTRADEITEMNUMBER, '(10)', L.NRLOTE) AS GS1STRING, L.QUANTIDADEITENS AS QUANTIDADE
	                                        FROM LOTE L
	                                        INNER JOIN PRODUTO P ON P.CODIGOPRODUTO = L.CODIGOPRODUTO
	                                        INNER JOIN STATUSGTIN SG ON SG.CODIGO = P.CODIGOSTATUSGTIN
	                                        INNER JOIN BIZSTEP B ON B.CODIGO = L.CODIGOBIZSTEP
	                                        INNER JOIN DISPOSITIONS D ON D.CODIGOBIZSTEP = B.CODIGO
	                                        LEFT JOIN LOTEURL LURL ON LURL.CODIGOLOTE = L.CODIGO
	                                        LEFT JOIN STATUSLOTE SL ON SL.CODIGO = L.CODIGOSTATUSLOTE 
                                            WHERE 1 = 1 ";

                string where = string.Empty;

                foreach (var item in xmlparam.Select((value, index) => new { value, index }))
                {
                    if (!ValidateHeader() || !ValidateGLN(item.value.GTIN))
                      return null;

                    (param as IDictionary<string, object>)["gtin" + item.index.ToString()] = item.value.GTIN;
                    (param as IDictionary<string, object>)["lote" + item.index.ToString()] = item.value.Lote;

                    if (item.index == 0) where += " AND "; 
                    else where += " OR ";

                    where += @"((P.GLOBALTRADEITEMNUMBER = @gtin" + item.index.ToString() + @"
	                            AND L.NRLOTE = @lote" + item.index.ToString() + @")) ";
                }

                if (where != string.Empty)
                {
                    query_lotes += where;
                }

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();

                    var results = ((List<dynamic>)db.Select(query_lotes, param));

                    if (IsResultEmpty(results)) return null;

                    ResponseHeader.returnCode = 0;

                    ResponseHeader.numberOfHits = ((results != null)) ? results.Count : 0;
                    return BuildLoteParty(results);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.WEBSERVICE.GEPIRSERVICE - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);


                }
                catch (Exception)
                {



                }

                ResponseHeader.returnCode = 99;
                ResponseHeader.numberOfHits = 0;
                return null;
            }
        }

        private ItemTradeItemUnitDescriptor resolveItemTradeItemUnitDescriptor(int idTipoProduto)
        {
            switch (idTipoProduto)
            {
                case 1:
                    return ItemTradeItemUnitDescriptor.PALLET;
                case 2:
                    return ItemTradeItemUnitDescriptor.BASE_UNIT_OR_EACH;
                case 3:
                    return ItemTradeItemUnitDescriptor.CASE;
                default:
                    return ItemTradeItemUnitDescriptor.BASE_UNIT_OR_EACH;
            }
        }

        private GepirExclusiveItem BuildGepirExclusiveItem(dynamic produtos, dynamic certificado)
        {
            var gepirItem = BuildGepirItem(produtos);
            var gepirExclusiveItem = new GepirExclusiveItem();
            var exclusiveItems = new List<ExclusiveItem>();

            foreach (var item in gepirItem.itemDataLine)
            {
                //var certificados = relCertificados.Where(x => x.Gtin == item.gtin);
                //foreach (var certificado in certificados)
                //foreach (var certificado in relCertificados)
                //{
                    exclusiveItems.Add(new ExclusiveItem
                    {
                        gtin = item.gtin != null && item.gtin.ToString() != "" ? item.gtin : null,
                        informationProviderGln = item.informationProviderGln != null && item.informationProviderGln.ToString() != "" ? item.informationProviderGln : null,
                        itemName = item.itemName != null && item.itemName.ToString() != "" ? item.itemName : null,
                        brandName = item.brandName != null && item.brandName.ToString() != "" ? item.brandName : null,
                        tradeItemUnitDescriptor = item.tradeItemUnitDescriptor != null && item.tradeItemUnitDescriptor.ToString() != "" ? item.tradeItemUnitDescriptor : null,
                        childItems = item.childItems != null && item.childItems.ToString() != "" ? item.childItems : null,
                        ImageUrl = item.ImageUrl != null && item.ImageUrl.ToString() != "" ? item.ImageUrl : null,
                        UrlMobilecom = item.UrlMobilecom != null && item.UrlMobilecom.ToString() != "" ? item.UrlMobilecom : null,
                        netContent = item.netContent != null && item.netContent.ToString() != "" && item.netContent.Value != null && item.netContent.Value.ToString() != "" ? item.netContent : null,

                        CertificateName = certificado.descricaostatus.Equals("SEM CERTIFICADO", StringComparison.InvariantCultureIgnoreCase)
                            ? certificado.descricaostatus : certificado.nomecertificado,
                        AssignDate = certificado.dataatribuicao != null && certificado.dataatribuicao.ToString() != "" ? certificado.dataatribuicao : null,
                        ExpirationDate = certificado.dataexpiracao != null && certificado.dataexpiracao.ToString() != "" ? certificado.dataexpiracao : null,
                        Status = certificado.status != null && certificado.status.ToString() != "" ? certificado.status : null

                    });
                //}
            }
            gepirExclusiveItem.itemDataLine = exclusiveItems.ToArray();
            return gepirExclusiveItem;
        }

        private GepirItem BuildGepirItem(dynamic produto)
        {
            var item = new GepirItem();
            var itemDataLines = new List<Item>();

            string query = @"SELECT Gtin, GtinInferior, QtdeItens 
                                FROM tbProdutoGtinInferior
                                WHERE GTIN = @gtin";

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                if (produto != null)
                {
                    //foreach (var produto in produtos)
                    //{
                    //if (produto.tipogtin != null && produto.tipogtin == 8 && string.IsNullOrWhiteSpace(produto.nrprefixo))
                    if (produto.tipogtin != null && produto.tipogtin == 1 && string.IsNullOrWhiteSpace(produto.nrprefixo))
                        produto.nrprefixo = produto.gtin;

                    dynamic param_produto_inferior = new ExpandoObject();
                    param_produto_inferior.gtin = produto.gtin;
                    var inferioresResult = ((List<dynamic>)db.Select(query, param_produto_inferior));

                    //var inferioresResult = ContainerFactory.GetDomainContainer().Resolve<ProdutoGtinInferiorBiz>().GetByGtin(produto.Gtin);

                    var inferiores = inferioresResult.Select(i => new ItemChildItem
                    {
                        gtin = i.gtininferior.ToString(),
                        numberContained = i.qtdeitens.ToString()
                    }).ToList();

                    //var inferiores = inferioresResult.ToList();

                    var urlImage = string.IsNullOrEmpty(produto.urlfoto1)
                        ? (string.IsNullOrEmpty(produto.urlfoto2) ? produto.urlfoto3 : produto.urlfoto2)
                        : produto.urlfoto1;

                    itemDataLines.Add(new Item
                    {
                        gtin = produto.gtin != null && produto.gtin.ToString() != "" ? produto.gtin : null,
                        informationProviderGln = "7898357410008",
                        //manufacturerGln
                        itemName = produto.descricao != null && produto.descricao.ToString() != "" ? produto.descricao : null,
                        brandName = produto.marcaproduto != null && produto.marcaproduto.ToString() != "" ? produto.marcaproduto : null,
                        //descriptiveSize
                        lastChangeDateTime = produto.lastchangedatetime != null && produto.lastchangedatetime.ToString() != "" ? produto.lastchangedatetime : null,
                        tradeItemUnitDescriptor = resolveItemTradeItemUnitDescriptor(produto.idtipoproduto != null && produto.idtipoproduto.ToString() != "" ? produto.idtipoproduto : 0),
                        //linkUri
                        //classificationCode 
                        childItems = inferiores.ToArray(),
                        ImageUrl = urlImage != null && urlImage.ToString() != "" ? urlImage : null,
                        UrlMobilecom = produto.urlmobilecom != null && produto.urlmobilecom.ToString() != "" ? produto.urlmobilecom : null,
                        totalQuantityOfNextLowerLevelTradeItem = produto.totalquantityofnextlowerleveltradeitem != null && produto.totalquantityofnextlowerleveltradeitem.ToString() != "" ? produto.totalquantityofnextlowerleveltradeitem : null,
                        width = produto.largura != null && produto.largura.ToString() != "" ? produto.largura : null,
                        widthMeasurementUnitCode = produto.widthmeasurementunitcode != null && produto.widthmeasurementunitcode.ToString() != "" ? produto.widthmeasurementunitcode : null,
                        height = produto.altura != null && produto.altura.ToString() != "" ? produto.altura : null,
                        heightMeasurementUnitCode = produto.heightmeasurementunitcode != null && produto.heightmeasurementunitcode.ToString() != "" ? produto.heightmeasurementunitcode : null,
                        depth = produto.profundidade != null && produto.profundidade.ToString() != "" ? produto.profundidade : null,
                        depthMeasurementUnitCode = produto.depthmeasurementunitcode != null && produto.depthmeasurementunitcode.ToString() != "" ? produto.depthmeasurementunitcode : null,
                        netContent = produto.netcontent != null && produto.netcontent.ToString() != "" ? produto.netcontent : null,
                        netContentMeasurementUnitCode = produto.netcontentmeasurementunitcode != null && produto.netcontentmeasurementunitcode.ToString() != "" ? produto.netcontentmeasurementunitcode : null, 
                        quantityOfChildren = inferiores.Count,
                        CodeSegment = produto.codesegment != null && produto.codesegment.ToString() != "" ? produto.codesegment : null,                        
                        CodeFamily = produto.codefamily != null && produto.codefamily.ToString() != "" ? produto.codefamily : null,
                        CodeClass = produto.codeclass != null && produto.codeclass.ToString() != "" ? produto.codeclass : null,
                        CodeBrick = produto.codebrick != null && produto.codebrick.ToString() != "" ? produto.codebrick : null,
                        //netContent = produto.pesoliquido != null && produto.pesoliquido.ToString() != "" && produto.pesoliquido.Value != null && produto.pesoliquido.Value.ToString() != "" ? new ItemNetContent { uom = UomType.KGM, Value = produto.pesoliquido.ToString() } : null,
                                                                                                  
                    });
                    //}
                }
                item.itemDataLine = itemDataLines.ToArray();
                return item;
            }
        }

        private GepirParty BuildParty(dynamic companies)
        {
            var gepirParty = new GepirParty();
            var partyDataLines = new List<Party>();
            //foreach (var c in companies)
            //{
                partyDataLines.Add(
                    new Party
                    {
                        returnCode = "0",
                        gln = companies.nrgln,
                        additionalPartyId = new string[] { companies.cpfcnpj },
                        partyName = companies.razaosocial,
                        streetAddress = new string[] { companies.endereco },
                        postalCode = companies.cep,
                        city = companies.cidade,
                        countryISOCode = companies.pais
                    });
            //}

            gepirParty.partyDataLine = partyDataLines.ToArray();
            return gepirParty;
        }

        private bool IsResultEmpty(ICollection results)
        {
            if (results != null && results.Count != 0) return false;

            ResponseHeader.returnCode = 8;
            return true;
        }

        private bool IsResultEmpty(dynamic results)
        {
            if (results != null && ((IDictionary<string, object>)results).Count != 0) return false;

            ResponseHeader.returnCode = 8;
            return true;
        }

        private dynamic FilterResultsBasedOnSharingInformation(dynamic result)
        {
            if (result.CompartilhaDado != null && result.CompartilhaDado.Equals("S"))
                return result;
            
            ResponseHeader.returnCode = 12;
            return null;
        }        

        private List<RelProdutoItemView> FilterResultsBasedOnSharingInformation(IEnumerable<RelProdutoItemView> results)
        {
            var list = new List<RelProdutoItemView>();

            Parallel.ForEach(results, currentResult =>
            {
                var filteredResult = FilterResultsBasedOnSharingInformation(currentResult);

                if (filteredResult != null)
                    list.Add(filteredResult);
            });

            return list;
        }

        private static bool CompanyIsOverdue(dynamic company)
        {
            return (company.status == null || !company.status.Equals("A")) || (company.statuspagto == null || !company.statuspagto.Equals("A"));
        }

        private static bool CompanyNeverShareInfo(dynamic company)
        {
            return (company.compartilhadado != null && company.compartilhadado.Equals("N"));
        }

        private static bool CompanyAlwaysShareInfo(dynamic company)
        {
            return (company.compartilhadado != null && company.compartilhadado.Equals("S"));
        }

        private bool ValidateGTIN(string value)
        {
            if (value.Length == 14 || value.Length == 13 || value.Length == 8)
                return true;

            ResponseHeader.returnCode = 1;
            return true;
        }

        private bool ValidateGLN(string value)
        {
            if (value.Length == 13)
                return true;

            ResponseHeader.returnCode = 1;
            return false;
        }

        private bool ValidateHeader()
        {
            if (RequestHeader == null || string.IsNullOrEmpty(RequestHeader.RequesterGln))
            {
                ResponseHeader.returnCode = 1;
                return false;
            }
            if (!InformedUserIsValid() || !InformedGlnIsValid())
            {
                ResponseHeader.returnCode = 1;
                return false;
            }

            return ValidateGLN(RequestHeader.RequesterGln);
        }

        private bool InformedGlnIsValid()
        {
            return ConfigurationManager.AppSettings["SearchAllowedGLN"].Equals(RequestHeader.RequesterGln);
        }

        private bool InformedUserIsValid()
        {
            var array = ConfigurationManager.AppSettings.Get("WSUSER").Split(';');

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == RequestHeader.WSUSER)
                    return true;
            }

            return false;
            //return ConfigurationManager.AppSettings.Get("WSUSER").Split(';').Any(x => x.Equals(RequestHeader.WSUSER));
        }

        private dynamic GetFilteredResults(dynamic results, string requestedGTIN)
        {
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                if (RequestHeader.IsExclusive)
                    return results;

                string query_associado = @"SELECT IdAssociado, CPFCNPJ, RazaoSocial, Status, StatusPagto, ContatoMaster, EmailContato, DtVencTaxa, CEP,
                                                Endereco, Numero, Complemento, Bairro, Cidade, UF, Pais, DtUltimaSinc, InscricaoEstadual, CompartilhaDado 
                                                FROM tbAssociado
                                                WHERE IdAssociado = @IdAssociado";

                dynamic param_associado = new ExpandoObject();
                param_associado.IdAssociado = results.GetType() == typeof(RelProdutoItemView) ? results.IdAssociado : results.idassociado;
                var company = ((List<dynamic>)db.Select(query_associado, param_associado)).FirstOrDefault();
                //var company = ContainerFactory.GetDomainContainer().Resolve<AssociadoBiz>().GetById(results.First().IdAssociado);

                if (CompanyIsOverdue(company))
                {
                    // deixar a chamada pronta caso haja necessidade de mudar as informações de registro no RGC
                    //var gln = ContainerFactory.GetDomainContainer().Resolve<AssociadoGLNBiz>().GetAll().FirstOrDefault(x => x.IdAssociado == results.First().IdAssociado);

                    //try
                    //{
                    //    new RGC.WSGS1004().GS1GRAVACONSULTA("MSALPHA", company.CpfCnpj, string.Empty, "GTIN", requestedGTIN, string.Empty, string.Empty, string.Empty);
                    //}
                    //catch (Exception) { }

                    ResponseHeader.returnCode = 11;
                    return null;
                }

                if (CompanyNeverShareInfo(company))
                {
                    ResponseHeader.returnCode = 9;
                    return null;
                }

                if (CompanyAlwaysShareInfo(company))
                    return results;

                return FilterResultsBasedOnSharingInformation(results);
            }
        }

        private dynamic GetFilteredResultsV2(dynamic results, string requestedGTIN)
        {
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                if (RequestHeader.IsExclusive)
                    return results;

                string query_associado = @"SELECT IdAssociado, CPFCNPJ, RazaoSocial, Status, StatusPagto, ContatoMaster, EmailContato, DtVencTaxa, CEP,
                                                Endereco, Numero, Complemento, Bairro, Cidade, UF, Pais, DtUltimaSinc, InscricaoEstadual, CompartilhaDado 
                                                FROM tbAssociado
                                                WHERE IdAssociado = @CodigoAssociado";

                dynamic param_associado = new ExpandoObject();
                param_associado.CodigoAssociado = results.CodigoAssociado;
                var company = ((List<dynamic>)db.Select(query_associado, param_associado)).FirstOrDefault();
                

                if (CompanyIsOverdue(company))
                {
                    // deixar a chamada pronta caso haja necessidade de mudar as informações de registro no RGC
                    //var gln = ContainerFactory.GetDomainContainer().Resolve<AssociadoGLNBiz>().GetAll().FirstOrDefault(x => x.IdAssociado == results.First().IdAssociado);

                    //try
                    //{
                    //    new RGC.WSGS1004().GS1GRAVACONSULTA("MSALPHA", company.CpfCnpj, string.Empty, "GTIN", requestedGTIN, string.Empty, string.Empty, string.Empty);
                    //}
                    //catch (Exception) { }

                    ResponseHeader.returnCode = 11;
                    return null;
                }

                if (CompanyNeverShareInfo(company))
                {
                    ResponseHeader.returnCode = 9;
                    return null;
                }

                if (CompanyAlwaysShareInfo(company))
                    return results;

                return FilterResultsBasedOnSharingInformation(results);
            }
        }


        private bool LogGTINCNP20(string GTIN)
        {

            string query = @"SELECT P.CODIGOPRODUTO AS CODIGOPRODUTO, 
		                            GETDATE() AS DATACONSULTA, 
                                    P.GLOBALTRADEITEMNUMBER,
		                            P.INDICADORCOMPARTILHADADOS AS 'COMPARTILHADADOSPRODUTO',
		                            A.CODIGOTIPOCOMPARTILHAMENTO AS 'COMPARTILHADADOSASSOCIADO' 
	                            FROM PRODUTO P
	                            INNER JOIN ASSOCIADO A ON A.CODIGO = P.CODIGOASSOCIADO
	                            LEFT JOIN CONTATOASSOCIADO CA ON CA.CODIGOASSOCIADO = A.CODIGO
	                            WHERE P.GLOBALTRADEITEMNUMBER = " + GTIN;

            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();

                var results = ((List<dynamic>)db.Select(query, null)).FirstOrDefault();
                //Encontrou o GTIN Informado no CNP 2.0
                if (results != null) 
                {
                    db.Insert("LogGerencialInbar", "Codigo", results);
                    return true;
                }
                //Não encontrou o GTIN Informado no CNP 2.0
                else 
                {
                    dynamic param = new ExpandoObject();
                    param.dataconsulta = DateTime.Now;
                    param.globaltradeitemnumber = GTIN;

                    db.Insert("LogGerencialInbar", "Codigo", param);

                    return false;
                }
            }
        }

        private LoteParty BuildLoteParty(dynamic lotes)
        {
            var loteParty = new LoteParty();
            var listLoteParty = new List<Lote>();

            foreach (var l in lotes)
            {
                listLoteParty.Add(
                    new Lote
                    {
                        returnCode = "0",
                        gln = l.gln,
                        statuslote = l.statuslote,
                        loteai = l.loteai,
                        lote = l.nrlote,
                        dataprocessamento = l.dataprocessamento,
                        datavencimento = l.datavencimento,
                        tipoprocessamento = l.tipoprocessamento,
                        url = l.url,
                        informacoesadicionais = l.informacoesadicionais,
                        bizstep = l.bizstep,
                        disposition = l.disposition,
                        lgtin = l.lgtin,
                        gs1string = l.gs1string,
                        quantidade = l.quantidade != null && l.quantidade.ToString() != string.Empty ? l.quantidade : 0,
                        gtin = l.globaltradeitemnumber,
                        statusproduto = l.statusproduto
                    }
                );
            }

            loteParty.partyLotes = listLoteParty.ToArray();
            return loteParty;
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public RelProdutoItemViewV2 GetCNPItemByGTINFull(string gtin)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            try
            {
                //Armazena o LOG da busca pelo GTIN
                LogGTINCNP20(gtin);

                if (!ValidateHeader() || !ValidateGTIN(gtin))
                    return null;

                string query = @"SELECT    A.[CodigoProduto]
                                          ,A.[globalTradeItemNumber]
                                          ,A.[productDescription]
                                          ,A.[CodigoTipoGTIN]
                                          ,A.[NrPrefixo]
                                          ,A.[CodItem]
                                          ,A.[VarianteLogistica]
                                          ,A.[CodigoAssociado]
                                          ,A.[CodigoUsuarioCriacao]
                                          ,A.[CodigoUsuarioAlteracao]
                                          ,A.[CodigoUsuarioExclusao]
                                          ,A.[tradeItemCountryOfOrigin]
                                          ,A.[CodigoStatusGTIN]
                                          ,A.[CodeSegment]
                                          ,A.[CodeFamily]
                                          ,A.[CodeClass]
                                          ,A.[CodeBrick]
                                          ,A.[countryCode]
                                          ,A.[CodeBrickAttribute]
                                          ,A.[IndicadorCompartilhaDados]
                                          ,A.[Observacoes]
                                          ,A.[Estado]
                                          ,A.[informationProvider]
                                          ,A.[brandName]
                                          ,A.[DataInclusao]
                                          ,A.[DataAlteracao]
                                          ,A.[DataSuspensao]
                                          ,A.[DataReativacao]
                                          ,A.[DataCancelamento]
                                          ,A.[DataReutilizacao]
                                          ,A.[modelNumber]
                                          ,A.[importClassificationType]
                                          ,A.[importClassificationValue]
                                          ,A.[minimumTradeItemLifespanFromTimeOfProduction]
                                          ,A.[startAvailabilityDateTime]
                                          ,A.[endAvailabilityDateTime]
                                          ,A.[depth]
                                          ,A.[depthMeasurementUnitCode]
                                          ,A.[height]
                                          ,A.[heightMeasurementUnitCode]
                                          ,A.[width]
                                          ,A.[widthMeasurementUnitCode]
                                          ,A.[netContent]
                                          ,A.[netContentMeasurementUnitCode]
                                          ,A.[grossWeight]
                                          ,A.[grossWeightMeasurementUnitCode]
                                          ,A.[netWeight]
                                          ,A.[netWeightMeasurementUnitCode]
                                          ,A.[CodigoTipoProduto]
                                          ,A.[isTradeItemABaseUnit]
                                          ,A.[isTradeItemAConsumerUnit]
                                          ,A.[isTradeItemAModel]
                                          ,A.[isTradeItemAnInvoiceUnit]
                                          ,A.[packagingTypeCode]
                                          ,A.[PalletTypeCode]
                                          ,A.[totalQuantityOfNextLowerLevelTradeItem]
                                          ,A.[StackingFactor]
                                          ,A.[quantityOfTradeItemContainedInACompleteLayer]
                                          ,A.[quantityOfTradeItemsPerPalletLayer]
                                          ,A.[quantityOfCompleteLayersContainedInATradeItem]
                                          ,A.[quantityOfLayersPerPallet]
                                          ,A.[CodigoProdutoOrigem]
                                          ,A.[deliveryToDistributionCenterTemperatureMinimum]
                                          ,A.[storageHandlingTempMinimumUOM]
                                          ,A.[storageHandlingTemperatureMaximum]
                                          ,A.[storageHandlingTemperatureMaximumunitOfMeasure]
                                          ,A.[isDangerousSubstanceIndicated]
                                          ,A.[ipiPerc]
                                          ,A.[isTradeItemAnOrderableUnit]
                                          ,A.[isTradeItemADespatchUnit]
                                          ,A.[orderSizingFactor]
                                          ,A.[orderQuantityMultiple]
                                          ,A.[orderQuantityMinimum]
                                          ,A.[barcodeCertified]
                                          ,A.[dataQualityCertified]
                                          ,A.[dataQualityCertifiedAgencyCode]
                                          ,A.[allGDSNAttributes]
                                          ,A.[CanalComunicacaoDados]
                                          ,A.[ProprioDonoInformacao]
                                          ,A.[ValidadoDonoDaInformacao]
                                          ,A.[IndicadorGDSN]
                                          ,A.[CodigoLingua]

	                                     
	                                      ,B.CPFCNPJ
	                                      ,B.CAD
	                                      ,B.Nome NomeAssociado

	                                      ,C.Nome NomeTipoGtin

	                                      ,D.Nome NomeUsuarioCriacao
	                                      ,E.Nome NomeUsuarioAlteracao
	                                      ,F.Nome NomeUsuarioExclusao

	                                      ,G.Nome NometradeItemCountryOfOrigin
	                                      ,H.Nome NomeStatusGTIN
	                                      ,I.Nome NomecountryCode
	                                      ,J.Nome NomeLingua

	                                      ,K.Nome NomeTipoProduto

                                      FROM [dbo].[Produto] A
                                    INNER JOIN ASSOCIADO B ON B.CODIGO = A.CodigoAssociado
                                    INNER JOIN TipoGTIN C ON C.CODIGO = A.CodigoTipoGTIN
                                    LEFT JOIN Usuario D ON D.Codigo = A.CodigoUsuarioCriacao
                                    LEFT JOIN Usuario E ON E.Codigo = A.CodigoUsuarioAlteracao
                                    LEFT JOIN Usuario F ON F.Codigo = A.CodigoUsuarioExclusao
                                    LEFT JOIN CountryOfOriginISO3166 G ON G.CodigoPais = A.tradeItemCountryOfOrigin
                                    INNER JOIN StatusGTIN H ON H.Codigo = A.CodigoStatusGTIN
                                    LEFT JOIN CountryOfOriginISO3166 I ON I.CodigoPais = A.countryCode
                                    LEFT JOIN Lingua J ON J.Codigo = A.CodigoLingua
                                    LEFT JOIN TipoProduto K ON K.Codigo = A.CodigoTipoProduto
 


                                    WHERE [globalTradeItemNumber] = @gtin";

                using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
                {
                    db.Open();
                    dynamic param = new ExpandoObject();
                    param.gtin = gtin;

                    var gtinResult = ((List<dynamic>)db.Select(query, param)).FirstOrDefault();

                    if (gtinResult == null)
                    {
                        ResponseHeader.returnCode = 8;
                        return null;
                    }

                    var list = new RelProdutoItemViewV2();

                    list.CodigoProduto = gtinResult.codigoproduto;
                    list.globalTradeItemNumber = gtinResult.globaltradeitemnumber;
                    list.productDescription = gtinResult.productdescription;
                    list.CodigoTipoGTIN = gtinResult.codigotipogtin;
                    list.NrPrefixo = gtinResult.nrprefixo;
                    list.CodItem = gtinResult.coditem;
                    if (gtinResult.variantelogistica != null && gtinResult.variantelogistica.ToString() != "") list.VarianteLogistica = gtinResult.variantelogistica;
                    list.CodigoAssociado = gtinResult.codigoassociado;
                    list.CodigoUsuarioCriacao = gtinResult.codigousuariocriacao;
                    if (gtinResult.codigousuarioalteracao != null && gtinResult.codigousuarioalteracao.ToString() != "") list.CodigoUsuarioAlteracao = gtinResult.codigousuarioalteracao;
                    if (gtinResult.codigousuarioexclusao != null && gtinResult.codigousuarioexclusao.ToString() != "") list.CodigoUsuarioExclusao = gtinResult.codigousuarioexclusao;
                    if (gtinResult.tradeitemcountryoforigin != null && gtinResult.tradeitemcountryoforigin.ToString() != "") list.tradeItemCountryOfOrigin = gtinResult.tradeitemcountryoforigin;
                    list.CodigoStatusGTIN = gtinResult.codigostatusgtin;
                    list.CodeSegment = gtinResult.codesegment;
                    list.CodeFamily = gtinResult.codefamily;
                    list.CodeClass = gtinResult.codeclass;
                    list.CodeBrick = gtinResult.codebrick;
                    list.countryCode = gtinResult.countrycode;
                    if (gtinResult.codebrickattribute != null && gtinResult.codebrickattribute.ToString() != "") list.CodeBrickAttribute = gtinResult.codebrickattribute;
                    list.IndicadorCompartilhaDados = gtinResult.indicadorcompartilhadados;
                    list.Observacoes = gtinResult.observacoes;
                    list.Estado = gtinResult.estado;
                    if (gtinResult.informationprovider != null && gtinResult.informationprovider.ToString() != "") list.informationProvider = gtinResult.informationprovider;
                    list.brandName = gtinResult.brandname;
                    if (gtinResult.datainclusao != null && gtinResult.datainclusao.ToString() != "") list.DataInclusao = Convert.ToDateTime(gtinResult.datainclusao.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                    if (gtinResult.dataalteracao != null && gtinResult.dataalteracao.ToString() != "") list.DataAlteracao = Convert.ToDateTime(gtinResult.dataalteracao.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                    if (gtinResult.datasuspensao != null && gtinResult.datasuspensao.ToString() != "") list.DataSuspensao = Convert.ToDateTime(gtinResult.datasuspensao.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                    if (gtinResult.datareativacao != null && gtinResult.datareativacao.ToString() != "") list.DataReativacao = Convert.ToDateTime(gtinResult.datareativacao.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                    if (gtinResult.datacancelamento != null && gtinResult.datacancelamento.ToString() != "") list.DataCancelamento = Convert.ToDateTime(gtinResult.datacancelamento.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                    if (gtinResult.datareutilizacao != null && gtinResult.datareutilizacao.ToString() != "") list.DataReutilizacao = Convert.ToDateTime(gtinResult.datareutilizacao.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                    list.modelNumber = gtinResult.modelnumber;
                    list.importClassificationType = gtinResult.importclassificationtype;
                    list.importClassificationValue = gtinResult.importclassificationvalue;
                    if (gtinResult.minimumtradeitemlifespanfromtimeofproduction != null && gtinResult.minimumtradeitemlifespanfromtimeofproduction.ToString() != "") list.minimumTradeItemLifespanFromTimeOfProduction = gtinResult.minimumtradeitemlifespanfromtimeofproduction;
                    if (gtinResult.startavailabilitydatetime != null && gtinResult.startavailabilitydatetime.ToString() != "") list.startAvailabilityDateTime = Convert.ToDateTime(gtinResult.startavailabilitydatetime.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                    if (gtinResult.endavailabilitydatetime != null && gtinResult.endavailabilitydatetime.ToString() != "") list.endAvailabilityDateTime = Convert.ToDateTime(gtinResult.endavailabilitydatetime.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                    if (gtinResult.depth != null && gtinResult.depth.ToString() != "") list.depth = Convert.ToDecimal(gtinResult.depth.ToString());
                    if (gtinResult.depthmeasurementunitcode != null && gtinResult.depthmeasurementunitcode.ToString() != "") list.depthMeasurementUnitCode = gtinResult.depthmeasurementunitcode;
                    if (gtinResult.height != null && gtinResult.height.ToString() != "") list.height = Convert.ToDecimal(gtinResult.height.ToString());
                    if (gtinResult.heightmeasurementunitcode != null && gtinResult.heightmeasurementunitcode.ToString() != "") list.heightMeasurementUnitCode = gtinResult.heightmeasurementunitcode;
                    if (gtinResult.width != null && gtinResult.width.ToString() != "") list.width = Convert.ToDecimal(gtinResult.width.ToString());
                    if (gtinResult.widthmeasurementunitcode != null && gtinResult.widthmeasurementunitcode.ToString() != "") list.widthMeasurementUnitCode = gtinResult.widthmeasurementunitcode;
                    if (gtinResult.netcontent != null && gtinResult.netcontent.ToString() != "") list.netContent = Convert.ToDecimal(gtinResult.netcontent.ToString());
                    if (gtinResult.netcontentmeasurementunitcode != null && gtinResult.netcontentmeasurementunitcode.ToString() != "") list.netContentMeasurementUnitCode = gtinResult.netcontentmeasurementunitcode;
                    if (gtinResult.grossweight != null && gtinResult.grossweight.ToString() != "") list.grossWeight = Convert.ToDecimal(gtinResult.grossweight.ToString());
                    if (gtinResult.grossweightmeasurementunitcode != null && gtinResult.grossweightmeasurementunitcode.ToString() != "") list.grossWeightMeasurementUnitCode = gtinResult.grossweightmeasurementunitcode;
                    if (gtinResult.netweight != null && gtinResult.netweight.ToString() != "") list.netWeight = Convert.ToDecimal(gtinResult.netweight.ToString());
                    if (gtinResult.netweightmeasurementunitcode != null && gtinResult.netweightmeasurementunitcode.ToString() != "") list.netWeightMeasurementUnitCode = gtinResult.netweightmeasurementunitcode;
                    if (gtinResult.codigotipoproduto != null && gtinResult.codigotipoproduto.ToString() != "") list.CodigoTipoProduto = gtinResult.codigotipoproduto;
                    if (gtinResult.istradeitemabaseunit != null && gtinResult.istradeitemabaseunit.ToString() != "") list.isTradeItemABaseUnit = gtinResult.istradeitemabaseunit;
                    if (gtinResult.istradeitemaconsumerunit != null && gtinResult.istradeitemaconsumerunit.ToString() != "") list.isTradeItemAConsumerUnit = gtinResult.istradeitemaconsumerunit;
                    if (gtinResult.istradeitemamodel != null && gtinResult.istradeitemamodel.ToString() != "") list.isTradeItemAModel = gtinResult.istradeitemamodel;
                    if (gtinResult.istradeitemaninvoiceunit != null && gtinResult.istradeitemaninvoiceunit.ToString() != "") list.isTradeItemAnInvoiceUnit = gtinResult.istradeitemaninvoiceunit;
                    list.packagingTypeCode = gtinResult.packagingtypecode;
                    list.PalletTypeCode = gtinResult.codebrick;
                    if (gtinResult.totalquantityofnextlowerleveltradeitem != null && gtinResult.totalquantityofnextlowerleveltradeitem.ToString() != "") list.totalQuantityOfNextLowerLevelTradeItem = gtinResult.totalquantityofnextlowerleveltradeitem;
                    if (gtinResult.stackingfactor != null && gtinResult.stackingfactor.ToString() != "") list.StackingFactor = gtinResult.stackingfactor;
                    if (gtinResult.quantityoftradeitemcontainedinacompletelayer != null && gtinResult.quantityoftradeitemcontainedinacompletelayer.ToString() != "") list.quantityOfTradeItemContainedInACompleteLayer = gtinResult.quantityoftradeitemcontainedinacompletelayer;
                    if (gtinResult.quantityoftradeitemsperpalletlayer != null && gtinResult.quantityoftradeitemsperpalletlayer.ToString() != "") list.quantityOfTradeItemsPerPalletLayer = gtinResult.quantityoftradeitemsperpalletlayer;
                    if (gtinResult.quantityofcompletelayerscontainedinatradeitem != null && gtinResult.quantityofcompletelayerscontainedinatradeitem.ToString() != "") list.quantityOfCompleteLayersContainedInATradeItem = gtinResult.quantityofcompletelayerscontainedinatradeitem;
                    if (gtinResult.quantityoflayersperpallet != null && gtinResult.quantityoflayersperpallet.ToString() != "") list.quantityOfLayersPerPallet = gtinResult.quantityoflayersperpallet;
                    if (gtinResult.codigoprodutoorigem != null && gtinResult.codigoprodutoorigem.ToString() != "") list.CodigoProdutoOrigem = gtinResult.codigoprodutoorigem;
                    if (gtinResult.deliverytodistributioncentertemperatureminimum != null && gtinResult.deliverytodistributioncentertemperatureminimum.ToString() != "") list.deliveryToDistributionCenterTemperatureMinimum = Convert.ToDecimal(gtinResult.deliverytodistributioncentertemperatureminimum.ToString());
                    list.storageHandlingTempMinimumUOM = gtinResult.storagehandlingtempminimumuom;
                    if (gtinResult.storagehandlingtemperaturemaximum != null && gtinResult.storagehandlingtemperaturemaximum.ToString() != "") list.storageHandlingTemperatureMaximum = Convert.ToDecimal(gtinResult.storagehandlingtemperaturemaximum.ToString());
                    list.storageHandlingTemperatureMaximumunitOfMeasure = gtinResult.storagehandlingtemperaturemaximumunitofmeasure;
                    if (gtinResult.isdangeroussubstanceindicated != null && gtinResult.isdangeroussubstanceindicated.ToString() != "") list.isDangerousSubstanceIndicated = gtinResult.isdangeroussubstanceindicated;
                    if (gtinResult.ipiperc != null && gtinResult.ipiperc.ToString() != "") list.ipiPerc = Convert.ToDecimal(gtinResult.ipiperc.ToString());
                    if (gtinResult.istradeitemanorderableunit != null && gtinResult.istradeitemanorderableunit.ToString() != "") list.isTradeItemAnOrderableUnit = gtinResult.istradeitemanorderableunit;
                    if (gtinResult.istradeitemadespatchunit != null && gtinResult.istradeitemadespatchunit.ToString() != "") list.isTradeItemADespatchUnit = gtinResult.istradeitemadespatchunit;
                    list.orderSizingFactor = gtinResult.ordersizingfactor;
                    if (gtinResult.orderquantitymultiple != null && gtinResult.orderquantitymultiple.ToString() != "") list.orderQuantityMultiple = gtinResult.orderquantitymultiple;
                    if (gtinResult.orderquantityminimum != null && gtinResult.orderquantityminimum.ToString() != "") list.orderQuantityMinimum = gtinResult.orderquantityminimum;
                    if (gtinResult.barcodecertified != null && gtinResult.barcodecertified.ToString() != "") list.barcodeCertified = gtinResult.barcodecertified;
                    if (gtinResult.dataqualitycertified != null && gtinResult.dataqualitycertified.ToString() != "") list.dataQualityCertified = gtinResult.dataqualitycertified;
                    if (gtinResult.dataqualitycertifiedagencycode != null && gtinResult.dataqualitycertifiedagencycode.ToString() != "") list.dataQualityCertifiedAgencyCode = gtinResult.dataqualitycertifiedagencycode;
                    if (gtinResult.allgdsnattributes != null && gtinResult.allgdsnattributes.ToString() != "") list.allGDSNAttributes = gtinResult.allgdsnattributes;
                    if (gtinResult.canalcomunicacaodados != null && gtinResult.canalcomunicacaodados.ToString() != "") list.CanalComunicacaoDados = gtinResult.canalcomunicacaodados;
                    if (gtinResult.propriodonoinformacao != null && gtinResult.propriodonoinformacao.ToString() != "") list.ProprioDonoInformacao = gtinResult.propriodonoinformacao;
                    if (gtinResult.validadodonodainformacao != null && gtinResult.validadodonodainformacao.ToString() != "") list.ValidadoDonoDaInformacao = gtinResult.validadodonodainformacao;
                    if (gtinResult.indicadorgdsn != null && gtinResult.indicadorgdsn.ToString() != "") list.IndicadorGDSN = gtinResult.indicadorgdsn;
                    if (gtinResult.codigolingua != null && gtinResult.codigolingua.ToString() != "") list.CodigoLingua = gtinResult.codigolingua;

                    list.CodigoAssociado = gtinResult.codigoassociado;
                    list.CPFCNPJ = gtinResult.cpfcnpj;
                    list.CAD = gtinResult.cad;
                    list.NomeAssociado = gtinResult.nomeassociado;

                    list.NomeTipoGtin = gtinResult.nometipogtin;
                    if (gtinResult.nomeusuariocriacao != null && gtinResult.nomeusuariocriacao.ToString() != "") list.NomeUsuarioCriacao = gtinResult.nomeusuariocriacao;
                    if (gtinResult.nomeusuarioalteracao != null && gtinResult.nomeusuarioalteracao.ToString() != "") list.NomeUsuarioAlteracao = gtinResult.nomeusuarioalteracao;
                    if (gtinResult.nomeusuarioexclusao != null && gtinResult.nomeusuarioexclusao.ToString() != "") list.NomeUsuarioExclusao = gtinResult.nomeusuarioexclusao;
                    if (gtinResult.nometradeitemcountryoforigin != null && gtinResult.nometradeitemcountryoforigin.ToString() != "") list.NometradeItemCountryOfOrigin = gtinResult.nometradeitemcountryoforigin;
                    if (gtinResult.nomestatusgtin != null && gtinResult.nomestatusgtin.ToString() != "") list.NomeStatusGTIN = gtinResult.nomestatusgtin;
                    if (gtinResult.nomecountrycode != null && gtinResult.nomecountrycode.ToString() != "") list.NomecountryCode = gtinResult.nomecountrycode;
                    if (gtinResult.nomelingua != null && gtinResult.nomelingua.ToString() != "") list.NomeLingua = gtinResult.nomelingua;
                    if (gtinResult.nometipoproduto != null && gtinResult.nometipoproduto.ToString() != "") list.NomeTipoProduto = gtinResult.nometipoproduto;
                    
                    list.AgenciasReguladoras = GetAgenciasProduto(list.CodigoProduto);                    
                    list.BrickTypeValues = GetBrickTypeValuesProduto(list.CodigoProduto);
                    list.ProdutosInferiores = GetProdutosInferiores(list.CodigoProduto);                    
                    list.ImagensProduto =   GetImagensProduto(list.CodigoProduto);
                    list.URLsProduto = GetURLsProduto(list.CodigoProduto);
                    list.ContatosAssociado = GetContatoAssociado(list.CodigoAssociado);


                    list = GetFilteredResultsV2(list, gtin);
                    
                    if (!(list == null))
                    {
                        ResponseHeader.numberOfHits = 1;
                    }
                    else
                        return null;

                   ResponseHeader.returnCode = 0;

                    return list;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (!EventLog.SourceExists(EVENT_SOURCE))
                        EventLog.CreateEventSource(EVENT_SOURCE, EVENT_LOG);

                    EventLog.WriteEntry(EVENT_SOURCE, "GS1.CNP.WEBSERVICE.GEPIRSERVICE - Erro ao Executar: " + ex.Message + " Stack: " + ex.StackTrace, EventLogEntryType.Error, 1);


                }
                catch (Exception)
                {



                }

                ResponseHeader.numberOfHits = 0;
                ResponseHeader.returnCode = 99;
                return null;
            }
        }


        private List<AgenciaReguladora> GetAgenciasProduto(long CodigoProduto)
        {
            string query = @"SELECT B.CODIGO, B.NOME, A.alternateItemIdentificationId 
                                FROM PRODUTOAGENCIAREGULADORA A
                                INNER JOIN AGENCIAREGULADORA B ON B.CODIGO = A.CODIGOAGENCIA
                                WHERE B.STATUS = 1
                                AND CODIGOPRODUTO = @CodigoProduto";
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.CodigoProduto = CodigoProduto;


                Func<IDataRecord, AgenciaReguladora> binder = r => new AgenciaReguladora()
                {
                    CodigoAgenciaReguladora = r.GetInt64(0),
                    NomeAgenciaReguladora = r.GetString(1),
                    alternateItemIdentificationId = r.GetString(2)
                };

                List<AgenciaReguladora> agencias = ((List<AgenciaReguladora>)db.Select(query, param, binder));

                return agencias;

            }
        }

        private List<BrickTypeValue> GetBrickTypeValuesProduto(long CodigoProduto)
        {
            string query = @"SELECT [Codigo]
                                  ,[CodigoProduto]
                                  ,[CodeBrick]
                                  ,[CodeClass]
                                  ,[CodeFamily]
                                  ,[CodeSegment]
                                  ,[Idioma]
                                  ,[CodeType]
                                  ,[CodeValue]
                              FROM [dbo].[ProdutoBrickTypeValue]
                            WHERE CODIGOPRODUTO = @CodigoProduto";
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.CodigoProduto = CodigoProduto;

                Func<IDataRecord, BrickTypeValue> binder = r => new BrickTypeValue()
                {
                    CodigoBrickTypeValue = r.GetInt64(0),
                    CodigoProduto  = r.GetInt64(1),
                    CodeBrick = r.GetString(2),
                    CodeClass = r.GetString(3),
                    CodeFamily = r.GetString(4),
                    CodeSegment = r.GetString(5),
                    Idioma = r.GetString(6),
                    CodeType = r.GetString(7),
                    CodeValue = r.GetString(8)
                };

                List<BrickTypeValue> brickTypeValue = ((List<BrickTypeValue>)db.Select(query, param, binder));

                return brickTypeValue;
            }
        }


        private List<ProdutoInferior> GetProdutosInferiores(long CodigoProduto)
        {
            string query = @"SELECT A.CODIGOPRODUTOINFERIOR, B.GLOBALTRADEITEMNUMBER, B.PRODUCTDESCRIPTION, A.QUANTIDADE 
                             FROM PRODUTOHIERARQUIA A
                             INNER JOIN PRODUTO B ON B.CODIGOPRODUTO = A.CODIGOPRODUTOINFERIOR
                              WHERE CODIGOPRODUTO = @CodigoProduto";
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.CodigoProduto = CodigoProduto;

                Func<IDataRecord, ProdutoInferior> binder = r => new ProdutoInferior()
                {
                    CodigoProdutoInferior = r.GetInt64(0),
                    globalTradeItemNumber = r.GetInt64(1),
                    productdescription = r.GetString(2),
                    Quantidade = r.GetInt32(3)
                };

                List<ProdutoInferior> produtosInferiores = ((List<ProdutoInferior>)db.Select(query, param, binder));

                return produtosInferiores;
            }
        }


        private List<ImagenProduto> GetImagensProduto(long CodigoProduto)
        {
            string query = @"SELECT Codigo, URL
                             FROM PRODUTOIMAGEM                             
                             WHERE STATUS = 1
                             AND CODIGOPRODUTO = @CodigoProduto";
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.CodigoProduto = CodigoProduto;

                Func<IDataRecord, ImagenProduto> binder = r => new ImagenProduto()
                {
                    CodigoImagem = r.GetInt64(0),
                    Url = r.GetString(1)
                };

                List<ImagenProduto> imagesProdutos = ((List<ImagenProduto>)db.Select(query, param, binder));

                return imagesProdutos;
            }
        }

        private List<URLProduto> GetURLsProduto(long CodigoProduto)
        {
            string query = @"SELECT A.Codigo, A.Nome, A.URL, A.CodigoTipoURL, B.Nome NomeTipoURL  
                             FROM PRODUTOURL a
                             INNER JOIN TIPOURL B ON B.CODIGO = A.CODIGOTIPOURL
                             WHERE A.STATUS = 1
                             AND CODIGOPRODUTO = @CodigoProduto";
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.CodigoProduto = CodigoProduto;

                Func<IDataRecord, URLProduto> binder = r => new URLProduto()
                {
                    CodigoURL = r.GetInt64(0),
                    NomeURL = r.GetString(1),
                    Url = r.GetString(2),
                    CodigoTipoURL = r.GetInt32(3),
                    NomeTipoURL = r.GetString(4)
                };

                List<URLProduto> URLS = ((List<URLProduto>)db.Select(query, param, binder));

                return URLS;
            }
        }

        private List<ContatoAssociado> GetContatoAssociado(long CodigoAssociado)
        {
            string query = @"SELECT A.CODIGO, A.NOME
                             FROM CONTATOASSOCIADO A
                             WHERE A.STATUS = 1
                             AND A.INDICADORPRINCIPAL = 1
                             AND A.CODIGOASSOCIADO = @CodigoAssociado";
            using (GS1.CNP.DAL.Database db = new GS1.CNP.DAL.Database("BD"))
            {
                db.Open();
                dynamic param = new ExpandoObject();
                param.CodigoAssociado = CodigoAssociado;

                Func<IDataRecord, ContatoAssociado> binder = r => new ContatoAssociado()
                {
                    CodigoContato= r.GetInt32(0),
                    NomeContato  = r.GetString(1)
                };

                List<ContatoAssociado> contatos = ((List<ContatoAssociado>)db.Select(query, param, binder));

                return contatos;
            }
        }
    }
}
