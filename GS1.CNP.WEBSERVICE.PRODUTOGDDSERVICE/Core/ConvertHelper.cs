﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Core.Attributes;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model;
using System.Web;
using GS1.CNP.BLL;
using GS1.CNP.BLL.Core;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Core
{
    public class ConvertHelper
    {
        public static dynamic ModelToDynamic(IModel model)
        {
            if (model != null)
            {
                Type tmodel = model.GetType();

                PropertyInfo[] properties = tmodel.GetProperties();

                if (properties != null && properties.Length > 0)
                {
                    dynamic returnObject = new ExpandoObject();

                    foreach (PropertyInfo prop in properties)
                    {
                        if (prop.PropertyType == typeof(string) || prop.PropertyType == typeof(int) || prop.PropertyType == typeof(long) ||
                            prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(bool?) ||
                            prop.PropertyType == typeof(int?) || prop.PropertyType == typeof(long?) || prop.PropertyType == typeof(decimal?) ||
                            prop.PropertyType == typeof(DateTime?) || prop.PropertyType == typeof(bool))
                        {
                            ColumnAttribute column = prop.GetCustomAttributes(typeof(ColumnAttribute), true).FirstOrDefault() as ColumnAttribute;
                            string columnName = (column != null ? column.ColumnName : prop.Name).ToLower();
                            object value = tmodel.GetProperty(prop.Name).GetValue(model, null);

                            if (value == null)
                                value = string.Empty;
                            else if (value is bool && !columnName.Equals("indicadorgdsn"))
                                value = ((bool)value ? 1 : 0);
                            else if (value is bool && columnName.Equals("indicadorgdsn"))
                                value = ((bool)value ? "sim" : "não");
                            else if (value is DateTime)
                                value = ((DateTime)value).ToString("dd/MM/yyyy");

                            ((IDictionary<string, object>)returnObject).Add(columnName, value);
                        }
                        else
                        {
                            ColumnAttribute column = prop.GetCustomAttributes(typeof(ColumnAttribute), true).FirstOrDefault() as ColumnAttribute;

                            if (prop.PropertyType.IsArray)
                            {
                                if (prop.Name.ToLower().Equals("gtininferiores"))
                                {
                                    GTINInferior[] modelArray = (GTINInferior[])tmodel.GetProperty(prop.Name).GetValue(model, null);

                                    if (modelArray != null && modelArray.Length > 0)
                                    {
                                        string gtininferior = string.Empty,
                                            quantidade = string.Empty;

                                        foreach (GTINInferior gtin in modelArray)
                                        {
                                            gtininferior += string.IsNullOrEmpty(gtininferior) ? gtin.CodigoProdutoInferior.ToString() : "," + gtin.CodigoProdutoInferior.ToString();
                                            quantidade += string.IsNullOrEmpty(quantidade) ? gtin.Quantidade.ToString() : "," + gtin.Quantidade.ToString();
                                        }
                                        if (!string.IsNullOrEmpty(gtininferior) && !string.IsNullOrEmpty(quantidade))
                                        {
                                            ((IDictionary<string, object>)returnObject).Add("gtininferior", gtininferior);
                                            ((IDictionary<string, object>)returnObject).Add("quantidadeinferior", quantidade);
                                        }
                                    }
                                }
                                else
                                {
                                    IModel[] modelArray = (IModel[])tmodel.GetProperty(prop.Name).GetValue(model, null);

                                    if (modelArray != null && modelArray.Length > 0)
                                    {
                                        for (int i = 0; i < modelArray.Length; i++)
                                        {
                                            var modelArr = ModelToDynamic(modelArray[i]);

                                            if (modelArr != null)
                                            {
                                                foreach (KeyValuePair<string, object> kv in (IDictionary<string, object>)modelArr)
                                                    ((IDictionary<string, object>)returnObject).Add(kv.Key + (i + 1).ToString(), kv.Value);
                                            }
                                        }
                                    }
                                }
                            }
                            else if (prop.PropertyType == typeof(IModel))
                            {
                                IModel m = (IModel)tmodel.GetProperty(prop.Name).GetValue(model, null);

                                ((IDictionary<string, object>)returnObject).Add((column != null ? column.ColumnName : prop.Name).ToLower(), ModelToDynamic(m));
                            }
                            else
                            {
                                object o = tmodel.GetProperty(prop.Name).GetValue(model, null);

                                ((IDictionary<string, object>)returnObject).Add((column != null ? column.ColumnName : prop.Name).ToLower(), o);
                            }
                        }
                    }

                    return returnObject;
                }
            }

            return null;
        }

        public static T DynamicToModel<T>(dynamic expando, int template)
        {
            if (expando != null && (expando as IDictionary<string, object>).Count > 0)
            {
                var model = Activator.CreateInstance<T>();
                var dictionary = expando as IDictionary<string, object>;
                var type = model.GetType();
                using (ImportarProdutosBO bo = new ImportarProdutosBO())
                {
                    dynamic dic = DynamicXml.Load(HttpContext.Current.Server.MapPath("/Dicionario.xml"));
                    List<dynamic> dicionario = bo.RecuperarMapeamentos(dic.mapa as List<DynamicXml>, template);
                    foreach (PropertyInfo prop in type.GetProperties())
                    {
                        Type elType = prop.PropertyType.GetElementType();
                        if (prop.PropertyType.IsArray && elType != null && elType.GetInterfaces().Length > 0 && elType.GetInterfaces()[0] == typeof(IModel))
                        {
                            if (elType.Name.ToLower().Equals("agenciareguladora"))
                            {
                                var lista = ConvertAgenciaReguladora(expando);
                                prop.SetValue(model, lista, null);
                            }
                            else if (elType.Name.ToLower().Equals("gtininferior"))
                            {
                                var lista = ConvertGTINInferiores(expando);
                                prop.SetValue(model, lista, null);
                            }
                            else if (elType.Name.ToLower().Equals("url"))
                            {
                                var lista = ConvertURL(expando);
                                prop.SetValue(model, lista, null);
                            }
                        }
                        else
                        {
                            dynamic col = (from dynamic d in dicionario
                                           where d.campo.ToLower().Equals(prop.Name.ToLower())
                                           select d).FirstOrDefault();
                            if (col != null)
                            {
                                if ((expando as IDictionary<string, object>).ContainsKey(col.nome))
                                {
                                    object value = (expando as IDictionary<string, object>)[col.nome];
                                    if (value == null || string.IsNullOrEmpty(value.ToString()))
                                        continue;
                                    else
                                    {
                                        bool canBeNull = !prop.PropertyType.IsValueType || (Nullable.GetUnderlyingType(prop.PropertyType) != null);
                                        Type[] genericArgs = prop.PropertyType.GetGenericArguments();
                                        if (canBeNull && genericArgs.Length > 0)
                                        {
                                            Type typeGeneric = genericArgs[0];
                                            if (typeGeneric == typeof(bool) && (value.Equals("sim") || value.Equals("não")))
                                                value = value.Equals("sim") ? true : false;
                                            
                                            prop.SetValue(model, Convert.ChangeType(value, typeGeneric), null);
                                        }
                                        else
                                            prop.SetValue(model, Convert.ChangeType(value, prop.PropertyType), null);
                                    }
                                }
                            }
                        }
                    }
                }
                return model;
            }
            return default(T);
        }

        private static URL[] ConvertURL(dynamic expando)
        {
            if (expando != null)
            {
                var lista = new List<URL>();
                int contador = 1;
                List<KeyValuePair<string, object>> its = null;
                while (contador > -1)
                {
                    its = (from KeyValuePair<string, object> kv in expando as IDictionary<string, object>
                           where kv.Key.ToLower().Contains("nomeurl" + contador.ToString()) ||
                                 kv.Key.ToLower().Contains("url" + contador.ToString()) ||
                                 kv.Key.ToLower().Contains("tipourl" + contador.ToString())
                           select kv).ToList<KeyValuePair<string, object>>();
                    if (its != null && its.Count > 0)
                    {
                        var ar = new URL();
                        foreach (KeyValuePair<string, object> it in its)
                        {
                            if (it.Key.ToLower().Contains("nomeurl"))
                                ar.Nome = it.Value.ToString();
                            else if (it.Key.ToLower().Contains("tipourl"))
                                ar.CodigoTipoURL = Convert.ToInt32(it.Value);
                            else if (it.Key.ToLower().Contains("url"))
                                ar.Url = it.Value.ToString();
                        }
                        lista.Add(ar);
                        contador++;
                    }
                    else
                        contador = -1;
                }
                return lista.ToArray();
            }
            return null;
        }

        private static GTINInferior[] ConvertGTINInferiores(dynamic expando)
        {
            if (expando != null)
            {
                var lista = new List<GTINInferior>();
                if (expando != null && (expando as IDictionary<string, object>).Count > 0)
                {
                    string[] gtininferior = null,
                        quantidade = null;
                    if ((expando as IDictionary<string, object>).ContainsKey("gtininferior"))
                        gtininferior = expando.gtininferior.ToString().Split(',');
                    if ((expando as IDictionary<string, object>).ContainsKey("quantidadeinferior"))
                        quantidade = expando.quantidadeinferior.ToString().Split(',');
                    if (gtininferior != null && gtininferior.Length > 0 && quantidade != null && quantidade.Length > 0)
                    {
                        for (int i = 0; i < gtininferior.Length; i++)
                        {
                            lista.Add(new GTINInferior()
                            {
                                CodigoProdutoInferior = Convert.ToInt64(gtininferior[i]),
                                Quantidade = Convert.ToInt32(quantidade[i])
                            });
                        }
                    }
                }
                return lista.ToArray();
            }
            return null;
        }

        private static AgenciaReguladora[] ConvertAgenciaReguladora(dynamic expando)
        {
            if (expando != null)
            {
                var lista = new List<AgenciaReguladora>();
                int contador = 1;
                List<KeyValuePair<string, object>> its = null;
                while (contador > -1)
                {
                    its = (from KeyValuePair<string, object> kv in expando as IDictionary<string, object>
                           where kv.Key.ToLower().Contains("agenciareguladora" + contador.ToString()) ||
                                 kv.Key.ToLower().Contains("identificacaoalternativa" + contador.ToString())
                           select kv).ToList<KeyValuePair<string, object>>();
                    if (its != null && its.Count > 0)
                    {
                        var ar = new AgenciaReguladora();
                        foreach (KeyValuePair<string, object> it in its)
                        {
                            if (it.Key.ToLower().Contains("agenciareguladora"))
                                ar.CodigoAgencia = Convert.ToInt32(it.Value);
                            else if (it.Key.ToLower().Contains("identificacaoalternativa"))
                                ar.alternateItemIdentificationId = it.Value.ToString();
                        }
                        lista.Add(ar);
                        contador++;
                    }
                    else
                        contador = -1;
                }
                return lista.ToArray();
            }
            return null;
        }
    }
}