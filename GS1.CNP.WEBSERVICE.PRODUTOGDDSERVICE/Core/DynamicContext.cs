﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Core
{
    [Serializable]
    public class DynamicContext : DynamicObject, ISerializable
    {
        private Dictionary<string, object> dynamicContext = new Dictionary<string, object>();

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return dynamicContext.TryGetValue(binder.Name, out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            dynamicContext.Add(binder.Name, value);
            return true;
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            foreach (KeyValuePair<string, object> kvp in dynamicContext)
                info.AddValue(kvp.Key, kvp.Value);
        }

        public DynamicContext() { }

        protected DynamicContext(SerializationInfo info, StreamingContext context)
        {
            foreach (SerializationEntry entry in info)
                dynamicContext.Add(entry.Name, entry.Value);
        }
    }
}