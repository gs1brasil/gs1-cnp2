﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Core
{
    public static class Extensions
    {
        public static int Count<T>(this IOrderedEnumerable<T> ordered)
        {
            Type type = ordered.GetType();
            var flags = BindingFlags.NonPublic | BindingFlags.Instance;
            var field = type.GetField("source", flags);
            var source = field.GetValue(ordered);
            if (source is ICollection<T>)
                return ((ICollection<T>)source).Count;

            return ordered.Count();
        }
    }
}