﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Core
{
    public class Util
    {
        //public List<string> ValidatingDeserializer<T>(T s)
        //{
           
        //    foreach (var pi in typeof(T).GetProperties())
        //    {
        //        var xmlnull = pi.GetCustomAttributes(typeof(XmlElementAttribute),false);
        //        if (xmlnull != null)
        //        {
        //            var xmlnullInst = (XmlElementAttribute)xmlnull;
        //            if (!xmlnullInst.IsNullable && pi.GetValue(deserialize) == null)
        //            {
        //                throw new Exception(String.Format("{0} is null", pi.Name));
        //            }
        //        }
        //    }
        //    return deserialize;
        //}

        public static bool Serialize<T>(T value, ref string serializeXml)
        {
            if (value == null)
            {
                return false;
            }
            try
            {
                XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
                StringWriter stringWriter = new StringWriter();
                XmlWriter writer = XmlWriter.Create(stringWriter);

                xmlserializer.Serialize(writer, value);

                serializeXml = stringWriter.ToString();

                writer.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}