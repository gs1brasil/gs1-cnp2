﻿using System;
using System.Web;
using System.Reflection;
using GS1.CNP.BLL.Core;
using System.IO;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(HttpContext.Current.Server.MapPath("log4net.config")));

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            if (exception != null)
            {
                if (exception is TargetInvocationException && exception.InnerException != null)
                    exception = exception.InnerException;

                Log.WriteErrorWS(exception.Message, exception);                
            }

        }

        void Application_EndRequest(Object sender, EventArgs e)
        {
            //Log.WriteInfoWS();
        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
