﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.AlimentosBebidas
{
    [Serializable]
    public class Allergen
    {
        public string allergenTypeCode { get; set; }
        public string levelOfContainmentCode { get; set; }
    }
}