﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.AlimentosBebidas
{
    public class AllergenInformationModule
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        public AllergenRelatedInformation allergenRelatedInformation { get; set; }
    }
}