﻿using System.Collections.Generic;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.AlimentosBebidas
{
    public class AllergenRelatedInformation
    {
        public List<Allergen> allergen { get; set; }
    }
}