﻿namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.AlimentosBebidas.NutritionalInformation
{
    public class CompoundStringAttributeValuePairType
    {
        public string attributeName { get; set; }
        public string attributeCode { get; set; }
        public string codeListNameCode { get; set; }
        public string codeListVersion { get; set; }
        public string value { get; set; }
    }
}
