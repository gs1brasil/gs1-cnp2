﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class FoodBeverageComposition
    {
        public string foodBeverageCompositionCode { get; set; }
        public string foodBeverageCompositionDatabaseCode { get; set; }
        public string foodBeverageCompositionDescription { get; set; }
    }
}