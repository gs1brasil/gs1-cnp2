﻿using System;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.AlimentosBebidas.NutritionalInformation;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class GS1_AttributeValuePairList
    {
        public CompoundStringAttributeValuePairType[] compoundStringAVP { get; set; }
        public StringAttributeValuePairType[] stringAVP { get; set; }
    }
}