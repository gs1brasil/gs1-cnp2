﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class NutrientDetail
    {
        public double dailyValueIntakePercent { get; set; }
        public string measurementPrecisionCode { get; set; }
        public string nutrientTypeCode { get; set; }
        public string quantityContained { get; set; }
        //publicS1_AttributeValuePairList avpList { get; set; }
    }
}