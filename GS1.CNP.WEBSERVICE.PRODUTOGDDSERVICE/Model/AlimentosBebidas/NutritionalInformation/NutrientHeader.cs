﻿using System;
using System.Collections.Generic;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class NutrientHeader
    {
        public string dailyValueIntakeReference { get; set; }
        //public Measurement nutrientBasisQuantity { get; set; }
        //public string nutrientBasisQuantityTypeCode { get; set; }
        //public string preparationStateCode { get; set; }
        public string servingSizeDescription { get; set; }
        //public GS1_AttributeValuePairList avpList { get; set; }
        public List<NutrientDetail> nutrientDetail { get; set; }
    }
}