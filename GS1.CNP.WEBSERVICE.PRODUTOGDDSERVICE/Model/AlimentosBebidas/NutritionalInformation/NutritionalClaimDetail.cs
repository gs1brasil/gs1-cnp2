﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class NutritionalClaimDetail
    {
        public string nutritionalClaimNutrientElementCode { get; set; }
        public string nutritionalClaimTypeCode { get; set; }
    }
}