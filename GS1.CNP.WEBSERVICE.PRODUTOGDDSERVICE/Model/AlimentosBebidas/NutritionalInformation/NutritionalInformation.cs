﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class NutritionalInformation
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        public string nutritionalClaim { get; set; }
        public FoodBeverageComposition foodComposition { get; set; }
        public NutrientDetail nutrientDetail { get; set; }
        public NutrientHeader nutrientHeader { get; set; }
        public NutritionalClaimDetail nutritionalClaimDetail { get; set; }
        public GS1_AttributeValuePairList avpList { get; set; }
    }
}