﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.AlimentosBebidas
{
    public class NutritionalInformationModule
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        public NutrientHeader nutrientHeader { get; set; }
        //NutritionalClaimDetail nutritionalClaimDetail { get; set; }
    }
}