﻿namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.AlimentosBebidas.NutritionalInformation
{
    public class StringAttributeValuePairType
    {
        public string attributeName { get; set; }
        public string value { get; set; }
    }
}
