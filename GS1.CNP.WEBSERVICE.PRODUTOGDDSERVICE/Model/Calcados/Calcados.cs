﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class CalcadosModel
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        public List<ColourModel> Colour { get; set; }
        public List<SizeModel> Size { get; set; }
        public string predominantMaterial { get; set; }
    }
}