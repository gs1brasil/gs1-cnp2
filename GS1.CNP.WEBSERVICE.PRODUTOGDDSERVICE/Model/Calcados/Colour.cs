﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class ColourModel
    {
        public string colourCode { get; set; }
        public string colourDescription { get; set; }
        public string colourCodeAgency { get; set; }
    }
}