﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class PartyInRole 
    {
        public string Nome { get; set; }
        public string GLN { get; set; }
        public string CNPJ { get; set; }
    }
}