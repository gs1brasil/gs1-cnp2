﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class SizeModel
    {
        public string descriptiveSize { get; set; }
        public string sizeCode { get; set; }
        public string sizeSystemCode { get; set; }
    }
}