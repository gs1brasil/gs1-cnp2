﻿namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    public enum taxCategoryCode
    {
        APPLICABLE,
        DOMESTIC_REVERSE_CHARGE,
        EXEMPT,
        FREE_EXPORT_ITEM,
        HIGH,
        LOW,
        MEDIUM,
        MIXED,
        NOT_APPLICABLE,
        PREPAID,
        REDUCTION_IN_BASE,
        REDUCTION_IN_TAX_RATE,
        SERVICES_OUTSIDE_SCOPE_OF_TAX,
        STANDARD,
        VALUE_ADDED,
        VALUE_ADDED_MARGIN,
        VALUE_ADDED_TAX_NOT_NOW_DUE_FOR_PAYMENT,
        VAT_REVERSE_CHARGE,
        ZERO
    }
}