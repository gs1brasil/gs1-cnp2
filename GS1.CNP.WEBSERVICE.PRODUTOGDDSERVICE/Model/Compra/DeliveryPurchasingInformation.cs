﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Compra
{
    public class DeliveryPurchasingInformation
    {
        public string orderingUnitOfMeasure { get; set; }
        public int? orderQuantityMinimum { get; set; }
        public int? orderQuantityMultiple { get; set; }
        public DateTime? startAvailabilityDateTime { get; set; }
        public DateTime? endAvailabilityDateTime { get; set; }
    }
}