﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Compra
{
    public class DeliveryPurchasingInformationModule
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public DeliveryPurchasingInformation deliveryPurchasingInformation { get; set; }

        //[XmlElementAttribute(IsNullable = true)]
        //public GS1_AttributeValuePairList avpList { get; set; }

        //[System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        //public bool isTradeItemAModel { get; set; }

        public string packagingTypeDescription { get; set; }
    }
}