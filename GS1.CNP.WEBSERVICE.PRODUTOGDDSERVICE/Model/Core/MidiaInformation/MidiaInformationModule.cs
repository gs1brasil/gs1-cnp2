﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    [System.Xml.Serialization.XmlRootAttribute("MidiaInformation", IsNullable = true)]
    public class MidiaInformationModule
    {
        [XmlElementAttribute(IsNullable = false)]
        public string referencedFileTypeCode { get; set; } //Tipo de URL

        [XmlElementAttribute(IsNullable = false)]
        public string fileFormatName { get; set; }

        [XmlElementAttribute(IsNullable = false)]
        public string uniformResourceIdentifier { get; set; }
    }
}