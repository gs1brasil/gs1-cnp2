﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Linq;


namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class ProdutoCoreModel : IModel
    {
        [XmlElementAttribute(IsNullable = false)]
        public TradeItemModule TradeItem { get; set; }

        [XmlElementAttribute(IsNullable = false)]
        public TradeItemClassificationModule TradeItemClassification { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public List<MidiaInformationModule> MidiaInformation { get; set; }

        [JsonIgnore]
        public List<string[]> Errors { get; set; }

        public bool Validate(int tipo, List<dynamic> bricks, List<dynamic> prefixos)
        {
            if (tipo == 1)
            {
                if (this.TradeItem == null)
                {
                    this.Errors.Add(new string[2] { "TradeItem", "TradeItem não informado" });
                }

                if (this.TradeItem.CodigoTipoGtin <= 0 || this.TradeItem.CodigoTipoGtin > 4)
                {
                    this.Errors.Add(new string[2] { "Tipo GTIN", "Tipo GTIN não inválido" });
                }

                if ((this.TradeItem.CodigoTipoGtin == 4) && this.TradeItem.GTIN == null || this.TradeItem.GTIN == string.Empty)
                {
                    this.Errors.Add(new string[2] { "GTIN", "GTIN não informado" });
                }
                else
                {
                    if (this.TradeItem.GTIN != null && this.TradeItem.GTIN != string.Empty)
                    {
                        if (this.TradeItem.CodigoTipoGtin > 0 && this.TradeItem.CodigoTipoGtin <= 4)
                        {
                            if (this.TradeItem.CodigoTipoGtin == 1)
                                this.Errors.Add(new string[2] { "GTIN-8", "Não é permitido o cadastro de GTIN-8" });

                            if (this.TradeItem.CodigoTipoGtin == 2 && this.TradeItem.GTIN.Length != 12)
                                this.Errors.Add(new string[2] { "GTIN-12", "GTIN-12 com formato incorreto" });

                            if (this.TradeItem.CodigoTipoGtin == 3 && this.TradeItem.GTIN.Length != 13)
                                this.Errors.Add(new string[2] { "GTIN-13", "GTIN-13 com formato incorreto" });

                            if (this.TradeItem.CodigoTipoGtin == 4 && this.TradeItem.GTIN.Length != 14)
                                this.Errors.Add(new string[2] { "GTIN-14", "GTIN-14 com formato incorreto" });

                            if (this.TradeItem.CodigoTipoGtin == 4 && (this.TradeItem.nextLowerLevel == null || this.TradeItem.nextLowerLevel.childTradeItem == null || this.TradeItem.nextLowerLevel.childTradeItem.Count() <= 0))
                                this.Errors.Add(new string[2] { "GTIN-14 - nextLowerLevel", "GTIN-14 com hierarquia inferior inválida" });

                            if (this.TradeItem.CodigoTipoGtin == 4 && (this.TradeItem.nextLowerLevel != null && this.TradeItem.nextLowerLevel.childTradeItem != null && this.TradeItem.nextLowerLevel.childTradeItem.Count() > 1))
                                this.Errors.Add(new string[2] { "GTIN-14 - nextLowerLevel", "GTIN-14 com hierarquia inferior inválida. Somente um GTIN inferior é permitido para este tipo de GTIN" });

                        }

                        bool flPrefixo = false;
                        foreach (var item in prefixos)
                        {
                            if (this.TradeItem.GTIN.Contains(item.numeroprefixo.ToString()))
                                flPrefixo = true;
                        }

                        if (!flPrefixo)
                            this.Errors.Add(new string[2] { "Prefixo", "Nenhum prefixo encontrado para o GTIN informado" });
                    }

                }

                if (this.TradeItem.tradeItemDescription == null || this.TradeItem.tradeItemDescription == string.Empty)
                {
                    this.Errors.Add(new string[2] { "tradeItemDescription", "Descrição não informada" });
                }

                if (this.TradeItemClassification == null || this.TradeItemClassification.tradeitemclassification == null || this.TradeItemClassification.tradeitemclassification.gpcCategoryCode == null || this.TradeItemClassification.tradeitemclassification.gpcCategoryCode == string.Empty)
                {
                    this.Errors.Add(new string[2] { "TradeItemClassification.gpcCategoryCode", "GPC não informado" });
                }
                else
                {
                    if (bricks != null)
                    {
                        IEnumerable<dynamic> produtosGPCQuery =
                           from brick in bricks
                           where brick.codebrick == this.TradeItemClassification.tradeitemclassification.gpcCategoryCode
                           select brick.codebrick;

                        if (produtosGPCQuery.Count() <= 0)
                            this.Errors.Add(new string[2] { "TradeItemClassification.gpcCategoryCode", "CodeBrick inválido" });
                    }
                    else
                    {
                        this.Errors.Add(new string[2] { "TradeItemClassification.gpcCategoryCode", "CodeBrick inválido" });
                    }
                }

            }
            else
            {
                if (this.TradeItem == null)
                {
                    this.Errors.Add(new string[2] { "TradeItem", "TradeItem não informado" });
                }              

                if (this.TradeItem.GTIN == null || this.TradeItem.GTIN == string.Empty)
                {
                    this.Errors.Add(new string[2] { "GTIN", "GTIN não informado" });
                }                               

                if (this.TradeItemClassification != null && this.TradeItemClassification.tradeitemclassification != null && this.TradeItemClassification.tradeitemclassification.gpcCategoryCode != null && this.TradeItemClassification.tradeitemclassification.gpcCategoryCode != string.Empty)
                {                
                    if (bricks != null)
                    {
                        IEnumerable<dynamic> produtosGPCQuery =
                           from brick in bricks
                           where brick.codebrick == this.TradeItemClassification.tradeitemclassification.gpcCategoryCode
                           select brick.codebrick;

                        if (produtosGPCQuery.Count() <= 0)
                            this.Errors.Add(new string[2] { "TradeItemClassification.gpcCategoryCode", "CodeBrick inválido" });
                    }
                    else
                    {
                        this.Errors.Add(new string[2] { "TradeItemClassification.gpcCategoryCode", "CodeBrick inválido" });
                    }
                }
            }
          
            return !(this.Errors.Count > 0);
        }
        
    }
}