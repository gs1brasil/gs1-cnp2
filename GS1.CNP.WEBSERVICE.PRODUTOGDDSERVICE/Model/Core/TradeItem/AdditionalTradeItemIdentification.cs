﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class AdditionalTradeItemIdentification
    {
        public string additionalTradeItemIdentificationTypeCode { get; set; } //ModelNumber, InternalCode, ANVISA, INMETRO, MAPA
        public string additionalTradeItemIdentificationValue { get; set; }
    }
}