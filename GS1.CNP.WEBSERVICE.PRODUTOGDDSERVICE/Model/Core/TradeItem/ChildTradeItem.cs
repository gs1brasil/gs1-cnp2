﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class ChildTradeItem
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        public string tradeItemDescription { get; set; }

        [System.Xml.Serialization.XmlElementAttribute(DataType = "nonNegativeInteger")]
        public string quantity { get; set; } //quantityOfNextLowerLevelTradeItem
    }
}