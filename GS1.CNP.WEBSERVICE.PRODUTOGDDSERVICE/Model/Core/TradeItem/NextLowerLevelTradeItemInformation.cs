﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class NextLowerLevelTradeItemInformation
    {
        [System.Xml.Serialization.XmlElementAttribute(DataType = "nonNegativeInteger")]
        public string quantityOfChildren { get; set; }

        public ChildTradeItem[] childTradeItem { get; set; }
    }
}