﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class PartyInformation
    {
        [XmlElementAttribute(IsNullable = true)]
        public string GLN { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string PartyName { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string PartyRole { get; set; }
    }
}