﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class ProdutoAgenciaReguladora
    {
        public int regulationTypeCode { get; set; } //produtoagenciareguladora.AlternateItemIdentificationId
        public string regulatoryAgency { get; set; } //produtoagenciareguladora.codigoagencia
    }
}