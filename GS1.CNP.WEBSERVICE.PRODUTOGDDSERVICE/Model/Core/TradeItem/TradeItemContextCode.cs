﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class TradeItemContextCode
    {
        public string contextIdentification { get; set; }
    }
}