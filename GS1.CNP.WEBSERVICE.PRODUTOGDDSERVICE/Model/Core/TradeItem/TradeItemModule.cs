﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class TradeItemModule
    {
        public TradeItemContextCode context { get; set; }

        [System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        public bool isTradeItemABaseUnit { get; set; }

        [System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        public bool isTradeItemAConsumerUnit { get; set; }

        [System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        public bool isTradeItemADespatchUnit { get; set; }

        [System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        public bool isTradeItemAnInvoiceUnit { get; set; }

        [System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        public bool isTradeItemAnOrderableUnit { get; set; }

        [System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        public bool isTradeItemAService { get; set; }

        [System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        public bool isTradeItemNonphysical { get; set; }

        [System.ComponentModel.DefaultValueAttribute(typeof(System.Boolean), "false")]
        public bool isTradeItemRecalled { get; set; }

        [XmlElementAttribute(IsNullable = false)]
        public int CodigoTipoGtin { get; set; }

        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        public List<PartyInformation> partyInformation { get; set; }

        [XmlElementAttribute(IsNullable = false)]
        public string tradeItemDescription { get; set; }

        public string descriptionShort { get; set; }
        public DateTime? startAvailabilityDatetime { get; set; }
        public string Language { get; set; }
        public int? countryCode { get; set; }
        public int? countryOfOrigin { get; set; }
        public string estado { get; set; }

        public List<AdditionalTradeItemIdentification> additionalTradeItemIdentification { get; set; }

        public NextLowerLevelTradeItemInformation nextLowerLevel { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string brandName { get; set; }

        public string additionalTradeItemDescription { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string StatusGTIN { get; set; }
    }
}