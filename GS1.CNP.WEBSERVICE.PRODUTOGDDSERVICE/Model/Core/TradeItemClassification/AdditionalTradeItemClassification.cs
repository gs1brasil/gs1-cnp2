﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class AdditionalTradeItemClassification
    {
        public string AdditionalTradeItemClassificationValue { get; set; }
        public string additionalTradeItemClassificationCodeDescription { get; set; }
        public string additionalTradeItemClassificationCodeValue { get; set; }
    }
}