﻿using System;
using System.Collections.Generic;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class TradeItemClassification
    {
        public string gpcCategoryCode { get; set; } //brick
        public List<AdditionalTradeItemClassification> additionalTradeItemClassification { get; set; }
    }
}