﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class TradeItemClassificationModule
    {
        public TradeItemClassification tradeitemclassification { get; set; }
    }
}