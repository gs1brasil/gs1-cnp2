﻿namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    public class Error
    {
        public long errorCode { get; set; }
        public string errorMessage { get; set; }
    }
}