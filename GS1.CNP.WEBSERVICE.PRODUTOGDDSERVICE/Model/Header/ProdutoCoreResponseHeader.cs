﻿using System.Web.Services.Protocols;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Header
{
    public class ProdutoCoreResponseHeader : SoapHeader
    {
        public int numberOfHits { get; set; }
        public int returnCode { get; set; }
        //public List<Error> errors { get; set; }
    }
}