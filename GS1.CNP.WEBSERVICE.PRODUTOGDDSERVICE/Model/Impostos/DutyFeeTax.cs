﻿namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Impostos
{
    public class DutyFeeTax
    {
        public currencyCode dutyFeeTaxAmount { get; set; }
        public string dutyFeeTaxBasis { get; set; }
        public taxCategoryCode dutyFeeTaxCategoryCode { get; set; }
        //ISO 3166-2:2013
        public string dutyFeeTaxCountrySubdivisionCode { get; set; }
        public partyRoleCode dutyFeeTaxExemptPartyRoleCode { get; set; }
        public decimal dutyFeeTaxRate { get; set; }
        public string dutyFeeTaxReductionCriteriaDescription { get; set; }

    }    
}