﻿using System.Collections.Generic;


namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Impostos
{    
    public class DutyFeeTaxInformation
    {        
        public string dutyFeeTaxTypeCode { get; set; }
        public string dutyFeeTaxTypeDescription { get; set; }
        public List<DutyFeeTax> dutyFeeTax { get; set; }        
    }   
}