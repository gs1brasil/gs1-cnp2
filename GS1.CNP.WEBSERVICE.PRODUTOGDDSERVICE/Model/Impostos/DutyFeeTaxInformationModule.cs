﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Impostos
{
    public class DutyFeeTaxInformationModule
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        public List<DutyFeeTaxInformation> dutyFeeTaxInformation { get; set; }
    }
}