﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class CatalogueItemNotificationModule
    {
        [XmlElementAttribute(IsNullable = true)]
        public TradeItem tradeItem { get; set; }
    }
}