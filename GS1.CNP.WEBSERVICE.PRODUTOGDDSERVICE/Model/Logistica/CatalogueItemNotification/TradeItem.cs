﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class TradeItem
    {
        public string tradeItemUnitDescriptorCode { get; set; }
    }
}