﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class DangerousHazardousLabel
    {
        [XmlElementAttribute(IsNullable = true)]
        public string dangerousHazardousLabelNumber { get; set; }

        //[System.Xml.Serialization.XmlElementAttribute(DataType = "nonNegativeInteger")]
        public string dangerousHazardousLabelSequenceNumber { get; set; }
    }
}