﻿namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    public class DangerousSubstanceInformation
    {
        public DangerousSubstanceProperties dangerousSubstanceProperties { get; set; }
    }
}