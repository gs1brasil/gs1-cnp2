﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class DangerousSubstanceInformationModule
    {        
        public DangerousSubstanceInformation dangerousSubstanceInformation { get; set; }
    }
}