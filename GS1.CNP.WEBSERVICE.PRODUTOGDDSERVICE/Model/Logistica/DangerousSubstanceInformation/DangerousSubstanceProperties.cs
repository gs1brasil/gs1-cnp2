﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    public class DangerousSubstanceProperties
    {
        [XmlElementAttribute(IsNullable = true)]
        public string isDangerousSubstance { get; set; }
        //public string dangerousSubstanceName { get; set; }
    }
}