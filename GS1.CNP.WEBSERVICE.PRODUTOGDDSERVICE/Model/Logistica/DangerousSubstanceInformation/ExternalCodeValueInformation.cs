﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class ExternalCodeValueInformation
    {
        [XmlElementAttribute(IsNullable = true)]
        public string externalAgencyName { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string externalCodeListName { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string externalCodeListVersion { get; set; }
    }
}