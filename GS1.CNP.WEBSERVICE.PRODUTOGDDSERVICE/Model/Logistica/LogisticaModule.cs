﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    public class LogisticaModule
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public CatalogueItemNotificationModule catalogueItemNotificationModule { get; set; }
        
        [XmlElementAttribute(IsNullable = true)]
        public TradeItemHierarchyModule tradeItemHierarchyModule { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public TradeItemHandlingModule tradeItemHandlingModule { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public TradeItemMeasurementsModule tradeItemMeasurementsModule { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public TradeItemTemperatureInformationModule tradeItemTemperatureInformationModule { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public DangerousSubstanceInformationModule dangerousSubstanceInformationModule { get; set; }
    }
}