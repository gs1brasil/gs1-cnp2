﻿using System.Xml.Serialization;


namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    public class TradeItemHandlingInformation
    {
        [XmlElementAttribute(IsNullable = true)]
        public TradeItemStacking tradeItemStacking { get; set; }
    }
}