﻿using System.Xml.Serialization;



namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    public class TradeItemHandlingModule
    {
        [XmlElementAttribute(IsNullable = true)]
        public TradeItemHandlingInformation tradeItemHandlingInformation { get; set; }
    }
}