﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class TradeItemStacking
    {
        [XmlElementAttribute(IsNullable = true)]
        public string stackingFactor { get; set; }
    }
}