﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class TradeItemHierarchy
    {
        [XmlElementAttribute(IsNullable = true)]
        public string totalQuantityOfNextLowerLevelTradeItem { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string quantityOfCompleteLayersContainedInATradeItem { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string quantityOfLayersPerPallet { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string quantityOfTradeItemsContainedInACompleteLayer { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string quantityOfTradeItemsPerPalletLayer { get; set; }

        //[XmlElementAttribute(IsNullable = true)]
        //public GS1_AttributeValuePairList avpList { get; set; }
    }
}