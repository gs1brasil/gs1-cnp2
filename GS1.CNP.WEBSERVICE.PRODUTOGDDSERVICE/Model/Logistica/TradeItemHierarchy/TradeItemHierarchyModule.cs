﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class TradeItemHierarchyModule
    {
        [XmlElementAttribute(IsNullable = true)]
        public TradeItemHierarchy tradeItemHierarchy { get; set; }
    }
}