﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    public class Measurement
    {
        [XmlElementAttribute(IsNullable = true)]
        public decimal? measurement { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string measurementUnitCode { get; set; }
    }
}