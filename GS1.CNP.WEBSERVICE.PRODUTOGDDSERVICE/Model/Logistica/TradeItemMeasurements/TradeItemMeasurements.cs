﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class TradeItemMeasurements
    {
        [XmlElementAttribute(IsNullable = true)]
        public Measurement depth { get; set; }
             
        [XmlElementAttribute(IsNullable = true)]
        public Measurement height { get; set; }
               
        [XmlElementAttribute(IsNullable = true)]
        public Measurement netContent { get; set; }
      
        [XmlElementAttribute(IsNullable = true)]
        public Measurement width { get; set; }
      
        [XmlElementAttribute(IsNullable = true)]
        public Measurement grossWeight { get; set; }
      
        [XmlElementAttribute(IsNullable = true)]
        public Measurement netWeight { get; set; }
      
    }
}