﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class TradeItemMeasurementsModule
    {
        [XmlElementAttribute(IsNullable = true)]
        public TradeItemMeasurements tradeItemMeasurements { get; set; }        
    }
}