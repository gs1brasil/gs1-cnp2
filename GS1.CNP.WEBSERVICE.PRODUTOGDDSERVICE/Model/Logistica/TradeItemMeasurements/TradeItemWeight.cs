﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class TradeItemWeight
    {
        [XmlElementAttribute(IsNullable = true)]
        public string grossWeight { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public string netWeight { get; set; }
    }
}