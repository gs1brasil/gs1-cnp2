﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    public class TemperatureMeasurement
    {
        [XmlElementAttribute(IsNullable = true)]
        public decimal? temperatureMeasurement { get; set;}
        
        [XmlElementAttribute(IsNullable = true)]
        public string temperatureMeasurementUnitCode { get; set; }
    }
}