﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    [Serializable]
    public class TradeItemTemperatureInformation
    {
        [XmlElementAttribute(IsNullable = true)]
        public TemperatureMeasurement maximumTemperature { get; set; }

        [XmlElementAttribute(IsNullable = true)]
        public TemperatureMeasurement minimumTemperature { get; set; }
       
    }
}