﻿using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica
{
    public class TradeItemTemperatureInformationModule
    {
        //List<TradeItemTemperatureInformation> tradeItemTemperatureInformation { get; set; }
        [XmlElementAttribute(IsNullable = true)]
        public TradeItemTemperatureInformation tradeItemTemperatureInformation { get; set; }
    }
}