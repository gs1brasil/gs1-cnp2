﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class Retorno
    {
        public int code { get; set; }
        public string message { get; set; }
        public object data { get; set; }

        public Retorno() { }

        public Retorno(int code, string message, object data = null)
        {
            this.code = code;
            this.message = message;
            this.data = data;
        }
    }
}
