﻿using System;
using System.Xml.Serialization;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    [Serializable]
    public class TradeItemLifespanModule
    {
        [XmlElementAttribute(IsNullable = false)]
        public string GTIN { get; set; }

        //[System.Xml.Serialization.XmlElementAttribute(DataType = "nonNegativeInteger")]
        public int? minimumTradeItemLifespanFromTimeOfProduction { get; set; }
    }
}