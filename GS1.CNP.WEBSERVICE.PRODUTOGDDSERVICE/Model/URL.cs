﻿using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Core.Attributes;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model
{
    public class URL : IModel
    {
        [Column("nomeurl")]
        public string Nome { get; set; }
        [Column("url")]
        public string Url { get; set; }
        [Column("tipourl")]
        public int CodigoTipoURL { get; set; }
    }
}
