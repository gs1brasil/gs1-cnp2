﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Compra;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Impostos;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Header;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.Logistica;
using GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE.Model.AlimentosBebidas;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL;
using System.Dynamic;
using System.Configuration;
using System.Web.Services.Protocols;
using Newtonsoft.Json;

namespace GS1.CNP.WEBSERVICE.PRODUTOGDDSERVICE
{
    /// <summary>
    /// Summary description for ProdutoCore
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProdutoCore : System.Web.Services.WebService
    {
        public ProdutoCoreResponseHeader ResponseHeader { get; set; }
  
        #region Core
        [WebMethod(EnableSession = false, Description = "<b>Método de cadastro de produtos</b><br/>" +
            "<b><i>produtos</i></b>: Lista de produtos<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<ProdutoCoreModel> InsertCNPCore(List<ProdutoCoreModel> produtos, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();               

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    return null;
                }                                             

                List<dynamic> bricks = GetCNPBrick(produtos);

                ProdutoBO produtoBO = new ProdutoBO();
                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido
                        return null;
                    }
                }
                else 
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido
                    return null;
                }

                if (produtos == null && produtos.Count <= 0)                                
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)
                    return null;
                }


                List<dynamic> prefixos = produtoBO.GetCNPPrefixoAssociado(GLN);                
                if (prefixos.Count() == 0)
                {
                    ResponseHeader.returnCode = -4; // Prefixos não encontrados.
                    return null;
                }

                LinguaBO linguaBo = new LinguaBO();
                string listaLinguas = linguaBo.BuscarLinguagensAtivas(null);
                List<dynamic> linguas = ((Newtonsoft.Json.Linq.JArray)JsonConvert.DeserializeObject(listaLinguas)).ToObject<List<dynamic>>();

                long codigousuario;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CodigoUsuario"]))
                {
                    codigousuario = Convert.ToInt64(ConfigurationManager.AppSettings["CodigoUsuario"]);
                }
                else 
                {
                    ResponseHeader.returnCode = -5; // Usuário não configurado.
                    return null;
                }

                foreach (ProdutoCoreModel produto in produtos)
                {
                    
                    if(produto.Errors.Count() == 0)
                    {
                        dynamic tradeitem = new ExpandoObject();
                        List<dynamic> additionaltradeitemidentificationList = new List<dynamic>();
                        List<dynamic> nextLowerLevelList = new List<dynamic>();
                        MontaObjetoCore(GLN,bricks,prefixos,codigousuario, ref produtos,ref tradeitem, ref additionaltradeitemidentificationList, ref nextLowerLevelList,1);
                        if (produto.Errors.Count() == 0)
                        {
                            string resultado = produtoBO.InsertCNPCore(tradeitem, additionaltradeitemidentificationList, nextLowerLevelList);
                            if (resultado != string.Empty)
                            {
                                produto.Errors.Add(new string[2] { "ProdutoCoreModel", resultado });
                            }
                        }            
                    }                    
                }

                ResponseHeader.numberOfHits = (from produto in produtos
                                               where produto.Errors.Count() == 0
                                               select produto).Count();

                return produtos;

            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
                return null;
            }
        }
        
        [WebMethod(EnableSession = false, Description = "<b>Método de atualização de produtos</b><br/>" +            
            "<b><i>produtos</i></b>: Lista de produtos<br/>" + 
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<ProdutoCoreModel> UpdateCNPCore(List<ProdutoCoreModel> produtos, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    return null;
                }

                if (produtos == null && produtos.Count <= 0)
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)
                    return null;
                }

                List<dynamic> bricks = GetCNPBrick(produtos);

                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido
                    return null;
                }

                List<dynamic> prefixos = produtoBO.GetCNPPrefixoAssociado(GLN);
                if (prefixos.Count() == 0)
                {
                    ResponseHeader.returnCode = -4; // Prefixos não encontrados.
                    return null;
                }

                LinguaBO linguaBo = new LinguaBO();
                string listaLinguas = linguaBo.BuscarLinguagensAtivas(null);
                List<dynamic> linguas = ((Newtonsoft.Json.Linq.JArray)JsonConvert.DeserializeObject(listaLinguas)).ToObject<List<dynamic>>();

                long codigousuario;
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CodigoUsuario"]))
                {
                    codigousuario = Convert.ToInt64(ConfigurationManager.AppSettings["CodigoUsuario"]);
                }
                else
                {
                    ResponseHeader.returnCode = -5; // Usuário não configurado.
                    return null;
                }

                foreach (ProdutoCoreModel produto in produtos)
                {
                    dynamic tradeitem = new ExpandoObject();
                    List<dynamic> additionaltradeitemidentificationList = new List<dynamic>();
                    List<dynamic> nextLowerLevelList = new List<dynamic>();
                    MontaObjetoCore(GLN, bricks, prefixos, codigousuario, ref produtos, ref tradeitem, ref additionaltradeitemidentificationList, ref nextLowerLevelList, 2);
                    
                    if (nextLowerLevelList.Count() > 0)
                    {
                        produto.Errors.Add(new string[2] { "nextLowerLevel", "Não é permitida a alteração do GTIN inferior" });
                    }

                    if (produto.Errors.Count() == 0)
                    {                       
                        string resultado = produtoBO.UpdateCNPCore(GLN, ref tradeitem, additionaltradeitemidentificationList, nextLowerLevelList);

                        if (((IDictionary<String, object>)tradeitem).ContainsKey("codigostatusgtin"))
                            produto.TradeItem.StatusGTIN = tradeitem.statusgtin;

                        if (resultado != string.Empty)
                        {
                            produto.Errors.Add(new string[2] { "ProdutoCoreModel", resultado });
                        }
                    }
                }

                ResponseHeader.numberOfHits = (from produto in produtos
                                               where produto.Errors.Count() == 0
                                               select produto).Count();
                return produtos;

            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
                return null;
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de busca de produtos</b><br/>" +
            "<b><i>GTIN</i></b>: Lista de GTINS a serem buscados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<ProdutoCoreModel> GetCNPCore(List<Int64> GTIN, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();
                List<ProdutoCoreModel> retorno = new List<ProdutoCoreModel>();
           
                if (GLN == string.Empty) {                     
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    return null;
                }

                string listaprodutos;
                if (GTIN != null && GTIN.Count > 0)
                {
                    listaprodutos = string.Join(",", GTIN);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)
                    return null;
                }

                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido
                    return null;
                }

                List<dynamic> produtosCore = produtoBO.GetCNPCore(listaprodutos, GLN);              
                foreach (var item in produtosCore)
                {
                    ProdutoCoreModel produtocoremodel = new ProdutoCoreModel();
                    produtocoremodel.TradeItem = new TradeItemModule();

                    if (item.istradeitemabaseunit != null && item.istradeitemabaseunit.ToString() != string.Empty)
                        produtocoremodel.TradeItem.isTradeItemABaseUnit = Convert.ChangeType(item.istradeitemabaseunit, typeof(bool));
                    if (item.istradeitemaconsumerunit != null && item.istradeitemaconsumerunit.ToString() != string.Empty)
                        produtocoremodel.TradeItem.isTradeItemAConsumerUnit = Convert.ChangeType(item.istradeitemaconsumerunit, typeof(bool));
                    if (item.istradeitemadespatchunit != null && item.istradeitemadespatchunit.ToString() != string.Empty)
                        produtocoremodel.TradeItem.isTradeItemADespatchUnit = Convert.ChangeType(item.istradeitemadespatchunit, typeof(bool));
                    if (item.istradeitemaninvoiceunit != null && item.istradeitemaninvoiceunit.ToString() != string.Empty)
                        produtocoremodel.TradeItem.isTradeItemAnInvoiceUnit = Convert.ChangeType(Convert.ChangeType(item.istradeitemaninvoiceunit.ToString(), typeof(int)), typeof(bool));
                    if (item.istradeitemanorderableunit != null && item.istradeitemanorderableunit.ToString() != string.Empty)
                        produtocoremodel.TradeItem.isTradeItemAnOrderableUnit = Convert.ChangeType(item.istradeitemanorderableunit, typeof(bool));
                    if (item.istradeitemaservice != null && item.istradeitemaservice.ToString() != string.Empty)
                        produtocoremodel.TradeItem.isTradeItemAService = Convert.ChangeType(item.istradeitemaservice, typeof(bool));
                    if (item.istradeitemnonphysical != null && item.istradeitemnonphysical.ToString() != string.Empty)
                        produtocoremodel.TradeItem.isTradeItemNonphysical = Convert.ChangeType(item.istradeitemnonphysical, typeof(bool));
                    if (item.istradeitemrecalled != null && item.istradeitemrecalled.ToString() != string.Empty)
                        produtocoremodel.TradeItem.isTradeItemRecalled = Convert.ChangeType(item.istradeitemrecalled, typeof(bool));

                    produtocoremodel.TradeItem.CodigoTipoGtin = item.codigotipogtin;
                    produtocoremodel.TradeItem.GTIN = item.globaltradeitemnumber.ToString();
                    
                    produtocoremodel.TradeItem.partyInformation = new List<PartyInformation>();
                    produtocoremodel.TradeItem.partyInformation.Add(new PartyInformation());
                    produtocoremodel.TradeItem.partyInformation[0].GLN = GLN;
                    produtocoremodel.TradeItem.partyInformation[0].PartyName = item.razaosocial;
                    produtocoremodel.TradeItem.partyInformation[0].PartyRole = "BRAND_OWNER";

                    produtocoremodel.TradeItem.tradeItemDescription = item.productdescription;

                    if (item.startavailabilitydatetime != null && item.startavailabilitydatetime.ToString() != string.Empty)
                        produtocoremodel.TradeItem.startAvailabilityDatetime = item.startavailabilitydatetime;                    

                    produtocoremodel.TradeItem.Language = item.lingua;
                    
                    if (item.countrycode != null && item.countrycode != string.Empty)
                        produtocoremodel.TradeItem.countryCode = Convert.ChangeType(item.countrycode, typeof(int));

                    if (item.tradeitemcountryoforigin != null && item.tradeitemcountryoforigin.ToString() != string.Empty)
                        produtocoremodel.TradeItem.countryOfOrigin = Convert.ChangeType(item.tradeitemcountryoforigin, typeof(int));
                    
                    produtocoremodel.TradeItem.estado = item.estado;

                    produtocoremodel.TradeItem.StatusGTIN = item.statusgtin;

                    //Agencias                    
                    dynamic paramAgencia = new ExpandoObject();                    
                    paramAgencia.codigoproduto = item.codigoproduto;
                    List<dynamic> agencias = produtoBO.GetCNPCoreBuscarAgenciasReguladoras(paramAgencia);
                    if (agencias.Count > 0)
                    {
                        produtocoremodel.TradeItem.additionalTradeItemIdentification = new List<AdditionalTradeItemIdentification>();                   
                        foreach (var agencia in agencias)
                        {
                            AdditionalTradeItemIdentification additionaltradeitemidentification = new AdditionalTradeItemIdentification();
                            additionaltradeitemidentification.additionalTradeItemIdentificationTypeCode = agencia.nomeagencia;
                            additionaltradeitemidentification.additionalTradeItemIdentificationValue = agencia.alternateitemidentificationid;
                            produtocoremodel.TradeItem.additionalTradeItemIdentification.Add(additionaltradeitemidentification);
                        }
                    }

                    //Hierarquia                    
                    dynamic paramHierarquia = new ExpandoObject();
                    paramHierarquia.codigoprodutosuperior = item.codigoproduto;
                    List<dynamic> hieraquia = produtoBO.GetCNPCorePesquisaGTINInferior(paramHierarquia);
                    if (hieraquia.Count > 0)
                    {
                        produtocoremodel.TradeItem.nextLowerLevel = new NextLowerLevelTradeItemInformation();
                        int i = 0;
                        produtocoremodel.TradeItem.nextLowerLevel.childTradeItem = new ChildTradeItem[hieraquia.Count];
                        produtocoremodel.TradeItem.nextLowerLevel.quantityOfChildren = hieraquia.Count().ToString();
                        foreach (var inferior in hieraquia)
                        {
                            ChildTradeItem childtradeitem = new ChildTradeItem();
                            childtradeitem.GTIN = inferior.globaltradeitemnumber.ToString();
                            childtradeitem.tradeItemDescription = inferior.productdescription;
                            childtradeitem.quantity = inferior.quantidade.ToString();
                            produtocoremodel.TradeItem.nextLowerLevel.childTradeItem[i] = childtradeitem;
                            i++;
                        }
                    }
                    else 
                    {
                        if (item.codigotipogtin == 4) // GTIN-14 
                        {
                            
                            if (item.codigogtinorigem != null && item.totalquantityofnextlowerleveltradeitem != null)
                            {
                                produtocoremodel.TradeItem.nextLowerLevel = new NextLowerLevelTradeItemInformation();
                                produtocoremodel.TradeItem.nextLowerLevel.childTradeItem = new ChildTradeItem[1];
                                produtocoremodel.TradeItem.nextLowerLevel.quantityOfChildren = "1";

                                ChildTradeItem childtradeitem = new ChildTradeItem();
                                childtradeitem.GTIN = item.codigogtinorigem.ToString();
                                childtradeitem.tradeItemDescription = item.nomeprodutoorigem;
                                childtradeitem.quantity = item.totalquantityofnextlowerleveltradeitem.ToString();
                                produtocoremodel.TradeItem.nextLowerLevel.childTradeItem[0] = childtradeitem;
                            }
                        }
                    }

                    produtocoremodel.TradeItem.brandName = item.brandname;
                    produtocoremodel.TradeItem.additionalTradeItemDescription = item.additionaltradeitemdescription;

                    if (item.codebrick != null && item.codebrick.ToString() != String.Empty)
                    {
                        produtocoremodel.TradeItemClassification = new TradeItemClassificationModule();
                        produtocoremodel.TradeItemClassification.tradeitemclassification = new TradeItemClassification();
                        produtocoremodel.TradeItemClassification.tradeitemclassification.gpcCategoryCode = item.codebrick;
                    }


                    //URL                    
                    dynamic paramURL = new ExpandoObject();
                    paramURL.codigoproduto = item.codigoproduto;
                    List<dynamic> urls = produtoBO.GetCNPCorePesquisaURL(paramURL);
                    if (urls.Count > 0)
                    {
                        produtocoremodel.MidiaInformation = new List<MidiaInformationModule>();
                        foreach (var url in urls)
                        {
                            MidiaInformationModule midiainformationmodule = new MidiaInformationModule();
                            midiainformationmodule.referencedFileTypeCode = url.tipourl;
                            midiainformationmodule.uniformResourceIdentifier = url.url;
                            produtocoremodel.MidiaInformation.Add(midiainformationmodule);
                        }
                    }

                    retorno.Add(produtocoremodel);
                }

                ResponseHeader.numberOfHits = retorno.Count();
                return retorno;
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                return null;

            }
        }

        private List<dynamic> GetCNPBrick(List<ProdutoCoreModel> produtos)
        {
            ProdutoBO proutoBO = new ProdutoBO();
            string listaGPC;
            if (produtos != null && produtos.Count > 0)
            {
                IEnumerable<string> produtosGPCQuery =
                    from produtocoremodel in produtos
                    where produtocoremodel.TradeItemClassification != null && produtocoremodel.TradeItemClassification.tradeitemclassification  !=null && produtocoremodel.TradeItemClassification.tradeitemclassification.gpcCategoryCode != null
                    select produtocoremodel.TradeItemClassification.tradeitemclassification.gpcCategoryCode;

                listaGPC = string.Join(",", produtosGPCQuery);
                if (listaGPC != null && listaGPC != String.Empty)
                {
                    return proutoBO.GetCNPBrick(listaGPC);
                }
                else
                {
                    return null;
                }
            }

            return null;
        }

        #endregion

        #region Calçados
        [WebMethod(EnableSession = false, Description = "<b>Método de cadastro de calçados</b><br/>" +            
            "<b><i>calcados</i></b>: Lista de calçados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void InsertCNPCalcados(List<CalcadosModel> calcados, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                        
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }

                string listaprodutos;
                if (calcados != null && calcados.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from produtocoremodel in calcados
                        where produtocoremodel.GTIN != null && produtocoremodel.GTIN != null
                        select produtocoremodel.GTIN;
                    
                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                        //return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)
                    //return null;
                }

                if (ResponseHeader.returnCode == 0)
                {
                    foreach (CalcadosModel calcado in calcados)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.predominantmaterial = calcado.predominantMaterial;
                        param.gtin = calcado.GTIN;
                        param.gln = GLN;

                        param.sizes = new List<ExpandoObject>();
                        foreach (var size in calcado.Size)
                        {
                            dynamic paramSize = new ExpandoObject();
                            paramSize.descriptivesize = size.descriptiveSize;
                            paramSize.sizecode = size.sizeCode;
                            paramSize.sizesystemcode = size.sizeSystemCode;
                            paramSize.gln = param.gln;
                            paramSize.gtin = param.gtin;
                            param.sizes.Add(paramSize);
                        }

                        param.colours = new List<ExpandoObject>();
                        foreach (var colour in calcado.Colour)
                        {
                            dynamic paramColour = new ExpandoObject();
                            paramColour.colourcode = colour.colourCode;
                            paramColour.colourdescription = colour.colourDescription;
                            paramColour.colourcodeagency = colour.colourCodeAgency;
                            paramColour.gln = param.gln;
                            paramColour.gtin = param.gtin;
                            param.colours.Add(paramColour);
                        }

                        produtoBO.InsertCNPCalcados(param);

                    }
                    ResponseHeader.numberOfHits = calcados.Count();
                }

                //return new Retorno(1, "Calçados cadastrados com sucesso.");
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                //return null;
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de atualização de calçados</b><br/>" +            
            "<b><i>calcados</i></b>: Lista de calçados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void UpdateCNPCalcados(List<CalcadosModel> calcados, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (calcados != null && calcados.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from produtocoremodel in calcados
                        where produtocoremodel.GTIN != null && produtocoremodel.GTIN != null
                        select produtocoremodel.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                        //return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)
                    //return null;
                }

                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                        
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }

                if (ResponseHeader.returnCode == 0)
                {
                    foreach (CalcadosModel calcado in calcados)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.predominantmaterial = calcado.predominantMaterial;
                        param.gtin = calcado.GTIN;
                        param.gln = GLN;

                        param.sizes = new List<ExpandoObject>();
                        foreach (var size in calcado.Size)
                        {
                            dynamic paramSize = new ExpandoObject();
                            paramSize.descriptivesize = size.descriptiveSize;
                            paramSize.sizecode = size.sizeCode;
                            paramSize.sizesystemcode = size.sizeSystemCode;
                            paramSize.gln = param.gln;
                            paramSize.gtin = param.gtin;
                            param.sizes.Add(paramSize);
                        }

                        param.colours = new List<ExpandoObject>();
                        foreach (var colour in calcado.Colour)
                        {
                            dynamic paramColour = new ExpandoObject();
                            paramColour.colourcode = colour.colourCode;
                            paramColour.colourdescription = colour.colourDescription;
                            paramColour.colourcodeagency = colour.colourCodeAgency;
                            paramColour.gln = param.gln;
                            paramColour.gtin = param.gtin;
                            param.colours.Add(paramColour);
                        }

                        produtoBO.UpdateCNPCalcados(param);

                    }

                    ResponseHeader.numberOfHits = calcados.Count();
                    //return new Retorno(1, "Calçados atualizados com sucesso.");
                }
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                //return null;
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de busca de calçados</b><br/>" +
            "<b><i>GTIN</i></b>: Lista de GTINS a serem buscados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<CalcadosModel> GetCNPCalcados(List<Int64> GTIN, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();
                List<CalcadosModel> retorno = new List<CalcadosModel>();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    return null;
                }

                string listaprodutos;
                if (GTIN != null && GTIN.Count > 0)
                {
                    //IEnumerable<string> produtosQuery =
                    //    from produtocoremodel in calcados
                    //    where produtocoremodel.GTIN != null && produtocoremodel.GTIN != null
                    //    select produtocoremodel.GTIN;

                    listaprodutos = string.Join(",", GTIN);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)
                    return null;
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido
                    return null;
                }

                List<dynamic> produtosCore = produtoBO.GetCNPCalcados(listaprodutos, GLN);
                foreach (var item in produtosCore)
                {
                    CalcadosModel calcadosmodel = new CalcadosModel();
                    calcadosmodel.GTIN = item.globaltradeitemnumber.ToString();
                    calcadosmodel.predominantMaterial = item.predominantmaterial;

                    //Size                    
                    dynamic paramSize = new ExpandoObject();
                    paramSize.codigoproduto = item.codigoproduto;
                    List<dynamic> sizes = produtoBO.GetProdutoSharedCommonComponentsSize(paramSize);
                    if (sizes.Count > 0)
                    {
                        calcadosmodel.Size = new List<SizeModel>();
                        foreach (var size in sizes)
                        {
                            SizeModel sizemodel = new SizeModel();
                            sizemodel.descriptiveSize = size.descriptivesize;
                            sizemodel.sizeCode = size.sizecode;
                            sizemodel.sizeSystemCode = size.sizesystemcode;
                            calcadosmodel.Size.Add(sizemodel);
                        }
                    }

                    //Colour                    
                    dynamic paramColour = new ExpandoObject();
                    paramColour.codigoproduto = item.codigoproduto;
                    List<dynamic> colours = produtoBO.GetProdutoSharedCommonComponentsColour(paramColour);
                    if (colours.Count > 0)
                    {
                        calcadosmodel.Colour = new List<ColourModel>();
                        foreach (var colour in colours)
                        {
                            ColourModel colourmodel = new ColourModel();
                            colourmodel.colourCode = colour.colourcode;
                            colourmodel.colourDescription = colour.colourdescription;
                            colourmodel.colourCodeAgency = colour.colourcodeagency;
                            calcadosmodel.Colour.Add(colourmodel);
                        }
                    }
                    retorno.Add(calcadosmodel);
                }

                ResponseHeader.numberOfHits = retorno.Count();
                return retorno;
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                return null;
            }
        }
        #endregion

        #region Informações Nutricionais

        [WebMethod(EnableSession = false, Description = "<b>Método de cadastro de informações nutricionais</b><br/>" +            
            "<b><i>nutricionais</i></b>: Lista de informações nutricionais<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void InsertCNPInformacaoNutricional(List<NutritionalInformationModule> nutricionais, string GLN)
        {
            try
            {

                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (nutricionais != null && nutricionais.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from deliverypurchasinginformation in nutricionais
                        where deliverypurchasinginformation.GTIN != null && deliverypurchasinginformation.GTIN != null
                        select deliverypurchasinginformation.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }
                

                if(ResponseHeader.returnCode == 0){
                    int qtdHeader = 0;
                    int qtdDetail = 0;
                    foreach (NutritionalInformationModule nutritionalInformationModule in nutricionais)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.gtin = nutritionalInformationModule.GTIN;
                        param.gln = GLN;
                        param.dailyvalueintakereference = nutritionalInformationModule.nutrientHeader.dailyValueIntakeReference;
                        param.servingsizedescription = nutritionalInformationModule.nutrientHeader.servingSizeDescription;
                        qtdHeader = produtoBO.InsertCNPInformacoesNutricionaisHeader(param);

                        foreach (NutrientDetail nutrientDetail in nutritionalInformationModule.nutrientHeader.nutrientDetail)
                        {
                            param.dailyvalueintakepercent = nutrientDetail.dailyValueIntakePercent;
                            param.measurementprecisioncode = nutrientDetail.measurementPrecisionCode;
                            param.nutrienttypecode = nutrientDetail.nutrientTypeCode;
                            param.quantitycontained = nutrientDetail.quantityContained;
                            qtdDetail = qtdDetail + produtoBO.InsertCNPInformacoesNutricionaisDetail(param);
                        }

                        if ((qtdHeader + qtdDetail) > 0)
                        {
                            ResponseHeader.numberOfHits = ResponseHeader.numberOfHits + 1;
                        }
                        qtdHeader = 0;
                        qtdDetail = 0;
                    }                               
                }
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de atualização de informações nutricionais</b><br/>" +            
            "<b><i>nutricionais</i></b>: Lista de informações nutricionais<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void UpdateCNPInformacaoNutricional(List<NutritionalInformationModule> nutricionais, string GLN)
        {
            try
            {

                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (nutricionais != null && nutricionais.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from deliverypurchasinginformation in nutricionais
                        where deliverypurchasinginformation.GTIN != null && deliverypurchasinginformation.GTIN != null
                        select deliverypurchasinginformation.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }



                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {
                    int qtdHeader = 0;
                    int qtdDetail = 0;
                    foreach (NutritionalInformationModule nutritionalInformationModule in nutricionais)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.gtin = nutritionalInformationModule.GTIN;
                        param.gln = GLN;
                        param.dailyvalueintakereference = nutritionalInformationModule.nutrientHeader.dailyValueIntakeReference;
                        param.servingsizedescription = nutritionalInformationModule.nutrientHeader.servingSizeDescription;
                        qtdHeader = produtoBO.UpdateCNPInformacoesNutricionaisHeader(param);

                        foreach (NutrientDetail nutrientDetail in nutritionalInformationModule.nutrientHeader.nutrientDetail)
                        {
                            param.dailyvalueintakepercent = nutrientDetail.dailyValueIntakePercent;
                            param.measurementprecisioncode = nutrientDetail.measurementPrecisionCode;
                            param.nutrienttypecode = nutrientDetail.nutrientTypeCode;
                            param.quantitycontained = nutrientDetail.quantityContained;
                            qtdDetail = qtdDetail + produtoBO.UpdateCNPInformacoesNutricionaisDetail(param);
                        }

                        if ((qtdHeader + qtdDetail) > 0)
                        {
                            ResponseHeader.numberOfHits = ResponseHeader.numberOfHits + 1;
                        }
                        qtdHeader = 0;
                        qtdDetail = 0;
                    }
                }

            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de busca de informações nutricionais</b><br/>" +
            "<b><i>GTIN</i></b>: Lista de GTINS a serem buscados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<NutritionalInformationModule> GetCNPInformacoesNutricionais(List<Int64> GTIN, string GLN)
        {
            try
            {
                List<NutritionalInformationModule> retorno = new List<NutritionalInformationModule>();
                
                ResponseHeader = new ProdutoCoreResponseHeader();                
                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    return null;
                }

                string listaprodutos;
                if (GTIN != null && GTIN.Count > 0)
                {                    
                    listaprodutos = string.Join(",", GTIN);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)
                    return null;
                }
                

                  ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido  
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                    return null;
                }


                if (ResponseHeader.returnCode == 0)
                {
                    List<dynamic> produtosNutricionais = produtoBO.GetCNPInformacoesNutricionais(listaprodutos, GLN);

                    NutritionalInformationModule nutritionalInformationModule = new NutritionalInformationModule();
                    nutritionalInformationModule.nutrientHeader = new NutrientHeader();
                    nutritionalInformationModule.nutrientHeader.nutrientDetail = new List<NutrientDetail>();

                    long gtin = 0;
                    foreach (var item in produtosNutricionais)
                    {

                        NutrientDetail nutrientDetail = new NutrientDetail();
                        if (item.dailyvalueintakepercent.ToString() != String.Empty)
                        {
                            nutrientDetail.dailyValueIntakePercent = double.Parse(item.dailyvalueintakepercent);
                        }
                        nutrientDetail.measurementPrecisionCode = item.measurementprecisioncode;
                        nutrientDetail.nutrientTypeCode = item.nutrienttypecode;
                        nutrientDetail.quantityContained = item.quantitycontained;



                        if (gtin == 0 || gtin != item.globaltradeitemnumber)
                        {
                            if (gtin > 0)
                            {
                                retorno.Add(nutritionalInformationModule);
                            }

                            nutritionalInformationModule = new NutritionalInformationModule();
                            nutritionalInformationModule.GTIN = item.globaltradeitemnumber.ToString();
                            nutritionalInformationModule.nutrientHeader = new NutrientHeader();

                            nutritionalInformationModule.nutrientHeader = new NutrientHeader();
                            nutritionalInformationModule.nutrientHeader.dailyValueIntakeReference = item.dailyvalueintakereference.ToString();
                            nutritionalInformationModule.nutrientHeader.servingSizeDescription = item.servingsizedescription.ToString();
                            nutritionalInformationModule.nutrientHeader.nutrientDetail = new List<NutrientDetail>();
                        }

                        nutritionalInformationModule.nutrientHeader.nutrientDetail.Add(nutrientDetail);
                        gtin = item.globaltradeitemnumber;

                    }
                    retorno.Add(nutritionalInformationModule);
                    ResponseHeader.numberOfHits = retorno.Count();
                    return retorno;
                }

                return null;
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
                return null;
            }
        }
        
        #endregion

        #region Alérgenos
        [WebMethod(EnableSession = false, Description = "<b>Método de cadastro de alérgenos</b><br/>" +            
            "<b><i>alergenos</i></b>: Lista de alérgenos<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void InsertCNPAlergenos(List<AllergenInformationModule> alergenos, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (alergenos != null && alergenos.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from deliverypurchasinginformation in alergenos
                        where deliverypurchasinginformation.GTIN != null && deliverypurchasinginformation.GTIN != null
                        select deliverypurchasinginformation.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }

                  ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {

                    int qtdHeader = 0;
                    int qtdDetail = 0;
                    foreach (AllergenInformationModule allergenInformationModule in alergenos)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.gtin = allergenInformationModule.GTIN;
                        param.gln = GLN;

                        foreach (Allergen allergen in allergenInformationModule.allergenRelatedInformation.allergen)
                        {
                            param.allergentypecode = allergen.allergenTypeCode;
                            param.levelofcontainmentcode = allergen.levelOfContainmentCode;
                            qtdDetail = qtdDetail + produtoBO.InsertCNPInformacoesAlergenos(param);
                        }

                        if ((qtdHeader + qtdDetail) > 0)
                        {
                            ResponseHeader.numberOfHits = ResponseHeader.numberOfHits + 1;
                        }
                        qtdHeader = 0;
                        qtdDetail = 0;
                    }
                }


            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de atualização de alérgenos</b><br/>" +            
            "<b><i>alergenos</i></b>: Lista de alérgenos<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void UpdateCNPAlergenos(List<AllergenInformationModule> alergenos, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (alergenos != null && alergenos.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from deliverypurchasinginformation in alergenos
                        where deliverypurchasinginformation.GTIN != null && deliverypurchasinginformation.GTIN != null
                        select deliverypurchasinginformation.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }

                 ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {

                    int qtdHeader = 0;
                    int qtdDetail = 0;
                    foreach (AllergenInformationModule allergenInformationModule in alergenos)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.gtin = allergenInformationModule.GTIN;
                        param.gln = GLN;

                        foreach (Allergen allergen in allergenInformationModule.allergenRelatedInformation.allergen)
                        {
                            param.allergentypecode = allergen.allergenTypeCode;
                            param.levelofcontainmentcode = allergen.levelOfContainmentCode;
                            qtdDetail = qtdDetail + produtoBO.UpdateCNPInformacoesAlergenos(param);
                        }

                        if ((qtdHeader + qtdDetail) > 0)
                        {
                            ResponseHeader.numberOfHits = ResponseHeader.numberOfHits + 1;
                        }
                        qtdHeader = 0;
                        qtdDetail = 0;
                    }
                }


            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de busca de alérgenos</b><br/>" +
            "<b><i>GTIN</i></b>: Lista de GTINS a serem buscados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<AllergenInformationModule> GetCNPAlergenos(List<Int64> GTIN, string GLN)
        {
            try
            {
                List<AllergenInformationModule> retorno = new List<AllergenInformationModule>();

                ResponseHeader = new ProdutoCoreResponseHeader();
                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    return null;
                }

                string listaprodutos;
                if (GTIN != null && GTIN.Count > 0)
                {
                    listaprodutos = string.Join(",", GTIN);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)
                    return null;
                }


                  ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido  
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido     
                    return null;
                }


                if (ResponseHeader.returnCode == 0)
                {
                    List<dynamic> produtosAlergenos = produtoBO.GetCNPInformacoesAlergenos(listaprodutos, GLN);

                    AllergenInformationModule allergenInformationModule = new AllergenInformationModule();
                    allergenInformationModule.allergenRelatedInformation = new AllergenRelatedInformation();
                    allergenInformationModule.allergenRelatedInformation.allergen = new List<Allergen>();

                    long gtin = 0;
                    foreach (var item in produtosAlergenos)
                    {

                        Allergen allergen = new Allergen();
                        allergen.allergenTypeCode = item.sigla;
                        allergen.levelOfContainmentCode = item.levelofcontainmentcode;


                        if (gtin == 0 || gtin != item.globaltradeitemnumber)
                        {
                            if (gtin > 0)
                            {
                                retorno.Add(allergenInformationModule);
                            }

                            allergenInformationModule = new AllergenInformationModule();
                            allergenInformationModule.GTIN = item.globaltradeitemnumber.ToString();
                            allergenInformationModule.allergenRelatedInformation = new AllergenRelatedInformation();

                            allergenInformationModule.allergenRelatedInformation.allergen = new List<Allergen>();
                        }


                        allergenInformationModule.allergenRelatedInformation.allergen.Add(allergen);
                        gtin = item.globaltradeitemnumber;

                    }

                    retorno.Add(allergenInformationModule);
                    ResponseHeader.numberOfHits = retorno.Count();
                    return retorno;
                }

                return null;
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
                return null;
            }
        }
        #endregion

        #region Validade Trade Item

        [WebMethod(EnableSession = false, Description = "<b>Método de cadastro de validade trade item</b><br/>" +            
            "<b><i>tradeitemlifespan</i></b>: Lista validades<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void UpdateCNPValidadeTradeItem(List<TradeItemLifespanModule> tradeItemLifespan, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();
                List<CalcadosModel> retorno = new List<CalcadosModel>();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado                    
                }

                string listaprodutos;
                if (tradeItemLifespan != null && tradeItemLifespan.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from tradeitemlifespan in tradeItemLifespan
                        where tradeitemlifespan.GTIN != null && tradeitemlifespan.GTIN != null
                        select tradeitemlifespan.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;                        
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                    
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {

                    foreach (TradeItemLifespanModule lifespan in tradeItemLifespan)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.minimumtradeitemlifespanfromtimeofproduction = lifespan.minimumTradeItemLifespanFromTimeOfProduction;
                        param.gtin = lifespan.GTIN;
                        param.gln = GLN;
                        produtoBO.UpdateCNPLifeSpan(param);
                    }
                    ResponseHeader.numberOfHits = tradeItemLifespan.Count();
                }
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.                
            }
        }


         [WebMethod(EnableSession = false, Description = "<b>Método de busca de validade trade item</b><br/>" +
            "<b><i>GTIN</i></b>: Lista de GTINS a serem buscados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<TradeItemLifespanModule> GetCNPValidadeTradeItem(List<Int64> GTIN, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();
                List<TradeItemLifespanModule> retorno = new List<TradeItemLifespanModule>();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado                    
                }

                string listaprodutos = string.Empty;
                if (GTIN != null && GTIN.Count > 0)
                {
                    //IEnumerable<string> produtosQuery =
                    //    from tradeitemlifespan in tradeItemLifespan
                    //    where tradeitemlifespan.GTIN != null && tradeitemlifespan.GTIN != null
                    //    select tradeitemlifespan.GTIN;

                    listaprodutos = string.Join(",", GTIN);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                    
                }

                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido    
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido        
                    return null;
                }


                if (ResponseHeader.returnCode == 0)
                {
                    List<dynamic> produtosLifeSpan = produtoBO.GetCNPLifeSpan(listaprodutos, GLN);
                    foreach (var item in produtosLifeSpan)
                    {
                        TradeItemLifespanModule tradeitemlifespanmodule = new TradeItemLifespanModule();

                        tradeitemlifespanmodule.GTIN = item.globaltradeitemnumber.ToString();
                        if (item.minimumtradeitemlifespanfromtimeofproduction.ToString() != String.Empty)
                        {
                            tradeitemlifespanmodule.minimumTradeItemLifespanFromTimeOfProduction = Int32.Parse(item.minimumtradeitemlifespanfromtimeofproduction.ToString());
                        }
                        retorno.Add(tradeitemlifespanmodule);
                    }

                    ResponseHeader.numberOfHits = retorno.Count();
                    return retorno;
                }

                return null;
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                return null;
            }
        }

        #endregion Validade Trade Item

        #region Compra

        [WebMethod(EnableSession = false, Description = "<b>Método de cadastro de informações de compra</b><br/>" +             
            "<b><i>deliveryPurchasingInformation</i></b>: Lista de informações de compra<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void InsertCNPCompra(List<DeliveryPurchasingInformationModule> deliveryPurchasingInformation, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (deliveryPurchasingInformation != null && deliveryPurchasingInformation.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from deliverypurchasinginformation in deliveryPurchasingInformation
                        where deliverypurchasinginformation.GTIN != null && deliverypurchasinginformation.GTIN != null
                        select deliverypurchasinginformation.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {

                    foreach (DeliveryPurchasingInformationModule deliverypurchasinginformation in deliveryPurchasingInformation)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.packagingtypedescription = deliverypurchasinginformation.packagingTypeDescription;
                        param.gtin = deliverypurchasinginformation.GTIN;
                        param.gln = GLN;

                        param.orderingUnitOfMeasure = deliverypurchasinginformation.deliveryPurchasingInformation.orderingUnitOfMeasure;
                        param.orderQuantityMinimum = deliverypurchasinginformation.deliveryPurchasingInformation.orderQuantityMinimum;
                        param.orderQuantityMultiple = deliverypurchasinginformation.deliveryPurchasingInformation.orderQuantityMultiple;
                        param.startAvailabilityDateTime = deliverypurchasinginformation.deliveryPurchasingInformation.startAvailabilityDateTime;
                        param.endAvailabilityDateTime = deliverypurchasinginformation.deliveryPurchasingInformation.endAvailabilityDateTime;

                        produtoBO.InsertCNPCompra(param);

                    }

                    ResponseHeader.numberOfHits = deliveryPurchasingInformation.Count();
                }

            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                //return null;
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de atualização de informações de compra</b><br/>" +             
            "<b><i>deliveryPurchasingInformation</i></b>: Lista de informações de compra<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void UpdateCNPCompra(List<DeliveryPurchasingInformationModule> deliveryPurchasingInformation, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (deliveryPurchasingInformation != null && deliveryPurchasingInformation.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from deliverypurchasinginformation in deliveryPurchasingInformation
                        where deliverypurchasinginformation.GTIN != null && deliverypurchasinginformation.GTIN != null
                        select deliverypurchasinginformation.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;                         
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }

                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {

                    foreach (DeliveryPurchasingInformationModule deliverypurchasinginformation in deliveryPurchasingInformation)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic param = new ExpandoObject();

                        param.packagingtypedescription = deliverypurchasinginformation.packagingTypeDescription;
                        param.gtin = deliverypurchasinginformation.GTIN;
                        param.gln = GLN;

                        param.orderingUnitOfMeasure = deliverypurchasinginformation.deliveryPurchasingInformation.orderingUnitOfMeasure;
                        param.orderQuantityMinimum = deliverypurchasinginformation.deliveryPurchasingInformation.orderQuantityMinimum;
                        param.orderQuantityMultiple = deliverypurchasinginformation.deliveryPurchasingInformation.orderQuantityMultiple;
                        param.startAvailabilityDateTime = deliverypurchasinginformation.deliveryPurchasingInformation.startAvailabilityDateTime;
                        param.endAvailabilityDateTime = deliverypurchasinginformation.deliveryPurchasingInformation.endAvailabilityDateTime;

                        produtoBO.UpdateCNPCompra(param);

                    }

                    ResponseHeader.numberOfHits = deliveryPurchasingInformation.Count();
                }
                 
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                //return null;
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de busca de informações de compra</b><br/>" +
            "<b><i>GTIN</i></b>: Lista de GTINS a serem buscados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<DeliveryPurchasingInformationModule> GetCNPCompra(List<Int64> GTIN, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();
                List<DeliveryPurchasingInformationModule> retorno = new List<DeliveryPurchasingInformationModule>();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos = string.Empty;
                if (GTIN != null && GTIN.Count > 0)
                {
                    //IEnumerable<string> produtosQuery =
                    //    from deliverypurchasinginformation in deliveryPurchasingInformation
                    //    where deliverypurchasinginformation.GTIN != null && deliverypurchasinginformation.GTIN != null
                    //    select deliverypurchasinginformation.GTIN;

                    listaprodutos = string.Join(",", GTIN);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }

                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido        
                        return null;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                    return null;
                }


                if (ResponseHeader.returnCode == 0)
                {
                    List<dynamic> produtosCompra = produtoBO.GetCNPCompra(listaprodutos, GLN);
                    foreach (var item in produtosCompra)
                    {
                        DeliveryPurchasingInformationModule DeliveryPurchasing = new DeliveryPurchasingInformationModule();
                        DeliveryPurchasing.GTIN = item.globaltradeitemnumber.ToString();
                        DeliveryPurchasing.packagingTypeDescription = item.packagingtypecode;
                        DeliveryPurchasing.deliveryPurchasingInformation = new DeliveryPurchasingInformation();
                        DeliveryPurchasing.deliveryPurchasingInformation.orderingUnitOfMeasure = item.orderingunitofmeasure;

                        if (item.orderquantityminimum.ToString() != string.Empty)
                        {
                            DeliveryPurchasing.deliveryPurchasingInformation.orderQuantityMinimum = item.orderquantityminimum;
                        }

                        if (item.orderquantitymultiple.ToString() != string.Empty)
                        {
                            DeliveryPurchasing.deliveryPurchasingInformation.orderQuantityMultiple = item.orderquantitymultiple;
                        }

                        if (item.startavailabilitydatetime.ToString() != string.Empty)
                        {
                            DeliveryPurchasing.deliveryPurchasingInformation.startAvailabilityDateTime = item.startavailabilitydatetime;
                        }

                        if (item.endavailabilitydatetime.ToString() != string.Empty)
                        {
                            DeliveryPurchasing.deliveryPurchasingInformation.endAvailabilityDateTime = item.endavailabilitydatetime;
                        }

                        retorno.Add(DeliveryPurchasing);
                    }

                    ResponseHeader.numberOfHits = retorno.Count();
                    return retorno;
                }

                return null;

            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                return null;
            }
        }

        #endregion

        #region Impostos

        [WebMethod(EnableSession = false, Description = "<b>Método de cadastro de impostos</b><br/>" +             
            "<b><i>impostos</i></b>: Lista de impostos<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void InsertCNPImpostos(List<DutyFeeTaxInformationModule> dutyFeeTaxInformationModule, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (dutyFeeTaxInformationModule != null && dutyFeeTaxInformationModule.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from dutyfeetax in dutyFeeTaxInformationModule                         
                        where dutyfeetax.GTIN != null && dutyfeetax.GTIN != null
                        select dutyfeetax.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {

                    foreach (DutyFeeTaxInformationModule taxmodule in dutyFeeTaxInformationModule)
                    {
                        foreach (DutyFeeTaxInformation tax in taxmodule.dutyFeeTaxInformation)
                        {
                            //ProdutoBO produtoBO = new ProdutoBO();
                            dynamic param = new ExpandoObject();

                            if (tax.dutyFeeTax.Count > 0)
                            {
                                dynamic paramDutyfeetax = new ExpandoObject();
                                foreach (var dutyfeetax in tax.dutyFeeTax)
                                {
                                    paramDutyfeetax.dutyfeetaxamount = dutyfeetax.dutyFeeTaxAmount.ToString();
                                    paramDutyfeetax.dutyfeetaxbasis = dutyfeetax.dutyFeeTaxBasis; ;
                                    paramDutyfeetax.dutyfeetaxcategorycode = dutyfeetax.dutyFeeTaxCategoryCode.ToString();
                                    paramDutyfeetax.dutyfeetaxcountrysubdivisioncode = dutyfeetax.dutyFeeTaxCountrySubdivisionCode;
                                    paramDutyfeetax.dutyfeetaxexemptpartyrolecode = dutyfeetax.dutyFeeTaxExemptPartyRoleCode.ToString();
                                    paramDutyfeetax.dutyfeetaxrate = dutyfeetax.dutyFeeTaxRate;
                                    paramDutyfeetax.dutyfeetaxreductioncriteriadescription = dutyfeetax.dutyFeeTaxReductionCriteriaDescription;

                                    paramDutyfeetax.dutyfeetaxtypecode = tax.dutyFeeTaxTypeCode;
                                    paramDutyfeetax.dutyfeetaxtypedescription = tax.dutyFeeTaxTypeDescription;
                                    paramDutyfeetax.gln = GLN;
                                    paramDutyfeetax.gtin = taxmodule.GTIN;
                                    produtoBO.InsertProdutoDutyFeeTax(paramDutyfeetax);
                                }
                            }
                            else
                            {
                                param.dutyfeetaxtypecode = tax.dutyFeeTaxTypeCode;
                                param.dutyfeetaxtypedescription = tax.dutyFeeTaxTypeDescription;
                                param.gtin = taxmodule.GTIN;
                                param.gln = GLN;
                                produtoBO.InsertProdutoDutyFeeTax(param);
                            }

                        }
                    }
                    ResponseHeader.numberOfHits = dutyFeeTaxInformationModule.Count();
                }

            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                //return null;
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de atualização de impostos</b><br/>" +
            "<b><i>impostos</i></b>: Lista de impostos<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void UpdateCNPImpostos(List<DutyFeeTaxInformationModule> dutyFeeTaxInformationModule, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (dutyFeeTaxInformationModule != null && dutyFeeTaxInformationModule.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from dutyfeetax in dutyFeeTaxInformationModule
                        where dutyfeetax.GTIN != null && dutyfeetax.GTIN != null
                        select dutyfeetax.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }

                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {

                    foreach (DutyFeeTaxInformationModule taxmodule in dutyFeeTaxInformationModule)
                    {
                        //ProdutoBO produtoBO = new ProdutoBO();
                        dynamic paramDel = new ExpandoObject();
                        paramDel.gtin = taxmodule.GTIN;
                        paramDel.gln = GLN;
                        produtoBO.DeleteProdutoDutyFeeTax(paramDel);

                        foreach (DutyFeeTaxInformation tax in taxmodule.dutyFeeTaxInformation)
                        {

                            dynamic param = new ExpandoObject();

                            if (tax.dutyFeeTax.Count > 0)
                            {
                                dynamic paramDutyfeetax = new ExpandoObject();
                                foreach (var dutyfeetax in tax.dutyFeeTax)
                                {
                                    paramDutyfeetax.dutyfeetaxamount = dutyfeetax.dutyFeeTaxAmount.ToString();
                                    paramDutyfeetax.dutyfeetaxbasis = dutyfeetax.dutyFeeTaxBasis; ;
                                    paramDutyfeetax.dutyfeetaxcategorycode = dutyfeetax.dutyFeeTaxCategoryCode.ToString();
                                    paramDutyfeetax.dutyfeetaxcountrysubdivisioncode = dutyfeetax.dutyFeeTaxCountrySubdivisionCode;
                                    paramDutyfeetax.dutyfeetaxexemptpartyrolecode = dutyfeetax.dutyFeeTaxExemptPartyRoleCode.ToString();
                                    paramDutyfeetax.dutyfeetaxrate = dutyfeetax.dutyFeeTaxRate;
                                    paramDutyfeetax.dutyfeetaxreductioncriteriadescription = dutyfeetax.dutyFeeTaxReductionCriteriaDescription;

                                    paramDutyfeetax.dutyfeetaxtypecode = tax.dutyFeeTaxTypeCode;
                                    paramDutyfeetax.dutyfeetaxtypedescription = tax.dutyFeeTaxTypeDescription;
                                    paramDutyfeetax.gln = GLN;
                                    paramDutyfeetax.gtin = taxmodule.GTIN;
                                    produtoBO.InsertProdutoDutyFeeTax(paramDutyfeetax);
                                }
                            }
                            else
                            {
                                param.dutyfeetaxtypecode = tax.dutyFeeTaxTypeCode;
                                param.dutyfeetaxtypedescription = tax.dutyFeeTaxTypeDescription;
                                param.gtin = taxmodule.GTIN;
                                param.gln = GLN;
                                produtoBO.InsertProdutoDutyFeeTax(param);
                            }

                        }
                    }
                    ResponseHeader.numberOfHits = dutyFeeTaxInformationModule.Count();
                }

            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar.
                //return null;
            }
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de busca de impostos</b><br/>" +
            "<b><i>GTIN</i></b>: Lista de GTINS a serem buscados<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public List<DutyFeeTaxInformationModule> GetCNPImpostos(List<Int64> GTIN, string GLN)
        {
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();
                List<DutyFeeTaxInformationModule> retorno = new List<DutyFeeTaxInformationModule>();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos = string.Empty;
                if (GTIN != null && GTIN.Count > 0)
                {                     
                    listaprodutos = string.Join(",", GTIN);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {
                    List<dynamic> produtosImposto = produtoBO.GetProdutoDutyFeeTax(listaprodutos, GLN);
                    string dutyfeetaxtypecode = string.Empty;
                    DutyFeeTaxInformationModule taxmodule = new DutyFeeTaxInformationModule();
                    DutyFeeTaxInformation taxInfo = new DutyFeeTaxInformation();
                    taxInfo.dutyFeeTax = new List<DutyFeeTax>();
                    taxmodule.dutyFeeTaxInformation = new List<DutyFeeTaxInformation>();

                    foreach (var item in produtosImposto)
                    {
                        DutyFeeTax dutyfeetax = new DutyFeeTax();
                        dutyfeetax.dutyFeeTaxAmount = Enum.Parse(typeof(currencyCode), item.dutyfeetaxamount); //item.dutyfeetaxamount;
                        dutyfeetax.dutyFeeTaxBasis = item.dutyfeetaxbasis;
                        dutyfeetax.dutyFeeTaxCategoryCode = Enum.Parse(typeof(taxCategoryCode), item.dutyfeetaxcategorycode); //item.dutyfeetaxcategorycode;
                        dutyfeetax.dutyFeeTaxCountrySubdivisionCode = item.dutyfeetaxcountrysubdivisioncode;
                        dutyfeetax.dutyFeeTaxExemptPartyRoleCode = Enum.Parse(typeof(partyRoleCode), item.dutyfeetaxexemptpartyrolecode); //item.dutyfeetaxexemptpartyrolecode;
                        dutyfeetax.dutyFeeTaxRate = item.dutyfeetaxrate;
                        dutyfeetax.dutyFeeTaxReductionCriteriaDescription = item.dutyfeetaxreductioncriteriadescription;

                        if (dutyfeetaxtypecode == string.Empty || dutyfeetaxtypecode != item.dutyfeetaxtypecode)
                        {
                            if (dutyfeetaxtypecode != string.Empty)
                            {
                                taxmodule.dutyFeeTaxInformation.Add(taxInfo);
                                retorno.Add(taxmodule);
                            }
                            taxmodule = new DutyFeeTaxInformationModule();
                            taxmodule.dutyFeeTaxInformation = new List<DutyFeeTaxInformation>();
                            taxmodule.GTIN = item.globaltradeitemnumber.ToString();

                            taxInfo = new DutyFeeTaxInformation();
                            taxInfo.dutyFeeTax = new List<DutyFeeTax>();
                            taxInfo.dutyFeeTaxTypeCode = item.dutyfeetaxtypecode;
                            taxInfo.dutyFeeTaxTypeDescription = item.dutyfeetaxtypedescription;

                        }

                        taxInfo.dutyFeeTax.Add(dutyfeetax);
                        dutyfeetaxtypecode = item.dutyfeetaxtypecode;

                    }

                    taxmodule.dutyFeeTaxInformation.Add(taxInfo);
                    retorno.Add(taxmodule);
                    ResponseHeader.numberOfHits = retorno.Count();
                    return retorno;
                }
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
            }

            return null;
        }

        #endregion

        #region Logística        
     
        [WebMethod(EnableSession = false, Description = "<b>Método de busca de infomações de logística</b><br/>" +
            "<b><i>logisticaModule</i></b>: Lista de informações de logística<br/>" +
            "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public void InsertCNPLogistica(List<LogisticaModule> logisticaModule, string GLN)
        {            
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos;
                if (logisticaModule != null && logisticaModule.Count > 0)
                {
                    IEnumerable<string> produtosQuery =
                        from dutyfeetax in logisticaModule
                        where dutyfeetax.GTIN != null && dutyfeetax.GTIN != null
                        select dutyfeetax.GTIN;

                    listaprodutos = string.Join(",", produtosQuery);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {


                    foreach (LogisticaModule item in logisticaModule)
                    {
                        dynamic param = new ExpandoObject();

                        if (item.catalogueItemNotificationModule != null)
                        {
                            param.tradeitemunitdescriptorcode = item.catalogueItemNotificationModule.tradeItem.tradeItemUnitDescriptorCode;
                        }

                        if (item.tradeItemHierarchyModule != null && item.tradeItemHierarchyModule.tradeItemHierarchy != null)
                        {
                            param.totalquantityofnextlowerleveltradeitem = item.tradeItemHierarchyModule.tradeItemHierarchy.totalQuantityOfNextLowerLevelTradeItem;
                            param.quantityofcompletelayerscontainedinatradeitem = item.tradeItemHierarchyModule.tradeItemHierarchy.quantityOfCompleteLayersContainedInATradeItem;
                            param.quantityoflayersperpallet = item.tradeItemHierarchyModule.tradeItemHierarchy.quantityOfLayersPerPallet;
                            param.quantityoftradeitemcontainedinacompletelayer = item.tradeItemHierarchyModule.tradeItemHierarchy.quantityOfTradeItemsContainedInACompleteLayer;
                            param.quantityoftradeitemsperpalletlayer = item.tradeItemHierarchyModule.tradeItemHierarchy.quantityOfTradeItemsPerPalletLayer;
                        }

                        if (item.tradeItemHandlingModule != null && item.tradeItemHandlingModule.tradeItemHandlingInformation != null && item.tradeItemHandlingModule.tradeItemHandlingInformation.tradeItemStacking != null)
                        {
                            param.stackingfactor = item.tradeItemHandlingModule.tradeItemHandlingInformation.tradeItemStacking.stackingFactor;
                        }

                        if (item.tradeItemMeasurementsModule != null && item.tradeItemMeasurementsModule.tradeItemMeasurements != null)
                        {
                            if (item.tradeItemMeasurementsModule.tradeItemMeasurements.depth != null)
                            {
                                param.depth = item.tradeItemMeasurementsModule.tradeItemMeasurements.depth.measurement;
                                param.depthmeasurementunitcode = item.tradeItemMeasurementsModule.tradeItemMeasurements.depth.measurementUnitCode;
                            }

                            if (item.tradeItemMeasurementsModule.tradeItemMeasurements.height != null)
                            {
                                param.height = item.tradeItemMeasurementsModule.tradeItemMeasurements.height.measurement;
                                param.heightmeasurementunitcode = item.tradeItemMeasurementsModule.tradeItemMeasurements.height.measurementUnitCode;
                            }

                            if (item.tradeItemMeasurementsModule.tradeItemMeasurements.netContent != null)
                            {
                                param.netcontent = item.tradeItemMeasurementsModule.tradeItemMeasurements.netContent.measurement;
                                param.netcontentmeasurementunitcode = item.tradeItemMeasurementsModule.tradeItemMeasurements.netContent.measurementUnitCode;
                            }

                            if (item.tradeItemMeasurementsModule.tradeItemMeasurements.width != null)
                            {
                                param.width = item.tradeItemMeasurementsModule.tradeItemMeasurements.width.measurement;
                                param.widthmeasurementunitcode = item.tradeItemMeasurementsModule.tradeItemMeasurements.width.measurementUnitCode;
                            }

                            if (item.tradeItemMeasurementsModule.tradeItemMeasurements.grossWeight != null)
                            {
                                param.grossweight = item.tradeItemMeasurementsModule.tradeItemMeasurements.grossWeight.measurement;
                                param.grossweightmeasurementunitcode = item.tradeItemMeasurementsModule.tradeItemMeasurements.grossWeight.measurementUnitCode;
                            }

                            if (item.tradeItemMeasurementsModule.tradeItemMeasurements.netWeight != null)
                            {
                                param.netweight = item.tradeItemMeasurementsModule.tradeItemMeasurements.netWeight.measurement;
                                param.netweightmeasurementunitcode = item.tradeItemMeasurementsModule.tradeItemMeasurements.netWeight.measurementUnitCode;
                            }

                        }

                        if (item.tradeItemTemperatureInformationModule != null && item.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation != null)
                        {
                            if (item.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.minimumTemperature != null)
                            {
                                param.storagehandlingtempminimumuom = item.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.minimumTemperature.temperatureMeasurement;
                            }

                            if (item.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.maximumTemperature != null)
                            {
                                param.storagehandlingtemperaturemaximum = item.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.maximumTemperature.temperatureMeasurement;
                                param.storagehandlingtemperaturemaximumunitofmeasure = item.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.maximumTemperature.temperatureMeasurementUnitCode;
                            }

                        }

                        if (item.dangerousSubstanceInformationModule != null && item.dangerousSubstanceInformationModule.dangerousSubstanceInformation != null && item.dangerousSubstanceInformationModule.dangerousSubstanceInformation.dangerousSubstanceProperties != null)
                        {
                            param.isdangeroussubstanceindicated = item.dangerousSubstanceInformationModule.dangerousSubstanceInformation.dangerousSubstanceProperties.isDangerousSubstance;
                        }

                        param.gtin = item.GTIN;
                        param.gln = GLN;

                        produtoBO.InsertCNPLogistica(param);

                    }

                }


            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
            }            
        }

        [WebMethod(EnableSession = false, Description = "<b>Método de busca de Impostos</b><br/>" +
           "<b><i>GTIN</i></b>: Lista de GTINS a serem buscados<br/>" +
           "<b><i>GLN</i></b>: GLN do Associado<br/>")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public LogisticaModule GetCNPLogistica(List<Int64> GTIN, string GLN)
        {
            LogisticaModule retorno = new LogisticaModule();
            try
            {
                ResponseHeader = new ProdutoCoreResponseHeader();

                if (GLN == string.Empty)
                {
                    ResponseHeader.returnCode = -1; // GLN Não Informado
                    //return null;
                }

                string listaprodutos = string.Empty;
                if (GTIN != null && GTIN.Count > 0)
                {
                    listaprodutos = string.Join(",", GTIN);

                    if (listaprodutos == string.Empty)
                    {
                        ResponseHeader.returnCode = -2;
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -2; // Produto(s) não informado(s)                     
                }


                ProdutoBO produtoBO = new ProdutoBO();

                long GLNConvertido;
                if (long.TryParse(GLN, out GLNConvertido))
                {
                    if (!produtoBO.ValidaGTIN(GLN))
                    {
                        ResponseHeader.returnCode = -6; // GLN Inválido                       
                    }
                }
                else
                {
                    ResponseHeader.returnCode = -6; // GLN Inválido                    
                }


                if (ResponseHeader.returnCode == 0)
                {
                    List<dynamic> produtosLogistica = produtoBO.GetProdutoLogistica(listaprodutos, GLN);
                    if (produtosLogistica.Count > 0)
                    {
                        retorno.catalogueItemNotificationModule = new CatalogueItemNotificationModule();
                        retorno.catalogueItemNotificationModule.tradeItem = new TradeItem();
                        retorno.catalogueItemNotificationModule.tradeItem.tradeItemUnitDescriptorCode = produtosLogistica[0].tradeitemunitdescriptorcode;

                        retorno.tradeItemHierarchyModule = new TradeItemHierarchyModule();
                        retorno.tradeItemHierarchyModule.tradeItemHierarchy = new TradeItemHierarchy();
                        retorno.tradeItemHierarchyModule.tradeItemHierarchy.totalQuantityOfNextLowerLevelTradeItem = produtosLogistica[0].totalquantityofnextlowerleveltradeitem.ToString();
                        retorno.tradeItemHierarchyModule.tradeItemHierarchy.quantityOfCompleteLayersContainedInATradeItem = produtosLogistica[0].quantityofcompletelayerscontainedinatradeitem.ToString();
                        retorno.tradeItemHierarchyModule.tradeItemHierarchy.quantityOfLayersPerPallet = produtosLogistica[0].quantityoflayersperpallet.ToString();
                        retorno.tradeItemHierarchyModule.tradeItemHierarchy.quantityOfTradeItemsContainedInACompleteLayer = produtosLogistica[0].quantityoftradeitemcontainedinacompletelayer.ToString();
                        retorno.tradeItemHierarchyModule.tradeItemHierarchy.quantityOfTradeItemsPerPalletLayer = produtosLogistica[0].quantityoftradeitemsperpalletlayer.ToString();


                        retorno.tradeItemHandlingModule = new TradeItemHandlingModule();
                        retorno.tradeItemHandlingModule.tradeItemHandlingInformation = new TradeItemHandlingInformation();
                        retorno.tradeItemHandlingModule.tradeItemHandlingInformation.tradeItemStacking = new TradeItemStacking();
                        retorno.tradeItemHandlingModule.tradeItemHandlingInformation.tradeItemStacking.stackingFactor = produtosLogistica[0].stackingfactor.ToString();


                        retorno.tradeItemMeasurementsModule = new TradeItemMeasurementsModule();
                        retorno.tradeItemMeasurementsModule.tradeItemMeasurements = new TradeItemMeasurements();

                        if (produtosLogistica[0].depth != null && produtosLogistica[0].depth.ToString() != string.Empty)
                        {
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.depth = new Measurement();
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.depth.measurement = produtosLogistica[0].depth;
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.depth.measurementUnitCode = produtosLogistica[0].depthmeasurementunitcode;
                        }

                        if (produtosLogistica[0].height != null && produtosLogistica[0].height.ToString() != string.Empty)
                        {
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.height = new Measurement();
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.height.measurement = produtosLogistica[0].height;
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.height.measurementUnitCode = produtosLogistica[0].heightmeasurementunitcode;
                        }

                        if (produtosLogistica[0].netcontent != null && produtosLogistica[0].netcontent.ToString() != string.Empty)
                        {
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.netContent = new Measurement();
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.netContent.measurement = produtosLogistica[0].netcontent;
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.netContent.measurementUnitCode = produtosLogistica[0].netcontentmeasurementunitcode;
                        }

                        if (produtosLogistica[0].width != null && produtosLogistica[0].width.ToString() != string.Empty)
                        {
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.width = new Measurement();
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.width.measurement = produtosLogistica[0].width;
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.width.measurementUnitCode = produtosLogistica[0].widthmeasurementunitcode;
                        }

                        if (produtosLogistica[0].grossweight != null && produtosLogistica[0].grossweight.ToString() != string.Empty)
                        {
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.grossWeight = new Measurement();
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.grossWeight.measurement = produtosLogistica[0].grossweight;
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.grossWeight.measurementUnitCode = produtosLogistica[0].grossweightmeasurementunitcode;
                        }

                        if (produtosLogistica[0].netweight != null && produtosLogistica[0].netweight.ToString() != string.Empty)
                        {
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.netWeight = new Measurement();
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.netWeight.measurement = produtosLogistica[0].netweight;
                            retorno.tradeItemMeasurementsModule.tradeItemMeasurements.netWeight.measurementUnitCode = produtosLogistica[0].netweightmeasurementunitcode;
                        }

                        retorno.tradeItemTemperatureInformationModule = new TradeItemTemperatureInformationModule();
                        retorno.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation = new TradeItemTemperatureInformation();

                        if (produtosLogistica[0].storagehandlingtempminimumuom != null && produtosLogistica[0].storagehandlingtempminimumuom.ToString() != string.Empty)
                        {
                            retorno.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.minimumTemperature = new TemperatureMeasurement();
                            retorno.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.minimumTemperature.temperatureMeasurement = decimal.Parse(produtosLogistica[0].storagehandlingtempminimumuom.ToString());
                            retorno.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.minimumTemperature.temperatureMeasurementUnitCode = produtosLogistica[0].storagehandlingtemperaturemaximumunitofmeasure;
                        }

                        if (produtosLogistica[0].storagehandlingtemperaturemaximum != null && produtosLogistica[0].storagehandlingtemperaturemaximum.ToString() != string.Empty)
                        {
                            retorno.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.maximumTemperature = new TemperatureMeasurement();
                            retorno.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.maximumTemperature.temperatureMeasurement = decimal.Parse(produtosLogistica[0].storagehandlingtemperaturemaximum.ToString());
                            retorno.tradeItemTemperatureInformationModule.tradeItemTemperatureInformation.maximumTemperature.temperatureMeasurementUnitCode = produtosLogistica[0].storagehandlingtemperaturemaximumunitofmeasure;
                        }

                        retorno.dangerousSubstanceInformationModule = new DangerousSubstanceInformationModule();
                        retorno.dangerousSubstanceInformationModule.dangerousSubstanceInformation = new DangerousSubstanceInformation();
                        retorno.dangerousSubstanceInformationModule.dangerousSubstanceInformation.dangerousSubstanceProperties = new DangerousSubstanceProperties();
                        retorno.dangerousSubstanceInformationModule.dangerousSubstanceInformation.dangerousSubstanceProperties.isDangerousSubstance = produtosLogistica[0].isdangeroussubstanceindicated.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                ResponseHeader.returnCode = -3; // Erro ao processar. 
            }
            return retorno;
        }

        #endregion
        
        private void MontaObjetoCore(string GLN, List<dynamic> bricks, List<dynamic> prefixos, long codigousuario, ref List<ProdutoCoreModel> produtos, ref dynamic tradeitem, ref List<dynamic> additionaltradeitemidentificationList, ref List<dynamic> nextLowerLevelList, int tipovalidacao)
        {

            LinguaBO linguaBo = new LinguaBO();
            string listaLinguas = linguaBo.BuscarLinguagensAtivas(null);
            List<dynamic> linguas = ((Newtonsoft.Json.Linq.JArray)JsonConvert.DeserializeObject(listaLinguas)).ToObject<List<dynamic>>();
            ProdutoBO produtoBO = new ProdutoBO();

            foreach (ProdutoCoreModel produto in produtos)
            {
                produto.Validate(tipovalidacao, bricks, prefixos);
                if (produto.Errors.Count() == 0)
                {
                    if ((produto.TradeItem.GTIN != null && produto.TradeItem.GTIN != string.Empty) && (!produtoBO.ValidaGTIN(produto.TradeItem.GTIN)))
                    {
                        produto.Errors.Add(new string[2] { "GTIN", "GTIN inválido." });
                    }
                    else
                    {                        

                        //TradeItem
                        if (tipovalidacao == 1)
                        {
                            tradeitem.codigoassociado = prefixos[0].codigoassociado;
                            tradeitem.codigousuariocriacao = codigousuario;
                            tradeitem.datainclusao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
                            tradeitem.codigostatusgtin = 1;


                            string prefixo = string.Empty;
                            if (produto.TradeItem.GTIN == null || produto.TradeItem.GTIN == string.Empty)
                            {
                                dynamic prefixovalido = produtoBO.PegarGTINValidoCore(produto.TradeItem.CodigoTipoGtin.ToString(), tradeitem.codigoassociado.ToString());
                                if (prefixovalido != null && prefixovalido[0] != null)
                                {
                                    prefixo = prefixovalido[0].numeroprefixo.ToString();

                                    int tamanhoGTIN = 0;
                                    if (produto.TradeItem.CodigoTipoGtin == 2)
                                    {
                                        tamanhoGTIN = 12;
                                    }
                                    else if (produto.TradeItem.CodigoTipoGtin == 3)
                                    {
                                        tamanhoGTIN = 13;
                                    }

                                    dynamic gtingerado = produtoBO.GeradorGTINCore(prefixo, tamanhoGTIN);
                                    if (gtingerado != null && gtingerado.globaltradeitemnumber != null)
                                    {
                                        produto.TradeItem.GTIN = gtingerado.globaltradeitemnumber;
                                    }
                                    else
                                    {
                                        produto.Errors.Add(new string[2] { "GTIN", "Quantidade de produtos máxima atingida" });
                                    }
                                }
                                else
                                {
                                    produto.Errors.Add(new string[2] { "Prefixo GTIN", "Nenhum prefixo encontrado, não é possível gerar um novo GTIN" });
                                }
                            }
                            else
                            {
                                foreach (var item in prefixos)
                                {
                                    if (produto.TradeItem.GTIN.Contains(item.numeroprefixo.ToString()))
                                        prefixo = item.numeroprefixo.ToString();
                                }
                            }

                            tradeitem.nrprefixo = prefixo;
                            tradeitem.coditem = produto.TradeItem.GTIN.Replace(prefixo, "").Substring(0, produto.TradeItem.GTIN.Replace(prefixo, "").Length - 1);

                            if (produto.TradeItem.CodigoTipoGtin == 4 && produto.TradeItem.GTIN.Length == 14)
                            {
                                tradeitem.variantelogistica = produto.TradeItem.GTIN.Substring(0, 1);
                            }
                            tradeitem.indicadorcompartilhadados = "1";

                            tradeitem.codigotipogtin = produto.TradeItem.CodigoTipoGtin;                            
                            tradeitem.informationProvider = GLN;
                        }
                        else
                        {
                            tradeitem.codigousuarioalteracao = codigousuario;
                            tradeitem.dataalteracao = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");

                            //validar tradeitem.codigostatusgtin = 1; //tradeitem.productdescription = produto.TradeItem.tradeItemDescription; //TradeItemClassification
                        }

                        if (produto.TradeItem.context != null)
                            tradeitem.contextidentification = produto.TradeItem.context.contextIdentification;

                        tradeitem.istradeitemabaseunit = produto.TradeItem.isTradeItemABaseUnit;
                        tradeitem.istradeitemaconsumerunit = produto.TradeItem.isTradeItemAConsumerUnit;
                        tradeitem.istradeitemadespatchunit = produto.TradeItem.isTradeItemADespatchUnit;
                        tradeitem.istradeitemaninvoiceunit = produto.TradeItem.isTradeItemAnInvoiceUnit;
                        tradeitem.istradeitemanorderableunit = produto.TradeItem.isTradeItemAnOrderableUnit;
                        tradeitem.istradeitemaservice = produto.TradeItem.isTradeItemAService;
                        tradeitem.istradeitemnonphysical = produto.TradeItem.isTradeItemNonphysical;

                        tradeitem.globaltradeitemnumber = produto.TradeItem.GTIN;
                        
                        if(produto.TradeItem.tradeItemDescription != null)
                            tradeitem.productdescription = produto.TradeItem.tradeItemDescription;
                        
                        if(produto.TradeItem.descriptionShort != null)
                            tradeitem.descriptionshort = produto.TradeItem.descriptionShort;
                        
                        if(produto.TradeItem.startAvailabilityDatetime != null)
                            tradeitem.startavailabilitydatetime = produto.TradeItem.startAvailabilityDatetime;

                        if (produto.TradeItem.Language != null && produto.TradeItem.Language != String.Empty)
                        {
                            var codLingua = from lingua in linguas
                                            where lingua.nome != null && lingua.nome.ToString().ToUpper() == produto.TradeItem.Language.ToUpper()
                                            select lingua.codigo;
                            if (codLingua.Count() > 0)
                            {
                                tradeitem.codigolingua = (int)codLingua.FirstOrDefault().Value;
                            }
                        }

                        if(produto.TradeItem.countryCode != null)
                            tradeitem.countrycode = produto.TradeItem.countryCode;
                        
                        if(produto.TradeItem.countryOfOrigin != null)
                            tradeitem.tradeitemcountryoforigin = produto.TradeItem.countryOfOrigin;
                        
                        if(produto.TradeItem.estado != null)
                            tradeitem.estado = produto.TradeItem.estado;

                        if (produto.TradeItem.additionalTradeItemIdentification != null)
                        {
                            foreach (AdditionalTradeItemIdentification item in produto.TradeItem.additionalTradeItemIdentification)
                            {
                                dynamic additionaltradeitemidentification = new ExpandoObject();
                                additionaltradeitemidentification.additionaltradeitemidentificationtypecode = item.additionalTradeItemIdentificationTypeCode;
                                additionaltradeitemidentification.additionaltradeitemidentificationvalue = item.additionalTradeItemIdentificationValue;
                                additionaltradeitemidentificationList.Add(additionaltradeitemidentification);
                            }
                        }

                        if (produto.TradeItem.nextLowerLevel != null)
                        {
                            foreach (ChildTradeItem item in produto.TradeItem.nextLowerLevel.childTradeItem)
                            {
                                dynamic nextLowerLevel = new ExpandoObject();
                                if (item.GTIN != null && item.quantity != null && item.GTIN != string.Empty)
                                {
                                    nextLowerLevel.gtin = item.GTIN;
                                    nextLowerLevel.tradeitemdescription = item.tradeItemDescription;
                                    nextLowerLevel.quantity = item.quantity;
                                    nextLowerLevelList.Add(nextLowerLevel);
                                }
                            }
                        }

                        if(produto.TradeItem.brandName != null)
                            tradeitem.brandname = produto.TradeItem.brandName;
                        
                        if(produto.TradeItem.additionalTradeItemDescription != null)
                            tradeitem.additionaltradeitemdescription = produto.TradeItem.additionalTradeItemDescription;

                        //TradeItemClassification
                        if (bricks != null)
                        {
                            IEnumerable<dynamic> produtosGPCQuery =
                                from brick in bricks
                                where brick.codebrick == produto.TradeItemClassification.tradeitemclassification.gpcCategoryCode
                                select brick;

                            if (produtosGPCQuery.Count() > 0)
                            {
                                dynamic brick = produtosGPCQuery.FirstOrDefault();
                                tradeitem.codebrick = brick.codebrick;
                                tradeitem.codeclass = brick.codeclass;
                                tradeitem.codefamily = brick.codefamily;
                                tradeitem.codesegment = brick.codesegment;
                            }
                        }
                    }
                }
            }
        }

    }
}