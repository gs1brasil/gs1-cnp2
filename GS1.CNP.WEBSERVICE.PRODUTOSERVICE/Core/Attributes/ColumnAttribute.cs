﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ColumnAttribute : Attribute
    {
        public string ColumnName { get; set; }

        public ColumnAttribute(string columnName)
        {
            this.ColumnName = columnName;
        }
    }
}