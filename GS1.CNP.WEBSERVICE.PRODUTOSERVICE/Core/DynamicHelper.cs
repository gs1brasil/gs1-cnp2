﻿using System;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;

public static class DynamicHelper
{
    private static readonly Type[] _writeTypes = new[] { 
        typeof(string),
        typeof(DateTime),
        typeof(Enum),
        typeof(decimal),
        typeof(Guid) 
    };

    public static bool IsSimpleType(this Type type)
    {
        return type.IsPrimitive || _writeTypes.Contains(type);
    }

    public static List<XElement> ToXml(this object input)
    {
        return input.ToXml(null);
    }

    public static List<XElement> ToXml(this object input, string element)
    {
        if (input == null)
        {
            return null;
        }

        if (String.IsNullOrEmpty(element))
        {
            element = "object";
        }

        element = XmlConvert.EncodeName(element);

        //var ret = new XElement(element);
        var ret = new List<XElement>();

        if (input != null)
        {
            var type = input.GetType();

            if (type.Name.ToLower().Equals("expandoobject"))
            {
                var props = input as IDictionary<string, object>;
                var elements = from prop in props
                               let name = new XElement(prop.Key)
                               let val = prop.Value is Array ? "array" : prop.Value
                               //let value = prop.Value is Array ? GetArrayDynamicElement(prop, (Array)prop.Value) : (!(prop.Value is Array) ? new XElement(name, val) : val.ToXml(name))
                               //where value != null
                               select val;

                ret.Add(new XElement(element, elements));
            }
            else
            {
                var props = type.GetProperties();
                var elements = from prop in props
                               let name = XmlConvert.EncodeName(prop.Name)
                               let val = prop.PropertyType.IsArray ? "array" : prop.GetValue(input, null)
                               //let value = prop.PropertyType.IsArray ? GetArrayElement(prop, (Array)prop.GetValue(input, null)) : (prop.PropertyType.IsSimpleType() ? new XElement(name, val) : val.ToXml(name))
                               //where value != null
                               select val;
                ret.Add(new XElement(element, elements));
            }
        }
        return ret;
    }
    //private static XElement GetArrayDynamicElement(KeyValuePair<string,object> info, Array input)
    //{
    //    var name = XmlConvert.EncodeName(info.Key);
    //    XElement rootElement = new XElement(name);
    //    var arrayCount = input.GetLength(0);
    //    for (int i = 0; i < arrayCount; i++)
    //    {
    //        var val = input.GetValue(i);
    //        XElement childElement = val.GetType().IsSimpleType() ? new XElement(name + "Child", val) : val.ToXml();
    //        rootElement.Add(childElement);
    //    }
    //    return rootElement;
    //}
    //private static XElement GetArrayElement(PropertyInfo info, Array input)
    //{
    //    var name = XmlConvert.EncodeName(info.Name);
    //    XElement rootElement = new XElement(name);
    //    var arrayCount = input.GetLength(0);
    //    for (int i = 0; i < arrayCount; i++)
    //    {
    //        var val = input.GetValue(i);
    //        XElement childElement = val.GetType().IsSimpleType() ? new XElement(name + "Child", val) : val.ToXml();
    //        rootElement.Add(childElement);
    //    }
    //    return rootElement;
    //}
}
