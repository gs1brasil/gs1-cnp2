﻿namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Exceptions
{
    public class GTINInvalidoException : ProdutoServiceException
    {
        public GTINInvalidoException() : base(-6, "O GTIN informado não é válido.") { }
    }
}