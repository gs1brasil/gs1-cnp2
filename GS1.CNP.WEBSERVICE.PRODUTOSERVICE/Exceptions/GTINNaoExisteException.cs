﻿namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Exceptions
{
    public class GTINNaoExisteException : ProdutoServiceException
    {
        public GTINNaoExisteException()
            : base(-7, "O GTIN informado não é existe.")
        { }
    }
}