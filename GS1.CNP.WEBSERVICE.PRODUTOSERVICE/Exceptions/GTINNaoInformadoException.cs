﻿namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Exceptions
{
    /// <summary>
    /// Exceção que indica que o GTIN não foi informado.
    /// </summary>
    public class GTINNaoInformadoException : ProdutoServiceException
    {
        public GTINNaoInformadoException()
            : base(-5, "O GTIN é obrigatório e deve ser informado.")
        { }
    }
}