﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Exceptions
{

    /// <summary>
    /// Representa um erro do serviço ProdutoService
    /// </summary>
    public abstract class ProdutoServiceException : Exception
    {
        /// <summary>
        /// Código do erro
        /// </summary>
        public int codigo { get; set; }

        /// <summary>
        /// Descrição do erro
        /// </summary>
        public string descricao { get; set; }

        public ProdutoServiceException(int cod, string desc)
            : base(string.Format("{0} - {1}", cod, desc))
        {
            this.codigo = cod;
            this.descricao = desc;
        }
    }
}