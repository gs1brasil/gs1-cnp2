﻿using GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Core.Attributes;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Model
{
    public class AgenciaReguladora : IModel
    {
        [Column("identificacaoalternativa")]
        public string alternateItemIdentificationId { get; set; }
        [Column("agenciareguladora")]
        public int CodigoAgencia { get; set; }
    }
}
