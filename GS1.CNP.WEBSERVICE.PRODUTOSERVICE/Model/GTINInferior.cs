﻿using GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Core.Attributes;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Model
{
    public class GTINInferior : IModel
    {
        [Column("gtininferior")]
        public long CodigoProdutoInferior { get; set; }
        [Column("quantidadeinferior")]
        public int Quantidade { get; set; }
    }
}
