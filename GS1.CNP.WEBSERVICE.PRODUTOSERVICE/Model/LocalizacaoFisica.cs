﻿using System.Collections.Generic;
using GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Core.Attributes;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Model
{
    public class LocalizacaoFisica : IModel
    {
        [Column("nomelocalizacao")]
        public string NomeLocalizacaoContrato { get; set; }

        public int? PapelFuncao { get; set; }

        public string StatusGLN { get; set; }

        [Column("gln")]
        public long? NumeroIdentificacaoGLN { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public string Pais { get; set; }

        [Column("codigopostal")]
        public string CEP { get; set; }

        public string Endereco { get; set; }

        public string Numero { get; set; }

        public string Bairro { get; set; }

        public string Complemento { get; set; }

        public string Estado { get; set; }

        public string Cidade { get; set; }

        public string Observacoes { get; set; }

        public List<string[]> Errors { get; set; }
    }
}