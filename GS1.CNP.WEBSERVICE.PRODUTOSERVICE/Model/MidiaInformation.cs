﻿using System;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Model
{
    [Serializable]
    public class MidiaInformation : IModel
    {
        public string itemFileLink { get; set; }
        public string fileFormatName { get; set; }
        public string uniformResourceIdentifier { get; set; }
    }
}