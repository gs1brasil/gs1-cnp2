﻿using System;
using GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Core.Attributes;
using System.Collections.Generic;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Model
{
    [Serializable]
    public class Produto : IModel
    {
        public bool? IndicadorGDSN { get; set; }

        [Column("statuscodigo")]
        public int? CodigoStatusGTIN { get; set; }

        [Column("gtinorigem")]
        public long? CodigoProdutoOrigem { get; set; }

        [Column("quantidadeitensporcaixa")]
        public string totalquantityofnextlowerleveltradeitem { get; set; }

        [Column("gtin")]
        public long? globalTradeItemNumber { get; set; }

        [Column("marcaproduto")]
        public string brandName { get; set; }

        [Column("numeromodelo")]
        public string modelnumber { get; set; }

        [Column("descricaoproduto")]
        public string productDescription { get; set; }

        [Column("datainicialdisponibilidade")]
        public DateTime? startavailabilitydatetime { get; set; }

        [Column("datafinaldisponibilidade")]
        public DateTime? endavailabilitydatetime { get; set; }

        [Column("ncm")]
        public string importClassificationValue { get; set; }

        [Column("segmento")]
        public string codesegment { get; set; }

        [Column("familia")]
        public string codefamily { get; set; }

        [Column("classe")]
        public string codeclass { get; set; }

        [Column("bloco")]
        public string codebrick { get; set; }

        public AgenciaReguladora[] AgenciasReguladoras { get; set; }

        [Column("paismercadodestino")]
        public int? countrycode { get; set; }

        [Column("paisorigem")]
        public int? tradeItemCountryOfOrigin { get; set; }

        public string Estado { get; set; }

        [Column("altura")]
        public decimal? height { get; set; }

        [Column("alturaunidademedida")]
        public string heightmeasurementunitcode { get; set; }

        [Column("largura")]
        public decimal? Width { get; set; }

        [Column("larguraunidademedida")]
        public string widthmeasurementunitcode { get; set; }

        [Column("profundidade")]
        public decimal? depth { get; set; }

        [Column("profundidadeunidademedida")]
        public string depthmeasurementunitcode { get; set; }

        [Column("conteudoliquido")]
        public decimal? netcontent { get; set; }

        [Column("conteudoliquidounidademedida")]
        public string netcontentmeasurementunitcode { get; set; }

        [Column("pesobruto")]
        public decimal? grossWeight { get; set; }

        [Column("pesobrutounidademedida")]
        public string grossweightmeasurementunitcode { get; set; }

        [Column("pesoliquido")]
        public decimal? netWeight { get; set; }

        [Column("pesoliquidounidademedida")]
        public string netweightmeasurementunitcode { get; set; }

        [Column("tempovidautil")]
        public int? minimumtradeitemlifespanfromtimeofproduction { get; set; }

        [Column("compartilhadados")]
        public string indicadorcompartilhadados { get; set; }

        [Column("observacao")]
        public string Observacoes { get; set; }

        [Column("tipoproduto")]
        public int? CodigoTipoProduto { get; set; }

        [Column("tipopallet")]
        public string pallettypecode { get; set; }

        [Column("fatorempilhamento")]
        public int? stackingfactor { get; set; }

        [Column("quantidadecamadaspallet")]
        public int? quantityoflayersperpallet { get; set; }

        [Column("quantidadeitenscomerciaiscamada")]
        public int? quantityoftradeitemsperpalletlayer { get; set; }

        [Column("quantidadeitenscomerciaiscompleta")]
        public int? quantityoftradeitemcontainedinacompletelayer { get; set; }

        [Column("quantidadecamadascompleta")]
        public int? quantityofcompletelayerscontainedinatradeitem { get; set; }
        
        public GTINInferior[] GTINInferiores { get; set; }

        [Column("ipiPerc")]
        public decimal? ipiPerc { get; set; }
        
        public URL[] Url { get; set; }

        [Column("indicadoritemcomercialliberadocompra")]
        public string istradeitemanorderableunit { get; set; }

        [Column("indicadorunidadedespacho")]
        public string istradeitemadespatchunit { get; set; }

        [Column("indicadorunidadebasicavenda")]
        public string istradeitemabaseunit { get; set; }

        [Column("indicadorunidadefaturamento")]
        public string istradeitemaninvoiceunit { get; set; }

        [Column("indicadoritemcomercialunidadeconsumo")]
        public string istradeitemaconsumerunit { get; set; }

        [Column("itemcomercialmodelo")]
        public string istradeitemamodel { get; set; }

        [Column("unidademedidapedido")]
        public string ordersizingfactor { get; set; }

        [Column("multiploquantidadepedido")]
        public int? orderquantitymultiple { get; set; }

        [Column("quantidademinimapedido")]
        public int? orderquantityminimum { get; set; }

        [Column("tipoembalagem")]
        public string packagingtypecode { get; set; }

        [Column("temperaturaminimaarmazenamento")]
        public decimal? deliverytodistributioncentertemperatureminimum { get; set; }

        [Column("unidademedidatemperaturaminimaarmazenamento")]
        public string storagehandlingtempminimumuom { get; set; }

        [Column("temperaturamaximaarmazenamento")]
        public decimal? storagehandlingtemperaturemaximum { get; set; }

        [Column("unidademedidatemperaturamaximaarmazenamento")]
        public string storagehandlingtemperaturemaximumunitofmeasure { get; set; }

        [Column("indicadormercadoriasperigosas")]
        public string isdangeroussubstanceindicated { get; set; }

        public string Tamanho { get; set; }

        public string Cor { get; set; }

        public List<string[]> Errors { get; set; }

        //GDSN 3.1

        public AllergenInformationModuleType allergenInformationModule { get; set; }        

        public CatalogueItemNotificationType catalogueItemNotification { get; set; }

        public DeliveryPurchasingInformationType deliveryPurchasingInformation { get; set; }

        public List<FoodAndBeverageIngredientType> foodAndBeverageIngredient { get; set; }

        public FoodAndBeveragePreparationServingModuleType foodAndBeveragePreparationServingModule { get; set; }        

        public List<NutrientHeaderType> nutrientHeader { get; set; }

        public List<PlaceOfItemActivityModuleType> placeOfItemActivityModule { get; set; }

        public PriceSynchronisationDocumentType priceSynchronisationDocument { get; set; }

        public RegistryPartyDataDumpType registryPartyDataDump { get; set; }

        public List<RegulatedTradeItemModuleType> regulatedTradeItemModule { get; set; }

        //public SalesInformationModuleType salesInformationModule { get; set; } - Não vai ser utilizado

        //public TradeItemDataCarrierAndIdentificationModuleType tradeItemDataCarrierAndIdentificationModule { get; set; } - Não vai ser utilizado

        public TradeItemHandlingModuleType tradeItemHandlingModule { get; set; }

        public TradeItemHierarchyModuleType tradeItemHierarchyModule { get; set; }

        public TradeItemLifespanModuleType tradeItemLifespanModule { get; set; }

        public TradeItemMeasurementsModuleType tradeItemMeasurementsModule { get; set; }

        public TradeItemSizeModuleType tradeItemSizeModule { get; set; } 

        public TradeItemTemperatureInformationModuleType tradeItemTemperatureInformationModule { get; set; }

        public VariableTradeItemInformationModuleType variableTradeItemInformationModule { get; set; } 
    }
}