﻿using GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Core.Attributes;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Model
{
    public class Usuario : IModel
    {
        [Column("email")]
        public string usuario { get; set; }

        public string senha { get; set; }

        [Column("cpfcnpj")]
        public string cnpj { get; set; }

        [Column("gln")]
        public string glnInformativo { get; set; }
    }
}