﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using GS1.CNP.BLL;
using GS1.CNP.BLL.Model;
using GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Model;
using GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Core;
using System.Xml.Serialization;
using System.Linq;
using System.Configuration;
using GS1.CNP.BLL.Core;
using GS1.CNP.DAL;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using GS1.CNP.WEBSERVICE.PRODUTOSERVICE.Exceptions;
using System.IO;
using System.Xml;

namespace GS1.CNP.WEBSERVICE.PRODUTOSERVICE
{
    [ScriptService]
    public class ProdutoService : System.Web.Services.WebService
    {
        #region "Retornos genericos"

        private Retorno getRetornoErroGenerico()
        {
            return new Retorno(-99, "Erro ao realizar operação.");
        }

        private Retorno getRetornoSucessoGenerico()
        {
            return new Retorno(0, "Operação realizada com sucesso.");
        }

        #endregion

        #region "Validacoes"

        /// <summary>
        /// Valida o GTIN
        /// </summary>
        /// <param name="GTIN">Global Trade Item Number - GTIN a ser validado</param>
        /// <exception cref="ProdutoServiceException">Lança alguma subclasse de ProdutoServiceException caso o GTIN seja nulo, inválido ou não exista.</exception>
        private void validarGTIN(long? GTIN)
        {
            ProdutoBO produtoBO = new ProdutoBO();

            if (GTIN == null || GTIN == 0)
                throw new GTINNaoInformadoException();

            if (!produtoBO.ValidaGTIN(GTIN.ToString()))
                throw new GTINInvalidoException();

            if (!produtoBO.VerificaExistenciaGTIN(GTIN.ToString()))
                throw new GTINNaoExisteException();
        }

        private dynamic validarGTINModulo(long GTIN, string modulo)
        {
            ProdutoBO produtoBO = new ProdutoBO();

            if (GTIN == null || GTIN == 0)
                throw new GTINNaoInformadoException();

            if (!produtoBO.ValidaGTIN(GTIN.ToString()))
                throw new GTINInvalidoException();

            dynamic produto = JsonConvert.DeserializeObject(produtoBO.VerificaExistenciaGTINModulo(GTIN, modulo).ToString());

            if (produto == null)
                throw new GTINNaoExisteException();
            
            return produto;
        }


        #endregion

        /// <summary>
        /// Método de cadastro de GTIN's
        /// </summary>
        /// <param name="produtos">Lista de Produtos</param>
        /// <param name="tipoGTIN">Tipo do GTIN:
        /// Valores aceitos:
        ///     * 1: UCC;
        ///     * 2: GTIN-13;
        ///     * 3: GTIN-14;
        /// </param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de GTIN</b><br/>" +
            "<b><i>cadAssociado</i></b>: CAD do Associado<br/>" +
            "<b><i>produtos</i></b>: Lista de Produtos<br/>" +
            "<b><i>tipoGTIN</i></b>: Tipo do GTIN:<br/>" +
            "<b><i>Valores aceitos</i></b>:<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;* 1: UCC;<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;* 2: GTIN-13;<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;* 3: GTIN-14;")]
        public Retorno InsertGTIN(string cadAssociado, List<Produto> produtos, int tipoGTIN)
        {
            try
            {                
                if (string.IsNullOrEmpty(cadAssociado))
                    return new Retorno(-5, "O CAD do associado é obrigatório e deve ser informado.");

                Login(cadAssociado);

                Associado associado = HttpContext.Current.Session["ASSOCIADO_SELECIONADO"] as Associado;

                if (associado != null && associado.ativado)
                {
                    bool? padraoGDSN = null;

                    if (produtos.Count == 0)
                        return new Retorno(-1, "A lista de produtos é obrigatória e deve ser informada.");

                    if (tipoGTIN != 1 && tipoGTIN != 2 && tipoGTIN != 3)
                        return new Retorno(-2, "O tipo de registro (UCC, GTIN-13 ou GTIN-14) é obrigatório e deve ser informado.");

                    using (ImportarProdutosBO bo = new ImportarProdutosBO())
                    {
                        dynamic[] lista = new ExpandoObject[produtos.Count];

                        for (int i = 0; i < produtos.Count; i++)
                        {
                            if (produtos[i] != null) lista[i] = ConvertHelper.ModelToDynamic(produtos[i]);
                            if (padraoGDSN == null || !padraoGDSN.HasValue) padraoGDSN = produtos[i].IndicadorGDSN;
                        }

                        dynamic param = new ExpandoObject();
                        int tipoArquivo = bo.RecuperarTipoArquivoPorID(tipoGTIN);

                        param.campos = new ExpandoObject();
                        param.campos.associado = associado.codigo;
                        param.campos.lista = lista;
                        param.campos.tipo = 3;
                        param.campos.padraoGDSN = (padraoGDSN != null && padraoGDSN.HasValue && padraoGDSN.Value ? 1 : 2);
                        param.campos.tipoArquivo = tipoArquivo;
                        param.campos.arquivo = "webservice";
                        param.campos.tipoAcao = 1;

                        dynamic resultadoValidacao = bo.Validar(param);

                        if (resultadoValidacao != null && !resultadoValidacao.existeErro)
                        {
                            object retorno;

                            param.campos.tipoArquivo = tipoGTIN;

                            if (associado.aprovacao)
                                retorno = bo.ImportarProdutosParaAprovacao(param);
                            else
                                retorno = bo.Importar(param);

                            if (retorno != null)
                            {
                                if (!associado.aprovacao)
                                    return new Retorno(1, "GTIN criado com sucesso.");
                                else
                                    return new Retorno(2, "GTIN cadastrado para aprovação com sucesso.");
                            }
                        }
                        else
                        {
                            if (resultadoValidacao != null && resultadoValidacao.lista != null)
                            {
                                var listaRetorno = new List<Produto>();
                                IOrderedEnumerable<dynamic> orderedList = resultadoValidacao.lista as IOrderedEnumerable<dynamic>;
                                if (orderedList.Count() > 0)
                                {
                                    foreach (dynamic res in orderedList)
                                    {
                                        Produto model = ConvertHelper.DynamicToModel<Produto>(res, tipoArquivo * 10 + param.campos.padraoGDSN);
                                        if (model != null && (res as IDictionary<string, object>).ContainsKey("errors") && res.errors != null)
                                        {
                                            model.Errors = new List<string[]>();
                                            foreach (KeyValuePair<string, string> kv in res.errors as Dictionary<string, string>)
                                                model.Errors.Add(new string[] { kv.Key, kv.Value });
                                        }
                                        listaRetorno.Add(model);
                                    }
                                }
                                return new Retorno(-3, "Não foi possível gravar o GTIN. Erros encontrados.", listaRetorno);
                            }
                            else
                                return new Retorno(-3, "Não foi possível gravar o GTIN. Erros encontrados.", resultadoValidacao);
                        }
                    }
                }
                else
                    throw new Exception("Acesso negado. Verifique as informações de acesso.");

                return null;
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true, Description = "<b>Método de atualização de GTIN</b><br/>" +
            "<b><i>cadAssociado</i></b>: CAD do Associado<br/>" +
            "<b><i>produtos</i></b>: Lista de Produtos<br/>" +
            "<b><i>tipoGTIN</i></b>: Tipo do GTIN:<br/>" +
            "<b><i>Valores aceitos</i></b>:<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;* 1: UCC;<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;* 2: GTIN-13;<br/>" +
            "&nbsp;&nbsp;&nbsp;&nbsp;* 3: GTIN-14;")]
        public object UpdateGTIN(string cadAssociado, List<Produto> produtos, int tipoGTIN)
        {
            try
            {
                if (string.IsNullOrEmpty(cadAssociado))
                    return new Retorno(-5, "O CAD do associado é obrigatório e deve ser informado.");

                Login(cadAssociado);

                Associado associado = HttpContext.Current.Session["ASSOCIADO_SELECIONADO"] as Associado;

                if (associado != null && associado.ativado)
                {
                    bool? padraoGDSN = null;

                    if (associado.indicadorcnaerestritivo.Equals("1"))
                        return new Retorno(-4, "Não é possível alterar GTIN da área de saúde!");

                    if (produtos.Count == 0)
                        return new Retorno(-1, "A lista de produtos é obrigatória e deve ser informada.");

                    if (tipoGTIN != 1 && tipoGTIN != 2 && tipoGTIN != 3)
                        return new Retorno(-2, "O tipo de registro (UCC, GTIN-13 ou GTIN-14) é obrigatório e deve ser informado.");

                    using (ImportarProdutosBO bo = new ImportarProdutosBO())
                    {
                        dynamic[] lista = new ExpandoObject[produtos.Count];

                        for (int i = 0; i < produtos.Count; i++)
                        {
                            if (produtos[i] != null) lista[i] = ConvertHelper.ModelToDynamic(produtos[i]);
                            if (padraoGDSN == null || !padraoGDSN.HasValue) padraoGDSN = produtos[i].IndicadorGDSN;
                        }

                        dynamic param = new ExpandoObject();
                        int tipoArquivo = bo.RecuperarTipoArquivoPorID(tipoGTIN);

                        param.campos = new ExpandoObject();
                        param.campos.associado = associado.codigo;
                        param.campos.lista = lista;
                        param.campos.tipo = 3;
                        param.campos.padraoGDSN = (padraoGDSN != null && padraoGDSN.HasValue && padraoGDSN.Value ? 1 : 2);
                        param.campos.tipoArquivo = tipoArquivo;
                        param.campos.arquivo = "webservice";
                        param.campos.tipoAcao = 2;

                        dynamic resultadoValidacao = bo.ValidarAtualizacao(param, false);

                        if (resultadoValidacao != null && !resultadoValidacao.existeErro)
                        {
                            object retorno;

                            if (associado.aprovacao)
                                retorno = bo.ImportarProdutosParaAprovacao(param);
                            else
                                retorno = bo.ImportarAtualizacao(param);


                            if (retorno != null)
                            {
                                if (!associado.aprovacao)
                                    return new Retorno(1, "GTIN atualizado com sucesso.");
                                else
                                    return new Retorno(2, "GTIN cadastrado para aprovação com sucesso.");
                            }
                        }
                        else
                        {
                            if (resultadoValidacao != null && resultadoValidacao.lista != null)
                            {
                                var listaRetorno = new List<Produto>();
                                IOrderedEnumerable<dynamic> orderedList = resultadoValidacao.lista as IOrderedEnumerable<dynamic>;
                                if (orderedList.Count() > 0)
                                {
                                    foreach (dynamic res in orderedList)
                                    {
                                        Produto model = ConvertHelper.DynamicToModel<Produto>(res, tipoArquivo * 10 + param.campos.padraoGDSN);
                                        if (model != null && (res as IDictionary<string, object>).ContainsKey("errors") && res.errors != null)
                                        {
                                            model.Errors = new List<string[]>();
                                            foreach (KeyValuePair<string, string> kv in res.errors as Dictionary<string, string>)
                                                model.Errors.Add(new string[] { kv.Key, kv.Value });
                                        }
                                        listaRetorno.Add(model);
                                    }
                                }
                                return new Retorno(-3, "Não foi possível gravar o GTIN. Erros encontrados.", listaRetorno);
                            }
                            else
                                return new Retorno(-3, "Não foi possível gravar o GTIN. Erros encontrados.", resultadoValidacao);
                        }
                    }
                }
                else
                    throw new Exception("Acesso negado. Verifique as informações de acesso.");

                return null;
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                throw e;
            }
        }

        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de GLN</b><br/>" +
            "<b><i>cadAssociado</i></b>: CAD do Associado<br/>" +
            "<b><i>listaLocalizacaoFisica</i></b>: Lista de Localização Física")]
        public object InsertGLN(string cadAssociado, List<Model.LocalizacaoFisica> listaLocalizacaoFisica)
        {
            try
            {
                if (string.IsNullOrEmpty(cadAssociado))
                    return new Retorno(-5, "O CAD do associado é obrigatório e deve ser informado.");

                Login(cadAssociado);

                Associado associado = HttpContext.Current.Session["ASSOCIADO_SELECIONADO"] as Associado;

                if (associado != null && associado.ativado)
                {
                    bool? padraoGDSN = null;

                    if (listaLocalizacaoFisica.Count == 0)
                        return new Retorno(-1, "A lista de localizações físicas é obrigatória e deve ser informada.");

                    using (ImportarProdutosBO bo = new ImportarProdutosBO())
                    {
                        dynamic[] lista = new ExpandoObject[listaLocalizacaoFisica.Count];

                        for (int i = 0; i < listaLocalizacaoFisica.Count; i++)
                        {
                            if (listaLocalizacaoFisica[i] != null)
                                lista[i] = ConvertHelper.ModelToDynamic(listaLocalizacaoFisica[i]);
                        }

                        dynamic param = new ExpandoObject();

                        param.campos = new ExpandoObject();
                        param.campos.associado = associado.codigo;
                        param.campos.lista = lista;
                        param.campos.tipo = 3;
                        param.campos.padraoGDSN = (padraoGDSN != null && padraoGDSN.HasValue && padraoGDSN.Value ? 1 : 2);
                        param.campos.tipoArquivo = 0;
                        param.campos.arquivo = "webservice";
                        param.campos.tipoAcao = 1;

                        dynamic resultadoValidacao = bo.Validar(param);

                        if (resultadoValidacao != null && !resultadoValidacao.existeErro)
                        {
                            object retorno;

                            if (associado.aprovacao)
                                retorno = bo.ImportarLocalizacaoFisicaParaAprovacao(param);
                            else
                                retorno = bo.Importar(param);

                            if (retorno != null)
                            {
                                if (!associado.aprovacao)
                                    return new Retorno(1, "GLN criado com sucesso.");
                                else
                                    return new Retorno(2, "GLN cadastrado para aprovação com sucesso.");
                            }
                        }
                        else
                        {
                            if (resultadoValidacao != null && resultadoValidacao.lista != null)
                            {
                                var listaRetorno = new List<Model.LocalizacaoFisica>();
                                IOrderedEnumerable<dynamic> orderedList = resultadoValidacao.lista as IOrderedEnumerable<dynamic>;
                                if (orderedList.Count() > 0)
                                {
                                    foreach (dynamic res in orderedList)
                                    {
                                        Model.LocalizacaoFisica model = ConvertHelper.DynamicToModel<Model.LocalizacaoFisica>(res, 0);
                                        if (model != null && (res as IDictionary<string, object>).ContainsKey("errors") && res.errors != null)
                                        {
                                            model.Errors = new List<string[]>();
                                            foreach (KeyValuePair<string, string> kv in res.errors as Dictionary<string, string>)
                                                model.Errors.Add(new string[] { kv.Key, kv.Value });
                                        }
                                        listaRetorno.Add(model);
                                    }
                                }
                                return new Retorno(-3, "Não foi possível gravar o GLN. Erros encontrados.", listaRetorno);
                            }
                            else
                                return new Retorno(-3, "Não foi possível gravar o GLN. Erros encontrados.", resultadoValidacao);
                        }
                    }
                }
                else
                    throw new Exception("Acesso negado. Verifique as informações de acesso.");

                return null;
            }
            catch (Exception e)
            {
                Log.WriteErrorWS(e.Message, e);
                throw e;
            }
        }

        private void Login(string cadAssociado)
        {
            LoginBO bo = new LoginBO();
            dynamic paramLogin = new ExpandoObject();
            paramLogin.where = new ExpandoObject();
            paramLogin.where.login = ConfigurationManager.AppSettings["login"];
            paramLogin.where.senha = ConfigurationManager.AppSettings["senha"];
            paramLogin.where.cpfcnpj = ConfigurationManager.AppSettings["cpfcnpj"];
            paramLogin.where.gln = ConfigurationManager.AppSettings["gln"];
            paramLogin.where.response = "";

            bo.Logar(paramLogin);
            Login usuario = HttpContext.Current.Session["USUARIO_LOGADO"] as Login;

            DadosDoAssociadoBO associadoBO = new DadosDoAssociadoBO();
            Associado associado = associadoBO.BuscarAssociadoPorCAD(cadAssociado);

            HttpContext.Current.Session.Add("ASSOCIADOS_USUARIO_LOGADO", associado);
            HttpContext.Current.Session.Add("ASSOCIADO_SELECIONADO", associado);
        }

        /// <summary>
        /// Método de cadastro de alérgenos
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="alergeno">Informação de alérgeno do produto</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Alérgenos</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>alergeno</i></b>: Informação de alérgeno do produto<br/>")]
        public Retorno InsertAlergeno(long GTIN, AllergenInformationModuleType alergeno)
        {
            try
            {
                ProdutoBO produtoBO = new ProdutoBO();

                dynamic parametro = new ExpandoObject();
                parametro.campos = new ExpandoObject();
                parametro.campos.globaltradeitemnumber = GTIN;

                if (alergeno == null)
                    return new Retorno(-5, "As informações de alérgeno são obrigatórias e devem ser informadas.");

                validarGTIN(GTIN);

                parametro.campos.codigoproduto = produtoBO.RetornaCodigoProdutoByGTIN(parametro);

                using (Database db = new Database("BD"))
                {
                    try
                    {
                        db.Open(true);
                        db.BeginTransaction();

                        parametro.campos.alergeno = new ExpandoObject();

                        parametro.campos.alergeno.allergenRelatedInformation = new ExpandoObject();
                        parametro.campos.alergeno.allergenRelatedInformation.allergen = new ExpandoObject();
                        parametro.campos.alergeno.allergenRelatedInformation.levelOfContainmentCode = new ExpandoObject();
                        //parametro.campos.alergeno = new JObject();
                        //parametro.campos.alergeno.Add("allergenRelatedInformation", new JObject());
                        //parametro.campos.alergeno.allergenRelatedInformation.Add("allergen", new JObject());
                        //parametro.campos.alergeno.allergenRelatedInformation.Add("levelOfContainmentCode", new JObject());

                        for (var i = 0; i < alergeno.allergenRelatedInformation.Count(); i++)
                        {
                            parametro.campos.alergeno.allergenRelatedInformation.allergenSpecificationAgency = alergeno.allergenRelatedInformation[i].allergenSpecificationAgency;
                            parametro.campos.alergeno.allergenRelatedInformation.allergenSpecificationName = alergeno.allergenRelatedInformation[i].allergenSpecificationName;
                            parametro.campos.alergeno.allergenRelatedInformation.allergenStatement = alergeno.allergenRelatedInformation[i].allergenStatement;

                            for (var j = 0; j < alergeno.allergenRelatedInformation[i].allergen.Count(); j++)
                            {
                                parametro.campos.alergeno.allergenRelatedInformation.allergen.codigo = alergeno.allergenRelatedInformation[i].allergen[j].allergenTypeCode.Value;
                                parametro.campos.alergeno.allergenRelatedInformation.levelOfContainmentCode.codigo = alergeno.allergenRelatedInformation[i].allergen[j].levelOfContainmentCode.Value;
                                produtoBO.InserirAlergenos(db, JsonConvert.DeserializeObject<JContainer>(JsonConvert.SerializeObject(parametro.campos.alergeno)), parametro.campos.codigoproduto);
                            }
                        }

                        return new Retorno(1, "");
                    }
                    catch (GS1TradeException e)
                    {
                        db.Rollback();
                        return new Retorno(-8, "Não foi possível inserir.");
                    }
                }
            }
            catch (ProdutoServiceException pse)
            {
                return new Retorno(pse.codigo, pse.descricao);
            }
            catch (Exception e)
            {
                return getRetornoErroGenerico();
            }
        }

        /// <summary>
        /// Método de cadastro de Catalogue Item Notification
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="CatalogueItemNotification">Informação de Catalogue Item Notification</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Catalogue Item Notification</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>CatalogueItemNotification</i></b>: Informações de Catalogue Item Notification<br/>")]
        public Retorno InsertCatalogueItemNotification(long GTIN, CatalogueItemNotificationType catalogueItemNotificationType)
        {
            try
            {
                dynamic produto = validarGTINModulo(GTIN, "CatalogueItemNotification");

                XmlSerializer xsSubmit = new XmlSerializer(typeof(CatalogueItemNotificationType));                
                using (StringWriter sww = new StringWriter())
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, catalogueItemNotificationType);
                    var xml = sww.ToString();                

                    if (produto != null && produto.Count > 0)
                    {
                        CatalogueItemNotificationBO catalogueItemNotificationBO = new CatalogueItemNotificationBO();
                       
                        if (produto[0].codigoprodutomodulo.Value.ToString() == string.Empty)
                        {
                            catalogueItemNotificationBO.InsertCatalogueItemNotification((long)produto[0].codigoproduto, xml);
                        }
                        else
                        {
                            catalogueItemNotificationBO.UpdateCatalogueItemNotification((long)produto[0].codigoproduto, xml);                            
                        }
                    }
                    else
                    {
                        throw new GTINNaoExisteException();
                    }
                }
                return new Retorno(0, "");

            }
            catch (Exception ex)
            {
                throw;
            }
        }
            

        /// <summary>
        /// Método de cadastro de Delivery Purchasing Information
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="DeliveryPurchasingInformation">Informação de Delivery Purchasing Information</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Delivery Purchasing Information</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>DeliveryPurchasingInformation</i></b>: Informações de Delivery Purchasing Information<br/>")]
        public Retorno InsertDeliveryPurchasingInformation(long GTIN, DeliveryPurchasingInformationType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Food And Beverage Ingredient
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="FoodAndBeverageIngredient">Informação de Food And Beverage Ingredient</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Food And Beverage Ingredient</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>FoodAndBeverageIngredient</i></b>: Informações de Food And Beverage Ingredient<br/>")]
        public Retorno InsertFoodAndBeverageIngredient(long GTIN, FoodAndBeverageIngredientType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Food And Beverage Preparation Serving Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="FoodAndBeveragePreparationServingModule">Informação de Food And Beverage Preparation Serving Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Food And Beverage Preparation Serving Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>FoodAndBeveragePreparationServingModule</i></b>: Informações de Food And Beverage Preparation Serving Module<br/>")]
        public Retorno InsertFoodAndBeveragePreparationServingModule(long GTIN, FoodAndBeveragePreparationServingModuleType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Nutrient Header
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="NutrientHeader">Informação de Nutrient Header</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Nutrient Header</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>NutrientHeader</i></b>: Informações de Nutrient Header<br/>")]
        public Retorno InsertNutrientHeader(long GTIN, NutritionalInformationModuleType nutritionalInformation)
        {
            try
            {
                validarGTIN(GTIN);

                ProdutoBO produtoBO = new ProdutoBO();

                dynamic parametro = new ExpandoObject();
                parametro.campos = new ExpandoObject();
                parametro.campos.globaltradeitemnumber = GTIN;
                parametro.campos.codigoproduto = produtoBO.RetornaCodigoProdutoByGTIN(parametro);

                using (Database db = new Database("BD"))
                {
                    try
                    {
                        db.Open(true);
                        db.BeginTransaction();

                        parametro.campos.alergeno = new ExpandoObject();
                        parametro.campos.alergeno.allergenRelatedInformation = new ExpandoObject();
                        parametro.campos.alergeno.allergenRelatedInformation.allergen = new ExpandoObject();
                        parametro.campos.alergeno.allergenRelatedInformation.levelOfContainmentCode = new ExpandoObject();

                        //for (var i = 0; i < alergeno.allergenRelatedInformation.Count(); i++)
                        //{
                        //    parametro.campos.alergeno.allergenRelatedInformation.allergenSpecificationAgency = alergeno.allergenRelatedInformation[i].allergenSpecificationAgency;
                        //    parametro.campos.alergeno.allergenRelatedInformation.allergenSpecificationName = alergeno.allergenRelatedInformation[i].allergenSpecificationName;
                        //    parametro.campos.alergeno.allergenRelatedInformation.allergenStatement = alergeno.allergenRelatedInformation[i].allergenStatement;

                        //    for (var j = 0; j < alergeno.allergenRelatedInformation[i].allergen.Count(); j++)
                        //    {
                        //        parametro.campos.alergeno.allergenRelatedInformation.allergen.codigo = alergeno.allergenRelatedInformation[i].allergen[j].allergenTypeCode.Value;
                        //        parametro.campos.alergeno.allergenRelatedInformation.levelOfContainmentCode.codigo = alergeno.allergenRelatedInformation[i].allergen[j].levelOfContainmentCode.Value;
                        //        produtoBO.InserirAlergenos(db, parametro.campos.alergeno, parametro.campos.codigoproduto);
                        //    }
                        //}

                        db.Commit();
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw e;
                    }
                }

                return getRetornoSucessoGenerico();
            }
            catch (ProdutoServiceException pse)
            {
                return new Retorno(pse.codigo, pse.descricao);
            }
            catch (Exception)
            {
                return getRetornoErroGenerico();
            }
        }

        /// <summary>
        /// Método de cadastro de Place Of Item Activity Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="PlaceOfItemActivityModule">Informação de Place Of Item Activity Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Place Of Item Activity Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>PlaceOfItemActivityModule</i></b>: Informações de Place Of Item Activity Module<br/>")]
        public Retorno InsertPlaceOfItemActivityModule(long GTIN, PlaceOfItemActivityModuleType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Price Synchronisation Document
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="PriceSynchronisationDocument">Informação de Price Synchronisation Document</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Price Synchronisation Document</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>PriceSynchronisationDocument</i></b>: Informações de Price Synchronisation Document<br/>")]
        public Retorno InsertPriceSynchronisationDocument(long GTIN, PriceSynchronisationDocumentType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Registry Party Data Dump
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="RegistryPartyDataDump">Informação de Registry Party Data Dump</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Registry Party Data Dump</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>RegistryPartyDataDump</i></b>: Informações de Registry Party Data Dump<br/>")]
        public Retorno InsertRegistryPartyDataDump(long GTIN, RegistryPartyDataDumpType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Regulated Trade Item Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="RegulatedTradeItemModule">Informação de Regulated Trade Item Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Regulated Trade Item Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>RegulatedTradeItemModule</i></b>: Informações de Regulated Trade Item Module<br/>")]
        public Retorno InsertRegulatedTradeItemModule(long GTIN, RegulatedTradeItemModuleType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Trade Item Handling Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="InsertTradeItemHandlingModule">Informação de Trade Item Handling Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Trade Item Handling Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>InsertTradeItemHandlingModule</i></b>: Informações de Trade Item Handling Module<br/>")]
        public Retorno InsertTradeItemHandlingModule(long GTIN, TradeItemHandlingModuleType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Trade Item Lifespan Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="TradeItemLifespanModule">Informação de Trade Item Lifespan Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Trade Item Lifespan Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>TradeItemLifespanModule</i></b>: Informações de Trade Item Lifespan Module<br/>")]
        public Retorno InsertTradeItemLifespanModule(long GTIN, TradeItemLifespanModuleType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Trade Item Measurements Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="TradeItemMeasurementsModule">Informação de Trade Item Measurements Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Trade Item Measurements Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>TradeItemMeasurementsModule</i></b>: Informações de Trade Item Measurements Module<br/>")]
        public Retorno InsertTradeItemMeasurementsModule(long GTIN, TradeItemMeasurementsModuleType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Trade Item Size Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="TradeItemSizeModule">Informação de Trade Item Size Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Trade Item Size Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>TradeItemSizeModule</i></b>: Informações de Trade Item Size Module<br/>")]
        public Retorno InsertTradeItemSizeModule(long GTIN, TradeItemSizeModuleType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Trade Item Temperature Information Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="TradeItemTemperatureInformationModule">Informação de Trade Item Temperature Information Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Trade Item Temperature Information Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>TradeItemTemperatureInformationModule</i></b>: Informações de Trade Item Temperature Information Module<br/>")]
        public Retorno InsertTradeItemTemperatureInformationModule(long GTIN, TradeItemTemperatureInformationModuleType alergeno)
        {
            return new Retorno(0, "");
        }

        /// <summary>
        /// Método de cadastro de Variable Trade Item Information Module
        /// </summary>
        /// <param name="GTIN">GTIN</param>
        /// <param name="VariableTradeItemInformationModule">Informação de Variable Trade Item Information Module</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "<b>Método de cadastro de Variable Trade Item Information Module</b><br/>" +
            "<b><i>GTIN</i></b>: GTIN do produto<br/>" +
            "<b><i>VariableTradeItemInformationModule</i></b>: Informações de Variable Trade Item Information Module<br/>")]
        public Retorno InsertVariableTradeItemInformationModule(long GTIN, VariableTradeItemInformationModuleType alergeno)
        {
            return new Retorno(0, "");
        }
    }
}
