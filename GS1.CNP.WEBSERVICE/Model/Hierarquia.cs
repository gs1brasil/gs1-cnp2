﻿using System;

namespace GS1.CNP.WEBSERVICE.Model
{
    [Serializable]
    public class Produto
    {
        public Int64 CodigoProduto { get; set; }
        public Int64 GTIN { get; set; }
        public Int64 NumeroPrefixo { get; set; }
        public int CodigoTipoGtin { get; set; }
        public string DescricaoProduto { get; set; }
        public string Marca { get; set; }
        public string GLNProvedorInformacao { get; set; }
        public string CompartilhaDados { get; set; }
        public string PaisMercadoDestino { get; set; }
        public string NumeroModelo { get; set; }
        public DateTime InicioDisponibilidade { get; set; }
        public DateTime DataInclusao { get; set; }
        public DateTime DataAlteracao { get; set; }
        public int CodigoTipoProduto { get; set; }
        public string Largura { get; set; }
        public string UnidadeMedidaLargura { get; set; }
        public string Altura { get; set; }
        public string UnidadeMedidaAltura { get; set; }
        public string Profundidade { get; set; }
        public string UnidadeMedidaProfundidade { get; set; }
        public string ConteudoLiquido { get; set; }
        public string UnidadeMedidaConteudoLiquido { get; set; }
        public string PesoBruto { get; set; }
        public string UnidadeMedidaPesoBruto { get; set; }
        public string Observacoes { get; set; }
        public string Segmento { get; set; }
        public string Familia { get; set; }
        public string Classe { get; set; }
        public string Bloco { get; set; }
        public string AtributoDoBloco { get; set; }
        public string ValorAtributoBloco { get; set; }
        public int Pais { get; set; }
        public string Lingua { get; set; }
        public int Estado { get; set; }
        public string AliquotaIPI { get; set; }
        public int NCM { get; set; }
        public string NumeroAlternativoIdentificacao { get; set; }
        public string AgenciaReguladora { get; set; }
        public string NomeURL { get; set; }
        public string NomeTipoURL { get; set; }
        public string ItemComercialLiberadoCompra { get; set; }
        public string ItemComercialUnidadeBasica { get; set; }
        public string UnidadeDeConsumo { get; set; }
        public int UnidadeMedidaPesoComercial { get; set; }
        public string TemperaturaMinima { get; set; }
        public int UnidadeTemperaturaMinima { get; set; }
        public string TemperaturaMaxima { get; set; }
        public string UnidadeTemperaturaMaxima { get; set; }
        public object ProdutoInferior { get; set; }
    }

    [Serializable]
    public class ProdutoInferior
    {
        public int CodigoProdutoInferior { get; set; }
        public int QuantidadeItens { get; set; }
        public string FatorDeEmpilhamento { get; set; }
        public string QuantidadeCamadasCompletas { get; set; }
        public string QuantidadeItensCamadasCompletas { get; set; }
        public string QuantidadeCamadasPorPallet { get; set; }
    }
}
