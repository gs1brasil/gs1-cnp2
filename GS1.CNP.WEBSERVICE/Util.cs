﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Xml.Linq;

namespace GS1.CNP.WEBSERVICE
{
    public class Util
    {
        /// <summary>
        /// Convert um datareader em string JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static string ConvertReaderTOJson(IDataReader reader, int pageNumber = 0)
        {
            if (reader != null || !reader.IsClosed)
            {
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                int rowcount = 0,
                    currentPage = 0;

                using (JsonWriter jsonWriter = new JsonTextWriter(sw))
                {
                    jsonWriter.WriteStartArray();

                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();

                        int fields = reader.FieldCount;

                        for (int i = 0; i < fields; i++)
                        {
                            if (!reader.GetName(i).ToUpper().Equals("ROWNUM") && !reader.GetName(i).ToUpper().Equals("ROWCOUNT") && !reader.GetName(i).ToUpper().Equals("CURRENTPAGE"))
                            {
                                jsonWriter.WritePropertyName(reader.GetName(i).ToLower());
                                jsonWriter.WriteValue(reader[i]);
                            }
                            else if (rowcount == 0 && reader.GetName(i).ToUpper().Equals("ROWCOUNT") && reader[i] != null)
                            {
                                rowcount = Int32.Parse(reader[i].ToString());
                            }
                            else if (currentPage == 0 && reader.GetName(i).ToUpper().Equals("CURRENTPAGE") && reader[i] != null)
                            {
                                currentPage = Int32.Parse(reader[i].ToString());
                            }
                        }

                        jsonWriter.WriteEndObject();
                    }

                    jsonWriter.WriteEndArray();
                }

                if (rowcount > 0)
                {
                    return "{\"RowCount\": " + rowcount.ToString() + ", \"CurrentPage\": " + currentPage.ToString() + ", \"data\": " + sw.ToString() + "}";
                }
                else
                {
                    return sw.ToString();
                }
            }
            else
            {
                throw new Exception("Reader não tem informação para geração do JSON");
            }
        }

        public static string ConvertReaderTOJsonWebService(IDataReader reader, int pageNumber = 0)
        {
            if (reader != null || !reader.IsClosed)
            {
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                int rowcount = 0,
                    currentPage = 0;

                using (JsonWriter jsonWriter = new JsonTextWriter(sw))
                {
                    jsonWriter.WriteStartArray();

                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();

                        int fields = reader.FieldCount;
                        bool breakSuperior = false;

                        for (int i = 0; i < fields; i++)
                        {
                            if (!reader.GetName(i).ToUpper().Equals("ROWNUM") && !reader.GetName(i).ToUpper().Equals("ROWCOUNT") && !reader.GetName(i).ToUpper().Equals("CURRENTPAGE"))
                            {
                                if (reader.GetName(i).ToLower() != "codigoprodutoinferior" && !breakSuperior)
                                {
                                    jsonWriter.WritePropertyName(reader.GetName(i).ToLower());
                                    jsonWriter.WriteValue(reader[i]);
                                }
                                else
                                {
                                    if (reader.GetName(i).ToLower() == "codigoprodutoinferior")
                                    {
                                        jsonWriter.WritePropertyName("produtoinferior");
                                        jsonWriter.WriteStartObject();
                                    }

                                    jsonWriter.WritePropertyName(reader.GetName(i).ToLower());
                                    jsonWriter.WriteValue(reader[i]);
                                    breakSuperior = true;
                                }
                            }
                            else if (rowcount == 0 && reader.GetName(i).ToUpper().Equals("ROWCOUNT") && reader[i] != null)
                            {
                                rowcount = Int32.Parse(reader[i].ToString());
                            }
                            else if (currentPage == 0 && reader.GetName(i).ToUpper().Equals("CURRENTPAGE") && reader[i] != null)
                            {
                                currentPage = Int32.Parse(reader[i].ToString());
                            }
                        }

                        jsonWriter.WriteEndObject();
                    }

                    jsonWriter.WriteEndArray();
                }

                if (rowcount > 0)
                {
                    return "{\"RowCount\": " + rowcount.ToString() + ", \"CurrentPage\": " + currentPage.ToString() + ", \"data\": " + sw.ToString() + "}";
                }
                else
                {
                    return sw.ToString();
                }
            }
            else
            {
                throw new Exception("Reader não tem informação para geração do JSON");
            }
        }

        public object ConvertReaderTOExpandoObject(IDataReader reader)
        {
            if (reader != null || !reader.IsClosed)
            {
                List<ExpandoObject> retorno = new List<ExpandoObject>();

                while (reader.Read())
                {
                    dynamic linha = new ExpandoObject();
                    int fields = reader.FieldCount;

                    for (int i = 0; i < fields; i++)
                    {
                        if (!reader.GetName(i).ToUpper().Equals("ROWNUM") && !reader.GetName(i).ToUpper().Equals("ROWCOUNT") && !reader.GetName(i).ToUpper().Equals("CURRENTPAGE"))
                        {
                            (linha as IDictionary<string, object>).Add(reader.GetName(i), reader.GetValue(i));
                        }
                    }

                    retorno.Add(linha);
                }

                return retorno;
            }
            else
            {
                throw new Exception("Reader não tem informação para geração do JSON");
            }
        }

        public static object ConvertJTokenToObject(JToken token)
        {
            if (token is JValue)
            {
                return ((JValue)token).Value;
            }
            if (token is JObject)
            {
                ExpandoObject expando = new ExpandoObject();

                (from childToken in ((JToken)token) where childToken is JProperty select childToken as JProperty).ToList().ForEach(property =>
                {
                    ((IDictionary<string, object>)expando).Add(property.Name, ConvertJTokenToObject(property.Value));
                });

                return expando;
            }
            if (token is JArray)
            {
                object[] array = new object[((JArray)token).Count];
                int index = 0;

                foreach (JToken arrayItem in ((JArray)token))
                {
                    array[index] = ConvertJTokenToObject(arrayItem);
                    index++;
                }

                return array;
            }

            throw new ArgumentException(string.Format("Unknown token type '{0}'", token.GetType()), "token");
        }
    }

    public static class JObjectExtensions
    {
        public static object ToExpando(this JToken token)
        {
            if (token is JValue)
            {
                return ((JValue)token).Value;
            }
            if (token is JObject)
            {
                ExpandoObject expando = new ExpandoObject();

                (from childToken in ((JToken)token) where childToken is JProperty select childToken as JProperty).ToList().ForEach(property =>
                {
                    ((IDictionary<string, object>)expando).Add(property.Name, ToExpando(property.Value));
                });

                return expando;
            }
            if (token is JArray)
            {
                object[] array = new object[((JArray)token).Count];
                int index = 0;

                foreach (JToken arrayItem in ((JArray)token))
                {
                    array[index] = ToExpando(arrayItem);
                    index++;
                }

                return array;
            }

            throw new ArgumentException(string.Format("Unknown token type '{0}'", token.GetType()), "token");
        }
    }

    public static class LinqToObjectExtensions
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
    }

    public static class EnumerableDebugger
    {
        public static ConsoleColor DefaultColor = ConsoleColor.Yellow;
        public static bool ShowWhiteSpace { get; set; }

        public static IEnumerable<T> Dump<T>(this IEnumerable<T> input, string title)
        {
            return Dump(input, title,
               item => item != null ? JsonConvert.SerializeObject(item) : "(null)", DefaultColor);
        }

        public static IEnumerable<T> Dump<T>(
            this IEnumerable<T> input,
            string title,
            ConsoleColor consoleColor)
        {
            return Dump(input, title,
                item => item != null ? JsonConvert.SerializeObject(item) : "(null)", consoleColor);
        }

        public static IEnumerable<T> Dump<T>(
            this IEnumerable<T> input,
            string title,
            Func<T, string> toString,
            ConsoleColor consoleColor)
        {
            foreach (var item in input)
            {
                Console.ForegroundColor = consoleColor;
                Debug.WriteLine(ShowWhiteSpace ? '[' + title + " : " + toString(item) + ']' : title + " : " + toString(item));
                Console.ResetColor();
                yield return item;
            }
        }
    }

    public class DynamicXml : DynamicObject
    {
        XElement _root;
        private DynamicXml(XElement root)
        {
            _root = root;
        }

        public static DynamicXml Parse(string xmlString)
        {
            return new DynamicXml(XDocument.Parse(xmlString).Root);
        }

        public static DynamicXml Load(string filename)
        {
            return new DynamicXml(XDocument.Load(filename).Root);
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;

            var att = _root.Attribute(binder.Name);
            if (att != null)
            {
                result = att.Value;
                return true;
            }

            var nodes = _root.Elements(binder.Name);
            if (nodes.Count() > 1)
            {
                result = nodes.Select(n => new DynamicXml(n)).ToList();
                return true;
            }

            var node = _root.Element(binder.Name);
            if (node != null)
            {
                if (node.HasElements)
                {
                    result = new DynamicXml(node);
                }
                else
                {
                    result = node.Value;
                }
                return true;
            }

            return true;
        }
    }
}
