﻿using System;
using System.Web.Services;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Runtime.Serialization;

namespace GS1.CNP.WEBSERVICE
{
    /// <summary>
    /// Summary description for wsHierarquia
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsHierarquia : System.Web.Services.WebService
    {

        [WebMethod]
        [System.Xml.Serialization.XmlInclude(typeof(Object[]))]
        public dynamic GetHierarchy(string gtin)
        {
            string sql = @"SELECT P.CODIGOPRODUTO
		                    , P.GLOBALTRADEITEMNUMBER AS GTIN
		                    , P.NRPREFIXO AS NUMEROPREFIXO
		                    , P.CODIGOTIPOGTIN
		                    , P.PRODUCTDESCRIPTION AS DESCRICAOPRODUTO
		                    , P.BRANDNAME AS MARCA
		                    , P.INFORMATIONPROVIDER AS GLNPROVEDORINFORMACAO
		                    , CASE WHEN p.INDICADORCOMPARTILHADADOS = 1 THEN 'Sim' WHEN p.INDICADORCOMPARTILHADADOS = 0 THEN 'Não' END AS COMPARTILHADADOS
		                    , P.COUNTRYCODE AS PAISMERCADODESTINO
		                    , P.MODELNUMBER AS NUMEROMODELO
		                    , P.STARTAVAILABILITYDATETIME AS INICIODISPONIBILIDADE
		                    , P.DATAINCLUSAO
		                    , P.DATAALTERACAO
		                    --, MOTIVO REATIVAÇÃO GTIN
		                    , P.CODIGOTIPOPRODUTO
		                    , P.WIDTH AS LARGURA
		                    , P.WIDTHMEASUREMENTUNITCODE AS UNIDADEMEDIDALARGURA
		                    , P.HEIGHT AS ALTURA
		                    , P.HEIGHTMEASUREMENTUNITCODE AS UNIDADEMEDIDAALTURA
		                    , P.DEPTH AS PROFUNDIDADE
		                    , P.DEPTHMEASUREMENTUNITCODE AS UNIDADEMEDIDAPROFUNDIDADE
		                    , P.NETCONTENT AS CONTEUDOLIQUIDO
		                    , P.NETCONTENTMEASUREMENTUNITCODE AS UNIDADEMEDIDACONTEUDOLIQUIDO
		                    , P.GROSSWEIGHT AS PESOBRUTO
		                    , P.GROSSWEIGHTMEASUREMENTUNITCODE AS UNIDADEMEDIDAPESOBRUTO
		                    , P.OBSERVACOES
		                    , P.CODESEGMENT AS SEGMENTO
		                    , P.CODEFAMILY AS FAMILIA
		                    , P.CODECLASS AS CLASSE
		                    , P.CODEBRICK AS BLOCO
		                    , PBTV.CODETYPE AS ATRIBUTODOBLOCO
		                    , PBTV.CODEVALUE AS VALORATRIBUTOBLOCO
		                    , P.TRADEITEMCOUNTRYOFORIGIN AS PAIS
		                    , L.NOME AS LINGUA
		                    , P.ESTADO
		                    , P.IPIPERC AS ALIQUOTAIPI
		                    , P.IMPORTCLASSIFICATIONVALUE AS NCM
		                    --, TIPO DE NCM
		                    , P.ALTERNATEITEMIDENTIFICATIONID AS NUMEROALTERNATIVOIDENTIFICACAO
		                    , P.ALTERNATEITEMIDENTIFICATIONAGENCY AS AGENCIAREGULADORA
		                    , PU.NOME AS NOMEURL
		                    , TU.NOME AS NOMETIPOURL
		                    , CASE WHEN P.ISTRADEITEMANORDERABLEUNIT = 1 THEN 'Sim' WHEN P.ISTRADEITEMANORDERABLEUNIT  = 0 THEN 'Não' END AS ITEMCOMERCIALLIBERADOCOMPRA
		                    , CASE WHEN P.ISTRADEITEMABASEUNIT = 1 THEN 'Sim' WHEN P.ISTRADEITEMABASEUNIT  = 0 THEN 'Não' END AS ITEMCOMERCIALUNIDADEBASICA
		                    , CASE WHEN P.ISTRADEITEMACONSUMERUNIT = 1 THEN 'Sim' WHEN P.ISTRADEITEMACONSUMERUNIT  = 0 THEN 'Não' END AS UNIDADEDECONSUMO
		                    , CASE WHEN P.ISTRADEITEMANORDERABLEUNIT = 1 THEN 'Sim' WHEN P.ISTRADEITEMANORDERABLEUNIT  = 0 THEN 'Não' END AS UNIDADEMEDIDAPESOCOMERCIAL
		                    , P.DELIVERYTODISTRIBUTIONCENTERTEMPERATUREMINIMUM AS TEMPERATURAMINIMA
		                    , P.STORAGEHANDLINGTEMPMINIMUMUOM AS UNIDADETEMPERATURAMINIMA
		                    , P.STORAGEHANDLINGTEMPERATUREMAXIMUM AS TEMPERATURAMAXIMA
		                    , P.STORAGEHANDLINGTEMPERATUREMAXIMUMUNITOFMEASURE AS UNIDADETEMPERATURAMAXIMA
		                    , P2.CODIGOPRODUTO AS CODIGOPRODUTOINFERIOR
		                    , PH.QUANTIDADE AS QUANTIDADEITENS
		                    , P.STACKINGFACTOR AS FATORDEEMPILHAMENTO
		                    , P.QUANTITYOFCOMPLETELAYERSCONTAINEDINATRADEITEM AS QUANTIDADECAMADASCOMPLETAS
		                    , P.QUANTITYOFTRADEITEMCONTAINEDINACOMPLETELAYER AS QUANTIDADEITENSCAMADASCOMPLETAS
		                    , P.QUANTITYOFLAYERSPERPALLET AS QUANTIDADECAMADASPORPALLET
                    FROM PRODUTOHIERARQUIA PH 
                    INNER JOIN PRODUTO P2 ON P2.CODIGOPRODUTO = PH.CODIGOPRODUTOINFERIOR
                    INNER JOIN PRODUTO P ON P.CODIGOPRODUTO = PH.CODIGOPRODUTOSUPERIOR
                    LEFT JOIN PRODUTOBRICKTYPEVALUE PBTV ON PBTV.CODEBRICK = P.CODEBRICK
                    LEFT JOIN LINGUA L ON L.CODIGO = P.CODIGOLINGUA
                    LEFT JOIN PRODUTOURL PU ON PU.CODIGOPRODUTO = P.CODIGOPRODUTO
                    LEFT JOIN TIPOURL TU ON TU.CODIGO = PU.CODIGOTIPOURL
                    WHERE PH.CODIGOPRODUTOSUPERIOR IS NOT NULL
                    AND P.GLOBALTRADEITEMNUMBER = @gtin";

            SqlConnection conexao = new SqlConnection(WebConfigurationManager.ConnectionStrings["BD"].ToString());
            SqlCommand db = new SqlCommand(sql, conexao);
            db.Parameters.Add(new SqlParameter("gtin", gtin));

            using (db)
            {
                try
                {
                    if (!string.IsNullOrEmpty(gtin))
                    {
                        conexao.Open();
                        SqlDataReader reader = db.ExecuteReader();
                        dynamic objetoSuperior = new ExpandoObject();
                        dynamic objetoInferior = new ExpandoObject();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                //objetoInferior.CodigoProdutoInferior = (reader.IsDBNull(48) ? 0 : reader.GetInt64(48));
                                //objetoInferior.QuantidadeItens = (reader.IsDBNull(49) ? 0 : reader.GetInt32(49));
                                //objetoInferior.FatorDeEmpilhamento = (reader.IsDBNull(50) ? string.Empty : reader.GetString(50));
                                //objetoInferior.QuantidadeCamadasCompletas = (reader.IsDBNull(51) ? string.Empty : reader.GetString(51));
                                //objetoInferior.QuantidadeItensCamadasCompletas = (reader.IsDBNull(52) ? string.Empty : reader.GetString(52));
                                //objetoInferior.QuantidadeCamadasPorPallet = (reader.IsDBNull(53) ? string.Empty : reader.GetString(53));

                                //objetoSuperior.CodigoProduto = reader.GetInt64(0);
                                //objetoSuperior.GTIN = (reader.IsDBNull(1) ? 0 : reader.GetInt64(1));
                                //objetoSuperior.NumeroPrefixo = (reader.IsDBNull(2) ? 0 : reader.GetInt64(2)); 
                                //objetoSuperior.CodigoTipoGtin = reader.GetInt32(3);
                                //objetoSuperior.DescricaoProduto = (reader.IsDBNull(4) ? string.Empty : reader.GetString(4));
                                //objetoSuperior.Marca = (reader.IsDBNull(5) ? string.Empty : reader.GetString(5));
                                //objetoSuperior.GLNProvedorInformacao = (reader.IsDBNull(6) ? string.Empty : reader.GetString(6));
                                //objetoSuperior.CompartilhaDados = (reader.IsDBNull(7) ? string.Empty : reader.GetString(7));
                                //objetoSuperior.PaisMercadoDestino = (reader.IsDBNull(8) ? string.Empty : reader.GetString(8));
                                //objetoSuperior.NumeroModelo = (reader.IsDBNull(9) ? string.Empty : reader.GetString(9));
                                //objetoSuperior.InicioDisponibilidade = (reader.IsDBNull(10) ? DateTime.MinValue : reader.GetDateTime(10));
                                //objetoSuperior.DataInclusao = reader.GetDateTime(11);
                                //objetoSuperior.DataAlteracao = reader.GetDateTime(12);
                                //objetoSuperior.CodigoTipoProduto = (reader.IsDBNull(13) ? 0 : reader.GetInt32(13));
                                //objetoSuperior.Largura = (reader.IsDBNull(14) ? string.Empty : reader.GetString(14));
                                //objetoSuperior.UnidadeMedidaLargura = (reader.IsDBNull(15) ? string.Empty : reader.GetString(15));
                                //objetoSuperior.Altura = (reader.IsDBNull(16) ? string.Empty : reader.GetString(16));
                                //objetoSuperior.UnidadeMedidaAltura = (reader.IsDBNull(17) ? string.Empty : reader.GetString(17));
                                //objetoSuperior.Profundidade = (reader.IsDBNull(18) ? string.Empty : reader.GetString(18));
                                //objetoSuperior.UnidadeMedidaProfundidade = (reader.IsDBNull(19) ? string.Empty : reader.GetString(19));
                                //objetoSuperior.ConteudoLiquido = (reader.IsDBNull(20) ? string.Empty : reader.GetString(20));
                                //objetoSuperior.UnidadeMedidaConteudoLiquido = (reader.IsDBNull(21) ? string.Empty : reader.GetString(21));
                                //objetoSuperior.PesoBruto = (reader.IsDBNull(22) ? string.Empty : reader.GetString(22));
                                //objetoSuperior.UnidadeMedidaPesoBruto = (reader.IsDBNull(23) ? string.Empty : reader.GetString(23));
                                //objetoSuperior.Observacoes = (reader.IsDBNull(24) ? string.Empty : reader.GetString(24));
                                //objetoSuperior.Segmento = (reader.IsDBNull(25) ? string.Empty : reader.GetString(25));
                                //objetoSuperior.Familia = (reader.IsDBNull(26) ? string.Empty : reader.GetString(26));
                                //objetoSuperior.Classe = (reader.IsDBNull(27) ? string.Empty : reader.GetString(27));
                                //objetoSuperior.Bloco = (reader.IsDBNull(28) ? string.Empty : reader.GetString(28));
                                //objetoSuperior.AtributoDoBloco = (reader.IsDBNull(29) ? string.Empty : reader.GetString(29));
                                //objetoSuperior.ValorAtributoBloco = (reader.IsDBNull(30) ? string.Empty : reader.GetString(30));
                                //objetoSuperior.Pais = (reader.IsDBNull(31) ? 0 : reader.GetInt32(31));
                                //objetoSuperior.Lingua = (reader.IsDBNull(32) ? string.Empty : reader.GetString(32));
                                //objetoSuperior.Estado = (reader.IsDBNull(33) ? 0 : reader.GetInt32(33));
                                //objetoSuperior.AliquotaIPI = (reader.IsDBNull(34) ? string.Empty : reader.GetString(34));
                                //objetoSuperior.NCM = (reader.IsDBNull(35) ? 0 : reader.GetInt32(35));
                                //objetoSuperior.NumeroAlternativoIdentificacao = (reader.IsDBNull(36) ? string.Empty : reader.GetString(36));
                                //objetoSuperior.AgenciaReguladora = (reader.IsDBNull(37) ? string.Empty : reader.GetString(37));
                                //objetoSuperior.NomeURL = (reader.IsDBNull(38) ? string.Empty : reader.GetString(38));
                                //objetoSuperior.NomeTipoURL = (reader.IsDBNull(39) ? string.Empty : reader.GetString(39));
                                //objetoSuperior.ItemComercialLiberadoCompra = (reader.IsDBNull(40) ? string.Empty : reader.GetString(40));
                                //objetoSuperior.ItemComercialUnidadeBasica = (reader.IsDBNull(41) ? string.Empty : reader.GetString(41));
                                //objetoSuperior.UnidadeDeConsumo = (reader.IsDBNull(42) ? string.Empty : reader.GetString(42));
                                //objetoSuperior.UnidadeMedidaPesoComercial = (reader.IsDBNull(43) ? 0 : reader.GetInt32(43));
                                //objetoSuperior.TemperaturaMinima = (reader.IsDBNull(44) ? string.Empty : reader.GetString(44));
                                //objetoSuperior.UnidadeTemperaturaMinima = (reader.IsDBNull(45) ? 0 : reader.GetInt32(45));
                                //objetoSuperior.TemperaturaMaxima = (reader.IsDBNull(46) ? string.Empty : reader.GetString(46));
                                //objetoSuperior.UnidadeTemperaturaMaxima = (reader.IsDBNull(47) ? string.Empty : reader.GetString(47));

                                //objetoSuperior.ProdutoInferior = objetoInferior;

                                if (!reader.IsDBNull(48)) objetoInferior.CodigoProdutoInferior = reader.GetInt64(48);
                                if (!reader.IsDBNull(49)) objetoInferior.QuantidadeItens = reader.GetInt32(49);
                                if (!reader.IsDBNull(50)) objetoInferior.FatorDeEmpilhamento = reader.GetString(50);
                                if (!reader.IsDBNull(51)) objetoInferior.QuantidadeCamadasCompletas = reader.GetString(51);
                                if (!reader.IsDBNull(52)) objetoInferior.QuantidadeItensCamadasCompletas = reader.GetString(52);
                                if (!reader.IsDBNull(53)) objetoInferior.QuantidadeCamadasPorPallet = reader.GetString(53);

                                if (!reader.IsDBNull(0)) objetoSuperior.CodigoProduto = reader.GetInt64(0);
                                if (!reader.IsDBNull(1)) objetoSuperior.GTIN = reader.GetInt64(1);
                                if (!reader.IsDBNull(2)) objetoSuperior.NumeroPrefixo = reader.GetInt64(2);
                                if (!reader.IsDBNull(3)) objetoSuperior.CodigoTipoGtin = reader.GetInt32(3);
                                if (!reader.IsDBNull(4)) objetoSuperior.DescricaoProduto = reader.GetString(4);
                                if (!reader.IsDBNull(5)) objetoSuperior.Marca = reader.GetString(5);
                                if (!reader.IsDBNull(6)) objetoSuperior.GLNProvedorInformacao = reader.GetString(6);
                                if (!reader.IsDBNull(7)) objetoSuperior.CompartilhaDados = reader.GetString(7);
                                if (!reader.IsDBNull(8)) objetoSuperior.PaisMercadoDestino = reader.GetString(8);
                                if (!reader.IsDBNull(9)) objetoSuperior.NumeroModelo = reader.GetString(9);
                                if (!reader.IsDBNull(10)) objetoSuperior.InicioDisponibilidade = reader.GetDateTime(10);
                                if (!reader.IsDBNull(11)) objetoSuperior.DataInclusao = reader.GetDateTime(11);
                                if (!reader.IsDBNull(12)) objetoSuperior.DataAlteracao = reader.GetDateTime(12);
                                if (!reader.IsDBNull(13)) objetoSuperior.CodigoTipoProduto = reader.GetInt32(13);
                                if (!reader.IsDBNull(14)) objetoSuperior.Largura = reader.GetString(14);
                                if (!reader.IsDBNull(15)) objetoSuperior.UnidadeMedidaLargura = reader.GetString(15);
                                if (!reader.IsDBNull(16)) objetoSuperior.Altura = reader.GetString(16);
                                if (!reader.IsDBNull(17)) objetoSuperior.UnidadeMedidaAltura = reader.GetString(17);
                                if (!reader.IsDBNull(18)) objetoSuperior.Profundidade = reader.GetString(18);
                                if (!reader.IsDBNull(19)) objetoSuperior.UnidadeMedidaProfundidade = reader.GetString(19);
                                if (!reader.IsDBNull(20)) objetoSuperior.ConteudoLiquido = reader.GetString(20);
                                if (!reader.IsDBNull(21)) objetoSuperior.UnidadeMedidaConteudoLiquido = reader.GetString(21);
                                if (!reader.IsDBNull(22)) objetoSuperior.PesoBruto = reader.GetString(22);
                                if (!reader.IsDBNull(23)) objetoSuperior.UnidadeMedidaPesoBruto = reader.GetString(23);
                                if (!reader.IsDBNull(24)) objetoSuperior.Observacoes = reader.GetString(24);
                                if (!reader.IsDBNull(25)) objetoSuperior.Segmento = reader.GetString(25);
                                if (!reader.IsDBNull(26)) objetoSuperior.Familia = reader.GetString(26);
                                if (!reader.IsDBNull(27)) objetoSuperior.Classe = reader.GetString(27);
                                if (!reader.IsDBNull(28)) objetoSuperior.Bloco = reader.GetString(28);
                                if (!reader.IsDBNull(29)) objetoSuperior.AtributoDoBloco = reader.GetString(29);
                                if (!reader.IsDBNull(30)) objetoSuperior.ValorAtributoBloco = reader.GetString(30);
                                if (!reader.IsDBNull(31)) objetoSuperior.Pais = reader.GetInt32(31);
                                if (!reader.IsDBNull(32)) objetoSuperior.Lingua = reader.GetString(32);
                                if (!reader.IsDBNull(33)) objetoSuperior.Estado = reader.GetInt32(33);
                                if (!reader.IsDBNull(34)) objetoSuperior.AliquotaIPI = reader.GetString(34);
                                if (!reader.IsDBNull(35)) objetoSuperior.NCM = reader.GetInt32(35);
                                if (!reader.IsDBNull(36)) objetoSuperior.NumeroAlternativoIdentificacao = reader.GetString(36);
                                if (!reader.IsDBNull(37)) objetoSuperior.AgenciaReguladora = reader.GetString(37);
                                if (!reader.IsDBNull(38)) objetoSuperior.NomeURL = reader.GetString(38);
                                if (!reader.IsDBNull(39)) objetoSuperior.NomeTipoURL = reader.GetString(39);
                                if (!reader.IsDBNull(40)) objetoSuperior.ItemComercialLiberadoCompra = reader.GetString(40);
                                if (!reader.IsDBNull(41)) objetoSuperior.ItemComercialUnidadeBasica = reader.GetString(41);
                                if (!reader.IsDBNull(42)) objetoSuperior.UnidadeDeConsumo = reader.GetString(42);
                                if (!reader.IsDBNull(43)) objetoSuperior.UnidadeMedidaPesoComercial = reader.GetInt32(43);
                                if (!reader.IsDBNull(44)) objetoSuperior.TemperaturaMinima = reader.GetString(44);
                                if (!reader.IsDBNull(45)) objetoSuperior.UnidadeTemperaturaMinima = reader.GetInt32(45);
                                if (!reader.IsDBNull(46)) objetoSuperior.TemperaturaMaxima = reader.GetString(46);
                                if (!reader.IsDBNull(47)) objetoSuperior.UnidadeTemperaturaMaxima = reader.GetString(47);

                                objetoSuperior.ProdutoInferior = objetoInferior;

                                //dynamic objeto = CNP.WEBSERVICE.Util.ConvertReaderTOJsonWebService(reader);

                                //var dataContractSerializer = new DataContractSerializer(objetoSuperior.GetType());
                                //using (MemoryStream memoryStream = new MemoryStream())
                                //{
                                //    dataContractSerializer.WriteObject(memoryStream, objetoSuperior);
                                //    string outputXml = Encoding.UTF8.GetString(memoryStream.ToArray());

                                //    //DataContractSerializer serializer = new DataContractSerializer(objetoSuperior.GetType());
                                //    //string outputXml = serializer.WriteObject(memoryStream, objetoSuperior);

                                //    return outputXml;
                                //}

                                //return Serialize(objetoSuperior);
                                return objetoSuperior;
                            }

                            return null;
                        }
                        else
                        {
                            dynamic retorno = new ExpandoObject();
                            retorno.error = "GTIN sem hierarquia.";

                            return Serialize(retorno);
                        }
                    }
                    else
                    {
                        dynamic retorno = new ExpandoObject();
                        retorno.error = "GTIN não encontrado.";

                        return Serialize(retorno);
                    }
                }
                finally
                {
                    conexao.Close();
                }

                return null;
            }

            
        }

        public static string Serialize(object obj)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            using (StreamReader reader = new StreamReader(memoryStream))
            {
                DataContractSerializer serializer = new DataContractSerializer(obj.GetType());
                serializer.WriteObject(memoryStream, obj);
                memoryStream.Position = 0;
                return reader.ReadToEnd();
            }
        }
    }
    
}
