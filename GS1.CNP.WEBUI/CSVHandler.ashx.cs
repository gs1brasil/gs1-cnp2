﻿using System;
using System.Web;
using System.IO;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Web.SessionState;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for CSVHandler1
    /// </summary>
    public class CSVHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string html = HttpUtility.UrlDecode((new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd());
            if (html != String.Empty && html.Length > 8)
            {
                html = html.Substring(8);
                html = html.Replace(" tableNoBorder", "");
            }

            //string filename = context.Request.UrlReferrer.Segments[2] + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".csv";

            string filename = "";
            if (context.Request.UrlReferrer != null && context.Request.UrlReferrer.Segments.Length > 2)
            {
                filename = context.Request.UrlReferrer.Segments[2] + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".csv";
            }
            else
            {
                filename = "relatorioCNP_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".csv";
            }


            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            //TODO: fazer da o LOAD do HTML do parametro
            //doc.Load(@"c:\temp\gs1.html");
            doc.LoadHtml(html.Replace(@"csvdata=    
                    ", ""));

            //remover cometários
            var nodes = doc.DocumentNode.SelectNodes("//comment()");
            if (nodes != null)
            {
                foreach (HtmlNode comment in nodes)
                {
                    comment.ParentNode.RemoveChild(comment);
                }
            }



            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string linha = "";
            int quantidadeCabecalho = 0;

            if (doc.DocumentNode.SelectSingleNode("//table//thead") != null)
            {
                HtmlNode tbResultadoCabecalho = doc.DocumentNode.SelectSingleNode("//table//thead");
                HtmlAgilityPack.HtmlDocument docTR = new HtmlDocument();
                docTR.LoadHtml(tbResultadoCabecalho.InnerHtml);
                if (docTR.DocumentNode.SelectNodes("//th") != null)
                {

                    foreach (HtmlNode th in docTR.DocumentNode.SelectNodes("//th"))
                    {
                        if ((th.Attributes["class"] == null) || (th.Attributes["class"] != null && !(th.Attributes["class"].Value.Contains("removerRelatorio"))))
                        {
                            linha += "\"" + th.InnerText.Trim().Replace(";", "") + "\"" + ";";
                            quantidadeCabecalho++;
                        }
                    }
                    //remove a ultima virgula
                    sb.AppendLine(linha.Substring(0, linha.Length - 1));

                    linha = null;
                }
            }

            if (doc.DocumentNode.SelectNodes("//table//tr[@class='ng-scope']") != null && doc.DocumentNode.SelectNodes("//table[contains(@class, 'mensal')]//tr") == null)
            {
                foreach (HtmlNode tbResultado in doc.DocumentNode.SelectNodes("//table//tr[@class='ng-scope']"))
                {
                    HtmlAgilityPack.HtmlDocument docTR = new HtmlDocument();
                    docTR.LoadHtml(tbResultado.InnerHtml);
                    if (docTR.DocumentNode.SelectNodes("//td[contains(@class,'ng-binding')]") != null)
                    {
                        if (doc.DocumentNode.SelectNodes("//table[contains(@class,'relatorioInteracoesFornecedor')]") != null
                               && docTR.DocumentNode.SelectNodes("//td[contains(@class,'ng-binding')]").Count < quantidadeCabecalho)
                        {
                            linha += ";";
                        }

                        foreach (HtmlNode td in docTR.DocumentNode.SelectNodes("//td[contains(@class,'ng-binding')]"))
                        {

                            //Remove quebra de linha (Log Erros)
                            if (td.Attributes["style"] != null && td.Attributes["style"].Value == "white-space:pre;" && (td.Attributes["class"] != null && td.Attributes["class"].Value == "removequebralinha"))
                            {
                                linha += "\"" + td.InnerText.Trim().Replace(";", "").Replace("\r\n", "") + "\"" + ";";
                            }
                            //Remove quebra de linha e columa virgula nas colunas regiao e categoria
                            else if (td.Attributes["style"] != null && td.Attributes["style"].Value == "white-space:pre;")
                            {
                                linha += "\"" + td.InnerText.Trim().Replace(";", "").Replace("\r\n", ",") + "\"" + ";";
                            }
                            else
                            {
                                linha += "\"" + td.InnerText.Trim().Replace(";", "") + "\"" + ";";
                            }
                        }
                        //remove a ultima virgula
                        sb.AppendLine(linha.Substring(0, linha.Length - 1));
                    }

                    linha = null;
                }
            }


            //Relatorio Mensal
            if (doc.DocumentNode.SelectNodes("//table[contains(@class, 'mensal')]//tr") != null)
            {
                foreach (HtmlNode tbResultado in doc.DocumentNode.SelectNodes("//table[contains(@class, 'mensal')]//tr"))
                {
                    linha += "\"" + Regex.Replace(tbResultado.InnerText.Trim().Replace(";", "").Replace("\r\n", "\";\""), "\"\\s+", "\"") + "\"" + ";";

                    sb.AppendLine(linha.Substring(0, linha.Length - 1));

                    linha = null;
                }
            }

            //Relatorio de Categoria
            if (doc.DocumentNode.SelectNodes("//a[@class='jstree-anchor']") != null)
            {
                foreach (HtmlNode tbResultado in doc.DocumentNode.SelectNodes("//a[@class='jstree-anchor']"))
                {
                    linha += "\"" + tbResultado.InnerText.Trim().Replace(";", "") + "\"" + ";";


                    for (int i = 0; i < Regex.Matches(tbResultado.XPath, "ul").Count - 1; i++)
                    {
                        linha = ";" + linha;
                    }

                    sb.AppendLine(linha.Substring(0, linha.Length - 1));

                    linha = null;
                }
            }


            //Relatorio Avaliacoes
            if (doc.DocumentNode.SelectNodes("//div[contains(@class,'blocoAvaliacao')]") != null)
            {
                foreach (HtmlNode tbResultado in doc.DocumentNode.SelectNodes("//div[contains(@class,'blocoAvaliacao')]"))
                {
                    HtmlAgilityPack.HtmlDocument docTR = new HtmlDocument();
                    docTR.LoadHtml(tbResultado.InnerHtml);
                    if (docTR.DocumentNode.SelectNodes("//span[@class='itemBlocoAvaliacao ng-binding']") != null)
                    {
                        foreach (HtmlNode span in docTR.DocumentNode.SelectNodes("//span[@class='itemBlocoAvaliacao ng-binding']"))
                        {
                            linha += "\"" + span.InnerText.Trim().Replace(";", "") + "\"" + ";";
                        }
                        //remove a ultima virgula
                        sb.AppendLine(linha.Substring(0, linha.Length - 1));
                    }
                    else
                    {
                        linha += "\"" + tbResultado.InnerText.Trim().Replace(";", "") + "\"" + ";";
                        sb.AppendLine(linha.Substring(0, linha.Length - 1));
                    }

                    linha = null;
                }
            }


            context.Response.Clear();
            context.Response.ContentType = "application/csv";
            context.Response.ContentEncoding = System.Text.Encoding.Unicode;
            context.Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            context.Response.Write(sb.ToString());

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}