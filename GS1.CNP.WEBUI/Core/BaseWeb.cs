﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GS1.CNP.WEBUI.Core
{
    public abstract class BaseWeb : System.Web.UI.Page

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["USUARIO_LOGADO"] == null)
            {
                //Response.Redirect("/login.aspx");
                Response.Redirect("/");
            }
            else
            {

                if (Session["PERMISSOES_USUARIO_LOGADO"] != null || this.PermissaoPagina == null)
                {
                    if (this.PermissaoPagina != null)
                    {
                        //var listafuncionalidades = ((List<Object>)Session["PERMISSOES_USUARIO_LOGADO"]).Cast<dynamic>().Select(x => x.nome);
                        var listafuncionalidades = ((List<string>)Session["PERMISSOES_USUARIO_LOGADO"]);

                        var acesso = (from funcionalidade in listafuncionalidades
                                      where funcionalidade == this.PermissaoPagina
                                      select funcionalidade);

                        if (acesso.Count() == 0)
                        {
                            Response.Redirect("/views/403.html");
                        }
                    }
                }
                else
                {
                    //Response.Redirect("/login.aspx");
                    Response.Redirect("/");
                }

            }
        }

        public abstract string PermissaoPagina {get;}
        
    }
}