﻿using System;
using System.Web;
using GS1.CNP.BLL;
using System.IO;
using Newtonsoft.Json.Linq;
using GS1.CNP.BLL.Core;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for DownloadTemplateHandler
    /// </summary>
    public class DownloadTemplateHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //dynamic param = GetParameterHandler(context);

            int codigo = Convert.ToInt32(context.Request.Form["codigo"]),
                tipoModelo = Convert.ToInt32(context.Request.Form["tipoModelo"]),
                padraoGDSN = Convert.ToInt32(context.Request.Form["padraoGDSN"]);
            
            ImportarProdutosBO bo = new ImportarProdutosBO();
            bo.Download(codigo, tipoModelo, padraoGDSN);
        }

        private dynamic GetParameterHandler(HttpContext context)
        {
            if (context.Request.InputStream.CanSeek && context.Request.InputStream.Position > 0)
            {
                context.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            }

            if (context.Request.InputStream.Length > 0)
            {
                string jsonParameter = (new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd();

                JObject jObj = JObject.Parse(jsonParameter);

                dynamic param = Util.ConvertJTokenToObject(jObj);
                return param;
            }
            else
            {
                throw new GS1TradeException("ParameterHandler não informado.");
            }
        }

        public bool IsReusable { get { return false; } }
    }
}