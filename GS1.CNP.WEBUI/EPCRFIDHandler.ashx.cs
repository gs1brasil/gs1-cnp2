﻿using System.Web;
using System.Web.SessionState;
using GS1.CNP.BLL;
using Newtonsoft.Json;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for EPCRFIDHandler
    /// </summary>
    public class EPCRFIDHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            EPCRFIDBO epc = new EPCRFIDBO();
            epc.GerarBaixarEPCRFID(JsonConvert.DeserializeObject(context.Request.Form["epcdata"]));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
