﻿using System.Web;
using GS1.CNP.BLL;
using System.Web.SessionState;
using Newtonsoft.Json.Linq;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for ExportaProdutosHandler
    /// </summary>
    public class ExportaProdutosHandler : IHttpHandler, IReadOnlySessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            JObject jObj = JObject.Parse(context.Request.Form["campos"]);
            ProdutoBO bo = new ProdutoBO();
            bo.Exportar(jObj.ToObject<dynamic>());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}