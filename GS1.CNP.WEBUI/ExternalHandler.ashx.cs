﻿using System.Web;
using System.Web.SessionState;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL.Model;


namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for ExternalHandler
    /// </summary>
    public class ExternalHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {

            //string urlexterna = HttpUtility.UrlDecode((new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd());
            if (context.Request.QueryString.Count > 0 && context.Request.QueryString["URL"] != null && context.Request.QueryString["URL"] != string.Empty)
            {
                string urlexterna = context.Request.QueryString["URL"];
                Login user = (Login)HttpContext.Current.Session["USUARIO_LOGADO"];
                if (user != null)
                {
                    Log.WriteInfo(null, "URL: " + urlexterna, null);
                    context.Response.Redirect(urlexterna);
                }
                else
                {
                    context.Response.Redirect("/");
                }
            }
            else
            {
                context.Response.Redirect("/");
            }

            //context.Response.Redirect(urlexterna);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        
    }
}