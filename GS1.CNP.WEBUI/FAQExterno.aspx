﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FAQExterno.aspx.cs" Inherits="GS1.CNP.WEBUI.FAQExterno" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base href="/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GS1 CNP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Language" content="pt-BR">   
    
    <link href="Styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Scripts/npm.js" rel="stylesheet">
    <link href="Styles/login.css" rel="stylesheet">
    <link href="Styles/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <style>
        
        .gotham-font-stack {
            font-family: "Gotham SSm A", "Gotham SSm B", Verdana, sans-serif;
        }

        .gotham-narrow-font-stack {
            font-family: "Gotham Narrow SSm A", "Gotham Narrow SSm B", Verdana, sans-serif;
        }

        .gotham-alt-font-stack {
            font-family: "Gotham A", "Gotham B", Verdana, sans-serif;
        }
        a:link {
	        color: #939598;
	        text-decoration: none;
        }
        a:hover {
	        color: #939598;
	        text-decoration: none;
        }
        a:visited {
	        text-decoration: none;
        }
        a:active {
	        text-decoration: none;
        }
        .header{
	        width:100%;
	        font-size:24px;
	        color:#6D6E71;
	        position: fixed;
	        background: #FFFFFF no-repeat scroll center center;
	        z-index: 200;
	        margin: 0;
        }
        .container.bg
        {
            background-position: center bottom;
            min-height: 535px;
        }
        .form-box
        {
            top: 110px;
            position: relative;
        }
        
        .topTitleLogin {
            margin-top: 10px;
        }
    </style>

</head>
<body ng-app="CNP.Backoffice" ng-controller="FAQExternoCtrl" class="login-layout toolkit" ng-init="cnpjMode = false; viewForgot = false;">
    <div class="row-fluid">
    	<div class="row header" >
        	<div class="col-sm-12 col-md-3 col-lg-3 logoCadastro">
            	<center><img class="tamanhoImagemLogo" src="/images/login/logoCadastro.png"></center>
            </div>
            <div class="col-sm-12 col-md-9 col-lg-9 textoCadastro gotham-alt-font-stack">
            	<p class="textoLogo" style="vertical-align:middle;">Uma ferramenta simples e dinâmica para o cadastro e a gestão dos produtos da sua empresa.</p>
            </div>
        </div>
    
    	<div class="container bg" ng-init="viewLogin = true; exibeCPFCNPJ = false; viewForgot = false;" ng-cloak>
        	<div id="rowForm" class="row formularioLogin animate-show" style="display:none;">
            	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-7"></div>
                <div class="col-sm-8 col-md-7 col-lg-4">
            		<div id="login" class="form-box"> 
                        <a class="pull-right" style="float:right;text-decoration: none;" href="/FAQ"><i class="icon-question-sign" title="Ajuda"></i></a>
                		<img src="/images/login/login.png"> <div class="tituloFormulario gotham-alt-font-stack">Perguntas Frequentes - FAQ </div>
                        <form>
  							<div class="form-group">
    							<label class="fontenormal gotham-font-stack">Usuário</label>
    							<input type="email" class="form-control" placeholder="E-mail" id="user" name="user" ng-model="usuario.login" maxlength="255" required />
  							</div>
  							<div class="form-group">
    							<label for="exampleInputEmail1" class="fontenormal gotham-font-stack">Senha</label>
    							<input type="password" class="form-control" placeholder="Senha" id="pwd" name="pwd" ng-model="usuario.senha" maxlength="255" on-enter="onLogin()" required />
  							</div>
                            <div class="form-group" ng-show="exibeCPFCNPJ == true" ng-cloak>
                                <label for="cpfcnpj" class="fontenormal gotham-font-stack">CPF/CNPJ</label>
                                <input type="text" class="form-control" placeholder="CPF/CNPJ" id="cpfcnpj" name="cpfcnpj" on-enter="onLogin()" ng-model="usuario.cpfcnpj" required  onkeydown="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, ''); return (event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" onblur="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, '');" onkeyup="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, '');" />
                            </div>
                            <div class="form-group" ng-show="exibeCAPTCHA == true">
                                <div vc-recaptcha key="'6LeVSAwTAAAAAGEN6LLRvqFuzMhquH_NJIBjZu7a'" on-success="setResponse(response)"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button class="btn btn-primary" ng-click="onLogin()" type="button" style="width:100%; margin-bottom:5px;">
                                        Entrar
                                    </button>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button type="button" class="btn btn-danger" ng-click="onClean()" style="width:100%;">
                                        Cancelar
                                    </button>
                                </div>
                           	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 bot-pass-w" style="text-decoration: none; padding-top: 0;">
  									<button type="button" class="btn btn-link btn-xs" ng-click="e.preventDefault(); viewLogin = false; viewForgot = true;">
                                        <img src="/images/login/cadeado.png"> 
                                        <span style="color: #939598">Esqueci minha senha</span>
                                    </button>
                                </div>
                                <div class="col-md-4"></div>
                                
                           	</div>
						</form>
                	</div>
                </div>
                <div class="col-sm-12 col-md-1 col-lg-1"></div>
            </div>
        </div>

        <footer class="row-fluid text-right rodape">
            Powered by <img alt="" src="/images/login/rodape.png">
        </footer>
    </div>

    <!-- bower:js -->
    <script  type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/scripts/bootstrap.min.js" type="text/javascript"></script>

    <script  type="text/javascript" src="bower_components/angular/angular.js"></script>
    <script  type="text/javascript" src="bower_components/angular-cookies/angular-cookies.js"></script>
    <script  type="text/javascript" src="bower_components/angular-resource/angular-resource.js"></script>
    <script  type="text/javascript" src="bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script  type="text/javascript" src="bower_components/angular-route/angular-route.js"></script>
    <script  type="text/javascript" src="bower_components/angular-animate/angular-animate.js"></script>
    <script  type="text/javascript" src="bower_components/ng-table/ng-table.js"></script>
    <script type="text/javascript" src="/bower_components/angular-bootstrap/ui-bootstrap.js"></script>
    <script src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js" type="text/javascript"></script>

    <script  type="text/javascript" src="/bower_components/angular-block-ui/dist/angular-block-ui.min.js"></script> 
    <script type="text/javascript" src="/bower_components/ngScrollTo/ng-scrollto.js"></script>

    <script src="/bower_components/angular-x2js/dist/x2js.min.js" type="text/javascript"></script>
    <script src="/bower_components/x2js/xml2json.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/bower_components/angular-colorpicker/js/bootstrap-colorpicker-module.js"></script>
    <script src="/bower_components/angular-wysiwyg/dist/angular-wysiwyg.js" type="text/javascript"></script>

    <!-- Include the JS ReCaptcha API -->
    <script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>

    <script src="/bower_components/angular-recaptcha/release/angular-recaptcha.js" type="text/javascript"></script>
    
    <!-- Angular - Google Maps API -->
    <script type="text/javascript" src="/bower_components/lodash/lodash.min.js" ></script>
    <script type="text/javascript" src="/bower_components/angular-simple-logger/dist/angular-simple-logger.min.js" ></script>
    <script type="text/javascript" src="/bower_components/angular-google-maps/dist/angular-google-maps.js" ></script>
    
    <!-- build:js({.tmp,app}) scripts/scripts.js -->
    <script src="/scripts/backoffice/app.js" type="text/javascript"></script>
    <script src="/scripts/backoffice/core/app.directives.js" type="text/javascript"></script>

    <script type="text/javascript" src="/scripts/lib/mask.js"></script>
    <script type="text/javascript" src="/assets/js/angular-input-masks/masks.js"></script>

    <script src="scripts/recaptcha.config.js" type="text/javascript"></script>   
    <script  type="text/javascript" src="scripts/backoffice/core/controllers/loginctrl.js"></script>
    <script  type="text/javascript" src="scripts/backoffice/core/services/loginservice.js"></script>
    <script  type="text/javascript" src="scripts/backoffice/core/services/genericservice.js"></script>

</body>
</html>
