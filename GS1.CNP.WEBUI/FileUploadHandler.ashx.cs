﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using Newtonsoft.Json;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for FileUploadHandler
    /// </summary>
    public class FileUploadHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection SelectedFiles = context.Request.Files;
                HttpPostedFile PostedFile;
                List<string> list = new List<string>();
                string fileName,
                    extensao,
                    folder = @"/upload/importacaoProdutos/";

                if (!Directory.Exists(context.Server.MapPath(folder)))
                {
                    Directory.CreateDirectory(context.Server.MapPath(folder));
                }

                for (int i = 0; i < SelectedFiles.Count; i++)
                {
                    PostedFile = SelectedFiles[i];
                    extensao = Path.GetExtension(PostedFile.FileName);

                    if (!extensao.ToLower().Equals(".xlsx") && !extensao.ToLower().Equals(".csv"))
                    {
                        //throw new Exception("Extensão não suportada");
                        
                        context.Response.Write(JsonConvert.SerializeObject(@"Extensão não suportada"));
                        context.Response.End();
                    }
                        

                    try
                    {
                        fileName = folder + PostedFile.FileName;
                        PostedFile.SaveAs(context.Server.MapPath(fileName));
                        list.Add(fileName);
                    }
                    catch(Exception ex)
                    {
                        //throw new Exception("Não foi possível fazer o upload do arquivo.");
                        context.Response.Write(JsonConvert.SerializeObject(String.Format(@"Não foi possível fazer o upload do arquivo. {0}",ex.Message)));
                        context.Response.End();
                    }
                }

                context.Response.Clear();
                context.Response.ContentType = "text/plain";
                context.Response.Write(JsonConvert.SerializeObject(list));
                context.Response.End();
        }
        else
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("[]");
            context.Response.End();
        }
    }

        public bool IsReusable { get { return false; } }
    }
}