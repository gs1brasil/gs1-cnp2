﻿using System;
using System.Web;
using System.IO;
using GS1.CNP.BLL.Core;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using System.Configuration;

namespace CNP
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(HttpContext.Current.Server.MapPath("log4net.config")));
        }

        void Application_End(object sender, EventArgs e) { }

        void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            if (exception != null)
            {
                const string ROOT_DOCUMENT_BACKOFFICE = "/views/backoffice/interna.aspx";
                const string ROOT_DOCUMENT = "/index.aspx";

                GS1TradeException gtException;
                bool isGTException;

                if (exception is TargetInvocationException && exception.InnerException != null)
                    exception = exception.InnerException;

                isGTException = exception is GS1TradeException;

                if (isGTException)
                    gtException = exception as GS1TradeException;
                else
                    gtException = new GS1TradeException(exception.Message, exception);

                Log.WriteError(gtException.Message, gtException);

                Server.ClearError();

                if (Context.Handler != null)
                {
                    //Limpa o cache
                    TimeSpan refresh = new TimeSpan(0, 0, 1);
                    Response.Cache.SetExpires(DateTime.Now.Add(refresh));
                    Response.Cache.SetMaxAge(refresh);
                    Response.Cache.SetCacheability(HttpCacheability.Server);
                    Response.Cache.SetValidUntilExpires(true);

                    Response.SetCookie(new HttpCookie("fileDownload", "false") { Path = "/" });
                    Response.ContentType = "application/json";
                    Response.ContentEncoding = Encoding.UTF8;
                    if (isGTException)
                        Response.StatusCode = 420;
                    else
                        Response.StatusCode = 419;

                    Response.Write(JsonConvert.SerializeObject(gtException));
                }
                else
                {
                    string url = Request.Url.LocalPath;
                    if (Request.RawUrl.Contains("/backoffice") || Request.RawUrl.Contains("/b#") || url.Equals("/b"))
                        Context.RewritePath(ROOT_DOCUMENT_BACKOFFICE, "", Request.QueryString.ToString());
                    else
                        Context.RewritePath(ROOT_DOCUMENT, "", Request.QueryString.ToString());
                }
            }
        }

        void Session_Start(object sender, EventArgs e)
        {
            HttpRequest request = HttpContext.Current.Request;
            HttpBrowserCapabilities browser = request.Browser;

            //Session.Add("Browser", browser);
        }

        void Session_End(object sender, EventArgs e)
        {
        }

        void Application_BeginRequest(Object sender, EventArgs e)
        {
            string pathHref = HttpContext.Current.Request.Url.ToString();
            string urlRetorno;

            if (!pathHref.Contains("/WebServices/AuthService.asmx") && !pathHref.Contains("/jsnlog.logger"))
            {
                if (pathHref.Contains("url"))
                    urlRetorno = HttpContext.Current.Request.Url.Query.ToString();
                else
                    urlRetorno = "?url=" + HttpContext.Current.Request.Url.AbsolutePath.ToString();

                const string ROOT_DOCUMENT_BACKOFFICE = "/views/backoffice/interna.aspx";
                const string ROOT_DOCUMENT = "/index.aspx";
                const string LOGIN_DOCUMENT = "/login.aspx";
                const string DETALHESAJUDA_DOCUMENT = "/views/backoffice/detalhesAjuda.aspx";
                const string DETALHESAJUDAPASSOAPASSO_DOCUMENT = "/views/backoffice/detalhesAjudaPassoaPasso.aspx";

                string url = Request.Url.LocalPath;

                if (!File.Exists(Context.Server.MapPath(url)) && !url.Contains(".aspx") && !url.Contains(".ashx") && !url.Contains(".woff"))
                {
                    if (((Request.RawUrl.Contains("/backoffice") && !Request.RawUrl.Contains("/detalhesAjuda")) && !Request.RawUrl.Contains("url")) || (Request.RawUrl.Contains("/b#") && !Request.RawUrl.Contains("url")) || (url.Equals("/b") && !Request.RawUrl.Contains("url")))
                        Context.RewritePath(ROOT_DOCUMENT_BACKOFFICE + urlRetorno);
                    else if (Request.RawUrl.Equals("/login"))
                        Context.RewritePath(LOGIN_DOCUMENT);
                    else if (Request.RawUrl.Contains("/detalhesAjudaPassoaPasso"))
                    {
                        if (!Request.RawUrl.Equals("/backoffice/detalhesAjudaPassoaPasso"))
                            Context.RewritePath(DETALHESAJUDAPASSOAPASSO_DOCUMENT);
                        else
                            Context.RewritePath(ROOT_DOCUMENT_BACKOFFICE + urlRetorno);
                    }
                    else if (Request.RawUrl.Contains("/detalhesAjuda"))
                    {
                        if(!Request.RawUrl.Equals("/backoffice/detalhesAjuda"))
                            Context.RewritePath(DETALHESAJUDA_DOCUMENT);
                        else
                            Context.RewritePath(ROOT_DOCUMENT_BACKOFFICE + urlRetorno);
                    }
                    else
                        Context.RewritePath(ROOT_DOCUMENT);
                }

                if (pathHref.Contains("produtoctrl.js") && ConfigurationManager.AppSettings["Versao"] != null && !pathHref.Contains("?versao="))
                {
                    //Context.RewritePath(HttpContext.Current.Request.Url.AbsolutePath + "?versao=" + ConfigurationManager.AppSettings["Versao"]);
                    HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.AbsolutePath + "?versao=" + ConfigurationManager.AppSettings["Versao"]);
                }
                        

            }

            if (pathHref.Contains("/jsnlog.logger"))
            {
                Log.WriteJsnLog();
                Context.Response.End();   
                
            }

        }
    }
}
