﻿using System;
using System.Web;
using System.IO;
using System.Drawing.Imaging;

namespace GS1.CNP.WEBUI
{
    public class ImageHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string html = HttpUtility.UrlDecode((new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd());
            if (html != String.Empty && html.Length > 10)
            {
                html = html.Substring(10);
            }

            
            string largura = "";
            if (context.Request.QueryString != null && context.Request.QueryString["largura"] != null)
            {
                largura = context.Request.QueryString["largura"];
            }
            string altura = "";
            if (context.Request.QueryString != null && context.Request.QueryString["altura"] != null)
            {
                altura = context.Request.QueryString["altura"];
            }


            var htmlToImageConv = new NReco.ImageGenerator.HtmlToImageConverter();
            //htmlToImageConv.CustomArgs = "--dpi 300";

            //if (largura != string.Empty)
            //    htmlToImageConv.Width = (int)(Math.Ceiling(double.Parse(largura, CultureInfo.InvariantCulture)));
            //else
            //    htmlToImageConv.Width = 312;

            //if (altura != string.Empty)
            //    htmlToImageConv.Height = (int)(Math.Ceiling(double.Parse(altura, CultureInfo.InvariantCulture)));
            //else
            //    htmlToImageConv.Height = 120;


            byte[] imageBytes = htmlToImageConv.GenerateImage(html, ImageFormat.Png.ToString());

            using (MemoryStream ms = new MemoryStream(imageBytes))
            {                 
                context.Response.Clear();
                context.Response.AddHeader("Content-Type", "image/png");
                context.Response.AddHeader("Content-Length", ms.ToArray().Length.ToString());
                context.Response.AddHeader("Content-Disposition", "attachment; filename=codigodebarras.png");
                context.Response.AddHeader("Cache-Control", "max-age=0");
                context.Response.AddHeader("Accept-Ranges", "none");
                context.Response.BinaryWrite(ms.ToArray());
                context.Response.Flush();
                context.SkipAuthorization = true;
            }
           
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}