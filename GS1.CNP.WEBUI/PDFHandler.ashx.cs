﻿using System;
using System.Web;
using System.Dynamic;
using System.IO;
using System.Web.SessionState;
using GS1.CNP.BLL.Core;
using System.Globalization;
using Newtonsoft.Json;
using GS1.CNP.BLL;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for PDFHandler
    /// </summary>
    public class PDFHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string html = HttpUtility.UrlDecode((new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd());
            if (html != String.Empty && html.Length > 8)
            {
                html = html.Substring(8);
            }

            string nomerelatorio = "";
            if (context.Request.QueryString != null && context.Request.QueryString["nomeetiqueta"] != null)
            {
                nomerelatorio = context.Request.QueryString["nomeetiqueta"];
            }


            string largurapagina = "";
            if (context.Request.QueryString != null && context.Request.QueryString["largurapagina"] != null)
            {
                largurapagina = context.Request.QueryString["largurapagina"];
            }
            string alturapagina = "";
            if (context.Request.QueryString != null && context.Request.QueryString["alturapagina"] != null)
            {
                alturapagina = context.Request.QueryString["alturapagina"];
            }
            string tamanhofolha = "";
            if (context.Request.QueryString != null && context.Request.QueryString["tamanhofolha"] != null)
            {
                tamanhofolha = context.Request.QueryString["tamanhofolha"];
            }

            string margemesquerda = "";
            if (context.Request.QueryString != null && context.Request.QueryString["margemesquerda"] != null)
            {
                margemesquerda = context.Request.QueryString["margemesquerda"];
            }
            string margemdireita = "";
            if (context.Request.QueryString != null && context.Request.QueryString["margemdireita"] != null)
            {
                margemdireita = context.Request.QueryString["margemdireita"];
            }
            string margemsuperior = "";
            if (context.Request.QueryString != null && context.Request.QueryString["margemsuperior"] != null)
            {
                margemsuperior = context.Request.QueryString["margemsuperior"];
            }
             string margeminferior = "";
            if (context.Request.QueryString != null && context.Request.QueryString["margeminferior"] != null)
            {
                margeminferior = context.Request.QueryString["margeminferior"];
            }

            dynamic where = null;
            dynamic parametro = new ExpandoObject();
            parametro.where = new ExpandoObject();

            if (context.Request.QueryString != null && context.Request.QueryString["where"] != null)
            {
                where = JsonConvert.DeserializeObject(context.Request.QueryString["where"]);

                parametro.where.alturacodigobarras = where.height;
                parametro.where.larguracodigobarras = where.width;
                parametro.where.idealmodulewidth = where.idealmodulewidth;
                parametro.where.leftguard = where.leftguard;
                parametro.where.magnificationfactor = where.magnificationfactor;
                parametro.where.rightguard = where.rightguard;
            }

            dynamic campos = null;
            if (context.Request.QueryString != null && context.Request.QueryString["campos"] != null)
            {
                campos = JsonConvert.DeserializeObject(context.Request.QueryString["campos"]);

                parametro.where.codigotipocodigobarras = campos.tipoetiqueta;
                parametro.where.codigogtin = campos.globaltradeitemnumber;
                parametro.where.altura = campos.altura;
                parametro.where.largura = campos.largura;
                parametro.where.descricao = campos.descricao;
                parametro.where.textolivre = campos.textolivre;
                parametro.where.codigoposicao = campos.codigoposicao;
                parametro.where.fontenome = campos.fontenome;
                parametro.where.fontetamanho = campos.fontetamanho;
                parametro.where.modelo128 = campos.modelo128;
                parametro.where.nrlote = campos.nrlote;
                parametro.where.datavencimento = campos.datavencimento;
                parametro.where.codigotipogtin = campos.codigotipogtin;
                parametro.where.quantidadeporcaixa = campos.quantidadeporcaixa;
                parametro.where.codigohomologacao = campos.registroanvisa;
                parametro.where.serialinicial = campos.serialinicial;
                parametro.where.serialfim = campos.serialfim;
            }

            if (campos != null && where != null && campos.tipoetiqueta == 9)
            {
                EtiquetaBO etiquetaBO = new EtiquetaBO();
                if(campos.serialinicial != null) {  //&& campos.serialfim != null

                    long valorfinal = (long)(campos.serialfim != null && campos.serialfim != "" ? campos.serialfim : campos.serialinicial);

                    for (long i = (long)campos.serialinicial; i <= valorfinal; i++)
                    {
                        parametro.where.codigoserial = i;
                        string resultadoEtiqueta = etiquetaBO.GerarCodigoBarras(parametro);

                        if (!string.IsNullOrEmpty(resultadoEtiqueta))
                        {
                            dynamic resultadoDeserializado = JsonConvert.DeserializeObject(resultadoEtiqueta);
                            html = html.Replace("//[[[img" + i, "data:image/png;base64," + resultadoDeserializado.imagedata.ToString());
                        }                        
                    }
                }
            }

            string filename = "";
            //if (context.Request.UrlReferrer != null && context.Request.UrlReferrer.Segments.Length > 2)
            //{
            //    filename = context.Request.UrlReferrer.Segments[2] + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";
            //}
            //else 
            if (!string.IsNullOrEmpty(nomerelatorio))
            {
                filename = Util.TratarNomeArquivo(nomerelatorio) + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";
            }
            else
            {
                filename = "etiqueta_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";
            }

            //NReco.PdfGenerator.HtmlToPdfConverter pdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            pdf.CustomWkHtmlArgs = "--encoding UTF-8 --disable-smart-shrinking";
            //pdf.CustomWkHtmlArgs = " --load-media-error-handling ignore ";

            if(tamanhofolha == "CARTA"){
                pdf.Size = NReco.PdfGenerator.PageSize.Letter;
            }else{
                pdf.Size = NReco.PdfGenerator.PageSize.A4;
            }

            //if (largurapagina != string.Empty)
            //    pdf.PageWidth = float.Parse(largurapagina, CultureInfo.InvariantCulture);
            //else
            //    pdf.PageWidth = 210;

            //if (alturapagina != string.Empty)
            //    pdf.PageHeight = float.Parse(alturapagina, CultureInfo.InvariantCulture);
            //else
                //pdf.PageHeight = 297;


            if(margemesquerda != string.Empty)
                pdf.Margins.Left = float.Parse(margemesquerda, CultureInfo.InvariantCulture);
            else
                pdf.Margins.Left = 5;

            if(margemdireita != string.Empty)
                pdf.Margins.Right = float.Parse(margemdireita, CultureInfo.InvariantCulture);
            else
                pdf.Margins.Right = 5;

            if(margemsuperior != string.Empty)
                pdf.Margins.Top = float.Parse(margemsuperior, CultureInfo.InvariantCulture);
            else
                pdf.Margins.Top = 15;

            if (margeminferior != string.Empty)
                pdf.Margins.Bottom = float.Parse(margeminferior, CultureInfo.InvariantCulture);
            else
                pdf.Margins.Bottom = 15;
            
            if (context.Request.QueryString != null && context.Request.QueryString["orientacaopapel"] != null)
            {
                if (context.Request.QueryString["orientacaopapel"].Equals("L"))
                {
                    pdf.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                }
            }

            //pdf.Size = NReco.PdfGenerator.PageSize.A4;
            pdf.Zoom = 1.0F;

            var pdfBytes = pdf.GeneratePdf(html);

            context.Response.Clear();
            context.Response.AddHeader("Content-Type", "application/pdf");
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            context.Response.AddHeader("Content-Length", pdfBytes.Length.ToString());
            context.Response.AddHeader("Cache-Control", "max-age=0");
            context.Response.AddHeader("Accept-Ranges", "none");
            context.Response.BinaryWrite(pdfBytes);
            context.Response.Flush();
            context.SkipAuthorization = true;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}