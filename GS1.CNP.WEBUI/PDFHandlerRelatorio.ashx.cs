﻿using System;
using System.Web;
using System.IO;
using System.Web.SessionState;
using GS1.CNP.BLL.Core;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for PDFHandlerRelatorio1
    /// </summary>
    public class PDFHandlerRelatorio : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {

            string html = HttpUtility.UrlDecode((new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd());
            if (html != String.Empty && html.Length > 8)
            {
                html = html.Substring(8);
            }

            string nomerelatorio = "";
            if (context.Request.QueryString != null && context.Request.QueryString["nomerelatorio"] != null)
            {
                nomerelatorio = context.Request.QueryString["nomerelatorio"];
            }

            string filename = "";
            if (context.Request.UrlReferrer != null && context.Request.UrlReferrer.Segments.Length > 2)
            {
                filename = context.Request.UrlReferrer.Segments[2] + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";
            }
            else if (!string.IsNullOrEmpty(nomerelatorio))
            {
                filename = Util.TratarNomeArquivo(nomerelatorio) + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";
            }
            else
            {
                filename = "relatorioCNP_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";
            }

            NReco.PdfGenerator.HtmlToPdfConverter pdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            //pdf.PageHeaderHtml = @"<div styl><img src='" + context.Request.Url.Scheme + "://" + context.Request.UrlReferrer.Authority + "/assets/images/logo.png' style='height:50px;width: 78px;'></div>";
            pdf.PageHeaderHtml = @"<div style='width:20%;float:left;margin-top:5px'><img src='" + context.Request.Url.Scheme + "://" + context.Request.UrlReferrer.Authority + "/assets/images/logo.png' style='height:50px;width:100px;'></div><div style='text-align: center; float:right;height:0px;width:90%;margin-top:-55px'><h3>" + nomerelatorio + @"</h3></div><div style='clear:both;'></div><br>";
            pdf.PageFooterHtml = @"<div style='font-size:8px'>Gerado em: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "</div>";

            pdf.CustomWkHtmlArgs = "--encoding UTF-8";

            if (context.Request.QueryString != null && context.Request.QueryString["orientacaorelatorio"] != null)
            {
                if (context.Request.QueryString["orientacaorelatorio"].Equals("L"))
                {
                    pdf.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                }
            }

            var pdfBytes = pdf.GeneratePdf(CreatePDFScript() + html);

            context.Response.Clear();
            context.Response.AddHeader("Content-Type", "application/pdf");
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            context.Response.AddHeader("Content-Length", pdfBytes.Length.ToString());
            context.Response.AddHeader("Cache-Control", "max-age=0");
            context.Response.AddHeader("Accept-Ranges", "none");
            context.Response.BinaryWrite(pdfBytes);
            context.Response.Flush();
            context.SkipAuthorization = true;

        }

        private string CreatePDFScript()
        {
            return "<html><head><style>td,th{line-height:20px;padding: 8px;} tr { page-break-inside: avoid }</style><script>function subst() {var vars={};var x=document.location.search.substring(1).split('&');for(var i in x) {var z=x[i].split('=',2);vars[z[0]] = unescape(z[1]);}" +
            "var x=['frompage','topage','page','webpage','section','subsection','subsubsection'];for(var i in x) {var y = document.getElementsByClassName(x[i]);" +
            "for(var j=0; j<y.length; ++j) y[j].textContent = vars[x[i]];}}</script></head><body onload=\"subst()\">";
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}