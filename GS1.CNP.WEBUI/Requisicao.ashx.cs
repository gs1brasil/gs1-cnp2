﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Reflection;
using System.Web.SessionState;
using GS1.CNP.BLL.Core;
using System.Dynamic;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for Requisicao
    /// </summary>
    public class Requisicao : IHttpHandler, IRequiresSessionState
    {
        static string BO_NAMESPACE = "GS1.CNP.BLL";

        public bool IsReusable { get { return false; } }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;

                ParameterHandler param = GetParameterHandler(context);
                if (param != null && 
                    !(  param.nomeBO.ToLower().Equals("loginbo")

                        && (
                            param.nomeMetodo.ToLower().Equals("logar") ||
                            param.nomeMetodo.ToLower().Equals("verificausuariologado") || param.nomeMetodo.ToLower().Equals("recupera") ||
                            param.nomeMetodo.ToLower().Equals("carregarparametroslogin") || param.nomeMetodo.ToLower().Equals("buscarfaqs") 
                    
                        )
                    )
                    && !(param.nomeBO.ToLower().Equals("idiomabo") && param.nomeMetodo.ToLower().Equals("buscartraducaohome"))
                    && !(param.nomeBO.ToLower().Equals("idiomabo") && param.nomeMetodo.ToLower().Equals("buscaridiomas"))
                    && (context.Session["USUARIO_LOGADO"] == null || context.Session["PERMISSOES_USUARIO_LOGADO"] == null)
                    )
                {
                    context.Response.StatusCode = 401;
                    context.Response.Write(JsonConvert.SerializeObject("Unauthorized"));
                    context.Response.End();
                }

                if (!string.IsNullOrEmpty(param.nomeBO))
                {
                    Type BOClassType = null;
                    MethodInfo method = null;
                    object BOObject = null;
                    object retorno = null;

                    Assembly assembly = Assembly.Load(BO_NAMESPACE);

                    BOClassType = assembly.GetType(String.Format("{0}.{1}", BO_NAMESPACE, param.nomeBO));
                    BOObject = Activator.CreateInstance(BOClassType);

                    method = BOClassType.GetMethod(param.nomeMetodo);

                    if (method != null)
                    {
                        GS1CNPAttribute attrBO = BOClassType.GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute;
                        GS1CNPAttribute attrMethod = method.GetCustomAttributes(typeof(GS1CNPAttribute), true).FirstOrDefault() as GS1CNPAttribute;

                        if (attrBO != null && attrMethod != null)
                        {
                            if (attrMethod.ValidarPermissao)
                            {
                                List<string> permissoes = context.Session["PERMISSOES_USUARIO_LOGADO"] as List<string>;

                                //if (permissoes == null || permissoes.Count == 0 || !param.nomeBO.Equals(attrBO.ChavePermissao) ||
                                //    !permissoes.Contains(attrMethod.ChavePermissao))
                                if(!attrMethod.GS1CNPValidaPermissao(permissoes))
                                {
                                    context.Response.StatusCode = 403;
                                    context.Response.Write(JsonConvert.SerializeObject("Forbidden"));
                                    context.Response.End();
                                }
                            }
                        }
                        else
                        {
                            context.Response.StatusCode = 403;
                            context.Response.Write(JsonConvert.SerializeObject("Forbidden"));
                            context.Response.End();
                        }

                        if (param != null && attrMethod != null && attrMethod.LogarMetodo)
                        {
                            Log.WriteInfo(param.nomeBO, param.nomeMetodo);
                        }

                        try
                        {
                            Object[] parameter = new Object[] { param };
                            retorno = BOClassType.InvokeMember(param.nomeMetodo, BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public, null, BOObject, parameter);
                        }
                        catch (Exception ex)
                        {
                            if (ex.InnerException != null)
                            {
                                //TODO - remover stack do alert
                                throw new GS1TradeException(ex.InnerException.Message, ex.InnerException);
                            }
                            else
                            {
                                //throw new GS1TradeException("Erro ao efetuar requisição.");
                                string telefoneGs1 = string.Empty;
                                try
                                {
                                    GS1.CNP.BLL.LoginBO loginBO = new GS1.CNP.BLL.LoginBO();
                                    telefoneGs1 = (string)(loginBO.CarregarParametrosLoginBO("bo.nu.telefone")[0].valor);                                    
                                }
                                catch (Exception)
                                {
                                    throw new GS1TradeException("Não foi possível executar a requisição. Por favor, entre em contato com a GS1 Brasil.", ex);
                                }

                                throw new GS1TradeException("Não foi possível executar a requisição. Por favor, entre em contato com a GS1 Brasil: Tel. " + telefoneGs1 + " ou através do nosso site.", ex);

                                
                            }
                        }
                    }
                    else
                    {
                        throw new GS1TradeException("Método não informado");
                    }

                    if (retorno is List<dynamic> || retorno is IEnumerable<dynamic> || retorno is ExpandoObject)
                        context.Response.Write(JsonConvert.SerializeObject(retorno));
                    else
                        context.Response.Write(retorno);
                }
                else
                {
                    throw new GS1TradeException("Service não informado");
                }
            }
            catch (TimeoutException ex)
            {
                throw new GS1TradeException("Erro de conexão com o banco de dados. Tente novamente.", ex);
            }
        }

        private ParameterHandler GetParameterHandler(HttpContext context)
        {
            string nomeBO = string.Empty,
                nomeMetodo = string.Empty;

            this.GetMethodInfo(context, out nomeBO, out nomeMetodo);

            if (context.Request.InputStream.CanSeek && context.Request.InputStream.Position > 0)
            {
                context.Request.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
            }

            if (context.Request.InputStream.Length > 0)
            {
                string jsonParameter = (new StreamReader(context.Request.InputStream, context.Request.ContentEncoding)).ReadToEnd();

                JObject jObj = JObject.Parse(jsonParameter);

                ParameterHandler parametro = jObj.ToObject<ParameterHandler>();

                if (parametro != null)
                {
                    parametro.nomeBO = nomeBO;
                    parametro.nomeMetodo = nomeMetodo;
                }

                return parametro;
            }
            else if (!string.IsNullOrEmpty(nomeBO) && !string.IsNullOrEmpty(nomeMetodo))
            {
                return new ParameterHandler() { nomeBO = nomeBO, nomeMetodo = nomeMetodo };
            }
            else
            {
                throw new GS1TradeException("ParameterHandler não informado.");
            }
        }

        private void GetMethodInfo(HttpContext context, out string boName, out string methodName)
        {
            boName = string.Empty;
            methodName = string.Empty;

            if (context != null)
            {
                string pathInfo = string.Empty;

                pathInfo = context.Request.PathInfo;

                if (string.IsNullOrEmpty(pathInfo))
                {
                    throw new GS1TradeException("Método não informado");
                }

                if (pathInfo.StartsWith("/"))
                {
                    pathInfo = pathInfo.Substring(1);
                }

                string[] path = pathInfo.Split('/');

                boName = (path.Length > 0 ? path[0] : string.Empty);
                methodName = (path.Length > 1 ? path[1] : string.Empty);
            }
        }
    }

    public class ParameterHandler
    {
        public string nomeBO { get; set; }
        public string nomeMetodo { get; set; }
        public dynamic campos { get; set; }
        public dynamic where { get; set; }
        public string sql { get; set; }
        public string tipoCampo { get; set; }
        public int registroPorPagina { get; set; }
        public int paginaAtual { get; set; }
        public string ordenacao { get; set; }
    }
}