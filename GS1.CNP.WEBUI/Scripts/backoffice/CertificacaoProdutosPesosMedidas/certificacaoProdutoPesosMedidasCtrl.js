app.controller('CertificacaoProdutoPesosMedidasCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {
        //inicializando abas do form
        $('#tabs-etiqueta a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({ html: true });
        });

        var mensagem = $translate.instant('46.LBL_TitlePesosMedidas');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '46.LBL_TitlePesosMedidas' ? 'Certificação de Produtos - Pesos e Medidas' : mensagem;

        $scope.isPesquisa = false;
        $scope.item = {};

        $scope.tableCertificacaoProduto = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridCertificacaoProduto($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        function carregaGridCertificacaoProduto(item, isPesquisa, callback) {
            var dados = null;

            $rootScope.verificaAssociadoSelecionado(function (data) {
                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                    if (isPesquisa) {
                        dados = angular.copy(item);

                        genericService.postRequisicao('CertificacaoProdutosPesosMedidasBO', 'PesquisarCertificacoesPesosMedidas', { campos: dados }).then(
                            function (result) {

                                $scope.form.submitted = false;
                                $scope.editMode = false;

                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                         );
                    }
                    else if (!isPesquisa) {

                        genericService.postRequisicao('CertificacaoProdutosPesosMedidasBO', 'BuscarCertificacoesPesosMedidas', { campos: dados }).then(
                            function (result) {
                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                         );
                    }

                    $rootScope.flagAssociadoSelecionado = true;
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }
            });
        }

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('CertificacaoProdutosPesosMedidasBO', 'InsereCertificacaoPesosMedidas', function () {
                $scope.item = {};

                var mensagem = $translate.instant('46.LBL_TitlePesosMedidas');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '46.LBL_TitlePesosMedidas' ? 'Certificação de Produtos - Pesos e Medidas' : mensagem;
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isCollapsedMidias = true;
                $scope.initMode = true;
                $scope.exibeVoltar = true;
                $scope.isPesquisa = false;
            });
        }

        //Clique no botão fechar
        $scope.onCancel = function (item) {
            $scope.item = {};
            var mensagem = $translate.instant('46.LBL_TitlePesosMedidas');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '46.LBL_TitlePesosMedidas' ? 'Certificação de Produtos - Pesos e Medidas' : mensagem;
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.gerar = false;
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableCertificacaoProduto.page(1);
            $scope.tableCertificacaoProduto.reload();
        }

        //Clique no botão pesquisar
        $scope.onSearch = function (item) {
            var mensagem = $translate.instant('46.LBL_TitlePesosMedidas');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '46.LBL_TitlePesosMedidas' ? 'Certificação de Produtos - Pesos e Medidas' : mensagem;
            $scope.item = angular.copy(item);
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableCertificacaoProduto.page(1);
            $scope.tableCertificacaoProduto.reload();
        }

        $scope.onSearchMode = function (item) {
            $scope.item = {};

            var mensagem = $translate.instant('46.LBL_TitlePesquisarCertificacaoProdutos');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '46.LBL_TitlePesquisarCertificacaoProdutos' ? 'Pesquisar Certificação de Produtos' : mensagem
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.initMode = true;
            $scope.exibeVoltar = true;
        }

        //Calcular campo Data de Validade, sendo esse valor parametrizado em meses
        $scope.calcularValidade = function (data) {

            if ($scope.item != undefined && $scope.item.datacertificado != undefined) {

                var numeromeses = $rootScope.retornaValorParametroByFiltro('certificacao.numeses.vencimento.pesosmedidas');
                $scope.item.datavalidade = Utils.MonthToAddDate(Utils.dataMesDiaAno(data), numeromeses);
            }
        }

        //Clique no botão limpar
        $scope.onClean = function (item) {
            $scope.item = {};
        }

        //Salva na base os item gerado
        $scope.onSave = function (item) {

            var dataCertificado = Utils.dataMesDiaAno(item.datacertificado);
            var dataValidade = Utils.dataMesDiaAno(item.datavalidade);

            if (!$scope.form.$valid || item.globaltradeitemnumber == undefined || item.globaltradeitemnumber == null) {

                var mensagem = $translate.instant('46.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '46.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
            }
            else {

                //Inserção de Certificado Pesos e Medidas
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {

                    if (!Utils.isValidDate(Utils.dataAnoMesDia(item.datacertificado))) {

                        var mensagem = $translate.instant('46.MENSAGEM_CertificacaoInvalida');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '46.MENSAGEM_CertificacaoInvalida' ? 'A Data de Certificação informada está inválida.' : mensagem);
                        return false;
                    }
                    else if (!Utils.isValidDate(Utils.dataAnoMesDia(item.datavalidade))) {

                        var mensagem = $translate.instant('46.MENSAGEM_ValidadeInvalida');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '46.MENSAGEM_ValidadeInvalida' ? 'A Data de Validade informada está inválida.' : mensagem);
                        
                        return false;
                    }
                    else {
                        var parametro = { status: item.status, datacertificado: dataCertificado, datavalidade: dataValidade, codigoproduto: item.codigoproduto };

                        genericService.postRequisicao('CertificacaoProdutosPesosMedidasBO', 'InsereCertificacaoPesosMedidas', { campos: parametro }).then(
                            function (result) {

                                if (result != undefined && result != null && result.data != null) {
                                    $scope.item = {};
                                    $scope.editMode = false;
                                    $scope.searchMode = false;
                                    $scope.form.submitted = false;
                                    $scope.isPesquisa = false;
                                    $scope.exibeVoltar = false;
                                    $scope.tableCertificacaoProduto.page(1);
                                    $scope.tableCertificacaoProduto.reload();
                                    $scope.initMode = false;
                                    Utils.moveScrollTop();

                                    var mensagem = $translate.instant('46.MENSAGEM_RegistroSalvoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '46.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);

                                }
                            },
                            $rootScope.TratarErro
                        );
                    }

                }
                //Atualização de Certificado Pesos e Medidas
                else {
                    if (!Utils.isValidDate(Utils.dataAnoMesDia(item.datacertificado))) {

                        var mensagem = $translate.instant('46.MENSAGEM_CertificacaoInvalida');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '46.MENSAGEM_CertificacaoInvalida' ? 'A Data de Certificação informada está inválida.' : mensagem);

                        return false;
                    }
                    else if (!Utils.isValidDate(Utils.dataAnoMesDia(item.datavalidade))) {

                        var mensagem = $translate.instant('46.MENSAGEM_ValidadeInvalida');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '46.MENSAGEM_ValidadeInvalida' ? 'A Data de Validade informada está inválida.' : mensagem);

                        return false;
                    }
                    else {

                        var parametro = { codigo: item.codigo, status: item.status, datacertificado: dataCertificado, datavalidade: dataValidade, codigoproduto: item.codigoproduto };

                        genericService.postRequisicao('CertificacaoProdutosPesosMedidasBO', 'EditarCertificacaoPesosMedidas', { campos: parametro, where: item.codigo }).then(
                            function (result) {
                                $scope.item = {};
                                $scope.editMode = false;
                                $scope.searchMode = false;
                                $scope.form.submitted = false;
                                $scope.isPesquisa = false;
                                $scope.exibeVoltar = false;
                                Utils.moveScrollTop();
                                $scope.tableCertificacaoProduto.page(1);
                                $scope.tableCertificacaoProduto.reload();
                                $scope.initMode = false;

                                var mensagem = $translate.instant('46.MENSAGEM_RegistroAlteradoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '46.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);

                            },
                            $rootScope.TratarErro
                        );
                    }
                }
            }
        }

        //Método para abertura do modal de Produto
        $scope.onProduto = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalProduto',
                controller: ModalProdutoCtrl,
                resolve: {
                    item: function () {
                        var produto = angular.copy(item);
                        return produto;
                    },
                    licencas: function () {
                        return $scope.licencas;
                    },
                    statusgtin: function () {
                        return $scope.statusgtin;
                    }
                }
            });

            modalInstance.result.then(function (data) {

                var antigaValidade = '', antigoCertificado = '', antigoStatus = '';

                if ($scope.item != undefined && $scope.item.datacertificado != undefined) {
                    antigoCertificado = $scope.item.datacertificado;
                }
                if ($scope.item != undefined && $scope.item.datavalidade != undefined) {
                    antigaValidade = $scope.item.datavalidade;
                }
                if ($scope.item != undefined && $scope.item.status != undefined) {
                    antigoStatus = $scope.item.status;
                }

                $scope.item = data;

                if (antigoCertificado != '') {
                    $scope.item.datacertificado = antigoCertificado;
                }
                if (antigaValidade != '') {
                    $scope.item.datavalidade = antigaValidade;
                }
                if (antigoStatus != '') {
                    $scope.item.status = antigoStatus;
                }

            }, function () { });
        }

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('CertificacaoProdutosPesosMedidasBO', 'EditarCertificacaoPesosMedidas', function () {

                var mensagem = $translate.instant('46.LBL_TitleAlterarCertificacaoProdutos');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '46.LBL_TitleAlterarCertificacaoProdutos' ? 'Alterar Certificação de Produtos' : mensagem;

                $scope.initMode = false;
                $scope.item = angular.copy(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.gerar = true;
                $scope.exibeVoltar = true;
            });
        }

        //Carregamento inicial
        $scope.onLoad = function () {

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                    //Licenças
                    genericService.postRequisicao('LicencaBO', 'buscarTipoGtinAssociado', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.licencas = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Produto
                    genericService.postRequisicao('ProdutoBO', 'BuscarProdutos', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.produtos = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Filtros
                    genericService.postRequisicao('TipoFiltroBO', 'BuscarTipoFiltrosAtivos', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.filtros = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Status do GTIN
                    genericService.postRequisicao('StatusGTINBO', 'BuscarStatusGTINAtivosFiltrado', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.statusgtin = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    $(window).scrollTop(0);
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }
            });
        }

        $scope.onLoad();

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };
    } ]);


//Controller para métodos do modal
var ModalProdutoCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, genericService, item, Utils, licencas, statusgtin) {

    $scope.initModal = true;

    $scope.licencas = licencas;
    $scope.statusgtin = statusgtin;
    $scope.itemForm = {
        gtin: ''
    , descricao: ''
    , codigotipogtin: ''
    , codigostatusgtin: ''
    };

    $scope.tableProdutos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridProdutos(function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                $scope.produtos = data;
            });
        }
    });

    function carregaGridProdutos(callback) {
        var dados = angular.copy($scope.itemForm);

        genericService.postRequisicao('CertificacaoProdutosPesosMedidasBO', 'PesquisaProdutos', { campos: dados }).then(
            function (result) {
                if (callback) callback(result.data);
            },
            $rootScope.TratarErro
        );
    }

    function validarPesquisa() {
        if (!$scope.itemForm.gtin
        && !$scope.itemForm.descricao
        && !$scope.itemForm.codigotipogtin
        && !$scope.itemForm.codigostatusgtin) {

            var mensagem = $translate.instant('46.MENSAGEM_PreenchaAoMenosUmFiltro');
            $scope.alert(typeof mensagem === 'object' || mensagem == '46.MENSAGEM_PreenchaAoMenosUmFiltro' ? 'Preencha ao menos um filtro.' : mensagem);
            return false;
        }

        return true;
    }

    //Chamado ao selecionar um PRODUTO no grid
    $scope.onSelectedProduto = function (item) {
        $modalInstance.close(item);
    }

    //Cancelar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }

};