app.controller('FAQCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$sce', '$translate', 
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $q, $sce, $translate) {

    $scope.titulo = 'Perguntas Frequentes - FAQ';
    $translate('41.LBL_TitleFAQ').then(function (result) {
        $scope.titulo = result;
    });

    $scope.form = {};
    $scope.itemForm = {};
    $scope.isPesquisa = false;
    $scope.paginaatual = 1;
    $scope.quantidadeporpagina = $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina');

    //Busca Perguntas Frequentes
    $scope.buscarFAQs = function (registroporpagina, paginaatual) {
        genericService.postRequisicao('FAQBO', 'BuscarFAQs', { campos: { registroporpagina: $scope.quantidadeporpagina, paginaatual: $scope.paginaatual, codigoidioma: $rootScope.codigoidioma} }).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $scope.faqs = result.data;

                    for (var i in $scope.faqs) {

                        if ($scope.faqs[i] != undefined && $scope.faqs[i] != '') {
                            Utils.EmbedYoutube($scope.faqs[i].video, function (data) {
                                $scope.faqs[i].video = data;
                            });
                        }
                    }

                    if ($scope.faqs != undefined && $scope.faqs.length > 0) {
                        $scope.totalregistros = result.data[0].quantidadetotal;
                    }
                }
            },
            $rootScope.TratarErro
        );
    }

    $scope.pageChanged = function (paginaatual) {
        $scope.paginaatual = paginaatual;
        $scope.buscarFAQs($scope.quantidadeporpagina, $scope.paginaatual);
    }

    //Renderizar HTML na página
    $scope.renderHtml = function (htmlItem) {
        return $sce.trustAsHtml(htmlItem);
    };

    $scope.buscarFAQs($scope.quantidadeporpagina, $scope.paginaatual);

} ]);