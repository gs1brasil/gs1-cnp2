app.controller('AceiteTermoAdesao', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$window', 'IndexService', '$route', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $window, IndexService, $route, $translate) {

        $scope.item = {};

        var mensagem = $translate.instant('52.LBL_TermosAdesao');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '52.LBL_TermosAdesao' ? 'Termo de adesão' : mensagem;

        //Valida se o termo atual ja foi aceito
        function verificaTermoAceito(item) {
            genericService.postRequisicao('TermosAdesaoBO', 'BuscarTermoAceitoUsuario', { campos: item }).then(
                function (result) {

                    if (result.data.length > 0) {
                        $scope.isVisualize = true;
                    }
                    else {
                        $scope.isVisualize = false;
                    }

                },
                $rootScope.TratarErro
            );
        };

        function carregaTermo() {
            genericService.postRequisicao('TermosAdesaoBO', 'BuscarTermoAdesaoAtivo', { campos: { codigoidioma: $rootScope.codigoidioma} }).then(
                function (result) {

                    if (result.data != undefined && result.data != '') {
                        $scope.item = angular.copy(result.data);
                        verificaTermoAceito($scope.item);
                        document.getElementById('termo').innerHTML = $scope.item.html;
                    }
                    else {
                        var mensagem = $translate.instant('52.MENSAGEM_NaoExisteTermoEmUso');
                        $scope.mensagem = typeof mensagem === 'object' || mensagem == '52.MENSAGEM_NaoExisteTermoEmUso' ? 'Não existe termo de adesão em uso no sistema.' : mensagem;

                        document.getElementById('termo').innerHTML = "<div align=\"center\"><font size=\"5\">" + $scope.mensagem + "</font></div>";
                        $scope.isVisualize = true;
                    }
                },
                $rootScope.TratarErro
            );
        };
        carregaTermo();

        $scope.aceitarTermo = function (item) {

            $("#aceitar").attr("disabled", true);
            $("#recusar").attr("disabled", true);

            genericService.postRequisicao('TermosAdesaoBO', 'AceitarTermoAdesao', { campos: item }).then(
                function (result) {
                    IndexService.RetornaUsuarioLogado("", function (data) {
                        if ((data != null) && (data.nome != null)) {
                            $rootScope.usuarioLogado = data;
                        }

                        if ((data.id_statususuario == 5) || (data.avisosenha == 2)) {
                            $location.path('/backoffice/troca_senha');
                            $route.reload();
                        }
                        else {
                            $scope.geraMenu();

                            var mensagem = $translate.instant('52.MENSAGEM_TermoAdesaoAceito');
                            var titulo = $translate.instant('52.MENSAGEM_Alerta');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '52.MENSAGEM_TermoAdesaoAceito' ? 'Termo de adesão aceito com sucesso.' : mensagem, function () {
                                $location.path('/backoffice/home');
                                $rootScope.verificarPesquisaSatisfacaoDisponivel();
                            }, typeof titulo === 'object' || titulo == '52.MENSAGEM_Alerta' ? 'Alerta' : titulo);

                        }

                    });
                },
                $rootScope.TratarErro
            );
        };

        $scope.rejeitarTermo = function () {
            if ($rootScope.usuarioLogado.termoadesaopendente == 1) {
                var mensagem = $translate.instant('52.MENSAGEM_AceitarTermo');
                $scope.confirm(typeof mensagem === 'object' || mensagem == '52.MENSAGEM_AceitarTermo' ? 'Não é possível prosseguir sem aceitar o termo de adesão. Deseja realmente rejeita-lo?' : mensagem, function () {
                    $location.path('/backoffice/logout');
                });
            } else {
                $scope.retornarHome();
            }

        }

        $scope.retornarHome = function () {
            $location.path('/backoffice/home');
        }
    } ]);