app.controller('agenciasReguladorasCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {
        //inicializando abas do form
        $('#tabs-agencia a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        var mensagem = $translate.instant('3.LBL_CadastroAgenciasReguladoras');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '3.LBL_CadastroAgenciasReguladoras' ? 'Cadastro de Agências Reguladoras' : mensagem;

        //Define se está ou não no mode de pesquisa
        $scope.isPesquisa = false;

        //Popula grid de Agências Reguladoras
        $scope.tableAgencia = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                nome: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.items = data;
                });
            }
        });

        //Fornece dados para a grid de Agências Reguladoras
        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            if (!isPesquisa) {

                genericService.postRequisicao('AgenciasReguladorasBO', 'BuscarAgenciasReguladoras', { where: dados }).then(
                    function (result) {

                        $scope.agencias = result.data;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );

            }
            else if (isPesquisa) {
                dados = angular.copy(item);

                genericService.postRequisicao('AgenciasReguladorasBO', 'BuscarAgenciaReguladora', { campos: dados }).then(
                    function (result) {

                        $scope.form.submitted = false;
                        $scope.editMode = false;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                 );
            }
        };

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('AgenciasReguladorasBO', 'CadastrarAgencia', function () {

                var mensagem = $translate.instant('3.LBL_CadastrarAgenciaReguladora');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '3.LBL_CadastrarAgenciaReguladora' ? 'Cadastrar Agências Reguladoras' : mensagem;

                Utils.bloquearCamposVisualizacao("VisualizarUsuario");

                $scope.item = {};
                $scope.isPesquisa = false;
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.exibeVoltar = true;
                $scope.initMode = true;

            });
        };

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('AgenciasReguladorasBO', 'AtualizarAgencia', function () {

                Utils.bloquearCamposVisualizacao("VisualizarUsuario");

                var mensagem = $translate.instant('3.LBL_TitleAlterarAgenciaReguladora');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '3.LBL_TitleAlterarAgenciaReguladora' ? 'Alterar Agência Reguladora' : mensagem;
                $scope.item = angular.copy(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;
                $scope.initMode = false;
            });
        };

        $scope.onCancel = function (item) {

            var mensagem = $translate.instant('3.LBL_CadastroAgenciasReguladoras');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '3.LBL_CadastroAgenciasReguladoras' ? 'Cadastro de Agências Reguladoras' : mensagem;
            $scope.item = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.exibeVoltar = false;
            $scope.isPesquisa = false;
            $scope.initMode = false;
            Utils.moveScrollTop();
            $scope.tableAgencia.page(1);
            $scope.tableAgencia.reload();
        };

        $scope.onSearchMode = function (item) {
            var mensagem = $translate.instant('3.LBL_PesquisarAgenciaReguladora');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '3.LBL_PesquisarAgenciaReguladora' ? 'Pesquisar Agência Reguladora' : mensagem;

            Utils.desbloquearCamposVisualizacaoPesquisa();

            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
            $scope.initMode = false;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
        };

        //Deleta um registro
        $scope.onDelete = function (item) {
            $rootScope.verificaPermissao('AgenciasReguladorasBO', 'RemoverAgencia', function () {

                var titulo = $translate.instant('3.LBL_Confirmacao');
                var mensagem = $translate.instant('3.MENSAGEM_DesejaRemover');
                $rootScope.confirm(typeof mensagem === 'object' || mensagem == '3.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {
                    genericService.postRequisicao('AgenciasReguladorasBO', 'RemoverAgencia', { campos: item }).then(
                            function (result) {
                                $scope.form.submitted = false;
                                $scope.tableAgencia.reload();
                                $scope.tableAgencia.page(1);
                                $scope.item = {};

                                var mensagem = $translate.instant('3.MENSAGEM_DadosRemovidosSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '3.MENSAGEM_DadosRemovidosSucesso' ? 'Dados removidos com sucesso.' : mensagem);

                            },
                            $rootScope.TratarErro
                        );

                }, function () { }, typeof titulo === 'object' || titulo == '3.LBL_Confirmacao' ? 'Confirmação' : titulo);
            });
        }

        $scope.onSearch = function (item) {
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableAgencia.page(1);
            $scope.tableAgencia.reload();
        }

        //Salva / Atualiza uma agência
        $scope.onSave = function (item) {

            if (!$scope.form.$valid) {

                var mensagem = $translate.instant('3.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '3.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
            }
            else {
                //Cadastro de agência reguladora
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {
                    genericService.postRequisicao('AgenciasReguladorasBO', 'CadastrarAgencia', { campos: item }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            Utils.moveScrollTop();
                            $scope.tableAgencia.page(1);
                            $scope.tableAgencia.reload();

                            var mensagem = $translate.instant('3.MENSAGEM_RegistroSalvoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '3.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                }
                //Alteração de agência reguladora
                else {
                    genericService.postRequisicao('AgenciasReguladorasBO', 'AtualizarAgencia', { campos: item }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            Utils.moveScrollTop();
                            $scope.tableAgencia.page(1);
                            $scope.tableAgencia.reload();

                            var mensagem = $translate.instant('3.MENSAGEM_RegistroAlteradoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '3.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                }
            }
        }

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };
    } ]);
