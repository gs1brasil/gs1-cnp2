app.controller('AjudaPassoAPassoCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$sce', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $q, $sce, $translate) {

    $scope.titulo = 'Ajuda Passo a Passo';
    $translate('51.LBL_AjudaPassoaPasso').then(function (result) {
        $scope.titulo = result;
    });

    $scope.form = {};
    $scope.itemForm = {};
    $scope.isPesquisa = false;
    $scope.paginaatual = 1;
    $scope.objAjudas = {};
    $scope.quantidadeporpagina = $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina');

    //Busca Formulários
    $scope.buscarFormularios = function (registroporpagina, paginaatual) {
        genericService.postRequisicao('AjudaPassoAPassoBO', 'BuscarFormularios', { campos: { registroporpagina: $scope.quantidadeporpagina, paginaatual: $scope.paginaatual, codigoidioma: $rootScope.codigoidioma} }).then(
            function (result) {

                if (result.data != undefined && result.data != null) {
                    $scope.formularios = result.data; ;

                    if ($scope.formularios != undefined && $scope.formularios.length > 0) {
                        $scope.totalregistros = result.data[0].quantidadetotal;
                    }
                }
            },
            $rootScope.TratarErro
        );
    }

    //Busca Ajudas Passo a Passo
    $scope.buscarAjudasPassoAPasso = function (item) {

        if ($scope.objAjudas != undefined && $scope.objAjudas[item.nome] != undefined) {
            $scope.ajudas = $scope.objAjudas[item.nome];
            return false;
        }

        genericService.postRequisicao('AjudaPassoAPassoBO', 'BuscarAjudasPassoAPasso', { campos: { codigoformulario: item.codigoformulario, codigoidioma: $rootScope.codigoidioma} }).then(
            function (result) {

                if (result.data != undefined && result.data != null) {
                    $scope.ajudas = result.data;
                    $scope.objAjudas[item.nome] = $scope.ajudas
                }
            },
            $rootScope.TratarErro
        );
    }

    $scope.pageChanged = function (paginaatual) {
        $scope.paginaatual = paginaatual;
        $scope.buscarFormularios($scope.quantidadeporpagina, $scope.paginaatual);
    }

    //Renderizar HTML na página
    $scope.renderHtml = function (htmlItem) {
        return $sce.trustAsHtml(htmlItem);
    };

    $scope.changeSign = function (indice) {
        angular.element('#accordion-question' + indice).attr('aria-expanded') === 'true' ? angular.element('.sign' + indice).removeClass('fa-minus').addClass('fa-plus') : angular.element('.sign' + indice).removeClass('fa-plus').addClass('fa-minus');
    }

    $scope.buscarFormularios($scope.quantidadeporpagina, $scope.paginaatual);

} ]);