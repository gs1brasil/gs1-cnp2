﻿'use strict';

var app = angular.module('CNP.Backoffice', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ui.bootstrap', //http://angular-ui.github.io/bootstrap/
    'ngTable', //http://ngmodules.org/modules/ng-table
    'ui.mask', //http://angular-ui.github.io/ui-utils/#/mask
    'ngAnimate', //http://docs.angularjs.org/api/ngAnimate
    'ngScrollTo',
//	'angularFileUpload',
    'blockUI',
//    'angularTreeview', //https://github.com/eu81273/angular.treeview
//    'ngTagsInput',
//    'infinite-scroll',
//    'ngDraggable',
    'colorpicker.module',
//    'google-maps'.ns(),
    'uiGmapgoogle-maps',
    'cb.x2js',
    'ui.utils.masks',
    'wysiwyg.module',    
    'ng.ckeditor',
    'vcRecaptcha',
    'ngStorage',
    'pascalprecht.translate'//,
    //'logToServer'
]);

angular.module('ui.bootstrap.carousel', ['ui.bootstrap.transition'])
.controller('CarouselController', ['$scope', '$timeout', '$transition', '$q', function ($scope, $timeout, $transition, $q) {
} ]).directive('carousel', [function () {
    return {

    }
} ]);

app.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['**']);
});


app.config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($q, $location, $injector, $rootScope, $window, blockUI, $translate) {
        return {
            'responseError': function (response) {
                var url;
                
                if (response != undefined && response != null && response.config != undefined && response.config != null &&
                    response.config.url != undefined && response.config.url != null && response.config.url != '') {
                    url = response.config.url;
                }

                if (response.status === 401) {
                    $rootScope.alert('Sua sessão expirou. Por favor, refaça seu login para poder continuar utilizando o sistema.', function () {
                        $window.location.href = '/';
                    }, 'Sessão expirada');
                } else if (response.status === 403) {
                    $location.path('/backoffice/erro/403');
                } else if (response.status === 404 && url.indexOf('youtube') >= 0) {
                    $rootScope.alert('Não foi possível adicionar o vídeo.');
                } else if (response.status === 404) {
                    $location.path('/backoffice/erro/404');
                } else if (response.status === 419) {
                    //$rootScope.alert(response.data.Message + "<br />Data Atual: " + response.data.DateOccurrence);
                    var message = response.data.Message + '<br />Data Atual: ' + response.data.DateOccurrence;

                    $rootScope.alert(message);
                } else if (response.status === 420) {
                    $rootScope.alert(response.data.Message);
                } else if (response.status === -1) {

                    $translate('38.MENSAGEM_ErroComunicacaoServidor').then(function (result) {
                        $rootScope.alert(result);
                    });
                    //$rootScope.iserrormessageactive = false;
                }

                blockUI.stop();

                $rootScope.$$phase || $rootScope.$apply();

                return $q.reject(response);
            }
        };
    });
});

app.config(["blockUIConfig",
    function (blockUIConfig) {
        blockUIConfig.message = "Carregando...";        
        blockUIConfig.template = '<div class=\"block-ui-overlay\"></div><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div class=\"block-ui-message\" ng-class=\"$_blockUiMessageClass\" translate=\"38.MENSAGEM_Carregando\">{{ state.message }}</div></div>';
        blockUIConfig.delay = 100;
        blockUIConfig.autoBlock = true;
        blockUIConfig.resetOnException = true;
        blockUIConfig.requestFilter = function (config) {
            // If the request starts with '/maps/api' ...

            if (config.url.match(/^\/maps($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/ImportarProdutosBO\/LerArquivo($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/ImportarProdutosBO\/dominios($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/LoginBO\/VerificaUsuarioLogado($|\/).*/)) {            
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/LoginBO\/BuscarAssociadosUsuarioAutoComplete($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/RelatorioBO\/BuscarUsuario($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/LoginBO\/RetornaUsuarioLogado($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/RelatorioBO\/BuscarLogin($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/RelatorioBO\/BuscarRazaoSocial($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/DashboardBO\/ConsultarMensagens($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/DashboardBO\/ConsultaBlocoProdutos($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/DashboardBO\/ConsultaTotalEtiquetas($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/DashboardBO\/ConsultarNoticias($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/Requisicao.ashx\/DashboardBO\/BannersEspacoBanner($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/DashboardBO\/ConsultaProdutosEmPreenchimento($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/LoginBO\/VerificarPesquisaSatisfacaoDisponivel($|\/).*/)) {
                return false; // ... don't block it.
            }
            if (config.url.match(/^\/requisicao.ashx\/DashboardBO\/ConsultarNoticias($|\/).*/)) {
                return false; // ... don't block it.
            }
//            if (config.url.match(/^\/requisicao.ashx\/DashboardBO\/ConsultarBoletos($|\/).*/)) {
//                return false; // ... don't block it.
//            }
            if (config.url.match(/^\/requisicao.ashx\/LoginBO\/ConsultarUltimoAcesso($|\/).*/)) {
                return false; // ... don't block it.
            }

            //console.log(config.url);
        };
    }
]);

    app.config(function ($routeProvider, $locationProvider) {
        $routeProvider
    .when('/home', { templateUrl: '/views/home.html' })
    .when('/index.aspx', { redirectTo: '/home' })
    .when('/backoffice/home', { templateUrl: '/views/main.html' })
    .when('/backoffice', { templateUrl: '/views/main.html' })
    .when('/backoffice/logout', {
        template: '<div ng-include src="pageUrl"></div>',
        controller: function ($scope, $routeParams, $templateCache, $http, $location) {
            //$location.path('/views/backoffice/logout.aspx');
            window.location = "/views/backoffice/logout.aspx";
        }
    })
    .when('/backoffice/detalhesAjuda', {
        template: '<div ng-include src="pageUrl"></div>',
        controller: function ($scope, $routeParams, $templateCache, $http, $location, $rootScope) {
            //window.location = "/backoffice/home";
            $location.path("#/backoffice/home");
        }
    })
    .when('/backoffice/detalhesAjuda/:codigo', {
        template: '<div ng-include src="pageUrl"></div>',
        controller: function ($scope, $routeParams, $templateCache, $http, $location, $rootScope) {
            $scope.pageUrl = '/views/backoffice/detalhesAjudaPassoaPasso.html';
            $scope.codigoUrl = $routeParams.codigo;
            //window.location = "/views/backoffice/detalhesAjuda.html";
        }
    })
    .when('/backoffice/erro/:page', {
        template: '<div ng-include src="pageUrl"></div>',
        controller: function ($scope, $routeParams, $templateCache, $http) {
            $scope.pageUrl = '/views/' + $routeParams.page + '.html';
        }
    })
    .when('/backoffice/mensagem/:codigo', {
        template: '<div ng-include src="pageUrl"></div>',
        controller: function ($scope, $routeParams, $templateCache, $http) {
            $scope.pageUrl = '/views/backoffice/mensagem.aspx';
        }
    })
    .when('/backoffice/mensagem/:caixa/:codigo/:pergunta', {
        template: '<div ng-include src="pageUrl"></div>',
        controller: function ($scope, $routeParams, $templateCache, $http) {
            $scope.pageUrl = '/views/backoffice/mensagem.aspx';
        }
    })
    .when('/backoffice/:page', {
        template: '<div ng-include src="pageUrl"></div>',
        controller: function ($rootScope, $scope, $routeParams, $templateCache, $http, $timeout) {
            $http.post('/requisicao.ashx/LoginBO/VerificaUsuarioLogado', '').then(
                function (result) {
                    if (result != undefined && result != null && result.data != undefined && result.data != null && result.data == true) {
                        if ($rootScope.usuarioLogado != undefined && $rootScope.usuarioLogado != null && $rootScope.usuarioLogado.termoadesaopendente == 1) {
                            $scope.pageUrl = '/views/backoffice/aceiteTermoAdesao.aspx';
                            $scope.usuarioLogado = $rootScope.usuarioLogado;
                        } else if ($rootScope.usuarioLogado != undefined && (($rootScope.usuarioLogado.id_statususuario == 5) || ($rootScope.usuarioLogado.avisosenha == 2))) {
                            //$location.path('/troca_senha');
                            $scope.pageUrl = '/views/backoffice/troca_senha.aspx';
                            $scope.usuarioLogado = $rootScope.usuarioLogado;
                        } else {
                            $scope.pageUrl = '/views/backoffice/' + $routeParams.page + '.aspx';
                        }
                    }
                },
                $rootScope.TratarErro
            );
        }
    })
    .otherwise({ redirectTo: '/backoffice/home' });

        $locationProvider.html5Mode(true);
    });

//'GoogleMapApiProvider'.ns() == 'uiGmapGoogleMapApiProvider'
//app.config(['GoogleMapApiProvider'.ns(), function (GoogleMapApi) {
//    GoogleMapApi.configure({
//        key: 'AIzaSyAMC4WjijyKUTEXlCZvt-iVlx-X7w-jflA',
//        v: '3.17',
//        libraries: 'places',
//        language: 'pt-br'
//    });
//}])uiGmapgoogle-maps

app.config(
    ['uiGmapGoogleMapApiProvider', function (GoogleMapApiProviders) {
        GoogleMapApiProviders.configure({
            key: 'AIzaSyAMC4WjijyKUTEXlCZvt-iVlx-X7w-jflA',
            v: '3.17',
            libraries: 'places',
            language: 'pt-br'
        });
    } ]
);

app.run(function ($rootScope, genericService, $location, blockUI) {

    //Busca os Códigos das Páginas que possuem Ajuda Publicada e Preenchida
    $rootScope.buscarCodigosAjuda = function (callback) {

        genericService.postRequisicao('DetalhesAjudaBO', 'BuscarCodigosAjuda', {}).then(
            function (result) {

                if (result.data != undefined && result.data != '') {
                    $rootScope.codigosAjuda = [];

                    for (var i in result.data) {
                        $rootScope.codigosAjuda.push(result.data[i].codigo);
                    }
                }
            },
            $rootScope.TratarErro
        );
    };

    if (!($location.path() == "/") && $location.path().indexOf("/login") == -1 && $location.path().indexOf("/index.aspx") == -1) {
        $rootScope.buscarCodigosAjuda();
    }
});

app.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.preferredLanguage('pt-br');
    $translateProvider.useLoader('idiomaLoader');
    $translateProvider.useMissingTranslationHandler('idiomaErrorHandler');
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
    //$translateProvider.useMissingTranslationHandlerLog();
    // remember language
    $translateProvider.useCookieStorage();
} ]);


app.factory('idiomaLoader', function ($http, $q, $rootScope, $location) {
    return function (options) {
        var deferred = $q.defer();

        if ($rootScope.traducoes == undefined || $rootScope.traducoes == null || ($rootScope.traducoes != undefined || $rootScope.traducoes != null && Object.keys($rootScope.traducoes).length == 0)) {
            var urltraducao = "";
            if ($location.path() == '/index.aspx' || $location.path() == '/') {
                urltraducao = "/requisicao.ashx/IdiomaBO/BuscarTraducaoHome";
            } else {
                urltraducao = "/requisicao.ashx/IdiomaBO/BuscarTraducao";
            }

            $http.post(urltraducao).success(function (data, status) {

                if (data != undefined && data != null) {
                    $rootScope.traducoes = {};
                    var index = 0;
                    var index2 = 0;
                    for (index = 0; index < data.length; ++index) {
                        if (data[index].length > 0) {
                            var traducao = {}
                            for (index2 = 0; index2 < data[index].length; ++index2) {
                                traducao[data[index][index2].codigoformulario + '.' + data[index][index2].nome] = data[index][index2].texto;
                                traducao['t.' + data[index][index2].codigoformulario + '.' + data[index][index2].nome] = data[index][index2].comentario;
                            }
                            $rootScope.traducoes[data[index][0].nomeidioma.toString()] = traducao;
                        }
                    }
                }

                if ($rootScope.traducoes) {
                    deferred.resolve($rootScope.traducoes[options.key]);
                }
            })
        }


        // $timeout(function () {

        // }, 2000);

        return deferred.promise;
    };
});

app.run(function ($rootScope, $translate, blockUI) {
    $rootScope.$on('$translateChangeStart', function (event, a) {
        blockUI.start();
    });

    $rootScope.$on('$translateChangeSuccess', function (event, a) {
        //blockUI.message = $translate.instant('38.MENSAGEM_Carregando');
        blockUI.stop();
    });

    
});

app.factory('idiomaErrorHandler', ['$q', function ($q) {
    return function (translationID) {
        // caso não encontre a tradução não substitui   
        var deferred = $q.defer();
        return deferred.promise;
    };
} ]);


String.prototype.splice = function (idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};