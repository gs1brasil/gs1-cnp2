﻿﻿app.controller('AprovaImportacaoProdutosCtrl', ['$rootScope', '$scope', '$filter', '$timeout', 'ngTableParams', 'Utils', 'genericService', 'blockUI', '$translate',
    function ($rootScope, $scope, $filter, $timeout, ngTableParams, Utils, http, blockUI, $translate) {
    var navigatorVersion = $rootScope.getInternetExplorerVersion();
    var self = this;
    var originalData = []
        //registros = [],
        padraoGDSN = 0,
        hot = null,
        listErrors = [];

    $scope.listaErros = [];
    $scope.listas = {};
    $scope.tipoArquivo = -1;
    $scope.exibeMensagem = false;
    
    Utils.bloquearCamposVisualizacao("VisualizarImportarProduto");

     $(document).ready(function () {
        $('[data-toggle="popover"]').popover({ html: true });
    });

    $('#tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var titulo = $translate.instant('18.LBL_ImportacaoProduto');
    $scope.titulo = typeof titulo === 'object' || titulo == '18.LBL_ImportacaoProduto' ? 'Importação de Produtos' : titulo;

    var screen = {
        RESULT: 1,
        ASSISTANT: 2,
    };

    function preInit() {
        $rootScope.verificaAssociadoSelecionado(function (data) {
            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                $rootScope.flagAssociadoSelecionado = true;

                initialize();

                onBuscarAprovacao();
            } else {
                $rootScope.flagAssociadoSelecionado = false;
                angular.element("#typea").focus();
            }
        });
    }

    function initialize() {
        // TODO Remover após configurar a permissão
        $rootScope.flagAssociadoSelecionado = true;
        // Carregar Produtos por associado
    }

    function ExibirTela(tela) {
        $scope.resultMode = tela == 1;
        $scope.verificacaoItensMode = tela == 2;
    }

    function getDomainParameters(row, col, manual) {
        if ($scope.tipoArquivo > 0) {
            if ((manual == true && col == 1) || (manual == false && col == 0))
                return { where: { listname: 'statusgtin', tablename: 'statusgtin', code: 'codigo', description: 'nome', condition: '', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 7) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 6) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 9) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 8))
                return { where: { listname: 'ncm', tablename: 'ncm', code: 'ncm', description: 'NCM + \' - \' + Descricao', condition: '', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 8) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 7) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 10) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 9))
                return { where: { listname: 'segment', tablename: 'tbSegment', code: 'CodeSegment', description: 'CodeSegment + \' - \' + Text', condition: '1=1 ORDER BY text ASC', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 9) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 8) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 11) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 10))
                return { where: { listname: 'family', tablename: 'tbFamily', code: 'CodeFamily', description: 'CodeFamily + \' - \' + Text', condition: '', value: [hot.getDataAtCell(row, col-1)] } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 10) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 9) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 12) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 11))
                return { where: { listname: 'class', tablename: 'tbClass', code: 'CodeClass', description: 'CodeClass + \' - \' + Text', condition: '', value: [hot.getDataAtCell(row, col-2), hot.getDataAtCell(row, col-1)] } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 11) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 10) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 13) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 12))
                return { where: { listname: 'brick', tablename: 'tbBrick', code: 'CodeBrick', description: 'CodeBrick + \' - \' + Text', condition: '', value: [hot.getDataAtCell(row, col-3), hot.getDataAtCell(row, col-2), hot.getDataAtCell(row, col-1)] } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && (col == 13 || col == 15 || col == 17 || col == 19 || col == 21)) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && (col == 12 || col == 14 || col == 16 || col == 18 || col == 20)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 15 || col == 17 || col == 19 || col == 21 || col == 23)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 14 || col == 16 || col == 18 || col == 20 || col == 22)))
                return { where: { listname: 'agenciareguladora', tablename: 'agenciareguladora', code: 'Codigo', description: 'Nome', condition: 'status=1', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && (col == 22 || col == 23)) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && (col == 21 || col == 22)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 24 || col == 25)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 23 || col == 24)))
                return { where: { listname: 'pais', tablename: 'CountryOfOriginISO3166', code: 'CodigoPais', description: 'Nome', condition: '', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 24) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 23) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 26) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 25))
                return { where: { listname: 'estado', tablename: 'tbStateCountry', code: 'StateName', description: 'StateName', condition: '', value: [hot.getDataAtCell(row, col-1)] } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 25) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 24) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 27) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 26))
                return { where: { listname: 'tipoproduto', tablename: 'TipoProduto', code: 'Codigo', description: 'Nome', condition: 'Status > 0', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && (col == 27 || col == 29 || col == 31)) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && (col == 26 || col == 28 || col == 30)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 29 || col == 31 || col == 33)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 28 || col == 30 || col == 32)))
                return { where: { listname: 'unidade3', tablename: 'UNECEREC20', code: 'COMMONCODE', description: 'SIGLA', condition: 'TIPO = 3 ORDER BY INDICADORPRINCIPAL DESC', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 33) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 32) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 35) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 34))
                return { where: { listname: 'unidade1', tablename: 'UNECEREC20', code: 'COMMONCODE', description: 'SIGLA', condition: 'TIPO = 1 ORDER BY INDICADORPRINCIPAL DESC', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && (col == 35 || col == 37)) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && (col == 34 || col == 36)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 37 || col == 39)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 36 || col == 38)))
                return { where: { listname: 'unidade2', tablename: 'UNECEREC20', code: 'COMMONCODE', description: 'SIGLA', condition: 'TIPO = 2 ORDER BY INDICADORPRINCIPAL DESC', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 41) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 40) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 43) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 42))
                return { where: { listname: 'pallettypecode', tablename: 'palletTypeCode', code: 'code', description: 'codedescription', condition: 'status = 1', value: '' } };
            else if (
                ($scope.tipoArquivo == 12 && padraoGDSN == 1 && (col == 50 || col == 53 || col == 56)) ||
                ($scope.tipoArquivo == 12 && padraoGDSN == 2 && (col == 49 || col == 52 || col == 55)) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 1 && (col == 52 || col == 55 || col == 58)) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 2 && (col == 51 || col == 54 || col == 57)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 52 || col == 55 || col == 58)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 51 || col == 54 || col == 57)))
                return { where: { listname: 'tipourl', tablename: 'TipoUrl', code: 'Codigo', description: 'Nome', condition: '', value: '' } };
            else if (
                ($scope.tipoArquivo == 12 && padraoGDSN == 1 && col == 63) ||
                ($scope.tipoArquivo == 12 && padraoGDSN == 2 && col == 57) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 1 && col == 65) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 2 && col == 59) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 65) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 59))
                return { where: { listname: 'unidademedida', tablename: 'GDSNUnitOfMeasureCodeList', code: 'CodeValue', description: 'Name', condition: 'Status = 1', value: '' } };
            else if (
                ($scope.tipoArquivo == 12 && padraoGDSN == 1 && col == 66) ||
                ($scope.tipoArquivo == 12 && padraoGDSN == 2 && col == 60) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 1 && col == 68) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 2 && col == 62) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 68) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 62))
                return { where: { listname: 'tipoembalagem', tablename: 'PackagingTypeCode', code: 'CodeValue', description: 'Name', condition: 'Status = 1', value: '' } };
            else if (
                ($scope.tipoArquivo == 12 && padraoGDSN == 1 && (col == 68 || col == 70)) ||
                ($scope.tipoArquivo == 12 && padraoGDSN == 2 && (col == 62 || col == 64)) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 1 && (col == 70 || col == 72)) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 2 && (col == 64 || col == 66)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 70 || col == 72)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 64 || col == 66)))
                return { where: { listname: 'unidade4', tablename: 'UNECEREC20', code: 'COMMONCODE', description: 'SIGLA', condition: 'TIPO = 4 ORDER BY INDICADORPRINCIPAL DESC', value: '' } };
        } else {
            if (col == 1)
                return { where: { listname: 'papelgln', tablename: 'PapelGLN', code: 'codigo', description: 'nome', condition: '', value: '' } };
            else if (col == 6)
                return { where: { listname: 'tbcountry', tablename: 'tbcountry', code: 'CountryCode', description: 'CountryName', condition: '', value: '' } };
            else if (col == 12)
                return { where: { listname: 'estadogln', tablename: 'tbStateCountry', code: 'StateName', description: 'StateName', condition: '', value: [hot.getDataAtCell(row, 6)] } };
        }
    }

    function errorValueRenderer(instance, td, row, col, prop, value, cellProperties) {
        Handsontable.renderers.TextRenderer.apply(this, arguments);

        if (row < listErrors.length && col < listErrors[row].length) {
            var value = listErrors[row][col];
            if (value != null && value != '' && (value == 'true' || value == true))
                td.style.color = 'red';
        }
    }

    function arrayIsNullOrEmpty(line) {
        if (line != null && line instanceof Array && line.length > 0) {
            for (var i = 0; i < line.length; i++) {
                if (line[i] != null && line[i] != '')
                    return false;
            }
        }
        return true;
    }

    function getAutoComplete(manual) {
        return function (query, process) {
            var column = (manual == false ? this.col : (this.col == 0 || this.col == 1 ? this.col : this.col - 1));

            if  ($scope.tipoArquivo > 0 && 
                ((manual == true && column == 0) ||
                    (padraoGDSN == 1 && 
                        (($scope.tipoArquivo == 12 && (column == 38 || column == 57 || column == 58 || column == 59 || column == 60 || column == 61 || column == 62 || column == 71)) ||
                         ($scope.tipoArquivo == 13 && (column == 38 || column == 59 || column == 60 || column == 61 || column == 62 || column == 63 || column == 64 || column == 73)) ||
                         ($scope.tipoArquivo == 14 && (column == 40 || column == 59 || column == 60 || column == 61 || column == 62 || column == 63 || column == 64 || column == 73)))
                    ) ||
                    (padraoGDSN == 2 && 
                        (($scope.tipoArquivo == 12 && (column == 37 || column == 56 || column == 65)) ||
                         ($scope.tipoArquivo == 13 && (column == 37 || column == 58 || column == 67)) ||
                         ($scope.tipoArquivo == 14 && (column == 39 || column == 58 || column == 67)))
                    )
                ))
                process(['Sim', 'Não']);
            else if ($scope.tipoArquivo == 0 && column == 2)
                process(['Ativo', 'Inativo']);
            else {
                var dados = getDomainParameters(this.row, column, manual);
                $.ajax({
                    url: 'requisicao.ashx/ImportarProdutosBO/dominios',
                    dataType: 'json',
                    method: 'POST',
                    data: JSON.stringify(dados),
                    success: function (response) {
                        var result = [];
                        for (var i = 0; i < response.length; i++)
                            result.push([response[i].descricao]);
                        process(result);
                    }
                });
            }
        }
    }

    function getCells(row, col, prop) {
        var cellProperties = { readOnly: false, valid: true };

        if ($scope.tipoArquivo > 0 && 
            (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && ((col >= 8 && col <= 10) || col == 13)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && ((col > 9 && col <= 11) || col == 15))
            )) {
            if (hot != null && (hot.getDataAtCell(row, col-1) == null || hot.getDataAtCell(row, col-1) == '')) {
                cellProperties.readOnly = true;
                cellProperties.valid = true;
            }
        } else if ($scope.tipoArquivo == 0 && col == 12) {
            if (hot != null && (hot.getDataAtCell(row, 6) == null || hot.getDataAtCell(row, 6) == '')) {
                cellProperties.readOnly = true;
                cellProperties.valid = true;
            }
        }

        cellProperties.renderer = "errorValueRenderer";
  
        return cellProperties;
    }

    function getAfterChange(change, source) {
        if (change != null && change.length > 0) {
            var row = change[0][0],
                col = change[0][1],
                oldvalue = change[0][2],
                newvalue = change[0][3];

            if (col == 'segmento' || col == 'familia' || col == 'classe' || col == 'paisorigem') {
                switch (col) {
                    case 'segmento':
                        col = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13 ? 8 : 9);
                        break;
                    case 'familia':
                        col = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13 ? 9 : 10);
                        break;
                    case 'classe':
                        col = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13 ? 10 : 11);
                        break;
                    case 'paisorigem':
                        col = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13 ? 13 : 15);
                        break;
                }
            }

            if ((($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && col >= 8 && col <= 11) || 
                ($scope.tipoArquivo == 14 && col >= 9 && col <= 12)) {
                if (hot != null) {
                    var changes = [];
                    var colmax = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) ? 10 : 12;
                    if ($scope.tipoArquivo == 14)
                        col++;
                    for (var i = 0; i <= colmax - col; i++) {
                        changes.push([row, col + i, null]);
                    }
                    if (changes.length > 0)
                        hot.setDataAtCell(changes);
                }
            } else if ((($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && col == 13) || 
                ($scope.tipoArquivo == 14 && col == 15)) {
                hot.setDataAtCell([[row, col, null]]);
                
            } else if ($scope.tipoArquivo == 0 && col == 6) {
                hot.setDataAtCell([[row, 12, null]]);
            }
        }
    }

    function onBuscarAprovacao() {
        blockUI.start();

        http.postRequisicao('AprovarImportacaoProdutosBO', 'BuscarAprovacao').then(function (result) {
            if (result != null && result.length > 0) {
                $scope.columns = result.data.colunas;
                $scope.tipoArquivo = result.data.tipoArquivo;
                $scope.tipoitem = result.data.tipoitem;
                $scope.gdsn = result.data.gdsn;
                padraoGDSN = result.data.padraoGDSN;

                var registros = result.data.lista;

                ExibirErros(registros);

                $scope.exibeValidar = result.data.existeErro;
                $scope.exibeImportar = !result.data.existeErro;

                blockUI.stop();

                var headers = [],
                    columns = [],
                    reg = [],
                    regs = [],
                    error = [],
                    container = document.getElementById('tableVerificacao');
                for (var i = 0; i < $scope.columns.length; i++) {
                    headers.push($scope.columns[i].title + ($scope.columns[i].required ? ' (*)' : ''));

                    if (($scope.columns[i].tablereference != null && $scope.columns[i].tablereference != '') || exibirDropdown($scope.columns[i].field))
                    {
                        var col = {
                            data: $scope.columns[i].field,
                            type: 'autocomplete',
                            strict: $scope.columns[i].required,
                            autoColumnSize: true,
                            source: getAutoComplete(false)
                        };
                    }
                    else
                    {
                        var col = { data: $scope.columns[i].field };

                        switch($scope.columns[i].type) {
                            case 'int':
                            case 'long':
                                col.type = 'numeric';
                                break;
                            case 'datetime':
                                col.type = 'date';
                                col.dateFormat = 'DD/MM/YYYY';
                                col.correctFormat = true;
                                break;
                        }
                    }
                    columns.push(col);
                }

                for (r in registros) {
                    error = [];

                    for (c in $scope.columns) {
                        if (registros[r].errors != null && registros[r].errors[$scope.columns[c].field] != null)
                            error.push(true);
                        else
                            error.push(null);
                    }

                    listErrors.push(error);
                }

                Handsontable.renderers.registerRenderer('errorValueRenderer', errorValueRenderer);

                hot = new Handsontable(container, {
                    data: registros,
                    afterChange: getAfterChange,
                    colHeaders: headers,
                    rowHeaders: true,
                    columns: columns,
                    manualColumnResize: true,
                    manualRowResize: false,
                    allowInsertRow: false,
                    contextMenu: {
                        items: {
                            "remove_row": { name: 'Deseja remover esta linha?' }
                        }
                    },
                    readOnlyCellClassName: 'readOnlyCell',
                    autoColumnSize: {syncLimit: 10},
                    cells: getCells,
                    height: 300
                });

                ExibirTela(screen.ASSISTANT);

                $timeout(function() { hot.render(); hot.selectCell(1, 1); }, 100);
            } else {
                blockUI.stop();
            }
        }, $rootScope.TratarErro);
    }

    function ExibirErros(lista) {
        $scope.listaErros = [];
        $scope.qtdeitem = 0;
        $scope.qtdeErros = 0;

        if (lista != null) {
            $scope.qtdeitem = lista.length;
            var contadorLinha = 0;

            angular.forEach(lista, function (value, key) {
                contadorLinha++;

                if (value.errors && !$.isEmptyObject(value.errors)) {
                    var linha = { title: 'Linha ' + contadorLinha + ':', messages: [] };

                    angular.forEach(value.errors, function (value) {
                        linha.messages.push(value);
                        $scope.qtdeErros++;
                    });

                    $scope.listaErros.push(linha);
                }
            });
        }
    }

    function onValidate() {
        var lista = hot.getData();

        if (lista != null && lista.length > 0) {
            var newlista = [];
            var item = null;

            for (var i = 0; i < lista.length; i++) {
                if (!arrayIsNullOrEmpty(lista[i])) {
                    item = {};
                    for (col in $scope.columns) item[$scope.columns[col].field] = lista[i][col];
                    newlista.push(item);
                }
            }

            var dados = {
                campos: { lista: newlista, tipoArquivo: $scope.tipoArquivo, padraoGDSN: padraoGDSN }
            };

            http.postRequisicao('ImportarProdutosBO', 'Validar', dados).then(function (result) {
                var registros = result.data.lista;

                ExibirErros(registros);

                listErrors = [];

                for (r in registros) {
                    error = [];

                    for (c in $scope.columns) {
                        if (registros[r].errors != null && registros[r].errors[$scope.columns[c].field] != null)
                            error.push(true);
                        else
                            error.push(null);
                    }

                    listErrors.push(error);
                }

                $scope.exibeValidar = result.data.existeErro;
                $scope.exibeImportar = !result.data.existeErro;

                hot.loadData(registros);
            }, $rootScope.TratarErro);
        }
    }

    function onImport() {
        var lista = hot.getData();

        if (lista != null && lista.length > 0) {
            var newlista = [];
            var item = null;

            for (var i = 0; i < lista.length; i++) {
                if (!arrayIsNullOrEmpty(lista[i])) {
                    item = {};

                    for (col in $scope.columns)
                        item[$scope.columns[col].field] = lista[i][col] || "";

                    newlista.push(item);
                }
            }

            var dados = {
                campos: {
                    lista: newlista,
                    tipoArquivo: $scope.tipoArquivo,
                    tipo: 3, // Serviço
                    arquivo : 'webservice',
                    padraoGDSN: padraoGDSN
                }
            };

            http.postRequisicao('AprovarImportacaoProdutosBO', 'ImportarAprovacao', dados).then(function (result) {
                $scope.exibeValidar = true;
                $scope.exibeImportar = false;

                onResult(result.data);
            }, $rootScope.TratarErro);
        }
    }
    
//    function onValidateTemplate() {
//        var lista = hot.getData();

//        if (lista != null && lista.length > 0) {
//            var newlista = [];
//            var item = null;

//            for (var i = 0; i < lista.length; i++) {
//                if (!arrayIsNullOrEmpty(lista[i])) {
//                    item = {};

//                    for (col in $scope.columnsTemplate)
//                        item[$scope.columnsTemplate[col].field] = lista[i][col];

//                    newlista.push(item);
//                }
//            }
//            
//            var dados = {
//                campos: {
//                    lista: newlista,
//                    tipoArquivo: $scope.itemModelo.codigo,
//                    padraoGDSN: padraoGDSN
//                }
//            };

//            http.postRequisicao('ImportarProdutosBO', 'ValidarTemplate', dados).then(function (result) {
//                var registros = result.data.lista;

//                ExibirErros(registros);

//                $scope.exibeValidar = result.data.existeErro;
//                $scope.exibeImportar = !result.data.existeErro;

//                $scope.exibeMensagem = true;

//                hot.loadData(registros);
//            }, $rootScope.TratarErro);
//        }
//    }

//    function onImportTemplate() {
//        var lista = hot.getData();

//        if (lista != null && lista.length > 0) {
//            var newlista = [];
//            var item = null;

//            for (var i = 0; i < lista.length; i++) {
//                if (!arrayIsNullOrEmpty(lista[i])) {
//                    item = {};

//                    for (col in $scope.columnsTemplate)
//                        item[$scope.columnsTemplate[col].field] = lista[i][col] || "";

//                    newlista.push(item);
//                }
//            }
//            var dados = {
//                campos: {
//                    lista: newlista,
//                    tipoArquivo: $scope.itemModelo.codigo,
//                    tipo: 2, // Tela
//                    arquivo: arquivo,
//                    padraoGDSN: padraoGDSN
//                }
//            };

//            http.postRequisicao('ImportarProdutosBO', 'Importar', dados).then(function (result) {
//                $scope.exibeValidar = true;
//                $scope.exibeImportar = false;

//                onResult(result.data);
//            }, $rootScope.TratarErro);
//        }
//    }

    function onResult(item) {
        $scope.item = angular.copy(item);

        ExibirTela(screen.RESULT);
    }

    function exibirDropdown(fieldname) {
        return fieldname == 'indicadorgdsn' ||
               fieldname == 'compartilhadados' ||
               fieldname == 'itemcomercialmodelo' ||
               fieldname == 'indicadormercadoriasperigosas' ||
               fieldname == 'indicadoritemcomercialliberadocompra' ||
               fieldname == 'indicadorunidadedespacho' ||
               fieldname == 'indicadorunidadebasicavenda' ||
               fieldname == 'indicadorunidadefaturamento' ||
               fieldname == 'indicadoritemcomercialunidadeconsumo' ||
               fieldname == 'statusgln';
    }

    $scope.onImport = onImport;
    //$scope.onImportTemplate = onImportTemplate;
    $scope.onValidate = onValidate;
    //$scope.onValidateTemplate = onValidateTemplate;
    //$scope.onCancel = onCancel;
    $scope.onResult = onResult;

    $scope.exibirDropdown = exibirDropdown;

    $scope.exibeValidar = false;
    $scope.exibeImportar = false;

    preInit();
} ]);