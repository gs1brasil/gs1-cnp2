app.factory('bannerservice', ['$q', '$http', function ($q, $http) {
    var url = '/UploadHandler.ashx';

    return {

        subirImg: function (fromdata, callback) {

            $http.post(url, fromdata).success(function (data) {
                callback(data);
            }).error(function (data, status, headers, config) {
                alert(data);
            });
        }

    };

} ]);
