﻿app.controller('EspacoBannerCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', '$log', 'genericService', 'bannerservice', 'Utils', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, $log, espacobanner, banner, Utils, $translate) {
        //inicializando abas do form
        $('#tabs-banner a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        var mensagem = $translate.instant('50.LBL_CadsatroBanner');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '50.LBL_CadsatroBanner' ? 'Cadastro de Banner' : mensagem;

        $("#MultipleFilesUpload").dblclick(function () {
            $(this).trigger("click");
        });

        var grid = {};

        //Busca dados do Grid EspaçoBanner
        function carregaGrid(callback) {
            espacobanner.postRequisicao('EspacoBannerBO', 'CarregarTodosEspacoBanners', null).then(
                function (result) {
                    $scope.items = result.data;
                    callback(result.data);
                },
                $rootScope.TratarErro
            );
        }

        //Busca dados do Grid Banner
        function carregaGridBanner(callback) {
            $scope.ordenacoes = [];
            espacobanner.postRequisicao('BannerBO', 'CarregaGridBanner', { where: { CodigoEspacoBanner: $rootScope.codigoespacobanner} }).then(
                function (result) {
                    $scope.itemsBanners = result.data;
                    $rootScope.itemsBanners = result.data;

                    callback(result.data);
                },
                $rootScope.TratarErro
            );
        }

        //Carregamento e paginação do Grid EspaçoBanner
        $scope.tableEspacoBanner = new ngTableParams({
            page: 1,
            count: $rootScope.parametros != undefined ? $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina') : 10,
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {

                var dados = carregaGrid(function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        //Carregamento e paginação do Grid Banner
        $scope.tableBanner = new ngTableParams({
            page: 1,
            count: 5,
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {

                var dados = carregaGridBanner(function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        //Método para abertura do modal
        $scope.open = function (item, items) {

            $rootScope.itemsBanners = $scope.itemsBanners;

            $rootScope.verificaPermissao('BannerBO', 'Insert', function () {
                $rootScope.preencheBanner = item;
                $rootScope.itemsBanner = items;

                if (item != null || item != undefined) {
                    $rootScope.preencheBanner.datainiciopublicacao = $filter('date')(item.datainiciopublicacao, 'dd/MM/yyyy');
                    $rootScope.preencheBanner.datafimpublicacao = $filter('date')(item.datafimpublicacao, 'dd/MM/yyyy');
                }

                var modalInstance = $modal.open({
                    templateUrl: 'modalImage',
                    backdrop: 'static',
                    controller: ModalBannerCtrl,
                    resolve: {
                        item: function () {
                            var banner = angular.copy(item);
                            return banner;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    $scope.tableBanner.reload();
                    $scope.selected = item;

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        //Modo de cadastro de banner
        $scope.onFormMode = function (item) {
            carregaDropDown();
            $scope.itemPerfil = {};
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.exibeVoltar = true;
            Utils.bloquearCamposVisualizacao("VisualizarBanner");

            var mensagem = $translate.instant('50.LBL_CadsatroBanner');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '50.LBL_CadsatroBanner' ? 'Cadastro de Banner.' : mensagem;
        };

        //Modo de pesquisa (não utilizado)
        $scope.onSearchMode = function (item) {
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.exibeVoltar = true;

            var mensagem = $translate.instant('50.LBL_PesquisarBanner');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '50.LBL_PesquisarBanner' ? 'Pesquisar Banner' : mensagem;
        };

        //Edição de informações dos EspaçosBanners
        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('EspacoBannerBO', 'Update', function () {

                Utils.bloquearCamposVisualizacao("VisualizarBanner");

                $rootScope.codigoespacobanner = item.codigo;
                $scope.tableBanner.reload();
                $scope.tableBanner.page(1);
                $rootScope.quantidademaxima = item.quantidademaxima;
                $rootScope.form = $scope.form;
                $scope.itemBanner = angular.copy(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.exibeVoltar = true;

                var mensagem = $translate.instant('50.LBL_EditarBanner');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '50.LBL_EditarBanner' ? 'Editar Banner' : mensagem;
            });
        };

        //Selecionar primeira aba
        $scope.onCancelImage = function (item) {
            $scope.apareceAba = true;
            $('#tabs-banner a:first').tab('show');
        };

        //Cancelar alteração de EspaçoBanner
        $scope.onCancel = function (item) {
            $scope.itemsBanners = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.apareceAba = true;
            $scope.exibeVoltar = false;

            var mensagem = $translate.instant('50.LBL_CadastroBanner');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '50.LBL_CadastroBanner' ? 'Cadastro de Banner' : mensagem;
        };

        //Limpa variável de escopo
        $scope.onClean = function (item) {
            $scope.itemBanner = {};
        };

        //Método de pesquisa
        $scope.onSearch = function (item) {
            espacobanner.PesquisarPerfil($scope.itemBanner, function () {
                $scope.form.submitted = false;
                $scope.editMode = false;
                $scope.tableEspacoBanner.page(1);
            });
        };

        //Remove imagem selecionada
        $rootScope.onDeleteImage = function (item) {
            $(".remove").click();
        }

        //Alterar informações dos EspaçosBanners
        $scope.onSave = function (item) {
            if ($scope.form.$valid) {

                var parametro = { Nome: item.nome, Descricao: item.descricao, Status: item.status, Altura: item.altura, Largura: item.largura, UrlPublicacao: item.urlpublicacao, QuantidadeMaxima: item.quantidademaxima };
                var dados = { campos: parametro, where: item.codigo };
                if (item.quantidademaxima < $scope.itemsBanners.length)
                {
                    var mensagem = $translate.instant('50.MENSAGEM_QuantidadeMaximaDeFotos');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_QuantidadeMaximaDeFotos' ? 'Este banner possui mais imagens que a quantidade máxima informada. Não é possível alterar esta informação.' : mensagem);
                }
                else 
                {
                    espacobanner.postRequisicao('EspacoBannerBO', 'Update', dados).then(
                        function (result) {
                            //$scope.itemBanner = {};
                            $rootScope.quantidademaxima = item.quantidademaxima;
                            $scope.tableEspacoBanner.reload();
                            $scope.form.submitted = false;
                            //$scope.editMode = false;
                            $scope.exibeVoltar = false;

                            var mensagem = $translate.instant('50.MENSAGEM_RegistroSalvoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                }
            }
            else {
                $scope.form.submitted = true;

                var mensagem = $translate.instant('50.LBL_CamposObrigatoriosInvalidos');
                $scope.alert(typeof mensagem === 'object' || mensagem == '50.LBL_CamposObrigatoriosInvalidos' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);

            }
        };

        //Remover Imagens (Banners) cadastrados
        $scope.onDelete = function (item) {
            $rootScope.verificaPermissao('BannerBO', 'Delete', function () {

                var mensagem = $translate.instant('50.MENSAGEM_DesejaRemover');
                $rootScope.confirm(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {

                    angular.forEach($scope.items, function (value, key) {
                        if (value.Codigo === item.Codigo) {
                            $scope.items[key] = item;
                        }
                    });

                    espacobanner.postRequisicao('BannerBO', 'Delete', { where: item.codigo }).then(
                        function (result) {
                            var index = $scope.items.indexOf(item);
                            $scope.items.splice(index, 1);
                            $scope.tableBanner.reload();
                            $scope.tableBanner.page(1);
                            $scope.tableEspacoBanner.reload();

                            var mensagem = $translate.instant('50.MENSAGEM_RegistroExcluidoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_RegistroExcluidoSucesso' ? 'Registro excluído com sucesso.' : mensagem);

                        },
                        $rootScope.TratarErro
                    );
                });
            });
        };

        $scope.onSearchMode($scope.itemBanner);
    } ]);

//Controller para métodos do modal
var ModalBannerCtrl = function ($scope, $http, $rootScope, $modalInstance, blockUI, genericService, bannerservice, item, Utils, $translate) {
        $scope.desabilitarCamposModal = false;
        $scope.form = {}
        $scope.item = item;
        
        var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
            return obj === 'VisualizarBanner';
        });
                   
        if (retorno != undefined && retorno != null && retorno.length > 0) 
        {
            $scope.desabilitarCamposModal = true;
        }
        else
        {
            $scope.desabilitarCamposModal = false;    
        }
        

        $scope.selected = {
            item: angular.copy(item)
        };

        var navigatorVersion = $rootScope.getInternetExplorerVersion();

        if (navigatorVersion == -1 || navigatorVersion > 9) {
            setTimeout(function () {
                $('#MultipleFilesUpload').ace_file_input({
                    no_file: 'Selecione uma imagem ...',
                    btn_choose: 'Selecione',
                    btn_change: 'Alterar',
                    droppable: false,
                    onchange: null,
                    thumbnail: 'small'
                });
                $("#MultipleFilesUpload").ace_file_input('show_file_list', [$scope.selected.item.nomeimagem]);
                $(".remove").on("click", function () {
                    if ($scope.selected.item != undefined) {
                        $scope.selected.item.nomeimagem = null;
                    }
                });
            }, 100);
        }

        //Botão Salvar do modal
        $scope.ok = function (item) {

            function findByOrdem(source, ordem) {
                for (var i = 0; i < source.length; i++) {
                    if (source[i].ordem === ordem) {
                        source[i].position = i;
                        return source[i];
                    }
                }
                return false;
            }

            if (item == undefined || item.nomeimagem == null) {
                $scope.form.banner.$valid = false;
            }

            //Função de validação de data
            function isValidDate(dateString) {
                // First check for the pattern
                if (!/^\d{4}\/\d{2}\/\d{2}$/.test(dateString))
                    return false;

                // Parse the date parts to integers
                var parts = dateString.split("/");
                var day = parseInt(parts[2], 10);
                var month = parseInt(parts[1], 10);
                var year = parseInt(parts[0], 10);

                // Check the ranges of month and year
                if (year < 1000 || year > 3000 || month == 0 || month > 12)
                    return false;

                var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

                // Adjust for leap years
                if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                    monthLength[1] = 29;

                // Check the range of the day
                return day > 0 && day <= monthLength[month - 1];
            };

            if ($scope.form.banner.$valid) {
                var df = item.datafimpublicacao.split('/');
                var di = item.datainiciopublicacao.split('/');
                var data;

                var dataFim = df[1] + "/" + df[0] + "/" + df[2];
                var dataInicio = di[1] + "/" + di[0] + "/" + di[2];
                var dataInicioCompare = di[2] + "/" + di[1] + "/" + di[0];
                var dataFimCompare = df[2] + "/" + df[1] + "/" + df[0];
                var imagem;
                d = new Date();
                var dataAtual = d.mmddyyyy();

                //Edição de banner
                if (item.codigo != null || item.codigo != undefined) {
                    data = item.link;
                    if (item.nomeimagem.indexOf("/upload/") > -1 || item.nomeimagem.indexOf("blob.core.windows.net") > -1) {
                        var parametro = { Nome: item.nome, Link: item.link, Descricao: item.descricao, Status: item.status, NomeImagem: item.nomeimagem, DataInicioPublicacao: dataInicio, DataFimPublicacao: dataFim };
                        var dados = { campos: parametro, where: item.codigo };

                        if (!isValidDate(dataInicioCompare) || !isValidDate(dataFimCompare)) {

                            var mensagem = $translate.instant('50.MENSAGEM_DataInvalida');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_DataInvalida' ? 'Data inválida.' : mensagem);
                        }
                        else if (dataInicioCompare > dataFimCompare) {

                            var mensagem = $translate.instant('50.MENSAGEM_DataInicialMaiorFinal');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_DataInicialMaiorFinal' ? 'A data inicial não pode ser maior que a data de final.' : mensagem);
                        }
                        else if (item.link != undefined && item.link != '' && data.startsWith('http://') == false && data.startsWith('https://') == false) {

                            var mensagem = $translate.instant('50.LBL_URL');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '50.LBL_URL' ? 'Infomar uma URL válida. Exemplo: http://www.minhaempresa.com.br' : mensagem);

                        }
                        else {
                            if (!findByOrdem($rootScope.itemsBanners, parseInt(item.ordem)) || (findByOrdem($rootScope.itemsBanners, parseInt(item.ordem)) && $rootScope.itemsBanners[findByOrdem($rootScope.itemsBanners, parseInt(item.ordem)).position].codigo == item.codigo)) {
                                genericService.postRequisicao('BannerBO', 'Update', dados).then(
                                            function (result) {
                                                genericService.postRequisicao('BannerBO', 'UpdateBannerEspacoBanner', { campos: { codigoespacobanner: $rootScope.codigoespacobanner.toString(), ordem: item.ordem }, where: { codigobannerespacobanner: item.codigobannerespacobanner} }).then(
                                                    function (result) {
                                                        $scope.form.submitted = false;
                                                        $scope.editMode = false;
                                                        $modalInstance.close();

                                                        var mensagem = $translate.instant('50.MENSAGEM_RegistroSalvoSucesso');
                                                        $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);

                                                    },
                                                    $rootScope.TratarErro
                                                );
                                            },
                                            $rootScope.TratarErro
                                        );
                            }
                            else {

                                var mensagem = $translate.instant('50.MENSAGEM_ExisteBannerComEstaOrdem');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_ExisteBannerComEstaOrdem' ? 'Já existe um banner cadastrado com esta ordem.' : mensagem);

                            }
                        }
                    }
                    //Se não, upa a imagem novamente.
                    else {
                        Utils.upload('MultipleFilesUpload', 'blobAzure', 'BannerBO', function (result) {

                            blockUI.stop();

                            if (result != undefined && result[0] == "Extensao nao suportada") {

                                var mensagem = $translate.instant('50.MENSAGEM_SelecionarImagemParaProsseguir');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_SelecionarImagemParaProsseguir' ? 'Favor escolher uma imagem JPG ou PNG para prosseguir.' : mensagem);
                            }

                            else {
                                if (result.length > 0) {
                                    item.nomeimagem = result[0];
                                }

                                var parametro = { Nome: item.nome, Link: item.link, Descricao: item.descricao, Status: item.status, NomeImagem: item.nomeimagem, DataInicioPublicacao: dataInicio, DataFimPublicacao: dataFim };
                                var dados = { campos: parametro, where: item.codigo };

                                if (!isValidDate(dataInicioCompare) || !isValidDate(dataFimCompare)) {

                                    var mensagem = $translate.instant('50.MENSAGEM_DataInvalida');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_DataInvalida' ? 'Data inválida.' : mensagem);
                                }
                                else if (dataInicioCompare > dataFimCompare) {

                                    var mensagem = $translate.instant('50.MENSAGEM_DataInicialMaiorFinal');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_DataInicialMaiorFinal' ? 'A data inicial não pode ser maior que a data de final.' : mensagem);
                                }
                                else if (item.link != undefined && item.link != '' && data.startsWith('http://') == false && data.startsWith('https://') == false) {

                                    var mensagem = $translate.instant('50.LBL_URL');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '50.LBL_URL' ? 'Infomar uma URL válida. Exemplo: http://www.minhaempresa.com.br' : mensagem);

                                }
                                else {
                                    if (!findByOrdem($rootScope.itemsBanners, parseInt(item.ordem)) || (findByOrdem($rootScope.itemsBanners, parseInt(item.ordem)) && $rootScope.itemsBanners[findByOrdem($rootScope.itemsBanners, parseInt(item.ordem)).position].codigo == item.codigo)) {
                                        genericService.postRequisicao('BannerBO', 'Update', dados).then(
                                            function (result) {
                                                genericService.postRequisicao('BannerBO', 'UpdateBannerEspacoBanner', { campos: { codigoespacobanner: $rootScope.codigoespacobanner.toString(), ordem: item.ordem }, where: { codigobannerespacobanner: item.codigobannerespacobanner} }).then(
                                                    function (result) {
                                                        $scope.form.submitted = false;
                                                        $scope.editMode = false;
                                                        $modalInstance.close();

                                                        var mensagem = $translate.instant('50.MENSAGEM_RegistroSalvoSucesso');
                                                        $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                                                    },
                                                    $rootScope.TratarErro
                                                );
                                            },
                                            $rootScope.TratarErro
                                        );
                                    }
                                    else {
                                        var mensagem = $translate.instant('50.MENSAGEM_ExisteBannerComEstaOrdem');
                                        $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_ExisteBannerComEstaOrdem' ? 'Já existe um banner cadastrado com esta ordem.' : mensagem);
                                    }
                                }
                            }
                        });
                    }
                }
                //Inserção de banner
                else if (item.codigo == null || item.codigo == undefined) {
                    var data = item.link;

                    //uploadImagem(function (result) {
                    Utils.upload('MultipleFilesUpload', 'blobAzure', 'BannerBO', function (result) {
                        blockUI.stop();

                        var parametro = { Nome: item.nome, Link: item.link, Descricao: item.descricao, Status: item.status, NomeImagem: imagem, DataInicioPublicacao: dataInicio, DataFimPublicacao: dataFim };

                        if (result[0] == "Extensao nao suportada") {

                            var mensagem = $translate.instant('50.MENSAGEM_SelecionarImagemParaProsseguir');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_SelecionarImagemParaProsseguir' ? 'Favor escolher uma imagem JPG ou PNG para prosseguir.' : mensagem);

                        }

                        else {
                            if (result.length > 0) {
                                item.nomeimagem = result[0];
                            }

                            item.codigoespacobanner = $rootScope.codigoespacobanner.toString();

                            var parametro = { Nome: item.nome, Link: item.link, Descricao: item.descricao, Status: item.status, NomeImagem: item.nomeimagem, DataInicioPublicacao: dataInicio, DataFimPublicacao: dataFim };

                            if (!isValidDate(dataInicioCompare) || !isValidDate(dataFimCompare)) {

                                var mensagem = $translate.instant('50.MENSAGEM_DataInvalida');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_DataInvalida' ? 'Data inválida.' : mensagem);
                            }
                            else if (dataInicio < dataAtual) {
								
								var mensagem = $translate.instant('50.MSG_InicioPublicacaoMaior');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '50.MSG_InicioPublicacaoMaior' ? 'A data de início da publicação não pode ser menor do que a data atual.' : mensagem);
                            }
                            else if (dataInicioCompare > dataFimCompare) {
                                var mensagem = $translate.instant('50.MSG_InicioPublicacaoMaior');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '50.MSG_InicioPublicacaoMaior' ? 'A data de início da publicação não pode ser menor do que a data atual.' : mensagem);
                            }
                            else if (dataInicioCompare > dataFimCompare) {
							
                                var mensagem = $translate.instant('50.MENSAGEM_DataInicialMaiorFinal');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_DataInicialMaiorFinal' ? 'A data inicial não pode ser maior que a data de final.' : mensagem);
                            }
                            else if (item.link != undefined && item.link != '' && (data.startsWith('http://') == false && data.startsWith('https://') == false)) {

                                var mensagem = $translate.instant('50.LBL_URL');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '50.LBL_URL' ? 'Infomar uma URL válida. Exemplo: http://www.minhaempresa.com.br' : mensagem);

                            }
                            else {
                                if (!findByOrdem($rootScope.itemsBanners, parseInt(item.ordem))) {
                                    genericService.postRequisicao('BannerBO', 'Insert', { campos: parametro, where: { codigoespacobanner: item.codigoespacobanner, ordem: item.ordem} }).then(
                                    function (result) {
                                        $scope.editMode = false;
                                        $modalInstance.close();

                                        var mensagem = $translate.instant('50.MENSAGEM_RegistroSalvoSucesso');
                                        $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                                    },
                                    $rootScope.TratarErro
                                );
                                }
                                else {
                                    var mensagem = $translate.instant('50.MENSAGEM_ExisteBannerComEstaOrdem');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '50.MENSAGEM_ExisteBannerComEstaOrdem' ? 'Já existe um banner cadastrado com esta ordem.' : mensagem);
                                }
                            }
                        }
                    });
                }
            } else {
                $scope.form.banner.submitted = true;

                var mensagem = $translate.instant('50.LBL_CamposObrigatoriosInvalidos');
                $scope.alert(typeof mensagem === 'object' || mensagem == '50.LBL_CamposObrigatoriosInvalidos' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            }
        };

        if (typeof String.prototype.startsWith != 'function') {
            // see below for better implementation!
            String.prototype.startsWith = function (str) {
                return this.indexOf(str) == 0;
            };
        }

        //Cancelar 
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        Date.prototype.mmddyyyy = function () {

            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = this.getDate().toString();

            date = (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + '/' + yyyy;
            return date;
        };
    }
