app.controller('CadastroAjudaPassoAPassoCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$log', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $log, $modal, genericService, Utils, blockUI, $q, $translate) {

    //inicializando abas do form
    $('#tabs-ajuda a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var mensagem = $translate.instant('44.LBL_GestaoAjudaPassoPasso');
    $scope.titulo = typeof mensagem === 'object' || mensagem == '44.LBL_GestaoAjudaPassoPasso' ? 'Gestão de Ajuda Passo a Passo' : mensagem;

    $scope.form = {};
    $scope.itemForm = {};
    $scope.isPesquisa = false;
    $scope.blockInputs = false;

    $scope.menuWysiwyg = [
            ['font'],
            ['font-size'],
            ['bold', 'italic', 'underline', 'strikethrough'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['code', 'paragraph']
        ];

    $scope.moveScrollTop = function () {
        $(window).scrollTop(0);
        $(".page-container").scrollTop(0);
    }

    $scope.tableCampos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridCampos($scope.itemForm, function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
            });
        }
    });

    //Exibe um modal com o anúncio da Ajuda cadastrada
    $scope.geraDetalhes = function (mensagem, subtitulo, video, sucessCallback) {

        var modalInstance = $modal.open({
            template: '<div class="modal-header">' +
                            '<button type="button" class="bootbox-close-button close" ng-click="cancel()">x</button>' +
                            '<h3>{{title}}</h3>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="text-center">' +
                                '<h4>{{subtitle}}</h4>' +
                            '</div>' +
                            '<div>' +
                                '<h5 style="color: black;"><span translate="44.LBL_VisaoGeral">Visão Geral</span>: </h5>' +
                                '<p ng-bind-html="message"></p><!--{{message}}-->' +
                            '</div>' +
                            '<div ng-if="video != undefined && video != \'\'">' +
                                '<br>' +
                                '<h5 style="color: black;"><span translate="44.LBL_VideoExplicativo">Vídeo Explicativo</span>: </h5>' +
                                '<iframe style="z-index:0!important;" id="iframe_youtube" width="523" height="388" ng-src="{{video | trusted}}" frameborder="0" allowfullscreen></iframe>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<button class="btn btn-info" ng-click="ok()">OK</button>' +
                        '</div>',
            size: 'lg',
            controller: function ($scope, $modalInstance) {
                $scope.message = mensagem;

                var msg = $translate.instant('44.LBL_VisualizacaoPreviaPassoPasso');
                $scope.titulo = typeof msg === 'object' || msg == '44.LBL_VisualizacaoPreviaPassoPasso' ? 'Visualização Prévia - Passo a Passo' : msg;
                $scope.subtitle = subtitulo;
                $scope.video = video || undefined;

                Utils.EmbedYoutube($scope.video, function (data) {
                    $scope.video = data;
                });

                $scope.ok = function () {
                    $(window).scrollTop(0);
                    $(".page-container").scrollTop(0);
                    $modalInstance.close();
                };

                $scope.cancel = function () {
                    $modalInstance.close();
                };
            }
        });

        modalInstance.result.then(sucessCallback);

    }

    //Popula grid de campos
    function carregaGridCampos(item, callback) {

        var dados = null;
        dados = angular.copy(item);

        genericService.postRequisicao('CadastroAjudaPassoaPassoBO', 'BuscarCampos', { campos: dados }).then(
            function (result) {
                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
    };

    //Popula dropdown de modulos
    $scope.carregaModulos = function () {
        return $q(function (resolve, reject) {
            genericService.postRequisicao('AjudaBO', 'BuscarModulosAtivos', { campos: {} }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        $scope.modulos = result.data;
                        resolve(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        });
    };

    //Popula dropdown de formularios
    $scope.carregaFormularios = function (item) {
        return $q(function (resolve, reject) {
            $scope.formularios = {};
            genericService.postRequisicao('AjudaBO', 'BuscarFormulariosAtivos', { campos: { modulo: item} }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        $scope.formularios = result.data;
                        $scope.carregaCampos();
                        resolve(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        });
    };

    //Busca os Status da Ajuda
    $scope.buscaStatusPublicacao = function () {
        genericService.postRequisicao('AjudaBO', 'BuscarStatusPublicacao', {}).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $scope.statusPublicacao = result.data;
                }
            },
            $rootScope.TratarErro
        );
    }

    //Busca os Idiomas do Sistema
    $scope.buscaIdiomas = function () {
        genericService.postRequisicao('IdiomaBO', 'BuscarIdiomas', {}).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $scope.idiomas = result.data;
                }
            },
            $rootScope.TratarErro
        );
    }

    //Método Inicial da Página
    $scope.onLoad = function () {
        $scope.shouldLoadCampos = true;
        $scope.itemForm = {};
        $scope.itemForm.codigoidioma = 1;
        $scope.tableCampos.reload();
        $scope.tableCampos.page(1);
        $scope.shouldLoadCampos = false;
        $scope.blockInputs = false;
        $scope.carregaModulos();
        $scope.buscaStatusPublicacao();
        $scope.buscaIdiomas();
        $scope.moveScrollTop();
    }

    $scope.onLoad();

    $scope.onClean = function () {
        $scope.onLoad();
    }

    //Chamado ao alterar o idioma
    $scope.alteraIdioma = function (item) {
        //if (item.codigoformulario != undefined && item.codigoformulario != '' && item.codigomodulo != undefined && item.codigomodulo != '') {
        $scope.itemForm = angular.copy(item);
        $scope.tableCampos.reload();
        $scope.tableCampos.page(1);
        //}
    }

    $scope.carregaCampos = function () {
        $scope.shouldLoadCampos = true;
        $scope.tableCampos.reload();
        $scope.tableCampos.page(1);
    }

    //Método para abertura do modal ModalEditaCampos
    $scope.onEditCampo = function (item) {
        $rootScope.verificaPermissao('CadastroAjudaPassoaPassoBO', 'AlteraFormularioAjudaPassoAPasso', function () {

            Utils.bloquearCamposVisualizacao("VisualizarAjudaPassoaPasso");

            var modalInstance = $modal.open({
                templateUrl: 'ModalEditaCampos',
                size: 'lg',
                backdrop: 'static',
                resolve: {
                    statusPublicacao: function () {
                        return $scope.statusPublicacao;
                    },
                    idiomas: function () {
                        return $scope.idiomas;
                    }
                },
                controller: function ($scope, $modalInstance, statusPublicacao, idiomas, Utils) {
                    $scope.desabilitarCamposModal = false;
                    $scope.form = {};
                    $scope.campoForm = angular.copy(item);
                    $scope.statusPublicacao = angular.copy(statusPublicacao);
                    $scope.idiomas = angular.copy(idiomas);
                    //Utils.bloquearCamposVisualizacao("VisualizarAjudaPassoaPasso");
                    
                    var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
                        return obj === 'VisualizarAjudaPassoaPasso';
                    });
                   
                    if (retorno != undefined && retorno != null && retorno.length > 0) 
                    {
                        $scope.desabilitarCamposModal = true;        
                    }
                    else
                    {
                        $scope.desabilitarCamposModal = false;   
                    }

                    //Cancelar 
                    $scope.cancel = function () {
                        $(window).scrollTop(0);
                        $(".page-container").scrollTop(0);
                        $modalInstance.close(true);
                    };

                    $scope.ok = function () {
                        if (!$scope.form.campo.$valid || ($scope.campoForm.ajuda == undefined || $scope.campoForm.ajuda == '')) {

                            if ($scope.campoForm.ajuda == undefined || $scope.campoForm.ajuda == '') {
                                angular.element('div[id^="cke_editor"]').addClass('borderCkeditor');
                            }
                            else {
                                angular.element('div[id^="cke_editor"]').removeClass('borderCkeditor');
                            }

                            var mensagem = $translate.instant('44.MENSAGEM_CamposObrigatorios');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '44.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);

                            $scope.form.campo.submitted = true;
                        }
                        else {
                            angular.element('div[id^="cke_editor"]').removeClass('borderCkeditor');

                            //Se possuir URL mas estiver inválida
                            if ($scope.form.campo.video.$viewValue != undefined && $scope.form.campo.video.$viewValue != '' && !Utils.youtubeURLValidator($scope.form.campo.video.$viewValue)) {

                                var mensagem = $translate.instant('44.MENSAGEM_InformeURLValida');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '44.MENSAGEM_InformeURLValida' ? 'Informe uma URL válida.' : mensagem);

                                $scope.form.campo.submitted = true;
                                return;
                            }
                            else {
                                Utils.EmbedYoutube($scope.campoForm.video, function (data) {
                                    $scope.campoForm.video = data;
                                });

                                genericService.postRequisicao('CadastroAjudaPassoaPassoBO', 'AlteraFormularioAjudaPassoAPasso', { campos: $scope.campoForm }).then(
                                    function (result) {
                                        if (result.data != undefined && result.data != null) {
                                            $modalInstance.close(true);
                                            var mensagem = $translate.instant('44.MENSAGEM_RegistroAlteradoSucesso');
                                            $scope.alert(typeof mensagem === 'object' || mensagem == '44.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                                        }
                                    },
                                    $rootScope.TratarErro
                                );
                            }
                        }
                    }

                }
            });

            modalInstance.result.then(function (data) {
                Utils.desbloquearCamposVisualizacaoPesquisa();
                $scope.tableCampos.reload();
            });
        });
    }

} ]);