app.controller('CadastroFAQCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$log', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $log, $modal, genericService, Utils, blockUI, $q, $translate) {

    //inicializando abas do form
    $('#tabs-ajuda a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var mensagem = $translate.instant('40.LBL_CadastroFAQ');
    $scope.titulo = typeof mensagem === 'object' || mensagem == '40.LBL_CadastroFAQ' ? 'Cadastro de Perguntas Frequentes - FAQ.' : mensagem;

    $scope.form = {};
    $scope.itemForm = {};
    $scope.isPesquisa = false;
    $scope.blockInputs = false;

    $scope.menuWysiwyg = [
            ['font'],
            ['font-size'],
            ['bold', 'italic', 'underline', 'strikethrough'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['paragraph']
        ];

    $scope.moveScrollTop = function () {
        $(window).scrollTop(0);
        $(".page-container").scrollTop(0);
    }

    $scope.tableCampos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridCampos($scope.campoForm, function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
            });
        }
    });

    //Exibe um modal com o anúncio da Ajuda cadastrada
    $scope.geraDetalhes = function (pergunta, resposta, video, sucessCallback) {

        var modalInstance = $modal.open({
            template: '<div class="modal-header">' +
                            '<button type="button" class="bootbox-close-button close" ng-click="cancel()">x</button>' +
                            '<h3>{{title}}</h3>' +
                        '</div>' +
                        '<div class="modal-body">' +
                            '<div class="text-center">' +
                                '<h4>{{subtitle}}</h4>' +
                            '</div>' +
                            '<div>' +
                                '<h5 style="color: black;"><span translate="40.LBL_Pergunta">Pergunta</span>: <span ng-bind="pergunta"></span></h5>' +
                                '<p ng-bind-html="message"></p><!--{{message}}-->' +
                            '</div>' +
                            '<div ng-if="video != undefined && video != \'\'">' +
                                '<br>' +
                                '<h5 style="color: black;"><span translate="40.LBL_VideoExplicativo">Vídeo Explicativo</span>: </h5>' +
                                '<iframe style="z-index:0!important;" id="iframe_youtube" width="523" height="388" ng-src="{{video | trusted}}" frameborder="0" allowfullscreen></iframe>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<button class="btn btn-info" ng-click="ok()">OK</button>' +
                        '</div>',
            size: 'lg',
            controller: function ($scope, $modalInstance) {
                $scope.message = resposta;

                var mensagem = $translate.instant('40.LBL_VisualizacaoPreviaFAQ');
                $scope.title = typeof mensagem === 'object' || mensagem == '40.LBL_VisualizacaoPreviaFAQ' ? 'Visualização Prévia - FAQ.' : mensagem;
                $scope.pergunta = pergunta;
                $scope.video = video || undefined;

                Utils.EmbedYoutube($scope.video, function (data) {
                    $scope.video = data;
                });

                $scope.ok = function () {
                    $scope.moveScrollTop();
                    $modalInstance.close();
                };

                $scope.cancel = function () {
                    $scope.moveScrollTop();
                    $modalInstance.close();
                };
            }
        });

        modalInstance.result.then(sucessCallback);

    }

    //Popula grid de campos
    function carregaGridCampos(item, callback) {

        var dados = null;


        if ($scope.isPesquisa) {
            dados = angular.copy(item);

            genericService.postRequisicao('CadastroFAQBO', 'BuscarFAQsPesquisa', { campos: dados }).then(
                function (result) {

                    $scope.form.submitted = false;
                    $scope.editMode = false;

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }
        else {
            genericService.postRequisicao('CadastroFAQBO', 'BuscarFAQs', { campos: dados }).then(
                function (result) {
                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

    };

    //Busca os Status da Ajuda
    $scope.buscaStatusPublicacao = function () {
        genericService.postRequisicao('CadastroFAQBO', 'BuscarStatusPublicacao', {}).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $scope.statusPublicacao = result.data;
                }
            },
            $rootScope.TratarErro
        );
    }

    //Busca os Tipos de Disponibilidade
    $scope.buscaTiposDisponibilidade = function () {
        genericService.postRequisicao('CadastroFAQBO', 'BuscarTiposDisponibilidade', {}).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $scope.tiposDisponibilidade = result.data;
                }
            },
            $rootScope.TratarErro
        );
    }

    //Busca os Idiomas do Sistema
    $scope.buscaIdiomas = function () {
        genericService.postRequisicao('IdiomaBO', 'BuscarIdiomas', {}).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $scope.idiomas = result.data;
                }
            },
            $rootScope.TratarErro
        );
    }

    //Método para abertura do modal ModalEditaCampos
    $scope.onFormMode = function () {
        $rootScope.verificaPermissao('CadastroFAQBO', 'CadastraFAQ', function () {
            $scope.moveScrollTop();
            var mensagem = $translate.instant('40.LBL_CadastroPergunta');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '40.LBL_CadastroPergunta' ? 'Cadastro de Pergunta.' : mensagem;
            $scope.campoForm = {};
            $scope.isPesquisa = false;
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.exibeVoltar = true;
            $scope.initMode = true;
            $("#resposta").text('');
            Utils.bloquearCamposVisualizacao("VisualizarFAQ");

        });
    };

    //Modo de Edição
    $scope.onEdit = function (item) {
        $rootScope.verificaPermissao('CadastroFAQBO', 'AlteraFAQ', function () {
            $scope.moveScrollTop();
            var mensagem = $translate.instant('40.LBL_CadastroPergunta');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '40.LBL_CadastroPergunta' ? 'Cadastro de Pergunta.' : mensagem;
            $scope.campoForm = {};
            $scope.isPesquisa = false;
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.exibeVoltar = true;
            $scope.initMode = false;
            $scope.campoForm = angular.copy(item);
            Utils.bloquearCamposVisualizacao("VisualizarFAQ");

        });
    };

    //Pesquisa FAQ cadastrado
    $scope.onSearch = function (item) {
        $scope.exibeVoltar = false;
        $scope.moveScrollTop();
        $scope.tableCampos.page(1);
        $scope.tableCampos.reload();
    }

    $scope.onSearchMode = function () {
        var mensagem = $translate.instant('40.LBL_PesquisarPergunta');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '40.LBL_PesquisarPergunta' ? 'Pesquisar FAQ.' : mensagem;

        Utils.desbloquearCamposVisualizacaoPesquisa();
        $scope.campoForm = {};
        $scope.editMode = true;
        $scope.searchMode = true;
        $scope.isPesquisa = true;
        $scope.exibeVoltar = true;
        $("#resposta").text('');
    }

    //Salvar ou Editar FAQ
    $scope.onSave = function () {

        if (!$scope.form.$valid) {
            var mensagem = $translate.instant('40.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '40.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            $scope.form.submitted = true;
        }
        else {
            //Se possuir URL mas estiver inválida
            if ($scope.form.video.$viewValue != undefined && $scope.form.video.$viewValue != '' && !Utils.youtubeURLValidator($scope.form.video.$viewValue)) {
                var mensagem = $translate.instant('40.MENSAGEM_InformeURLValida');
                $scope.alert(typeof mensagem === 'object' || mensagem == '40.MENSAGEM_InformeURLValida' ? 'Informe uma URL válida.' : mensagem);
                $scope.form.submitted = true;
                return;
            }
            else {
                if ($scope.campoForm.codigo != undefined && $scope.campoForm.codigo != '') {
                    genericService.postRequisicao('CadastroFAQBO', 'AlteraFAQ', { campos: $scope.campoForm }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.moveScrollTop();
                                var mensagem = $translate.instant('40.LBL_CadastroFAQ');
                                $scope.titulo = typeof mensagem === 'object' || mensagem == '40.LBL_CadastroFAQ' ? 'Cadastro de Perguntas Frequentes - FAQ.' : mensagem;
                                $scope.campoForm = {};
                                $scope.editMode = false;
                                $scope.searchMode = false;
                                $scope.form.submitted = false;
                                $scope.isPesquisa = false;
                                $scope.exibeVoltar = false;
                                $scope.tableCampos.page(1);
                                $scope.tableCampos.reload();
                                var mensagem = $translate.instant('40.MENSAGEM_RegistroAlteradoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '40.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                            }
                        },
                        $rootScope.TratarErro
                    );
                }
                else {
                    genericService.postRequisicao('CadastroFAQBO', 'CadastraFAQ', { campos: $scope.campoForm }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.moveScrollTop();
                                var mensagem = $translate.instant('40.LBL_CadastroFAQ');
                                $scope.titulo = typeof mensagem === 'object' || mensagem == '40.LBL_CadastroFAQ' ? 'Cadastro de Perguntas Frequentes - FAQ.' : mensagem;
                                $scope.campoForm = {};
                                $scope.editMode = false;
                                $scope.searchMode = false;
                                $scope.form.submitted = false;
                                $scope.isPesquisa = false;
                                $scope.exibeVoltar = false;
                                $scope.tableCampos.page(1);
                                $scope.tableCampos.reload();

                                var mensagem = $translate.instant('40.MENSAGEM_RegistroSalvoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '40.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                            }
                        },
                        $rootScope.TratarErro
                    );
                }
            }
        }
    }

    $scope.cancelar = function () {
        $scope.moveScrollTop();
        var mensagem = $translate.instant('40.LBL_CadastroFAQ');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '40.LBL_CadastroFAQ' ? 'Cadastro de Perguntas Frequentes - FAQ.' : mensagem;
        $scope.campoForm = {};
        $scope.editMode = false;
        $scope.searchMode = false;
        $scope.form.submitted = false;
        $scope.isPesquisa = false;
        $scope.exibeVoltar = false;
        $scope.tableCampos.page(1);
        $scope.tableCampos.reload();

    };

    //Método Inicial da Página
    $scope.onLoad = function () {
        $scope.shouldLoadCampos = true;
        $scope.campoForm = {};
        $scope.campoForm.codigoidioma = 1;
        $scope.shouldLoadCampos = false;
        $scope.blockInputs = false;
        $scope.moveScrollTop();
        $scope.buscaStatusPublicacao();
        $scope.buscaTiposDisponibilidade();
        $scope.buscaIdiomas();
    }

    $scope.onLoad();

    $scope.onClean = function () {
        $scope.campoForm = {};
    }

    //Método para abertura do modal ModalEditaCampos
    $scope.onEditCampo = function (item) {
        $scope.initMode = false;
        var modalInstance = $modal.open({
            templateUrl: 'ModalEditaCampos',
            size: 'lg',
            resolve: {
                statusPublicacao: function () {
                    return $scope.statusPublicacao;
                },
                idiomas: function () {
                    return $scope.idiomas;
                }
            },
            controller: function ($scope, $modalInstance, statusPublicacao, idiomas) {
                $scope.form = {};
                $scope.campoForm = angular.copy(item);
                $scope.statusPublicacao = angular.copy(statusPublicacao);
                $scope.idiomas = angular.copy(idiomas);
                Utils.bloquearCamposVisualizacao("VisualizarFAQ");
                //Cancelar 
                $scope.cancel = function () {
                    $modalInstance.close(true);
                };

                $scope.ok = function () {

                    if (!$scope.form.$valid) {
                        var mensagem = $translate.instant('40.MENSAGEM_CamposObrigatorios');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '40.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                        $scope.form.submitted = true;
                    }
                    else {
                        //Se possuir URL mas estiver inválida
                        if (!Utils.youtubeURLValidator($scope.form.video.$viewValue)) {
                            var mensagem = $translate.instant('40.MENSAGEM_InformeURLValida');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '40.MENSAGEM_InformeURLValida' ? 'Informe uma URL válida.' : mensagem);
                            $scope.form.submitted = true;
                            return;
                        }
                        else {
                            genericService.postRequisicao('CadastroFAQBO', 'AlteraFormularioAjuda', { campos: $scope.campoForm }).then(
                                function (result) {
                                    if (result.data != undefined && result.data != null) {
                                        $modalInstance.close(true);
                                        var mensagem = $translate.instant('40.MENSAGEM_RegistroAlteradoSucesso');
                                        $scope.alert(typeof mensagem === 'object' || mensagem == '40.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                                    }
                                },
                                $rootScope.TratarErro
                            );
                        }
                    }
                }

            }
        });

        modalInstance.result.then(function (data) {
            $scope.tableCampos.reload();
        });
    };

} ]);