app.controller('CadastroProdutosCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'ProdutoService', 'Utils', 'blockUI','ngAnimate', 
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, produto, Utils, blockUI, ngAnimate) {
    //inicializando abas do form
    $('#tabs-etiqueta a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
        
    $('#collapseInformacoesComplementares a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
        
    $scope.isPesquisa = false;
    $scope.isCollapseInformacoesComplementares = true;
    $scope.images = [{src: 'images/no-photo.jpg'}];
    $scope.maxImgs = 5;
        
        
    $scope.produtos = [
        {codigo: 1, tipo: 'Tipo 1', nome: 'Leite Protótipo A', situacao: 'Personalizada', statusPublicacao: 'Publicado'},
        {codigo: 2, tipo: 'Tipo 2', nome: 'Leite Protótipo X', situacao: 'Personalizada', statusPublicacao: 'Publicado'},
        {codigo: 3, tipo: 'Tipo 1', nome: 'Leite Protótipo Y', situacao: 'Personalizada', statusPublicacao: 'Publicado'},
        {codigo: 4, tipo: 'Tipo 1', nome: 'Leite Protótipo C', situacao: 'Personalizada', statusPublicacao: 'Publicado'},
        {codigo: 5, tipo: 'Tipo 1', nome: 'Leite Protótipo G', situacao: 'Personalizada', statusPublicacao: 'Publicado'},
        {codigo: 6, tipo: 'Tipo 4', nome: 'Leite Protótipo A', situacao: 'Personalizada', statusPublicacao: 'Publicado'},
        {codigo: 7, tipo: 'Tipo 1', nome: 'Leite Protótipo A', situacao: 'Personalizada', statusPublicacao: 'Publicado'},
        {codigo: 8, tipo: 'Tipo 3', nome: 'Leite Protótipo Z', situacao: 'Personalizada', statusPublicacao: 'Publicado'}
    ];
        
    $scope.tableProdutos = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridProdutos($scope.ais, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.item = data;
                });
            }
    });
        
    $scope.expandedAll = function(){
        
        $('.collapse').collapse('show');
        $scope.isCollapseInformacoesComplementares = false;
    }
    
    $scope.focus = function(secao, campo){  
        $('#'+secao).collapse('show');
        $('#'+campo).focus();
    }
    
    function carregaGridProdutos(item, isPesquisa, callback) {
        if (isPesquisa == false) {
            var data = $scope.produtos;
            callback(data);
        }
        else if (isPesquisa == true) {
            var data = $scope.produtos;
            callback(data);
        }
    } ;
        
    function CarregaListaMidias() {
        $scope.isCollapsedMidias = true;
        $scope.images = [];
        
        for (var i = 0; i < $scope.maxImgs; i++) {
            $scope.images.push({ idVideo: '', tipoMidia: '1', ordem: i, src: 'images/no-photo.jpg' });
        }
        
    };
    
    $scope.qtdImages = 0;
    $scope.draggableObjects = $scope.images;

    $scope.onDropComplete = function (index, obj, evt) {
        var otherObj = $scope.draggableObjects[index];
        var otherIndex = $scope.draggableObjects.indexOf(obj);
        var ordem = obj.ordem;
        var otherOrdem = otherObj.ordem;
        obj.ordem = otherOrdem;
        otherObj.ordem = ordem;
        $scope.draggableObjects[index] = obj;
        $scope.draggableObjects[otherIndex] = otherObj;
        $scope.images = $scope.draggableObjects;

        produto.ReordenarImagens(obj, otherObj, $scope.item.codigo, function (data) {
        });
    };

    $scope.loadMore = function () {
        var last = $scope.images[$scope.images.length - 1];
        
        for (var i = 1; i <= 4; i++) {
            $scope.images.push(last + i);
        }
    };
        
    $scope.onFormMode = function (item) {

            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isCollapsedMidias = true;
    };

    $scope.onCancel = function (item) {
        $scope.item = {};
        $scope.editMode = false;
        $scope.searchMode = false;
        $scope.form.submitted = false;
        $scope.isPesquisa = false;

        $scope.tableProdutos.page(1);
        $scope.tableProdutos.reload();
    };

    $scope.onSearchMode = function (item) {
        $scope.editMode = true;
        $scope.searchMode = true;
    };

    $scope.onClean = function (item) {
        $scope.item = {};
    };
        
    $scope.onEdit = function (item) {
        $scope.item = angular.copy(item);
        
        $scope.isCollapsedMidias = true;
        $scope.editMode = true;
        $scope.searchMode = false;
        $scope.isPesquisa = false;
    };
        
    $scope.AbrirModalHierarquia = function () {
            var modalInstance = $modal.open({
                templateUrl: 'ModalHierarquia.html',
                resolve: {

                },
                controller: function ($scope, $modalInstance) {
                    $scope.ok = function () {

                    }

                    $scope.cancel = function () {
                        $modalInstance.close(false);
                    }
                }
            });

            modalInstance.result.then(
                function (result) {

                },
                function () { }
            );
    };
        
    $scope.AbrirModalPreviewInBar = function () {
            var modalInstance = $modal.open({
                templateUrl: 'ModalPreviewInBar.html',
                resolve: {

                },
                controller: function ($scope, $modalInstance) {
                    $scope.ok = function () {

                    }

                    $scope.cancel = function () {
                        $modalInstance.close(false);
                    }
                }
            });

            modalInstance.result.then(
                function (result) {

                },
                function () { }
            );
    };    
        
        
} ]);