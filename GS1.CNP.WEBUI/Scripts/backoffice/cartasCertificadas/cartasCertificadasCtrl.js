app.controller('CartasCertificadasCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {

        $scope.isPesquisa = false;

        var titulo = $translate.instant('48.LBL_CartasCertificadas');
        $scope.titulo = typeof titulo === 'object' || titulo == '48.LBL_CartasCertificadas' ? 'Cartas de Filiação' : titulo;

        $scope.gtinsSelecionados = [];
        $scope.chaves = [];
        $scope.tiposGtin = {};
        $scope.itemForm = {};

        $scope.onInit = function () {
            $rootScope.verificaAssociadoSelecionado(function (data) {
                
                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                    
                    verificaChaves(function (result) {
                        
                        if (result)
                        {
                            $scope.tableCartas = new ngTableParams({
                                page: 1,
                                count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
                                sorting: {
                                    LOGIN: 'asc'
                                }
                            }, {
                                    counts: [],
                                    total: 0,
                                    getData: function($defer, params) {
                                        var dados = carregaGrid($scope.itemForm, $scope.isPesquisa, function(data) {
                                            params.total(data.length);
                                            var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                                            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                                            $scope.items = manterGridSelecionada(data);
                                        });
                                    }
                                });   
                        }
                            
                    });
                    
                    $rootScope.flagAssociadoSelecionado = true;
                    carregaTipoGtin(); 
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }
            });
        };
        $scope.onInit();
        
        function  verificaChaves(callback) {
            genericService.postRequisicao('CartasCertificadasBO', 'VerificarChaves', { campos: {} }).then(
                function (result) {
                    if (result.data.length > 0) {
                        callback(true);
                    }else{
                        var mensagem = $translate.instant(['12.MENSAGEM_EmpresaNaoPossuiChaveAutenticacao', '12.MENSAGEM_NaoPossuiChaveTelefoneGS1']);
                        $scope.alert(
                        typeof mensagem === 'object' || ((mensagem['12.MENSAGEM_EmpresaNaoPossuiChaveAutenticacao'] == '12.MENSAGEM_EmpresaNaoPossuiChaveAutenticacao') || (mensagem['12.MENSAGEM_AtravesNossoChat'] == '12.MENSAGEM_AtravesNossoChat'))
                            ? 'A empresa não possui chave de autenticação gerada. Por favor, entre em contato com a GS1 Brasil: Tel. ' + $rootScope.retornaValorParametroByFiltro('bo.nu.telefone') + ' ou através do nosso chat no portal www.gs1br.org e solicite a geração das chaves de autenticação.'
                            : mensagem['12.MENSAGEM_EmpresaNaoPossuiChaveAutenticacao'] + ' ' + $rootScope.retornaValorParametroByFiltro('bo.nu.telefone') + ' ' + mensagem['12.MENSAGEM_AtravesNossoChat']
                        );
                        
                        $scope.tableCartas = new ngTableParams({
                            page: 1,
                            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
                            sorting: {
                                LOGIN: 'asc'
                            }
                        }, 
                        {
                            counts: [],
                            total: 0,
                            getData: function($defer, params) {
                                var dados = [];
                            }
                        });
                        
                    }
                },
                $rootScope.TratarErro
             );  
        }

        function carregaGrid(item, isPesquisa, callback) {
            var dados = {};

            if (isPesquisa) {
                dados = angular.copy(item);
                genericService.postRequisicao('CartasCertificadasBO', 'BuscarProduto', { campos: dados }).then(
                    function (result) {
                        if (result != undefined && result != null && result.data.length > 0) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                 );

            }
            else {
                genericService.postRequisicao('CartasCertificadasBO', 'BuscarProdutos', { campos: dados }).then(
                    function (result) {
                        
                        if (result != undefined && result != null && result.data.length > 0) {
                                callback(result.data);    
                        }     
                        else
                        {
                            var mensagem = $translate.instant('48.MENSAGEM_NaoExisteProdutoCadastrado');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '48.MENSAGEM_NaoExisteProdutoCadastrado' ? 'Não existem produtos cadastrados para serem exibidos.' : mensagem);    
                        } 
                    },
                    $rootScope.TratarErro
                 );
            }
        };

        function carregaTipoGtin() {
            var dados = {};

            genericService.postRequisicao('CartasCertificadasBO', 'BuscarTiposGtin', { campos: dados }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        $scope.tiposGtin = result.data;
                    }
                },
                $rootScope.TratarErro
             );
        };

        function validarChaves(produtos) {

            for (i = 0; i < produtos.length; i++) {

                if (produtos[i].numeroprefixo == '' || produtos[i].numeroprefixo == undefined) {

                    var mensagem = $translate.instant(['48.MENSAGEM_UmOuMais', '48.MENSAGEM_NaoPossuiChaveTelefoneGS1', '48.MENSAGEM_ChatGeracaoChaveAutenticacao']);

                    $scope.alert(typeof mensagem === 'object' ||
                        ((mensagem['48.MENSAGEM_InformeURLValida'] == '48.MENSAGEM_InformeURLValida') || (mensagem['48.MENSAGEM_NaoPossuiChaveTelefoneGS1'] == '48.MENSAGEM_NaoPossuiChaveTelefoneGS1') ||
                        (mensagem['48.MENSAGEM_ChatGeracaoChaveAutenticacao'] == '48.MENSAGEM_ChatGeracaoChaveAutenticacao')) ? 'Um ou mais ' + produtos[i].tipodegtin +
                        ' não possuem uma chave de autenticação gerada. Por favor, entre em contato com a GS1 Brasil: Tel.' + $rootScope.retornaValorParametroByFiltro('bo.nu.telefone') +
                        ' ou através do nosso chat no portal www.gs1br.org e solicite a geração das chaves de autenticação.' : mensagem);

                    $scope.gtinsSelecionados = [];
                    $scope.tableCartas.page(1);
                    $scope.tableCartas.reload();
                    return false;
                }

            }
            return true;
        }

        $scope.flagMarcarDesmarcar = function (item) {

            if (item.checked) {
                $scope.gtinsSelecionados.push(item);

                if ($scope.chaves.indexOf(item.chave) === -1) {
                    $scope.chaves.push(item.chave);
                }
            }
            else {
                $scope.removerPorGtin($scope.gtinsSelecionados, item.gtin);
            }
        }

        $scope.removerPorGtin = function (item, gtin) {
            for (var i in item) {
                if (item[i].gtin == gtin) {
                    item.splice(i, 1);
                }
            }
        }

        $scope.gerarCarta = function (item) {

            var dados = { produtos: $scope.gtinsSelecionados, chaves: $scope.chaves, currentDate: Utils.longDate() };

            //verificar se todos os dados necessários estão corretos
            if (dados.produtos.length === 0) {

                var mensagem = $translate.instant('48.MENSAGEM_SelecioneUmItemGRID');
                $scope.alert(typeof mensagem === 'object' || mensagem == '48.MENSAGEM_SelecioneUmItemGRID' ? 'Por favor, selecione os produtos desejados para gerar a Carta de Filiação.' : mensagem);

                return false;
            } else {

                if (validarChaves(dados.produtos)) {

                    $("#worddatacartaprodutos").val(JSON.stringify({ 'campos': dados }));

                    $('#WordHandlerCartaProdutos').submit();

                    $timeout(function () {
                        $scope.tableCartas.page(1);
                        $scope.tableCartas.reload();
                        blockUI.stop();
                    }, 100);

                    $scope.gtinsSelecionados = [];
                }

            }
        }

        $scope.onSearchMode = function (item) {
            var titulo = $translate.instant('48.LBL_CartasCertificadas');
            $scope.titulo = typeof titulo === 'object' || titulo == '48.LBL_CartasCertificadas' ? 'Cartas de Filiação' : titulo;

            Utils.desbloquearCamposVisualizacaoPesquisa();

            $scope.itemForm = {};
            $scope.searchMode = true;
        };

        $scope.onSearch = function (item) {
            $scope.searchMode = false;
            $scope.isPesquisa = true;
            $scope.tableCartas.page(1);
            $scope.tableCartas.reload();
        }

        $scope.onClean = function (item) {
            $scope.itemForm = {};
        };

        $scope.onCancel = function (item) {
            var titulo = $translate.instant('48.LBL_CartasCertificadas');
            $scope.titulo = typeof titulo === 'object' || titulo == '48.LBL_CartasCertificadas' ? 'Cartas de Filiação' : titulo;

            $scope.itemForm = {};
            $scope.searchMode = false;
            $scope.tableCartas.page(1);
            $scope.tableCartas.reload();
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        function manterGridSelecionada(data) {
            for (i = 0; i < $scope.gtinsSelecionados.length; i++) {
                for (j = 0; j < data.length; j++) {
                    if ($scope.gtinsSelecionados[i].gtin == data[j].gtin) {
                        data[j].checked = true;
                    }
                }
            }
            return data;
        }

    } ]);
