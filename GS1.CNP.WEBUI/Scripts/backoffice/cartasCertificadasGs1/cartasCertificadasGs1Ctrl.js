app.controller('CartasCertificadasGs1', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {

        var mensagem = $translate.instant('12.LBL_CartasCertificadas');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '12.LBL_CartasCertificadas' ? 'Cartas de Filiação' : mensagem;
        
        $scope.onInit = function () {
            $rootScope.verificaAssociadoSelecionado(function (data) {
                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                    $scope.tableCartasGs1 = new ngTableParams({
                        page: 1,
                        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
                        sorting: {
                            LOGIN: 'asc'
                        }
                    }, {
                        counts: [],
                        total: 0,
                        getData: function ($defer, params) {
                            var dados = carregaGrid($scope.item, function (data) {
                                params.total(data.length);
                                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                                $scope.items = data;
                            });
                        }
                    });

                    $rootScope.flagAssociadoSelecionado = true;
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }
            });
        }

        $scope.onInit();

        //Carregamento inicial do GRID
        function carregaGrid(item, callback) {
            var dados = {};

            genericService.postRequisicao('CartasCertificadasGs1BO', 'BuscaInformacoesAssociado', { campos: dados }).then(
                function (result) {

                    $scope.cartas = result.data;

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        //Geração de carta a partir do template da Licença selecionada
        $scope.gerarCarta = function (item) {
            $rootScope.verificaPermissao('CartasCertificadasGs1BO', 'GerarCarta', function () {
                if (item != undefined && item != null) {
                    if (item.chave == undefined || item.chave == null || item.chave == '') {
                        $scope.telefone = $rootScope.retornaValorParametroByFiltro('bo.nu.telefone');

                        var mensagem = $translate.instant(['12.MENSAGEM_SemChaveLicenca', '12.MENSAGEM_AtravesNossoChat']);
                        $scope.alert(
                            typeof mensagem === 'object' || mensagem == ['12.MENSAGEM_SemChaveLicenca', '12.MENSAGEM_AtravesNossoChat'] ?
                            ''+mensagem['12.MENSAGEM_SemChaveLicenca'] + ' ' + $scope.telefone + ' ' + mensagem['12.MENSAGEM_AtravesNossoChat']+'' : mensagem
                        );
                    }
                    else if (item.chave != null && item.chave != undefined) {
                        item.currentDate = Utils.longDate();

                        var dados = angular.copy(item);

                        dados.prefixo = "";
                        dados.prefixo = dados.prefixos.toString();

                        $("#worddata").val(JSON.stringify({ 'campos': dados }));
                        $('#WordHandler').submit();

                        $timeout(function () {
                            $scope.tableCartasGs1.page(1);
                            $scope.tableCartasGs1.reload();
                            blockUI.stop();
                        }, 100);
                    }
                }
                else {
                    $("#WordHandler").attr('action', '/WordHandler.ashx');
                    $('#WordHandler').submit();
                    blockUI.stop();
                }
            });
        }
    } ]);
