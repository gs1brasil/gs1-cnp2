app.controller('CodigoSerieUnidadeLogisticaCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$log', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $log, $modal, genericService, Utils, blockUI, $q, $translate) {

    //inicializando abas do form
    $('#tabs-ajuda a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var titulo = $translate.instant('60.LBL_CodigoSerieUnidadeLogisica');
    $scope.titulo = typeof titulo === 'object' || titulo == '60.LBL_CodigoSerieUnidadeLogisica' ? 'Código de Série da Unidade Logística' : titulo;
    $scope.form = {};
    $scope.itemForm = {};
    $scope.initMode = true;
    $scope.showInputs = false;

    $scope.tableCampos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if (!$scope.initMode) {
                var dados = carregaGridCampos($scope.itemForm, function (data) {                    
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.consultaefetuada = true;
                });
            }
        }
    });

    //Popula grid de campos
    function carregaGridCampos(item, callback) {

        genericService.postRequisicao('GepirRouterBO', 'ConsultarWSPartyBySSCC', { campos: item }).then(
            function (result) {
                if (result.data != undefined && result.data != null) {                    
                    callback(result.data.gepirParty == null ? [] : result.data.gepirParty);
                }
            },
            $rootScope.TratarErro
        );
    };

    //Método Inicial da Página
    $scope.onLoad = function () {
        $scope.shouldLoadCampos = true;
        $scope.itemForm = {};
        $scope.consultaefetuada = false;

        $rootScope.verificaAssociadoSelecionado(function (data) {

            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                $rootScope.flagAssociadoSelecionado = true;
            } else {
                $rootScope.flagAssociadoSelecionado = false;
            }
        });
    }

    $scope.onLoad();

    //Limpar campos
    $scope.onClean = function () {
        $scope.onLoad();
    }

    $scope.onSearch = function (item) {
        if (!$scope.form.$valid) {
            var mensagem = $translate.instant('60.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '60.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            $scope.form.submitted = true;
        }
        else {
            if (item.sscc.toString().length < 18) {
                var mensagem = $translate.instant('60.MENSAGEM_SSCC18digitos');
                $scope.alert(typeof mensagem === 'object' || mensagem == '60.MENSAGEM_SSCC18digitos' ? 'O SSCC deve conter 18 dígitos.' : mensagem);
            }
            else if (!Utils.validaDigitoVerificador(item.sscc.toString())) {
                var mensagem = $translate.instant('60.MENSAGEM_DigitoVerificadorInvalido');
                $scope.alert(typeof mensagem === 'object' || mensagem == '60.MENSAGEM_DigitoVerificadorInvalido' ? 'Dígito Verificador inválido.' : mensagem);
            }
            else {
                $scope.initMode = false;
                $scope.itemForm = item;
                $scope.tableCampos.reload();
                $scope.tableCampos.page(1);
            }
        }
    }

} ]);