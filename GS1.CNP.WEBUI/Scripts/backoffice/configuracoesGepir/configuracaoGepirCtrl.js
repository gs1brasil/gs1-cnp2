﻿app.controller('ConfiguracaoGepirCtrl', ['$scope', '$http', '$rootScope', 'ngTableParams', '$window', '$modal', '$log', '$filter', 'genericService', '$location', '$translate', 'Utils',
function ($scope, $http, $rootScope, ngTableParams, $window, $modal, $log, $filter, genericService, $location, $translate, Utils) {

    //inicializando abas do form
    $('#tabs-parametros a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $scope.insertGepirMode = true;
    $scope.listGepir = [];

    $scope.itemForm = {};
    $scope.showFieldsSearch = true;

    var mensagem = $translate.instant('58.LBL_ConfiguracaoGepir');
    $scope.titulo = typeof mensagem === 'object' || mensagem == '58.LBL_ConfiguracaoGepir' ? 'Configurações do Gepir' : mensagem;


    //Busca Empresas Gepir
    function carregaGridPerfil(item, paginaAtual, registroPorPagina, ordenacao, callback) {
        var dados = angular.copy(item);

        genericService.postRequisicao('ConfiguracaoGepirBO', 'ConsultarEmpresas', { where: dados, paginaAtual: paginaAtual, registroPorPagina: registroPorPagina, ordenacao: ordenacao }).then(
            function (result) {
                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
    }

    $scope.tableEmpresaGepir = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
            Codigo: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if ($scope.itemParam != undefined && $scope.itemParam) {
                var ordenacao = params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '') + ' ';
                ordenacao = ordenacao + params.sorting()[params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '')].toString();

                var dados = carregaGridPerfil($scope.itemParam, params.page(), $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'), ordenacao, function (result) {
                    if (result && result.length > 0) {
                        params.total(result[0].totalregistros);
                    }
                    if (result.length == 0) {
                        params.total(0);
                    }

                    $defer.resolve(result);
                });
            }
        }
    });


    $scope.itemParam = {};

    $scope.carregaParametros = function () {
        var parametro = null;
        genericService.postRequisicao('GepirBO', 'ConsultarConfigurationProfile').then(
            function (result) {
                $scope.configurationprofile = result.data;
            },
            $rootScope.TratarErro
        );
    }

    $scope.novaEmpresa = function () {
        $scope.itemEmpresa = {};
        $scope.showFieldsSearch = false;
        $scope.showFieldsEdit = false;
    }

    $scope.cancelarEmpresa = function () {
        $scope.showFieldsSearch = true;
        $scope.showFieldsEdit = false;

        $scope.itemEmpresa = {};
        $scope.itemParam.perfilpadrao = null;

        $scope.codigoassociado = null;
        $scope.nomeassociado = null;
    }

    $scope.limpaEmpresa = function () {
        if ($scope.itemParam.empresaassociada.codigo == undefined) {
            $scope.itemParam.empresaassociada = null;
        }
    }


    $scope.onEdit = function (itemParam) {
        $scope.form.submitted = true;

        if (!$scope.form.$valid) {
            var mensagem = $translate.instant('10.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            return false;
        }

        var dados = {};
        dados.codigoperfilgepir = itemParam.perfilpadrao;
        dados.codigoassociado = itemParam.codigoassociado;

        genericService.postRequisicao('ConfiguracaoGepirBO', 'SalvarEmpresa', { campos: dados }).then(
            function (result) {
                $scope.tableEmpresaGepir.page(1);
                $scope.tableEmpresaGepir.reload();
                $scope.showFieldsSearch = true;
                $scope.showFieldsEdit = false;
                $scope.codigoassociado = null;
                $scope.nomeassociado = null;
                $scope.itemParam.perfilpadrao = null;
                var mensagem = $translate.instant('10.MENSAGEM_RegistroAlteradoSucesso');
                $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
            },
            $rootScope.TratarErro
        );

    };

    $scope.onSearch = function (item) {
        $rootScope.verificaPermissao('ConfiguracaoGepirBO', 'ConsultarEmpresas', function () {
            $scope.tableEmpresaGepir.reload();
        });
    }

    $scope.onAlterGepir = function (item) {
        $scope.showFieldsSearch = false;
        $scope.showFieldsEdit = true;
        $scope.itemParam.perfilpadrao = item.codigoperfilgepir;
        $scope.itemParam.codigoassociado = item.codigo;
        $scope.nomeassociado = item.nome;
    }


    function isNumber(obj) {
        return !isNaN(parseFloat(obj))
    }

    function isNumber(n) {
        return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
    }

    String.prototype.replaceAll = function (find, replace) {
        var str = this;
        return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
    };

    //Carrega associados cadastrados 
    $scope.BuscarAssociadosAutoComplete = function (nome) {
        if (isNumber(nome)) {
            nome = nome.replaceAll('.', '').replaceAll('-', '');
        }

        return genericService.postRequisicao('LoginBO', 'BuscarAssociadosUsuarioAutoComplete', { where: { nome: nome} }).then(
            function (result) {
                var associadosDoUsuario = [];
                angular.forEach(result.data, function (item) {
                    associadosDoUsuario.push({ codigo: item.codigo, nome: item.nome, cpfcnpj: item.cpfcnpj, cad: item.cad,
                        indicadorcnaerestritivo: item.indicadorcnaerestritivo ? item.indicadorcnaerestritivo : ''
                    });
                });
                return associadosDoUsuario;
            }
        );
    }


    $scope.carregaParametros();
} ]);
