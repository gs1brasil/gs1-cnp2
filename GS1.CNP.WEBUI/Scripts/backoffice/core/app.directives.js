﻿var app = angular.module('CNP.Backoffice');

app.directive('currencyInput', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        scope: {},
        link: function (scope, element, attrs, ngModel) {

            $(element).bind('keyup', function (e) {

                var model = ngModel.$modelValue;

                var input = $(element)
                var inputVal = input.val();

                //clearing left side zeros
                while (model.charAt(0) == '0') {
                    model = model.substr(1);
                }

                model = model.replace(/[^\d.\',']/g, '');

                var point = model.indexOf(",");
                if (point >= 0) {
                    model = model.slice(0, point + 3);
                }

                var decimalSplit = model.split(",");
                var intPart = decimalSplit[0];
                var decPart = decimalSplit[1];

                intPart = intPart.replace(/[^\d]/g, '');
                if (intPart.length > 3) {
                    var intDiv = Math.floor(intPart.length / 3);
                    while (intDiv > 0) {
                        var lastComma = intPart.indexOf(".");
                        if (lastComma < 0) {
                            lastComma = intPart.length;
                        }

                        if (lastComma - 3 > 0) {
                            intPart = intPart.splice(lastComma - 3, 0, ".");
                        }
                        intDiv--;
                    }
                }

                if (decPart === undefined) {
                    decPart = "";
                }
                else {
                    decPart = "," + decPart;
                }
                var res = intPart + decPart;

                $(element).val(res);

                scope.$apply(function () {
                    ngModel.$setViewValue(res);
                });

            });

        }
    };
});

app.directive('modalLogin', function () {
    return {
        restrict: 'E',
        templateUrl: function (elem, attrs) {
            return '/views/backoffice/modal.html';
        },
        transclude: true,
        link: function ($scope, $element, $attrs) {
            $scope.dismiss = function () {
                $element.modal('hide');
            };
        },
        controller: function ($scope, $element, $rootScope, $timeout, $location, genericService, $cookies, $window, $route, $injector, vcRecaptchaService) {
            $('#modal1').on('hidden.bs.modal', function (e) {
                $scope.usuario = {
                    login: null,
                    senha: null,
                    cpfcnpj: null
                }
                $scope.exibeCPFCNPJ = false;

                if ($window.location.pathname.split('/')[1] != 'produto' && $window.location.pathname.split('/')[1] != 'fornecedor' && $window.location.pathname.split('/')[1] != 'fornecedor' && $rootScope.voltarPosModal == 1) {
                    $window.location.href = '/home';
                }
            });

            $('#modal1').on('show.bs.modal', function (e) {
                $('.btnVoltar').click();
            });

            $rootScope.usuarioLogado = null;

            $scope.usuario = {
                login: null,
                senha: null,
                cpfcnpj: null
            }

            $scope.showCadastro = function () {
                $rootScope.voltarPosModal = 0;
                $('#modal1').modal('hide');
                $('#modal2').modal('toggle');
            }

            var blockUI = $injector.get('blockUI');

            $scope.criaRecaptcha = function () {
                if (typeof (Recaptcha) != 'undefined') {
                    if (Recaptcha.widget == null) {
                        Recaptcha.create(recaptchaPublicKey, "recaptcha",
                            {
                                theme: "white",
                                callback: Recaptcha.focus_response_field
                            }
                        );
                    } else {
                        Recaptcha.reload();
                    }
                    return true;
                } else {
                    return false;
                }
            };

            //TODO - comentar
            //$scope.exibeCAPTCHA = false;
            //TODO - remover comentario
            var exibeCAPTCHACookie = $cookies.exibeCAPTCHACookie;
            //var exibeCAPTCHACookie = false;

            if (exibeCAPTCHACookie == "true") {
                $scope.exibeCAPTCHA = true;
                //vcRecaptchaService.reload();
            }

            $scope.exibeCPFCNPJ = false;

            $scope.onLogin = function () {
            blockUI.start();

            $scope.usuario.challenge = null;
            $scope.usuario.response = null;

            if ($scope.usuario.login !== '' && $scope.usuario.senha !== '' && $scope.usuario.login != null && $scope.usuario.senha != null) {
                if (($scope.exibeCAPTCHA && (vcRecaptchaService.getResponse() != '' && vcRecaptchaService.getResponse() != null) || !($scope.exibeCAPTCHA))) {
                    if ($scope.exibeCAPTCHA) {
                        //$scope.usuario.challenge = Recaptcha.get_challenge();
                        $scope.usuario.response = vcRecaptchaService.getResponse();
                    }

                    var dados = { where: $scope.usuario };
                    genericService.postRequisicao('LoginBO', 'Logar', dados).then(function (result) {
                        if (result.data) {
                            if (result.data.status_login) {
                                $scope.exibeCAPTCHA = false;
                                $cookies.exibeCAPTCHACookie = "";
                                $rootScope.usuarioLogado = true;

                                if (history.pushState) {
                                    $window.location.href = '/backoffice/home';
                                } else {
                                    $window.location.href = '/b#/backoffice/home';
                                }
                            } else {

                                if (result.data.primeiro_acesso) {
                                    $scope.exibeCPFCNPJ = true;
                                    $scope.alert(result.data.mensagem);
                                } else {
                                    $scope.exibeCPFCNPJ = false;
                                    //TODO - Melhorar mensagem
                                    var mensagem = result.data.mensagem;
                                    if (result.data.quantidade_tentativa > 0) {
                                        $scope.exibeCAPTCHA = true;
                                        $cookies.exibeCAPTCHACookie = "true";
                                        $scope.usuario.senha = "";

                                        vcRecaptchaService.reload();
                                        /*if (!$scope.criaRecaptcha()) {
                                            $cookies.exibeCAPTCHACookie = "false";
                                            $scope.usuario.senha = "";
                                            mensagem = 'Erro ao processar Código de Validação.';
                                        }*/
                                    }
                                    $scope.alert(mensagem);
                                }
                            }
                        } else {
                            //TODO - Melhorar mensagem
                            $scope.alert('Erro ao realizar login.');
                        }
                    },
                    function (result, status, headers, config) {
                        if (result.status != 419 && result.status != 420)
                            $rootScope.alert(result);
                    });
                } else {
                
                        if (typeof (vcRecaptchaService) != 'undefined') {
                            //TODO - Melhorar mensagem
                            vcRecaptchaService.reload();
                            $cookies.exibeCAPTCHACookie = "true";
                            $scope.usuario.senha = "";
                            $scope.alert('Informe o Código de Validação.');
                        } else {
                            //TODO - Melhorar mensagem                        
                            $cookies.exibeCAPTCHACookie = "false";
                            $scope.usuario.senha = "";
                            $scope.alert('Erro ao processar Código de Validação.');
                        }
                    }
                } else {
                    $scope.alert('Informe um Email e Senha Válidos.');
                }
                blockUI.stop();
            }

            $scope.enviarEmail = function () {
                if ($scope.usuario.email != "" && $scope.usuario.email != undefined) {
                    if ($scope.formRecuperar.$valid == false) {
                        $scope.alert('Endereço de e-mail incorreto.');
                    }
                    else {
                        blockUI.start();
                        var dados = { campos: { email: $scope.usuario.email} };

                        genericService.postRequisicao('LoginBO', 'Recupera', dados).then(function (result) {
                            blockUI.stop();
                            if (result.data == 0) {
                                $rootScope.alert('Endereço de e-mail não pertence a um usuário ativo.');
                            }
                            else if (result.data == 2) {
                                $rootScope.alert('Endereço de e-mail não pertence a um usuário ativo.');
                            }
                            else if (result.data == 3) {
                                $rootScope.alert('Erro ao enviar e-mail.');
                            }
                            else if (result.data == 4) {
                                var dados = { where: 'bo.nu.telefone' };

                                genericService.postRequisicao('LoginBO', 'CarregarParametrosLogin', dados).then(function (result) {
                                    if (result.data.length > 0)
                                        $scope.telefone = result.data[0].valor;

                                    $rootScope.alert('Acesso negado. Por favor, entre em contato com o atendimento GS1 Brasil. Tel. ' + $scope.telefone + ' ou através do nosso site.');
                                });
                            }
                            else if (result.data == 5) {
                                var dados = { where: 'bo.nu.telefone' };

                                genericService.postRequisicao('LoginBO', 'CarregarParametrosLogin', dados).then(function (result) {
                                    if (result.data.length > 0)
                                        $scope.telefone = result.data[0].valor;

                                    $scope.alert("Operação não permitida. Por favor, entre em contato com a GS1 Brasil pelo tel. " + $scope.telefone + ".");
                                });

                            }
                            else {
                                $rootScope.alert('Verifique seu e-mail. Em breve você receberá a nova senha. Caso não receba o e-mail, entrar em contato com a GS1 Brasil.', function () {
                                    $scope.viewLogin = true;
                                    $scope.viewForgot = false;
                                }, 'Alerta');

                                //$scope.alert('Verifique seu e-mail. Em breve você receberá a nova senha.');
                            }
                        },
                        function (result, status, headers, config) {
                            blockUI.stop();
                            if (result.status != 419 && result.status != 420)
                                $rootScope.alert(result);
                        });

                        //loginService.enviarEmail($scope.usuario, function (data) {
                        //    if (data == 0) {
                        //        $scope.alert('Endereço de e-mail não pertence a um usuário ativo.');
                        //    }
                        //    else if (data == 2) {
                        //        $scope.alert('Endereço de e-mail não pertence a um usuário ativo.');
                        //    }
                        //    else if (data == 3) {
                        //        $scope.alert('Erro ao enviar e-mail.');
                        //    }
                        //    else if (data == 4) {
                        //        //TODO - parametrizar email
                        //        $scope.alert('Acesso negado. Por favor, entre em contato com o atendimento GS1 Brasil. Tel. (11) 3068-6229 ou através do nosso site.');
                        //    }
                        //    else {
                        //        $scope.alert('Verifique seu e-mail. Em breve você receberá a nova senha.');
                        //    }
                        //});
                    }
                }
                else {
                    $scope.alert('Informe um email para prosseguir.');
                }
            }

            $scope.onClean = function () {
                $scope.usuario.email = '';
            }

            //TODO - melhorar tratameto
            //$rootScope.alert = function (data, sucessCallback, title) {
            //    var message = "";

            // if (data.InnerException != null) {
            //     if (data.InnerException.Message != null) {
            //         message = data.InnerException.Message;
            //     } else {
            //         message = data;
            //     }
            // } else {
            //     if (data.Message != null) {
            //         message = data.Message;
            //     } else {
            //         if (data != null) {
            //             message = data;
            //         } else {
            //             message = "Erro ao processar requisição";
            //         }
            //     }
            // }

            //var modalInstance = $modal.open({
            //    template: ' <div class="modal-header"><h3>{{title}}</h3></div><div class="modal-body"><p ng-bind-html="message"></p><!--{{message}}--></div><div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button></div>',
            //    controller: function ($scope, $modalInstance) {
            //        $scope.message = message;
            //        $scope.title = title || "Alerta";
            //        $scope.ok = function () {
            //            $modalInstance.close();
            //        };
            //    }
            //});

            // //                modalInstance.result.then(sucessCallback);
            //            };
        }
    };
});

app.directive("navigation", ['$compile',
    function ($compile) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                menu: '='
            },
            templateUrl: "/views/backoffice/menu.html",
            controller: function ($scope) {

                $scope.$parent.geraMenu();

                $scope.selectedIndex = 0; // Whatever the default selected index is, use -1 for no selection

                $scope.itemClicked = function ($index) {
                    $scope.selectedIndex = $index;
                };

                $scope.subItemClicked = function ($index, $subitem) {
                    $scope.selectedIndex = $index + "." + $subitem;
                };

                $scope.hideAllSubmenus = function(menu, atual) {
                    
                    for(var i = 0; i < menu.length; i++) {
                        if(menu[i] != atual)
                            menu[i].show = false;
                    }

                    atual.submenus != null ? (atual.show = !atual.show) : false
                }
            },
            compile: function (el) {
                var contents = el.contents().remove();
                return function (scope, el) {
                    $compile(contents)(scope, function (clone) {
                        el.append(clone);
                    });
                };
            }
        };
    }
]);

app.directive("navigationAtalho", ['$compile',
    function ($compile) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                atalho: '='
            },
            templateUrl: "/views/backoffice/atalho.html",
            controller: function ($scope) {
                $scope.$parent.geraAtalho();

                $scope.selectedIndex = 0; // Whatever the default selected index is, use -1 for no selection

                $scope.itemClicked = function ($index) {
                    $scope.selectedIndex = $index;
                };

                $scope.subItemClicked = function ($index, $subitem) {
                    $scope.selectedIndex = $index + "." + $subitem;
                };
            },
            compile: function (el) {
                var contents = el.contents().remove();
                return function (scope, el) {
                    $compile(contents)(scope, function (clone) {
                        el.append(clone);
                    });
                };
            }
        };
    }
]);


app.directive('modalAjuda', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        link: function (scope, elm, attrs) {

            var html =
            '<a href="/##modal_ajuda" target="_self" data-toggle="modal" title="Ajuda"> ' +
            '    <i class="icon-question-sign" style="font-size: 20px; text-decoration: none;"></i>' +
            '</a>' +
            '    <div id="modal_ajuda" class="modal fade">' +
	        '        <div class="modal-dialog text-left">' +
		    '            <div class="modal-content">' +
			'                <div class="modal-header">' +
			'                    <h3 class="text-center" style="margin-top:0; padding-top:20px;">' +
			'	                    <div ng-include="getContentUrl()"></div>' +
			'	                    <button type="button" class="close" data-dismiss="modal" ng-click="dismiss()">' +
			'		                    <span aria-hidden="true">×</span>' +
			'		                    <span class="sr-only">Close</span>' +
			'	                    </button>' +
			'                    </h3>' +
			'                </div>' +
			'                <div class="main-container">' +
			'                    <div class="main-content">' +
			'	                    <form name="form" id="form" novalidate class="form" role="form">' +
			'		                    <div class="col-md-12">' +
			'			                    <div class="clearfix col-xs-12">' +
			'			                    </div>' +
			'		                    </div>' +
			'	                    </form>' +
			'	                    <br /><br />' +
			'                    </div>' +
			'                </div><!-- /.main-container -->' +
		    '            </div>' +
	        '        </div>' +
            '    </div>';

            scope.dismiss = function () {
                $element.modal('hide');
            };

            scope.getContentUrl = function () {
                return '/views/backoffice/ajuda/' + attrs.urlajuda + '.html';
            }

            var children = $compile(html)(scope);
            elm.append(children);
        }
    };
});

app.directive('onEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.onEnter);
                });

                event.preventDefault();
            }
        });
    };
});

//http://www.eyecon.ro/bootstrap-datepicker/
app.directive('sbxDatepicker', function () {
    return {
        restrict: 'A',
        require: '^ngModel',
        scope: {
            sbxOptions: '@'
        },
        link: function (scope, elem, attrs, ctrl) {

            var defaults = {
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: "pt-BR"
            };

            var options = angular.extend(defaults, attrs.sbxOptions ? JSON.parse(attrs.sbxOptions) : {});

            var $elem = $(elem);

            $elem.datepicker(options);

            $elem.on("changeDate", function (e) {
                scope.$apply(function () {
                    ctrl.$setViewValue($elem.val());
                });
            });

            $elem.next(".input-group-addon").on("click", function () {
                $elem.datepicker("show");
            });
        }
    }
});

app.directive('capitalize', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var capitalize = function (inputValue) {
                if (inputValue) {
                    var capitalized = inputValue.toUpperCase();
                    if (capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                }
                return null;
            }
            modelCtrl.$parsers.push(capitalize);
            capitalize(scope[attrs.ngModel]); // capitalize initial value
        }
    };
});

/*
app.directive('watchFetch', function () {
return {
restrict: 'E',
require: 'ngModel',
transclude: true,
scope: {
ids: '@'
},
controller: function ($scope, db, bo) {
var ids = $scope.ids.split(' ');
            
angular.forEach(ids, function (el, idx) {
                
var reg = /^(.+)\[(.+)\]$/;
var method; 
                
if(reg.test(el)) {
method = el.replace(reg, "$2");
el = el.replace(reg, "$1");
}                
                
$scope.$parent.$watch(el, function(newValue, oldValue) {
if (newValue != undefined) {
var model = el.substring(el.indexOf('.')+4);
                                    
if(method != undefined) {
var fncMethod = method.split('.').reduce(function (obj,i) {
return obj[i];
}, bo);
                            
fncMethod({"id": newValue}).then(function(res){
if (res.data != undefined) {
$scope.$parent[el.split('.')[0]][model] = res.data;
}
});

} else {
db.find(model, {"example": {"id": newValue}}).then(function (res) {
if (res.data != undefined && res.data.length > 0) {
$scope.$parent[el.split('.')[0]][model] = res.data[0];
}
});    
}
}
});
});
}
};
});
*/
app.directive('importContainer', function () {
    return {
        restrict: 'E',
        transclude: true,
        templateUrl: 'views/directives/import_container.html'
    }
});

app.directive('fileDownload', function ($compile) {
    var fd = {
        restrict: 'E',
        link: function (scope, iElement, iAttrs) {
            scope.$on("downloadFile", function (e, url) {
                var iFrame = iElement.find("iframe");
                if (!(iFrame && iFrame.length > 0)) {
                    iFrame = $("<iframe style='position:fixed;display:none;top:-1px;left:-1px;'/>");
                    iElement.append(iFrame);
                }

                iFrame.attr("src", url);
                iFrame[0].onload = function () {
                    var content = iFrame[0].contentDocument.body.innerHTML;
                    if (content[0] == '{') {
                        var error = JSON.parse(content);
                        scope.warn(error.message, "Aviso");
                    }
                };
            });
        }
    };
    return fd;
});

app.directive('resize', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return { 'h': w.height(), 'w': w.width() };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;

            scope.styleHeight = function (diff) {
                diff = diff || 130;
                return {
                    'height': (window.innerWidth <= 992 ? "100%" : (newValue.h - diff) + 'px')
                };
            };

            scope.style = function () {
                return {
                    'height': (window.innerWidth <= 992 ? "100%" : (newValue.h - diff) + 'px'),
                    'width': (newValue.w - 130) + 'px'
                };
            };

        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
});

//app.directive('resizeSidebar', function ($window) {
//    return function (scope, element) {
//        var w = angular.element($window);
//        scope.getWindowDimensions = function () {
//            return { 'h': w.height() };
//        };
//        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
//            $("#sidebar").css("height", window.innerHeight - 50 + "px")
//            $("#sidebar").parent(".slimScrollDiv").css("height", window.innerHeight - 50 + "px");
//        }, true);

//        w.bind('resize', function () {
//            scope.$apply();
//        });
//    }
//});

app.directive('blurOnEnter', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attr, ngModelCtrl) {
            elm.bind('keydown', function (e) {
                if (e.which == 13) e.target.blur();
            });
        }
    };
});

app.directive('moDateInput', function ($window) {
    return {
        require: '^ngModel',
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            var moment = $window.moment;
            var dateFormat = attrs.moMediumDate;
            attrs.$observe('moDateInput', function (newValue) {
                if (dateFormat == newValue || !ctrl.$modelValue) return;
                dateFormat = newValue;
                ctrl.$modelValue = new Date(ctrl.$setViewValue);
            });

            ctrl.$formatters.unshift(function (modelValue) {
                scope = scope;
                if (!dateFormat || !modelValue) return "";
                var retVal = moment(modelValue).format(dateFormat);
                return retVal;
            });

            ctrl.$parsers.unshift(function (viewValue) {
                scope = scope;
                var date = moment(viewValue, dateFormat);
                return (date && date.isValid() && date.year() > 1950) ? date.toDate() : "";
            });
        }
    };
});

app.directive('datepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    dateFormat: 'dd/mm/yy',
                    onSelect: function (date) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    }
                });
            });
        }
    }
});

app.directive('sbxMaxlength', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            var maxlength = Number(attrs.sbxMaxlength);
            function fromUser(text) {
                if (text != undefined && text.length > maxlength) {
                    var transformedInput = text.substring(0, maxlength);
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                    return transformedInput;
                }
                return text;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {

            if (!ctrl) return;

            var format = {
                    prefix: '',
                    centsSeparator: '.',
                    thousandsSeparator: '',
            };
            if(attrs.negative !== undefined){
                format.allowNegative = true;
            }

            var decimals = attrs.decimals ? parseInt(attrs.decimals, 10) : 2;
            format.centsLimit = decimals;

            ctrl.$parsers.unshift(function (value) {
                elem.priceFormat(format);

                return elem[0].value;
            });

            ctrl.$formatters.unshift(function (value) {

                value = parseFloat(value) || 0;
                elem[0].value = value.toFixed(decimals);
                elem.priceFormat(format);
                return elem[0].value;
            })

        }
    };
}]);

/**
 * @ngdoc directive
 * @name validityBindFix
 * @module cxForm
 *
 * @description
 * Prevents ngModelController from nulling the model value when it's set invalid by some rule.
 * Works in both directions, making sure an invalid model value is copied into the view value and making sure an invalid
 * model value is copied into the model. **Warning:** totally bypasses formatters/parsers when invalid, but probably good 
 * enough to use in most cases, like maxlength or pattern.
 *
 * See angular issue: https://github.com/angular/angular.js/issues/1412
 *
 * Inspired by the strategy provided by Emil van Galen here:
 * http://blog.jdriven.com/2013/09/how-angularjs-directives-renders-model-value-and-parses-user-input/
 *
 * @restrict A
 * @scope
 *
 * @param {object} ngModel Required `ng-model` value. If not present in the same element an error occurs.
 */
app.directive('validityBindFix', function () {
    'use strict';

    return {
        require: '?ngModel',
        priority: 9999,
        restrict: 'A',
        link: function ($scope, $element, $attrs, ngModelController) {
            ngModelController.$formatters.unshift(function (value) {
                if (ngModelController.$invalid && angular.isUndefined(value)) {
                    return ngModelController.$modelValue;
                } else {
                    return value;
                }
            });
            ngModelController.$parsers.push(function (value) {
                if (ngModelController.$invalid && angular.isUndefined(value)) {
                    return ngModelController.$viewValue;
                } else {
                    if (/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(ngModelController.$viewValue)){
                    return value;
                    }
                }
            });
        }
    };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive("dynamicName", function ($compile) {
    return {
        restrict: "A",
        terminal: true,
        priority: 1000,
        link: function (scope, element, attrs) {
            element.attr('name', scope.$eval(attrs.dynamicName));
            element.removeAttr("dynamic-name");
            $compile(element)(scope);
        }
    };
});

app.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);

app.directive('integer', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function(text){
                var transformedInput = text.replace(/[^0-9]/g, '');
                if(transformedInput !== text) {
                    elm.val('');
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

app.directive('gtin', function () {
    var maxlengthGtin = {
        "1": 8
        , "2": 12
        , "3": 13
        , "4": 14
    };

    var validateGtin = function(gtin, tipogtin) {

        if (tipogtin && gtin && gtin.length !== maxlengthGtin[tipogtin]) {
            return false;
        } else if (!tipogtin && gtin) {
            var valid = false;
            angular.forEach(maxlengthGtin, function (value) {
                if (gtin.length === value) {
                    valid = true;
                }
            });

            if (!valid) {
                return false;
            }
        }

        return true;
    };
    return {
        restrict: "A",
        scope: {
            tipoGtin: "="
        },
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$validators.gtin = function(modelValue, viewValue) {
                return validateGtin(modelValue, scope.tipoGtin);
            };
        }
    };
});

app.directive('ajuda', function ($compile, $translate, $filter) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            codigo: '=',
            marginRight: '=',
            marginLeft: '=',
            marginTop: '=',
            marginBottom: '=',
            tooltipAjuda: '='
        },
        link: function ($scope, $elm, $attrs) {
            if($scope.showAjuda) {

                var html =  '<a href="/backoffice/detalhesAjuda/' + $scope.codigo + '" ' +
                                'ng-style="showMarginRight ? { \'margin-right\':\'' + $scope.marginRight + '\' } : {\'margin-right\' : \'0px\'}"' +
                                '          showMarginLeft ? { \'margin-left\':\'' + $scope.marginLeft + '\' } : {\'margin-left\' : \'0px\'}"' +
                                '          showMarginTop ? { \'margin-top\':\'' + $scope.marginTop + '\' } : {\'margin-top\' : \'0px\'}"' +
                                '          showMarginBottom ? { \'margin-bottom\':\'' + $scope.marginBottom + '\' } : {\'margin-bottom\' : \'0px\'}" ' +
                                'class="btn btn-warning btn-xs" name="ajuda" title="' + $scope.tooltip + ' " target="_blank">' +
                                '<i class="icon-question-sign"></i>' +
                            '</a>';

                var children = $compile(html)($scope);
                $elm.append(children);
            }
        },
        controller: function ($scope, $rootScope, $translate) {
            
            $scope.showMarginRight = $scope.marginRight != undefined ? true : false;
            $scope.showMarginLeft = $scope.marginLeft != undefined ? true : false;
            $scope.showMarginTop = $scope.marginTop != undefined ? true : false;
            $scope.showMarginBottom = $scope.marginBottom != undefined ? true : false;

            $scope.tooltip = $translate.instant($scope.tooltipAjuda);
            if(typeof $scope.tooltip === 'object')
                $scope.tooltip = 'Ajuda';

            if($rootScope.codigosAjuda != undefined) {
                var codigoEncontrado = $.grep($rootScope.codigosAjuda, function (obj) {
                    return obj === $scope.codigo;
                });

                if(codigoEncontrado != undefined && codigoEncontrado.length > 0)
                    $scope.showAjuda = true;
                else
                    $scope.showAjuda = false;
            }
        }
    };
});

app.directive('popupAjuda', function ($compile) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            codigo: '=',
            marginRight: '=',
            marginLeft: '=',
            marginTop: '=',
            marginBottom: '=',
            tooltipAjuda: '='
        },
        link: function ($scope, $elm, $attrs) {
            if($scope.showAjuda) {
                var html =  '<a style="float:right;text-decoration: none;" name="ajuda" href="/backoffice/detalhesAjuda/' + $scope.codigo + '" target="_blank" ' +
                                'ng-style="showMarginRight ? { \'margin-right\':\'' + $scope.marginRight + '\' } : {\'margin-right\' : \'0px\'}"' +
                                '          showMarginLeft ? { \'margin-left\':\'' + $scope.marginLeft + '\' } : {\'margin-left\' : \'0px\'}"' +
                                '          showMarginTop ? { \'margin-top\':\'' + $scope.marginTop + '\' } : {\'margin-top\' : \'0px\'}"' +
                                '          showMarginBottom ? { \'margin-bottom\':\'' + $scope.marginBottom + '\' } : {\'margin-bottom\' : \'0px\'}">' +
                                '<i class="icon-question-sign" title="' + $scope.tooltip + '" ></i>' +
                            '</a>';

                var children = $compile(html)($scope);
                $elm.append(children);
            }
        },
        controller: function ($scope, $rootScope, $translate) {
            
            $scope.showMarginRight = $scope.marginRight != undefined ? true : false;
            $scope.showMarginLeft = $scope.marginLeft != undefined ? true : false;
            $scope.showMarginTop = $scope.marginTop != undefined ? true : false;
            $scope.showMarginBottom = $scope.marginBottom != undefined ? true : false;


            $scope.tooltip = $translate.instant($scope.tooltipAjuda);
            if(typeof $scope.tooltip === 'object')
                $scope.tooltip = 'Ajuda*';

            if($rootScope.codigosAjuda != undefined) {
                var codigoEncontrado = $.grep($rootScope.codigosAjuda, function (obj) {
                    return obj === $scope.codigo;
                });

                if(codigoEncontrado != undefined && codigoEncontrado.length > 0)
                    $scope.showAjuda = true;
                else
                    $scope.showAjuda = false;
            }
        }
    };
});


app.directive('idioma', function ($compile) {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            codigoformulario: '=',
            nomecampo: '='
        },
        link: function ($scope, $elm, $attrs) {
            if($scope.$root.codigoidioma != 1){
                //$elm.value="Teste";
                $elm.html("Teste");
            }

        },
        controller: function ($scope, $rootScope) {
            
           
        }
    };
});

app.directive('tableHeader', function ($compile) {
    return {
        restrict: 'A',
        scope: {
          tabela: '=',
          campo: '=',
          colunaTraduzir: '=',
          texto: '='
        },
        link: function ($scope, $elm, $attrs) {
            
            var html =  '<span translate=\'' + $scope.colunaTraduzir + '\'> ' + $scope.texto + ' &nbsp;&nbsp; ' +
                                '<i class="text-center icon-sort" ng-class="{ ' +
                                    '\'icon-sort-up\': ' + $scope.tabela + '.isSortBy(\'' + $scope.campo + '\', \'asc\'),' +
                                    '\'icon-sort-down\': ' + $scope.tabela + '.isSortBy(\'' + $scope.campo + '\', \'desc\')}">' +
                                '</i>' +
                        '</span> ';

            var children = $compile(html)($scope);
            $elm.append(children);
        },
        controller: function ($scope, $rootScope) {

        }
    };
});


app.directive('toggle', function(){
  return {
    restrict: 'A',
    link: function(scope, element, attrs){
      if (attrs.toggle=="tooltip"){
        $(element).tooltip();
      }
      if (attrs.toggle=="popover"){
        $(element).popover();
      }
    }
  };
});


app.directive('espacoBanner', function ($compile) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            codigo: "@codigo"
        },
        template: '<span class="banner" ng-show="exibebanner" style="margin-top: 15px; margin-bottom: 15px;">' +
                      '<a ng-if="link != undefined && link != null && link != \'\'" href="{{link}}" target="_blank">' +
                      '     <img class="img-banner" ng-src="{{nomeimagem}}" width="100%" height="{{altura}}" alt="{{nomebanner}}" border="0">' +
                      '</a>' +
                      '<img class="img-banner" ng-if="link == undefined || link == null || link == \'\'" ng-src="{{nomeimagem}}" width="100%" height="{{altura}}" alt="{{nomebanner}}" border="0">' +
                  '</span>',
        controller: function ($scope, $attrs, $http, $rootScope, $location, $interval) {
            $scope.codigobanner = $attrs.codigo;

            var dados = { where: { codigo: $scope.codigo } };

            $http.post('/Requisicao.ashx/DashboardBO/BannersEspacoBanner', dados).success(function (data) {
                if (data.length > 0) {
                    var time = 0;

                    var alteraBanner = function() { 
                        if($location.absUrl().split('/')[$location.absUrl().split('/').length - 1] == "home") {                            
                            $scope.nomeimagem = data[time].nomeimagem;
                            $scope.altura = data[time].altura;
                            $scope.largura = data[time].largura;
                            $scope.nomebanner = data[time].nomebanner;
                            $scope.link = data[time].link;
                            $scope.exibebanner = true;

                            if(time < data.length - 1) time++;
                            else time = 0;
                        }
                    }
                    alteraBanner();

                    $interval(alteraBanner, 10000);

                } else {
                    $scope.exibebanner = false;
                    return false;
                }
            }).error($rootScope.TratarErro);

        }

    };
});

app.directive('cnpInformacoesNutricionais', function(){
  return {
    restrict: 'E'
    , templateUrl: function() {return '/views/backoffice/directives/informacoesNutricionais.html';}
    , scope: {
        item: '='
    }
    , link: function(scope, element, attrs) {
        
    }
    , controller: ['$rootScope', '$scope', '$translate', 'ngTableParams', 'genericService'
        , function ($rootScope, $scope, $translate, ngTableParams, genericService) {
        
        $scope.newInformacaoNutricional = function newInformacaoNutricional() {
            $scope.detail = {};
            $scope.showCadastroItemInformacaoNutricional = true;
        };

        $scope.cancelarNutrientDetail = function cancelarNutrientDetail() {
            $scope.detail = {};
            $scope.showCadastroItemInformacaoNutricional = false;
        };

        $scope.selectNutrientDetail = function selectNutrientDetail(item) {
            $scope.detail = angular.copy(item);
            $scope.detail.editar = true;
            $scope.showCadastroItemInformacaoNutricional = true;
        };

        $scope.removeNutrientDetail = function removeNutrientDetail(item) {
            var mensagem = $translate.instant('15.MENSAGEM_DesejaRemover');

            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {
                $scope.item.produtoNutrientDetail = $scope.item.produtoNutrientDetail.filter(function(n) {
                    return n.codigo !== item.codigo;
                });
            }, function () { }, 'Confirmação');
        };

        $scope.salvarNutrientDetail = function salvarNutrientDetail(item) {
            item = angular.copy(item);

            if (validarNutrientDetail(item)) {

                //Editar agência
                if (item.editar) {
                    editNutrientDetail(item);
                }
                else { //Add agência
                    addNutrientDetail(item);
                }

                $scope.detail = {};
                $scope.showCadastroItemInformacaoNutricional = false;
            }
        };

        $scope.changeNutrient = function changeNutrient() {
            if ($scope.detail.nutrientTypeCode) {
                var sigla = $scope.detail.nutrientTypeCode.sigla;
                
                angular.forEach($scope.nutrients, function(value) {
                    if (value.sigla === sigla && value.tipounidademedida !== $scope.tipounidademedida) {
                        loadUnidadesMedida(value.tipounidademedida);
                    }
                });
            }
        };

        var validarNutrientDetail = function validarNutrientDetail(item) {
            return validarFormNutrientDetail(item) 
                && validarNutrientDetailUnique(item);
        };

        var validarFormNutrientDetail = function validarFormNutrientDetail(item) {
            if (! (item 
                && item.nutrientTypeCode
                && item.quantityContained
                && item.measurementPrecisionCode
                && item.dailyValueIntakePercent)) {
                
                $rootScope.alert('15.MENSAGEM_CamposObrigatorios', null, null, true, 'Campos obrigatórios não preenchidos ou inválidos.');
                return false;
            }

            return true;
        };

        var validarNutrientDetailUnique = function validarNutrientDetailUnique(item) {
            var unique = true;
            angular.forEach($scope.item.produtoNutrientDetail, function (val) {
                if (val.nutrientTypeCode.codigo === item.nutrientTypeCode.codigo && val.codigo !== item.codigo) {
                    unique = false;
                }
            });

            if (!unique) {
                $rootScope.alert('15.MENSAGEM_NutrientDetailExiste', null, null, true, 'Este item já foi informado.');
                return false;
            }
            
            return true;
        };

        var addNutrientDetail = function addNutrientDetail(item) {
            var maior = verificaMaiorCodigo($scope.item.produtoNutrientDetail);
            item.codigo = maior + 1;
            item.novo = 1;

            if($scope.item.produtoNutrientDetail == undefined || $scope.item.produtoNutrientDetail == null){
                $scope.item.produtoNutrientDetail = []
            }

            $scope.item.produtoNutrientDetail.push(item);
        }

        var editNutrientDetail = function editNutrientDetail(item) {
            angular.forEach($scope.item.produtoNutrientDetail, function (val, key) {
                if (val.codigo === item.codigo) {
                    angular.extend(val, item);
                }
            });
        }

        var verificaMaiorCodigo = function verificaMaiorCodigo(item) {
            var maior = 0;

            //Pegar o maior CODIGO da lista
            angular.forEach(item, function (val) {
                if (val.codigo > maior) {
                    maior = val.codigo;
                }
            });

            return maior;
        }
        
        var loadUnidadesMedida = function loadUnidadesMedida(tipo) {
            $scope.tipounidademedida = tipo;
            $scope.measurements = [];
            var where = {'tipo': tipo};
            genericService.postRequisicao('UnidadesMedidaBO', 'BuscarMedidasPorTipo', {'where': where}).then(
                function (result) {
                    if (result.data) {
                        $scope.measurements = result.data;
                    }
                },
                $rootScope.TratarErro
            );
        }

        var initListas = function initListas() {
            
            $scope.nutrients = [];
            $scope.tipounidademedida = null;
            genericService.postRequisicao('NutrienteBO', 'BuscarNutrientesAtivos', {}).then(
                function (result) {
                    if (result.data) {
                        $scope.nutrients = result.data;
                    }
                },
                $rootScope.TratarErro
            );
        };

        // Init
        (function init() {
            initListas();
            $scope.item.produtoNutrientDetail = $scope.item.produtoNutrientDetail || [];
        })();
        
    }]
  };
});

app.directive('cnpAlergenos', function(){
  return {
    restrict: 'E'
    , templateUrl: function() {return '/views/backoffice/directives/alergenos.html';}
    , scope: {
        list: '='
    }
    , link: function(scope, element, attrs) {
        
    }
    , controller: ['$rootScope', '$scope', '$translate', 'ngTableParams', 'genericService'
        , function ($rootScope, $scope, $translate, ngTableParams, genericService) {
        
        var _DEFAULT_DETAIL = {
            allergenRelatedInformation: {
                allergen: {}
            }
            
        };

        $scope.novo = function novo() {
            $scope.detail = angular.copy(_DEFAULT_DETAIL);
            $scope.showCadastro = true;
        };

        $scope.cancelar = function cancelar() {
            $scope.detail = angular.copy(_DEFAULT_DETAIL);
            $scope.showCadastro = false;
        };

        $scope.selecionar = function selecionar(item) {
            $scope.detail = angular.copy(item);
            $scope.detail.editar = true;
            $scope.showCadastro = true;
        };

        $scope.remover = function remover(item) {
            var mensagem = $translate.instant('15.MENSAGEM_DesejaRemover');

            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {
                $scope.list = $scope.list.filter(function(n) {
                    return n.codigo !== item.codigo;
                });
            }, function () { }, 'Confirmação');
        };

        $scope.salvar = function salvar(item) {
            item = angular.copy(item);

            if (validar(item)) {

                if (item.editar) {
                    edit(item);
                }
                else {
                    add(item);
                }

                $scope.detail = angular.copy(_DEFAULT_DETAIL);
                $scope.showCadastro = false;
            }
        };

        var validar = function validar(item) {
            return validarForm(item) 
                && validarUnique(item);
        };

        var validarForm = function validarForm(item) {
            if (! (item 
                && item.allergenRelatedInformation
                && item.allergenRelatedInformation.allergen
                && item.allergenRelatedInformation.allergen.codigo
                && item.levelOfContainmentCode
                && item.levelOfContainmentCode.codigo)) {
                
                $rootScope.alert('15.MENSAGEM_CamposObrigatorios', null, null, true, 'Campos obrigatórios não preenchidos ou inválidos.');
                return false;
            }

            return true;
        };

        var validarUnique = function validarUnique(item) {
            var unique = true;
            angular.forEach($scope.list, function (val) {
                if (val.allergenRelatedInformation.allergen.codigo === item.allergenRelatedInformation.allergen.codigo && val.codigo !== item.codigo) {
                    unique = false;
                }
            });

            if (!unique) {
                $rootScope.alert('15.MENSAGEM_AllergenExiste', null, null, true, 'Este item já foi informado.');
                return false;
            }
            
            return true;
        };

        var add = function add(item) {
            item.codigo = $rootScope.verificaMaiorCodigo($scope.list) + 1;
            $scope.list.push(item);
        }

        var edit = function edit(item) {
            angular.forEach($scope.list, function (val, key) {
                if (val.codigo === item.codigo) {
                    angular.extend(val, item);
                }
            });
        }

        var initListas = function initListas() {
            
            $scope.alergenTypeCodes = [];
            genericService.postRequisicao('AlergenoBO', 'BuscarAtivos', {}).then(
                function (result) {
                    if (result.data) {
                        $scope.alergenTypeCodes = result.data;
                    }
                },
                $rootScope.TratarErro
            );

            $scope.containmentCodes = [
                  {'id': 1, 'codigo': 'CONTAINS', 'nome': 'CONTÉM', 'descricao': 'Intencionalmente incluído no produto.'}
                , {'id': 2, 'codigo': 'FREE_FROM', 'nome': 'NÃO CONTÉM', 'descricao': 'O produto não contém a substância indicada.'}
                , {'id': 3, 'codigo': 'MAY_CONTAIN', 'nome': 'PODE CONTER', 'descricao': 'A substância não é incluída intencionalmente. No entanto, devido às instalações de produção compartilhadas ou por outros motivos, o produto pode conter a substância.'}
            ];
            
        };

        // Init
        (function init() {
            initListas();
        })();
        
    }]
  };
});