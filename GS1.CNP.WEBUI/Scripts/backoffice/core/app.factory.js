﻿app.factory('Utils', function ($q, $rootScope, $timeout) {
    function UploadFile(url, inputSelectorId, callback) { //TipoUpload: fisico, blob ou blobAzure
        if ("FormData" in window) {
            var uploadfiles = $('#' + inputSelectorId),
            uploadedfiles = uploadfiles.get(0).files,
            fromdata = new FormData(),
            deferred;

            for (var i = 0; i < uploadedfiles.length; i++)
                fromdata.append(uploadedfiles[i].name, uploadedfiles[i]);

            deferred = $.ajax({
                url: url,
                type: "POST",
                data: fromdata,
                contentType: false,
                processData: false
            });

            deferred.done(function (result) {
                if (result && result.length > 0) {
                    //var ret = (result instanceof Array) ? result[0] : JSON.parse(result);
                    var ret = (result instanceof Array) ? result : JSON.parse(result);

                    //                        if (ret instanceof Array)
                    //ret = ret[0];

                    //                    var ret = (result instanceof Array) ? result[0] : JSON.parse(result);

                    //                    if (ret instanceof Array)
                    //                        ret = ret[0];

                    callback(ret);
                }
            }).fail(function (err) { alert(err.statusText); });

            deferred.promise();
        }
        else {
            $.ajaxFileUpload({
                url: url,
                secureuri: false,
                fileElementId: inputSelectorId,
                dataType: "json",
                success: function (result) { if (result && result.length > 0) { callback(result[0]); } },
                error: function (data, status, e) { alert(e); }
            });
        }
    }

    Date.prototype.yyyymmdd = function () {

        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based         
        var dd = this.getDate().toString();

        date = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
        return date;
    };

    var _utils = {

        maxlengthGtin: {
            "1": 8
            , "2": 12
            , "3": 13
            , "4": 14
        },

        isImage: function (src) {
            var deferred = $q.defer();
            var image = new Image();
            image.onerror = function () { deferred.resolve(false); };
            image.onload = function () { deferred.resolve(true); };
            image.src = src;

            return deferred.promise;
        },
        longDate: function () {

            var now = new Date();
            var days = new Array('Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado');
            var months = new Array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
            var date = ((now.getDate() < 10) ? "0" : "") + now.getDate();

            //Calcular 4 dígitos do ano
            function fourdigits(number) {
                return (number < 1000) ? number + 1900 : number;
            }

            //Junção das datas
            var today = days[now.getDay()] + ", " + date + " de " + months[now.getMonth()] + " de " + (fourdigits(now.getYear()));

            //Retorno da data completa
            return today
        },
        upload: function (inputSelectorId, tipoUpload, BO, callback) {
            var url = 'UploadHandler.ashx' + '?TipoUpload=' + tipoUpload + '&BO=' + BO;
            UploadFile(url, inputSelectorId, callback);
        },
        uploadXML: function (inputSelectorId, callback) {
            UploadFile('XMLUploadHandler.ashx', inputSelectorId, callback);
        },
        uploadFile: function (inputSelectorId, callback) {
            UploadFile('FileUploadHandler.ashx', inputSelectorId, callback);
        },
        isValidDate: function (date) {
            // First check for the pattern
            if (!/^\d{4}\/\d{2}\/\d{2}$/.test(date))
                return false;

            // Parse the date parts to integers
            var parts = date.split("/");
            var day = parseInt(parts[2], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[0], 10);

            // Check the ranges of month and year
            if (year < 1000 || year > 3000 || month == 0 || month > 12)
                return false;

            var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

            // Adjust for leap years
            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                monthLength[1] = 29;

            // Check the range of the day
            return day > 0 && day <= monthLength[month - 1];
        },

        validarPeriodo: function (inicio, fim) {
            if (!inicio && !fim) {
                return true;
            }

            var dataInicio = _utils.dataAnoMesDia(inicio);
            var dataFim = _utils.dataAnoMesDia(fim);

            if (!dataInicio || !dataFim) {
                $rootScope.alert("Por favor, informar o período de datas.");
                return false;
            } else if (!_utils.isValidDate(dataInicio) || !_utils.isValidDate(dataFim)) {
                $rootScope.alert("Insira datas válidas.");
                return false;
            } else if (dataInicio > dataFim) {
                $rootScope.alert("A data de início do relatório não pode ser maior que a data de fim do relatório.");
                return false;
            }

            return true;
        },

        validarPeriodoPesquisaSatisfacao: function (inicio, fim, validaDataAtual) {
            if (!inicio && !fim) {
                return true;
            }

            d = new Date();
            var dataAtual = d.yyyymmdd();

            var dataInicio = _utils.dataAnoMesDia(inicio);
            var dataFim = _utils.dataAnoMesDia(fim);

            if (!dataInicio || !dataFim) {
                $rootScope.alert("Por favor, informar o período de datas.");
                return false;
            } else if (!_utils.isValidDate(dataInicio) || !_utils.isValidDate(dataFim)) {
                $rootScope.alert("Insira datas válidas.");
                return false;
            } else if (dataInicio < dataAtual && dataInicio != dataAtual && validaDataAtual) {
                $rootScope.alert("A data inicial da Pesquisa de Satisfação não pode ser menor que a data atual.");
                return false;
            } else if (dataInicio > dataFim) {
                $rootScope.alert("A data de início da Pesquisa de Satisfação não pode ser maior que a data final.");
                return false;
            }

            return true;
        },

        dataAnoMesDia: function (data) {
            if (data != null && data != undefined && data != '') {
                var di = data.split('/');
                return di[2] + "/" + di[1] + "/" + di[0];
            }
            else
                return data;
        },
        dataMesDiaAno: function (data) {
            if (data != null && data != undefined && data != '') {
                var di = data.split('/');
                return di[1] + "/" + di[0] + "/" + di[2];
            }
            else
                return data;
        },
        dataDiaMesAno: function (data) {
            if (data != null && data != undefined && data != '') {
                var di = data.split('/');
                return di[0] + "/" + di[1] + "/" + di[2];
            }
            else
                return data;
        },
        validaDataComAtual: function (data) {
            if (data != null && data != undefined && data != '') {
                d = new Date();
                var dataAtual = d.yyyymmdd();

                var data = _utils.dataAnoMesDia(data);

                if (data < dataAtual && data != dataAtual) {
                    $rootScope.alert("A data informada não pode ser menor que a data atual.");
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        },
        validaDataProcessamentoComAtual: function (data) {
            if (data != null && data != undefined && data != '') {
                d = new Date();
                var dataAtual = d.yyyymmdd();

                var data = _utils.dataAnoMesDia(data);

                if (data < dataAtual && data != dataAtual) {
                    if (window.location.href.indexOf("backoffice/lotes") > -1) {
                        $rootScope.alert("A data de processamento informada não pode ser menor que a data atual.");
                    }
                    else {
                        $rootScope.alert("A data informada não pode ser menor que a data atual.");
                    }
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        },
        validaDataVencimentoComAtual: function (data) {
            if (data != null && data != undefined && data != '') {
                d = new Date();
                var dataAtual = d.yyyymmdd();

                var data = _utils.dataAnoMesDia(data);

                if (data < dataAtual && data != dataAtual) {
                    if (window.location.href.indexOf("backoffice/lotes") > -1) {
                        $rootScope.alert("A data de vencimento informada não pode ser menor que a data atual.");
                    }
                    else {
                        $rootScope.alert("A data informada não pode ser menor que a data atual.");
                    }
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        },
        validaCPF: function (cpf) {

            cpf = cpf.replace(/[^\d]+/g, '');
            if (cpf === '' || cpf === '00000000000' || cpf === '11111111111' || cpf === '22222222222' || cpf === '33333333333' || cpf === '44444444444' || cpf === '55555555555' || cpf === '66666666666' || cpf === '77777777777' || cpf === '88888888888' || cpf === '99999999999' || cpf.length !== 11) {
                return false;
            }
            function validateDigit(digit) {
                var add = 0;
                var init = digit - 9;
                for (var i = 0; i < 9; i++) {
                    add += parseInt(cpf.charAt(i + init)) * (i + 1);
                }
                return (add % 11) % 10 === parseInt(cpf.charAt(digit));
            }
            return validateDigit(9) && validateDigit(10);
        },

        validaCNPJ: function (c) {

            var b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
            c = c.replace(/[^\d]/g, '').split('');
            if (c.length !== 14) {
                return false;
            }

            for (var i = 0, n = 0; i < 12; i++) {
                n += c[i] * b[i + 1];
            }
            n = 11 - n % 11;
            n = n >= 10 ? 0 : n;
            if (parseInt(c[12]) !== n) {
                return false;
            }

            for (i = 0, n = 0; i <= 12; i++) {
                n += c[i] * b[i];
            }
            n = 11 - n % 11;
            n = n >= 10 ? 0 : n;
            if (parseInt(c[13]) !== n) {
                return false;
            }
            return true;
        },

        EmbedYoutube: function (video, callback) {
            if (video != undefined && video != null && video != '') {
                var idVideo;
                //                var links = video.split('?');
                //                for (x in links) {
                //                    if ((/^v=/).test(links[x]) || (/^V=/).test(links[x])) {
                //                        arrayLinks = links[x].split('=');
                //                        idVideo = arrayLinks[1].split('&')[0].toString();
                //                    }
                //                }

                var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
                var match = video.match(regExp);
                idVideo = (match && match[7].length == 11) ? match[7] : false;

                if (idVideo != undefined && idVideo != '')
                    callback('//www.youtube.com/embed/' + idVideo + '?wmode=transparent&rel=0');
                else
                    callback();
            }
        },

        ThumbYouTube: function (campoForm, callback) {

            if (campoForm != undefined && campoForm.video != undefined && campoForm.video != null && campoForm.video != '') {
                var idVideo, encontrou = false;
                links = campoForm.video.split('?');

                for (var x = 0; x < links.length; x++) {
                    if ((/^v=/).test(links[x]) || (/^V=/).test(links[x])) {
                        arrayLinks = links[x].split('=');
                        idVideo = arrayLinks[1].split('&')[0].toString();
                    }
                }

                encontrou = false;

                if (campoForm != undefined) {
                    campoForm.video = 'http://i.ytimg.com/vi/' + idVideo + '/default.jpg';
                    encontrou = true;
                }

                if (encontrou) {
                    callback(campoForm);
                }

            } else {
                callback();
            }
        },

        MonthToAddDate: function (data, monthtoadd) {
            function z(n) { return (n < 10 ? '0' : '') + n }

            Date.isLeapYear = function (year) {
                return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
            };

            Date.getDaysInMonth = function (year, month) {
                return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
            };

            Date.prototype.isLeapYear = function () {
                return Date.isLeapYear(this.getFullYear());
            };

            Date.prototype.getDaysInMonth = function () {
                return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
            };

            Date.prototype.addMonths = function (value) {
                var n = this.getDate();
                this.setDate(1);
                this.setMonth(this.getMonth() + parseInt(value));
                this.setDate(Math.min(n, this.getDaysInMonth()));
                return this;
            };

            var myDate = new Date(data); //'01/31/2016'
            var result1 = myDate.addMonths(monthtoadd);

            return z(result1.getDate()) + "/" + z(result1.getMonth() + 1) + "/" + z(result1.getFullYear());
        },

        //Mover scroll da tela para o início
        moveScrollTop: function () {
            $timeout(function () {
                $(".page-container").scrollTop(0);
                $(window).scrollTop(0);
            }, 1500);
        },

        youtubeURLValidator: function (url) {
            if (url != undefined || url != '') {
                var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
                var match = url.match(regExp);
                if (match && match[2].length == 11) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        },

        bloquearCamposVisualizacao: function (chave) {
            if ($rootScope.usuarioLogado != undefined) {

                var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
                    return obj === chave;
                });

                if (retorno != undefined && retorno != null && retorno.length > 0) {
                    angular.element(".form-container :input, .grid-container :input, .page-container :input, .modal :input").attr("disabled", true);
                    angular.element(".form-container :button, .grid-container :button, .page-container :button, .modal :button").attr("disabled", true);
                    angular.element("button[name=NovoRegistro]").attr("disabled", true);
                    angular.element("button[name=cancel], button[name=fechar], button[name=avancar], button[name=voltar], button[name=find]").attr("disabled", false);
                    angular.element("a").attr("disabled", true);
                    angular.element("a[name=NovaPesquisa]").attr("disabled", false);
                    angular.element("a[name=limpar], a[name=ajuda], a[name=NovoRegistro]").attr("disabled", false);
                    angular.element(".btn-inverse").attr("disabled", false);
                    angular.element(".tools-bottom a").attr("disabled", "disabled").css('pointer-events', 'none');
                    angular.element(".list-epc a").attr("disabled", "disabled").css('pointer-events', 'none');
                    angular.element("#sidebar-shortcuts a").attr("disabled", false);
                    angular.element("input[name=inputSearch]").attr("disabled", false);

                    if (document.getElementsByClassName("jstree").length > 0) { }

                    if (document.getElementsByClassName("wysiwyg-textarea").length > 0) {
                        angular.element(".wysiwyg-textarea").attr("disabled", true);
                    }
                }
                else {
                    angular.element(".form-container :input, .grid-container :input, .page-container :input, .modal :input").attr("disabled", false);
                    angular.element(".form-container :button, .grid-container :button, .page-container :button, .modal :button").attr("disabled", false);
                    angular.element("button[name=NovoRegistro]").attr("disabled", false);
                    angular.element("a").attr("disabled", false);
                }
                angular.element(".fieldDisabled").attr("disabled", true);
                angular.element(".fieldEnable").removeAttr("disabled");
            }
        },

        desbloquearCamposVisualizacaoPesquisa: function () {

            angular.element(".form-container :input, .grid-container :input, .page-container :input, .modal :input").attr("disabled", false);
            angular.element(".form-container :button, .grid-container :button, .page-container :button, .modal :button").attr("disabled", false);
            angular.element("button[name=NovoRegistro]").attr("disabled", false);
            angular.element("a").attr("disabled", false);

            if (document.getElementsByClassName("jstree").length > 0) {

            }

            if (document.getElementsByClassName("wysiwyg-textarea").length > 0) {
                angular.element(".wysiwyg-textarea").attr("disabled", false);
            }
            angular.element(".fieldDisabled").attr("disabled", true);
            angular.element(".fieldEnable").removeAttr("disabled");
        },

        resetaData: function (nomeid) {
            angular.element("#" + nomeid).datepicker("update", new Date()).val("");
        },

        //Validação de Dígito Verificador
        validaDigitoVerificador: function (valor) {
            var resultado = 0;

            DVAtual = valor.substring(valor.length - 1, valor.length);
            valor = valor.substring(0, valor.length - 1);

            for (var i = 0; i < valor.length; i++) {

                if (((i + 1) % 2 == 0 && valor.length % 2 == 0)
                        || ((i + 1) % 2 == 1 && valor.length % 2 == 1)) {
                    resultado += (parseInt(valor[i]) * 3);
                }
                else {
                    resultado += parseInt(valor[i]);
                }
            }

            var verificadorResultado = 10 - (resultado % 10);
            if (verificadorResultado == 10) verificadorResultado = 0;

            //Valida DV
            if (verificadorResultado == DVAtual)
                return true;
            else
                return false;
        }
    };

    return _utils;
});