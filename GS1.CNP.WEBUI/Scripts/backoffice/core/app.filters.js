﻿var app = angular.module('CNP.Backoffice');

app.filter('formatStatus', function () {
    return function (input) {
        if (input) {
            if (input == "EM_CADASTRO") {
                return "EM CADASTRO";
            } else if (input == "AGUARDANDO_APROVACAO") {
                return "AGUARDANDO APROVAÇÃO"
            } else if (input == "AGUARDANDO_APURACAO") {
                return "AGUARDANDO APURAÇÃO";
            } else {
                return input;
            }
        }
    };
});

app.filter('tipoassociado', function () {
    return function (input) {
        if (input) {
            if (input == "1") {
                return "PREMIUM";
            } else if (input == "0") {
                return "STANDARD"
            } else {
                return input;
            }
        }
    };
});

app.filter('tipoassociado2', function () {
    return function (input) {
        if (input == 0 || input == 1) {
            if (input == "1") {
                return "Premium";
            } else if (input == "0") {
                return "Standard"
            } else {
                return input;
            }
        }
    };
});

app.filter('status', function () {
  return function (input) {
	  if (input != null) {
		  if (input == "1") {
			  return "Ativo";
		  } else if (input == "0") {
			  return "Inativo"
		  } else if (input == "Ativo") {
			  return "1"
		  } else if (input == "Inativo") {
			  return "0"
		  } else {
			  return input;
		  }
	  }
  };
});

app.filter('codigoprodutotipo', function () {
    return function (input) {
        if (input != null) {
            if (input == "1") {
                return "Produto";
            } else if (input == "2") {
                return "Serviço"
            } else if (input == "Produto") {
                return "1"
            } else if (input == "Serviço") {
                return "2"
            } else {
                return input;
            }
        }
    };
});

app.filter('situacao', function () {
    return function (input) {
        if (input != null) {
            if (input == "1") {
                return "Usado";
            } else if (input == "0") {
                return "Novo"
            } else if (input == "Usado") {
                return "1"
            } else if (input == "Novo") {
                return "0"
            } else {
                return input;
            }
        }
    };
});

app.filter('statuspublicacao', function () {
    return function (input) {
        if (input != null) {
            if (input == "1") {
                return "Aguardando Publicação";
            }
            else if (input == "0") {
                return "Desativado";
            }
            else if (input == "2") {
                return "Publicado";
            }
            else if (input == "3") {
                return "Bloqueado";
            }
            else if (input == "4") {
                return "Aguardando Desbloqueio";
            }

            else if (input == "Aguardando Publicação") {
                return "1";
            } else if (input == "Desativado") {
                return "0";
            } else if (input == "Publicado") {
                return "2";
            } else if (input == "Bloqueado") {
                return "3";
            } else if (input == "Aguardando Desbloqueio") {
                return "4";
            } else {
                return input;
            }
        }
    };
});

app.filter('statusbloqueio', function () {
    return function (input) {
        if (input != null) {
            if (input == "3") {
                return "Não";
            }
            else if (input == "4") {
                return "Sim";
            } 
            else if (input == "Não") {
                return "3";
            }
            else if (input == "Sim") {
                return "4";
            } 
            else {
                return input;
            }
        }
    };
});

  app.filter('formatData', function () {
      return function (input) {
          if (input != null) {
              return value.getDate() + "/" + value.getMonth() + 1 + "/" + value.getYear();
          }
      };
  });

  app.filter('status_grid', function (item) {
      //return function (input) {
          //if (input) {

          $filter('status')(item[i].status);

         // }
     // };
  });


app.filter('percent', function () {
    return function (input) {
        if (input) {
            return input + "%";
        } else {
            return '-';
        }
    };
});

app.filter('cnpj', function () {
    return function (input) {
        if (input && input.length == 14) {
            return input.substr(0, 2) + "." + input.substr(2, 3) + "." + input.substr(5, 3) + "/" + input.substr(8, 4) + "-" + input.substr(12, 2);
        } else {
            return input || "";
        }
    };
});

app.filter('telefone', function () {
    return function (input) {
        if (input && input.length == 10) {
            return "(" + input.substring(0, 2) + ") " + input.substr(2, 4) + "-" + input.substr(6, 4);
        } else if (input && input.length == 11) {
            return "(" + input.substring(0, 2) + ") " + input.substr(2, 5) + "-" + input.substr(7, 4);
        } else {
            return input || "";
        }
    };
});

app.filter('formatQuebraLinha', function () {
    return function (input, valor) {
        if (input) {
            var nome = input.trim();

            var flag = 0;
            var out = "";

            for (var i in nome) {
                if (flag == 0) {
                    out += nome.substring(0, valor) + "\n";
                    flag++;
                }

                else if (i % valor == 0) {
                    out += nome.substring(flag * valor, (flag * valor) + valor) + "\n"; ;
                    flag++;
                }
            }
            return out;
        } else
            return input;
    };
});

app.filter('cortaTexto', function () {
    return function (input, valor) {
        if (input) {
            //var value = input.trim();
            var value = input;

            if (value != undefined && value != null && value != '' && value.length > valor) {
                return value.substring(0, valor) + '...';
            } else {
                return value;
            }
        } else {
            return input;
        }
    };
});

app.filter('pipequebralinha', function () {
    return function (input) {
        return input.replace(/\|/gi, "\n");
    };
});

app.filter('tipourl', function () {
    return function (input) {
        if (input) {
            if (input == "1") {
                return "Foto";
            } else if (input == "2") {
                return "Reserva"
            } else if (input == "3") {
                return "Linkeddata"
            } else if (input == "4") {
                return "Youtube"
            } else if (input == "5") {
                return "Produto"
            } else {
                return input;
            }
        }
    };
});

app.filter('statusgtin', function () {
    return function (input) {
        if (input) {
            if (input == "1") {
                return "Ativo";
            } else if (input == "2") {
                return "Suspenso"
            } else if (input == "3") {
                return "Reativado"
            } else if (input == "4") {
                return "Cancelado"
            } else if (input == "5") {
                return "Em Elaboração"
            } else if (input == "6") {
                return "Reutilizado"
            } else if (input == "7") {
                return "Transferido"
            }
            else {
                return input;
            }
        }
    };
});

app.filter('statussimnao', function () {
    return function (input) {
        if (input != undefined) {
            if (input == "1") {
                return "Sim";
            } else if (input == "0") {
                return "Não"
            }
            else {
                return input;
            }
        }
    };
});

app.filter('statusgln', function () {
    return function (input) {
        if (input != undefined) {
            if (input == "1") {
                return "Ativo";
            } else if (input == "0") {
                return "Inativo"
            }
            else {
                return input;
            }
        }
    };
});

app.filter('filterTable', function ($filter) {
    return function () {
        var filterName = [].splice.call(arguments, 1, 1)[0] || "filter";
        var filter = filterName.split(":");
        if (filter.length > 1) {
            filterName = filter[0];
            for (var i = 1, k = filter.length; i < k; i++) {
                [ ].push.call(arguments, filter[i]);
            }
        }
        return $filter(filterName.toLowerCase()).apply(null, arguments);
    };
});

app.filter('validaUPCE', function () {
    return function (items, codigotipogtin, codigogtin) {
        if (codigotipogtin != undefined && codigogtin != undefined && codigotipogtin == 2) {
            var blValido = true;
            var UPCA = '0' + codigogtin.toString().substring(0, 7);
            if (UPCA.substring(0, 1) != '0' && UPCA.substring(0, 1) != '1')
                blValido = false;
            else {
                
                if (UPCA.substring(3, 6) == '000' || UPCA.substring(3, 6) == '100' || UPCA.substring(3, 6) == '200')
                    blValido = true;
                else if (UPCA.substring(4, 6) == '00')
                    blValido = true;
                else if (UPCA.substring(5, 6) == '0')
                    blValido = true;
                else if (UPCA.substring(10, 11) >= '5')
                    blValido = true;
                else
                    blValido = false;
            }

            if (blValido) {
                return items;
            } else {
                var filtered = [];
                angular.forEach(items, function (item) {
                    if (item.codigo != 30) {
                        filtered.push(item);
                    }
                });
            }

            return filtered;
        } else {
            return items;
        }

    };
});

app.filter('numberFixedLen', function () {
    return function (n, len) {
        var num = parseInt(n, 10);
        len = parseInt(len, 10);
        if (isNaN(num) || isNaN(len)) {
            return n;
        }
        num = '' + num;
        while (num.length < len) {
            num = '0' + num;
        }
        return num;
    };
});

app.filter('trusted', ['$sce', function ($sce) {
    return function (url) {
        return $sce.trustAsResourceUrl(url);
    };
} ]);

app.filter('statuspublicacao', function () {
    return function (input) {
        if (input != null) {
            if (input == "0") {
                return "Não Publicado";
            }
            else if (input == "1") {
                return "Publicado";
            }

            else if (input == "Não Publicado") {
                return "0";
            } else if (input == "Publicado") {
                return "1";
            } else {
                return input;
            }
        }
    };
});

app.filter('statuslote', function () {
    return function (input) {
        if (input != null) {
            if (input == "1") {
                return "Em Elaboração";
            }
            else if (input == "2") {
                return "Publicado";
            }

            else if (input == "Em Elaboração") {
                return "1";
            } else if (input == "Publicado") {
                return "2";
            } else {
                return input;
            }
        }
    };
});

app.filter('cpfcnpj', function () {
    return function (input) {
        if (input != null && input != '') {
            var str = input + '';
                str = str.replace(/\D/g, '');

            if (input.length == 11) {
                str = str.replace(/(\d{3})(\d)/, "$1.$2");
                str = str.replace(/(\d{3})(\d)/, "$1.$2");
                str = str.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            } else if (input.length == 14) {
                str = str.replace(/^(\d{2})(\d)/, '$1.$2');
                str = str.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
                str = str.replace(/\.(\d{3})(\d)/, '.$1/$2');
                str = str.replace(/(\d{4})(\d)/, '$1-$2');
            }

            return str;
        }
    };
});

app.filter('to_trusted', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
} ]);

app.filter('perfilgepir', function () {
    return function (codigoperfil, perfis, campo) {
        if (perfis != undefined && perfis != null && perfis.length > 0) {
            var perfil = $.grep(perfis, function (obj) {
                return obj.id === codigoperfil;
            });
            if (perfil != undefined && perfil != null && perfil.length > 0) {
                return perfil[0][campo];
            } else {
                return codigoperfil;
            }
        } else {
            return codigoperfil;
        }
    };
});