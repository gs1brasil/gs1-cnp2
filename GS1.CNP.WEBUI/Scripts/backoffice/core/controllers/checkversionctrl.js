app.controller('CheckVersionCtrl', ['$scope', '$modal', '$rootScope', '$timeout', '$location', 'LoginService', 'blockUI', '$filter', '$sce', 'genericService', '$cookies', '$window', 'vcRecaptchaService', 'Utils', '$translate',
    function ($scope, $modal, $rootScope, $timeout, $location, login, blockUI, $filter, $sce, genericService, $cookies, $window, vcRecaptchaService, Utils, $translate) {

        //Retorna a versão do Android
        function getAndroidVersion(ua) {
            ua = (ua || navigator.userAgent).toLowerCase();
            var match = ua.match(/android\s([0-9\.]*)/);
            return match ? match[1] : false;
        };

        //Retorna a versão do iOS
        function getIosVersion() {
            var ua = navigator.userAgent;
            var uaindex;

            mobileOS = 'iOS';
            uaindex = ua.indexOf('OS ');

            mobileOSver = ua.substr(uaindex + 3, 3).replace('_', '.');

            return mobileOSver;
        };

        //Retorna a versão do Windows Phone
        function getWPVersion(ua) {
            ua = (ua || navigator.userAgent).toLowerCase();
            var match = ua.match(/windows phone\s([0-9\.]*)/);
            return match ? match[1] : false;
        };

        function check_navigator() {
            var ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE ' + (tem[1] || '');
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        }

        //Verifica se a versão do Browser possui suporte
        function check_navigator_uptodate() {
            var resultado = check_navigator();

            var browser = resultado.split(' ')[0];
            var version = resultado.split(' ')[1];

            $scope.showVersionMessage = false;

            if (browser == 'MSIE' || browser == 'IE' || browser == 'Edge') {
                if (version < 10) {
                    $scope.showVersionMessage = true;
                }
            }
            else if (browser == 'Firefox') {
                if (version < 42.0) {
                    $scope.showVersionMessage = true;
                }
            }
            else if (browser == 'Chrome') {
                if (version < 40) {
                    $scope.showVersionMessage = true;
                }
            }
        }

        //Verifica se a versão do SO ou do Browser possui suporte
        function check_uptodate() {
            var isiDevice = /ipad|iphone|ipod/i.test(navigator.userAgent.toLowerCase());
            var isAndroid = /android/i.test(navigator.userAgent.toLowerCase());
            var isWindowsPhone = /windows phone/i.test(navigator.userAgent.toLowerCase());

            $scope.showVersionMessage = false;

            if (isiDevice) {
                var version = getIosVersion();

                if (Number(version.charAt(0)) < 7) {
                    $scope.showVersionMessage = true;
                }
            }
            else if (isAndroid) {
                var version = parseInt(getAndroidVersion(), 10);

                if (version < 4) {
                    $scope.showVersionMessage = true;
                }
            }
            else if (isWindowsPhone) {
                var version = parseInt(getWPVersion(), 10);

                if (version < 8) {
                    $scope.showVersionMessage = true;
                }
            }
            else {
                check_navigator_uptodate()
            }
        }

        function detectmob() {
            if (navigator.userAgent.match(/Android/i)
                 || navigator.userAgent.match(/webOS/i)
                 || navigator.userAgent.match(/iPhone/i)
                 || navigator.userAgent.match(/iPad/i)
                 || navigator.userAgent.match(/iPod/i)
                 || navigator.userAgent.match(/BlackBerry/i)
                 || navigator.userAgent.match(/Windows Phone/i)
            ) {
                return true;
            }
            else {
                return false;
            }
        }

        check_uptodate();

    }
]);