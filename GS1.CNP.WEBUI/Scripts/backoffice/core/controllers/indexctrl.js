﻿var STR_PESQUISANDO = "PESQUISANDO..."


app.controller('IndexCtrl', ['$scope', '$modal', '$rootScope', '$timeout', '$location', '$interval', 'IndexService', 'genericService','x2js', '$route', 'blockUI','$window','$sessionStorage','$translate', '$localStorage',
function ($scope, $modal, $rootScope, $timeout, $location, $interval, IndexService, genericService, x2js, $route, blockUI, $window, $sessionStorage, $translate, $localStorage) {
    
    var usuario;

//    $scope.$storage = $localStorage.$default({
//        counter: 42
//    });
    $scope.usuario = {};
    $rootScope.notificacoes = [];
    $scope.exibeNotificacaoMensagem = false;
    $rootScope.traducoes = {};
    
    $(function(){
        $("#menu-toggler").on("click", function () {
            if ("ontouchend" in document) {
	            if ($(this).hasClass("display"))
		            $(".slimScrollDiv").css("display", "block");
	            else
		            $(".slimScrollDiv").css("display", "none");
            }});
    });
    
    $rootScope.refreshParametros = function(callback){
        
        IndexService.CarregaParametros("", function(data){
            if (data != null && data != undefined){
                $rootScope.parametros = data;        
            }

            if (callback) callback();
        });   
        
    };
    $rootScope.refreshParametros();

    IndexService.RetornaUsuarioLogado("", function (data) {if((data != null) && (data.nome != null)){
            $scope.usuario.nom_usuario = data.nome;       
            $rootScope.usuarioLogado = data;
            
            if(data.avisosenha == 1)
            {
                $translate(['38.MENSAGEM_SenhaExpiraAmanha', '38.MENSAGEM_SenhaExpiraEm', '38.MENSAGEM_Dias']).then(function (resultado) {
                    
                    if((data.diasvencimentosenha == 1) || (data.diasvencimentosenha == 0)){
                        mesagemsenha = resultado['38.MENSAGEM_SenhaExpiraAmanha'];
                    } else{
                        mesagemsenha = resultado['38.MENSAGEM_SenhaExpiraEm'] + ' ' + data.diasvencimentosenha + ' ' + resultado['38.MENSAGEM_Dias'] + '.';
                    }

                     $scope.dropdown = true;
                     $rootScope.notificacoes.push({ titulo: 'CNP', 
                                                   mensagem: mesagemsenha,
                                                   linkdescricao: 'Trocar Agora',
                                                   linkurl: '/backoffice/troca_senha'
                                                });
                });
            }

            if(data.termoadesaopendente == 1){
                $location.path('/backoffice/aceiteTermoAdesao');
            }else{
                $rootScope.verificarPesquisaSatisfacaoDisponivel();
            }

            if((data.id_statususuario == 5) || (data.avisosenha == 2))
            {
                $location.path('/backoffice/troca_senha');
            }

            // Implementar notificação para o GPC
       }
    });

    $rootScope.getInternetExplorerVersion = function() {
        var rv = -1;
        if (navigator.appName == 'Microsoft Internet Explorer') {
            var ua = navigator.userAgent;
            var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null)
                rv = parseFloat(RegExp.$1);
        }
        else if (navigator.appName == 'Netscape') {
            var ua = navigator.userAgent;
            var re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null)
                rv = parseFloat(RegExp.$1);
        }
        return rv;
    }


	$rootScope.verificaPermissao = function (nomeBO, nomeMethod, callback) {
        IndexService.VerificaPermissao(nomeBO, nomeMethod, callback);
    };

    setInterval(function() {
        IndexService.VerificaUsuarioLogado("", function (data) {
           if(data != undefined && data != null && data.data == true) {
                IndexService.RetornaUsuarioLogado("", function (data) {
                    if((data != null) && (data.nome != null)){
                        $scope.usuario.nom_usuario = data.nome;       
                        $rootScope.usuarioLogado = data;
           
//                        if(data.avisosenha == 1)
//                        {
//                            if((data.diasvencimentosenha == 1) || (data.diasvencimentosenha == 0)){
//                                mesagemsenha = 'Sua senha expira amanhã.';
//                            } else{
//                                mesagemsenha = 'Sua senha expira em ' + data.diasvencimentosenha + ' dias.';
//                            }

//                                $scope.dropdown = true;
//                                $rootScope.notificacoes.push({ titulo: 'GS1 Trade', 
//                                                            mensagem: mesagemsenha,
//                                                            linkdescricao: 'Trocar Agora',
//                                                            linkurl: '/backoffice/troca_senha'
//                                                        });
//                        }

                        if(data.termoadesaopendente ==1)
                        {
                            $location.path('/backoffice/aceiteTermoAdesao');
                        }

                        if((data.id_statususuario == 5) || (data.avisosenha == 2))
                        {
                            $location.path('/backoffice/troca_senha');
                        }

                        

                        // Implementar notificação para o GPC
                    }
                });
           }
        });

        setTimeout(function () {
            angular.element("#notificacao").removeClass("open");
        }, 200);
        
    }, 60000);

    $scope.focusFirstInput = function(event){
        if ((window.event ? event.keyCode : event.which) == 9) {
            event.stopPropagation();
            event.preventDefault();
            $('input:text:enabled:visible:first').focus();
        }
    };

    $scope.geraMenu = function() {
        $scope.menu = [{
		                  href: "/backoffice/home",
		                  icon: "icon-home",
		                  label: "Home"
                        }];
         
        IndexService.RetornaModulosUsuarioLogado("", function (data) {

            if(data != undefined && data != null){
                var jsonObj = x2js.xml_str2json(data);
            }

            IndexService.RetornaUsuarioLogado("", function (data) {
                if((data != null) && (data.nome != null)) {
                    $scope.usuario.nom_usuario = data.nome;       
                    $rootScope.usuarioLogado = data;

                    var qtmodulos = 0;

                    if(jsonObj && jsonObj.Modulos) {

                        if (jsonObj.Modulos.MODULO && jsonObj.Modulos.MODULO.length) {
                
                            for (index = 0; index < jsonObj.Modulos.MODULO.length; ++index) {

                                $scope.menu.push(carregarModulo(jsonObj.Modulos.MODULO[index]));
                            }
                
                        } else {

                            $scope.menu.push(carregarModulo(jsonObj.Modulos.MODULO));
                            
                        }
                    }
            
			        $timeout(function () {
				        angular.element("#sidebar li ul").on("click", function() {
					        angular.element("#menu-toggler").click()
				        });
			        }, 100);
                }
            });
        });
    };

    function carregarModulo(modulo) {
        var submenus = [];

        if (modulo.FORMULARIO && modulo.FORMULARIO.length) {
            for (var i = 0; i < modulo.FORMULARIO.length; i++) {                    
                submenus.push(carregarFormulario(modulo.FORMULARIO[i]));
            }
        } else {
            submenus.push(carregarFormulario(modulo.FORMULARIO));
        }

        return {'id': modulo._Codigo
            , 'href': modulo._Url
            , 'icon': modulo._Icone
            , 'label': modulo._Nome
            , 'submenus': submenus
            , 'show': (modulo._Codigo == 2 && $rootScope.usuarioLogado.id_tipousuario == 2 ? true : false)};
    }

    function carregarFormulario(formulario) {
        return {
            'id': formulario._Codigo
            , 'href': formulario._Url
            , 'icon': formulario._Icone
            , 'label': formulario._Nome};
    }

    $scope.geraAtalho = function() {
        genericService.postRequisicao('AtalhoBO', 'BuscarAtalhos', {}).then(
            function (result) {
                if(result != undefined && result.data != undefined) {
                    $scope.atalho = result.data
                }
            },
            $rootScope.TratarErro
        );
    }

	$rootScope.typeaheadBlur = function(item, model) {
        if (item[model] != undefined && item[model].id) {
			item['id_' + model] = item[model].id;
		} else {
			item[model] = null;
			item['id_' + model] = null;
		}
	};

    $rootScope.typeaheadClick = function($event) {
        var target = $($event.target).next();
        
        target.data("oldVal", target.eq(0).val());
        target.eq(0).val(".").trigger("input"); // este trigger é apenas para 'mudar o change'
        target.eq(0).val(STR_PESQUISANDO).trigger("input"); // esse sim está valendo

	};

    $rootScope.TratarErro = function (result, status, headers, config) {
        if (result != undefined && result != null && result.status == 403 || result.status == 404) {
            $location.path('/backoffice/erro/' + result.status);
        } else if (result.status != 401 && result.status != 419 && result.status != 420 && status != 401 && status != 419 && status != 420) {
            $rootScope.alert(result);
            blockUI.stop();
        }
    }

    $rootScope.alert = function (message, sucessCallback, title, traduzir, traducaoPadrao) {
        if (traduzir) {
            var traduzido = $translate.instant(message);
            message = (typeof traduzido === 'object' || traduzido == message) ? traducaoPadrao : traduzido;
        }

        if(!$rootScope.iserrormessageactive){
            $rootScope.iserrormessageactive = true;
		    var modalInstance = $modal.open({
			    template: ' <div class="modal-header" ><h3>{{title}}</h3></div><div class="modal-body"><p ng-bind-html="message"></p><!--{{message}}--></div><div class="modal-footer"><button class="btn btn-info" ng-click="ok()">OK</button></div>',
			    backdrop: 'static',
                controller: function ($scope, $modalInstance) {
				    $scope.message = message;
                    $scope.title = "Alerta";
                    $translate('38.MENSAGEM_Alerta').then(function (resultado) {
                        $scope.title = title || resultado || "Alerta";
                    });
				    
				    $scope.ok = function () {
					    $modalInstance.close();
                        $rootScope.iserrormessageactive = false;
				    };
			    }
		    });

            modalInstance.result.then(sucessCallback);
        }
	};

	$rootScope.confirm = function (message, positiveCallback, negativeCallback, title) {

		$modal.open({
			template: ' <div class="modal-header"><h3>{{title}}</h3></div><div class="modal-body">{{message}}</div><div class="modal-footer"><button class="btn btn-info" ng-click="sim()">Sim</button><button class="btn btn-danger" ng-click="nao()">Não</button></div>',
			backdrop: 'static',
            controller: function ($scope, $modalInstance) {
				$scope.message = message;
                $translate('38.MENSAGEM_Confirmacao').then(function (resultado) {
                    $scope.title = title || resultado || "Confirmação";
                });
				$scope.sim = function () {
					$modalInstance.result.then(positiveCallback);
					$modalInstance.close();
				};
				$scope.nao = function () {
					$modalInstance.result.then(negativeCallback);
					$modalInstance.close();
				};
			}
		});
	};
    
    $scope.promptVariacaoYTG = function (message, positiveCallback, negativeCallback, title) {
		$modal.open({
			templateUrl: 'views/templates/prompt_variacaoYTG.html',
			controller: function ($scope, $modalInstance) {
				$scope.message = message;
				$translate('38.MENSAGEM_Confirmacao').then(function (resultado) {
                    $scope.title = title || resultado;
                });
				$scope.ok = function (descricao) {
                    positiveCallback(descricao, function(){
                       $modalInstance.close();
                    });
				};
				$scope.cancel = function () {
					$modalInstance.result.then(negativeCallback);
					$modalInstance.close();
				};
			}
		});
	};


    $rootScope.warn = function (message, title, sucessCallback) {

		var modalInstance = $modal.open({
			template: ' <div class="modal-header"><h3>{{title}}</h3></div><div class="modal-body">{{message}}</div><div class="modal-footer"><button class="btn btn-danger" ng-click="ok()">OK</button></div>',
			controller: function ($scope, $modalInstance) {
				$scope.message = message;
                $translate('38.MENSAGEM_Erro').then(function (resultado) {
                    $scope.title = title || resultado;
                });
				$scope.ok = function () {
					$modalInstance.close();
				};
			}
		});
		modalInstance.result.then(sucessCallback);

	};

    $scope.alertPopup = function (message, title, time){
        $.gritter.add({
            title: title || "Aviso",
            text: message,
            sticky: false,
            time: time || 5000,
            class_name: 'gritter-info'
         });
    }

    $scope.errorPopup = function (message, title, time){
        $.gritter.add({
            title: title || "Aviso",
            text: message,
            sticky: false,
            time: time || 5000,
            class_name: 'gritter-error'
         });
    }

    $rootScope.errorCallback = function(error){
        if(error.data){
            $scope.warn(error.data.message);
        }else{
            $translate('38.MENSAGEM_ErroExecutarAcao').then(function (resultado) {
                $scope.warn(resultado);
            });
        }
    }

    $scope.showImportErrors = function (errors) {
		var modalInstance = $modal.open({
			templateUrl: 'views/templates/import_errors_modal.html',
			controller: function ($scope, $modalInstance) {
				$scope.errors = errors;
				$scope.close = function () {
					$modalInstance.close();
				};
			}
		});

	};

    $scope.showImportResult = function (infos, type) {
        if(infos.length == 0){
            return;
        }

		var modalInstance = $modal.open({
			templateUrl: 'views/templates/import_results_modal.html',
			controller: function ($scope, $modalInstance) {
                $scope.infos = infos;
                $scope.update = false;
                $scope.new = false;
                $scope.inactivate = false;

                $translate(['38.MENSAGEM_RegistrosInseridos', '38.MENSAGEM_RegistrosAtualizados', '38.MENSAGEM_RegistrosInativos']).then(function (resultado) {

                    if(type == "new"){
                        $scope.message = resultado['38.MENSAGEM_SeguinteRegistroInserido'] + ' ';
                        $scope.new = true;
                        $scope.title = resultado['38.MENSAGEM_RegistrosInseridos'];
                    } else if(type == "update"){
                        $scope.message = resultado['38.MENSAGEM_SeguinteRegistroAtualizado'] + ' ';
                        $scope.update = true;
                        $scope.title = resultado['38.MENSAGEM_RegistrosAtualizados'];
                    } else if(type == "inactivate") {
                        $scope.message = resultado['38.MENSAGEM_SeguinteRegistroInativo'] + ' ';
                        $scope.inactivate = true;
                        $scope.title = resultado['38.MENSAGEM_RegistrosInativos'];
                    }
                });

				$scope.close = function () {
					$modalInstance.close();
				};
			}
		});
	};

    $scope.toJavaDate = function(date) {
        return "(new Date(" + date.getFullYear() + "," + date.getMonth() + "," + date.getDate() + "," + date.getHours() + "," + date.getMinutes() + "," + date.getSeconds() + "," + date.getMilliseconds() + "))";
    }

    $rootScope.buscarAssociados = function(callback) {
        genericService.postRequisicao('LoginBO', 'BuscarAssociadosUsuario', { where: { usuarioId: $scope.usuarioLogado.id} }).then(
            function (result) {

                $rootScope.associadosDoUsuario = result.data; 

                if($rootScope.associadosDoUsuario != undefined && $rootScope.associadosDoUsuario.length > 0 && $scope.associadoatual == undefined && $scope.usuarioLogado.id_tipousuario == 2){
                    $scope.associadoatual = $rootScope.associadosDoUsuario[0].codigo;
                }
                else if($rootScope.associadosDoUsuario != undefined && $rootScope.associadosDoUsuario.length > 0 && $scope.associadoatual == undefined && $scope.usuarioLogado.id_tipousuario != 2){
                    $scope.bloqAssociado = true;
                    $scope.associadoatual = $rootScope.associadosDoUsuario[0].nome;
                }

                if(callback != undefined) {
                    callback();
                }
            },
            $rootScope.TratarErro
        );
    }

    $rootScope.verificaMaiorCodigo = function verificaMaiorCodigo(lista) {
        var maior = 0;

        //Pegar o maior CODIGO da lista
        angular.forEach(lista, function (val) {
            if (val.codigo > maior) {
                maior = val.codigo;
            }
        });

        return maior;
    };

    function isNumber(obj) { 
        return !isNaN(parseFloat(obj)) 
    }

    function isNumber(n) { 
        return /^-?[\d.]+(?:e-?\d+)?$/.test(n); 
    } 

    String.prototype.replaceAll = function (find, replace) {
        var str = this;
        return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
    };

    //Carrega associados cadastrados 
    $scope.BuscarAssociadosAutoComplete = function (nome) {
        if(isNumber(nome)) {
            nome = nome.replaceAll('.', '').replaceAll('-', '');
        }

        return genericService.postRequisicao('LoginBO', 'BuscarAssociadosUsuarioAutoComplete', { where: { nome: nome } }).then(
            function (result) {
                var associadosDoUsuario = [];
                angular.forEach(result.data, function (item) {
                    associadosDoUsuario.push({ codigo: item.codigo, nome: item.nome, cpfcnpj: item.cpfcnpj, cad: item.cad, 
                        indicadorcnaerestritivo: item.indicadorcnaerestritivo ? item.indicadorcnaerestritivo : '', codigoperfilgepir: item.codigoperfilgepir });
                });
                return associadosDoUsuario;
            }
        );
    }

    $scope.alteraAssociado = function(item) {

        if($scope.usuarioLogado.id_tipousuario == 1) {

            genericService.postRequisicao('LoginBO', 'AlteraAssociadoGS1', { where: { codigo: item.codigo, nome: item.nome, cpfcnpj: item.cpfcnpj, cad: item.cad, 
                                        indicadorcnaerestritivo: item.indicadorcnaerestritivo ? item.indicadorcnaerestritivo : '' , codigoperfilgepir: item.codigoperfilgepir} }).then(
                function (result) {
                    if(window.location.href.indexOf("backoffice/home") > -1) {
                        $scope.initializeDashboard();					   
                    }
                    $scope.ConsultarBoletos();
                    $rootScope.ConsultarMensagens();
                    $rootScope.flagAssociadoSelecionado = true;
                    $route.reload();                                
                },
                $rootScope.TratarErro
            );
        }
        else {
            genericService.postRequisicao('LoginBO', 'AlteraAssociado', { where: { codigo: item } }).then(
                function (result) {
                    $scope.ConsultarBoletos();
                     $rootScope.flagAssociadoSelecionado = true;
                    $route.reload();
                },
                $rootScope.TratarErro
            );
        }

        $scope.bloqAssociado = true;
    }

//    $scope.disableBloqAssociado = function() {
//        $scope.bloqAssociado = false;
//        angular.element("#typea").val("");
//    }

    $scope.disableBloqAssociado = function() {
        $scope.bloqAssociado = false;
        angular.element("#typea").val("");
        genericService.postRequisicao('LoginBO', 'AlteraAssociado', { where: { codigo: -1 } }).then(
            function (result) {
                    
                if(window.location.href.indexOf("backoffice/home") > -1) {
					$scope.initializeDashboard();                    
                }

                $scope.ConsultarBoletos();
                $rootScope.ConsultarMensagens();
                $route.reload();
            },
            $rootScope.TratarErro
        );
    }

    $scope.abreAtendimentoOnline = function () {   
        if($scope.urlchat == undefined || $scope.urlchat == null || $scope.urlchat == ""){ 
            var dados = { where: 'ajuda.urlchat' };

            genericService.postRequisicao('LoginBO', 'CarregarParametrosLogin', dados).then(function (result) {
                if (result.data.length > 0)
                    $scope.urlchat = result.data[0].valor;
                    window.open($scope.urlchat,'_blank','width=500,height=150'); 
            });

        }else{
            window.open($scope.urlchat,'_blank','width=500,height=150');      
        }
    }

    $scope.abreFAQ = function () {   
        $location.path("/backoffice/FAQ");
    }

    $scope.abrePassoaPasso = function () {   
        $location.path("/backoffice/ajudaPassoAPasso");
    }

    $scope.onLoad = function () {    
        genericService.postRequisicao('UsuarioBO', 'BuscarUsuarioLogado', '').then(
            function (result) {
                $scope.usuarioLogado = result.data;

                //GS1 Administrador
                if($scope.usuarioLogado != undefined && $scope.usuarioLogado.id_tipousuario == 1) {
                    $rootScope.verificaAssociadoSelecionado(function(data) {
                        if (data != undefined && data != null && data.nome != undefined && data.nome != null) {
                            $scope.bloqAssociado = true;
                            $scope.associadoatual = data.nome;
                        }

                        if(window.location.href.indexOf("backoffice/home") > -1) {
                            $scope.initializeDashboard();
                        }
                    });
                }
                else {
                    $scope.buscarAssociados(function() {
                        if(window.location.href.indexOf("backoffice/home") > -1) {
                            $scope.initializeDashboard();
                        }
                    });    
                }           
            },
            $rootScope.TratarErro
        );

        $scope.ConsultarUltimoAcesso();

        $rootScope.buscaIdiomas();        
        $rootScope.buscaTraducao();

        $rootScope.ConsultarMensagens();
        $scope.ConsultarBoletos();

        $interval(function() {
            $rootScope.ConsultarMensagens(); 
        }, 120000); 
        
//        $translate('38.MENSAGEM_Carregando').then(function (resultado) {
//            blockUI.message = resultado;
//        });

    }

    //Filtrar parâmetros do sistema
    $rootScope.retornaValorParametroByFiltro = function (filtro) {
        if (!$rootScope.parametros) {
            $rootScope.refreshParametros();
        }
        
        var filtro = filtro;

        function filterByID(obj) {
            return obj.chave == filtro;
        }

        if (!$rootScope.parametros) {
            if(filtro == 'grid.resultados.por.pagina')
                return 10;
            else
                return null;
        }

        var arrByID = $rootScope.parametros.filter(filterByID);

        return arrByID[0].valor;
    }

    $rootScope.verificaAssociadoSelecionado = function (callback) {
        
        genericService.postRequisicao('LoginBO', 'VerificarAssociadoSelecionado', {}).then(
            function (result) {
                
                if(result.data != undefined) {
                    callback(result.data);
                }else{
                    $rootScope.flagAssociadoSelecionado = false;
                    callback(null);                     
                }        
            },
            $rootScope.TratarErro
        );
    };

    $rootScope.verificarPesquisaSatisfacaoDisponivel = function() {
       
        genericService.postRequisicao('LoginBO', 'VerificarPesquisaSatisfacaoDisponivel', {}).then(
            function (result) {

                if(result.data != undefined && result.data != '') {
                    
                    $rootScope.pesquisasSatisfacaoDisponiveis = result.data;

                    var mensagem = '';

                    $translate(['38.MENSAGEM_VocePossui', '38.MENSAGEM_PesquisaSatisfacaoPendente', '38.MENSAGEM_PesquisasSatisfacaoPendentes']).then(function (resultado) {
                        if(result.data.length == 1)
                            mensagem = resultado['38.MENSAGEM_VocePossui'] + ' ' + result.data.length + ' ' + resultado['38.MENSAGEM_PesquisaSatisfacaoPendente'];
                        else if(result.data.length > 1)
                            mensagem = resultado['38.MENSAGEM_VocePossui'] + ' ' + result.data.length + ' ' + resultado['38.MENSAGEM_PesquisasSatisfacaoPendentes'];

                        //Remove Todas as Pesquisas de Satisfação das Notificações
                        for (var i in $rootScope.notificacoes) {
                            if ($rootScope.notificacoes[i].linkdescricao == 'Pesquisa de Satisfação') {
                                $rootScope.notificacoes.splice(i, 1);
                            }
                        }

                        $rootScope.notificacoes.push({ titulo: 'CNP', 
                                                mensagem: mensagem,
                                                linkdescricao: 'Pesquisa de Satisfação',
                                                linkurl: '/backoffice/home'
                                            });
                    });

                }
                else {
                    $rootScope.pesquisasSatisfacaoDisponiveis = [];
                    //Remove Todas as Pesquisas de Satisfação das Notificações
                    for (var i in $rootScope.notificacoes) {
                        if ($rootScope.notificacoes[i].linkdescricao == 'Pesquisa de Satisfação') {
                            $rootScope.notificacoes.splice(i, 1);
                        }
                    }
                }
            },
            $rootScope.TratarErro
        );
    }

    $rootScope.buscaIdiomas = function() {
        genericService.postRequisicao('IdiomaBO', 'BuscarIdiomasAtivos', {}).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $rootScope.idiomas = result.data;
                }
            },
            $rootScope.TratarErro
        );
    }

    $rootScope.buscaTraducao = function() {
        if($localStorage.idioma == undefined || $localStorage.idioma == '') {
            $rootScope.codigoidioma = 1;
            $rootScope.idiomaAtual = 'pt-br';
            $scope.$storage = $localStorage.$default({
                'idioma': 'pt-br',
                'codigoidioma': 1
            });
        } else {
            $translate.use($localStorage.idioma);
            $rootScope.codigoidioma = $localStorage.codigoidioma;
            $rootScope.idiomaAtual = $localStorage.idioma;
        }
    }

    $rootScope.alteraIdioma = function(codigo, descricao) {
        $rootScope.codigoidioma = codigo;      
        $rootScope.idiomaAtual = descricao;
        //$rootScope.mensagemcarregando = $translate.instant('38.MENSAGEM_Carregando');

        if($localStorage != undefined && $localStorage != '') {
            $localStorage.idioma = descricao;
            $localStorage.codigoidioma = codigo;
        }

        $translate.use(descricao);
        $route.reload();
    }

    $scope.initializeDashboard = function() {
        $scope.ConsultaBlocoProdutos(function(data) {
            $scope.ConsultaProdutosEmPreenchimento();
        });

        //$scope.ConsultaProdutos();
        $scope.ConsultaTotalEtiquetas();
        //$scope.ConsultarMensagens();
        //$scope.ConsultarBoletos();
    }

    $scope.ConsultaBlocoProdutos = function(callback) {

        var myBlock = blockUI.instances.get('blockProdutos');
        myBlock.start();

        genericService.postRequisicao('DashboardBO', 'ConsultaBlocoProdutos', {}).then(function (result) {
            
            myBlock.stop();

            if (result != null && result.data != null) {
                $scope.totalProdutosCadastrados = result.data[0].produtoscadastrados;
                $scope.totalProdutosPublicados = result.data[0].produtospublicados;
                $scope.totalProdutosDesativados = result.data[0].produtosdesativados;
                $scope.totalEmpresasAtivas = result.data[0].empresasativas;
                callback(result.data);
            }

        }, $rootScope.TratarErro);
    }

    $scope.ConsultaProdutos = function() {
        if(angular.element("#chartProdutos").length) {

            var chart = new CanvasJS.Chart("chartProdutos",
            {
                animationEnabled: true,
                axisX:{ gridColor: "Silver", tickColor: "silver", minimum: 0, gridThickness: 1, tickLength: 1, interval: 1 },                        
                axisY: { gridColor: "Silver", tickColor: "silver", minimum: 0, maximum: 100, gridThickness: 1, tickLength: 1, interval: 25 },
                toolTip:{ shared:false },
                theme: "theme1",
                legend:{ verticalAlign: "center", horizontalAlign: "right" },
                data: [
                    {        
                        type: "line",
                        showInLegend: true,
                        lineThickness: 2,
                        legendText: "GEPIR",
                        color: "#F08080",
                        dataPoints: [ { x: 0, y: 0 } ]
                    },
                    {        
                        type: "line",
                        showInLegend: true,
                        legendText: "Inbar",
                        color: "#20B2AA",
                        lineThickness: 2,
                        dataPoints: [ { x: 0, y: 0 } ]
                    }
                ],
                legend: {
                    cursor:"pointer",
                    fontSize: 13,
                    itemclick:function(e) {
                        e.dataSeries.visible = !(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible);
                        chart.render();
                    }
                }
            });

            $timeout(function() {
                chart.render(); 
            }, 500);  
        }
    }

    $scope.ConsultaProdutosEmPreenchimento = function() {
        genericService.postRequisicao('DashboardBO', 'ConsultaProdutosEmPreenchimento', {}).then(function (result) {

            if (result != null && result.data != null) {
                
                $scope.totalProdutosEmPreenchimento = result.data;

                if ($scope.totalProdutosEmPreenchimento > 0)
                    $scope.totalProdutosEmPreenchimento = ($scope.totalProdutosEmPreenchimento * 100) / $scope.totalProdutosCadastrados;

                $('#chartPreenchimento').html("");

                $timeout(function() {
                    $('#chartPreenchimento').circliful();
                }, 1000);
            }

        }, $rootScope.TratarErro);
    }

	$scope.initializeDashboard = function() {

        
        $scope.blockEtiquetas = blockUI.instances.get('blockEtiquetas');
        $scope.blockElaboracao = blockUI.instances.get('blockElaboracao');
        $scope.blockProduto = blockUI.instances.get('blockProduto');


        //$scope.blockEtiquetas.template = '<div class=\"block-ui-overlay\"></div><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div class=\"block-ui-message\" translate=\"38.MENSAGEM_Carregando\">{{ state.message }}</div></div>';
        //.template = '<div class=\"block-ui-overlay\"></div><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div class=\"block-ui-message\" ng-class=\"$_blockUiMessageClass\" translate=\"38.MENSAGEM_Carregando\">{{ state.message }}</div></div>';
        
        $scope.ConsultaBlocoProdutos(function(data) {
            $scope.ConsultaProdutosEmPreenchimento();
            $scope.blockElaboracao.stop();
        });

        //$scope.ConsultaProdutos();
        $scope.ConsultaTotalEtiquetas();
        $rootScope.ConsultarMensagens();
        $scope.ConsultarBoletos();
        $scope.ConsultarNoticias();
    }
	
	$scope.ConsultaBlocoProdutos = function(callback) {
       
        $scope.blockProduto.start();
        $scope.blockElaboracao.start();
        
        genericService.postRequisicao('DashboardBO', 'ConsultaBlocoProdutos', {}).then(function (result) {
            

            if (result != null && result.data != null) {
                $scope.totalProdutosCadastrados = result.data[0].produtoscadastrados;
                $scope.totalProdutosPublicados = result.data[0].produtospublicados;
                $scope.totalProdutosDesativados = result.data[0].produtosdesativados;
                $scope.totalEmpresasAtivas = result.data[0].empresasativas;
                $scope.blockProduto.stop();
                callback(result.data);
            }

        }, $rootScope.TratarErro);
    }

    $scope.ConsultaProdutos = function() {
        if(angular.element("#chartProdutos").length) {

            var chart = new CanvasJS.Chart("chartProdutos",
            {
                animationEnabled: true,
                axisX:{ gridColor: "Silver", tickColor: "silver", minimum: 0, gridThickness: 1, tickLength: 1, interval: 1 },                        
                axisY: { gridColor: "Silver", tickColor: "silver", minimum: 0, maximum: 100, gridThickness: 1, tickLength: 1, interval: 25 },
                toolTip:{ shared:false },
                theme: "theme1",
                legend:{ verticalAlign: "center", horizontalAlign: "right" },
                data: [
                    {        
                        type: "line",
                        showInLegend: true,
                        lineThickness: 2,
                        legendText: "GEPIR",
                        color: "#F08080",
                        dataPoints: [ { x: 0, y: 0 } ]
                    },
                    {        
                        type: "line",
                        showInLegend: true,
                        legendText: "Inbar",
                        color: "#20B2AA",
                        lineThickness: 2,
                        dataPoints: [ { x: 0, y: 0 } ]
                    }
                ],
                legend: {
                    cursor:"pointer",
                    fontSize: 13,
                    itemclick:function(e) {
                        e.dataSeries.visible = !(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible);
                        chart.render();
                    }
                }
            });

            $timeout(function() {
                chart.render(); 
            }, 500);  
        }
    }

    $scope.ConsultaProdutosEmPreenchimento = function() {
        $scope.blockElaboracao.start();
        genericService.postRequisicao('DashboardBO', 'ConsultaProdutosEmPreenchimento', {}).then(function (result) {

            if (result != null && result.data != null) {
                
                $scope.totalProdutosEmPreenchimento = result.data;

                if ($scope.totalProdutosEmPreenchimento > 0)
                    $scope.totalProdutosEmPreenchimento = ($scope.totalProdutosEmPreenchimento * 100) / $scope.totalProdutosCadastrados;

                $timeout(function() {
                    $("#chartPreenchimento").empty();
                    $('#chartPreenchimento').circliful();
                    $scope.blockElaboracao.stop();
                }, 1000);
            }

        }, $rootScope.TratarErro);
    }

    $scope.ConsultaTotalEtiquetas = function() {
    	var data = [];
        $scope.totaletiquetas = 0;
        $scope.blockEtiquetas.start();

        genericService.postRequisicao('DashboardBO', 'ConsultaTotalEtiquetas', {}).then(function (result) {

            if (result != null && result.data != null && result.data.length > 0) {
                for (var i = 0; i < result.data.length; i++) {
                    //data.push({ name: result.data[i].nome + ' ('+result.data[i].contador+')' ,  y: result.data[i].contador});
                    data.push({ label: result.data[i].nome,  y: result.data[i].contador, x: i, indexLabel: result.data[i].contador + ' '});
                    $scope.totaletiquetas = $scope.totaletiquetas + result.data[i].contador;
                }
            }

            if(angular.element("#chartEtiquetas").length) {

                var chart = new CanvasJS.Chart("chartEtiquetas",
	            {
                    animationEnabled: true,
                    backgroundColor: "#FFFFFF",
		            legend: {
			            verticalAlign: "bottom",
			            horizontalAlign: "center",
                        fontSize: 13
		            },
                    axisY: {
                        minimum: 0
                    },
		            theme: "theme1",
		            data: [
		                {        

			                legendText: "Tipo GTIN",
			                indexLabelFontFamily: "Garamond",       
			                indexLabelFontSize: 13,
			                indexLabelFontWeight: "bold",
			                indexLabelFontColor: "darkgrey",       
			                //indexLabelFontColor: "MistyRose",       
			                //indexLabelLineColor: "darkgrey", 
			                //indexLabelPlacement: "inside", 
			        
			                //showInLegend: true,
			        
			                dataPoints: data
		                }
		            ]
	            });
	            chart.render();
                $scope.blockEtiquetas.stop();
            }

        }, $rootScope.TratarErro);
    }

    function labelFormatter(label, series) {
		return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
	}

    $scope.exibirPesquisaSatisfacao = function(pesquisa) {
        
        var modalInstance = $modal.open({
            template: '<div class="modal-header">' +
                            '<button type="button" class="bootbox-close-button close" ng-click="cancel()">x</button>' +
                            '<h3 ng-bind="pesquisa.nome"></h3>' +
                        '</div>' +
                        '<div class="modal-body" style="margin-bottom: -13px;">' +
                            '<p ng-bind-html="pesquisa.descricao"></p><!--{{message}}-->' +
                            '<hr>' + 
                            '<div class="center" style="margin-top: 15px;">' +
                                '<button class="btn btn-info" ng-click="redirecionarPesquisa(pesquisa.url)">Responda Agora</button>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="center">' +
                                '<span>Caso não deseje visualizar esse alerta responda uma das opções</span>' +
                            '</div>' +
                            '<div class="row center" style="margin-top: 15px;">' +
                                '<button class="btn btn-info span3" ng-click="armazenarPesquisa(pesquisa, 1)">Já respondi a Pesquisa</button>' +
                                '<button class="btn btn-info span3" ng-click="armazenarPesquisa(pesquisa, 3)">Não desejo responder a Pesquisa</button>' +
                            '<div>' + 
                        '</div>',
            backdrop: 'static',
            controller: function ($scope, $modalInstance) {
                $scope.pesquisa = pesquisa;

                $scope.ok = function () {
                    $modalInstance.close();
                }

                $scope.redirecionarPesquisa = function (url) {
                    window.open(url, "_blank");
                    $modalInstance.close();
                }

                $scope.armazenarPesquisa = function (pesquisa, status) {

                    var pesquisa = angular.copy(pesquisa);
                    if(pesquisa != undefined) pesquisa.status = status;

                    genericService.postRequisicao('PesquisaSatisfacaoBO', 'CadastrarPesquisaSatisfacaoUsuario', { campos: pesquisa }).then(
                        function (result) {

                            $rootScope.verificarPesquisaSatisfacaoDisponivel();
                            $modalInstance.close();
                        },
                        $rootScope.TratarErro
                    );
                }

                $scope.cancel = function () {
                    $modalInstance.close();
                };
            }
        });

        modalInstance.result.then();
    }


    $rootScope.ConsultarMensagens = function() {

        genericService.postRequisicao('DashboardBO', 'ConsultarMensagens', {}).then(function (result) {

            if (result != null && result.data != null) {
                $scope.mensagensDisponiveis = result.data;
                $scope.totalMensagensNaoLidas = 0;
                $scope.totalMensagens = 0;
                if($scope.mensagensDisponiveis != undefined && $scope.mensagensDisponiveis != null && $scope.mensagensDisponiveis.length > 0){
                	$scope.totalMensagensNaoLidas = $scope.mensagensDisponiveis[0].totalnaolidas;
                    $scope.totalMensagens = $scope.mensagensDisponiveis[0].totalregistros;
                }
            }            

        }, $rootScope.TratarErro);
    }


    $scope.ConsultarNoticias = function() {

        genericService.postRequisicao('DashboardBO', 'ConsultarNoticias', {}).then(function (result) {

            if (result != null && result.data != null) {
                $scope.noticiasDisponiveis = result.data;
            }

        }, $rootScope.TratarErro);
    }

    $scope.ConsultarBoletos = function() {

        genericService.postRequisicao('DashboardBO', 'ConsultarBoletos', {}).then(function (result) {
            if (result != null && result.data != null) {
                $rootScope.boletosDisponiveis = [];

                if(result.data != undefined && result.data != '') {
                    for(var i = 0; i < result.data.length; i++) {
                        $rootScope.boletosDisponiveis.push({ 
                            datavencimento: result.data[i].datavencimento,
                            valortitulo: result.data[i].valortitulo,
                            tipotitulonegocio: result.data[i].tipotitulonegocio,
                            numerotitulo: result.data[i].numerotitulo,
                            numbco: result.data[i].numbco
                        });
                    }
                }
            }

        }, $rootScope.TratarErro);
    }

    $scope.ConsultarUltimoAcesso = function() {

        genericService.postRequisicao('LoginBO', 'ConsultarUltimoAcesso', {}).then(function (result) {
            if (result != null && result.data != null) {

                if(result.data != undefined && result.data != '') {
                    $rootScope.ultimoAcesso = result.data[0];
                }
            }

        }, $rootScope.TratarErro);
    }

    $scope.AbrirBoletos = function(nossonumero) {
    	var modalInstance = $modal.open({
			template: '<div class="modal-header" ><h3>Imprimir Boleto</h3></div>' +
                      '<div class="modal-body"><div></div>' +
                      'Prezado Associado,<br/><br/>Para emitir a 2ª via de seu boleto vencido, ' +
                      'copie o Nosso Número <b>{{nossonumero}}</b> e clique no botão "Abrir site do banco". Ao abrir o ' +
                      'site do banco, basta informar o número copiado e seguir os procedimentos do site para visualizar ' +
                      'o seu boleto.</div><div class="modal-footer">' +
                      '<button class="btn btn-info" ng-click="ok()">Abrir site do banco</button>' +
                      '<button class="btn btn-danger" ng-click="cancel()">Cancelar</button></div>',
			backdrop: 'static',
            controller: function ($scope, $modalInstance, $window) {
                $scope.nossonumero = nossonumero;

				$scope.ok = function () {
                    $window.open('ExternalHandler.ashx?url=https://www.itau.com.br/servicos/boletos/atualizar/', '_blank');
					$modalInstance.close();
				};
				    
				$scope.cancel = function () {
					$modalInstance.close();
				};
			}
		});
    }

    angular.element(document).ready(function () {
        $scope.onLoad();    
    });  

}]);

function Logout() {
    $.ajax({
        url: '/backoffice/logout.aspx',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function() {
        }
    });
}