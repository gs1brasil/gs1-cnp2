app.controller('LoginCtrl', ['$scope', '$modal', '$rootScope', '$timeout', '$location', 'LoginService', 'blockUI', '$filter', '$sce', 'genericService', '$cookies', '$window', 'vcRecaptchaService', 'Utils', '$translate',
    function ($scope, $modal, $rootScope, $timeout, $location, login, blockUI, $filter, $sce, genericService, $cookies, $window, vcRecaptchaService, Utils, $translate) {

        $timeout(function () {
            angular.element("#rowForm").show();
        }, 100);


        $rootScope.usuarioLogado = false;
        $scope.usuario = {
            login: null,
            senha: null,
            cpfcnpj: null
        }

        $scope.criaRecaptcha = function () {
            if (typeof (Recaptcha) != 'undefined') {
                if (Recaptcha.widget == null) {
                    Recaptcha.create(recaptchaPublicKey, "recaptcha",
                        {
                            theme: "white",
                            callback: Recaptcha.focus_response_field
                        }
                    );
                } else {
                    Recaptcha.reload();
                }
                return true;
            } else {
                return false;
            }
        };


        //TODO - comentar
        //$scope.exibeCAPTCHA = false;
        //TODO - remover comentario
        var exibeCAPTCHACookie = $cookies.exibeCAPTCHACookie;
        //var exibeCAPTCHACookie = false;

        if (exibeCAPTCHACookie == "true") {
            $scope.exibeCAPTCHA = true;
            $scope.criaRecaptcha();
        }

        $scope.exibeCPFCNPJ = false;


        $scope.onLogin = function () {

            blockUI.start();

            $scope.usuario.challenge = null;
            $scope.usuario.response = null;

            if ($scope.usuario.login !== '' && $scope.usuario.senha !== '' && $scope.usuario.login != null && $scope.usuario.senha != null) {
                if (($scope.exibeCAPTCHA && (vcRecaptchaService.getResponse() != '' && vcRecaptchaService.getResponse() != null) || !($scope.exibeCAPTCHA))) {
                    if ($scope.exibeCAPTCHA) {
                        //$scope.usuario.challenge = Recaptcha.get_challenge();
                        $scope.usuario.response = vcRecaptchaService.getResponse();
                    }

                    var dados = { where: $scope.usuario };
                    genericService.postRequisicao('LoginBO', 'Logar', dados).then(function (result) {
                        if (result.data) {
                            if (result.data.status_login) {
                                $scope.exibeCAPTCHA = false;
                                $cookies.exibeCAPTCHACookie = "";
                                $rootScope.usuarioLogado = true;

                                $rootScope.ConsultarBoletos();

                                if (history.pushState) {
                                    $window.location.href = '/backoffice/home';
                                } else {
                                    $window.location.href = '/b#/backoffice/home';
                                }
                            } else {

                                if (result.data.primeiro_acesso) {
                                    $scope.exibeCPFCNPJ = true;
                                    $scope.alert(result.data.mensagem);
                                } else {
                                    $scope.exibeCPFCNPJ = false;
                                    //TODO - Melhorar mensagem
                                    var mensagem = result.data.mensagem;
                                    if (result.data.quantidade_tentativa > 0) {
                                        $scope.exibeCAPTCHA = true;
                                        $cookies.exibeCAPTCHACookie = "true";
                                        $scope.usuario.senha = "";

                                        vcRecaptchaService.reload();
                                        /*if (!$scope.criaRecaptcha()) {
                                        $cookies.exibeCAPTCHACookie = "false";
                                        $scope.usuario.senha = "";
                                        mensagem = 'Erro ao processar Código de Validação.';
                                        }*/
                                    }
                                    $scope.alert(mensagem);
                                }
                            }
                        } else {
                            //TODO - Melhorar mensagem
                            $scope.alert('Erro ao realizar login.');
                        }
                    },
                function (result, status, headers, config) {
                    if (result.status != 419 && result.status != 420)
                        $rootScope.alert(result);
                });
                } else {

                    if (typeof (vcRecaptchaService) != 'undefined') {
                        vcRecaptchaService.reload();
                        $cookies.exibeCAPTCHACookie = "true";
                        $scope.usuario.senha = "";
                        $scope.alert('Informe o Código de Validação.');
                    } else {
                        $cookies.exibeCAPTCHACookie = "false";
                        $scope.usuario.senha = "";
                        $scope.alert('Erro ao processar Código de Validação.');
                    }
                }
            } else {
                $scope.alert('Informe um Email e Senha válidos.');
            }
            blockUI.stop();
        }

        $rootScope.ConsultarBoletos = function () {

            genericService.postRequisicao('DashboardBO', 'ConsultarBoletos', {}).then(function (result) {
                if (result != null && result.data != null) {
                    $rootScope.boletosDisponiveis = [];

                    if (result.data != undefined && result.data != '') {
                        for (var i = 0; i < result.data.length; i++) {
                            $rootScope.boletosDisponiveis.push({
                                valortitulo: result.data[i].valortitulo,
                                url: result.data[i].url,
                                datavencimento: result.data[i].datavencimento
                            });
                        }
                    }
                }

            }, $rootScope.TratarErro);
        }


        $scope.enviarEmail = function () {
            if ($scope.usuario.email != "" && $scope.usuario.email != undefined) {
                if ($scope.formRecuperar.$valid == false) {
                    $scope.alert('Endereço de e-mail incorreto.');
                }
                else {
                    blockUI.start();
                    var dados = { campos: { email: $scope.usuario.email} };

                    genericService.postRequisicao('LoginBO', 'Recupera', dados).then(function (result) {
                        blockUI.stop();
                        if (result.data == 0) {
                            $rootScope.alert('Endereço de e-mail não pertence a um usuário ativo.');
                        }
                        else if (result.data == 2) {
                            $rootScope.alert('Endereço de e-mail não pertence a um usuário ativo.');
                        }
                        else if (result.data == 3) {
                            $rootScope.alert('Erro ao enviar e-mail.');
                        }
                        else if (result.data == 4) {
                            var dados = { where: 'bo.nu.telefone' };

                            genericService.postRequisicao('LoginBO', 'CarregarParametrosLogin', dados).then(function (result) {
                                if (result.data.length > 0)
                                    $scope.telefone = result.data[0].valor;

                                $rootScope.alert('Acesso negado. Por favor, entre em contato com o atendimento GS1 Brasil. Tel. ' + $scope.telefone + ' ou através do nosso site.');
                            });
                        }
                        else if (result.data == 5) {
                            var dados = { where: 'bo.nu.telefone' };

                            genericService.postRequisicao('LoginBO', 'CarregarParametrosLogin', dados).then(function (result) {
                                if (result.data.length > 0)
                                    $scope.telefone = result.data[0].valor;

                                $scope.alert("Operação não permitida. Por favor, entre em contato com a GS1 Brasil pelo tel. " + $scope.telefone + ".");
                            });

                        }
                        else {
                            $rootScope.alert('Verifique seu e-mail. Em breve você receberá a nova senha. Caso não receba o e-mail, entrar em contato com a GS1 Brasil.', function () {
                                $scope.viewLogin = true;
                                $scope.viewForgot = false;
                            }, 'Alerta');
                        }
                    },
                function (result, status, headers, config) {
                    blockUI.stop();
                    if (result.status != 419 && result.status != 420)
                        $rootScope.alert(result);
                });
                }
            }
            else {
                $scope.alert('Informe um email para prosseguir.');
            }
        }

        $scope.onClean = function () {
            $scope.usuario = {};
            $scope.exibeCPFCNPJ = false;
        }

        //TODO - melhorar tratameto
        $rootScope.alert = function (data, sucessCallback, title) {
            var message = "";

            if (data.InnerException != null) {
                if (data.InnerException.Message != null) {
                    message = data.InnerException.Message;
                } else {
                    message = data;
                }
            } else {
                if (data.Message != null) {
                    message = data.Message;
                } else {
                    if (data != null) {
                        message = data;
                    } else {
                        message = "Erro ao processar requisição";
                    }
                }
            }

            var modalInstance = $modal.open({
                template: ' <div class="modal-header"><h3>{{title}}</h3></div><div class="modal-body"><p ng-bind-html="message"></p><!--{{message}}--></div><div class="modal-footer"><button class="btn btn-info" ng-click="ok()">OK</button></div>',
                controller: function ($scope, $modalInstance) {
                    $scope.message = message;
                    $scope.title = "Alerta";
                    $translate('38.MENSAGEM_Alerta').then(function (resultado) {
                        $scope.title = title || resultado || "Alerta";
                    });
                    $scope.ok = function () {
                        $modalInstance.close();
                    };
                }
            });

            modalInstance.result.then(sucessCallback);

        };

        //Busca Perguntas Frequentes
        $scope.buscarFAQs = function (codigoidioma) {
            if (codigoidioma != null && codigoidioma != undefined) {
                genericService.postRequisicao('LoginBO', 'BuscarFAQs', { "campos": { "codigoidioma": codigoidioma} }).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.faqs = result.data;

                            for (var i in $scope.faqs) {

                                if ($scope.faqs[i] != undefined && $scope.faqs[i] != '') {
                                    Utils.EmbedYoutube($scope.faqs[i].video, function (data) {
                                        $scope.faqs[i].video = data;
                                    });
                                }
                            }

                        }
                    },
                        $rootScope.TratarErro
                    );
            }
        }

        //Busca os Idiomas do Sistema
        $scope.buscaIdiomas = function () {
            genericService.postRequisicao('IdiomaBO', 'BuscarIdiomas', {}).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        $scope.idiomas = result.data;
                        $scope.codigoidioma = 1;
                        $scope.nomeidioma = "pt-br";
                        $scope.buscarFAQs($scope.codigoidioma);
                    }
                },
                $rootScope.TratarErro
            );
        }

        //Chamado ao alterar o idioma
        $scope.alteraIdioma = function (codigoidioma, nomeidioma) {
            $scope.codigoidioma = codigoidioma;
            $scope.nomeidioma = nomeidioma;
            $scope.buscarFAQs(codigoidioma);
        }

        //Renderizar HTML na página
        $scope.renderHtml = function (htmlItem) {
            return $sce.trustAsHtml(htmlItem);
        };

        $scope.buscaIdiomas();

    } ]);