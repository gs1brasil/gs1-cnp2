﻿'use strict';

var app = angular.module('CNP.Backoffice', [
    'ngCookies',    
    'ngSanitize',
    'ui.bootstrap', //http://angular-ui.github.io/bootstrap/,
    'blockUI'
]);


app.config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($q, $location, $injector, $rootScope) {
        return {
            'responseError': function (response) {
                if (response.status === 401) {
                    //window.location = '/login.aspx';
                    window.location = '/';
                } else if (response.status === 403) {
                    $location.path('/page/403');
                } else if (response.status === 404) {
                    $location.path('/views/404.html');
                } else if (response.status === 419) {
                    //$rootScope.alert(response.data.Message + "<br />Data Atual: " + response.data.DateOccurrence);
                    var message = response.data.Message + '<br />Data Atual: ' + response.data.DateOccurrence + '<br /><br />Stack Trace: <br/><div>' + response.data.StackTrace + '</div>';

                    $rootScope.alert(message);
                } else if (response.status === 420) {
                    //$rootScope.alert(response.data.Message);
                    $rootScope.alert(response.data.Message);
                }

                $rootScope.$$phase || $rootScope.$apply();

                return $q.reject(response);
            }
        };
    });
});

