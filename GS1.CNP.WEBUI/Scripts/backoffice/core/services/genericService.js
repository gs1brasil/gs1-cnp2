﻿app.factory('genericService', ['$q', '$http', '$location', function ($q, $http, $location) {
    var url = '/requisicao.ashx';

    return {
        //        getRequisicao: function (bo, operacao, params, callback) {
        //            $http.get(url + '/' + bo + '/' + operacao, params).success(function (data) {
        //                callback(data);
        //            }).error(function (data, status, headers, config) {
        //                $rootScope.alert(data);
        //            });
        //        },

        getRequisicao: function (bo, operacao, dados) {
            //var dados = { campos: asaus, where: params };
            return $http.get(url + '/' + bo + '/' + operacao, dados);
        },
        postRequisicao: function (bo, operacao, dados) {
            //var dados = { where: params };
            return $http.post(url + '/' + bo + '/' + operacao, dados);
        }
    };
} ]);