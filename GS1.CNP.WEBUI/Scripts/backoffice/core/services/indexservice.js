app.factory('IndexService', ['$q', '$http', '$rootScope', function ($q, $http, $rootScope) {
    var url = '/requisicao.ashx';

    var indexService = [];

    indexService.RetornaUsuarioLogado = function (data, callback) {
        var dados = "";

        $http.post(url + '/LoginBO/RetornaUsuarioLogado', dados).success(function (data) {
            callback(data);
        }).error(function (data, status, headers, config) {
            if (!typeof data === 'object')
                $rootScope.alert(data);
        });
    };

    indexService.VerificaUsuarioLogado = function (data, callback) {
        var dados = "";

        //        $http.post(url + '/LoginBO/VerificaUsuarioLogado', dados).success(function (data) {
        //            callback(data);
        //        }).error(function (data, status, headers, config) {
        //            $rootScope.alert(data);
        //        });

        $http({ method: 'POST', url: url + '/LoginBO/VerificaUsuarioLogado', data: dados, responseType: 'string' })

        .then(function (data) {
            callback(data);
        }, function (data, status, headers, config) {
            if (!typeof data === 'object')
                $rootScope.alert(data);
        });



    };

    indexService.RetornaModulosUsuarioLogado = function (data, callback) {
        var dados = "";

        $http.post(url + '/LoginBO/RetornaModulosUsuarioLogado', dados).success(function (data) {
            callback(data);
        }).error(function (data, status, headers, config) {
            if (!typeof data === 'object')
                $rootScope.alert(data);
        });
    };

    indexService.VerificaPermissao = function (nomeBO, nomeMethod, callback) {
        var dados = { campos: { nomeBO: nomeBO, nomeMethod: nomeMethod} };

        //$http.post('/views/backoffice/interna.aspx/VerificaPermissoes', dados).success(function (result) {
        $http.post(url + '/LoginBO/VerificaPermissoes', dados).success(function (result) {
            if (result != undefined && result != null && result == true) {
                callback();
            } else {
                $rootScope.alert('Sem permissão para acessar esta funcionalidade.');
            }
        });
    };

    indexService.CarregaParametros = function (data, callback) {

        $http.post(url + '/LoginBO/CarregarParametrosLogin', null).success(function (result) {
            if (result != undefined && result != null) {
                callback(result);
            } else {
                $rootScope.alert('Sem permissão para acessar esta funcionalidade.');
            }
        });
    };

    return indexService;
} ]);
