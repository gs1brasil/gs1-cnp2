﻿app.factory('LoginService', ['$q', '$http', '$rootScope',  function ($q, $http, $rootScope) {
    var url = '/requisicao.ashx';

    return {
        logarUsuario: function (params, callback) {
            var dados = { where: params };

            $http.post(url + '/LoginBO/Logar', dados).success(function (data) {
                callback(data);
            }).error(function (data, status, headers, config) {
                $rootScope.alert(data);
            });
        },

        CarregarParametrosLogin: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/LoginBO/CarregarParametrosLogin', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        enviarEmail: function (params, callback) {
            var dados = { campos: { email: params.email} }

            $http.post(url + '/LoginBO/Recupera', dados).success(function (data) {
                callback(data);
            }).error(function (data, status, headers, config) {
                $rootScope.alert(data);
            });
        }
    };
} ]);
