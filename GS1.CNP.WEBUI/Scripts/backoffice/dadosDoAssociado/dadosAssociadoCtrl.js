app.controller('DadosAssociado', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {

        //inicializando abas do form
        $('#tabs-associado a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $scope.usuarioGS1 = false;
        genericService.postRequisicao('LoginBO', 'RetornaUsuarioLogado', {}).then(
            function (result) {
                $scope.usuarioGS1 = result.data && result.data.id_tipousuario === 1;
            },
            $rootScope.TratarErro
        );

        var titulo = $translate.instant('11.LBL_DadosAssociado');
        $scope.titulo = typeof titulo === 'object' || titulo == '11.LBL_DadosAssociado' ? 'Dados do Associado' : titulo;

        $scope.codigoAssociado = undefined;
        $scope.isPesquisa = false;

        $scope.tableDadosAssociado = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                nome: 'asc'
            }
        }, {

            counts: [],
            total: 0,
            getData: function ($defer, params) {

                var ordenacao = params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '') + ' ';
                ordenacao = ordenacao + params.sorting()[params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '')].toString();

                var dados = carregaGrid($scope.item, $scope.isPesquisa, params.page(), $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'), ordenacao, function (data) {

                    if (data && data.length > 0) {
                        params.total(data[0].totalregistros);
                    }
                    if (data.length == 0) {
                        params.total(0);
                    }
                    $defer.resolve(data);
                    $scope.items = data;
                });

            }
        });

        $scope.tableRepresentantesLegais = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                nome: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if ($scope.codigoAssociado != undefined) {
                    var dados = buscaRepresentantesLegais($scope.codigoAssociado, function (data) {
                        params.count(data.length);
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData)
                        $scope.representantesLegais = data;
                    });
                }
            }
        });

        $scope.tableLicencas = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                nome: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if ($scope.codigoAssociado != undefined) {
                    var dados = buscaLicencas($scope.codigoAssociado, function (data) {
                        params.count(data.length);
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData)
                        $scope.licencas = data;
                    });
                }
            }
        });


        $scope.tableUsuarios = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                nome: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if ($scope.codigoAssociado != undefined) {
                    var dados = buscaUsuarios($scope.codigoAssociado, function (data) {
                        params.count(data.length);
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData)
                        $scope.usuariosassociado = data;
                    });
                }
            }
        });

        function carregaGrid(item, isPesquisa, paginaAtual, registroPorPagina, ordenacao, callback) {
            var dados = {};

            if (isPesquisa) {
                dados = angular.copy(item);

                genericService.postRequisicao('DadosDoAssociadoBO', 'PesquisaDadosDoAssociado', { campos: dados, paginaAtual: paginaAtual, registroPorPagina: registroPorPagina, ordenacao: ordenacao }).then(
                    function (result) {

                        $scope.form.submitted = false;
                        $scope.editMode = false;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            }
            else {
                genericService.postRequisicao('DadosDoAssociadoBO', 'BuscaDadosDoAssociado', { campos: dados, paginaAtual: paginaAtual, registroPorPagina: registroPorPagina, ordenacao: ordenacao }).then(
                    function (result) {

                        $scope.empresasAssociadas = result.data;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            }
        };

        function buscaRepresentantesLegais(associado, callback) {
            genericService.postRequisicao('DadosDoAssociadoBO', 'BuscaRepresentantesLegais', { campos: { codigoAssociado: associado} }).then(
                function (result) {

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        function buscaLicencas(associado, callback) {
            genericService.postRequisicao('DadosDoAssociadoBO', 'BuscaLicencas', { campos: { codigoAssociado: associado} }).then(
                function (result) {

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        function buscaUsuarios(associado, callback) {
            genericService.postRequisicao('DadosDoAssociadoBO', 'BuscaUsuarios', { campos: { codigoAssociado: associado} }).then(
                function (result) {

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        //Carrega dropdown de TiposCompartilhamento
        function carregaDropDown() {
            genericService.postRequisicao('DadosDoAssociadoBO', 'BuscarTipoCompartilhamento', null).then(
                function (result) {
                    $scope.tiposCompartilhamento = result.data;
                },
                $rootScope.TratarErro
            );
        }
        carregaDropDown();

        $scope.onFormMode = function (item) {
            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = true;
            $('#form :input:first').focus();
        };

        $scope.onEdit = function (item) {
            $scope.item = angular.copy(item);

            var titulo = $translate.instant('11.LBL_AlterarDadosAssociado');
            $scope.titulo = typeof titulo === 'object' || titulo == '11.LBL_AlterarDadosAssociado' ? 'Alterar Dados do Associado' : titulo;

            Utils.bloquearCamposVisualizacao("VisualizarDadosAssociado");

            $scope.codigoAssociado = $scope.item.codigo;
            $scope.tableDadosAssociado.page(1);
            $scope.tableRepresentantesLegais.reload();

            $('#tabs-associado a:first').tab('show');
            $scope.tableLicencas.page(1);
            $scope.tableLicencas.reload();

            $scope.tableUsuarios.page(1);
            $scope.tableUsuarios.reload();

            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = true;
            $('#form :input:first').focus();
        };

        $scope.onCancel = function (item) {
            $scope.item = {};
            var titulo = $translate.instant('11.LBL_DadosAssociado');
            $scope.titulo = typeof titulo === 'object' || titulo == '11.LBL_DadosAssociado' ? 'Dados do Associado' : titulo;

            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $('#tabs-associado a:first').tab('show');
            Utils.moveScrollTop();
            $scope.tableDadosAssociado.page(1);
            $scope.tableDadosAssociado.reload();
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        //Pesquisa Associados cadastrados
        $scope.onSearch = function (item) {
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableDadosAssociado.page(1);
            $scope.tableDadosAssociado.reload();
        };

        $scope.onSearchMode = function (item) {
            var titulo = $translate.instant('11.LBL_PesquisarAssociado');
            $scope.titulo = typeof titulo === 'object' || titulo == '11.LBL_PesquisarAssociado' ? 'Pesquisar Associado' : titulo;

            Utils.desbloquearCamposVisualizacaoPesquisa();
            $('#tabs-associado a:first').tab('show');
            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;

            genericService.postRequisicao('StatusAssociadoBO', 'buscarStatusAssociado', {}).then(
                function (result) {
                    $scope.statusassociados = result.data;
                    angular.element('#formcontainer').focus();
                },
                $rootScope.TratarErro
            );
            
        };

        $scope.onClean = function (item) {
            $scope.item = {};
        };

        $scope.onSave = function (item) {
            if (!$scope.form.$valid) {
                $('#tabs-associado a:first').tab('show');
                var mensagem = $translate.instant('11.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '11.MENSAGEM_CamposObrigatorios' ? 'Campos obrigat�rios n�o preenchidos ou inv�lidos.' : mensagem);

                $scope.form.submitted = true;
            }
            else {
                genericService.postRequisicao('DadosDoAssociadoBO', 'AtualizarTipoCompartilhamento', { campos: { codigo: item.codigo, tipoCompartilhamento: item.tipocompartilhamentocodigo, ativado: item.ativado, aprovacao: item.aprovacao} }).then(
                    function (result) {
                        $scope.item = {};
                        $scope.editMode = false;
                        $scope.isPesquisa = false;
                        $scope.form.submitted = false;
                        $scope.exibeVoltar = false;
                        $('#tabs-associado a:first').tab('show');
                        Utils.moveScrollTop();
                        $scope.tableDadosAssociado.page(1);
                        $scope.tableDadosAssociado.reload();
                        var mensagem = $translate.instant('11.MENSAGEM_RegistroAlteradoSucesso');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '11.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                    },
                    $rootScope.TratarErro
                );

            }
        }
    } ]);
