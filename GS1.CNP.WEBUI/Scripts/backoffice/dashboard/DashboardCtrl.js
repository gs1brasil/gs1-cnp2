﻿﻿app.controller('DashboardCtrl', ['$rootScope', '$scope', '$filter', '$timeout', 'Utils', 'genericService', '$modal', 'blockUI',
 function ($rootScope, $scope, $filter, $timeout, Utils, http, $modal, blockUI) {

    function initialize() {
        ConsultaBlocoProdutos(function(data) {
            ConsultaProdutosEmPreenchimento();
        });

        //ConsultaTotalProdutosCadastrados(function(data) {
        //    ConsultaProdutosEmPreenchimento();
        //});
        //ConsultaTotalEmpresasAtivas();
        //ConsultaTotalProdutosPublicados();
        //ConsultaTotalProdutosDesativados();

        ConsultaProdutos();
        ConsultaTotalEtiquetas();

        //ConsultarMensagens();
    }

    function ConsultaBlocoProdutos(callback) {

        var myBlock = blockUI.instances.get('blockProdutos');
        myBlock.start();

        http.postRequisicao('DashboardBO', 'ConsultaBlocoProdutos', {}).then(function (result) {
            
            myBlock.stop();

            if (result != null && result.data != null) {
                $scope.totalProdutosCadastrados = result.data[0].produtoscadastrados;
                $scope.totalProdutosPublicados = result.data[0].produtospublicados;
                $scope.totalProdutosDesativados = result.data[0].produtosdesativados;
                $scope.totalEmpresasAtivas = result.data[0].empresasativas;
                callback(result.data);
            }

        }, $rootScope.TratarErro);
    }

//    function ConsultaTotalProdutosCadastrados(callback) {

//        http.postRequisicao('DashboardBO', 'ConsultaTotalProdutosCadastrados', {}).then(function (result) {

//            if (result != null && result.data != null) {
//                $scope.totalProdutosCadastrados = result.data;
//                callback(result.data);
//            }

//        }, $rootScope.TratarErro);
//    }

//    function ConsultaTotalProdutosPublicados() {

//        http.postRequisicao('DashboardBO', 'ConsultaTotalProdutosPublicados', {}).then(function (result) {

//            if (result != null && result.data != null) {
//                $scope.totalProdutosPublicados = result.data;
//            }

//        }, $rootScope.TratarErro);

//    }

//    function ConsultaTotalProdutosDesativados() {

//        http.postRequisicao('DashboardBO', 'ConsultaTotalProdutosDesativados', {}).then(function (result) {

//            if (result != null && result.data != null) {
//                $scope.totalProdutosDesativados = result.data;
//            }

//        }, $rootScope.TratarErro);

//    }

    function ConsultaProdutos() {
        var chart = new CanvasJS.Chart("chartProdutos",
        {
            animationEnabled: true,
            axisX:{ gridColor: "Silver", tickColor: "silver", minimum: 0, gridThickness: 1, tickLength: 1, interval: 1 },                        
            axisY: { gridColor: "Silver", tickColor: "silver", minimum: 0, maximum: 100, gridThickness: 1, tickLength: 1, interval: 25 },
            toolTip:{ shared:false },
            theme: "theme1",
            legend:{ verticalAlign: "center", horizontalAlign: "right" },
            data: [
                {        
                    type: "line",
                    showInLegend: true,
                    lineThickness: 2,
                    legendText: "GEPIR",
                    color: "#F08080",
                    dataPoints: [ { x: 0, y: 0 } ]
                },
                {        
                    type: "line",
                    showInLegend: true,
                    legendText: "Inbar",
                    color: "#20B2AA",
                    lineThickness: 2,
                    dataPoints: [ { x: 0, y: 0 } ]
                }
            ],
            legend: {
                cursor:"pointer",
                fontSize: 13,
                itemclick:function(e) {
                    e.dataSeries.visible = !(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible);
                    chart.render();
                }
            }
        });

        $timeout(function() {
            chart.render(); 
        }, 500);  
    }

    function ConsultaProdutosEmPreenchimento() {
        http.postRequisicao('DashboardBO', 'ConsultaProdutosEmPreenchimento', {}).then(function (result) {

            if (result != null && result.data != null) {
                
                $scope.totalProdutosEmPreenchimento = result.data;

                if ($scope.totalProdutosEmPreenchimento > 0)
                    $scope.totalProdutosEmPreenchimento = ($scope.totalProdutosEmPreenchimento * 100) / $scope.totalProdutosCadastrados;

                $timeout(function() {
                    $('#chartPreenchimento').circliful();
                }, 1000);
            }

        }, $rootScope.TratarErro);
    }

    function ConsultaTotalEtiquetas() {
    	var data = [];
        $scope.totaletiquetas = 0;

        http.postRequisicao('DashboardBO', 'ConsultaTotalEtiquetas', {}).then(function (result) {

            if (result != null && result.data != null && result.data.length > 0) {
                for (var i = 0; i < result.data.length; i++) {
                    //data.push({ name: result.data[i].nome + ' ('+result.data[i].contador+')' ,  y: result.data[i].contador});
                    data.push({ label: result.data[i].nome,  y: result.data[i].contador, x: i, indexLabel: result.data[i].contador + ' '});
                    $scope.totaletiquetas = $scope.totaletiquetas + result.data[i].contador;
                }
            }

            var chart = new CanvasJS.Chart("chartEtiquetas",
	        {
                animationEnabled: true,
                backgroundColor: "#FFFFFF",
		        legend: {
			        verticalAlign: "bottom",
			        horizontalAlign: "center",
                    fontSize: 13
		        },
                axisY: {
                    minimum: 0
                },
		        theme: "theme1",
		        data: [
		            {        

			            legendText: "Tipo GTIN",
			            indexLabelFontFamily: "Garamond",       
			            indexLabelFontSize: 13,
			            indexLabelFontWeight: "bold",
			        
			            indexLabelFontColor: "MistyRose",       
			            indexLabelLineColor: "darkgrey", 
			            indexLabelPlacement: "inside", 
			        
			            showInLegend: true,
			        
			            dataPoints: data
		            }
		        ]
	        });
	        chart.render();

        }, $rootScope.TratarErro);
    }

    function labelFormatter(label, series) {
		return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
	}


//    function ConsultaTotalEmpresasAtivas(callback) {

//        if($scope.usuarioLogado == undefined || $scope.usuarioLogado == null){

//            http.postRequisicao('UsuarioBO', 'BuscarUsuarioLogado', '').then(
//                    function (result) {
//                        $scope.usuarioLogado = result.data;

//                        if ($scope.usuarioLogado.id_tipousuario == 1) {
//                            $scope.typeUser = 'GS1';
//                        } else if ($scope.usuarioLogado.id_tipousuario == 2) {
//                            $scope.typeUser = 'Associado';
//                        }

//                        if($scope.usuarioLogado.id_tipousuario == 1){
//                            http.postRequisicao('DashboardBO', 'ConsultaTotalEmpresasAtivas', {}).then(function (result) {

//                                if (result != null && result.data != null) {
//                                    $scope.totalEmpresasAtivas = result.data;
//                                    //callback(result.data);
//                                }

//                            }, $rootScope.TratarErro);
//                        }
//                    },
//                    $rootScope.TratarErro
//                );

//        }else{

//            if($scope.usuarioLogado.id_tipousuario == 1){
//                http.postRequisicao('DashboardBO', 'ConsultaTotalEmpresasAtivas', {}).then(function (result) {

//                    if (result != null && result.data != null) {
//                        $scope.totalEmpresasAtivas = result.data;
//                        //callback(result.data);
//                    }

//                }, $rootScope.TratarErro);
//            }
//        }
//    }

    $scope.exibirPesquisaSatisfacao = function(pesquisa) {
        
        var modalInstance = $modal.open({
            template: '<div class="modal-header">' +
                            '<button type="button" class="bootbox-close-button close" ng-click="cancel()">x</button>' +
                            '<h3 ng-bind="pesquisa.nome"></h3>' +
                        '</div>' +
                        '<div class="modal-body" style="margin-bottom: -13px;">' +
                            '<p ng-bind-html="pesquisa.descricao"></p><!--{{message}}-->' +
                            '<hr>' + 
                            '<div class="center" style="margin-top: 15px;">' +
                                '<button class="btn btn-info" ng-click="redirecionarPesquisa(pesquisa.url)">Responda Agora</button>' +
                            '</div>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                            '<div class="center">' +
                                '<span>Caso não deseje visualizar esse alerta responda uma das opções</span>' +
                            '</div>' +
                            '<div class="row center" style="margin-top: 15px;">' +
                                '<button class="btn btn-info span3" ng-click="armazenarPesquisa(pesquisa, 1)">Já respondi a Pesquisa</button>' +
                                '<button class="btn btn-info span3" ng-click="armazenarPesquisa(pesquisa, 3)">Não desejo responder a Pesquisa</button>' +
                            '<div>' + 
                        '</div>',
            backdrop: 'static',
            controller: function ($scope, $modalInstance) {
                $scope.pesquisa = pesquisa;

                $scope.ok = function () {
                    $modalInstance.close();
                }

                $scope.redirecionarPesquisa = function (url) {
                    window.open(url, "_blank");
                    $modalInstance.close();
                }

                $scope.armazenarPesquisa = function (pesquisa, status) {

                    var pesquisa = angular.copy(pesquisa);
                    if(pesquisa != undefined) pesquisa.status = status;

                    http.postRequisicao('PesquisaSatisfacaoBO', 'CadastrarPesquisaSatisfacaoUsuario', { campos: pesquisa }).then(
                        function (result) {

                            $rootScope.verificarPesquisaSatisfacaoDisponivel();
                            $modalInstance.close();
                        },
                        $rootScope.TratarErro
                    );
                }

                $scope.cancel = function () {
                    $modalInstance.close();
                };
            }
        });

        modalInstance.result.then();
    }


    function ConsultarMensagens() {

        http.postRequisicao('DashboardBO', 'ConsultarMensagens', {}).then(function (result) {

            if (result != null && result.data != null) {
                $scope.mensagensDisponiveis = result.data;
            }

        }, $rootScope.TratarErro);

    }

    initialize();

}]);