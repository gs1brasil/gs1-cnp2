app.controller('detalhesAjudaCtrl', ['$scope', '$rootScope', '$routeParams', 'genericService', 'Utils', 'blockUI', '$sce', '$location',
    function ($scope, $rootScope, $routeParams, genericService, Utils, blockUI, $sce, $location) {

        $scope.pageUrl = window.location.href;
        $scope.codigoAjuda = $scope.pageUrl.split('/')[$scope.pageUrl.split('/').length - 1].split('.')[0];

        //Busca informa��es da tela
        $scope.onLoadPage = function (callback) {

            genericService.postRequisicao('DetalhesAjudaBO', 'BuscarDados', { campos: { codigo: $scope.codigoAjuda} }).then(
                function (result) {
                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        };

        //Carregamento Inicial
        $scope.onLoadPage(function (retorno) {

            if (retorno != null && retorno.length > 0) {
                $scope.item = retorno[0];

                if ($scope.item != undefined && $scope.item != '') {
                    Utils.EmbedYoutube($scope.item.urlvideo, function (data) {
                        $scope.item.urlvideo = data;
                    });
                }
            }
            else {
                window.location = "/backoffice/home";
            }
        });

        //Renderizar HTML na p�gina
        $scope.renderHtml = function (htmlItem) {
            return $sce.trustAsHtml(htmlItem);
        };

    } ]);
