app.controller('detalhesAjudaPassoaPassoCtrl', ['$scope', '$rootScope', '$routeParams', 'genericService', 'Utils', 'blockUI', '$sce', '$location', '$localStorage',
    function ($scope, $rootScope, $routeParams, genericService, Utils, blockUI, $sce, $location, $localStorage) {

        $scope.pageUrl = window.location.href;
        $scope.codigo = $scope.pageUrl.split('/')[$scope.pageUrl.split('/').length - 1].split('.')[0];

        //Busca informa��es da tela
        $scope.onLoadPage = function (callback) {

            if ($localStorage != undefined) {
                genericService.postRequisicao('DetalhesAjudaPassoaPassoBO', 'BuscarAjudasPassoAPasso', { campos: { codigo: $scope.codigo, codigoidioma: $localStorage.codigoidioma} }).then(
                    function (result) {

                        if (result.data != undefined && result.data != null && result.data.length > 0) {
                            $scope.ajudas = result.data[0];
                            callback(result.data);
                        }
                        else {
                            callback();
                        }
                    },
                    $rootScope.TratarErro
                );
            }
        };

        //Carregamento Inicial
        $scope.onLoadPage(function (retorno) {

            if (retorno != null && retorno.length > 0) {
                $scope.item = retorno[0];

                if ($scope.item != undefined && $scope.item != '') {
                    Utils.EmbedYoutube($scope.item.video, function (data) {
                        $scope.item.video = data;
                    });
                }
            }
            else {
                window.location = "/backoffice/home";
            }
        });

        //Renderizar HTML na p�gina
        $scope.renderHtml = function (htmlItem) {
            return $sce.trustAsHtml(htmlItem);
        };

    } ]);
