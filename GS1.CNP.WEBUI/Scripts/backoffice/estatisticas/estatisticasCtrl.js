﻿app.controller('EstatisticasCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate', 'uiGmapIsReady', '$q',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate, IsReady, $q) {
    //inicializando abas do form
    $('#tabs-estatisticas a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var titulo = $translate.instant('57.LBL_EstatisticasGepirInbar');
    $scope.titulo = typeof titulo === 'object' || titulo == '57.LBL_EstatisticasGepirInbar' ? 'Estatísticas GEPIR e Inbar' : titulo;

    $scope.tableEstatisticasGepir = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridEstatisticasGepir(function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(data);
                $scope.items = data;
            });
        }
    });

    function carregaGridEstatisticasGepir(callback) {

        $rootScope.verificaAssociadoSelecionado(function (data) {

            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                genericService.postRequisicao('EstatisticaBO', 'BuscarEstatisticasGepir', { campos: {} }).then(
                    function (result) {

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );

                $rootScope.flagAssociadoSelecionado = true;
            } else {
                $rootScope.flagAssociadoSelecionado = false;
            }
        });

    };

    $scope.tableEstatisticasInbar = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridEstatisticasInbar(function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                $scope.items = data;
            });
        }
    });

    function carregaGridEstatisticasInbar(callback) {

        $rootScope.verificaAssociadoSelecionado(function (data) {

            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                genericService.postRequisicao('EstatisticaBO', 'BuscarEstatisticasInbar', { campos: {} }).then(
                    function (result) {

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );

                $rootScope.flagAssociadoSelecionado = true;
            } else {
                $rootScope.flagAssociadoSelecionado = false;
            }
        });

    };

    //Google Maps API
    $scope.map = {
        center: {
            latitude: -22.16980650917504,
            longitude: -22.16980650917504
        },
        zoom: 2,
        dragging: false,
        bounds: {},
        markers: [],
        idkey: 'place_id',
        control: {},
        events: {}//,
//        markersEvents: {
//            click: function (marker, eventName, model) {
//                //$scope.map.panTo(this.getPosition());
//                $scope.map.center.latitude = this.model.latitude;
//                $scope.map.center.longitude = this.model.longitude;
//                $scope.map.zoom = 16;
//            }
//        }
    };


    $scope.options = { scrollwheel: false };

    $scope.limparMarkers = function () {
        var mensagem = $translate.instant('57.MENSAGEM_MapaSeraRemovido');
        $scope.confirm(typeof mensagem === 'object' || mensagem == '57.MENSAGEM_MapaSeraRemovido' ? 'O mapa será removido da localização física no CNP. Deseja realmente limpar a localização do mapa?' : mensagem, function () {
            $scope.map.markers = {};
            $scope.map.center.latitude = -22.16980650917504;
            $scope.map.center.longitude = -22.16980650917504;
            $scope.item.latitude = "";
            $scope.item.longitude = "";
            $scope.map.zoom = 2;
        });
    }

    $scope.displayed = false;
    $scope.showMap = function () {
        $scope.displayed = true;
        IsReady.promise().then(function (maps) {
            var center = maps[0].map.getCenter();
            google.maps.event.trigger(maps[0].map, "resize");
            maps[0].map.setCenter(center);
        });

        $scope.buscarMarcadores().then(function (marcadores) {
            $scope.plotarMarcadores(marcadores);
        });
    }

    $scope.buscarMarcadores = function () {
        return $q(function (resolve, reject) {
            genericService.postRequisicao('EstatisticaBO', 'InformacoesMapa', { campos: {} }).then(
                function (result) {
                    resolve(result.data);
                },
                $rootScope.TratarErro
            );
        });
    }

    $scope.showInfoWindow = function (marker, eventName, model) {
        model.show = !model.show;
    };

    $scope.plotarMarcadores = function (marcadores) {

        var newMarkers = [];

        angular.forEach(marcadores, function (item, index) {
            //$scope.map.center.latitude = item.latitude;
            //$scope.map.center.longitude = item.longitude;
            //$scope.map.zoom = 16;

            var marker = {
                id: index,
                place_id: index,
                name: '',
                latitude: item.latitude,
                longitude: item.longitude,
                status: item.situacao,
                options: {
                    visible: false
                },
                templateurl: 'window.tpl.html',
                templateparameter: {}
            };

            if (item.situacao == 'ExistentesCNP') {
                marker.icon = '/images/markerExistente.png';
            }
            else {
                marker.icon = '/images/markerNaoExistente.png';
            }

            newMarkers.push(marker);
        });

        $scope.map.markers = newMarkers;
        var bounds = new google.maps.LatLngBounds();
        //console.log(bounds.getCenter());
        //$scope.map.setCenter(bounds.getCenter());
        //$scope.map.center.latitude = bounds.getCenter().lat();
        //$scope.map.center.longitude = bounds.getCenter().lng();
        $scope.map.zoom = 2;
    }

} ]);

 