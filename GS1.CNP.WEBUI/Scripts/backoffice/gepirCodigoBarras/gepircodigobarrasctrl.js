app.controller('GepirCodigoBarrasCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$log', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $log, $modal, genericService, Utils, blockUI, $q, $translate) {

    //inicializando abas do form
    $('#tabs-ajuda a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var titulo = $translate.instant('66.LBL_ConsultarCodigoBarras');
    $scope.titulo = typeof titulo === 'object' || titulo == '66.LBL_ConsultarCodigoBarras' ? 'Consultar por Código de Barras - GTIN' : titulo;
    $scope.form = {};
    $scope.itemForm = {};
    $scope.initMode = true;
    $scope.showInputs = false;

    $scope.tableCampos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if (!$scope.initMode) {
                var dados = carregaGridCampos($scope.itemForm, function (data) {
                    $scope.consultaefetuada = true;
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        }
    });

    $scope.tableCampos.settings().$scope = $scope;

    //Popula grid de campos
    function carregaGridCampos(item, callback) {

        if (item.tipoconsulta == 1) { //Dono do Item
            item.key = "GTIN";
            item.value = item.gtin;
            genericService.postRequisicao('GepirRouterBO', 'getKeyLicensee', { campos: item }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        callback(result.data.gepirParty == null ? [] : result.data.gepirParty);
                    }
                },
                $rootScope.TratarErro
            );
        } else { // Item

            genericService.postRequisicao('GepirRouterBO', 'ConsultarWSItemByGTIN', { campos: item }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        callback(result.data.gepirItem == null ? [] : result.data.gepirItem);
                    }
                },
                $rootScope.TratarErro
            );
        }
    };

    $scope.tipoConsulta = function () {
        //        $scope.tiposConsulta = [
        //            {
        //                "codigo": 1,
        //                "nome": $translate.instant('66.LBL_InformacoesDonoItem') //'Detalhes sobre o dono do item'
        //            },
        //            {
        //                "codigo": 2,
        //                "nome": $translate.instant('66.LBL_DetalhesItem') //'Detalhes do Item'
        //            }
        //        ]

        $scope.tiposConsulta = [
            {
                "codigo": 1,
                "nome": "Detalhes sobre o dono do item"
            },
            {
                "codigo": 2,
                "nome": "Detalhes do Item"
            }
        ]


    }

    //Método Inicial da Página
    $scope.onLoad = function () {
        $scope.shouldLoadCampos = true;
        $scope.itemForm = {};
        $scope.tipoConsulta();
        $scope.consultaefetuada = false;

        $rootScope.verificaAssociadoSelecionado(function (data) {

            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                $rootScope.flagAssociadoSelecionado = true;
            } else {
                $rootScope.flagAssociadoSelecionado = false;
            }
        });
    }

    $scope.alteraTipoConsulta = function () {
        $scope.consultaefetuada = false;
    }


    $scope.onLoad();

    //Limpar campos
    $scope.onClean = function () {
        $scope.onLoad();
    }

    $scope.onSearch = function (item) {

        $scope.consultaefetuada = false;
        $scope.erroWS = '';

        if (!$scope.form.$valid) {
            var mensagem = $translate.instant('66.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '66.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            $scope.form.submitted = true;
        }
        else {

            if (item.gtin.toString().length < 14) {
                $scope.prefixoGTIN = item.gtin.toString().substring(0, 3);
            }
            else {
                $scope.prefixoGTIN = item.gtin.toString().substring(1, 4);
            }

            if (item.gtin.toString().length < 14) {
                var zerosEsquerda = "";

                while (item.gtin.toString().length + zerosEsquerda.toString().length < 14) {
                    zerosEsquerda += "0";
                }

                item.gtin = zerosEsquerda + item.gtin;
            }

            if (!Utils.validaDigitoVerificador(item.gtin.toString())) {
                var mensagem = $translate.instant('66.MENSAGEM_DigitoVerificadorInvalido');
                $scope.alert(typeof mensagem === 'object' || mensagem == '66.MENSAGEM_DigitoVerificadorInvalido' ? 'Dígito Verificador inválido.' : mensagem);
            }
            else {
                if ($scope.itemForm.tipoconsulta == 2) {


                    if ($scope.prefixoGTIN == "789" || $scope.prefixoGTIN == "790") {
                        genericService.postRequisicao('GepirRouterBO', 'ConsultarWSFullItemByGTIN', { campos: item }).then(
                            function (result) {
                                if (result.data != undefined && result.data != null) {
                                    if (result.data.Item != undefined && result.data.Item != null) {
                                        $scope.dadosItem = result.data.Item;
                                    } else {
                                        $scope.erroWS = result.data;
                                    }
                                } else {
                                    $scope.dadosItem = null;
                                }
                                $scope.consultaefetuada = true;
                            },
                            $rootScope.TratarErro
                        );

                    } else {
                        $scope.dadosItem = null;
                        $scope.initMode = false;
                        $scope.itemForm = item;
                        $scope.tableCampos.reload();
                        //$scope.tableCampos.page(1);
                    }


                } else {
                    $scope.dadosItem = null;
                    $scope.initMode = false;
                    $scope.itemForm = item;
                    $scope.tableCampos.reload();
                    //$scope.tableCampos.page(1);

                }
            }
        }
    }
} ]);