app.controller('GepirNomeEmpresaCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$log', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $log, $modal, genericService, Utils, blockUI, $q, $translate) {

    //inicializando abas do form
    $('#tabs-ajuda a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var titulo = $translate.instant('62.LBL_ConsultarNomeEmpresa');
    $scope.titulo = typeof titulo === 'object' || titulo == '62.LBL_ConsultarNomeEmpresa' ? 'Consultar por Nome da Empresa' : titulo;
    $scope.form = {};
    $scope.itemForm = {};
    $scope.initMode = true;
    $scope.showInputs = false;

    $scope.tableCampos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if (!$scope.initMode) {
                var dados = carregaGridCampos($scope.itemForm, function (data) {
                    $scope.consultaefetuada = true;
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        }
    });

    //Popula grid de campos
    function carregaGridCampos(item, callback) {

        genericService.postRequisicao('GepirRouterBO', 'ConsultarWSPartyByName', { campos: item }).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    callback(result.data.gepirParty == null ? [] : result.data.gepirParty);
                }
            },
            $rootScope.TratarErro
        );
    };


    //Mercado Alvo e Países
    $scope.carregarPaises = function () {
        genericService.postRequisicao('ISOBO', 'consultarPaisesISO', {}).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $scope.paises = result.data;
                }
            },
            $rootScope.TratarErro
        );
    }

    //Chamado ao selecionar um país
    $scope.onSelectedCountry = function (pais) {
        //Código 24 é o código ISO referente ao Brazil
        if (pais != 24 && pais != undefined) {
            $scope.showInputs = true;
        }
        else {
            $scope.showInputs = false;
        }
        $scope.consultaefetuada = false;
    }

    //Método Inicial da Página
    $scope.onLoad = function () {
        $scope.shouldLoadCampos = true;
        $scope.itemForm = {};
        $scope.carregarPaises();
        $scope.consultaefetuada = false;

        $rootScope.verificaAssociadoSelecionado(function (data) {

            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                $rootScope.flagAssociadoSelecionado = true;
            } else {
                $rootScope.flagAssociadoSelecionado = false;
            }
        });
    }

    $scope.onLoad();

    //Limpar campos
    $scope.onClean = function () {
        $scope.onLoad();
    }

 
    $scope.onSearch = function (item) {

        if (!$scope.form.$valid) {
            var mensagem = $translate.instant('62.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '62.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            $scope.form.submitted = true;
        }
        else {
            if (item.nomeempresa.toString().length < 3) {
                var mensagem = $translate.instant('62.MENSAGEM_NomeEmpresaTresDigitos');
                $scope.alert(typeof mensagem === 'object' || mensagem == '62.MENSAGEM_NomeEmpresaTresDigitos' ? 'O nome da empresa deve ter pelo menos 3 caracteres.' : mensagem);
            }
            else {
                $scope.initMode = false;
                $scope.itemForm = item;
                $scope.tableCampos.reload();
                $scope.tableCampos.page(1);
            }
        }
    }

} ]);