app.controller('GeracaoEpcRfidCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {
        //inicializando abas do form
        $('#tabs-etiqueta a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({ html: true });
        });

        var titulo = $translate.instant('16.LBL_TitleGeracaoEPCRFID');
        $scope.titulo = typeof titulo === 'object' || titulo == '16.LBL_TitleGeracaoEPCRFID' ? 'Geração de EPC/RFID' : titulo;

        $scope.isPesquisa = false;
        $scope.item = {};

        $scope.tableEpcRfid = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                dataalteracao: 'desc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridEpcRfid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        function carregaGridEpcRfid(item, isPesquisa, callback) {
            var dados = null;

            $rootScope.verificaAssociadoSelecionado(function (data) {
                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                    if (isPesquisa) {
                        dados = angular.copy(item);

                        genericService.postRequisicao('EPCRFIDBO', 'PesquisarEPCRFIDs', { campos: dados }).then(
                            function (result) {

                                $scope.form.submitted = false;
                                $scope.editMode = false;

                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                         );
                    }
                    else if (!isPesquisa) {

                        genericService.postRequisicao('EPCRFIDBO', 'BuscarEPCRFIDs', { campos: dados }).then(
                            function (result) {
                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                         );
                    }

                    $rootScope.flagAssociadoSelecionado = true;
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }
            });
        }

        $scope.$watch('item.serialinicio', function (newValue, oldValue) {
            if (newValue && newValue.length > $scope.item.quantidademaximacaracteres) {
                $scope.item.serialinicio = oldValue;
                return;
            }
        });

        $scope.$watch('item.serialfim', function (newValue, oldValue) {
            if (newValue && newValue.length > $scope.item.quantidademaximacaracteres) {
                $scope.item.serialfim = oldValue;
                return;
            }
        });

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('EPCRFIDBO', 'InsereHistoricoEPCRFID', function () {
                Utils.bloquearCamposVisualizacao("VisualizarEPCRFID");

                $scope.item = {};

                var titulo = $translate.instant('16.LBL_TitleGeracaoEPCRFID');
                $scope.titulo = typeof titulo === 'object' || titulo == '16.LBL_TitleGeracaoEPCRFID' ? 'Geração de EPC/RFID' : titulo;

                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isCollapsedMidias = true;
                $scope.initMode = true;
                $scope.exibeVoltar = true;
                $scope.isPesquisa = false;
            });
        }

        //Clique no botão fechar
        $scope.onCancel = function (item) {
            $scope.item = {};
            var titulo = $translate.instant('16.LBL_TitleGeracaoEPCRFID');
            $scope.titulo = typeof titulo === 'object' || titulo == '16.LBL_TitleGeracaoEPCRFID' ? 'Geração de EPC/RFID' : titulo;
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.gerar = false;
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableEpcRfid.page(1);
            $scope.tableEpcRfid.reload();
        }

        //Clique no botão pesquisar
        $scope.onSearch = function (item) {
            $scope.item = angular.copy(item);
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableEpcRfid.page(1);
            $scope.tableEpcRfid.reload();
        }

        $scope.onSearchMode = function (item) {
            $scope.item = {};
            var titulo = $translate.instant('16.LBL_PesquisarEPC');
            $scope.titulo = typeof titulo === 'object' || titulo == '16.LBL_PesquisarEPC' ? 'Pesquisar EPC/RFID' : titulo;
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.initMode = true;
            $scope.exibeVoltar = true;
        }

        //Remove os zeros dos campos Serial Inicio ou Serial Fim
        $scope.removeZeros = function (name, value) {
            if ($scope.item.codigotipohash == 1) {
                if (value != undefined && value != null && value != '') {
                    var inteiro = parseInt(value);
                    $scope.item[name] = inteiro.toString();
                }
            }
        }

        //Clique no botão limpar
        $scope.onClean = function (item) {
            $scope.item = {};
        }

        $scope.onChangeSerial = function (value) {
            if ($scope.item != undefined) {
                $scope.item.serialinicio = '';
                $scope.item.serialfim = '';
            }

            if (value == 1) {
                $scope.item.quantidademaximacaracteres = 8;
            }
            else if (value == 2) {
                $scope.item.quantidademaximacaracteres = 16;
            }
        }

        function isitHEX(entry) {
            validChar = '012345678ABCDEFabcdef.';   // legal chars
            strlen = entry.length;       // test string length
            if (strlen < 1) { return false; }
            entry = entry.toUpperCase(); // case insensitive
            // Now scan for illegal characters
            for (idx = 0; idx < strlen; idx++) {
                if (validChar.indexOf(entry.charAt(idx)) < 0) {
                    return false;
                }
            } // end scanning
            return true;
        }

        //Clique no botão gerar. Gera os EPCs/RFIDs e salva na base.
        $scope.gerarRFID = function (item) {

            if (!$scope.form.$valid || item.globaltradeitemnumber == undefined || item.globaltradeitemnumber == null) {
                var mensagem = $translate.instant('16.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '16.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
                $scope.gerar = false;
            }
            else {
                if (item.codigotipohash != 2) {
                    if (parseInt(item.serialinicio) > parseInt(item.serialfim)) {
                        var mensagem = $translate.instant('16.MENSAGEM_SerialInicioMaiorFim');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '16.MENSAGEM_SerialInicioMaiorFim' ? 'Serial Início não pode ser maior do que o Serial Fim.' : mensagem);
                        return false;
                    }
                } else {
                    var msg = "";
                    if (!isitHEX(item.serialinicio)) {
                        var mensagem = $translate.instant('16.MENSAGEM_SerialInicioInvalido');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '16.MENSAGEM_SerialInicioInvalido' ? 'Serial Início inválido.' : mensagem + '\n');
                    }

                    //                    if (!isHex(item.serialfim)) {
                    //                        msg = msg + 'Serial Fim inválido.\n';
                    //                    }

                    //                    if (msg == "") {
                    //                        if (parseInt(item.serialinicio, 16) > parseInt(item.serialfim, 16)) {
                    //                            $scope.alert('Serial Início não pode ser maior do que o Serial Fim.');
                    //                            return false;
                    //                        }
                    //                    }

                    if (msg != "") {
                        $scope.alert(msg);
                        return false;
                    }

                }

                genericService.postRequisicao('EPCRFIDBO', 'InsereHistoricoEPCRFID', { campos: item }).then(
                    function (result) {

                        if (result != undefined && result != null && result.data != null) {
                            Utils.moveScrollTop();
                            var mensagem = $translate.instant('16.MENSAGEM_EPCGeradoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '16.MENSAGEM_EPCGeradoSucesso' ? 'EPC/RFID gerado com sucesso. Selecione um formato abaixo para visualizar o arquivo gerado.' : mensagem);
                            $scope.initMode = false;
                            $scope.gerar = true;
                        }
                    },
                    $rootScope.TratarErro
                );
            }
        }

        //Download do EPC/RFID selecionado
        $scope.downloadFile = function (value) {

            if (value != undefined && value != null) {

                var dados = angular.copy($scope.item);
                dados.itemGerado = value;

                $("#epcdata").val(JSON.stringify({ 'campos': dados }));
                $('#EPCRFIDHandler').submit();
            }
        }

        //Método para abertura do modal de Produto
        $scope.onProduto = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalProduto',
                controller: ModalProdutoCtrl,
                resolve: {
                    item: function () {
                        var produto = angular.copy(item);
                        return produto;
                    },
                    licencas: function () {
                        return $scope.licencas;
                    },
                    statusgtin: function () {
                        return $scope.statusgtin;
                    }
                }
            });

            modalInstance.result.then(function (data) {

                var antigoSerialInicial = '', antigoSerialFinal = '', antigoCodigoTipoHash = '', antigoQuantidade = ''; antigoFiltro = '';

                if ($scope.item != undefined && $scope.item.serialinicio != undefined) {
                    antigoSerialInicial = $scope.item.serialinicio;
                }
                if ($scope.item != undefined && $scope.item.serialfim != undefined) {
                    antigoSerialFinal = $scope.item.serialfim;
                }
                if ($scope.item != undefined && $scope.item.codigotipohash != undefined) {
                    antigoCodigoTipoHash = $scope.item.codigotipohash;
                }
                if ($scope.item != undefined && $scope.item.quantidademaximacaracteres != undefined) {
                    antigoQuantidade = $scope.item.quantidademaximacaracteres;
                }
                if ($scope.item != undefined && $scope.item.codigotipofiltro != undefined) {
                    antigoFiltro = $scope.item.codigotipofiltro;
                }

                $scope.item = data;

                if (antigoSerialInicial != '') {
                    $scope.item.serialinicio = antigoSerialInicial;
                }
                if (antigoSerialFinal != '') {
                    $scope.item.serialfim = antigoSerialFinal;
                }
                if (antigoCodigoTipoHash != '') {
                    $scope.item.codigotipohash = antigoCodigoTipoHash;
                }
                if (antigoQuantidade != '') {
                    $scope.item.quantidademaximacaracteres = antigoQuantidade;
                }
                if (antigoFiltro != '') {
                    $scope.item.codigotipofiltro = antigoFiltro;
                }

            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        }

        $scope.onEdit = function (item) {
            //$rootScope.verificaPermissao('EPCRFIDBO', 'EditarEPCRFID', function () {
            Utils.bloquearCamposVisualizacao("VisualizarEPCRFID");

            var titulo = $translate.instant('16.LBL_AlterarEPC');
            $scope.titulo = typeof titulo === 'object' || titulo == '16.LBL_AlterarEPC' ? 'Alterar EPC/RFID' : titulo;
            $scope.initMode = false;
            $scope.item = angular.copy(item);
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isPesquisa = false;
            $scope.gerar = true;
            $scope.exibeVoltar = true;
            //});
        }

        //Carregamento inicial
        $scope.onLoad = function () {

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                    //Licenças
                    genericService.postRequisicao('LicencaBO', 'buscarTipoGtinAssociado', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.licencas = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Produto
                    genericService.postRequisicao('ProdutoBO', 'BuscarProdutos', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.produtos = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Filtros
                    genericService.postRequisicao('TipoFiltroBO', 'BuscarTipoFiltrosAtivos', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.filtros = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Status do GTIN
                    genericService.postRequisicao('StatusGTINBO', 'BuscarStatusGTINAtivosFiltrado', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.statusgtin = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    $(window).scrollTop(0);
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }
            });
        }

        $scope.onLoad();

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };
    } ]);


//Controller para métodos do modal
    var ModalProdutoCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, genericService, item, Utils, licencas, statusgtin) {

        $scope.initModal = true;

        $scope.licencas = licencas;
        $scope.statusgtin = statusgtin;
        $scope.itemForm = {
            gtin: ''
        , descricao: ''
        , codigotipogtin: ''
        , codigostatusgtin: ''
        };

        $scope.tableProdutos = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridProdutos(function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.produtos = data;
                });
            }
        });

        function carregaGridProdutos(callback) {
            var dados = angular.copy($scope.itemForm);

            genericService.postRequisicao('EPCRFIDBO', 'PesquisaProdutos', { campos: dados }).then(
                function (result) {
                    if (callback) callback(result.data);
                },
                $rootScope.TratarErro
            );
        }

        //Chamado ao selecionar a opção "Pesquisar"
        $scope.onFilter = function () {
            if (!validarPesquisa()) {
                return false;
            }

            $scope.isPesquisa = true;
            $scope.tableProdutos.page(1);
            $scope.tableProdutos.reload();
        };

        function validarPesquisa() {
            if (!$scope.itemForm.gtin
            && !$scope.itemForm.descricao
            && !$scope.itemForm.codigotipogtin
            && !$scope.itemForm.codigostatusgtin) {

                var mensagem = $translate.instant('16.MENSAGEM_PreenchaAoMenosUmFiltro');
                $scope.alert(typeof mensagem === 'object' || mensagem == '16.MENSAGEM_PreenchaAoMenosUmFiltro' ? 'Preencha ao menos um filtro.' : mensagem);
                return false;
            }

            return true;
        }

        //Chamado ao selecionar um PRODUTO no grid
        $scope.onSelectedProduto = function (item) {
            //$rootScope.confirm('Tem certeza que deseja selecionar o produto "' + item.productdescription + '"?', function () {
            $modalInstance.close(item);
            //}, function () { }, 'Confirmação');
        }

        //Cancelar 
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }

    };