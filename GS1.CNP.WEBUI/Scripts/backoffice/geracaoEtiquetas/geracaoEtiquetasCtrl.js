app.controller('GeracaoEtiquetasCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $q, $translate) {

        var titulo = $translate.instant('17.LBL_GeracaoEtiqueta');
        $scope.titulo = typeof titulo === 'object' || titulo == '17.LBL_GeracaoEtiqueta' ? 'Geração de Etiquetas' : titulo;

        $scope.isPesquisa = false;
        $scope.stepCalculation = 1;
        $scope.item = {};
        //$scope.item.codigotipogeracao = 1;
        $scope.item.codigoposicao = 1;

        $scope.item.alinhamentodescricao = "left";
        $scope.item.fontenomedescricao = "Arial";
        $scope.item.fontetamanhodescricao = "10";

        $scope.item.alinhamentotextolivre = "left";
        $scope.item.fontenometextolivre = "Arial";
        $scope.item.fontetamanhotextolivre = "8";



        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({ html: true });
        });

        function montaArraySteps() {
            $scope.steps = [];

            $translate(['17.ABA_SelecioneProduto', '17.ABA_ConfigureCodigoBarras', '17.ABA_ImpressaoEtiqueta']).then(function (translations) {
                $scope.steps.push(translations['17.ABA_SelecioneProduto']);
                $scope.steps.push(translations['17.ABA_ConfigureCodigoBarras']);
                $scope.steps.push(translations['17.ABA_ImpressaoEtiqueta']);
            });
        }

        montaArraySteps();

        $scope.selectStep = function (step) {
            if ($scope.stepCalculation == step) {
                return;
            }
            $(window).scrollTop(0);
            $scope.stepCalculation = step;
        }

        $scope.irParaAbaConfiguracao = function (form, step) {
            if ($scope.item.globaltradeitemnumber != undefined) {
                $scope.irParaAba(form, step)
            } else {

                var mensagem = $translate.instant('17.MENSAGEM_SelecioneProduto');
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_SelecioneProduto' ? 'Selecione um produto antes de prosseguir.' : mensagem);
            }
        }

        $scope.irParaAba = function (form, step) {
            if ($scope.stepCalculation == step) {
                return;
            }
            $(window).scrollTop(0);
            $scope.stepCalculation = step;
        }
        $scope.voltarParaAba = function (step) {
            $(window).scrollTop(0);
            $scope.stepCalculation = step;
        }

        $scope.tableEtiquetas = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                dataimpressao: 'desc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridEtiquetas($scope.itemForm, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.items = data;
                });
            }
        });

        //Busca Etiquetas
        function carregaGridEtiquetas(item, isPesquisa, callback) {
            var dados = null;

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                    if (isPesquisa) {
                        dados = angular.copy(item);

                        genericService.postRequisicao('EtiquetaBO', 'PesquisarHistoricoEtiqueta', { campos: dados }).then(
                            function (result) {

                                $scope.form.submitted = false;
                                $scope.editMode = false;

                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                        );
                    }
                    else {
                        genericService.postRequisicao('EtiquetaBO', 'BuscarHistoricoEtiqueta', { campos: dados }).then(
                            function (result) {
                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                        );
                    }

                    $rootScope.flagAssociadoSelecionado = true;
                }
            });
        }

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('EtiquetaBO', 'CadastrarEtiqueta', function () {
                var titulo = $translate.instant('17.LBL_CadastrarEtiqueta');
                $scope.titulo = typeof titulo === 'object' || titulo == '17.LBL_CadastrarEtiqueta' ? 'Cadastrar Etiquetas' : titulo;

                $scope.stepCalculation = 1;
                $('#tabs-produto a:first').tab('show');
                $scope.item = {};
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.initMode = true;
                $scope.urlcodigobarras = "";
                $scope.urletiqueta = "";
                $scope.exibeVoltar = true;
                $scope.wasGenerated = false;
                $scope.BuscarTipoGeracao();
                $('#formcontainer').focus();

                Utils.bloquearCamposVisualizacao("VisualizarEtiqueta");
            });
        };

        $scope.onCancel = function (item) {
            $scope.item = {};
            var titulo = $translate.instant('17.LBL_GeracaoEtiqueta');
            $scope.titulo = typeof titulo === 'object' || titulo == '17.LBL_GeracaoEtiqueta' ? 'Geração de Etiquetas' : titulo;
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $scope.urlcodigobarras = "";
            $scope.urletiqueta = "";
            Utils.moveScrollTop();
            $scope.tableEtiquetas.page(1);
            $scope.tableEtiquetas.reload();
        };

        $scope.onSearchMode = function (item) {
            var titulo = $translate.instant('17.LBL_PesquisarEtiquetas');
            $scope.titulo = typeof titulo === 'object' || titulo == '17.LBL_PesquisarEtiquetas' ? 'Pesquisar Etiquetas' : titulo;
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.initMode = false;
            $('#tabs-produto a:first').tab('show');
            $scope.itemForm = {};
            $scope.urlcodigobarras = "";
            $scope.urletiqueta = "";
            $scope.exibeVoltar = true;

            $scope.BuscarTipoGeracaoPesquisa();
            $scope.BuscarFabricantes();
            $scope.BuscarCodigoBarras();
            $scope.BuscarModeloEtiquetas();
            $('#formcontainer').focus();
        };

        $scope.onEdit = function (item) {

            blockUI.start();
            $scope.item = angular.copy(item);
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isPesquisa = false;
            $scope.initMode = false;
            $scope.wasGenerated = false;
            $scope.BuscarTipoGeracao();
            $scope.BuscarFabricantes();
            $scope.BuscarModeloEtiquetas();
            $scope.BuscarCodigoBarras();
            $scope.BuscarPosicaoTexto();
            $scope.exibeVoltar = true;


            if ($scope.item.tipoetiqueta != undefined && $scope.item.tipoetiqueta != null) {
                var fabricantes = $scope.buscaEtiquetasFabricante($scope.item);
                fabricantes.then(function () {

                    var etiqueta = $.grep($scope.etiquetas, function (obj) {
                        return obj.codigo === item.codigoetiqueta;
                    });

                    if (etiqueta != undefined && etiqueta != null && etiqueta.length > 0) {
                        $scope.item.altura = etiqueta[0].altura;
                        $scope.item.largura = etiqueta[0].largura;

                        var quantidadetotaletiqueta = etiqueta[0].quantidadelinhas * etiqueta[0].quantidadecolunas;
                        $scope.item.quantidadesetiquetas = new Array(quantidadetotaletiqueta - 1);
                        for (i = 0; i <= quantidadetotaletiqueta - 1; i++) {
                            $scope.item.quantidadesetiquetas[i] = parseInt(i + 1);
                        }
                        $scope.item.quantidadetotaletiqueta = parseInt(quantidadetotaletiqueta);

                        $scope.atualizaAlinhamento();
                        $scope.atualizaPosicao();
                        Utils.bloquearCamposVisualizacao("VisualizarEtiqueta");

                        $scope.item.registroanvisa = [];
                        $scope.item.registroanvisa[0] = { alternateitemidentificationid: item.alternateitemidentificationid };
                        //                        if ($scope.homologacoesanvisa != undefined) {
                        //                            $scope.item.registroanvisa = $.grep($scope.homologacoesanvisa, function (obj) {
                        //                                return obj.codigo === $scope.item.codigohomologacao;
                        //                            });
                        //                        }

                        genericService.postRequisicao('EtiquetaBO', 'GerarCodigoBarras', { where: { codigotipocodigobarras: $scope.item.tipoetiqueta, codigogtin: $scope.item.globaltradeitemnumber, altura: $scope.item.altura, largura: $scope.item.largura, alturacodigobarras: etiqueta[0].height, larguracodigobarras: etiqueta[0].width, descricao: $scope.item.descricao, textolivre: $scope.item.textolivre, codigoposicao: $scope.item.codigoposicao, fontenome: item.fontenome, fontetamanho: item.fontetamanho, idealmodulewidth: etiqueta[0].idealmodulewidth, leftguard: etiqueta[0].leftguard, magnificationfactor: etiqueta[0].magnificationfactor, rightguard: etiqueta[0].rightguard, modelo128: item.modelo128, nrlote: item.nrlote, datavencimento: item.datavencimento, codigotipogtin: item.codigotipogtin, quantidadeporcaixa: item.quantidadeporcaixa, codigohomologacao: $scope.item.registroanvisa, serialinicial: item.serialinicial, serialfim: item.serialfim} }).then(
                            function (result) {
                                if (result.data != undefined && result.data != null) {
                                    $scope.urletiqueta = result.data.imagedata;
                                    $scope.atualizaPosicao();
                                    Utils.bloquearCamposVisualizacao("VisualizarEtiqueta");
                                }
                            },
                            $rootScope.TratarErro
                         );
                    } else if ($scope.item.tipoetiqueta == 10 || $scope.item.tipoetiqueta == 18) {

                        $scope.atualizaAlinhamento();
                        $scope.atualizaPosicao();
                        Utils.bloquearCamposVisualizacao("VisualizarEtiqueta");
                        angular.element("#ImprimirEtiqueta").attr("disabled", true);


                        genericService.postRequisicao('EtiquetaBO', 'GerarCodigoBarras', { where: { codigotipocodigobarras: $scope.item.tipoetiqueta, codigogtin: $scope.item.globaltradeitemnumber, altura: $scope.item.altura, largura: $scope.item.largura, descricao: $scope.item.descricao, textolivre: $scope.item.textolivre, codigoposicao: $scope.item.codigoposicao, fontenome: item.fontenome, fontetamanho: item.fontetamanho, modelo128: item.modelo128, nrlote: item.nrlote, datavencimento: item.datavencimento, codigotipogtin: item.codigotipogtin, quantidadeporcaixa: item.quantidadeporcaixa, codigohomologacao: $scope.item.registroanvisa, serialinicial: item.serialinicial, serialfim: item.serialfim} }).then(
                            function (result) {
                                if (result.data != undefined && result.data != null) {
                                    $scope.urlcodigobarras = result.data.imagedata;
                                    $scope.atualizaPosicao();
                                    Utils.bloquearCamposVisualizacao("VisualizarEtiqueta");
                                    angular.element("#ImprimirEtiqueta").attr("disabled", true);
                                }
                            },
                            $rootScope.TratarErro
                         );

                    } else {
                        blockUI.stop();
                    }

                }, $rootScope.TratarErro);
            }
            $('#formcontainer').focus();


        };

        $scope.onClean = function (item) {
            $scope.item = {};
            $scope.itemForm = {};
            $scope.urlcodigobarras = "";
            $scope.urletiqueta = "";
        };

        $scope.onSearch = function (item) {
            $scope.item = {};
            $scope.exibeVoltar = false;
            $scope.itemForm = angular.copy(item);
            Utils.moveScrollTop();
            $scope.tableEtiquetas.page(1);
            $scope.tableEtiquetas.reload();
        };

        //Buscar Tipo de Geração
        $scope.BuscarTipoGeracao = function () {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('EtiquetaBO', 'BuscarTipoGeracao', {}).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.tiposgeracao = result.data;

                            //Usado para corrigir bug no ie, no momento de esconder a grid e apresentar o form
                            $('#formcontainer').focus();

                            //                            if ($scope.initMode) {
                            //                                $scope.item.codigotipogeracao = 1;
                            //                            }

                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }

        //Buscar Tipo de Geração Pesquisa
        $scope.BuscarTipoGeracaoPesquisa = function () {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('EtiquetaBO', 'BuscarTipoGeracaoPesquisa', {}).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.tiposgeracao = result.data;
                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }

        //Buscar Posição do Texto
        $scope.BuscarPosicaoTexto = function () {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('EtiquetaBO', 'BuscarPosicaoTexto', {}).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.posicoestexto = result.data;

                            if ($scope.initMode) {
                                $scope.item.codigoposicao = 1;
                            }

                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }

        //Buscar Tipos de Código de Barras
        $scope.BuscarCodigoBarras = function () {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('BarCodeTypeListBO', 'BuscarCodigoBarras', {}).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.codigosbarra = result.data;
                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }

        //Buscar Fabricantes
        $scope.BuscarFabricantes = function () {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('EtiquetaBO', 'BuscarFabricantes', {}).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.fabricantes = result.data;

                            //                            if ($scope.initMode || $scope.isPesquisa) {
                            //                                $scope.item.codigofabricante = 1;
                            //                                $scope.item.codigoetiqueta = 1;
                            //                            }
                            $scope.buscaEtiquetasFabricante($scope.item);
                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }

        //Buscar Fabricantes
        $scope.BuscarFabricantesTipoCodigoBarras = function (item) {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('EtiquetaBO', 'BuscarFabricantesTipoCodigoBarras', { where: { codigotipocodigobarras: item.tipoetiqueta, modelo128: item.modelo128} }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        $scope.fabricantes = result.data;
                        $scope.buscaEtiquetasFabricante($scope.item);
                        resolve(result.data);
                    }
                },
                $rootScope.TratarErro
            );
            });
        }


        //Buscar Modelo de etiquetas
        $scope.BuscarModeloEtiquetas = function () {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('EtiquetaBO', 'BuscarModeloEtiquetas', {}).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.modeloetiquetas = result.data;
                            if ($scope.item.tipoetiqueta != undefined && $scope.item.tipoetiqueta != null && $scope.item.tipoetiqueta != '' && $scope.item.tipoetiqueta != 1 && $scope.item.tipoetiqueta != 9) {
                                $scope.visualizaEtiqueta($scope.item);
                            }

                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }


        //Metodo para abertura do modal de selecao de produto
        $scope.onProduto = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalProdutoGeracaoEtiqueta',
                controller: ModalProdutoGeracaoEtiquetaCtrl,
                size: 'lg',
                resolve: {
                    item: function () {
                        var produto = angular.copy(item);
                        return produto;
                    },
                    licencas: function () {
                        return $scope.licencas;
                    },
                    statusgtin: function () {
                        return $scope.statusgtin;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $scope.item = data;
                $scope.urlcodigobarras = "";
                $scope.urletiqueta = "";

                //Status do GTIN
                genericService.postRequisicao('BarCodeTypeListBO', 'buscarTipoCodigoBarras', { where: { codigotipogtin: data.codigotipogtin} }).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            if (result.data.length > 0) {
                                $scope.tiposetiquetas = result.data;
                            } else {
                                var mensagem = $translate.instant('17.MENSAGEM_NaoEncontradoCodigoBarrasDisponiveis');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_NaoEncontradoCodigoBarrasDisponiveis' ? 'Não foram encontrados tipos de código de barras disponíveis para este tipo de GTIN.' : mensagem);
                            }
                        } else {
                            var mensagem = $translate.instant('17.MENSAGEM_ErroBuscarCodigoBarras');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_ErroBuscarCodigoBarras' ? 'Erro ao buscar os tipos de códigos de barras.' : mensagem);
                        }
                    },
                    $rootScope.TratarErro
                );

                $scope.BuscarTipoGeracao();
                $scope.BuscarPosicaoTexto();
                $scope.BuscarFabricantes();


            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        }

        $scope.visualizaCodigoBarras = function (item) {
            //Status do GTIN
            var tipoetiquetaselecionada = $.grep($scope.tiposetiquetas, function (obj) {
                return obj.codigo === $scope.item.tipoetiqueta;
            });

            $scope.item.registroanvisa = '';
            if ($scope.homologacoesanvisa != undefined) {
                $scope.item.registroanvisa = $.grep($scope.homologacoesanvisa, function (obj) {
                    return obj.codigo === $scope.item.codigohomologacao;
                });
            }
            if ($scope.item.tipoetiqueta == 1 && ($scope.item.modelo128 == undefined || $scope.item.modelo128 == null || $scope.item.modelo128 == '')) {
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_Modelo128' || mensagem == undefined ? 'Selecione um modelo.' : mensagem);
                return;
            }

            if ((($scope.item.tipoetiqueta == 1 && $scope.item.modelo128 == 1) || $scope.item.tipoetiqueta == 10 || $scope.item.tipoetiqueta == 12) && ($scope.item.nrlote == undefined || $scope.item.nrlote == null || $scope.item.nrlote == "")) {
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_InformeLote' || mensagem == undefined ? 'Selecione um lote.' : mensagem);
                return;
            }

            if ($scope.item.tipoetiqueta == 1 && $scope.item.modelo128 == 2 && ($scope.item.codigohomologacao == undefined || $scope.item.codigohomologacao == null || $scope.item.codigohomologacao == "")) {
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_InformeAnatel' || mensagem == undefined ? 'Selecione um código de homologação Anatel.' : mensagem);
                return;
            }

            if ($scope.item.tipoetiqueta == 1 && ($scope.item.modelo128 == undefined || $scope.item.modelo128 == null || $scope.item.modelo128 == '')) {
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_DataMatrix' || mensagem == undefined ? 'Selecione um lote e o código de registro.' : mensagem);
                return;
            }

            //            if ($scope.item.tipoetiqueta == 9 && (($scope.item.nrlote == undefined || $scope.item.nrlote == null || $scope.item.nrlote == "") || ($scope.item.codigohomologacao == undefined || $scope.item.codigohomologacao == null || $scope.item.codigohomologacao == ""))) {
            //                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_DataMatrix' || mensagem == undefined ? 'Selecione um lote e o código de registro.' : mensagem);
            //                return;
            //            }

            if ($scope.item.tipoetiqueta == 9 && (($scope.item.nrlote == undefined || $scope.item.nrlote == null || $scope.item.nrlote == "") || ($scope.item.codigohomologacao == undefined || $scope.item.codigohomologacao == null || $scope.item.codigohomologacao == "") ||
                    ($scope.item.serialinicial == undefined || $scope.item.serialinicial == null || $scope.item.serialinicial == ""))) {
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_DataMatrix' || mensagem == undefined ? 'Selecione um lote, código de registro e o serial inicial.' : mensagem);
                return;
            }

            if ($scope.item.tipoetiqueta == 1 && ($scope.item.modelo128 == undefined || $scope.item.modelo128 == null || $scope.item.modelo128 == '')) {
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_Modelo128' || mensagem == undefined ? 'Selecione um modelo.' : mensagem);
                return;
            }

            //            if ($scope.item.tipoetiqueta == 9 && (($scope.item.nrlote == undefined || $scope.item.nrlote == null || $scope.item.nrlote == "") || ($scope.item.codigohomologacao == undefined || $scope.item.codigohomologacao == null || $scope.item.codigohomologacao == ""))) {
            //                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_DataMatrix' || mensagem == undefined ? 'Selecione um lote e o código de registro.' : mensagem);
            //                return;
            //            }

            if ($scope.item.tipoetiqueta == 9 && ($scope.item.serialinicial != undefined && $scope.item.serialinicial != null && $scope.item.serialinicial != "") && ($scope.item.serialfim != undefined && $scope.item.serialfim != null && $scope.item.serialfim != "")) {
                if (parseInt($scope.item.serialinicial) > parseInt($scope.item.serialfim)) {
                    var mensagem = $translate.instant('16.MENSAGEM_SerialInicioMaiorFim');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '16.MENSAGEM_SerialInicioMaiorFim' ? 'Serial Início não pode ser maior do que o Serial Fim.' : mensagem);
                    return false;
                }
                if (parseInt($scope.item.serialinicial) == 0 || parseInt($scope.item.serialfim) == 0) {
                    var mensagem = $translate.instant('17.MENSAGEM_SerialZero');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_SerialZero' ? 'Serial  pode ser zero.' : mensagem);
                    return false;
                }
            }



            if (tipoetiquetaselecionada.length > 0 && tipoetiquetaselecionada[0].height != undefined && tipoetiquetaselecionada[0].width != undefined
                && (
                        (
                                (item.tipoetiqueta == 1 || item.tipoetiqueta == 10 || item.tipoetiqueta == 12)
                                && (
                                        (item.tipoetiqueta == 1 && item.modelo128 == 1 && item.nrlote != null && item.datavencimento != null)
                                        || (item.tipoetiqueta == 1 && item.modelo128 == 2 && item.codigohomologacao != null)
                                        || (item.tipoetiqueta == 10 && item.nrlote != null && item.datavencimento != null)
                                        || (item.tipoetiqueta == 12 && item.nrlote != null && item.datavencimento != null)
                                   )
                        )
                                || (item.tipoetiqueta != 1 && item.tipoetiqueta != 10 && item.tipoetiqueta != 12)
                    )
              ) {

                if (item != null && item != undefined && item.tipoetiqueta != null && item.tipoetiqueta != undefined && item.globaltradeitemnumber != null && item.globaltradeitemnumber != undefined) {
                    genericService.postRequisicao('EtiquetaBO', 'GerarCodigoBarras', { where: { codigotipocodigobarras: item.tipoetiqueta, codigogtin: item.globaltradeitemnumber, alturacodigobarras: tipoetiquetaselecionada[0].height, larguracodigobarras: tipoetiquetaselecionada[0].width, idealmodulewidth: tipoetiquetaselecionada[0].idealmodulewidth, leftguard: tipoetiquetaselecionada[0].leftguard, magnificationfactor: tipoetiquetaselecionada[0].magnificationfactor, rightguard: tipoetiquetaselecionada[0].rightguard, modelo128: item.modelo128, nrlote: item.nrlote, datavencimento: item.datavencimento, codigotipogtin: item.codigotipogtin, quantidadeporcaixa: item.quantidadeporcaixa, codigohomologacao: $scope.item.registroanvisa, serialinicial: item.serialinicial, serialfim: item.serialfim} }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.urlcodigobarras = result.data.imagedata;
                            }
                        },
                        $rootScope.TratarErro
                    );
                }
            } else {
                var mensagem = $translate.instant('17.MENSAGEM_ConfiguracaoInvalida');
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_ConfiguracaoInvalida' ? 'Não foi possível visualizar o código de barras. Configuração inválida.' : mensagem);
            }
        }

        $scope.configurarImpressao = function () {
            if ($scope.item.tipoetiqueta != undefined && $scope.item.tipoetiqueta != null && $scope.item.tipoetiqueta != '' && ($scope.item.tipoetiqueta)) {
                if ((($scope.item.tipoetiqueta == 1 && $scope.item.modelo128 == 1) || $scope.item.tipoetiqueta == 10 || $scope.item.tipoetiqueta == 12) && ($scope.item.nrlote == undefined || $scope.item.nrlote == null || $scope.item.nrlote == "")) {
                    $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_InformeLote' || mensagem == undefined ? 'Selecione um lote.' : mensagem);
                    return;
                }

                if ($scope.item.tipoetiqueta == 1 && $scope.item.modelo128 == 2 && ($scope.item.codigohomologacao == undefined || $scope.item.codigohomologacao == null || $scope.item.codigohomologacao == "")) {
                    $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_InformeAnatel' || mensagem == undefined ? 'Selecione um código de homologação Anatel.' : mensagem);
                    return;
                }

                if ($scope.item.tipoetiqueta == 1 && ($scope.item.modelo128 == undefined || $scope.item.modelo128 == null || $scope.item.modelo128 == '')) {
                    $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_DataMatrix' || mensagem == undefined ? 'Selecione um lote e o código de registro.' : mensagem);
                    return;
                }

                if ($scope.item.tipoetiqueta == 9 && (($scope.item.nrlote == undefined || $scope.item.nrlote == null || $scope.item.nrlote == "") || ($scope.item.codigohomologacao == undefined || $scope.item.codigohomologacao == null || $scope.item.codigohomologacao == "") ||
                    ($scope.item.serialinicial == undefined || $scope.item.serialinicial == null || $scope.item.serialinicial == ""))) {
                    $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_DataMatrix' || mensagem == undefined ? 'Selecione um lote, código de registro e o serial inicial.' : mensagem);
                    return;
                }
                if ($scope.item.tipoetiqueta == 1 && ($scope.item.modelo128 == undefined || $scope.item.modelo128 == null || $scope.item.modelo128 == '')) {
                    $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_Modelo128' || mensagem == undefined ? 'Selecione um modelo.' : mensagem);
                    return;
                }

                //                if ($scope.item.tipoetiqueta == 9 && (($scope.item.nrlote == undefined || $scope.item.nrlote == null || $scope.item.nrlote == "") || ($scope.item.codigohomologacao == undefined || $scope.item.codigohomologacao == null || $scope.item.codigohomologacao == ""))) {
                //                    $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_DataMatrix' || mensagem == undefined ? 'Selecione um lote e o código de registro.' : mensagem);
                //                    return;
                //                }

                if ($scope.item.tipoetiqueta == 9 && ($scope.item.serialinicial != undefined && $scope.item.serialinicial != null && $scope.item.serialinicial != "") && ($scope.item.serialfim != undefined && $scope.item.serialfim != null && $scope.item.serialfim != "")) {
                    if (parseInt($scope.item.serialinicial) > parseInt($scope.item.serialfim)) {
                        var mensagem = $translate.instant('16.MENSAGEM_SerialInicioMaiorFim');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '16.MENSAGEM_SerialInicioMaiorFim' ? 'Serial Início não pode ser maior do que o Serial Fim.' : mensagem);
                        return false;
                    }
                    if (parseInt($scope.item.serialinicial) == 0 || parseInt($scope.item.serialfim) == 0) {
                        var mensagem = $translate.instant('17.MENSAGEM_SerialZero');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_SerialZero' ? 'Serial pode ser zero.' : mensagem);
                        return false;
                    }
                }

                if ($scope.item.tipoetiqueta == 9 && ($scope.item.serialinicial != undefined && $scope.item.serialinicial != null && $scope.item.serialinicial != '')) { //&& $scope.item.serialfim != undefined && $scope.item.serialfim != null && $scope.item.serialfim != ''

                    if (parseInt($scope.item.serialinicial) == parseInt($scope.item.serialfim)) {
                        $scope.item.quantidadeetiqueta = 1;
                    }
                    else if ($scope.item.serialfim == undefined && $scope.item.serialinicial != undefined) {
                        $scope.item.quantidadeetiqueta = 1;
                    }
                    else {
                        $scope.item.quantidadeetiqueta = parseInt($scope.item.serialfim) - parseInt($scope.item.serialinicial) + 1;
                    }
                }

                if ($scope.item.tipoetiqueta == 9) {
                    $scope.item.registroanvisa = '';
                    if ($scope.homologacoesanvisa != undefined) {
                        $scope.item.registroanvisa = $.grep($scope.homologacoesanvisa, function (obj) {
                            return obj.codigo === $scope.item.codigohomologacao;
                        });
                    }
                }


                $scope.irParaAba('pesos', 3);
                $scope.BuscarFabricantesTipoCodigoBarras($scope.item);
                $scope.visualizaEtiqueta($scope.item);
            } else {
                var mensagem = $translate.instant('17.MENSAGEM_InformeTipoCodigoBarras');
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_InformeTipoCodigoBarras' ? 'Informe o Tipo de Código de Barras.' : mensagem);
                angular.element("#tipoetiqueta").focus();
            }
        }

        $scope.buscaEtiquetasFabricante = function (item) {
            return $q(function (resolve, reject) {
                if (item != null && item.codigofabricante != null && item.tipoetiqueta != null) {
                    genericService.postRequisicao('EtiquetaBO', 'BuscarEtiquetas', { where: { codigofabricante: item.codigofabricante, codigotipocodigobarras: item.tipoetiqueta, modelo128: item.modelo128} }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.etiquetas = result.data;
                                resolve(result.data);
                            }
                        },
                        $rootScope.TratarErro
                    );
                } else {
                    $scope.etiquetas = {};
                    $scope.item.quantidadesetiquetas = {};
                }
            });
        }

        $scope.atualizaPosicao = function () {

            var etiqueta = $.grep($scope.etiquetas, function (obj) {
                return obj.codigo === $scope.item.codigoetiqueta;
            });

            if (etiqueta.length > 0) {
                var altura = etiqueta[0].altura;
                var largura = etiqueta[0].largura;

                if ($scope.item.tipoetiqueta == 9) {
                    var alturacodigobarras = etiqueta[0].height;
                    var larguracodigobarras = etiqueta[0].width;
                } else {
                    var alturacodigobarras = etiqueta[0].height + 8;
                    var larguracodigobarras = etiqueta[0].width;
                }


                var margemimagemesquerda = 1;

                var margemimagemesquerda = 1;
                if ($scope.item.tipoetiqueta == 2) {
                    margemimagemesquerda = 0;
                }

                if ($scope.item.tipoetiqueta == 24) {
                    alturacodigobarras = etiqueta[0].height;
                    larguracodigobarras = etiqueta[0].width;
                    margemimagemesquerda = 0;
                }

                if ($scope.item.tipoetiqueta == 18) {
                    alturacodigobarras = "";
                    larguracodigobarras = etiqueta[0].width;
                }

                if ($scope.item.tipoetiqueta == 1 && $scope.item.modelo128 == 1) { //128 padrao
                    alturacodigobarras = etiqueta[0].height;
                    larguracodigobarras = etiqueta[0].width;

                }

                if ($scope.item.tipoetiqueta == 1 && $scope.item.modelo128 == 2) { //128 Anatel
                    //angular.element($('.logoAnatel')).attr('style', 'text-align:center');
                }

                var ajustelargura = 2;
                var ajustemargemdireitatexto = 5;

                if ($scope.item.codigoetiqueta == 8) { //CA4255
                    ajustelargura = 6;
                    margemdireita = 3;
                    margemimagemesquerda = 1;
                }

                if ($scope.item.codigoetiqueta == 8 || $scope.item.codigoetiqueta == 10 || $scope.item.codigoetiqueta == 10) { //Colacril
                    ajustemargemdireitatexto = 7;
                }

                if ($scope.item.tipoetiqueta == 6) {//EAN8
                    ajustelargura = 5;
                    margemdireita = 3;
                    ajustemargemdireitatexto = 7;
                }

                angular.element($('#etiqueta')).attr('style', 'width:' + largura + 'mm !important; height:' + altura + 'mm !important;margin-left:1px;line-height:normal;outline: solid 1px #CCCCCC;');

                if ($scope.item.codigotipogeracao == 1) {
                    if ($scope.item.modelo128 == 1) {


                        if ($scope.item.tipoetiqueta == 9) {
                            angular.element(".blocotexto").css({ marginLeft: "22mm", marginTop: "1mm" });
                            //angular.element(".blocotexto").css({ marginTop: "1mm" });
                        } else {
                            angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;');
                            angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;margin: auto;display: block;margin-top: 4mm;position:relative;');
                        }
                    } else if ($scope.item.tipoetiqueta == 9) {
                        angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important');
                        angular.element($('#urletiqueta')).attr('style', 'width:mm !important; height:mm !important');
                        angular.element($('.blocotexto')).attr('style', 'width:' + (largura - larguracodigobarras) + 'mm !important; height:' + altura + 'mm !important;left:' + (larguracodigobarras + 5) + 'mm');
                    } else {
                        angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important');
                        angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;margin-left:4mm');
                    }
                } else {
                    if ($scope.item.codigoposicao == 1) {
                        angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;margin-left:' + margemimagemesquerda + 'mm;');
                        angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important');

                        if ((largura - larguracodigobarras - ajustelargura) > 0) {
                            angular.element($('.blocotexto')).attr('style', 'width:' + (largura - larguracodigobarras - ajustelargura) + 'mm !important; height:' + (altura - 2) + 'mm !important;left:' + (larguracodigobarras + ajustemargemdireitatexto) + 'mm;margin-top:1mm;word-wrap: break-word;');
                        } else {
                            angular.element($('.blocotexto')).attr('style', 'width:0mm !important; height:' + (altura - 2) + 'mm !important;left:' + (larguracodigobarras + ajustemargemdireitatexto) + 'mm;margin-top:1mm;word-wrap: break-word;');
                        }
                    }

                    if ($scope.item.codigoposicao == 2) {
                        angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;left:' + (largura - larguracodigobarras - 1) + 'mm');
                        angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important');

                        if ((largura - larguracodigobarras - ajustelargura) > 0) {
                            angular.element($('.blocotexto')).attr('style', 'width:' + (largura - larguracodigobarras - ajustelargura) + 'mm !important; height:' + (altura - 2) + 'mm !important;'); 7
                        } else {
                            angular.element($('.blocotexto')).attr('style', 'width:0mm !important; height:' + (altura - 2) + 'mm !important;');
                        }
                    }

                    ajusteAbaixo = 3;
                    ajusteAcima = 3;
                    ajusteTextoAcima = 0;


                    if ($scope.item.codigoetiqueta == 7) { //L7160                    
                        ajusteAcima = 2;
                        ajusteTextoAcima = -1;
                    }

                    if ($scope.item.codigoetiqueta == 8) { //CA4255       
                        ajusteAcima = 2;
                        ajusteTextoAcima = -3;
                    }

                    if ($scope.item.codigoetiqueta == 4) { //A4256
                        ajusteAbaixo = 0;
                        ajusteAcima = 0;
                    }


                    if ($scope.item.codigoposicao == 3) {
                        angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;left:' + ((largura - larguracodigobarras) / 2) + 'mm');
                        //angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;left:' + ((largura - larguracodigobarras) / 2) + 'mm');
                        angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;');
                        angular.element($('.blocotexto')).attr('style', 'width:' + largura + 'mm !important; height:' + (altura - alturacodigobarras) + 'mm !important;top:' + alturacodigobarras + 'mm;display:block;');

                        if ($scope.item.codigoetiqueta == 4) { //A4256
                            angular.element($('.blocotexto')).attr('style', 'width:' + largura + 'mm !important; height:' + (altura - alturacodigobarras) + 'mm !important;top:' + alturacodigobarras + 'mm;display:none;');
                        }
                    }

                    if ($scope.item.codigoposicao == 4) {
                        //angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;top:' + (altura - alturacodigobarras + ajusteAcima) + 'mm;left:' + ((largura - larguracodigobarras) / 2) + 'mm');
                        angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;top:' + (altura - alturacodigobarras) + 'mm;left:' + ((largura - larguracodigobarras) / 2) + 'mm');
                        //angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;top:' + (altura - alturacodigobarras + ajusteAcima) + 'mm;left:' + ((largura - larguracodigobarras) / 2) + 'mm');
                        angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;');
                        angular.element($('.blocotexto')).attr('style', 'width:' + largura + 'mm !important; height:' + (altura - alturacodigobarras - ajusteTextoAcima) + 'mm !important;display:block;');

                        if ($scope.item.codigoetiqueta == 4) { //A4256
                            angular.element($('.blocotexto')).attr('style', 'width:' + largura + 'mm !important; height:' + (altura - alturacodigobarras - ajusteTextoAcima) + 'mm !important;display:none;');
                        }
                    }
                }
            }

            blockUI.stop();

            setTimeout(function () {
                $('[data-toggle="popover"]').popover({ html: true });
            }, 1000);

        }

        $scope.limpaVisualizacao = function () {
            $scope.urlcodigobarras = null;

            if ($scope.item.tipoetiqueta != 1) {
                $scope.item.modelo128 = "1";
            } else {
                if ($scope.item.codigotipogtin == 3) {
                    $scope.item.modelo128 = "2";
                    $scope.BuscarHomologacoesAnatel();
                    $scope.item.codigotipogeracao = 1;
                } else {
                    $scope.item.modelo128 = "1";
                }
            }

        }

        $scope.preparaVisualizacao = function () {

            $scope.item.alinhamentodescricao = "left";
            $scope.item.fontenomedescricao = "Arial";
            $scope.item.fontetamanhodescricao = "10";

            $scope.item.alinhamentotextolivre = "left";
            $scope.item.fontenometextolivre = "Arial";
            $scope.item.fontetamanhotextolivre = "8";

            //$scope.item.modelo128 = "1";

            $scope.atualizaAlinhamento();

            if ($scope.item.tipoetiqueta == 9) {
                $scope.BuscarHomologacoesAnvisa();
            }
        }


        $scope.atualizaAlinhamento = function () {
            if ($scope.item.tipoetiqueta != 9) {
                angular.element($('.descricaoetiqueta')).attr('style', 'width: 96%;display:block;text-align:' + $scope.item.alinhamentodescricao + ';font-family:' + $scope.item.fontenomedescricao + ' !important;font-size:' + $scope.item.fontetamanhodescricao + 'px !important;');
                angular.element($('.textoetiqueta')).attr('style', 'display:block;text-align:' + $scope.item.alinhamentotextolivre + ';font-family:' + $scope.item.fontenometextolivre + ' !important;font-size:' + $scope.item.fontetamanhotextolivre + 'px !important;');
            }
        }


        $scope.visualizaEtiqueta = function (item) {
            //Status do GTIN
            if (item != null && item.codigoetiqueta != null && $scope.etiquetas != null) {

                var etiqueta = $.grep($scope.etiquetas, function (obj) {
                    return obj.codigo === item.codigoetiqueta;
                });

                if ($scope.homologacoesanatel != undefined && $scope.homologacoesanatel != null) {
                    $scope.item.textocodigohomologacao = $.grep($scope.homologacoesanatel, function (obj) {
                        return obj.codigo === $scope.item.codigohomologacao;
                    });
                }

                if ($scope.item.tipoetiqueta == 1 && $scope.item.codigotipogeracao == undefined || $scope.item.codigotipogeracao == null) {
                    $scope.item.codigotipogeracao = 1;
                }


                //                var tipoetiquetaselecionada = $.grep($scope.tiposetiquetas, function (obj) {
                //                    return obj.codigo === $scope.item.tipoetiqueta;
                //                });
                if (etiqueta.length > 0) {
                    $scope.item.altura = etiqueta[0].altura;
                    $scope.item.largura = etiqueta[0].largura;

                    if ($scope.item.tipoetiqueta != 9 || $scope.item.tipoetiqueta == 9 && (($scope.item.serialinicial == undefined || $scope.item.serialinicial == null || $scope.item.serialinicial == '') && ($scope.item.serialfim == undefined || $scope.item.serialfim == null || $scope.item.serialfim == ''))) {
                        var quantidadeetiqueta = etiqueta[0].quantidadelinhas * etiqueta[0].quantidadecolunas;

                        $scope.item.quantidadesetiquetas = new Array(quantidadeetiqueta - 1);
                        for (i = 0; i <= quantidadeetiqueta - 1; i++) {
                            $scope.item.quantidadesetiquetas[i] = parseInt(i + 1);
                        }
                        $scope.item.quantidadeetiqueta = parseInt(quantidadeetiqueta);
                    }

                    $scope.item.registroanvisa = '';
                    if ($scope.homologacoesanvisa != undefined) {
                        $scope.item.registroanvisa = $.grep($scope.homologacoesanvisa, function (obj) {
                            return obj.codigo === $scope.item.codigohomologacao;
                        });
                    }

                    genericService.postRequisicao('EtiquetaBO', 'GerarCodigoBarras', { where: { codigotipocodigobarras: $scope.item.tipoetiqueta, codigogtin: $scope.item.globaltradeitemnumber, descricao: $scope.item.descricao, textolivre: $scope.item.textolivre, codigoposicao: $scope.item.codigoposicao, alturacodigobarras: etiqueta[0].height, larguracodigobarras: etiqueta[0].width, fontenome: item.fontenome, fontetamanho: item.fontetamanho, idealmodulewidth: etiqueta[0].idealmodulewidth, leftguard: etiqueta[0].leftguard, magnificationfactor: etiqueta[0].magnificationfactor, rightguard: etiqueta[0].rightguard, modelo128: item.modelo128, nrlote: item.nrlote, datavencimento: item.datavencimento, codigotipogtin: item.codigotipogtin, quantidadeporcaixa: item.quantidadeporcaixa, codigohomologacao: $scope.item.registroanvisa, serialinicial: item.serialinicial, serialfim: item.serialfim} }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.urletiqueta = result.data.imagedata;
                                $scope.atualizaAlinhamento();
                                $scope.atualizaPosicao();
                            }
                        },
                        $rootScope.TratarErro
                    );
                }
            } else {

                if ($scope.item.tipoetiqueta == 10 || $scope.item.tipoetiqueta == 18) {
                    $scope.visualizaCodigoBarras($scope.item);
                } else {
                    $scope.urletiqueta = "";
                }
            }
        }

        $scope.salvarCodigoBarras = function () {

            var dados = null;
            dados = angular.copy($scope.item);
            dados.codigofabricante = null;
            dados.codigoetiqueta = null;
            dados.quantidadeetiqueta = null;
            dados.codigotipogeracao = 4;

            $scope.item.registroanvisa = '';
            if ($scope.homologacoesanvisa != undefined) {
                $scope.item.registroanvisa = $.grep($scope.homologacoesanvisa, function (obj) {
                    return obj.codigo === $scope.item.codigohomologacao;
                });
            }

            genericService.postRequisicao('EtiquetaBO', 'CadastrarEtiqueta', { campos: dados }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        if ($scope.item != null && $scope.item != undefined && $scope.item.tipoetiqueta != null && $scope.item.tipoetiqueta != undefined && $scope.item.globaltradeitemnumber != null && $scope.item.globaltradeitemnumber != undefined) {
                            genericService.postRequisicao('EtiquetaBO', 'GerarCodigoBarras', { where: { codigotipocodigobarras: $scope.item.tipoetiqueta, codigogtin: $scope.item.globaltradeitemnumber, alturacodigobarras: $scope.item.alturacodigobarras, larguracodigobarras: $scope.item.larguracodigobarras, fontenome: $scope.item.fontenome, fontetamanho: $scope.item.fontetamanho, modelo128: $scope.item.modelo128, nrlote: $scope.item.nrlote, datavencimento: $scope.item.datavencimento, codigotipogtin: $scope.item.codigotipogtin, quantidadeporcaixa: $scope.item.quantidadeporcaixa, codigohomologacao: $scope.item.registroanvisa, serialinicial: $scope.item.serialinicial, serialfim: $scope.item.serialfim} }).then(
                                function (result) {
                                    if (result.data != undefined && result.data != null) {
                                        $scope.urlcodigobarras = result.data.imagedata;
                                        var mensagem = $translate.instant('17.MENSAGEM_ImagemGeradaSucesso');
                                        $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_ImagemGeradaSucesso' ? 'Imagem gerada com sucesso.' : mensagem);
                                        $scope.tableEtiquetas.page(1);
                                        $scope.tableEtiquetas.reload();
                                        $scope.wasGenerated = true;
                                        download('data:image/png;base64,' + $scope.urlcodigobarras, 'codigodebarras.png', 'image/png');
                                    }
                                },
                                $rootScope.TratarErro
                            );
                        }
                    }
                },
                $rootScope.TratarErro
            );

        }

        $scope.gerarPDF = function () {

            var etiquetaselecionada = $.grep($scope.etiquetas, function (obj) {
                return obj.codigo === $scope.item.codigoetiqueta;
            });

            var fabricanteselecionado = $.grep($scope.fabricantes, function (obj) {
                return obj.codigo === $scope.item.codigofabricante;
            });



            var largurapagina = 210;
            var alturapagina = 297;
            if (etiquetaselecionada[0].tamanhofolha == 'CARTA') {
                largurapagina = 215.9;
                alturapagina = 279.4;
            }

            var margemesquerda = 2;

            var largurasemborda = largurapagina - etiquetaselecionada[0].margemesquerda - etiquetaselecionada[0].margemdireita;
            var alturasemborda = alturapagina - etiquetaselecionada[0].margemsuperior - etiquetaselecionada[0].margeminferior;


            var alturacodigobarras = etiquetaselecionada[0].height + 6.3;
            var larguracodigobarras = etiquetaselecionada[0].width;

            //GTIN-8
            if ($scope.item.tipoetiqueta == 6) {
                alturacodigobarras = etiquetaselecionada[0].height + 4.3;
                larguracodigobarras = etiquetaselecionada[0].width - 2.0;
            }

            //GTIN-12
            if ($scope.item.tipoetiqueta == 26) {
                alturacodigobarras = etiquetaselecionada[0].height + 6.2;
                larguracodigobarras = etiquetaselecionada[0].width + 1.6;
            }

            //GTIN-14
            if ($scope.item.tipoetiqueta == 24) {
                //alturacodigobarras = etiquetaselecionada[0].height + 9.6;
                //larguracodigobarras = etiquetaselecionada[0].width + (etiquetaselecionada[0].idealmodulewidth * etiquetaselecionada[0].rightguard) + (etiquetaselecionada[0].idealmodulewidth * etiquetaselecionada[0].leftguard) + 9.6;

                //alturacodigobarras = etiquetaselecionada[0].height + 9.6 + 10.72;
                //larguracodigobarras = etiquetaselecionada[0].width - 2;

                alturacodigobarras = etiquetaselecionada[0].height;
                larguracodigobarras = etiquetaselecionada[0].width;
                margemesquerda = 0
            }

            if ($scope.item.tipoetiqueta == 18) {
                alturacodigobarras = "";
                larguracodigobarras = etiquetaselecionada[0].width;
            }

            if ($scope.item.tipoetiqueta == 10) {
                alturacodigobarras = etiquetaselecionada[0].height;
                larguracodigobarras = "";
            }

            //128
            if ($scope.item.tipoetiqueta == 1) {
                alturacodigobarras = etiquetaselecionada[0].height + 4.6;
                larguracodigobarras = etiquetaselecionada[0].width + 1;

                if ($scope.item.modelo128 == 1) {
                    alturacodigobarras = etiquetaselecionada[0].height + 6.6;
                    larguracodigobarras = etiquetaselecionada[0].width + 1;
                }
            }

            if ($scope.item.codigotipogeracao == 1) {
                //                var estilo = "<style>#urletiqueta { width: " + etiquetaselecionada[0].largura * 0.8 + "mm; height: " + etiquetaselecionada[0].altura * 0.8 + "mm;margin-left:10%} ";
                //                estilo += " .formatoEtiqueta { width: " + etiquetaselecionada[0].largura * 0.8 + "mm; height: " + etiquetaselecionada[0].altura * 0.8 + "mm}";

                //                angular.element($('.blocoimagem')).attr('style', 'width:' + etiquetaselecionada[0].largura * 0.8 + 'mm !important; height:' + etiquetaselecionada[0].altura * 0.8 + 'mm !important');
                //                angular.element($('#urletiqueta')).attr('style', 'width:' + etiquetaselecionada[0].largura * 0.8 + 'mm !important; height:' + etiquetaselecionada[0].altura * 0.8 + 'mm !important;margin-left:10%');

                var estilo = "<style>";
                //estilo += " <style>#urletiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm;margin-left:" + margemesquerda + "mm} ";
                estilo += " .formatoEtiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm}";
                estilo += " .logoAnatel { width: " + (larguracodigobarras + 4) + "mm;text-align:center}";

                if ($scope.item.modelo128 == 1) {
                    estilo += " .linhaGS1128 { margin-top: -1px; display: block; border: solid 1px; width:101mm; height: 11mm; text-align: center; margin-bottom: -1px;}";
                    estilo += " .div128 { width: 101mm; height: 13mm; border: solid 1px; margin-top: -1px; font-size: 12.5px; }";
                    estilo += " .celulaGS1128Left { margin-top: 0px; display: block; outline: solid 1px; width: 51mm; text-align: center; margin-left: 0mm; height: 13mm; float: left; margin-left: 1px; outline: none; }";
                    estilo += " .celulaGS1128Right { margin-top: -13mm; display: block; outline: solid 1px; width: 51mm; text-align: center; margin-left: 0mm; height: 13mm; float: right; }";
                    estilo += " .linhaGS1128-bottom { height: 80mm !important; padding-top: 4mm; }";
                    estilo += " body { margin-top: 10mm; }";
                    estilo += " .blocodescricao { height: 8mm; display: table-cell; height: 8mm; vertical-align: middle; }";
                    estilo += " .blocosscc { height: 6mm; }";
                    estilo += " .div128.div128ClassRow { height: 10mm !important; }";
                    estilo += " .div128.div128ClassRow .celulaGS1128Right { height: 10mm !important; margin-top: -10mm; }";
                    estilo += " .div128.div128ClassRow .celulaGS1128Left { height: 10mm !important; }";
                    estilo += " .bloconomeempresa { border-top: 1px solid; }";
                } else {
                    estilo += " .linhaGS1128{ margin-top: -1px;display: block;border: solid 1px;width:104mm;height: 11mm;text-align: center;}";
                    estilo += " .div128{width: 104mm;height: 12mm;border: solid 1px; margin-top: -1px;}";
                    estilo += " .celulaGS1128Left{margin-top: 0px;display: block;outline: solid 1px;width: 52mm;text-align: center;margin-left: 0mm;height: 12mm;float: left;margin-left: 1px;outline: none;}";
                    estilo += " .celulaGS1128Right{margin-top: -12mm;display: block;outline: solid 1px;width: 52mm;text-align: center;margin-left: 0mm;height: 12mm;float: right;}";
                }

                var margemacimacodigo = 4;

                if ($scope.item.codigoetiqueta == 4) { //A4356
                    margemacimacodigo = 1;
                }


                if ($scope.item.tipoetiqueta != 9 && $scope.item.tipoetiqueta != 12) {

                    //estilo += " #urletiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm;margin-left:" + margemesquerda + "mm} ";

                    //angular.element($('.blocoimagem')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important');

                    if ($scope.item.modelo128 == 1) {
                        //angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;margin-left:2mm;margin-top:4mm !important;');
                        estilo += " #urletiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm !important;margin: auto;margin-left:2mm;margin-top:" + margemacimacodigo  + "mm !important;} ";
                    } else {
                        //angular.element($('#urletiqueta')).attr('style', 'width:' + larguracodigobarras + 'mm !important; height:' + alturacodigobarras + 'mm !important;margin-left:4mm');
                        estilo += " #urletiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm !important;margin: auto;margin-left:4mm} ";
                    }
                } else {
                    estilo += " #urletiqueta {margin: auto;} ";
                }

                if ($scope.item.tipoetiqueta == 9) {
                    //estilo += " .blocoimagem {position:absolute; width: " + larguracodigobarras / 2 + "mm; height: " + alturacodigobarras + "mm;margin-bottom:1mm;} ";
                    estilo += " .blocotexto {position: relative; overflow: hidden; float: right; left: 2mm; word-wrap: break-word; display: block;width: " + (etiquetaselecionada[0].largura - larguracodigobarras) + "mm; height: " + (etiquetaselecionada[0].altura - 1) + "mm;margin-top:1mm;} ";
                }

                estilo += " #meio {text-align: center;} ";

            } else {

                var estilo = "<style>#urletiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm} ";
                estilo += " .formatoEtiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm}";
                estilo += " .logoAnatel { width: " + (larguracodigobarras + 4) + "mm;text-align:center}";

                var margemdireita = 2;
                var ajustelargura = 4;
                var margemacimacodigo = 1;
                var margemacimatexto = 1;
                var margemimagemesquerda = 1;
                ajusteAbaixo = 3;
                ajusteAcima = 3.3;


                if ($scope.item.codigoetiqueta == 8) { //CA4255
                    ajustelargura = 7;
                    margemdireita = 3;
                    ajusteAcima = 0;
                }

                if ($scope.item.codigoetiqueta == 9) { //CA4262
                    ajustelargura = 7;
                    margemdireita = 3;
                    ajusteAcima = 0;
                }


                if ($scope.item.codigoetiqueta == 10) { //CC283
                    margemdireita = 2;
                }

                if ($scope.item.codigoetiqueta == 4) { //A4256 - A4356
                    margemdireita = 2;
                    margemacimacodigo = 0;
                    margemacimatexto = 0;
                }

                if ($scope.item.codigoetiqueta == 6) { //6283                    
                    margemdireita = 1;
                    margemacimacodigo = 3;
                    margemacimatexto = 6;
                }



                if ($scope.item.codigoposicao == 1) {

                    if ($scope.item.codigoetiqueta == 7) { //L7160
                        ajustelargura = 6;
                    }

                    estilo += " .blocoimagem {position:absolute; width: " + larguracodigobarras / 2 + "mm; height: " + alturacodigobarras + "mm;margin-bottom:1mm;margin-left:" + margemimagemesquerda + "mm;margin-top:" + margemacimacodigo + "mm;} ";

                    if ((etiquetaselecionada[0].largura - larguracodigobarras - ajustelargura) > 0) {
                        estilo += " .blocotexto {position: relative; overflow: hidden; float: right;word-wrap: break-word; display: block;width: " + (etiquetaselecionada[0].largura - larguracodigobarras - ajustelargura) + "mm; height: " + (etiquetaselecionada[0].altura - 1) + "mm;margin-top:" + margemacimatexto + "mm;margin-right:" + margemdireita + "mm} ";
                    } else {
                        estilo += " .blocotexto {position: relative; overflow: hidden; float: right;word-wrap: break-word; display: block;width:0mm; height: " + (etiquetaselecionada[0].altura - 1) + "mm;margin-top:" + margemacimatexto + "mm;margin-right:" + margemdireita + "mm} ";
                    }
                }

                if ($scope.item.codigoposicao == 2) {
                    estilo += " .blocoimagem {position: relative; float: right; left:4mm; margin-right:5mm; width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm;margin-bottom:1mm;margin-top:" + margemacima + "mm;} ";

                    if ((etiquetaselecionada[0].largura - larguracodigobarras - ajustelargura) > 0) {
                        estilo += " .blocotexto {position: absolute; overflow: hidden; word-wrap: break-word; display: block; width: " + (etiquetaselecionada[0].largura - larguracodigobarras - ajustelargura) + "mm; height: " + (etiquetaselecionada[0].altura - 1) + "mm;margin-top:" + margemacimacodigo + "mm;margin-left:2mm;} ";
                    } else {
                        estilo += " .blocotexto {position: absolute; overflow: hidden; word-wrap: break-word; display: block; width:0mm; height: " + (etiquetaselecionada[0].altura - 1) + "mm;margin-top:" + margemacimatexto + "mm;margin-left:2mm;} ";
                    }
                }

                if ($scope.item.codigoetiqueta == 7) { //L7160
                    ajusteAbaixo = 2;
                    ajusteAcima = 2;
                }

                if ($scope.item.codigoetiqueta == 8) { //CA4255       
                    ajusteAcima = 2;
                    ajusteTextoAcima = -3;
                }

                if ($scope.item.codigoposicao == 3) {
                    estilo += " .blocoimagem {position:relative;left:" + ((etiquetaselecionada[0].largura - larguracodigobarras) / 4) + "mm;top:1mm;}";

                    if ($scope.item.codigoetiqueta == 4) { //A4256
                        estilo += " .blocotexto {position: relative; overflow: hidden; word-wrap: break-word; display: none; width: " + (etiquetaselecionada[0].largura - 3) + "mm; height: " + (etiquetaselecionada[0].altura - alturacodigobarras - ajusteAbaixo) + "mm} ";
                    } else {
                        estilo += " .blocotexto {position: relative; overflow: hidden; word-wrap: break-word; display: block; width: " + (etiquetaselecionada[0].largura - 3) + "mm; height: " + (etiquetaselecionada[0].altura - alturacodigobarras - ajusteAbaixo) + "mm;margin-left:2mm;} ";
                    }
                }

                if ($scope.item.codigoposicao == 4) {
                    estilo += " .blocoimagem {position: relative; left:" + ((etiquetaselecionada[0].largura - larguracodigobarras) / 4) + "mm; width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm} ";
                    if ($scope.item.codigoetiqueta == 4) { //A4256
                        estilo += " .blocotexto {position: relative; overflow: hidden; word-wrap: break-word; display: none; width: " + (etiquetaselecionada[0].largura - 3) + "mm; height: " + (etiquetaselecionada[0].altura - alturacodigobarras - ajusteAcima) + "mm;margin-bottom:1mm;} ";
                    } else {
                        estilo += " .blocotexto {position: relative; overflow: hidden; word-wrap: break-word; display: block; width: " + (etiquetaselecionada[0].largura - 3) + "mm; height: " + (etiquetaselecionada[0].altura - alturacodigobarras - ajusteAcima) + "mm;margin-bottom:1mm;margin-left:2mm;} ";
                    }

                }
            }


            estilo += " .descricaoetiqueta {font-weight:bolder; font-size: " + $scope.item.fontetamanhodescricao + "px; font-family: " + $scope.item.fontenomedescricao + " !important;display: block;text-align: " + $scope.item.alinhamentodescricao + "}";
            estilo += " .divLinha {display: table;}";

            //estilo += " .divCelula {outline: 1px dotted;float: left;width: " + etiquetaselecionada[0].largura + "mm; height: " + etiquetaselecionada[0].altura + "mm;} ";
            estilo += " .divCelula {float: left;width: " + etiquetaselecionada[0].largura + "mm; height: " + etiquetaselecionada[0].altura + "mm;} ";

            estilo += " .divLinhaSpacer {height: " + etiquetaselecionada[0].espacamentovertical + "mm;}";
            estilo += " .divCelulaSpacer {width: " + etiquetaselecionada[0].espacamentohorizontal + "mm;}";
            estilo += " body {margin: 0px;}";

            estilo += " </style>";

            var etiqueta = $("<div></div>").attr('id', 'etiqueta').append(document.getElementById('etiqueta').innerHTML.replace($(etiqueta).find('.blocotexto').attr('style'), '').replace($(etiqueta).find('.blocotexto').attr('style'), ''));
            etiqueta.children().children().each(function () {
                $(this).removeAttr('style');
            });

            etiqueta.find('#urletiqueta').each(function () {
                $(this).removeAttr('style');
            });

            if ($scope.item.tipoetiqueta == 9) {

                etiqueta.find('.blocotexto').each(function () {
                    $(this).removeAttr('style');
                });
            }

            if ($scope.item.tipoetiqueta != 1 && $scope.item.tipoetiqueta != 9) {
                if ($scope.item.codigoposicao == 1) {
                    etiqueta[0].children.acima.remove();
                    etiqueta[0].children.abaixo.remove();
                    etiqueta[0].children.meio.children.esquerda.remove();
                } else if ($scope.item.codigoposicao == 2) {
                    etiqueta[0].children.acima.remove();
                    etiqueta[0].children.abaixo.remove();
                    etiqueta[0].children.meio.children.direita.remove();
                } else if ($scope.item.codigoposicao == 3) {
                    etiqueta[0].children.acima.remove();
                    etiqueta[0].children.meio.children.esquerda.remove();
                    etiqueta[0].children.meio.children.direita.remove();
                } else if ($scope.item.codigoposicao == 4) {
                    etiqueta[0].children.abaixo.remove();
                    etiqueta[0].children.meio.children.esquerda.remove();
                    etiqueta[0].children.meio.children.direita.remove();
                }
            }

            var linha = "";
            var qtdconcat = 0;
            var contadorSerial = 0;
            var contadorpaginas = 0;
            var paginas = Math.ceil($scope.item.quantidadeetiqueta / (etiquetaselecionada[0].quantidadelinhas * etiquetaselecionada[0].quantidadecolunas));
            contadorSerial = $scope.item.serialinicial != undefined && $scope.item.serialinicial != '' ? $scope.item.serialinicial : 0;

            for (contadorpaginas = 0; contadorpaginas < paginas; contadorpaginas++) {
                for (i = 0; i < etiquetaselecionada[0].quantidadelinhas; i++) {
                    linha += "<div class='divLinha'>";
                    for (j = 0; j < etiquetaselecionada[0].quantidadecolunas; j++) {
                        if (qtdconcat < $scope.item.quantidadeetiqueta) {

                            if ($scope.item.tipoetiqueta == 9) {

                                etiqueta.find('#urletiqueta')[0].src = "//[[[img" + contadorSerial;

                                var urletiqueta = etiqueta.find('#urletiqueta');
                                $(urletiqueta[0].attributes).each(function () {
                                    if (this.nodeName == 'data-ng-src') {
                                        this.nodeValue = "//[[[img" + contadorSerial;
                                    }
                                });
                                angular.element("#urletiqueta").addClass("img" + contadorSerial);

                                var serianinicial = etiqueta.find('.serialinicial');
                                serianinicial.text("(21) " + contadorSerial);

                                contadorSerial++;
                            }

                            linha += "<div class='divCelula' style='padding-top: 0.5mm;'>" + etiqueta[0].innerHTML + "</div>";

                            qtdconcat++;
                        } else {
                            linha += "<div class='divCelula'></div>";
                        }
                        if (j + 1 < etiquetaselecionada[0].quantidadecolunas) {

                            linha += "<div class='divCelulaSpacer' style='float: left;width: " + etiquetaselecionada[0].espacamentohorizontal + "mm;height:1mm;'></div>";
                        }
                    }
                    linha += "</div>";

                    if (i + 1 < etiquetaselecionada[0].quantidadelinhas) {

                        if ($scope.item.tipoetiqueta == 9) {
                            if (etiquetaselecionada[0].codigo == 7 || etiquetaselecionada[0].codigo == 8) {
                                linha += "<div class='divLinhaSpacer' style='height: " + parseFloat(etiquetaselecionada[0].espacamentovertical - 1.5) + "mm;'></div>";
                            }
                            else if (etiquetaselecionada[0].codigo == 9) {
                                linha += "<div class='divLinhaSpacer' style='height: " + parseFloat(etiquetaselecionada[0].espacamentovertical - 3) + "mm;'></div>";
                            }
                            else if (etiquetaselecionada[0].codigo == 11) {
                                linha += "<div class='divLinhaSpacer' style='height: " + parseFloat(etiquetaselecionada[0].espacamentovertical + 2) + "mm;'></div>";
                            }
                            else if (etiquetaselecionada[0].codigo == 2) {
                                linha += "<div class='divLinhaSpacer' style='height: " + parseFloat(etiquetaselecionada[0].espacamentovertical + 20) + "mm;'></div>";
                            }
                            else {
                                linha += "<div class='divLinhaSpacer' style='height: " + parseFloat(etiquetaselecionada[0].espacamentovertical - 3.5) + "mm;'></div>";
                            }
                        }
                        else {
                            linha += "<div class='divLinhaSpacer' style='height: " + etiquetaselecionada[0].espacamentovertical + "mm;'></div>";
                        }

                    }
                    else {
                        if ($scope.item.tipoetiqueta == 9 && etiquetaselecionada[0].codigo == 2) {
                            linha += "<div class='divLinhaSpacer' style='height: " + parseFloat(etiquetaselecionada[0].espacamentovertical + 15.3) + "mm;'></div>";
                        }
                    }
                }
            }

            var relatorio = $("<div></div>").append(estilo).append($('<div class="pdfContent"></div>').append(linha));
            $('#pdfdata').val(relatorio[0].innerHTML);
            var nomerelatorio = $scope.item.globaltradeitemnumber + "_" + fabricanteselecionado[0].nome + "_" + etiquetaselecionada[0].nome;

            var margemsuperiorenvio = 0, margininferiorenvio = 0;

            //GS1 128
            if ($scope.item != undefined && $scope.item.tipoetiqueta == 1 && $scope.item.modelo128 == 1) {
                margemsuperiorenvio = parseFloat(etiquetaselecionada[0].margemsuperior - 3) < 0 ? 0 : parseFloat(etiquetaselecionada[0].margemsuperior - 3);
                margininferiorenvio = parseFloat(etiquetaselecionada[0].margeminferior - 3) < 0 ? 0 : parseFloat(etiquetaselecionada[0].margeminferior - 3);
            }
            //DataMatrix
            else if ($scope.item != undefined && $scope.item.tipoetiqueta == 9) {
                //8096
                if (etiquetaselecionada[0].codigo == 2) {
                    margemsuperiorenvio = parseFloat(etiquetaselecionada[0].margemsuperior + 10);
                    margininferiorenvio = etiquetaselecionada[0].margeminferior;
                }
                //6288
                else if (etiquetaselecionada[0].codigo == 11) {
                    margemsuperiorenvio = parseFloat(etiquetaselecionada[0].margemsuperior);
                    margininferiorenvio = etiquetaselecionada[0].margeminferior;
                }
                else {
                    margemsuperiorenvio = parseFloat(etiquetaselecionada[0].margemsuperior - 9.3) < 0 ? 0 : parseFloat(etiquetaselecionada[0].margemsuperior - 9.3);
                    margininferiorenvio = etiquetaselecionada[0].margeminferior;
                }
            }
            else {
                margemsuperiorenvio = etiquetaselecionada[0].margemsuperior;
                margininferiorenvio = etiquetaselecionada[0].margeminferior;
            }

            var campos = angular.copy($scope.item);

            if (campos && campos.productdescription)
                delete campos.productdescription;
            if (campos && campos.nomeassociado)
                delete campos.nomeassociado;

            var url = '/PDFHandler.ashx?nomeetiqueta=' + nomerelatorio + '&tamanhofolha=' + etiquetaselecionada[0].tamanhofolha + '&margemesquerda=' + etiquetaselecionada[0].margemesquerda;
            url += '&margemdireita=' + etiquetaselecionada[0].margemdireita + '&margemsuperior=' + margemsuperiorenvio + '&margeminferior=' + margininferiorenvio;
            url += '&largurapagina=' + largurapagina + '&alturapagina=' + alturapagina + '&where=' + JSON.stringify(etiquetaselecionada[0]) + '&campos=' + JSON.stringify(campos);

            $("#PDFHandler").attr('action', url);
            $('#PDFHandler').submit();
        }


        $scope.salvarEtiqueta = function () {
            var etiquetaselecionada = $.grep($scope.etiquetas, function (obj) {
                return obj.codigo === $scope.item.codigoetiqueta;
            });

            //            var altura = Math.ceil(Math.round((etiquetaselecionada[0].altura * 250 / 50.80) * 100) / 100);
            //            var largura = Math.ceil(Math.round((etiquetaselecionada[0].largura * 632 / 101.60) * 100) / 100);

            var altura = etiquetaselecionada[0].altura;
            var largura = etiquetaselecionada[0].largura;

            var alturacodigobarras = etiquetaselecionada[0].height
            var larguracodigobarras = etiquetaselecionada[0].width - (etiquetaselecionada[0].rightguard * 0.33);

            var estilo = "<style>#urletiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm} ";
            estilo += " .formatoEtiqueta { width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm}";

            if ($scope.item.codigoposicao == 1) {
                estilo += " .blocoimagem {position:absolute; width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm;} ";
                estilo += " .blocotexto {position: relative; overflow: hidden; float: right; left: " + ((etiquetaselecionada[0].rightguard * 0.33) + 2) + "mm; word-wrap: break-word; width: " + (largura - larguracodigobarras) + "mm; height: " + altura + "mm} ";
            }

            if ($scope.item.codigoposicao == 2) {
                estilo += " .blocoimagem {position: relative; float: right; left:" + (etiquetaselecionada[0].lefttguard * 0.33) + "mm; margin-right:2mm; width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm} ";
                estilo += " .blocotexto {position: absolute; overflow: hidden; word-wrap: break-word; width: " + (largura - larguracodigobarras) + "mm; height: " + altura + "mm} ";
            }

            ajusteAbaixo = 3;
            ajusteAcima = 3;
            ajusteTextoAcima = 0;


            if ($scope.item.codigoetiqueta == 7) { //L7160                    
                ajusteAcima = 2;
                ajusteTextoAcima = -1;
            }

            if ($scope.item.codigoetiqueta == 8) { //CA4255       
                ajusteAcima = 2;
                ajusteTextoAcima = -3;
            }

            if ($scope.item.codigoetiqueta == 4) { //A4256
                ajusteAbaixo = 0;
                ajusteAcima = 0;
            }

            if ($scope.item.codigoposicao == 3) {
                estilo += " .blocoimagem {position:relative;left:" + ((largura - larguracodigobarras) / 2) + "mm; width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm}";
                estilo += " .blocotexto {position: relative; overflow: hidden; word-wrap: break-word; width: " + largura + "mm; height: " + (altura - alturacodigobarras) + "mm;top:" + alturacodigobarras + "mm;display:block;} ";

                if ($scope.item.codigoetiqueta == 4) { //A4256
                    estilo += " .blocotexto {position: relative; overflow: hidden; word-wrap: break-word; width: " + largura + "mm; height: " + (altura - alturacodigobarras) + "mm;top:" + alturacodigobarras + "mm;display:none;} ";
                }
            }

            if ($scope.item.codigoposicao == 4) {
                estilo += " .blocoimagem {position:relative;left:" + ((largura - larguracodigobarras) / 2) + "mm; width: " + larguracodigobarras + "mm; height: " + alturacodigobarras + "mm;top:" + (altura - alturacodigobarras + ajusteAcima) + "}";
                estilo += " .blocotexto {position: relative; overflow: hidden; word-wrap: break-word; width: " + largura + "mm; height: " + (altura - alturacodigobarras - ajusteTextoAcima) + "mm} ";
            }

            estilo += " .descricaoetiqueta {font-weight:bolder; font-size: 11px; font-family: Arial !important;display: block}";
            estilo += " .textoetiqueta {font-size: 10px; font-family: Arial !important;}";
            estilo += " #etiqueta {font-family: Arial !important;}";
            estilo += " .divLinha {display: table;}";
            estilo += " .divCelula {float: left;width: " + largura + "mm; height: " + altura + "mm} ";
            estilo += " </style>";

            //var etiqueta = $("<div></div>").attr('id', 'etiqueta').append(document.getElementById('etiqueta').innerHTML.replace($(etiqueta).find('.blocotexto').attr('style'), ''));
            var etiqueta = $("<div></div>").attr('id', 'etiqueta').append(document.getElementById('etiqueta').innerHTML.replace($(etiqueta).find('.blocotexto').attr('style'), '').replace($(etiqueta).find('.blocotexto').attr('style'), ''));
            etiqueta.children().children().each(function () {
                $(this).removeAttr('style');
            });

            if ($scope.item.codigoposicao == 1) {
                etiqueta[0].children.acima.remove();
                etiqueta[0].children.abaixo.remove();
                etiqueta[0].children.meio.children.esquerda.remove();
            } else if ($scope.item.codigoposicao == 2) {
                etiqueta[0].children.acima.remove();
                etiqueta[0].children.abaixo.remove();
                etiqueta[0].children.meio.children.direita.remove();
            } else if ($scope.item.codigoposicao == 3) {
                etiqueta[0].children.acima.remove();
                etiqueta[0].children.meio.children.esquerda.remove();
                etiqueta[0].children.meio.children.direita.remove();
            } else if ($scope.item.codigoposicao == 4) {
                etiqueta[0].children.abaixo.remove();
                etiqueta[0].children.meio.children.esquerda.remove();
                etiqueta[0].children.meio.children.direita.remove();
            }

            var relatorio = $("<div></div>").append(estilo).append($('<div></div>').attr('class', 'divCelula').append(etiqueta[0].innerHTML));
            $('#imagedata').val(relatorio[0].innerHTML);
            var nomeetiqueta = 'etiqueta';
            var url = '/ImageHandler.ashx?nomeetiqueta=' + nomeetiqueta;
            url += '&largura=' + ((largura * 300) / 254) + '&altura=' + ((altura * 300) / 254);
            $("#ImageHandler").attr('action', url);
            $('#ImageHandler').submit();
        }

        $scope.onLoad = function () {
            $rootScope.verificaAssociadoSelecionado(function (data) {
                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                    $rootScope.flagAssociadoSelecionado = true;
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }
            });
        }

        //Salvar na base de dados a etiqueta gerada
        $scope.onSave = function (item) {
            $scope.ValidaTamanhoTextoEtiqueta().then(
                function (result) {
                    if (result) {
                        if ($scope.item.codigofabricante != undefined && $scope.item.codigofabricante != null && $scope.item.codigofabricante != ""
                                && $scope.item.codigoetiqueta != undefined && $scope.item.codigoetiqueta != null && $scope.item.codigoetiqueta != ""
                                && $scope.item.quantidadeetiqueta != undefined && $scope.item.quantidadeetiqueta != null && $scope.item.quantidadeetiqueta != ""
                                && $scope.item.codigotipogeracao != undefined && $scope.item.codigotipogeracao != null && $scope.item.codigotipogeracao != "" && $scope.item.codigotipogeracao != 4) {

                            var dados = null;
                            dados = angular.copy($scope.item);

                            genericService.postRequisicao('EtiquetaBO', 'CadastrarEtiqueta', { campos: dados }).then(
                                    function (result) {
                                        if (result.data != undefined && result.data != null) {
                                            $scope.tableEtiquetas.page(1);
                                            $scope.tableEtiquetas.reload();
                                            $scope.gerarPDF();
                                            $scope.wasGenerated = true;
                                            var mensagem = $translate.instant('17.MENSAGEM_EtiquetaGeradaSucesso');
                                            $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_EtiquetaGeradaSucesso' ? 'Etiqueta gerada com sucesso.' : mensagem);
                                        }
                                    },
                                    $rootScope.TratarErro
                                );
                        } else {
                            var mensagem = $translate.instant('17.MENSAGEM_InformeFabricanteEtiquetaTipo');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_InformeFabricanteEtiquetaTipo' ? 'Campos obrigatórios não preenchidos. Informe o Fabricante, Etiqueta, Quantidade e o Tipo.' : mensagem);
                        }
                    }
                }

        );




        }





        //Metodo para abertura do modal de selecao de produto
        $scope.onLote = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalLoteGeracaoEtiqueta',
                controller: ModalLoteGeracaoEtiquetaCtrl,
                size: 'lg',
                resolve: {
                    item: function () {
                        var produto = angular.copy(item);
                        return produto;
                    }
                }
            });

            modalInstance.result.then(function (data) {

                $scope.item.nrlote = data.nrlote;
                $scope.item.datavencimento = data.datavencimento;

                //Status do GTIN
                //                genericService.postRequisicao('BarCodeTypeListBO', 'buscarTipoCodigoBarras', { where: { codigotipogtin: data.codigotipogtin} }).then(
                //                    function (result) {
                //                        if (result.data != undefined && result.data != null) {
                //                            if (result.data.length > 0) {
                //                                $scope.tiposetiquetas = result.data;
                //                            } else {
                //                                var mensagem = $translate.instant('17.MENSAGEM_NaoEncontradoCodigoBarrasDisponiveis');
                //                                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_NaoEncontradoCodigoBarrasDisponiveis' ? 'Não foram encontrados tipos de código de barras disponíveis para este tipo de GTIN.' : mensagem);
                //                            }
                //                        } else {
                //                            var mensagem = $translate.instant('17.MENSAGEM_ErroBuscarCodigoBarras');
                //                            $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_ErroBuscarCodigoBarras' ? 'Erro ao buscar os tipos de códigos de barras.' : mensagem);
                //                        }
                //                    },
                //                    $rootScope.TratarErro
                //                );

                //                $scope.BuscarTipoGeracao();
                //                $scope.BuscarPosicaoTexto();
                //                $scope.BuscarFabricantes();


            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });


        }


        $scope.OnModelo128 = function (item) {
            if (item.modelo128 == 2) {
                $scope.BuscarHomologacoesAnatel();
            }
        }


        //Buscar Homologacoes Anatel
        $scope.BuscarHomologacoesAnatel = function () {
            return $q(function (resolve, reject) {
                var dados = null;
                dados = angular.copy($scope.item);
                genericService.postRequisicao('EtiquetaBO', 'BuscarHomologacoesAnatel', { campos: dados }).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.homologacoesanatel = result.data;
                            //Usado para corrigir bug no ie, no momento de esconder a grid e apresentar o form
                            $('#formcontainer').focus();
                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }

        //Buscar Homologacoes Anatel
        $scope.BuscarHomologacoesAnvisa = function () {
            return $q(function (resolve, reject) {
                var dados = null;
                dados = angular.copy($scope.item);
                genericService.postRequisicao('EtiquetaBO', 'BuscarHomologacoesAnvisa', { campos: dados }).then(
                    function (result) {
                        if (result.data != undefined && result.data != null) {
                            $scope.homologacoesanvisa = result.data;
                            //Usado para corrigir bug no ie, no momento de esconder a grid e apresentar o form
                            $('#formcontainer').focus();
                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }


        $scope.ValidaTamanhoTextoEtiqueta = function () {
            var deferred = $q.defer();

            if ($scope.item.codigotipogeracao == 2 || $scope.item.codigotipogeracao == 3) {
                var retorno = true;
                if ($scope.item.codigoposicao == 1) {
                    if ($('#etiqueta #direita.blocotexto .descricaoetiqueta').height() + $('#etiqueta #direita.blocotexto .textoetiqueta').height() > $('#etiqueta #direita.blocotexto').height())
                        retorno = false;
                } else if ($scope.item.codigoposicao == 2) {
                    if ($('#etiqueta #esquerda.blocotexto .descricaoetiqueta').height() + $('#etiqueta #esquerda.blocotexto .textoetiqueta').height() > $('#etiqueta #esquerda.blocotexto').height())
                        retorno = false;
                } else if ($scope.item.codigoposicao == 3) {
                    if ($('#etiqueta #abaixo .blocotexto .descricaoetiqueta').height() + $('#etiqueta #abaixo .blocotexto .textoetiqueta').height() > $('#etiqueta #abaixo .blocotexto').height())
                        retorno = false;
                } else if ($scope.item.codigoposicao == 4) {
                    if ($('#etiqueta #acima .blocotexto .descricaoetiqueta').height() + $('#etiqueta #acima .blocotexto .textoetiqueta').height() > $('#etiqueta #acima .blocotexto').height())
                        retorno = false;
                }
                if (!retorno) {
                    var mensagem = $translate.instant('17.MENSAGEM_ConfirmarLayoutIncorreto');
                    $scope.confirm(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_ConfirmarLayoutIncorreto' ? 'O texto informado excede o tamanho da etiqueta e será adequado automaticamente. Deseja realmente continuar?' : mensagem
                            , function () { deferred.resolve(true); }, function () { deferred.resolve(false); });
                } else {
                    deferred.resolve(true);
                }

            } else {
                deferred.resolve(true);
            }

            return deferred.promise;
        }





        $scope.onLoad();

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };
    } ]);

//Controller para métodos do modal
var ModalProdutoGeracaoEtiquetaCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, $translate, genericService, item, Utils, licencas, statusgtin) {

    $scope.itemGTIN = {};
    $scope.initModal = true;

    $scope.licencas = licencas;
    $scope.statusgtin = statusgtin;

    angular.element("#typea").focus();

    //Status do GTIN
    genericService.postRequisicao('StatusGTINBO', 'BuscarStatusGTINEtiquetas', {}).then(
        function (result) {
            if (result.data != undefined && result.data != null) {
                $scope.statusgtin = result.data;

                for (var i = 0; i < $scope.statusgtin.length; i++) {
                    if ($scope.statusgtin[i].codigo == 5) {
                        $scope.statusgtin.splice(i, 1);
                        break;
                    }
                }
            }
        },
        $rootScope.TratarErro
    );

    //Tipo GTIN
    genericService.postRequisicao('LicencaBO', 'buscarTipoGtinAssociado', {}).then(
        function (result) {
            if (result.data != undefined && result.data != null) {
                $scope.tiposgtin = result.data;
            }
        },
        $rootScope.TratarErro
    );

    $scope.tableProdutos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridProdutos($scope.itemForm, $scope.isPesquisa, function (data) {
                if (data != undefined && data != null) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                }
                $scope.produtos = data;
            });
        }
    });

    function carregaGridProdutos(item, isPesquisa, callback) {
        var dados = null;
        dados = angular.copy($scope.itemForm);
        if (dados != undefined) {

            genericService.postRequisicao('EtiquetaBO', 'PesquisaProdutosGeracaoEtiqueta', { campos: dados }).then(
                function (result) {
                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        } else {
            callback(null);
        }
    }

    //Carrega produtos cadastrados 
    $scope.BuscarProdutosAutoComplete = function (nome) {
        return genericService.postRequisicao('EtiquetaBO', 'BuscarProdutosAutoCompleteGeracaoEtiqueta', { where: { nome: nome} }).then(
            function (result) {
                var produtosAutoComplete = [];
                angular.forEach(result.data, function (item) {
                    produtosAutoComplete.push({ globaltradeitemnumber: item.globaltradeitemnumber, productdescription: item.productdescription, codigoproduto: item.codigoproduto });
                });
                return produtosAutoComplete;
            }
        );
    }

    //Chamado ao selecionar a opção "Pesquisar"
    $scope.onFilter = function (item) {

        if (item == undefined) {
            var mensagem = $translate.instant('17.MENSAGEM_PreenchaUmDosCampos');
            $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_PreenchaUmDosCampos' ? 'Por favor, preencha um dos campos para realizar a pesquisa.' : mensagem);
        } else {
            if (item.pesquisaProduto != undefined) {
                if (item.pesquisaProduto.globaltradeitemnumber != undefined) {
                    item.globaltradeitemnumber = item.pesquisaProduto.globaltradeitemnumber;
                }
                else {
                    item.globaltradeitemnumber = null;
                }

                if (item.pesquisaProduto.productdescription != undefined) {
                    item.productdescription = item.pesquisaProduto.productdescription;
                }
                else {
                    item.productdescription = null;
                }

                if (item.pesquisaProduto.codigoproduto != undefined) {
                    item.codigoproduto = item.pesquisaProduto.codigoproduto;
                }
                else {
                    item.codigoproduto = null;
                }

            } else {

                if (item.globaltradeitemnumber == null)
                    item.globaltradeitemnumber = undefined;

                if (item.productdescription == null)
                    item.productdescription = undefined;

                if (item.codigoproduto == null)
                    item.codigoproduto = undefined;


            }
            if (
                (item.globaltradeitemnumber == undefined || item.globaltradeitemnumber == null)
                && (item.productdescription == undefined || item.productdescription == null)
                && (item.codigostatusgtin == undefined || item.codigostatusgtin == null)
                && (item.codigotipogtin == undefined || item.codigotipogtin == null)
                && (item.pesquisaProduto == undefined || item.pesquisaProduto == null)
                ) {
                var mensagem = $translate.instant('17.MENSAGEM_PreenchaUmDosCampos');
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_PreenchaUmDosCampos' ? 'Por favor, preencha um dos campos para realizar a pesquisa.' : mensagem);
            } else {
                $scope.itemForm = angular.copy(item);
                $scope.isPesquisa = true;
                $scope.tableProdutos.page(1);
                $scope.tableProdutos.reload();
            }
        }
    }

    //Chamado ao selecionar um PRODUTO no grid
    $scope.onSelectedProduto = function (item) {

        $modalInstance.close(item);
        //}, function () { }, 'Confirmação');
    }

    //Cancelar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }
}


//Controller para métodos do modal de Lote
var ModalLoteGeracaoEtiquetaCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $translate, $filter, genericService, item, Utils) {

    $scope.itemGTIN = {};
    $scope.initModal = true;
    $scope.codigoproduto = item.codigoproduto;

    $scope.tableLotes = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridLotes($scope.item, $scope.isPesquisa, function (data) {
                if (data != undefined && data != null) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                }
                $scope.produtos = data;
            });
        }
    });

    function carregaGridLotes(item, isPesquisa, callback) {
        var dados = null;
        dados = angular.copy($scope.itemForm);
        if (dados != undefined) {

            genericService.postRequisicao('EtiquetaBO', 'PesquisaLotesGeracaoEtiqueta', { campos: dados }).then(
                function (result) {
                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        } else {
            callback(null);
        }
    }

    //Chamado ao selecionar a opção "Pesquisar"
    $scope.onFilter = function (item) {
        if (item == undefined) {
            var mensagem = $translate.instant('17.MENSAGEM_PreenchaUmDosCampos');
            $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_PreenchaUmDosCampos' ? 'Por favor, preencha um dos campos para realizar a pesquisa.' : mensagem);
        } else {
            if (item.pesquisaLote != undefined) {
                if (item.pesquisaLote.de_dataprocessamento != undefined) {
                    item.de_dataprocessamento = item.pesquisaLote.de_dataprocessamento;
                }
                else {
                    item.de_dataprocessamento = null;
                }

                if (item.pesquisaLote.ate_dataprocessamento != undefined) {
                    item.ate_dataprocessamento = item.pesquisaLote.ate_dataprocessamento;
                }
                else {
                    item.ate_dataprocessamento = null;
                }


                if (item.pesquisaLote.de_datavencimento != undefined) {
                    item.de_datavencimento = item.pesquisaLote.de_datavencimento;
                }
                else {
                    item.de_datavencimento = null;
                }

                if (item.pesquisaLote.ate_datavencimento != undefined) {
                    item.ate_datavencimento = item.pesquisaLote.ate_datavencimento;
                }
                else {
                    item.ate_datavencimento = null;
                }

                if (item.pesquisaLote.nr_lote != undefined) {
                    item.nr_lote = item.pesquisaLote.nr_lote;
                }
                else {
                    item.nr_lote = null;
                }

                item.codigoproduto = $scope.codigoproduto;


            } else {

                if (item.de_dataprocessamento == null)
                    item.de_dataprocessamento = undefined;

                if (item.ate_dataprocessamento == null)
                    item.ate_dataprocessamento = undefined;


                if (item.de_datavencimento == null)
                    item.de_datavencimento = undefined;

                if (item.ate_datavencimento == null)
                    item.ate_datavencimento = undefined;

                if (item.nr_lote == null)
                    item.nr_lote = undefined;

            }

            if (
                (item.de_dataprocessamento == undefined || item.de_dataprocessamento == null)
                && (item.ate_dataprocessamento == undefined || item.ate_dataprocessamento == null)
                && (item.de_datavencimento == undefined || item.de_datavencimento == null)
                && (item.ate_datavencimento == undefined || item.ate_datavencimento == null)
                && (item.nr_lote == undefined || item.nr_lote == null)
                ) {
                var mensagem = $translate.instant('17.MENSAGEM_PreenchaUmDosCampos');
                $scope.alert(typeof mensagem === 'object' || mensagem == '17.MENSAGEM_PreenchaUmDosCampos' ? 'Por favor, preencha um dos campos para realizar a pesquisa.' : mensagem);
            } else {
                $scope.itemForm = angular.copy(item);
                $scope.isPesquisa = true;
                $scope.tableLotes.page(1);
                $scope.tableLotes.reload();
            }
        }
    }

    //Chamado ao selecionar um PRODUTO no grid
    $scope.onSelectedLote = function (item) {

        $modalInstance.close(item);
        //}, function () { }, 'Confirmação');
    }

    //Cancelar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }

}
