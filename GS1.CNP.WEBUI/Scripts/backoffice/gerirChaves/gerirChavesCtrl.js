﻿app.controller('GerirChavesCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {
    //inicializando abas do form
    $('#tabs-chavesGeradas a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var titulo = $translate.instant('13.LBL_GestaoChaves');
    $scope.titulo = typeof titulo === 'object' || titulo == '13.LBL_GestaoChaves' ? 'Gestão de Chaves' : titulo;
    
    $scope.tableChavesGeradas = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            datacriacao: 'desc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridChavesGeradas($scope.item, $scope.isPesquisa, function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                $scope.items = data;
            });
        }
    });

    function carregaGridChavesGeradas(item, isPesquisa, callback) {
        
        $rootScope.verificaAssociadoSelecionado(function (data) {

            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                genericService.postRequisicao('ChaveBO', 'BuscarChaves', { campos: {} }).then(
                    function (result) {

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
                $rootScope.flagAssociadoSelecionado = true;
            } else {
                $rootScope.flagAssociadoSelecionado = false;
            }
        });
    };

    $scope.tableGerarChaves = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridGerarChaves($scope.item, $scope.isPesquisa, function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                $scope.items = data;
            });
        }
    });

    function carregaGridGerarChaves(item, isPesquisa, callback) {

        $rootScope.verificaAssociadoSelecionado(function (data) {

            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                genericService.postRequisicao('ChaveBO', 'BuscarPrefixosSemChave', { campos: {} }).then(
                    function (result) {

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
                $rootScope.flagAssociadoSelecionado = true;
            } else {
                $rootScope.flagAssociadoSelecionado = false;
            }
        });

    };

    $scope.gerarChave = function (item) {
        $rootScope.verificaPermissao('ChaveBO', 'GerarChave', function () {
            Utils.bloquearCamposVisualizacao("VisualizarChave");

            genericService.postRequisicao('ChaveBO', 'GerarChave', { campos: item }).then(
                function (result) {

                    $scope.tableChavesGeradas.page(1);
                    $scope.tableChavesGeradas.reload();
                    Utils.moveScrollTop();
                    $scope.tableGerarChaves.page(1);
                    $scope.tableGerarChaves.reload();

                    var mensagem = $translate.instant('13.MENSAGEM_ChaveGeradaSucesso');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '13.MENSAGEM_ChaveGeradaSucesso' ? 'Chave gerada com sucesso.' : mensagem);
                },
                $rootScope.TratarErro
            );
        });
    }

    $scope.baixarCarta = function (item) {

        if (item != undefined && item != null) {
            
            item.currentDate = Utils.longDate();

            var dados = angular.copy(item);

            dados.prefixo = "";
            dados.prefixo = dados.prefixos.toString();
            dados.prefixos = dados.prefixo;

            $("#worddata").val(JSON.stringify({ 'campos': dados }));
            $('#WordHandler').submit();

            $timeout(function () {
                blockUI.stop();
            }, 100);
        }
        else {
            $("#WordHandler").attr('action', '/WordHandler.ashx');
            $('#WordHandler').submit();
            blockUI.stop();
        }
    }

    $scope.expirarChave = function (item) {
        $rootScope.verificaPermissao('ChaveBO', 'ExpirarChave', function () {
            Utils.bloquearCamposVisualizacao("VisualizarChave");

            $translate('13.MENSAGEM_DesejaExpirarChave').then(function (resultado) {
                $scope.confirm(resultado, function () {
                    genericService.postRequisicao('ChaveBO', 'ExpirarChave', { campos: item }).then(
                        function (result) {

                            $scope.tableChavesGeradas.page(1);
                            $scope.tableChavesGeradas.reload();

                            var mensagem = $translate.instant('13.MENSAGEM_ChaveExpirada');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '13.MENSAGEM_ChaveExpirada' ? 'A chave foi expirada.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                });
            });
        });
    }

    $scope.enviarChave = function (item) {

        $rootScope.verificaPermissao('ChaveBO', 'EnviarChave', function () {
            Utils.bloquearCamposVisualizacao("VisualizarChave");

            item.currentDate = Utils.longDate();

            var dados = angular.copy(item);

            genericService.postRequisicao('ChaveBO', 'EnviarChave', { campos: dados }).then(
                function (result) {
                    var mensagem = $translate.instant(['13.MENSAGEM_ChaveEnviada', '13.MENSAGEM_Sucesso']);
                    $scope.alert(typeof mensagem === 'object' || (mensagem['13.MENSAGEM_ChaveEnviada'] == '13.MENSAGEM_ChaveEnviada' || mensagem['13.MENSAGEM_Sucesso'] == '13.MENSAGEM_Sucesso') ?
                    'Chave enviada para o email ' + dados.emailcontato + ' com sucesso.'  : mensagem['13.MENSAGEM_ChaveEnviada'] + ' ' + dados.emailcontato + ' ' + mensagem['13.MENSAGEM_Sucesso']);
                },
                $rootScope.TratarErro
            );
        });
    }

    $scope.exibirExpirar = function (item) {
        
        var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
            return obj === 'ExpirarChave';
        });

        if (retorno == undefined || retorno == null || retorno.length <= 0) {
            return false;
        }

        if (item.status.toLowerCase() == 'expirada') {
            return false;
        }

        return true;
    }

    $scope.exibirEnviar = function (item) {

        var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
            return obj === 'EnviarChave';
        });
        
        if (retorno == undefined || retorno == null || retorno.length <= 0) {
            return false;
        }

        if (item.status.toLowerCase() == 'expirada') {
            return false;
        }

        return true;
    }

    $scope.exibirBaixar = function (item) {
        
        var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
            return obj === 'BaixarChave';
        });
        
        if (retorno == undefined || retorno == null || retorno.length <= 0) {
            return false;
        }

        if (item.status.toLowerCase() == 'expirada') {
            return false;
        }

        return true;
    }
    
    $scope.exibirGerar = function (item){
        
        var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
            return obj === 'GerarChave';
        });
        
        if (retorno == undefined || retorno == null || retorno.length <= 0) {
            return false;
        }

        return true;    
    }

} ]);