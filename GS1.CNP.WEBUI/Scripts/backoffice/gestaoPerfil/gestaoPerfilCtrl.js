app.controller('GestaoPerfilCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI) {

    var jstree = null;
    $scope.isPesquisa = false;
        
    $scope.perfis = [
            {codigo: 1, nome: 'GS1 - Administrador', tipoUsuario: '1 - GS1 - Super Administrador', descricao: 'Perfil do Administrador Geral do Sistema', status: 'Ativo', quantidade: 2},
            {codigo: 2, nome: 'GS1 - Admin Default', tipoUsuario: '2 - GS1 - Administrador', descricao: 'Perfil da área de atendimento', status: 'Ativo', quantidade: 7},
            {codigo: 3, nome: 'GS1 - Admin Produtos', tipoUsuario: '2 - GS1 - Administrador', descricao: 'Perfil apenas para suporte a produtos', status: 'Ativo', quantidade: 5},
            {codigo: 4, nome: 'GS1 - Admin Relatórios', tipoUsuario: '2 - GS1 - Administrador', descricao: 'Perfil apenas para suporte a relatórios', status: 'Ativo', quantidade: 5},
            {codigo: 5, nome: 'Administrador', tipoUsuario: '3 - Associado Administrador', descricao: 'Perfil do Administrador Geral para associado', status: 'Ativo', quantidade: 5062},
            {codigo: 6, nome: 'Produtos', tipoUsuario: '4 - Associado usuário', descricao: 'Perfil do associado produtos', status: 'Ativo', quantidade: 125},
            {codigo: 6, nome: 'Relatórios', tipoUsuario: '4 - Associado usuário', descricao: 'Perfil do associado relatórios', status: 'Ativo', quantidade: 1894},
            {codigo: 6, nome: 'RFID', tipoUsuario: '4 - Associado usuário', descricao: 'Perfil do associado Geração RFID', status: 'Ativo', quantidade: 3599},
            {codigo: 6, nome: 'Consultas', tipoUsuario: '4 - Associado usuário', descricao: 'Perfil do associado Somente consultas', status: 'Ativo', quantidade: 1547},
            {codigo: 6, nome: 'Geral', tipoUsuario: '4 - Associado usuário', descricao: 'Perfil do associado em Geral', status: 'Ativo', quantidade: 8078}
    ];

    $scope.arvore = [
                {
                    descricao: 'Administração',
                    children: [
                        {descricao: 'Gestão de Perfis de Acesso'},
                        {descricao: 'Idioma'},
                        {descricao: 'Cadastro de Usuários'},
                        {descricao: 'Cadastro de Papel GLN'},
                        {descricao: 'Cadastro de Agências'},
                        {descricao: 'Cadastro de Tipo Produto'},
                        {descricao: 'Importação GPC'},
                        {descricao: 'Cadastro FAQ'},
                        {descricao: 'Consulta Associado'},
                        {descricao: 'Parametros do Sistema'},
                        {descricao: 'Administração de Usuários'},
                        {descricao: 'Meus Dados'},
                        {descricao: 'Pesquisa de Satisfação'},
                        {descricao: 'Upload de Cartas'},
                        {descricao: 'Cartas Certificadas'},
                        {descricao: 'Gestão de chaves'}
                    ]
                },
                {
                    descricao: 'Gestão de Código',
                    children: [
                        {descricao: 'Cadastro de Produtos'},
                        {descricao: 'Importação de Produto'},
                        {descricao: 'Cadastro GLN'},
                        {descricao: 'Impressão Código de Barras'}
                    ]
                },
                {
                    descricao: 'Geração EPC/RFID',
                    children: []
                },
                {
                    descricao: 'Certificação de Produtos',
                    children: [
                        {descricao: 'Peso e Medida'},
                        {descricao: 'Código de Barras'}
                    ]
                },
                {
                    descricao: 'Relatórios',
                    children: [
                        {descricao: 'Relatório de Impressão GS1'},
                        {descricao: 'Relatório Certificado GS1'},
                        {descricao: 'Relatório Certificado Associado'},
                        {descricao: 'Relatório de Localizações Físicas GS1'},
                        {descricao: 'Relatório de Localizações Físicas Associado'},
                        {descricao: 'Relatório de Histórico Uso GS1'},
                        {descricao: 'Relatório de Histórico Uso Associado'},
                        {descricao: 'Relatório de Login Acesso GS1'},
                        {descricao: 'Relatório de Login Acesso Associado'},
                        {descricao: 'Relatório de Produto Item GS1'},
                        {descricao: 'Relatório de Produto Item Associado'},
                        {descricao: 'Relatório de Associado Adm'},
                        {descricao: 'Relatório de Historico Status Produto Associado'},
                        {descricao: 'Relatório de Historico Status Produto GS1'}
                    ]
                },
                {
                    descricao: 'Ajuda',
                    children: [
                        {descricao: 'FAQ'},
                        {descricao: 'Ajuda'},
                        {descricao: 'Cadastro GLN'},
                        {descricao: 'Passo a Passo'}
                    ]
                },
                {
                    descricao: 'Home',
                    children: []
                }
    ];
        
    $scope.statusPerfis = ["Ativo", "Inativo"];
        
    $scope.tiposUsuario = [
        {codigo: 1, nome: '1 - GS1 - Super Administrador'},
        {codigo: 2, nome: '2 - GS1 - Administrador'},
        {codigo: 3, nome: '3 - Associado Administrador'},
        {codigo: 4, nome: '4 - Associado usuário'}
    ];

    $scope.titulo = 'Gestão de perfil e permissões';
        
    $scope.tablePerfil = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                $scope.items = data;
            });
        }
    });

    function carregaGrid(item, isPesquisa, callback) {
        if (isPesquisa == false) {
            var data = $scope.perfis;
            callback(data);

            //produto.CarregarTodosProdutos(function (data) {
            //    callback(data);
            //});
        }
        else if (isPesquisa == true) {
            var data = $scope.perfis;
            callback(data);
            //produto.CarregarProdutosPesquisa($scope.item, $scope.tags, function (data) {
            //    callback(data);
            //});
        }
    };

    $scope.onFormMode = function (item) {

            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isCollapsedMidias = true;
    };

    $scope.onEdit = function (item) {
        $scope.item = angular.copy(item);

        $scope.editMode = true;
        $scope.searchMode = false;
        $scope.isPesquisa = false;
    };

    $scope.onCancel = function (item) {
        $scope.item = {};
        $scope.editMode = false;
        $scope.searchMode = false;
        $scope.form.submitted = false;
        $scope.isPesquisa = false;

        $scope.tablePerfil.page(1);
        $scope.tablePerfil.reload();
    };

    $scope.onSearchMode = function (item) {
        Utils.desbloquearCamposVisualizacaoPesquisa();
        $scope.editMode = true;
        $scope.searchMode = true;
    };

    $scope.onClean = function (item) {
        $scope.item = {};
    };
        
    $scope.onDelete = function ($index) {
            
        $scope.confirm('Deseja remover o registro selecionado?', function () {
            $scope.perfis.splice($index, 1);
            $scope.tablePerfil.reload();
        });
    };

}]);
