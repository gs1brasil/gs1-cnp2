app.controller('IdiomaCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$log', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $log, $modal, genericService, Utils, blockUI, $translate) {

        //inicializando abas do form
        $('#tabs-idioma a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        var titulo = $translate.instant('49.LBL_TitleGestaoIdiomas');
        $scope.titulo = typeof titulo === 'object' || titulo == '49.LBL_TitleGestaoIdiomas' ? 'Gestão de idiomas' : titulo;

        $scope.form = {};
        $scope.itemForm = {};
        $scope.isPesquisa = false;
        $scope.shouldLoadCampos = false;

        $scope.tableIdiomas = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.itemForm, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        $scope.tableCampos = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if ($scope.shouldLoadCampos) {
                    var dados = carregaGridCampos($scope.itemForm, function (data) {
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    });
                }
            }
        });

        //Popula grid de idiomas
        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            if (isPesquisa) {
                dados = angular.copy(item);

                genericService.postRequisicao('IdiomaBO', 'PesquisarIdiomas', { campos: dados }).then(
                function (result) {
                    $scope.form.submitted = false;
                    $scope.editMode = false;

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
            }
            else {

                genericService.postRequisicao('IdiomaBO', 'BuscarIdiomas', {}).then(
                function (result) {
                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
            }
        };

        //Popula grid de campos
        function carregaGridCampos(item, callback) {

            var dados = null;
            dados = angular.copy(item);

            genericService.postRequisicao('IdiomaBO', 'BuscarCampos', { campos: dados }).then(
            function (result) {
                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
        };

        //Popula a grid Campos
        $scope.carregaCampos = function () {
            $scope.shouldLoadCampos = true;
            $scope.tableCampos.reload();
            $scope.tableCampos.page(1);
        };

        //Popula a grid de Menus
        $scope.carregaMenus = function () {

            if ($scope.tableMenus == undefined) {

                $scope.tableMenus = new ngTableParams({
                    page: 1,
                    count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
                    sorting: {
                        LOGIN: 'asc'
                    }
                }, {
                    counts: [],
                    total: 0,
                    getData: function ($defer, params) {
                        var dados = carregaGridMenus($scope.itemForm, function (data) {
                            params.total(data.length);
                            var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                        });
                    }
                });

                function carregaGridMenus(item, callback) {

                    var dados = null;
                    dados = angular.copy(item);

                    genericService.postRequisicao('IdiomaBO', 'BuscarItensMenus', { campos: dados }).then(
                    function (result) {
                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
                };

            } else {
                $scope.tableMenus.reload();
            }

        }

        //Popula dropdown de idiomas para tradução
        $scope.carregaIdiomasTraducao = function () {
            genericService.postRequisicao('IdiomaBO', 'IdiomasTraducao', {}).then(
            function (result) {

                if (result.data != undefined && result.data != null) {
                    $scope.idiomasTraducao = result.data;
                }
            },
            $rootScope.TratarErro
        );
        };

        //Popula dropdown de modulos
        function carregaModulos() {
            genericService.postRequisicao('IdiomaBO', 'BuscarModulosAtivos', { campos: {} }).then(
            function (result) {

                if (result.data != undefined && result.data != null) {
                    $scope.modulos = result.data;
                }
            },
            $rootScope.TratarErro
        );
        };

        //Popula dropdown de modulos
        function carregaMenus() {
            genericService.postRequisicao('IdiomaBO', 'BuscarItensMenus', { campos: { codigoidioma: $scope.itemForm.codigoidioma} }).then(
            function (result) {

                if (result.data != undefined && result.data != null) {
                    $scope.menus = result.data;
                }
            },
            $rootScope.TratarErro
        );
        };

        //Popula dropdown de formularios
        $scope.carregaFormularios = function (item) {

            $scope.formularios = {};

            genericService.postRequisicao('IdiomaBO', 'BuscarFormulariosAtivos', { campos: { modulo: item} }).then(
            function (result) {

                if (result.data != undefined && result.data != null) {
                    $scope.formularios = result.data;
                    $scope.itemForm.codigoformulario = '';
                    $scope.itemForm.codigoidioma = '';
                    $scope.tableCampos.reload();
                    $scope.tableCampos.page(1);
                }
            },
            $rootScope.TratarErro
        );
        };

        //Edição de informações de idioma
        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('IdiomaBO', 'AlterarStatus', function () {

                Utils.bloquearCamposVisualizacao("VisualizarIdiomas");

                $scope.itemForm = angular.copy(item);
                $scope.itemInsert = { codigo: item.codigo, status: item.status };

                var titulo = $translate.instant('49.LBL_AlterarIdioma');
                $scope.titulo = typeof titulo === 'object' || titulo == '49.LBL_AlterarIdioma' ? 'Alterar Idioma' : titulo;

                carregaModulos();
                $scope.carregaMenus();
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.exibeVoltar = true;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;
                $scope.shouldLoadCampos = false;
                $scope.initMode = false;
                $('#tabs-idioma a:first').tab('show');
            });
        };

        //Criando novo idioma
        $scope.onFormMode = function () {
            $rootScope.verificaPermissao('IdiomaBO', 'TraduzirTextos', function () {
                var titulo = $translate.instant('49.LBL_CadastrarIdioma');
                $scope.titulo = typeof titulo === 'object' || titulo == '49.LBL_CadastrarIdioma' ? 'Cadastrar Idioma' : titulo;
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;
                $scope.shouldLoadCampos = false;
                $scope.initMode = true;
                $scope.itemInsert = {};
                $scope.itemInsert.status = "0";
                $scope.itemInsert.idiomabase = "Português (PT-BR)";
                $scope.carregaIdiomasTraducao();
                $('#tabs-idioma a:first').tab('show');
                Utils.bloquearCamposVisualizacao("VisualizarIdiomas");
            });
        };

        //Voltar para a tela inicial de Idiomas
        $scope.voltar = function () {
            var titulo = $translate.instant('49.LBL_TitleGestaoIdiomas');
            $scope.titulo = typeof titulo === 'object' || titulo == '49.LBL_TitleGestaoIdiomas' ? 'Gestão de Idiomas' : titulo;
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $scope.isPesquisa = false;
            Utils.moveScrollTop();

            //Resetar GRID de Campos
            $scope.itemForm.codigoformulario = '';
            $scope.itemForm.codigoidioma = '';
            $scope.tableCampos.reload();
            $scope.tableCampos.page(1);
        }

        //Voltar para a tela inicial de Idiomas
        $scope.cancelar = function () {
            var titulo = $translate.instant('49.LBL_TitleGestaoIdiomas');
            $scope.titulo = typeof titulo === 'object' || titulo == '49.LBL_TitleGestaoIdiomas' ? 'Gestão de Idiomas' : titulo;
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $scope.isPesquisa = false;
            Utils.moveScrollTop();
            $scope.tableIdiomas.reload();
            $scope.tableIdiomas.page(1);
        }

        $scope.recarregarMenus = function (item) {
            $scope.itemForm.codigomodulomenu = item;
            $scope.tableMenus.reload();
            $scope.tableMenus.page(1);
        };

        $scope.onSave = function (item) {

            if (!$scope.form.$valid) {
                var mensagem = $translate.instant('49.LBL_CamposObrigatoriosInvalidos');
                $scope.alert(typeof mensagem === 'object' || mensagem == '49.LBL_CamposObrigatoriosInvalidos' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
            }
            else {
                if (item == null || item.codigo == null) {
                    var filtro = item.idiomatraducao;

                    function filterByID(obj) {
                        return obj.codigo == filtro;
                    }

                    var arrByID = $scope.idiomasTraducao.filter(filterByID);

                    item.sigla = arrByID[0].sigla;
                    item.nome = arrByID[0].nome;

                    genericService.postRequisicao('IdiomaBO', 'TraduzirTextos', { campos: item }).then(
                    function (result) {

                        if (result.data != undefined && result.data != null) {
                            var titulo = $translate.instant('49.LBL_TitleGestaoIdiomas');
                            $scope.titulo = typeof titulo === 'object' || titulo == '49.LBL_TitleGestaoIdiomas' ? 'Gestão de Idiomas' : titulo;
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            $scope.isPesquisa = false;
                            $scope.form.submitted = false
                            Utils.moveScrollTop();

                            //Resetar GRID de Campos
                            $scope.itemForm.codigoformulario = '';
                            $scope.itemForm.codigoidioma = '';
                            $scope.tableIdiomas.reload();
                            $scope.tableIdiomas.page(1);

                            $rootScope.buscaIdiomas();

                            var mensagem = $translate.instant('49.MENSAGEM_RegistroSalvoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '49.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                        }
                    },
                    $rootScope.TratarErro
                );
                } else {
                    genericService.postRequisicao('IdiomaBO', 'AlterarStatus', { campos: item }).then(
                    function (result) {

                        if (result.data != undefined && result.data != null) {
                            $rootScope.buscaIdiomas();

                            var mensagem = $translate.instant('49.MENSAGEM_RegistroSalvoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '49.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                        }
                    },
                    $rootScope.TratarErro
                );
                }
            }
        }

        $scope.onSearchMode = function () {
            var mensagem = $translate.instant('49.LBL_PesquisarIdioma');
            $scope.mensagem = typeof mensagem === 'object' || mensagem == '49.LBL_PesquisarIdioma' ? 'Pesquisar Idioma' : mensagem;
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.itemSearch = {};
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
            $scope.shouldLoadCampos = false;
            $scope.initMode = false;
            $('#tabs-idioma a:first').tab('show');
        }

        $scope.onSearch = function (item) {
            $scope.exibeVoltar = false;
            $scope.itemForm = angular.copy(item);
            Utils.moveScrollTop();
            $scope.tableIdiomas.reload();
            $scope.tableIdiomas.page(1);
        }

        //Método para abertura do modal ModalEditaCampos
        $scope.onEditCampo = function (item) {

            item.idioma = $scope.itemForm.codigo;

            var modalInstance = $modal.open({
                templateUrl: 'ModalEditaCampos',
                backdrop: 'static',
                controller: function ($scope, $modalInstance) {
                    $scope.desabilitarCamposModal = false;
                    $scope.form = {};
                    $scope.campoForm = angular.copy(item);

                    $scope.campoMenu = false;

                    //Cancelar 
                    $scope.cancel = function () {
                        $modalInstance.close(true);
                    };

                    $scope.ok = function () {
                        if (!$scope.form.campo.$valid) {
                            var mensagem = $translate.instant('49.LBL_CamposObrigatoriosInvalidos');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '49.LBL_CamposObrigatoriosInvalidos' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                            $scope.form.campo.submitted = true;
                        }
                        else {
                            genericService.postRequisicao('IdiomaBO', 'AlteraCampo', { campos: $scope.campoForm }).then(
                            function (result) {

                                if (result.data != undefined && result.data != null) {
                                    $modalInstance.close(true);
                                    $scope.form.campo.submitted = false;
                                    var mensagem = $translate.instant('49.MENSAGEM_RegistroAlteradoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '49.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                                }
                            },
                            $rootScope.TratarErro
                        );
                        }
                    }

                    var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
                        return obj === 'VisualizarIdiomas';
                    });

                    if (retorno != undefined && retorno != null && retorno.length > 0) {
                        $scope.desabilitarCamposModal = true;
                    } else {
                        $scope.desabilitarCamposModal = false;
                    }

                }

            });

            modalInstance.result.then(function (data) {
                $scope.tableCampos.reload();
                //$scope.tableCampos.page(1);
            });

        };

        //Método para abertura do modal ModalEditaCampos
        $scope.onEditMenu = function (item) {

            var modalInstance = $modal.open({
                templateUrl: 'ModalEditaCampos',
                backdrop: 'static',
                controller: function ($scope, $modalInstance) {
                    $scope.desabilitarCamposModal = false;
                    $scope.form = {};
                    $scope.campoForm = angular.copy(item);

                    $scope.campoMenu = true;

                    //Cancelar 
                    $scope.cancel = function () {
                        $modalInstance.close(true);
                    };

                    $scope.ok = function () {
                        if (!$scope.form.campo.$valid) {
                            var mensagem = $translate.instant('49.LBL_CamposObrigatoriosInvalidos');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '49.LBL_CamposObrigatoriosInvalidos' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                            $scope.form.campo.submitted = true;
                        }
                        else {
                            genericService.postRequisicao('IdiomaBO', 'AlteraCampoMenu', { campos: $scope.campoForm }).then(
                            function (result) {

                                if (result.data != undefined && result.data != null) {
                                    $scope.form.campo.submitted = false
                                    $modalInstance.close(true);
                                    var mensagem = $translate.instant('49.MENSAGEM_RegistroAlteradoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '49.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                                }
                            },
                            $rootScope.TratarErro
                        );
                        }
                    }

                    var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
                        return obj === 'VisualizarIdiomas';
                    });

                    if (retorno != undefined && retorno != null && retorno.length > 0) {
                        $scope.desabilitarCamposModal = true;
                    } else {
                        $scope.desabilitarCamposModal = false;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $scope.tableMenus.reload();
            });
        };

    } ]);