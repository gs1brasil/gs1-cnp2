app.controller('ImportacaoProdutoCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI) {

        $scope.isPesquisa = false;
        $scope.titulo = 'Importação de produtos';
        
        $scope.tablePapel = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        //Busca papeis GLN
        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            if (isPesquisa) {
                dados = angular.copy(item);

                genericService.postRequisicao('PapelGlnBO', 'BuscarPapelGLN', { campos: dados }).then(
                    function (result) {

                        $scope.form.submitted = false;
                        $scope.editMode = false;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                 );
            }
            else {
                genericService.postRequisicao('PapelGlnBO', 'BuscarPapeisGLN', { where: dados }).then(
                    function (result) {

                        $scope.papeis = result.data;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                 );
            }
        }

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('PapelGlnBO', 'CadastrarPapelGln', function () {
                $scope.titulo = 'Importação de produtos';
                $scope.item = {};
                $scope.isPesquisa = false;
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.exibeVoltar = true;
            });
        };

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('PapelGlnBO', 'AtualizarPapelGln', function () {
                $scope.titulo = 'Importação de produtos';
                $scope.item = angular.copy(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;
            });
        };

        $scope.onCancel = function (item) {
            $scope.item = {};
            $scope.titulo = 'Importação de produtos';
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $scope.tablePapel.page(1);
            $scope.tablePapel.reload();
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        $scope.onSearchMode = function (item) {
            $scope.titulo = 'Importação de produtos';
            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
        };

        //Remove Papel GLN cadastrado
        $scope.onDelete = function (item) {
            $rootScope.verificaPermissao('PapelGlnBO', 'RemoverPapelGLN', function () {
                $rootScope.confirm('Deseja remover o registro selecionado?', function () {
                    genericService.postRequisicao('PapelGlnBO', 'RemoverPapelGLN', { campos: item }).then(
                        function (result) {
                            $scope.form.submitted = false;
                            $scope.tablePapel.reload();
                            $scope.tablePapel.page(1);
                            $scope.item = {};
                            $scope.alert('Dados removidos com sucesso.');
                        },
                        $rootScope.TratarErro
                    );

                }, function () { }, 'Confirmação');
            });
        }

        //Pesquisa produto cadastrado
        $scope.onSearch = function (item) {
            $scope.exibeVoltar = false;
            $scope.tablePapel.page(1);
            $scope.tablePapel.reload();
        }

        $scope.onSave = function (item) {

            if (!$scope.form.$valid) {
                $scope.alert('Campos obrigatórios não preenchidos ou inválidos.');
                $scope.form.submitted = true;
            }
            else {
                //Inserção de Papel GLN
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {
                    genericService.postRequisicao('PapelGlnBO', 'CadastrarPapelGln', { campos: item }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            $scope.tablePapel.page(1);
                            $scope.tablePapel.reload();
                            $scope.alert('Registro salvo com sucesso.');
                        },
                        $rootScope.TratarErro
                    );
                }
                //Atualização de Papel GLN
                else {
                    genericService.postRequisicao('PapelGlnBO', 'AtualizarPapelGln', { campos: item }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            $scope.tablePapel.page(1);
                            $scope.tablePapel.reload();
                            $scope.alert('Registro alterado com sucesso.');
                        },
                        $rootScope.TratarErro
                    );
                }

            }
        }

    } ]);
