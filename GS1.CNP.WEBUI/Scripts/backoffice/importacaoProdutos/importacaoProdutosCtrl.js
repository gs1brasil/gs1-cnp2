app.controller('ImportacaoProdutosCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI) {
    
    //inicializando abas do form
    $('#tabs-importacao a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
        
    $scope.isPesquisa = false;
        
    $scope.importacoes = [
        { codigo: 1, data: '21/07/2015 09:07', arquivo: 'GTIN-13 Lista', tipoImportacao: 'Template', itens: 1054, erros: 0, resultado: 'Importado com Sucesso', usuario: 'dudus@dudus.com' },
        { codigo: 2, data: '21/07/2015 09:01', arquivo: '-', tipoImportacao: 'Serviço', itens: 2, erros: 2, resultado: 'Não Importado', usuario: 'dudus@dudus.com' },
        { codigo: 3, data: '21/07/2015 09:00', arquivo: 'GTIN-14 Lista plan teste123', tipoImportacao: 'Template', itens: 41, erros: 0, resultado: 'Importado com Sucesso', usuario: 'dudus@dudus.com' },
        { codigo: 4, data: '21/07/2015 08:57', arquivo: 'GTIN-8 Lista', tipoImportacao: 'Template', itens: 52, erros: 0, resultado: 'Importado com Sucesso', usuario: 'dudus@dudus.com' },
        { codigo: 5, data: '21/07/2015 08:17', arquivo: '-', tipoImportacao: 'Tela', itens: 15, erros: 0, resultado: 'Importado com Sucesso', usuario: 'dudus@dudus.com' },
        { codigo: 6, data: '21/07/2015 06:07', arquivo: '-', tipoImportacao: 'Serviço', itens: 18, erros: 1, resultado: 'Não Importado', usuario: 'dudus@dudus.com' },
        { codigo: 7, data: '20/07/2015 23:07', arquivo: '-', tipoImportacao: 'Serviço', itens: 2, erros: 1, resultado: 'Não Importado', usuario: 'dudus@dudus.com' },
        { codigo: 8, data: '21/07/2015 22:17', arquivo: 'GTIN-13 Lista', tipoImportacao: 'Template', itens: 18, erros: 0, resultado: 'Importado com Sucesso', usuario: 'dudus@dudus.com' }
    ];
    
    $scope.errosImportacao = 2;
    $scope.importacoesArquivo = [
        {status: 'Ativo', data: '27/07/2015', gtin: 7898357410015, codigoInterno: 'ABCD', descricaoProduto: 'Leite Protótipo A', gtinInferior: '', quantidadeItens: ''},
        {status: 'Reutilizado', data: '27/07/2015', gtin: 7898357410014, codigoInterno: 'DEFG', descricaoProduto: 'Leite Protótipo B', gtinInferior: '', quantidadeItens: ''},
        {status: 'Suspenso', data: '27/07/2015', gtin: 7898357410013, codigoInterno: 'HIJK', descricaoProduto: 'Leite Protótipo X', gtinInferior: '', quantidadeItens: ''},
        {status: 'Cancelado', data: '27/07/2015', gtin: 7898357410012, codigoInterno: 'ABCE', descricaoProduto: 'Leite Protótipo Y', gtinInferior: '', quantidadeItens: ''},
        {status: 'Teste', data: '27/07/2015', gtin: 7898357410011, codigoInterno: 'DEFR', descricaoProduto: 'Leite Protótipo Z', gtinInferior: '', quantidadeItens: '',
            erros: [
                {campo: 'status', descricao: 'Status para o item não existe'}
            ]
        } ,
        {status: 'Ativo', data: '27/07/2015', gtin: 7898357410014, codigoInterno: 'HIJI', descricaoProduto: 'Leite Protótipo K', gtinInferior: '', quantidadeItens: '',
            erros: [
                {campo: 'gtin', descricao: 'Não foi possível gravar o GTIN/GLN porque o GTIN já existe'}
            ]
        }
    ];
        
    $scope.importacoesManual = [
        {status: 'Selecione', data: '20/08/2015', gtin: 7898357410015, codigoInterno: '', descricaoProduto: '', gtinInferior: '', quantidadeItens: ''}
    ];
    
        
    $scope.tableImportacoes = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridImportacoes($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    $scope.items = data;
                });
            }
    });
        
    $scope.tableImportacoesArquivo = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridImportacoesArquivo($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    $scope.items = data;
                });
            }
    });
        
    $scope.tableImportacoesManual = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridImportacoesManual($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    $scope.items = data;
                });
            }
    });

    function carregaGridImportacoes(item, isPesquisa, callback) {
        $rootScope.verificaAssociadoSelecionado(function (data) {
            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                if (isPesquisa == false) {
                    var data = $scope.importacoes;
                    callback(data);
                }
                else if (isPesquisa == true) {
                    var data = $scope.importacoes;
                    callback(data);
                }

                $rootScope.flagAssociadoSelecionado = true;
            }
            else {
                $rootScope.flagAssociadoSelecionado = false;
                angular.element("#typea").focus();
            }
        });
    };
        
    function carregaGridImportacoesArquivo(item, isPesquisa, callback) {
        if (isPesquisa == false) {
            var data = $scope.importacoesArquivo;
            callback(data);
        }
        else if (isPesquisa == true) {
            var data = $scope.importacoesArquivo;
            callback(data);
        }
    };
        
    function carregaGridImportacoesManual(item, isPesquisa, callback) {
        if (isPesquisa == false) {
            var data = $scope.importacoesManual;
            callback(data);
        }
        else if (isPesquisa == true) {
            var data = $scope.importacoesManual;
            callback(data);
        }
    };
        
    $scope.onCancel = function (item) {
        $scope.item = {};
        $scope.editMode = false;
        $scope.searchMode = false;
        $scope.form.submitted = false;
        $scope.isPesquisa = false;
        $scope.importacaoManual = false;

        $scope.tableImportacoes.page(1);
        $scope.tableImportacoes.reload();
    };
        
    $scope.onValidate = function(){
        $('#liTabResultado a').click();    
    }

} ]);