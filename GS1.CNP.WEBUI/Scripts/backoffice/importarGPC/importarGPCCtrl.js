app.controller('ImportarGPCCtrl', ['$rootScope', '$scope', '$filter', 'ngTableParams', 'Utils', 'genericService', '$translate', 
    function ($rootScope, $scope, $filter, ngTableParams, Utils, http, $translate) {
    var navigatorVersion = $rootScope.getInternetExplorerVersion();
    var jstree = null;
    
    $('#tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#btnPesquisarLista').click(function (e) {
        Listar($('#pesquisa').val());

        $('#lista').jstree('search', $('#pesquisa').val());
    });

    var mensagem = $translate.instant('8.LBL_ImportacaoGPC');
    $scope.titulo = typeof mensagem === 'object' || mensagem == '8.LBL_ImportacaoGPC' ? 'Importação de GPC' : mensagem;
        
    function initializeAce() {
        if (navigatorVersion == -1 || navigatorVersion > 9) {

            var mensagem = $translate.instant(['8.PLACEHOLDER_SelecioneArquivoXML', '8.BTN_Selecione', '8.BTN_Alterar']);
                var options = {
                    no_file: mensagem['8.PLACEHOLDER_SelecioneArquivoXML'],
                    btn_choose: mensagem['8.BTN_Selecione'],
                    btn_change: mensagem['8.BTN_Alterar'],
                    droppable: false,
                    onchange: null,
                    thumbnail: 'small'
                };

                $('#arquivo').ace_file_input(options).ace_file_input('reset_input');
        }
    }

    function initialize() {
        initializeAce();

        Listar();

        $scope.tableParams = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                data: 'desc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {

                var dados = ListarHistorico(function (data) {
                    var filteredData = $filter('filter')(data, $scope.filter);
                    params.total(filteredData.length);

                    var orderedData = params.sorting() ?
                                $filter('orderBy')(filteredData, params.orderBy()) :
                                filteredData;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.items = data;
                });
            }
        });

        http.postRequisicao('ImportarGPCBO', 'VerificarProcessamentoPendente', null).then(function(result){
            if (result != null && result.data != null && result.data.existeimportacaopendente != null){
                $scope.existeimportacaopendente = result.data.existeimportacaopendente;
                Utils.bloquearCamposVisualizacao("VisualizarGPC");
            }
        }, $rootScope.TratarErro);

    }

    function Listar() {
        var options = {
            'core': {
                'strings' : {
				    'loading' : 'Carregando...', //this text will change the label when you create a new node
			    },
                'themes': {
                    'theme': 'default',
                    'dots': false,
                    'icons': false,
                    'url': '/styles/style.min.css'
                },
                'data': {
                    'type': 'POST',
                    'dataType': 'json',
                    'url': function (node) {
                        if ($('#pesquisa').val() == null || $('#pesquisa').val() == undefined || $('#pesquisa').val() == '')
                            return '/requisicao.ashx/ImportarGPCBO/Listar';
                        else
                            return '/requisicao.ashx/ImportarGPCBO/Pesquisar';
                    },
                    'data': function (node) {
                        var params = '';

                        if ($('#pesquisa').val() != null && $('#pesquisa').val() != undefined && $('#pesquisa').val() != '')
                            params = 'text: "' + $('#pesquisa').val() + '"';

                        params += (params != null && params != '' ? ',' + MontarParametros(node) : MontarParametros(node));

                        return '{ where: { id: "' + node.id + '" ' + (params != null && params != '' ? ',' + params : '') + '} }';
                    }
                }
            },
            'search': { 'case_insensitive': true },
            'plugins': ['search']
        };

        if (jstree != null)
            jstree.jstree("destroy");
            jstree = $('#lista').on("ready.jstree", function () {
                if ($('#pesquisa').val() != null && $('#pesquisa').val() != undefined && $('#pesquisa').val() != '')
                    $('#lista').jstree('search', $('#pesquisa').val());
            }).jstree(options);
        //        else
        //            jstree.jstree("refresh");

        Utils.bloquearCamposVisualizacao("VisualizarGPC");
    }

    function MontarParametros(node) {
        if (node != null && node.parents != null && node.parents.length > 0) {
            var params = '';

            for (var i = 0; i < node.parents.length; i++) {
                var value = node.parents[i];

                if (value[0] == 's')
                    params += (params == '' ? '' : ',') + 'segment: "' + value.substring(1) + '"';
                else if (value[0] == 'f')
                    params += (params == '' ? '' : ',') + 'family: "' + value.substring(1) + '"';
                else if (value[0] == 'c')
                    params += (params == '' ? '' : ',') + 'classes: "' + value.substring(1) + '"';
                else if (value[0] == 'b')
                    params += (params == '' ? '' : ',') + 'brick: "' + value.substring(1) + '"';
                else if (value[0] == 'a')
                    params += (params == '' ? '' : ',') + 'attribute: "' + value.substring(1) + '"';
                else if (value[0] == 'v')
                    params += (params == '' ? '' : ',') + 'value: "' + value.substring(1) + '"';
            }

            return params;
        }

        return '';
    }

    function ListarHistorico(callback) {
        http.postRequisicao('ImportarGPCBO', 'ListarHistorico', null).then(function (result) {
            Utils.bloquearCamposVisualizacao("VisualizarGPC");
            if (callback != null) {
                callback(result.data);
            }
        }, $rootScope.TratarErro);
    }

    function onImport() {
        
        var arquivo = angular.element("#arquivo").val();
        
        //Valida se foi selecionado um arquivo
        if (arquivo == ""){

            var mensagem = $translate.instant('8.MENSAGEM_SelecioneArquivoExtensao');
            $scope.alert(typeof mensagem === 'object' || mensagem == '8.MENSAGEM_SelecioneArquivoExtensao' ? 'Selecione um arquivo com uma das seguintes extensões: *.xml, *.xlsx ou *.csv' : mensagem);

            return false;    
        }
        
        //Valida a extensão
        var extensao = (arquivo.substring(arquivo.lastIndexOf("."))).toLowerCase();
        if (extensao != ".xml" && extensao != ".xlsx" && extensao != ".csv"){

            var mensagem = $translate.instant('8.MENSAGEM_SelecioneArquivoExtensao');
            $scope.alert(typeof mensagem === 'object' || mensagem == '8.MENSAGEM_SelecioneArquivoExtensao' ? 'Selecione um arquivo com uma das seguintes extensões: *.xml, *.xlsx ou *.csv' : mensagem);

            return false;    
        }
        
        Utils.uploadXML('arquivo', function (result) {
            
            if (result == undefined) {

                var mensagem = $translate.instant('8.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '8.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);

                $scope.uploadObrigatorio = true;
                return false;
                
            } else if (result == "Extensão não suportada") {

                var mensagem = $translate.instant('8.MENSAGEM_ExtensaoDiferenteEsperado');
                $scope.alert(typeof mensagem === 'object' || mensagem == '8.MENSAGEM_ExtensaoDiferenteEsperado' ? 'Extensão de arquivo diferente do esperado. Selecione um arquivo com uma das seguintes extensões: *.xml, *.xlsx ou *.csv' : mensagem);

                return false;
                
            } else if (result.indexOf("Não foi possível fazer o upload do arquivo.") != -1) {
                $scope.alert(result);
                return false;
                
            } else {
                var ret = result;

                if (ret instanceof Array)
                    ret = ret[0];

                var dados = { campos: { arquivo: ret} };

                http.postRequisicao('ImportarGPCBO', 'Importar', dados).then(function (result) {
                    if (result != null && result.data != null && (result.data == "True" || result.data == true)) {
                        initializeAce();

                        var mensagem = $translate.instant('8.MENSAGEM_ImportacaoGPCSucesso');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '8.MENSAGEM_ImportacaoGPCSucesso' ? 'Importação de GPC iniciada com sucesso.' : mensagem);

                    }
                }, $rootScope.TratarErro);
            }
        });
    }

    function onCancel() {
        $scope.resultMode = false;
    };

    //Chamado ao pressionar ESC na tela
    window.onkeydown = function (event) {
        if (event.keyCode === 27) {
            $scope.onCancel();
        }
    };

    function onResult(item) {
        $scope.item = angular.copy(item);

        $scope.resultMode = true;
    }

    function onSearch() {
        $scope.tableParams.reload();
        $scope.tableParams.page(1);
    }

    $scope.onImport = onImport;
    $scope.onCancel = onCancel;
    $scope.onResult = onResult;
    $scope.onSearch = onSearch;

    initialize();
} ]);