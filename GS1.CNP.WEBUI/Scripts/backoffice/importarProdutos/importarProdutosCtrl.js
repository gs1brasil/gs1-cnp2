﻿﻿app.controller('ImportarProdutosCtrl', ['$rootScope', '$scope', '$filter', '$timeout', 'ngTableParams', 'Utils', 'genericService', 'blockUI', '$translate',
    function ($rootScope, $scope, $filter, $timeout, ngTableParams, Utils, http, blockUI, $translate) {
    var navigatorVersion = $rootScope.getInternetExplorerVersion();
    var self = this;
    var originalData = []
        //registros = [],
        arquivo = '',
        padraoGDSN = 0,
        hot = null,
        listErrors = [];

    $scope.listaErros = [];
    $scope.listas = {};
    $scope.tipoArquivo = -1;
    $scope.exibeMensagem = false;
    
    Utils.bloquearCamposVisualizacao("VisualizarImportarProduto");

     $(document).ready(function () {
        $('[data-toggle="popover"]').popover({ html: true });
    });

    $('#tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#btnPesquisarLista').click(function (e) {
        $('#lista').jstree('search', $('#pesquisa').val());
    });

    var titulo = $translate.instant('18.LBL_ImportacaoProduto');
    $scope.titulo = typeof titulo === 'object' || titulo == '18.LBL_ImportacaoProduto' ? 'Importação de Produtos' : titulo;

    var screen = {
        GRID: 0,
        RESULT: 1,
        ASSISTANT: 2,
        IMPORT: 3
    };

    function preInit() {
        $rootScope.verificaAssociadoSelecionado(function (data) {
            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                $rootScope.flagAssociadoSelecionado = true;

                initialize();
            } else {
                $rootScope.flagAssociadoSelecionado = false;
                angular.element("#typea").focus();
            }
        });
    }

    function initialize() {
        if (navigatorVersion == -1 || navigatorVersion > 9) {
            var options = {
                no_file: 'Arquivo Excel ou CSV (no formato template)',
                btn_choose: 'Selecione',
                btn_change: 'Alterar',
                droppable: false,
                onchange: null,
                thumbnail: 'small'
            };

            $('#arquivo').ace_file_input(options).ace_file_input('reset_input');
        }

        BuscarModelos();

        $scope.tableParams = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: { data: 'desc' }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {

                var dados = ListarHistorico(function (data) {
                    var filteredData = $filter('filter')(data, $scope.filter);
                    params.total(filteredData.length);

                    var orderedData = params.sorting() ?
                                $filter('orderBy')(filteredData, params.orderBy()) :
                                filteredData;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.items = data;
                });
            }
        });
    }

    function ExibirTela(tela) {
        $scope.formMode = tela > 0;
        $scope.resultMode = tela == 1;
        $scope.verificacaoItensMode = tela == 2;
        $scope.importacaoManualMode = tela == 3;

        if (tela > 0) {
            $(".grid-container input, .grid-container button, .grid-container select").prop("disabled", true);
            $(".grid-container a").addClass("disabled");
        } else {
            $(".grid-container input, .grid-container button, .grid-container select").prop("disabled", false);
            $(".grid-container a").removeClass("disabled");
        }
    }

    function BuscarModelos() {
        http.postRequisicao('ImportarProdutosBO', 'BuscarModelos', null).then(function (result) {
            $scope.listaModelos = result.data;
        }, $rootScope.TratarErro);
    }

    function ListarHistorico(callback) {
        http.postRequisicao('ImportarProdutosBO', 'ListarHistorico', null).then(function (result) {
            if (callback != null)
                callback(result.data);
        }, $rootScope.TratarErro);
    }

    function onDownload() {
        $scope.itemModelo.padraoGDSN = 2;
        if ($scope.itemModelo != null && $scope.itemModelo.codigo != null && $scope.itemModelo.tipoModelo != null && ($scope.itemModelo.codigo == 5 || ($scope.itemModelo.padraoGDSN != null))) {
            var dados = {
                codigo: $scope.itemModelo.codigo,
                tipoModelo: $scope.itemModelo.tipoModelo,
                padraoGDSN: ($scope.itemModelo.codigo == 5 ? 2 : $scope.itemModelo.padraoGDSN || 2)
            };

            $.ajaxFileDownload({
                url: 'DownloadTemplateHandler.ashx',
                secureuri: false,
                dataType: "json",
                data: dados,
                error: function (xhr, status, e) {
                    $scope.alert(e);
                }
            });
        } else { 
            if ($scope.itemModelo == null || $scope.itemModelo.codigo == null) {
                var mensagem = $translate.instant('18.MENSAGEM_SelecioneTemplate');
                $scope.alert(typeof mensagem === 'object' || mensagem == '18.MENSAGEM_SelecioneTemplate' ? 'Selecione um template.' : mensagem);
            }
            else if ($scope.itemModelo.tipoModelo == null) {
                var mensagem = $translate.instant('18.MENSAGEM_SelecioneTipoTemplate');
                $scope.alert(typeof mensagem === 'object' || mensagem == '18.MENSAGEM_SelecioneTipoTemplate' ? 'Selecione o tipo do template.' : mensagem);
            }
            else if ($scope.itemModelo.padraoGDSN == null) {
                var mensagem = $translate.instant('18.MENSAGEM_SelecionePadraoGDSN');
                $scope.alert(typeof mensagem === 'object' || mensagem == '18.MENSAGEM_SelecionePadraoGDSN' ? 'Selecione o padrão GDSN.' : mensagem);
            }
        }
    }

    function getDomainParameters(row, col, manual) {
        if ($scope.tipoArquivo > 0) {
            if ((manual == true && col == 1) || (manual == false && col == 0))
                return { where: { listname: 'statusgtin', tablename: 'statusgtin', code: 'codigo', description: 'nome', condition: '', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 7) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 6) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 9) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 8))
                return { where: { listname: 'ncm', tablename: 'ncm', code: 'ncm', description: 'NCM + \' - \' + Descricao', condition: '', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 8) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 7) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 10) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 9))
                return { where: { listname: 'cest', tablename: 'cest', code: 'codigoCest', description: 'CEST + \' - \' + Descricao_CEST', condition: '', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 9) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 8) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 11) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 10))
                return { where: { listname: 'segment', tablename: 'tbSegment', code: 'CodeSegment', description: 'CodeSegment + \' - \' + Text', condition: '1=1 ORDER BY text ASC', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 10) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 9) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 12) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 11))
                return { where: { listname: 'family', tablename: 'tbFamily', code: 'CodeFamily', description: 'CodeFamily + \' - \' + Text', condition: '', value: [hot.getDataAtCell(row, col-1)] } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 11) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 10) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 13) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 12))
                return { where: { listname: 'class', tablename: 'tbClass', code: 'CodeClass', description: 'CodeClass + \' - \' + Text', condition: '', value: [hot.getDataAtCell(row, col-2), hot.getDataAtCell(row, col-1)] } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 12) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 11) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 14) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 13))
                return { where: { listname: 'brick', tablename: 'tbBrick', code: 'CodeBrick', description: 'CodeBrick + \' - \' + Text', condition: '', value: [hot.getDataAtCell(row, col-3), hot.getDataAtCell(row, col-2), hot.getDataAtCell(row, col-1)] } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && (col == 14 || col == 16 || col == 18 || col == 20 || col == 22)) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && (col == 13 || col == 15 || col == 17 || col == 19 || col == 21)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 16 || col == 18 || col == 20 || col == 22 || col == 24)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 15 || col == 17 || col == 19 || col == 21 || col == 23)))
                return { where: { listname: 'agenciareguladora', tablename: 'agenciareguladora', code: 'Codigo', description: 'Nome', condition: 'status=1', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && (col == 23 || col == 24)) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && (col == 22 || col == 23)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 25 || col == 26)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 24 || col == 25)))
                return { where: { listname: 'pais', tablename: 'CountryOfOriginISO3166', code: 'CodigoPais', description: 'Nome', condition: '', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 25) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 24) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 25) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 27))
                return { where: { listname: 'estado', tablename: 'tbStateCountry', code: 'StateName', description: 'StateName', condition: '', value: [hot.getDataAtCell(row, col-1)] } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 26) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 25) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 28) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 27))
                return { where: { listname: 'tipoproduto', tablename: 'TipoProduto', code: 'Codigo', description: 'Nome', condition: 'Status > 0', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && (col == 28 || col == 30 || col == 32)) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && (col == 27 || col == 29 || col == 31)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 30 || col == 32 || col == 34)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 29 || col == 31 || col == 33)))
                return { where: { listname: 'unidade3', tablename: 'UNECEREC20', code: 'COMMONCODE', description: 'SIGLA', condition: 'TIPO = 3 ORDER BY INDICADORPRINCIPAL DESC', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 34) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 33) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 36) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 35))
                return { where: { listname: 'unidade1', tablename: 'UNECEREC20', code: 'COMMONCODE', description: 'SIGLA', condition: 'TIPO = 1 ORDER BY INDICADORPRINCIPAL DESC', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && (col == 36 || col == 38)) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && (col == 35 || col == 37)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 38 || col == 40)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 37 || col == 39)))
                return { where: { listname: 'unidade2', tablename: 'UNECEREC20', code: 'COMMONCODE', description: 'SIGLA', condition: 'TIPO = 2 ORDER BY INDICADORPRINCIPAL DESC', value: '' } };
            else if (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 1 && col == 42) ||
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && col == 41) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 44) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 43))
                return { where: { listname: 'pallettypecode', tablename: 'palletTypeCode', code: 'code', description: 'codedescription', condition: 'status = 1', value: '' } };
            else if (
                ($scope.tipoArquivo == 12 && padraoGDSN == 1 && (col == 51 || col == 54 || col == 57)) ||
                ($scope.tipoArquivo == 12 && padraoGDSN == 2 && (col == 50 || col == 53 || col == 56)) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 1 && (col == 53 || col == 56 || col == 59)) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 2 && (col == 52 || col == 55 || col == 58)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 53 || col == 56 || col == 59)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 52 || col == 55 || col == 58)))
                return { where: { listname: 'tipourl', tablename: 'TipoUrl', code: 'Codigo', description: 'Nome', condition: '', value: '' } };
            else if (
                ($scope.tipoArquivo == 12 && padraoGDSN == 1 && col == 64) ||
                ($scope.tipoArquivo == 12 && padraoGDSN == 2 && col == 58) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 1 && col == 66) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 2 && col == 60) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 66) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 60))
                return { where: { listname: 'unidademedida', tablename: 'GDSNUnitOfMeasureCodeList', code: 'CodeValue', description: 'Name', condition: 'Status = 1', value: '' } };
            else if (
                ($scope.tipoArquivo == 12 && padraoGDSN == 1 && col == 67) ||
                ($scope.tipoArquivo == 12 && padraoGDSN == 2 && col == 61) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 1 && col == 69) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 2 && col == 63) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && col == 69) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && col == 63))
                return { where: { listname: 'tipoembalagem', tablename: 'PackagingTypeCode', code: 'CodeValue', description: 'Name', condition: 'Status = 1', value: '' } };
            else if (
                ($scope.tipoArquivo == 12 && padraoGDSN == 1 && (col == 69 || col == 71)) ||
                ($scope.tipoArquivo == 12 && padraoGDSN == 2 && (col == 63 || col == 65)) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 1 && (col == 71 || col == 73)) ||
                ($scope.tipoArquivo == 13 && padraoGDSN == 2 && (col == 65 || col == 67)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 1 && (col == 71 || col == 73)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && (col == 65 || col == 67)))
                return { where: { listname: 'unidade4', tablename: 'UNECEREC20', code: 'COMMONCODE', description: 'SIGLA', condition: 'TIPO = 4 ORDER BY INDICADORPRINCIPAL DESC', value: '' } };
        } else {
            if (col == 1)
                return { where: { listname: 'papelgln', tablename: 'PapelGLN', code: 'codigo', description: 'nome', condition: '', value: '' } };
            else if (col == 6)
                return { where: { listname: 'tbcountry', tablename: 'tbcountry', code: 'CountryCode', description: 'CountryName', condition: '', value: '' } };
            else if (col == 13)
                return { where: { listname: 'estadogln', tablename: 'tbStateCountry', code: 'StateName', description: 'StateName', condition: '', value: [hot.getDataAtCell(row, 6)] } };
        }
    }

    function errorValueRenderer(instance, td, row, col, prop, value, cellProperties) {
        Handsontable.renderers.TextRenderer.apply(this, arguments);

        if (row < listErrors.length && col < listErrors[row].length) {
            var val = listErrors[row][col];
            if (val != null && val != '' && (val == 'true' || val == true)) {
                if (value == null || value == '')
                    td.style.backgroundColor = 'red';
                else
                    td.style.color = 'red';
            }
        }
    }

    function arrayIsNullOrEmpty(line) {
        if (line != null && line instanceof Array && line.length > 0) {
            for (var i = 0; i < line.length; i++) {
                if (line[i] != null && line[i] != '')
                    return false;
            }
        }
        return true;
    }

    function getAutoComplete(manual) {
        return function (query, process) {
            var column = (manual == false ? this.col : (this.col == 0 || this.col == 1 ? this.col : this.col - 1));

            if  ($scope.tipoArquivo > 0 && 
                ((manual == true && column == 0) ||
                    (padraoGDSN == 1 && 
                        (($scope.tipoArquivo == 12 && (column == 39 || column == 58 || column == 59 || column == 60 || column == 61 || column == 62 || column == 63 || column == 72)) ||
                         ($scope.tipoArquivo == 13 && (column == 39 || column == 60 || column == 61 || column == 62 || column == 63 || column == 64 || column == 65 || column == 74)) ||
                         ($scope.tipoArquivo == 14 && (column == 41 || column == 60 || column == 61 || column == 62 || column == 63 || column == 64 || column == 65 || column == 74)))
                    ) ||
                    (padraoGDSN == 2 && 
                        (($scope.tipoArquivo == 12 && (column == 39 || column == 57 || column == 66)) ||
                         ($scope.tipoArquivo == 13 && (column == 39 || column == 59 || column == 68)) ||
                         ($scope.tipoArquivo == 14 && (column == 41 || column == 59 || column == 68)))
                    )
                ))
                process(['Sim', 'Não']);
            else if ($scope.tipoArquivo == 0 && column == 2)
                process(['Ativo', 'Inativo']);
            else {
                var dados = getDomainParameters(this.row, column, manual);
                $.ajax({
                    url: 'requisicao.ashx/ImportarProdutosBO/dominios',
                    dataType: 'json',
                    method: 'POST',
                    data: JSON.stringify(dados),
                    success: function (response) {
                        var result = [];
                        for (var i = 0; i < response.length; i++)
                            result.push([response[i].descricao]);
                        process(result);
                    }
                });
            }
        }
    }

    function getCells(row, col, prop) {
        var cellProperties = { readOnly: false, valid: true };

        if ($scope.tipoArquivo > 0 && 
            (
                (($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && padraoGDSN == 2 && ((col >= 9 && col <= 11) || col == 14)) ||
                ($scope.tipoArquivo == 14 && padraoGDSN == 2 && ((col > 10 && col <= 12) || col == 16))
            )) {
            if (hot != null && (hot.getDataAtCell(row, col-1) == null || hot.getDataAtCell(row, col-1) == '')) {
                cellProperties.readOnly = true;
                cellProperties.valid = true;
            }
        } else if ($scope.tipoArquivo == 0 && col == 13) {
            if (hot != null && (hot.getDataAtCell(row, 6) == null || hot.getDataAtCell(row, 6) == '')) {
                cellProperties.readOnly = true;
                cellProperties.valid = true;
            }
        }

        cellProperties.renderer = "errorValueRenderer";
  
        return cellProperties;
    }

    function getAfterChange(change, source) {
        if (change != null && change.length > 0) {
            var row = change[0][0],
                col = change[0][1],
                oldvalue = change[0][2],
                newvalue = change[0][3];

            if (col == 'segmento' || col == 'familia' || col == 'classe' || col == 'paisorigem') {
                switch (col) {
                    case 'segmento':
                        col = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13 ? 9 : 10);
                        break;
                    case 'familia':
                        col = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13 ? 10 : 11);
                        break;
                    case 'classe':
                        col = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13 ? 11 : 12);
                        break;
                    case 'paisorigem':
                        col = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13 ? 14 : 16);
                        break;
                }
            }

            if ((($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && col >= 9 && col <= 12) || 
                ($scope.tipoArquivo == 14 && col >= 10 && col <= 13)) {
                if (hot != null) {
                    var changes = [];
                    var colmax = ($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) ? 11 : 13;
                    if ($scope.tipoArquivo == 14)
                        col++;
                    for (var i = 0; i <= colmax - col; i++) {
                        changes.push([row, col + i, null]);
                    }
                    if (changes.length > 0)
                        hot.setDataAtCell(changes);
                }
            } else if ((($scope.tipoArquivo == 12 || $scope.tipoArquivo == 13) && col == 14) || 
                ($scope.tipoArquivo == 14 && col == 16)) {
                hot.setDataAtCell([[row, col, null]]);
                
            } else if ($scope.tipoArquivo == 0 && col == 6) {
                hot.setDataAtCell([[row, 13, null]]);
            }
        }
    }

    function onAssistant() {
        if (!$("#arquivo").data('ace_input_files')) {
            var mensagem = $translate.instant('18.MENSAGEM_SelecioneArquivoImportacao');
            $scope.alert(typeof mensagem === 'object' || mensagem == '18.MENSAGEM_SelecioneArquivoImportacao' ? 'Selecione uma arquivo para iniciar a importação.' : mensagem);
            return false;
        }

        blockUI.start();

        Utils.uploadFile('arquivo', function (result) {
            if (result == undefined || (result instanceof Array && result.length == 0)) {
                var mensagem = $translate.instant('18.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '18.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.uploadObrigatorio = true;
                return false;
            } else if (result == "Extensão não suportada") {
                var mensagem = $translate.instant('18.MENSAGEM_ExtensaoDiferenteXLSXeCSV');
                $scope.alert(typeof mensagem === 'object' || mensagem == '18.MENSAGEM_ExtensaoDiferenteXLSXeCSV' ? 'Extensão de arquivo diferente do esperado. Selecione um arquivo com uma das seguintes extensões: .xlsx ou .csv.' : mensagem);
                return false;
            } else {
                var dados = { campos: { arquivo: result[0], tipoitem: $scope.itemImportacao.codigo } };

                http.postRequisicao('ImportarProdutosBO', 'LerArquivo', dados).then(function (result) {
                    $scope.columns = result.data.colunas;
                    $scope.tipoArquivo = result.data.tipoArquivo;
                    $scope.tipoitem = result.data.tipoitem;
                    $scope.gdsn = result.data.gdsn;
                    arquivo = result.data.arquivo;
                    padraoGDSN = result.data.padraoGDSN;

                    var registros = result.data.lista;

                    ExibirErros(registros);

                    $scope.exibeValidar = result.data.existeErro;
                    $scope.exibeImportar = !result.data.existeErro;

                    blockUI.stop();

                    var headers = [],
                        columns = [],
                        reg = [],
                        regs = [],
                        error = [],
                        container = document.getElementById('tableVerificacao');
                    for (var i = 0; i < $scope.columns.length; i++) {
                        if ($scope.columns[i].field != 'indicadorgdsn') {
                            headers.push($scope.columns[i].title + ($scope.columns[i].required ? ' (*)' : ''));

                            if (($scope.columns[i].tablereference != null && $scope.columns[i].tablereference != '') || exibirDropdown($scope.columns[i].field))
                            {
                                var col = {
                                    data: $scope.columns[i].field,
                                    type: 'autocomplete',
                                    strict: $scope.columns[i].required,
                                    autoColumnSize: true,
                                    source: getAutoComplete(false)
                                };
                            }
                            else
                            {
                                var col = { data: $scope.columns[i].field };

                                switch($scope.columns[i].type) {
                                    case 'int':
                                    case 'long':
                                        col.type = 'numeric';
                                        break;
                                    case 'datetime':
                                        col.type = 'date';
                                        col.dateFormat = 'DD/MM/YYYY';
                                        col.correctFormat = true;
                                        break;
                                }
                            }
                            columns.push(col);
                        }
                    }

                    for (r in registros) {
                        error = [];

                        for (c in $scope.columns) {
                            if (registros[r].errors[$scope.columns[c].field] != null)
                                error.push(true);
                            else
                                error.push(null);
                        }

                        listErrors.push(error);
                    }

                    Handsontable.renderers.registerRenderer('errorValueRenderer', errorValueRenderer);

                    hot = new Handsontable(container, {
                        data: registros,
                        afterChange: getAfterChange,
                        colHeaders: headers,
                        rowHeaders: true,
                        columns: columns,
                        manualColumnResize: true,
                        manualRowResize: false,
                        allowInsertRow: false,
                        contextMenu: {
                            items: {
                                "remove_row": { name: 'Deseja remover esta linha?' }
                            }
                        },
                        readOnlyCellClassName: 'readOnlyCell',
                        autoColumnSize: {syncLimit: 10},
                        cells: getCells,
                        height: 300
                    });

                    ExibirTela(screen.ASSISTANT);

                    $timeout(function() { hot.render(); hot.selectCell(1, 1); }, 100);

                }, $rootScope.TratarErro);
            }
        });
    }

    function ExibirErros(lista) {
        $scope.listaErros = [];
        $scope.qtdeitem = 0;
        $scope.qtdeErros = 0;
        
        if (lista != null) {
            $scope.qtdeitem = lista.length;
            var contadorLinha = 0;

            angular.forEach(lista, function (value, key) {
                contadorLinha++;
                if (value.errors && !$.isEmptyObject(value.errors)) {
                    var linha = { title: 'Linha ' + contadorLinha + ':', messages: [] };

                    angular.forEach(value.errors, function (value) {
                        linha.messages.push(value);
                        $scope.qtdeErros++;
                    });

                    $scope.listaErros.push(linha);
                }
            });
        }
    }

    function onValidate() {
        var lista = hot.getData();
        //if ($scope.tableVerificacao.data.length > 0) {
        if (lista != null && lista.length > 0) {
            var newlista = [];
            var item = null;

            for (var i = 0; i < lista.length; i++) {
                if (!arrayIsNullOrEmpty(lista[i])) {
                    item = {};

                    for (col in $scope.columns)
                        item[$scope.columns[col].field] = lista[i][col];

                    newlista.push(item);
                }
            }

            var dados = {
                campos: {
                    lista: newlista, //$scope.tableVerificacao.data,
                    tipoArquivo: $scope.tipoArquivo,
                    padraoGDSN: padraoGDSN
                }
            };

            http.postRequisicao('ImportarProdutosBO', 'Validar', dados).then(function (result) {
                var registros = result.data.lista;

                ExibirErros(registros);

                listErrors = [];

                for (r in registros) {
                    error = [];

                    for (c in $scope.columns) {
                        if (registros[r].errors != null && registros[r].errors[$scope.columns[c].field] != null)
                            error.push(true);
                        else
                            error.push(null);
                    }

                    listErrors.push(error);
                }

                $scope.exibeValidar = result.data.existeErro;
                $scope.exibeImportar = !result.data.existeErro;

                //$scope.tableVerificacao.reload();
                //$scope.tableVerificacao.page(1);

                hot.loadData(registros);
            }, $rootScope.TratarErro);
        }
    }

    function onImport() {
        var lista = hot.getData();
        //if ($scope.tableVerificacao.data.length > 0) {
        if (lista != null && lista.length > 0) {
            var newlista = [];
            var item = null;

            for (var i = 0; i < lista.length; i++) {
                if (!arrayIsNullOrEmpty(lista[i])) {
                    item = {};

                    for (col in $scope.columns)
                        item[$scope.columns[col].field] = lista[i][col] || "";

                    newlista.push(item);
                }
            }

            var dados = {
                campos: {
                    lista: newlista,//$scope.tableVerificacao.data,
                    tipoArquivo: $scope.tipoArquivo,
                    tipo: 1, // Template
                    arquivo : arquivo,
                    padraoGDSN: padraoGDSN
                }
            };

            http.postRequisicao('ImportarProdutosBO', 'Importar', dados).then(function (result) {
                //registros = [];

                $scope.exibeValidar = true;
                $scope.exibeImportar = false;

                $scope.tableParams.reload();
                $scope.tableParams.page(1);

                onResult(result.data);
            }, $rootScope.TratarErro);
        }
    }

    
    function onValidateTemplate() {
        var lista = hot.getData();

        if (lista != null && lista.length > 0) {
            var newlista = [];
            var item = null;

            for (var i = 0; i < lista.length; i++) {
                if (!arrayIsNullOrEmpty(lista[i])) {
                    item = {};

                    for (col in $scope.columnsTemplate)
                        item[$scope.columnsTemplate[col].field] = lista[i][col];

                    newlista.push(item);
                }
            }
            
            if (newlista.length > 0) {
                var dados = {
                    campos: {
                        lista: newlista,
                        tipoArquivo: $scope.itemModelo.codigo,
                        padraoGDSN: padraoGDSN
                    }
                };

                http.postRequisicao('ImportarProdutosBO', 'ValidarTemplate', dados).then(function (result) {
                    var registros = result.data.lista;

                    ExibirErros(registros);
                
                listErrors = [];

				$scope.exibeValidar = result.data.existeErro;
				$scope.exibeImportar = !result.data.existeErro;
				
                for (r in registros) {
                    error = [];

                    for (c in $scope.columnsTemplate) {
                        if (registros[r].errors != null && registros[r].errors[$scope.columnsTemplate[c].field] != null)
                            error.push(true);
                        else
                            error.push(null);
                    }

                    listErrors.push(error);
                }

                    $scope.exibeMensagem = true;

                    hot.loadData(registros);
                }, $rootScope.TratarErro);
            } else {
                $scope.alert('Nenhum registro informado. Favor preencher os dados antes de clicar no botão Validar.');
            }
        } else {
            $scope.alert('Nenhum registro informado. Favor preencher os dados antes de clicar no botão Validar.');
        }
    }

    function onImportTemplate() {
        var lista = hot.getData();

        if (lista != null && lista.length > 0) {
            var newlista = [];
            var item = null;

            for (var i = 0; i < lista.length; i++) {
                if (!arrayIsNullOrEmpty(lista[i])) {
                    item = {};

                    for (col in $scope.columnsTemplate)
                        item[$scope.columnsTemplate[col].field] = lista[i][col] || "";

                    newlista.push(item);
                }
            }
            var dados = {
                campos: {
                    lista: newlista,//$scope.tableManual.data,
                    tipoArquivo: $scope.itemModelo.codigo,
                    tipo: 2, // Tela
                    arquivo: arquivo,
                    padraoGDSN: padraoGDSN
                }
            };

            http.postRequisicao('ImportarProdutosBO', 'Importar', dados).then(function (result) {
                //registros = [];

                $scope.exibeValidar = true;
                $scope.exibeImportar = false;

                $scope.tableParams.reload();
                $scope.tableParams.page(1);

                onResult(result.data);
            }, $rootScope.TratarErro);
        }
    }

    function onCancel() {
        $scope.formMode = false;

        $scope.columnsTemplate = [];

        $scope.columns = [];

        $scope.listaErros = [];
        $scope.itemModelo = {};

        $scope.rowTemplate = {isEditing: true};

        $scope.exibeValidar = false;
        $scope.exibeImportar = false;
        $scope.exibeMensagem = false;

        //registros = [];
        $scope.tipoArquivo = -1;
        arquivo = '';

        $scope.filter = {};
        $("#arquivo").ace_file_input('reset_input');

        if ($scope.tableManual != null) {
            $scope.tableManual.reload();
            $scope.tableManual.page(1);
        }

        if (hot != null) {
            hot.destroy();
            hot = null;
        }
//        if ($scope.tableVerificacao != null) {
//            $scope.tableVerificacao.reload();
//            $scope.tableVerificacao.page(1);
//        }

        $timeout(function () {
            ExibirTela(screen.GRID);
        }, 1000);
    };

    //Chamado ao pressionar ESC na tela
//    window.onkeydown = function (event) {
//        if (event.keyCode === 27) {
//            $scope.onCancel();
//        }
//    };

    function onResult(item) {
        $scope.item = angular.copy(item);

        ExibirTela(screen.RESULT);
    }

    function onSearch() {
        $scope.tableParams.reload();
        $scope.tableParams.page(1);
    }


    function onImportManual() {
        ExibirTela(screen.IMPORT);

        $scope.itemModelo = {};
    }

    function onLoadTemplate() {
        $scope.itemModelo.padraoGDSN = 2;

        if (hot != null) {
            hot.destroy();
            hot = null;
        }

        if ($scope.itemModelo != null && $scope.itemModelo.codigo != null && $scope.itemModelo.codigo > 0 && ($scope.itemModelo.codigo == 5 || $scope.itemModelo.padraoGDSN != null)) {
        //if ($scope.itemModelo != null && $scope.itemModelo.codigo != null && $scope.itemModelo.codigo > 0 && $scope.itemModelo.padraoGDSN != null) {
            $scope.columnsTemplate = [];
            padraoGDSN = ($scope.itemModelo.codigo == 5 ? 2 : $scope.itemModelo.padraoGDSN || 2);

            http.postRequisicao('ImportarProdutosBO', 'RecuperarColunasTemplate', { where: { tipoArquivo: $scope.itemModelo.codigo, padraoGDSN: padraoGDSN } }).then(function (result) {
                $scope.columnsTemplate = result.data.colunas;
                $scope.tipoArquivo = result.data.tipoArquivo;

                $scope.exibeValidar = true;
                $scope.exibeImportar = false;
                $scope.rowTemplate = { isEditing: true, indicadorgdsn: $scope.itemModelo.padraoGDSN };
                $scope.listaErros = [];
                $scope.exibeMensagem = false;
                //registros = [];

                var headers = [],
                    columns = [],
                    error = [],
                    registros = [[]];
                    container = document.getElementById('tableManual');

                for (var i = 0; i < $scope.columnsTemplate.length; i++) {
                    headers.push($scope.columnsTemplate[i].title + ($scope.columnsTemplate[i].required ? ' (*)' : ''));

                    if (($scope.columnsTemplate[i].tablereference != null && $scope.columnsTemplate[i].tablereference != '') || exibirDropdown($scope.columnsTemplate[i].field))
                    {
                        var col = {
                            data: $scope.columnsTemplate[i].field,
                            type: 'autocomplete',
                            strict: $scope.columnsTemplate[i].required,
                            autoColumnSize: true,
                            source: getAutoComplete(false)
                        };
                    }
                    else
                    {
                        var col = { data: $scope.columnsTemplate[i].field };

                        switch($scope.columnsTemplate[i].type) {
                            case 'int':
                            case 'long':
                                col.type = 'numeric';
                                break;
                            case 'datetime':
                                col.type = 'date';
                                col.dateFormat = 'DD/MM/YYYY';
                                col.correctFormat = true;
                                break;
                        }
                    }
                    columns.push(col);
                }

                Handsontable.renderers.registerRenderer('errorValueRenderer', errorValueRenderer);

                hot = new Handsontable(container, {
                    data: registros,
                    afterChange: getAfterChange,
                    colHeaders: headers,
                    rowHeaders: true,
                    columns: columns,
                    manualColumnResize: true,
                    manualRowResize: false,
                    allowInsertRow: true,
                    renderAllRows: true,
                    contextMenu: {
                        items: {
                            "row_below": { name: 'Adicionar linha' },
                            "remove_row": { name: 'Deseja remover esta linha?' }
                        }
                    },
                    autoColumnSize: {syncLimit: 10},
                    cells: getCells,
                    height: 300
                });



                $timeout(function() { hot.render(); hot.selectCell(1, 1); }, 100);
            }, $rootScope.TratarErro);
        } else if ($scope.itemModelo.codigo == null || $scope.itemModelo.codigo == 0) {

            var mensagem = $translate.instant('18.MENSAGEM_SelecioneTipoItem');
            $scope.alert(typeof mensagem === 'object' || mensagem == '18.MENSAGEM_SelecioneTipoItem' ? 'Selecione o tipo de item antes de carregar o template.' : mensagem);
        } else if ($scope.itemModelo.padraoGDSN == null) {
            
            var mensagem = $translate.instant('18.MENSAGEM_SelecionePadraoGDSN');
            $scope.alert(typeof mensagem === 'object' || mensagem == '18.MENSAGEM_SelecionePadraoGDSN' ? 'Selecione o padrão GDSN.' : mensagem);
        }
    }

    function exibirDropdown(fieldname) {
        return fieldname == 'indicadorgdsn' ||
               fieldname == 'compartilhadados' ||
               fieldname == 'itemcomercialmodelo' ||
               fieldname == 'indicadormercadoriasperigosas' ||
               fieldname == 'indicadoritemcomercialliberadocompra' ||
               fieldname == 'indicadorunidadedespacho' ||
               fieldname == 'indicadorunidadebasicavenda' ||
               fieldname == 'indicadorunidadefaturamento' ||
               fieldname == 'indicadoritemcomercialunidadeconsumo' ||
               fieldname == 'statusgln';
    }

    $scope.onImportManual = onImportManual;
    $scope.onLoadTemplate = onLoadTemplate;
    $scope.onImport = onImport;
    $scope.onImportTemplate = onImportTemplate;
    $scope.onValidate = onValidate;
    $scope.onValidateTemplate = onValidateTemplate;
    $scope.onDownload = onDownload;
    $scope.onAssistant = onAssistant;
    $scope.onCancel = onCancel;
    $scope.onResult = onResult;
    $scope.onSearch = onSearch;

    $scope.exibirDropdown = exibirDropdown;

    $scope.exibeValidar = false;
    $scope.exibeImportar = false;

    preInit();
} ]);