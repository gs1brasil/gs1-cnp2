app.controller('IntegracaoCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {

        $scope.isPesquisa = false;
        $scope.integracoesMarcadas = [];

        var titulo = $translate.instant('53.LBL_Integracao');
        $scope.titulo = typeof titulo === 'object' || titulo == '53.LBL_Integracao' ? 'Integração' : titulo;

        $scope.tableIntegracao = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                nome: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        $scope.tableFuncionalidadeIntegracao = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                //if ($scope.codigoAssociado != undefined) {
                var dados = carregaFuncionalidades($scope.codigoToken, function (result) {
                    params.total(result.data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(result.data, params.orderBy()) : result.data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
                //}
            }
        });

        //Busca papeis GLN
        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                    if (isPesquisa) {
                        dados = angular.copy(item);

                        genericService.postRequisicao('IntegracaoBO', 'BuscarToken', { campos: dados }).then(
                            function (result) {
                                $scope.form.submitted = false;
                                    $scope.editMode = false;

                                    if (callback != undefined && callback != null) {
                                        callback(result.data);
                                    }
                                },
                            $rootScope.TratarErro
                        );
                    }
                    else {
                        genericService.postRequisicao('IntegracaoBO', 'BuscarTokens', { where: dados }).then(
                            function (result) {

                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                        );
                    }

                    $rootScope.flagAssociadoSelecionado = true;
                }
            });
        }

        function carregaFuncionalidades(codigo, callback) {

            genericService.postRequisicao('IntegracaoBO', 'BuscarFuncionalidadesIntegracao', { campos: { "codigo": codigo || null} }).then(
                function (result) { callback(result); },
                $rootScope.TratarErro
            );
        }

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('IntegracaoBO', 'CadastrarToken', function () {

                var mensagem = $translate.instant('53.LBL_Integracao');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '53.LBL_Integracao' ? 'Integração' : mensagem;
                $scope.item = {};
                $scope.isPesquisa = false;
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.exibeVoltar = true;

                $scope.codigoToken = "";
                $scope.tableFuncionalidadeIntegracao.page(1);
                $scope.tableFuncionalidadeIntegracao.reload();
            });
        };

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('IntegracaoBO', 'AtualizarToken', function () {

                Utils.bloquearCamposVisualizacao("VisualizarIntegracao");

                var mensagem = $translate.instant('53.LBL_AlterarIntegracao');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '53.LBL_AlterarIntegracao' ? 'Alterar Integração' : mensagem;
                $scope.item = angular.copy(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;

                $scope.codigoToken = item.codigo;
                $scope.tableFuncionalidadeIntegracao.page(1);
                $scope.tableFuncionalidadeIntegracao.reload();
            });
        };

        $scope.onCancel = function (item) {
            var mensagem = $translate.instant('53.LBL_Integracao');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '53.LBL_Integracao' ? 'Integração' : mensagem;
            $scope.item = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $scope.associadosMarcados = [];
            Utils.moveScrollTop();
            $scope.tableIntegracao.page(1);
            $scope.tableIntegracao.reload();
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        $scope.onSearchMode = function (item) {
            var mensagem = $translate.instant('53.LBL_PesquisarIntegracao');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '53.LBL_PesquisarIntegracao' ? 'Pesquisar Integração' : mensagem;
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
        };

        //Remove Integração cadastrado
        $scope.onDelete = function (item) {
            $rootScope.verificaPermissao('IntegracaoBO', 'RemoverToken', function () {

                var titulo = $translate.instant('53.LBL_Confirmacao');
                var mensagem = $translate.instant('53.MENSAGEM_DesejaRemover');

                $rootScope.confirm(typeof mensagem === 'object' || mensagem == '53.MENSAGEM_DesejaRemover' ? 'Deseja remover o item selecionado?' : mensagem, function () {
                    genericService.postRequisicao('IntegracaoBO', 'RemoverIntegracao', { campos: item }).then(
                        function (result) {
                            $scope.form.submitted = false;
                            Utils.moveScrollTop();
                            $scope.tableIntegracao.reload();
                            $scope.tableIntegracao.page(1);
                            $scope.item = {};

                            var mensagem = $translate.instant('53.MENSAGEM_DadosRemovidosSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '53.MENSAGEM_DadosRemovidosSucesso' ? 'Dados removidos com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );

                }, function () { }, typeof titulo === 'object' || titulo == '53.LBL_Confirmacao' ? 'Confirmação' : titulo);
            });
        }

        //Pesquisa Integração cadastrado
        $scope.onSearch = function (item) {
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableIntegracao.reload();
            $scope.tableIntegracao.page(1);
        }

        $scope.onSave = function (item) {

            if (!$scope.form.$valid) {

                var mensagem = $translate.instant('53.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '53.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
            }
            else if (!verifyTableIntegracoes()) {
                return false;
            }
            else {
                var item = angular.copy(item);

                //Converte datas
                d = new Date();
                var dataAtual = d.mmddyyyy();

                var dataCompare = Utils.dataAnoMesDia(item.dataexpiracao);
                item.dataexpiracao = Utils.dataMesDiaAno(item.dataexpiracao);

                //Validação de datas
                if ((item.dataexpiracao != undefined && item.dataexpiracao != null && item.dataexpiracao != '') &&
                            (!Utils.isValidDate(dataCompare))) {

                    var mensagem = $translate.instant('53.MENSAGEM_DataInvalida');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '53.MENSAGEM_DataInvalida' ? 'Data inválida.' : mensagem);

                    return false;
                }

                //Inserção de Integração
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {
                    if (item.dataexpiracao < dataAtual) {

                        var mensagem = $translate.instant('53.MSG_DataExpiracaoMaior');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '53.MSG_DataExpiracaoMaior' ? 'A data de expiração não pode ser menor do que a data atual.' : mensagem);
                        return false;
                    }

                    var parametro = {
                        dataexpiracao: item.dataexpiracao,
                        codigostatustoken: item.codigostatustoken,
                        arrayIntegracoes: $scope.integracoesMarcadas
                    };

                    genericService.postRequisicao('IntegracaoBO', 'CadastrarToken', { campos: parametro }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            Utils.moveScrollTop();
                            $scope.tableIntegracao.page(1);
                            $scope.tableIntegracao.reload();

                            var mensagem = $translate.instant('53.MENSAGEM_RegistroSalvoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '53.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                }
                //Atualização de Integração
                else {
                    var parametro = {
                        dataexpiracao: item.dataexpiracao,
                        codigostatustoken: item.codigostatustoken,
                        arrayIntegracoes: $scope.integracoesMarcadas
                    };

                    genericService.postRequisicao('IntegracaoBO', 'AtualizarToken', { campos: parametro, where: item.codigo }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            Utils.moveScrollTop();
                            $scope.tableIntegracao.page(1);
                            $scope.tableIntegracao.reload();

                            var mensagem = $translate.instant('53.MENSAGEM_RegistroAlteradoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '53.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                }

            }
        }

        //Conversão da data atual para MM-DD-YYYY
        Date.prototype.mmddyyyy = function () {

            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = this.getDate().toString();

            date = (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + '/' + yyyy;
            return date;
        };

        //Carregamento dos Status Token
        $scope.buscarStatusToken = function () {
            genericService.postRequisicao('StatusTokenBO', 'BuscarStatusTokenAtivo', {}).then(
                function (result) {
                    $scope.statusToken = result.data;
                },
                $rootScope.TratarErro
            );
        }

        function verifyTableIntegracoes() {

            $scope.integracoesMarcadas = [];

            for (var i = 0; i < $scope.tableFuncionalidadeIntegracao.data.length; i++) {

                if ($scope.tableFuncionalidadeIntegracao.data[i].checked == true) {
                    $scope.integracoesMarcadas.push($scope.tableFuncionalidadeIntegracao.data[i].codigo);
                }
            }

            if ($scope.integracoesMarcadas.length == 0) {
                var mensagem = $translate.instant('53.MENSAGEM_SelecioneUmaFuncionalidade');
                $scope.alert(typeof mensagem === 'object' || mensagem == '53.MENSAGEM_SelecioneUmaFuncionalidade' ? 'Selecione pelo menos uma funcionalidade para continuar.' : mensagem);
                return false;
            }
            else {
                return true;
            }
        }

        $scope.insereIntegracaoLista = function (integracao) {

            var joinedData = {
                codigo: integracao.codigo,
                nome: integracao.nome,
                checked: 1
            }

            var exists = false;

            for (var i = 0; i < $scope.tableFuncionalidadeIntegracao.data.length; i++) {
                if ($scope.tableFuncionalidadeIntegracao.data[i].codigo == integracao.codigo) {
                    exists = true;
                }
            }

            if (!exists) {
                $scope.tableFuncionalidadeIntegracao.data.push(joinedData);
                $scope.integracoesMarcadas.push(integracao.codigo);
            }
        }

        $scope.onDeleteFuncionalidadeTable = function (item) {
            for (var i in $scope.tableFuncionalidadeIntegracao.data) {
                if ($scope.tableFuncionalidadeIntegracao.data[i].codigo == item.codigo) {
                    $scope.tableFuncionalidadeIntegracao.data.splice(i, 1);
                }
            }
        }

        function findAndRemove(array, property, value) {
            $.each(array, function (index, result) {
                if (result[property] == value) {
                    //Remove from array
                    array.splice(index, 1);
                }
            });
        }

        //CONTROLE DE CHECKBOX
        $scope.flagMarcarIntegracao = function (item) {
            if (item != undefined && item.checked === 1) {
                item.checked = false;
                $scope.removerPorId($scope.integracoesMarcadas, item.codigo);
            }
            else {
                if (item.checked) {
                    $scope.integracoesMarcadas.push(item.codigo);
                }
                else {
                    $scope.removerPorId($scope.integracoesMarcadas, item.codigo);
                }
            }
        }

        $scope.removerPorId = function (itens, codigo) {
            for (var i in itens) {
                if (itens[i] == codigo) {
                    itens.splice(i, 1);
                }
            }
        }

        $scope.marcarDesmarcarIntegracoes = function (flag) {

            if (flag == true) {
                $scope.marcarTodasIntegracoes = true;
                $scope.integracoesMarcadas = [];
                angular.forEach($scope.tableFuncionalidadeIntegracao.data, function (val, key) {
                    $scope.integracoesMarcadas.push(val.codigo);
                    val.checked = true;
                });
            } else {
                $scope.marcarTodasIntegracoes = false;
                $scope.integracoesMarcadas = [];
                angular.forEach($scope.tableFuncionalidadeIntegracao.data, function (val, key) {
                    val.checked = false;
                });
            }
        };


        $scope.onLoad = function () {
            $scope.buscarStatusToken();
        }

        $scope.onLoad();
    } ]);
