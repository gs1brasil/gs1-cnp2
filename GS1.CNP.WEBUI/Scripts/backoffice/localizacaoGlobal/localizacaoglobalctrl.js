app.controller('LocalizacaoGlobalCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$log', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $log, $modal, genericService, Utils, blockUI, $q, $translate) {

    //inicializando abas do form
    $('#tabs-ajuda a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var titulo = $translate.instant('68.LBL_ConsultarNumeroLocalizacaoGlobal');
    $scope.titulo = typeof titulo === 'object' || titulo == '68.LBL_ConsultarNumeroLocalizacaoGlobal' ? 'Consultar Número de Localização Global' : titulo;
    $scope.form = {};
    $scope.itemForm = {};
    $scope.initMode = true;
    $scope.showInputs = false;

    $scope.tableCampos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if (!$scope.initMode) {
                var dados = carregaGridCampos($scope.itemForm, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        }
    });

    $scope.tableCamposDono = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if (!$scope.initMode) {
                var dados = carregaGridCampos($scope.itemForm, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        }
    });


    //Popula grid de campos
    function carregaGridCampos(item, callback) {

        if ($scope.itemForm.tipoconsulta == 2) {
            genericService.postRequisicao('GepirRouterBO', 'ConsultarWSPartyByGLN', { campos: item }).then(
            function (result) {
                $scope.consultaefetuada = true;
                if (result.data != undefined && result.data != null && result.data != "") {
                    callback(result.data.partyDataLine == null ? [] : result.data.partyDataLine);
                }
            },
            $rootScope.TratarErro
        );
        } else {
            item.key = "GLN"
            item.value = item.gln;
            genericService.postRequisicao('GepirRouterBO', 'getKeyLicensee', { campos: item }).then(
                function (result) {
                    $scope.consultaefetuada = true;
                    if (result.data != undefined && result.data != null && result.data != "") {                        
                        callback(result.data.gepirParty == null ? [] : result.data.gepirParty);
                    }
                },
                $rootScope.TratarErro
            );
        }
    };


    $scope.tipoConsulta = function () {
        //        $scope.tiposConsulta = [
        //            {
        //                "codigo": 1,
        //                "nome": $translate.instant('66.LBL_InformacoesDonoItem') //'Detalhes sobre o dono do item'
        //            },
        //            {
        //                "codigo": 2,
        //                "nome": $translate.instant('66.LBL_DetalhesItem') //'Detalhes do Item'
        //            }
        //        ]

        $scope.tiposConsulta = [
            {
                "codigo": 1,
                "nome": "Detalhes sobre o dono do item"
            },
            {
                "codigo": 2,
                "nome": "Detalhes do Item"
            }
        ]


    }

    //Método Inicial da Página
    $scope.onLoad = function () {
        $scope.shouldLoadCampos = true;
        $scope.itemForm = {};
        $scope.tipoConsulta();
        $scope.consultaefetuada = false;

        $rootScope.verificaAssociadoSelecionado(function (data) {

            if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                $rootScope.flagAssociadoSelecionado = true;
            } else {
                $rootScope.flagAssociadoSelecionado = false;
            }
        });
    }

    $scope.onLoad();

    $scope.alteraTipoConsulta = function () {
        $scope.consultaefetuada = false;
    }

    //Limpar campos
    $scope.onClean = function () {
        $scope.onLoad();
    }

    $scope.onSearch = function (item) {

        if (!$scope.form.$valid) {
            var mensagem = $translate.instant('68.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '68.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            $scope.form.submitted = true;
        }
        else {
            if (item.gln.toString().length < 13) {
                var mensagem = $translate.instant('68.MENSAGEM_GLN13digitos');
                $scope.alert(typeof mensagem === 'object' || mensagem == '61.MENSAGEM_GLN13digitos' ? 'O GLN deve possuir exatamente 13 dígitos.' : mensagem);
            }
            else if (!Utils.validaDigitoVerificador(item.gln.toString())) {
                var mensagem = $translate.instant('68.MENSAGEM_DigitoVerificadorInvalido');
                $scope.alert(typeof mensagem === 'object' || mensagem == '68.MENSAGEM_DigitoVerificadorInvalido' ? 'Dígito Verificador inválido.' : mensagem);
            }
            else {
                $scope.initMode = false;
                $scope.itemForm = item;
                if ($scope.itemForm.tipoconsulta == 2) {
                    $scope.tableCampos.reload();
                    $scope.tableCampos.page(1);
                } else {
                    $scope.tableCamposDono.reload();
                    $scope.tableCamposDono.page(1);
                }
            }
        }
    }
} ]);