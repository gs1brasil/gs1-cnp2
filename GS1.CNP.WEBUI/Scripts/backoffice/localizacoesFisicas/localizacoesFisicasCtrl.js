app.controller('LocalizacoesFisicasCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', 'uiGmapIsReady', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, IsReady, $translate) {

        //inicializando abas do form
        $('#tabs-localizacao a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        var titulo = $translate.instant('14.LBL_CadastroLocalizacaoFisica');
        $scope.titulo = typeof titulo === 'object' || titulo == '14.LBL_CadastroLocalizacaoFisica' ? 'Cadastro de Localizações Físicas' : titulo;
        $scope.isPesquisa = false;

        $scope.tableLocalizacoes = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                nome: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        //Busca no WebService informações do CEP
        $scope.buscarWebServiceCep = function (cep) {
          
                if(cep != ""){
                    blockUI.start();
                    $scope.UFS = [];                                    
                    genericService.postRequisicao('CEPBO', 'consultaEnderecoPorCEP', { campos: {cep: cep} }).then(
                        function (result) {
                            if(result.data != undefined && result.data != null){

                                //CEP completo encontrado
                                if(result.data.Out_LogradouroP.trim() != ""){
                                    $scope.item.endereco = result.data.Out_LogradouroP;
                                    $scope.item.bairro = result.data.Out_BairroP;                                                                
                                    $scope.item.cidade = result.data.Out_LocalidadeP;   
                                    $scope.UFS = [{"uf":result.data.Out_UFP}];                                    
                                    $scope.item.estado = result.data.Out_UFP;
                                    $("#estado").val(result.data.Out_UFP);
                                    $scope.AlternaCamposCEP(false);                            
                                }

                                //CEP genérico
                                if(result.data.Out_LogradouroP.trim() == "" && result.data.Out_LocalidadeP.trim() !=""){
                                    $scope.UFS = [{"uf":result.data.Out_UFP}];                                    
                                    $scope.item.estado = result.data.Out_UFP;                                    
                                    $("#estado").val(result.data.Out_UFP);
                                    $scope.item.cidade = result.data.Out_LocalidadeP;
                                    $scope.AlternaCamposCEPGenerico(false);                                                
                                }

                                //CEP não encontrado
                                if(result.data.Out_LogradouroP.trim() == "" && result.data.Out_LocalidadeP.trim() ==""){
                                    $scope.AlternaCamposCEP(true); 
                                    $scope.consultarUF($scope.item.pais);                           
                                }
                                                                                                                   
                            }   
                            blockUI.stop();                            
                        },
                        $rootScope.TratarErro
                    );
            
                }else{
                    $scope.item.endereco = "";
                    $scope.item.bairro = "";                            
                    $scope.item.estado = "";
                    $scope.item.cidade = "";                                    
                    $scope.item.numero = "";
                    $scope.item.complemento = "";
                    $scope.AlternaCamposCEP(true);
                }
            
        }

        $scope.AlternaCamposCEP = function(statuscampo){
            $('#endereco').prop('disabled', !statuscampo);
            $('#bairro').prop('disabled', !statuscampo);
            $('#estado').prop('disabled', !statuscampo);
            $('#txtestado').prop('disabled', !statuscampo);
            $('#cidade').prop('disabled', !statuscampo);            
        }

        $scope.AlternaCamposCEPGenerico = function(statuscampo){
            $('#endereco').prop('disabled', false);
            $('#bairro').prop('disabled', false);
            $('#estado').prop('disabled', !statuscampo);
            $('#txtestado').prop('disabled', !statuscampo);
            $('#cidade').prop('disabled', !statuscampo);            
        }

        $scope.limpaEndereco = function(){                    
            
            if($scope.item.pais != undefined && $scope.item.pais != null && $scope.item.pais != "")
            {
                if( $scope.item.pais == 'Brazil'){
                    $scope.AlternaCamposCEP(false);
                    if($scope.item.cep!= undefined && $scope.item.cep!="")
                    {
                        //$scope.validaCEP($scope.item.cep);
                        $scope.buscarWebServiceCep($scope.item.cep);
                    }
                }else{
                    $scope.consultarUF($scope.item.pais);
                    $scope.AlternaCamposCEP(true);
                }
            }else{
                $scope.AlternaCamposCEP(true);
            }
        }

        $scope.consultarPaises = function () {
            if (!$scope.isPesquisa) {
                genericService.postRequisicao('CEPBO', 'consultarPaises', {}).then(
                    function (result) {
                        if(result.data != undefined && result.data != null){
                            $scope.Paises = result.data;
                        }                               
                    },
                    $rootScope.TratarErro
                );
            }
        }
        $scope.consultarPaises();
      
        $scope.consultarUF = function (pais) {
            if (!$scope.isPesquisa) {
                genericService.postRequisicao('CEPBO', 'consultarUF', { campos: {pais: pais}}).then(
                    function (result) {
                        if(result.data != undefined && result.data != null){
                            $scope.UFS = result.data;                          
                        }                               
                    },
                    $rootScope.TratarErro
                );
            }
        }

        //Busca Localizações Físicas
        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                    if (isPesquisa) {
                        dados = angular.copy(item);

                        genericService.postRequisicao('LocalizacoesFisicasBO', 'PesquisaLocalizacaoFisica', { campos: dados }).then(
                            function (result) {

                                $scope.form.submitted = false;
                                $scope.editMode = false;

                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                         );
                    }
                    else {
                        genericService.postRequisicao('LocalizacoesFisicasBO', 'BuscarLocalizacoesFisicas', { campos: dados }).then(
                            function (result) {
                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                         );
                    }

                    $rootScope.flagAssociadoSelecionado = true;
                }
            });
        }

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('LocalizacoesFisicasBO', 'CadastrarLocalizacoesFisicas', function () {
                var titulo = $translate.instant('14.LBL_CadastroLocalizacaoFisica');
                $scope.titulo = typeof titulo === 'object' || titulo == '14.LBL_CadastroLocalizacaoFisica' ? 'Cadastro de Localizações Físicas' : titulo;
                $scope.limparUploadFiles();
                $scope.exibeVoltar = true;
                
                //genericService.postRequisicao('LocalizacoesFisicasBO', 'BuscarPrefixo', {where: 'bo.nu.telefone'}).then(
                genericService.postRequisicao('LocalizacoesFisicasBO', 'BuscarPrefixo', {}).then(
                    function (prefixos) {
                        
                        

                        Utils.bloquearCamposVisualizacao("VisualizarLocalizacaoFisica");

                        if (prefixos && prefixos.data && prefixos.data.length > 0 && prefixos.data[0].prefixo) {
                            $scope.item = {};
                            $scope.item.pais = 'Brazil';
                            $scope.consultarUF($scope.item.pais);
                            $scope.editMode = true;
                            $scope.searchMode = false;
                            $scope.isPesquisa = false;
                            angular.element("#mapsAddress").val("");                    
                            $scope.AlternaCamposCEP(true); 
                            $scope.UFS = [];
                            
                            $scope.prefixo = prefixos.data[0].prefixo;
                        } else {
                            var dados = { where: 'bo.nu.telefone' };
                            genericService.postRequisicao('LoginBO', 'CarregarParametrosLogin', dados).then(function (result) {
                                if (result.data != undefined && result.data != null && result.data.length > 0) {
                                    $scope.telefone = result.data[0].valor;
                                } else {
                                    $scope.telefone = '';
                                }
                                var mensagem = $translate.instant(['14.MENSAGEM_AssociadoSemLicencaParaCadastrarLocalizacaoFisica'], ['14.MENSAGEM_AtravesDoNossoChatNoPortal']);
                                $scope.alert(
                                    typeof mensagem === 'object' || mensagem == ['14.MENSAGEM_AssociadoSemLicencaParaCadastrarLocalizacaoFisica', '14.MENSAGEM_AtravesDoNossoChatNoPortal'] ?
                                    'Este associado não possui licença apropriada para cadastrar localizações físicas. Por favor entre em contato com o atendimento GS1 Brasil: Tel. ' + $scope.telefone + ' ou através do nosso chat no portal www.gs1br.org.' :
                                     mensagem['14.MENSAGEM_AssociadoSemLicencaParaCadastrarLocalizacaoFisica'] + ' ' +$scope.telefone + ' ' + mensagem['14.MENSAGEM_AtravesDoNossoChatNoPortal']
                                );
                            })
                            .catch(function(fallback) {

                                var mensagem = $translate.instant('14.MENSAGEM_AssociadoSemLicencaApropriadaParaCadastrarLocalizacao');
                                $scope.alert(
                                    typeof mensagem === 'object' || mensagem == '14.MENSAGEM_AssociadoSemLicencaApropriadaParaCadastrarLocalizacao' ? 
                                    'Este associado não possui licença apropriada para cadastrar localizações físicas. Por favor entre em contato com o atendimento GS1 Brasil por telefone ou através do nosso chat no portal www.gs1br.org.' :
                                    mensagem
                                );
                            });
                        }
                    },
                    $rootScope.TratarErro
                );

                
            });
        };

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('LocalizacoesFisicasBO', 'EditarLocalizacaoFisica', function () {
                var titulo = $translate.instant('14.LBL_AlterarLocalizacao');
                $scope.titulo = typeof titulo === 'object' || titulo == '14.LBL_AlterarLocalizacao' ? 'Alterar Localização Física' : titulo;

                if (item != undefined && item.nomeimagem == "") {
                    $scope.nomeimagem = undefined;
                    item.nomeimagem = undefined;
                }
                else {
                    $scope.nomeimagem = item.nomeimagem;
                }

                $scope.limparUploadFiles();
                $scope.item = angular.copy(item);
                $('#tabs-localizacao a:first').tab('show');
                angular.element("#mapsAddress").val("");
                $scope.desabilitaCampos = true;
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;

//                if($scope.item.nomeestado == undefined || $scope.item.nomeestado == '') {
//                    if($scope.item.pais == 'Brazil'){
//                        //$scope.validaCEP($scope.item.cep);
//                        $scope.buscarWebServiceCep($scope.item.cep);
//                    } else{
//                        $scope.AlternaCamposCEP(true);
//                        $scope.consultarUF($scope.item.pais);
//                    }

//                    console.log($scope.item);
//                }

//                if($scope.item.pais == 'Brazil'){
//                    //$scope.validaCEP($scope.item.cep);
//                    $scope.buscarWebServiceCep($scope.item.cep);
//                }else{
//                    $scope.AlternaCamposCEP(true);
//                    $scope.consultarUF($scope.item.pais);
//                }
                
                Utils.bloquearCamposVisualizacao("VisualizarLocalizacaoFisica");

                //Cria o marker no mapa
                if ($scope.item.latitude != undefined && $scope.item.latitude != null && $scope.item.latitude != '' &&
                    $scope.item.longitude != undefined && $scope.item.longitude != null && $scope.item.longitude != '') {

                    $scope.map.center.latitude = $scope.item.latitude;
                    $scope.map.center.longitude = $scope.item.longitude;
                    $scope.map.zoom = 16;

                    var newMarkers = [];

                    var marker = {
                        id: 0,
                        place_id: 0,
                        name: '',
                        latitude: $scope.item.latitude,
                        longitude: $scope.item.longitude,
                        options: {
                            visible: false
                        },
                        templateurl: 'window.tpl.html',
                        templateparameter: {}
                    };

                    newMarkers.push(marker);
                    $scope.map.markers = newMarkers
                }
                else {
                    angular.element("#mapsAddress").val("");

                    $scope.map = {
                        center: {
                            latitude: -22.16980650917504,
                            longitude: -22.16980650917504
                        },
                        zoom: 2,
                        dragging: false,
                        bounds: {},
                        markers: [],
                        idkey: 'place_id',
                        control: {},
                        events: {}
                    }
                }
            });
        }

        $scope.alterImage = function () {
            $scope.nomeimagem = undefined
        }

        $scope.onCancel = function (item) {
            var titulo = $translate.instant('14.LBL_CadastroLocalizacaoFisica');
            $scope.titulo = typeof titulo === 'object' || titulo == '14.LBL_CadastroLocalizacaoFisica' ? 'Cadastro de Localizações Físicas' : titulo;
            $scope.item = {};
            $scope.item.email = undefined;
            $('#tabs-localizacao a:first').tab('show');
            $scope.limparUploadFiles();
            $scope.desabilitaCampos = false;
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableLocalizacoes.page(1);
            $scope.tableLocalizacoes.reload();
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        $scope.onSearchMode = function (item) {
            var titulo = $translate.instant('14.LBL_PesquisarLocalizacao');
            $scope.titulo = typeof titulo === 'object' || titulo == '14.LBL_PesquisarLocalizacao' ? 'Pesquisar Localizações Físicas' : titulo;
            $scope.item = {};
            $scope.item.email = undefined;
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.limparUploadFiles();
            $('#tabs-localizacao a:first').tab('show');
            angular.element("#mapsAddress").val("");
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
            $scope.item.email = undefined;
            $scope.limparUploadFiles();
        }

        //Exclusão de imagem cadastrada
        $scope.ExcluirImagem = function (item) {
            var mensagem = $translate.instant('14.MENSAGEM_ImagemSeraExcluidaBase');
            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_ImagemSeraExcluidaBase' ? 'A imagem selecionada será excluída da nossa base de dados. Deseja realmente remover a imagem?' : mensagem, function () {
                genericService.postRequisicao('LocalizacoesFisicasBO', 'DeletarImagem', { campos: item }).then(
                    function (result) {
                        item.nomeimagem = undefined;
                        $scope.nomeimagem = undefined;

                        var mensagem = $translate.instant('14.MENSAGEM_ImagemRemovidaSucesso');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_ImagemRemovidaSucesso' ? 'Imagem removida com sucesso.' : mensagem);
                    },
                    $rootScope.TratarErro
                );

            }, function () { }, 'Confirmação');
        }

        //Gerador de VCard
        $scope.onGenerateVCard = function (item) {
            if (item != undefined && item != null) {
                $("#codigolocalizacaofisica").val(item.codigo);
                $('#VcardHandler').submit();
            }
        }

        $scope.limparUploadFiles = function () {
            $("#imagemLocalizacao").val('');
        }

        $scope.onInit = function () {

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                    //Busca informações do Dropdwon de Papel GLN
                    genericService.postRequisicao('LocalizacoesFisicasBO', 'BuscarPapeisGLN', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.papeis = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    $rootScope.flagAssociadoSelecionado = true;
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }

            });
        }

        //Remove Localizações Físicas cadastrada
        $scope.onDelete = function (item) {
            $rootScope.verificaPermissao('LocalizacoesFisicasBO', 'RemoverLocalizacoesFisicas', function () {
                var mensagem = $translate.instant('14.MENSAGEM_DesejaRemover');
                $rootScope.confirm(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {
                    genericService.postRequisicao('LocalizacoesFisicasBO', 'RemoverLocalizacoesFisicas', { campos: item }).then(
                        function (result) {
                            $scope.form.submitted = false;
                            $scope.tableLocalizacoes.page(1);
                            $scope.tableLocalizacoes.reload();
                            $scope.item = {};
                            var mensagem = $translate.instant('14.MENSAGEM_DadosRemovidosSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_DadosRemovidosSucesso' ? 'Dados removidos com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );

                }, function () { }, 'Confirmação');
            });
        }

        //Pesquisa Localizações Físicas cadastradas
        $scope.onSearch = function (item) {
            $scope.limparUploadFiles();
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableLocalizacoes.page(1);
            $scope.tableLocalizacoes.reload();
        };

        $scope.onSave = function (item) {
            if (!$scope.form.$valid) {
                $('#tabs-localizacao a:first').tab('show');
                var mensagem = $translate.instant('14.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
            }
            else {

                //Inserção de Papel GLN
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {

                    var mensagem = $translate.instant('14.MENSAGEM_GravarLocalizacaoFisica');
                    $scope.confirm(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_GravarLocalizacaoFisica' ? 'Deseja gravar as informações da localização física? Após confirmação, não será possível alterar as informações.' : mensagem, function () {
                        
                        if(item != undefined) {
                            item.prefixo = $scope.prefixo
                        }

                        //Se não possuir imagem selecionada, insere na base normalmente.
                        if (item != undefined && item.nomeimagem == undefined) {
                            //item.nomeimagem = "";

                            genericService.postRequisicao('LocalizacoesFisicasBO', 'CadastrarLocalizacoesFisicas', { campos: item }).then(
                                function (result) {
                                    $scope.item = {};
                                    $scope.desabilitaCampos = false;
                                    $scope.editMode = false;
                                    $scope.searchMode = false;
                                    $scope.form.submitted = false;
                                    $scope.isPesquisa = false;
                                    $scope.exibeVoltar = false;
                                    $('#tabs-localizacao a:first').tab('show');
                                    Utils.moveScrollTop();
                                    $scope.tableLocalizacoes.page(1);
                                    $scope.tableLocalizacoes.reload();
                                    $scope.AlternaCamposCEP(true);
                                    var mensagem = $translate.instant('14.MENSAGEM_RegistroSalvoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                                },
                                $rootScope.TratarErro
                            );
                        }
                        //Se possuir imagem selecionada, faz o upload e depois insere na base.
                        else {
                            Utils.upload('imagemLocalizacao', 'blobAzure', 'TipoProdutoBO', function (result) {

                                if (result == undefined) {
                                    return false;
                                }
                                else {
                                    if(result.length > 0) {
                                        item.nomeimagem = result[0];
                                    }

                                    genericService.postRequisicao('LocalizacoesFisicasBO', 'CadastrarLocalizacoesFisicas', { campos: item }).then(
                                        function (result) {
                                            $scope.item = {};
                                            $scope.desabilitaCampos = false;
                                            $scope.editMode = false;
                                            $scope.searchMode = false;
                                            $scope.form.submitted = false;
                                            $scope.isPesquisa = false;
                                            $scope.exibeVoltar = false;
                                            $('#tabs-localizacao a:first').tab('show');
                                            Utils.moveScrollTop();
                                            $scope.tableLocalizacoes.page(1);
                                            $scope.tableLocalizacoes.reload();
                                            var mensagem = $translate.instant('14.MENSAGEM_RegistroSalvoSucesso');
                                            $scope.alert(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                                        },
                                        $rootScope.TratarErro
                                    );
                                }

                            });
                        }
                    });
                }
                //Atualização de Localizações Físicas
                else {
                    if (item != undefined && item.nomeimagem == undefined) {
                        item.nomeimagem = "";

                        genericService.postRequisicao('LocalizacoesFisicasBO', 'EditarLocalizacaoFisica', { campos: item, where: item.codigo }).then(
                            function (result) {
                                $scope.item = {};
                                $scope.desabilitaCampos = false;
                                $scope.editMode = false;
                                $scope.searchMode = false;
                                $scope.form.submitted = false;
                                $scope.isPesquisa = false;
                                $scope.exibeVoltar = false;
                                $('#tabs-localizacao a:first').tab('show');
                                Utils.moveScrollTop();
                                $scope.tableLocalizacoes.page(1);
                                $scope.tableLocalizacoes.reload();
                                var mensagem = $translate.instant('14.MENSAGEM_RegistroSalvoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                            },
                            $rootScope.TratarErro
                        );
                    }
                    else {
                        //Verifica se a imagem já foi upada. Se sim, não upa novamente.
                        if (item.nomeimagem.indexOf("/upload/") > -1 || item.nomeimagem.indexOf("blob.core.windows.net") > -1) {
                            genericService.postRequisicao('LocalizacoesFisicasBO', 'EditarLocalizacaoFisica', { campos: item, where: item.codigo }).then(
                                function (result) {
                                    $scope.item = {};
                                    $scope.desabilitaCampos = false;
                                    $scope.editMode = false;
                                    $scope.searchMode = false;
                                    $scope.form.submitted = false;
                                    $scope.isPesquisa = false;
                                    $scope.exibeVoltar = false;
                                    $('#tabs-localizacao a:first').tab('show');
                                    Utils.moveScrollTop();
                                    $scope.tableLocalizacoes.page(1);
                                    $scope.tableLocalizacoes.reload();
                                    var mensagem = $translate.instant('14.MENSAGEM_RegistroAlteradoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                                },
                                $rootScope.TratarErro
                            );
                        }
                        //Se não, upa a imagem novamente.
                        else {
                            Utils.upload('imagemLocalizacao', 'blobAzure', 'TipoProdutoBO', function (result) {
                                if (result == undefined) {
                                    return false;
                                }
                                else {
                                    if(result.length > 0) {
                                        item.nomeimagem = result[0];
                                    }

                                    genericService.postRequisicao('LocalizacoesFisicasBO', 'EditarLocalizacaoFisica', { campos: item, where: item.codigo }).then(
                                        function (result) {
                                            $scope.item = {};
                                            $scope.desabilitaCampos = false;
                                            $scope.editMode = false;
                                            $scope.searchMode = false;
                                            $scope.form.submitted = false;
                                            $scope.isPesquisa = false;
                                            $scope.exibeVoltar = false;
                                            $('#tabs-localizacao a:first').tab('show');
                                            Utils.moveScrollTop();
                                            $scope.tableLocalizacoes.page(1);
                                            $scope.tableLocalizacoes.reload();
                                            var mensagem = $translate.instant('14.MENSAGEM_RegistroAlteradoSucesso');
                                            $scope.alert(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                                        },
                                        $rootScope.TratarErro
                                    );
                                }
                            });
                        }
                    }

                }

            }
        }

        //Google Maps API
        $scope.map = {
            center: {
                latitude: -22.16980650917504,
                longitude: -22.16980650917504
            },
            zoom: 2,
            dragging: false,
            bounds: {},
            markers: [],
            idkey: 'place_id',
            control: {},
            events: {}
        };

        $scope.options = { scrollwheel: false };
        $scope.searchbox = {
            template: 'searchbox.tpl.html',
            position: 'top-left',
            events: {
                places_changed: function (searchBox) {
                    places = searchBox.getPlaces()
                    if (places.length == 0) {
                        $scope.map.markers = {};
                        return;
                    }

                    var place = places[0];

                    $scope.map.center.latitude = place.geometry.location.lat();
                    $scope.map.center.longitude = place.geometry.location.lng();
                    $scope.map.zoom = 16;

                    $scope.item.latitude = place.geometry.location.lat();
                    $scope.item.longitude = place.geometry.location.lng();

                    var newMarkers = [];

                    var marker = {
                        id: 0,
                        place_id: 0,
                        name: '',
                        latitude: $scope.item.latitude,
                        longitude: $scope.item.longitude,
                        options: {
                            visible: false
                        },
                        templateurl: 'window.tpl.html',
                        templateparameter: {}
                    };

                    newMarkers.push(marker);
                    $scope.map.markers = newMarkers
                }
            }
        }

        $scope.limparMarkers = function () {
            var mensagem = $translate.instant('14.MENSAGEM_MapaSeraRemovido');
            $scope.confirm(typeof mensagem === 'object' || mensagem == '14.MENSAGEM_MapaSeraRemovido' ? 'O mapa será removido da localização física no CNP. Deseja realmente limpar a localização do mapa?' : mensagem, function () {
                $scope.map.markers = {};
                $scope.map.center.latitude = -22.16980650917504;
                $scope.map.center.longitude = -22.16980650917504;
                $scope.item.latitude = "";
                $scope.item.longitude = "";
                $scope.map.zoom = 2;
            });
        }

        $scope.displayed = false;
        $scope.showMap = function () {
            $scope.displayed = true;
            IsReady.promise().then(function (maps) {
                var center = maps[0].map.getCenter();
                google.maps.event.trigger(maps[0].map, "resize");
                maps[0].map.setCenter(center);
            });
        }

    } ]);
