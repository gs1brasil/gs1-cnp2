app.controller('LotesCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $q, $translate) {
        //inicializando abas do form
        $('#tabs-etiqueta a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        var mensagem = $translate.instant('47.LBL_TitleGestaoLotes');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '47.LBL_TitleGestaoLotes' ? 'Gestão de Lotes' : mensagem;
        $scope.isPesquisa = false;
        $scope.parametroURL = $rootScope.retornaValorParametroByFiltro('midia.numax.url.lotes');
        $scope.item = {};
        $scope.listURL = [];
        $scope.insertURLMode = true;

        $scope.moveScrollTop = function () {
            $timeout(function () {
                $(".page-container").scrollTop(0);
                $(window).scrollTop(0);
            }, 500);
        }

        $scope.tableLotes = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGridCertificacaoProduto($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        $scope.tableURL = new ngTableParams({
            page: 1,
            count: 5,
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if ($scope.item.globaltradeitemnumber != undefined && $scope.item.globaltradeitemnumber != '') {
                    var dados = carregaGridURL($scope.item, function (data) {
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                        $scope.listURL = data;
                    });
                }
            }
        });

        function carregaGridCertificacaoProduto(item, isPesquisa, callback) {
            var dados = null;

            $rootScope.verificaAssociadoSelecionado(function (data) {
                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
                    if (isPesquisa) {
                        dados = angular.copy(item);


                        if ((dados.de_dataprocessamento != undefined && dados.de_dataprocessamento != null && dados.de_dataprocessamento != "") && $scope.validaHora('de_dataprocessamento')) {
                            dados.de_dataprocessamento = dados.de_dataprocessamento + " " + dados.horaprocessamento;
                        }

                        if ((dados.para_dataprocessamento != undefined && dados.para_dataprocessamento != null && dados.para_dataprocessamento != "") && $scope.validaHora('para_dataprocessamento')) {
                            dados.para_dataprocessamento = dados.para_dataprocessamento + " " + dados.horaprocessamento;
                        } 


                        genericService.postRequisicao('LotesBO', 'PesquisarLotes', { campos: dados }).then(
                            function (result) {

                                $scope.form.submitted = false;
                                $scope.editMode = false;

                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                         );
                    }
                    else if (!isPesquisa) {

                        genericService.postRequisicao('LotesBO', 'BuscarLotes', { campos: dados }).then(
                            function (result) {
                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                         );
                    }
                    $rootScope.flagAssociadoSelecionado = true;
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }

            });
        }

        //Busca URL
        function carregaGridURL(item, callback) {
            var dados = angular.copy(item);

            genericService.postRequisicao('LotesBO', 'BuscarURLsLote', { campos: dados }).then(
                function (result) {
                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        $scope.onFormMode = function () {
            $scope.item = {};
            $scope.itemUrl = {};
            $scope.moveScrollTop();
            var mensagem = $translate.instant('47.LBL_TitleCadastrarLotes');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '47.LBL_TitleCadastrarLotes' ? 'Cadastrar Lotes' : mensagem;
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isCollapsedMidias = true;
            $scope.insertURLMode = true;
            $scope.initMode = true;
            $scope.exibeVoltar = true;
            $scope.isPesquisa = false;
            $scope.item.statuslote = 1;
            $scope.item.codigobizstep = 5;
            $scope.listURL = [];
            $scope.isRequiredURL = false;
            angular.element("#nenhumProdutoSelecionado").removeClass("has-error-label");

            Utils.bloquearCamposVisualizacao("VisualizarLotes");
            Utils.resetaData("processamento");
            Utils.resetaData("vencimento");

            var disposition = $scope.onSelectProcessamento($scope.item.codigobizstep);
            disposition.then(function () {
                $scope.item.codigodispositions = 6;
            }, $rootScope.TratarErro);

        }

        function reloadTable(callback) {
            $scope.tableLotes.page(1);
            $scope.tableLotes.reload();
            callback();
        }

        //Clique no botão fechar
        $scope.onCancel = function (item) {
            $scope.item = {};
            $scope.itemUrl = {};
            reloadTable(function () {
                blockUI.start();
                $scope.moveScrollTop();
                blockUI.stop();
            });
            $scope.tableLotes.page(1);
            $scope.tableLotes.reload();
            angular.element("#nenhumProdutoSelecionado").removeClass("has-error-label");
            var mensagem = $translate.instant('47.LBL_TitleGestaoLotes');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '47.LBL_TitleGestaoLotes' ? 'Gestão de Lotes' : mensagem;
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.gerar = false;
            $scope.exibeVoltar = false;
        }

        //Clique no botão pesquisar
        $scope.onSearch = function (item) {

            var mensagem = $translate.instant('47.LBL_TitleGestaoLotes');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '47.LBL_TitleGestaoLotes' ? 'Gestão de Lotes' : mensagem;
            $scope.item = angular.copy(item);
            $scope.exibeVoltar = false;
            $scope.tableLotes.page(1);
            $scope.tableLotes.reload();
            $scope.moveScrollTop();
        }

        $scope.onSearchMode = function (item) {
            $scope.item = {};
            var mensagem = $translate.instant('47.LBL_PesquisarLotes');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '47.LBL_PesquisarLotes' ? 'Pesquisar Lotes' : mensagem;

            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.initMode = true;
            $scope.exibeVoltar = true;
            $scope.moveScrollTop();
        }

        //Clique no botão limpar
        $scope.onClean = function (item) {
            $scope.item = {};
        }

        $scope.onPublish = function (item) {
            if (!$scope.form.$valid || item.globaltradeitemnumber == undefined || item.globaltradeitemnumber == null) {

                var mensagem = $translate.instant('47.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);

                $scope.form.submitted = true;
            }
            else {
                genericService.postRequisicao('LotesBO', 'VerificaPermissaoLotes', { campos: { "permissao": "PublicarLotes"} }).then(
                    function (result) {
                        var mensagem = $translate.instant('47.MENSAGEM_PublicarLote');
                        $rootScope.confirm(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_PublicarLote' ? 'Deseja publicar o lote? Não será possível alterar um lote depois de publicado.' : mensagem, function () {

                            $scope.itemForm = angular.copy(item);
                            //$scope.itemForm.statuslote = 2;

                            $scope.onSave($scope.itemForm, "PublicarLotes");
                        }, function () { }, 'Confirmação');
                    },
                    $rootScope.TratarErro
                );
            }
        }

        //Salva na base os item gerado
        $scope.onSave = function (item, permissao) {

            //Converte datas
            var dataProcessamento = Utils.dataMesDiaAno(item.dataprocessamento);
            var dataVencimento = Utils.dataMesDiaAno(item.datavencimento);

            if ((dataProcessamento != undefined && dataProcessamento != null && dataProcessamento != "") && $scope.validaHora('horaprocessamento')) {
                dataProcessamento = dataProcessamento + " " + item.horaprocessamento;
                var dataProcessamentoCompare = Utils.dataAnoMesDia(item.dataprocessamento) + " " + item.horaprocessamento;
            } else {
                var dataProcessamentoCompare = Utils.dataAnoMesDia(item.dataprocessamento)
            }

            if ((dataVencimento != undefined && dataVencimento != null && dataVencimento != "") && $scope.validaHora('horavencimento')) {
                dataVencimento = dataVencimento + " " + item.horavencimento;
                var dataVencimentoCompare = Utils.dataAnoMesDia(item.datavencimento) + " " + item.horavencimento;
            } else {
                var dataVencimentoCompare = Utils.dataAnoMesDia(item.datavencimento);
            }


            if (item.globaltradeitemnumber == undefined || item.globaltradeitemnumber == null) {

                var mensagem = $translate.instant('47.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);

                angular.element("#nenhumProdutoSelecionado").addClass("has-error-label");
            }
            else {
                angular.element("#nenhumProdutoSelecionado").removeClass("has-error-label");

                var bizstep = $.grep($scope.bizsteps, function (obj) {
                    return obj.codigo == item.codigobizstep;
                });

                var disposition = $.grep($scope.dispositions, function (obj) {
                    return obj.codigo == item.codigodispositions;
                });

                //Lista de URLs
                if ($scope.listURL != undefined && $scope.listURL.length > 0) {
                    item.urls = $scope.listURL;
                }

                //Inserção de Lote
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {

                    if (item.dataprocessamento != undefined && item.dataprocessamento != '' && !Utils.isValidDate(Utils.dataAnoMesDia(item.dataprocessamento))) {
                        var mensagem = $translate.instant('47.MENSAGEM_ProcessamentoInvalido');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_ProcessamentoInvalido' ? 'A Data de Processamento informada está inválida.' : mensagem);
                        return false;
                    }
                    else if (item.datavencimento != undefined && item.datavencimento != '' && !Utils.isValidDate(Utils.dataAnoMesDia(item.datavencimento))) {
                        var mensagem = $translate.instant('47.MENSAGEM_VencimentoInvalida');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_VencimentoInvalida' ? 'A Data de Vencimento informada está inválida.' : mensagem);
                        return false;
                    }
                    else if (!Utils.validaDataVencimentoComAtual(item.datavencimento)) { //!Utils.validaDataProcessamentoComAtual(item.dataprocessamento) || 
                        return false;
                    }
                    else if ((item.dataprocessamento != undefined && item.dataprocessamento != null && item.dataprocessamento != '') &&
                                (item.datavencimento != undefined && item.datavencimento != null && item.datavencimento != '') && (dataProcessamentoCompare > dataVencimentoCompare)) {

                        var mensagem = $translate.instant('47.MENSAGEM_DataProcessamentoMaiorVencimento');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_DataProcessamentoMaiorVencimento' ? 'A data de processamento não pode ser maior que a data de vencimento.' : mensagem);

                        return false;
                    }
                    else {
                        var parametro = { codigostatuslote: item.statuslote, dataprocessamento: dataProcessamento, datavencimento: dataVencimento, codigoproduto: item.codigoproduto,
                            informacoesadicionais: item.informacoesadicionais, bizstep: bizstep[0].nome, disposition: disposition[0].nome, codigobizstep: item.codigobizstep,
                            codigodispositions: item.codigodispositions, urls: item.urls, nrlote: item.nrlote, permissao: permissao ? permissao : "CadastrarLotes", quantidadeitens: item.quantidadeitens
                        };

                        genericService.postRequisicao('LotesBO', 'InsereLotes', { campos: parametro }).then(
                            function (result) {

                                if (result != undefined && result != null && result.data != null) {
                                    $scope.item = {};
                                    $scope.editMode = false;
                                    $scope.searchMode = false;
                                    $scope.form.submitted = false;
                                    $scope.isPesquisa = false;
                                    $scope.exibeVoltar = false;
                                    $scope.tableLotes.page(1);
                                    $scope.tableLotes.reload();
                                    $scope.initMode = false;

                                    var mensagem = $translate.instant('47.LBL_TitleGestaoLotes');
                                    $scope.titulo = typeof mensagem === 'object' || mensagem == '47.LBL_TitleGestaoLotes' ? 'Gestão de Lotes' : mensagem;

                                    if ($scope.form.midia != undefined) $scope.form.midia.submitted = false;

                                    var mensagem = $translate.instant('47.MENSAGEM_RegistroSalvoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);

                                    $scope.moveScrollTop();
                                }
                            },
                            $rootScope.TratarErro
                        );
                    }

                }
                //Atualização de Lote
                else {
                    if (item.dataprocessamento != undefined && item.dataprocessamento != '' && !Utils.isValidDate(Utils.dataAnoMesDia(item.dataprocessamento))) {
                        var mensagem = $translate.instant('47.MENSAGEM_ProcessamentoInvalido');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_ProcessamentoInvalido' ? 'A Data de Processamento informada está inválida.' : mensagem);
                        return false;
                    }
                    else if (item.datavencimento != undefined && item.datavencimento != '' && !Utils.isValidDate(Utils.dataAnoMesDia(item.datavencimento))) {
                        var mensagem = $translate.instant('47.MENSAGEM_VencimentoInvalida');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_VencimentoInvalida' ? 'A Data de Vencimento informada está inválida.' : mensagem);
                        return false;
                    }
                    else if (!Utils.validaDataVencimentoComAtual(item.datavencimento)) { //!Utils.validaDataProcessamentoComAtual(item.dataprocessamento) || 
                        return false;
                    }
                    else if ((item.dataprocessamento != undefined && item.dataprocessamento != null && item.dataprocessamento != '') &&
                                (item.datavencimento != undefined && item.datavencimento != null && item.datavencimento != '') && (dataProcessamentoCompare > dataVencimentoCompare)) {

                        var mensagem = $translate.instant('47.MENSAGEM_DataProcessamentoMaiorVencimento');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_DataProcessamentoMaiorVencimento' ? 'A data de processamento não pode ser maior que a data de vencimento.' : mensagem);

                        return false;
                    }
                    else {
                        var parametro = { codigo: item.codigo, codigostatuslote: item.statuslote, dataprocessamento: dataProcessamento, datavencimento: dataVencimento, codigoproduto: item.codigoproduto,
                            informacoesadicionais: item.informacoesadicionais, bizstep: bizstep[0].nome, disposition: disposition[0].nome, codigobizstep: item.codigobizstep,
                            codigodispositions: item.codigodispositions, urls: item.urls, nrlote: item.nrlote, permissao: permissao ? permissao : "EditarLotes", quantidadeitens: item.quantidadeitens
                        };

                        genericService.postRequisicao('LotesBO', 'EditarLotes', { campos: parametro, where: item.codigo }).then(
                            function (result) {
                                $scope.item = {};
                                $scope.editMode = false;
                                $scope.searchMode = false;
                                $scope.form.submitted = false;
                                $scope.isPesquisa = false;
                                $scope.exibeVoltar = false;
                                $scope.tableLotes.page(1);
                                $scope.tableLotes.reload();
                                $scope.initMode = false;

                                var mensagem = $translate.instant('47.LBL_TitleGestaoLotes');
                                $scope.titulo = typeof mensagem === 'object' || mensagem == '47.LBL_TitleGestaoLotes' ? 'Gestão de Lotes' : mensagem;
                                if ($scope.form.midia != undefined) $scope.form.midia.submitted = false;

                                var mensagem = $translate.instant('47.MENSAGEM_RegistroAlteradoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                                $scope.moveScrollTop();
                            },
                            $rootScope.TratarErro
                        );
                    }
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////                        GERENCIAMENTO DE URL                   ///////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Salvar URL na tabela provisória
        $scope.onSaveURL = function (items) {

            $scope.isRequiredURL = true;
            var URL_REGEX = "((http\://|https\://|ftp\://)|(www.))+(([a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9%:/-_\?\.'~]*)?";
            var patt = new RegExp(URL_REGEX);

            if (items != undefined && items.url != undefined) {

                if ($scope.listURL.length > 0) {
                    var canSave = $scope.canSave(items);

                    if (items.url != undefined && items.url != '' && patt.test(items.url) == false) {

                        var mensagem = $translate.instant('47.MENSAGEM_URLFormatoInvalido');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_URLFormatoInvalido' ? 'A URL informada não possui um formato válido. Por favor, verifique a URL informada.' : mensagem);
                    }
                    else {
                        if (canSave == 1) {
                            var maior = verificaMaiorCodigo();
                            items.codigo = maior + 1;
                            items.status = "novo";
                            $scope.listURL.push(items);

                            var mensagem = $translate.instant('47.MENSAGEM_URLInseridaSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_URLInseridaSucesso' ? 'URL inserida com sucesso.' : mensagem);

                            $scope.itemUrl = {};
                            $scope.form.midia.nome.submitted = false;
                            $scope.form.midia.url.submitted = false;
                        }
                        else if (canSave == 2) {
                            var mensagem = $translate.instant('47.MENSAGEM_QuantidadeLinksSuperior');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_QuantidadeLinksSuperior' ? 'Quantidade de links superior ao permitido.' : mensagem);
                        }
                        else {
                            var mensagem = $translate.instant('47.MENSAGEM_URLJaExiste');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_URLJaExiste' ? 'A URL informada já existe.' : mensagem);
                        }
                    }
                }
                else {
                    if (items.url != undefined && items.url != '' && patt.test(items.url) == false) {
                        var mensagem = $translate.instant('47.MENSAGEM_URLFormatoInvalido');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_URLFormatoInvalido' ? 'A URL informada não possui um formato válido. Por favor, verifique a URL informada.' : mensagem);
                    }
                    else {
                        var maior = verificaMaiorCodigo();
                        items.codigo = maior + 1;
                        items.status = "novo";
                        $scope.listURL.push(items);
                        $scope.itemUrl = {};
                        var mensagem = $translate.instant('47.MENSAGEM_URLInseridaSucesso');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_URLInseridaSucesso' ? 'URL inserida com sucesso.' : mensagem);
                    }
                }

            }
            else {
                $scope.form.midia.nome.submitted = true;
                $scope.form.midia.url.submitted = true;
                var mensagem = $translate.instant('47.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            }
        }

        //Percorre a tabela provisória e retorna o maior CODIGO
        function verificaMaiorCodigo() {
            var maior = 0;

            //Pegar o maior CODIGO da tabela
            angular.forEach($scope.listURL, function (val, key) {

                if (val.codigo > maior) {
                    maior = val.codigo;
                }
            });

            return maior;
        }

        //Verifica parâmetros e tbm se não existe outro com os mesmos dados
        $scope.canSave = function (items) {
            var hasAnotherOne = false;
            var contador = 0;

            angular.forEach($scope.listURL, function (val, key) {

                if (val.url == items.url && val.nome == items.nome && val.codigo != items.codigo) {
                    hasAnotherOne = true;
                }
            });

            if (!hasAnotherOne && $scope.insertURLMode) {
                angular.forEach($scope.listURL, function (val, key) {
                    contador += 1;
                });

                if (contador >= $scope.parametroURL) {
                    return 2;
                }

                return 1;
            }
            else {
                return 1;
            }
        }

        //Modo de Alteração de URL
        $scope.onAlterURL = function (item) {
            $scope.itemUrl = angular.copy(item);
            $scope.insertURLMode = false;
        }

        //Remover URL da tabela provisória
        $scope.removeURL = function (item) {

            var mensagem = $translate.instant('47.MENSAGEM_DesejaRemover');
            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {

                for (var i in $scope.listURL) {
                    if ($scope.listURL[i].codigo == item.codigo) {
                        if ($scope.listURL[i].status == "novo" || $scope.listURL[i].status == "novo-e-editado")
                            $scope.listURL[i].status = "excluido2";
                        else
                            $scope.listURL[i].status = "excluido";
                    }
                }

                $scope.insertURLMode = true;
                $scope.itemUrl = {};

            }, function () { }, 'Confirmação');
        }

        //Alterar URL da tabela provisória
        $scope.atualizarURL = function (item) {

            if ($scope.form.midia.nome.$valid && $scope.form.midia.url.$valid) {

                var URL_REGEX = "((http\://|https\://|ftp\://)|(www.))+(([a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9%:/-_\?\.'~]*)?";
                var patt = new RegExp(URL_REGEX);

                var canSave = $scope.canSave(item);

                if (item.url != undefined && item.url != '' && patt.test(item.url) == false) {
                    var mensagem = $translate.instant('47.MENSAGEM_URLFormatoInvalido');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_URLFormatoInvalido' ? 'A URL informada não possui um formato válido. Por favor, verifique a URL informada.' : mensagem);
                }
                else {
                    if (canSave == 1) {

                        for (var i = 0; i < $scope.listURL.length; i++) {
                            if (item.codigo == $scope.listURL[i].codigo) {
                                if ($scope.listURL[i].status == "novo")
                                    item.status = "novo-e-editado";
                                else
                                    item.status = "editado";
                                $scope.listURL[i] = item;
                            }
                        }

                        $scope.insertURLMode = true;
                        var mensagem = $translate.instant('47.MENSAGEM_URLAlteradaSucesso');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_URLAlteradaSucesso' ? 'URL alterada com sucesso.' : mensagem);
                        $scope.itemUrl = {};
                    }
                    else if (canSave == 2) {
                        var mensagem = $translate.instant('47.MENSAGEM_QuantidadeLinksSuperior');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_QuantidadeLinksSuperior' ? 'Quantidade de links superior ao permitido.' : mensagem);
                    }
                    else {
                        var mensagem = $translate.instant('47.MENSAGEM_URLJaExiste');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_URLJaExiste' ? 'A URL informada já existe.' : mensagem);
                    }
                }
            }
            else {
                $scope.form.midia.nome.submitted = true;
                $scope.form.midia.url.submitted = true;
                var mensagem = $translate.instant('47.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            }
        }

        $scope.cancelarURL = function () {
            $scope.itemUrl = {};
            $scope.form.midia.nome.submitted = false;
            $scope.form.midia.url.submitted = false;
            $scope.insertURLMode = true;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////                        FIM GERENCIAMENTO DE URL                   /////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //Método para abertura do modal de Produto
        $scope.onProduto = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalProduto',
                size: 'lg',
                backdrop: 'static',
                controller: ModalProdutoCtrl,
                resolve: {
                    item: function () {
                        var produto = angular.copy(item);
                        return produto;
                    },
                    licencas: function () {
                        return $scope.licencas;
                    },
                    statusgtin: function () {
                        return $scope.statusgtin;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $scope.item.gln = data.gln;
                $scope.item.globaltradeitemnumber = data.globaltradeitemnumber;
                $scope.item.codigoproduto = data.codigoproduto;
                $scope.item.productdescription = data.productdescription;
                $scope.item.nometipogtin = data.nometipogtin;
                $scope.item.nrprefixo = data.nrprefixo;
                $scope.item.coditem = data.coditem;
                $scope.item.variantelogistica = data.variantelogistica;

            }, function () { });
        }

        $scope.onEdit = function (item) {
            var mensagem = $translate.instant('47.LBL_TitleAlterarLotes');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '47.LBL_TitleAlterarLotes' ? 'Alterar Lotes' : mensagem;
            $scope.initMode = false;
            $scope.item = angular.copy(item);
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isPesquisa = false;
            $scope.gerar = true;
            $scope.exibeVoltar = true;
            $scope.item.codigobizstep = 5;

            if ($scope.item.dataprocessamento != null && $scope.item.dataprocessamento != undefined) {
                $scope.item.dataprocessamento = $scope.item.dataprocessamento.substring(0, 10);
            }

            if ($scope.item.datavencimento != null && $scope.item.datavencimento != undefined) {
                $scope.item.datavencimento = $scope.item.datavencimento.substring(0, 10);
            }

            angular.element("#nenhumProdutoSelecionado").removeClass("has-error-label");
            var disposition = $scope.onSelectProcessamento(item.codigobizstep);
            disposition.then(function () { }, $rootScope.TratarErro);
            $scope.tableURL.reload();
            Utils.bloquearCamposVisualizacao("VisualizarLotes");
            $scope.moveScrollTop();
        }

        //Chamado ao selecionar um segmento
        $scope.onSelectProcessamento = function (processamento) {
            return $q(function (resolve, reject) {
                if (processamento != undefined && processamento != null && processamento != '') {
                    genericService.postRequisicao('DispositionsBO', 'buscarDisposition', { campos: { "processamento": processamento} }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.dispositions = result.data;

                                $('#formcontainer').focus(); //Usado para corrigir bug no ie na exibição do layout, no momento de esconder a grid e aprensetar o form

                                resolve(result.data);
                            }
                        },
                        $rootScope.TratarErro
                    )
                }
            });
        }
       

        $scope.validaHora = function (campo) {
            if (this.item[campo] != null && this.item[campo] != undefined) {
                if (!/^([0-2]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/.test(this.item[campo])) {
                    this.item[campo] = "";
                    return false;
                }
            } else {
                this.item[campo] = "";
                return false;
            }
            return true;
        }

        //Carregamento inicial
        $scope.onLoad = function () {

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                    //Licenças
                    genericService.postRequisicao('LicencaBO', 'buscarTipoGtinAssociado', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.licencas = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Produto
                    genericService.postRequisicao('ProdutoBO', 'BuscarProdutos', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.produtos = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Status do GTIN
                    genericService.postRequisicao('StatusGTINBO', 'BuscarStatusGTINAtivosFiltrado', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.statusgtin = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Status do Lote
                    genericService.postRequisicao('StatusLoteBO', 'BuscarStatusLoteAtivo', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.statuslote = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //BizStep
                    genericService.postRequisicao('BizStepBO', 'BuscarBizStepAtivo', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.bizsteps = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    $scope.moveScrollTop();
                }
                else {
                    $rootScope.flagAssociadoSelecionado = false;
                    angular.element("#typea").focus();
                }
            });
        }

        $scope.onLoad();

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };
    } ]);


//Controller para métodos do modal
var ModalProdutoCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, genericService, item, Utils, licencas, statusgtin, $translate) {

    $scope.initModal = true;

    $scope.licencas = licencas;
    $scope.statusgtin = statusgtin;
    $scope.itemForm = {
            gtin: ''
        , descricao: ''
        , codigotipogtin: ''
        , codigostatusgtin: ''
    };

    $scope.tableProdutos = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGridProdutos(function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                $scope.produtos = data;
            });
        }
    });

    function carregaGridProdutos(callback) {
        var dados = angular.copy($scope.itemForm);

        genericService.postRequisicao('LotesBO', 'PesquisaProdutos', { campos: dados }).then(
            function (result) {
                if (callback) callback(result.data);
            },
            $rootScope.TratarErro
        );
    }

    function validarPesquisa() {
        if (!$scope.itemForm.gtin && !$scope.itemForm.descricao && !$scope.itemForm.codigotipogtin && !$scope.itemForm.codigostatusgtin) {

            var mensagem = $translate.instant('47.MENSAGEM_PreenchaAoMenosUmFiltro');
            $scope.alert(typeof mensagem === 'object' || mensagem == '47.MENSAGEM_PreenchaAoMenosUmFiltro' ? 'Preencha ao menos um filtro.' : mensagem);
            return false;
        }

        return true;
    }

    //Chamado ao selecionar a opção "Pesquisar"
    $scope.onFilter = function (item) {

        if (validarPesquisa()) {
            $scope.itemForm = angular.copy(item);
            $scope.isPesquisa = true;
            $scope.tableProdutos.page(1);
            $scope.tableProdutos.reload();
        }
    }

    //Chamado ao selecionar um PRODUTO no grid
    $scope.onSelectedProduto = function (item) {
        $modalInstance.close(item);
    }

    //Cancelar 
    $scope.cancel = function () {
        $(window).scrollTop(0);
        $(".page-container").scrollTop(0);
        $modalInstance.dismiss('cancel');
    }

};