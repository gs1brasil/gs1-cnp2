﻿app.controller('MensagemCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$q', '$translate', '$routeParams',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $q, $translate, $routeParams) {

        var titulo = $translate.instant('59.LBL_Mensagens');
        $scope.titulo = typeof titulo === 'object' || titulo == '59.LBL_Mensagens' ? 'Mensagens' : titulo;

        $scope.onLoad = function () {
//            $rootScope.verificaAssociadoSelecionado(function (data) {
//                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {
//                    $rootScope.flagAssociadoSelecionado = true;
//                } else {
//                    $rootScope.flagAssociadoSelecionado = false;
//                    angular.element("#typea").focus();
//                }
//            });
        }

        $scope.tableMensagens = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                datasincronizacao: 'desc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                blockUI.start();
                var ordenacao = params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '') + ' ';
                ordenacao = ordenacao + params.sorting()[params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '')].toString();

                var dados = $scope.ConsultarMensagens($scope.itemForm, params.page(), $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'), ordenacao, function (result) {
                    if (result.data && result.data.length > 0) {
                        params.total(result.data[0].totalregistros);
                    }
                    if (result.data.length == 0) {
                        params.total(0);
                    }

                    $defer.resolve(result.data);

                    blockUI.stop();
                });
            }
        });

        $scope.ConsultarMensagens = function (params, paginaAtual, registroPorPagina, ordenacao, callback) {

            genericService.postRequisicao('DashboardBO', 'ConsultarMensagens', { paginaAtual: paginaAtual, registroPorPagina: registroPorPagina, ordenacao: ordenacao }).then(function (result) {
                $rootScope.ConsultarMensagens();
                if (result != null && result.data != null) {
                    if (callback != undefined && callback != null) {
                        callback(result);
                    }
                }

            }, $rootScope.TratarErro);
        }

        $scope.ConsultarMensagem = function (codigomensagem) {
            genericService.postRequisicao('MensagemBO', 'ConsultarMensagem', { where: { codigomensagem: codigomensagem} }).then(function (result) {
                if (result != null && result.data != null && result.data.length > 0) {
                    $scope.onEdit(result.data[0]);
                } else {
                    var mensagem = $translate.instant('59.MENSAGEM_MensagemNaoEncontrada');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '59.MENSAGEM_MensagemNaoEncontrada' ? 'Mensagem não encontrada.' : mensagem);
                }
            }, $rootScope.TratarErro);
        }


        $scope.onEdit = function (item) {

            $scope.itemForm = angular.copy(item);
            $scope.item = {};
            //$scope.itemForm = {};
            $scope.formMode = true;
            $scope.searchMode = false;
            $scope.exibeVoltar = true;
            $scope.isPesquisa = false;
            $scope.initMode = true;

            if (item.dataleitura == null || item.dataleitura == '') {
                genericService.postRequisicao('MensagemBO', 'marcarMensagemLida', { where: { codigo: item.codigo} }).then(
                    function (result) {
                        //$scope.tableMensagens.page(1);
                        $scope.tableMensagens.reload();
                        $rootScope.ConsultarMensagens();
                    },
                    $rootScope.TratarErro
                );
            }


        }


        $scope.onCancel = function (item) {
            var titulo = $translate.instant('59.LBL_Mensagens');
            $scope.titulo = typeof titulo === 'object' || titulo == '59.LBL_Mensagens' ? 'Mensagens' : titulo;

            $scope.item = {};
            $scope.itemForm = {};
            //$scope.associadosMarcados = [];
            //$scope.perfisUsuario = {};
            $scope.formMode = false;
            $scope.searchMode = false;
            //$scope.form.submitted = false;
            $scope.exibeVoltar = false;
            //$scope.statusUsuario = [];
            $scope.isPesquisa = false;
            $scope.initMode = false;
            Utils.moveScrollTop();
            $scope.tableMensagens.page(1);
            $scope.tableMensagens.reload();
        };

        if ($routeParams != null && $routeParams.codigo != null) {
            $scope.ConsultarMensagem($routeParams.codigo);
        }



        $scope.onLoad();

    } ]);