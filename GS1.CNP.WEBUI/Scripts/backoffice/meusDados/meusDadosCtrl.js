app.controller('MeusDados', ['$scope', '$http', '$rootScope', 'ngTableParams', '$modal', '$log', '$filter', 'genericService', '$window', '$location', '$translate', 
function ($scope, $http, $rootScope, ngTableParams, $modal, $log, $filter, genericService, $window, $location, $translate) {
    $scope.usuario = {};

    var titulo = $translate.instant('6.LBL_MeusDados');
    $scope.titulo = typeof titulo === 'object' || titulo == '6.LBL_MeusDados' ? 'Meus Dados' : titulo;

    $scope.onCancel = function () {
        $location.path('/backoffice/home');
    };

    $scope.onSave = function () {
        if (!$scope.form.$valid) {
            var mensagem = $translate.instant('6.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '6.MENSAGEM_CamposObrigatorios' ? 'Campos obrigat�rios n�o preenchidos ou inv�lidos.' : mensagem);
            $scope.form.submitted = true;
        }
        else {

            genericService.postRequisicao('MeusDadosBO', 'AtualizarMeusDados', { campos: $scope.usuario }).then(
                function (result) {
                    $scope.form.submitted = false;
                    var mensagem = $translate.instant('6.MENSAGEM_RegistroSalvoSucesso');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '6.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                },
                $rootScope.TratarErro
            );
        }
    }

    $scope.tableEmpresasAssociadas = new ngTableParams({
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGrid($scope.item, function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData)
                $scope.items = data;
            });
        }
    });

    function carregaGrid(item, callback) {
        var dados = {};
        genericService.postRequisicao('MeusDadosBO', 'BuscaEmpresasAssociadas', { campos: {} }).then(
            function (result) {

                $scope.empresasAssociadas = result.data;
                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
    };

    function buscarMeusDados() {
        genericService.postRequisicao('MeusDadosBO', 'BuscarMeusDados', { campos: {} }).then(
            function (result) {
                $scope.usuario = result.data[0];
            },
            $rootScope.TratarErro
        );
    }

    buscarMeusDados();

} ]);
