app.controller('NotificacoesCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI) {
    $scope.titulo = 'Central de notificações';
    
    $scope.notificacoes = [
        {codigo: 1, assunto: 'Teste 87', mensagem: 'teste 87', remetente: 'teste@teste.com', vigenciaInicio: '14/09/2015', vigenciaFim: '14/09/2016', status: 1},
        {codigo: 2, assunto: 'Teste 88', mensagem: 'teste 88', remetente: 'teste@teste.com', vigenciaInicio: '14/09/2015', vigenciaFim: '14/09/2016', status: 1},
        {codigo: 3, assunto: 'Teste 89', mensagem: 'teste 89', remetente: 'teste@teste.com', vigenciaInicio: '14/09/2015', vigenciaFim: '14/09/2016', status: 1},
        {codigo: 4, assunto: 'Teste 90', mensagem: 'Teste 90', remetente: 'teste@teste.com', vigenciaInicio: '14/09/2015', vigenciaFim: '14/09/2016', status: 1},
        {codigo: 5, assunto: 'Teste 91', mensagem: 'Teste 91', remetente: 'teste@teste.com', vigenciaInicio: '14/09/2015', vigenciaFim: '14/09/2016', status: 2},
        {codigo: 6, assunto: 'Teste 92', mensagem: 'Teste 92', remetente: 'teste@teste.com', vigenciaInicio: '14/09/2015', vigenciaFim: '14/09/2016', status: 2}
    ];
    
    $scope.tableNotificacoes = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });
    
    function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            if (isPesquisa) {
                dados = $scope.notificacoes;
                callback(dados);
            }
            else {
                dados = $scope.notificacoes;
                callback(dados);
            }
        }
    
    $scope.onVisualize = function (item) {
        
        $scope.item = angular.copy(item);
        item.status = 2;
        $scope.visualizeMode = true;
        $scope.searchMode = false;
        $scope.isPesquisa = false;
    };
    
    $scope.onBack = function(){
        $scope.item = {};
        $scope.visualizeMode = false;
        $scope.isPesquisa = false;
        $scope.tableNotificacoes.page(1);
        $scope.tableNotificacoes.reload();    
    }
}]);