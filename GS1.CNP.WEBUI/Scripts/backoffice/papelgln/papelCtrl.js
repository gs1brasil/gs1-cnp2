app.controller('PapelCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate', 
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {

        $scope.isPesquisa = false;

        var mensagem = $translate.instant('4.LBL_CadastroPapelGLN');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '4.LBL_CadastroPapelGLN' ? 'Cadastro de Papel GLN' : mensagem;

        $scope.tablePapel = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                nome: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        //Busca papeis GLN
        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            if (isPesquisa) {
                dados = angular.copy(item);

                genericService.postRequisicao('PapelGlnBO', 'BuscarPapelGLN', { campos: dados }).then(
                    function (result) {

                        $scope.form.submitted = false;
                        $scope.editMode = false;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                 );
            }
            else {
                genericService.postRequisicao('PapelGlnBO', 'BuscarPapeisGLN', { where: dados }).then(
                    function (result) {

                        $scope.papeis = result.data;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                 );
            }
        }

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('PapelGlnBO', 'CadastrarPapelGln', function () {

                var mensagem = $translate.instant('4.LBL_CadastroPapelGLN');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '4.LBL_CadastroPapelGLN' ? 'Cadastro de Papel GLN' : mensagem;
                $scope.item = {};
                $scope.isPesquisa = false;
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.exibeVoltar = true;
                Utils.bloquearCamposVisualizacao("VisualizarPapelGLN");
            });
        };

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('PapelGlnBO', 'AtualizarPapelGln', function () {

                Utils.bloquearCamposVisualizacao("VisualizarPapelGLN");

                var mensagem = $translate.instant('4.LBL_AlterarPapelGLN');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '4.LBL_AlterarPapelGLN' ? 'Alterar Papel GLN' : mensagem;
                $scope.item = angular.copy(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;
            });
        };

        $scope.onCancel = function (item) {

            var mensagem = $translate.instant('4.LBL_CadastroPapelGLN');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '4.LBL_CadastroPapelGLN' ? 'Cadastro de Papel GLN' : mensagem;
            $scope.item = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tablePapel.page(1);
            $scope.tablePapel.reload();
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        $scope.onSearchMode = function (item) {
            var mensagem = $translate.instant('4.LBL_PesquisarPapelGLN');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '4.LBL_PesquisarPapelGLN' ? 'Pesquisar Papel GLN' : mensagem;
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
        };

        //Remove Papel GLN cadastrado
        $scope.onDelete = function (item) {
            $rootScope.verificaPermissao('PapelGlnBO', 'RemoverPapelGLN', function () {

                var titulo = $translate.instant('4.LBL_Confirmacao');
                var mensagem = $translate.instant('4.MENSAGEM_DesejaRemover');

                $rootScope.confirm(typeof mensagem === 'object' || mensagem == '4.MENSAGEM_DesejaRemover' ? 'Pesquisar Papel GLN' : mensagem, function () {
                        genericService.postRequisicao('PapelGlnBO', 'RemoverPapelGLN', { campos: item }).then(
                            function (result) {
                                $scope.form.submitted = false;
                                Utils.moveScrollTop();
                                $scope.tablePapel.reload();
                                $scope.tablePapel.page(1);
                                $scope.item = {};

                                var mensagem = $translate.instant('4.MENSAGEM_DadosRemovidosSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '4.MENSAGEM_DadosRemovidosSucesso' ? 'Dados removidos com sucesso.' : mensagem);
                            },
                            $rootScope.TratarErro
                        );

                        }, function () { }, typeof titulo === 'object' || titulo == '4.LBL_Confirmacao' ? 'Confirmação' : titulo);
            });
        }

        //Pesquisa Papel GLN cadastrado
        $scope.onSearch = function (item) {
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tablePapel.page(1);
            $scope.tablePapel.reload();
        }

        $scope.onSave = function (item) {

            if (!$scope.form.$valid) {

                var mensagem = $translate.instant('4.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '4.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
            }
            else {
                //Inserção de Papel GLN
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {
                    genericService.postRequisicao('PapelGlnBO', 'CadastrarPapelGln', { campos: item }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            Utils.moveScrollTop();
                            $scope.tablePapel.page(1);
                            $scope.tablePapel.reload();

                            var mensagem = $translate.instant('4.MENSAGEM_RegistroSalvoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '4.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                }
                //Atualização de Papel GLN
                else {
                    genericService.postRequisicao('PapelGlnBO', 'AtualizarPapelGln', { campos: item }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            Utils.moveScrollTop();
                            $scope.tablePapel.page(1);
                            $scope.tablePapel.reload();

                            var mensagem = $translate.instant('4.MENSAGEM_RegistroAlteradoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '4.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                }

            }
        }

    } ]);
