app.controller('ParametroSistema', ['$scope', '$http', '$rootScope', 'ngTableParams', '$window', '$modal', '$log', '$filter', 'genericService', '$location', '$translate', 'Utils', 
function ($scope, $http, $rootScope, ngTableParams, $window, $modal, $log, $filter, genericService, $location, $translate, Utils) {

    //inicializando abas do form
    $('#tabs-parametros a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $scope.insertGepirMode = true;
    $scope.listGepir = [];

    var mensagem = $translate.instant('2.LBL_ParametrosSistema');
    $scope.titulo = typeof mensagem === 'object' || mensagem == '2.LBL_ParametrosSistema' ? 'Par�metros do Sistema' : mensagem;

    $scope.onCancel = function () {
        $location.path('/b#/home');
    }

    $scope.buscaValor = function (chave) {
        for (var i in $scope.parametros) {
            if ($scope.parametros[i].chave == chave) {
                return $scope.parametros[i].valor;
                break;
            }
        }
    }

    $scope.itemParam = {};

    $scope.carregaParametros = function () {
        var parametro = null;
        genericService.postRequisicao('ParametroBO', 'BuscarParametros').then(
            function (result) {
                $scope.parametros = result.data;

                $scope.itemParam.senhadiferenteultimas = $scope.buscaValor('autenticacao.senha.diferente.n.ultimas');
                $scope.itemParam.diasavisoexpiracao = $scope.buscaValor('autenticacao.senha.nudias.avisoexpiracao');
                $scope.itemParam.diasvalidadesenha = $scope.buscaValor('autenticacao.senha.nudias.validade');
                $scope.itemParam.tentativassenha = $scope.buscaValor('autenticacao.senha.nutentativas');
                $scope.itemParam.tentativassenhasemrecaptcha = $scope.buscaValor('autenticacao.senha.nutentativasprerecaptcha');

                $scope.itemParam.numerotelefone = $scope.buscaValor('bo.nu.telefone');
                $scope.itemParam.urlchat = $scope.buscaValor('ajuda.urlchat');

                $scope.itemParam.nuurlfoto = $scope.buscaValor('midia.nuurl.foto');
                $scope.itemParam.nuurlyoutube = $scope.buscaValor('midia.nuurl.youtube');
                $scope.itemParam.nuurllinkeddata = $scope.buscaValor('midia.nuurl.linkeddata');
                $scope.itemParam.nuurlproduto = $scope.buscaValor('midia.nuurl.produto');
                $scope.itemParam.nuurlreserva = $scope.buscaValor('midia.nuurl.reserva');
                $scope.itemParam.nufoto = $scope.buscaValor('midia.numax.foto');

                $scope.itemParam.resultadosporpagina = $scope.buscaValor('grid.resultados.por.pagina');

//                $scope.itemParam.emailenvio = $scope.buscaValor('email.envio.relatorio.mensal');

                $scope.itemParam.validadecertificacaocodigobarras = $scope.buscaValor('certificacao.numeses.vencimento.codigobarras');
                $scope.itemParam.validadecertificacaopesosmedidas = $scope.buscaValor('certificacao.numeses.vencimento.pesosmedidas');
                $scope.itemParam.ultimaSincronizacaoCrmRgc = $scope.buscaValor('parametros.integracoes.ultimaSincronizacaoCrmRgc');

                $scope.itemParam.emailenvio = $scope.buscaValor('email.envio.relatorio.mensal');

                $scope.itemParam.perfilpadrao =  parseInt($scope.buscaValor('gepir.perfilpadrao'));
                $scope.itemParam.diarenovacaofranquia = $scope.buscaValor('gepir.diarenovacaofranquia');
                $scope.itemParam.glnconsultapadrao = $scope.buscaValor('gepir.glnconsultapadrao');

                genericService.postRequisicao('GepirBO', 'ConsultarConfigurationProfile').then(
                    function (result) {
                        $scope.configurationprofile = result.data;
                    },
                    $rootScope.TratarErro
                );
                

                Utils.bloquearCamposVisualizacao("VisualizarParametro");
            },
            $rootScope.TratarErro          
        );
        
        genericService.postRequisicao('ParametroBO', 'BuscarQuantidadeSessoesAtivas').then(
            function (result) {
                $scope.itemParam.sessoesativas = result.data;               
            },
            $rootScope.TratarErro
        );

       
    }

    $scope.onSave = function (itemParam) {
        if ($scope.form.$valid) {
            var parametro = itemParam;

            var dados = { campos: parametro };

            genericService.postRequisicao('ParametroBO', 'SalvarParametros', dados).then(
                function (result) {
                    $scope.form.submitted = false;
                    $rootScope.refreshParametros();
                    Utils.moveScrollTop();

                    var mensagem = $translate.instant('2.MENSAGEM_RegistroSalvoSucesso');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '2.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                },
                $rootScope.TratarErro
            );
        }
        else {
            $scope.form.submitted = true;

            var mensagem = $translate.instant('2.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '2.MENSAGEM_CamposObrigatorios' ? 'Campos obrigat�rios n�o preenchidos ou inv�lidos.' : mensagem);
        }
    };

    function isNumber(obj) {
        return !isNaN(parseFloat(obj))
    }

    function isNumber(n) {
        return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
    }

    String.prototype.replaceAll = function (find, replace) {
        var str = this;
        return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
    };

    //Carrega associados cadastrados 
    $scope.BuscarAssociadosAutoComplete = function (nome) {
        if (isNumber(nome)) {
            nome = nome.replaceAll('.', '').replaceAll('-', '');
        }

        return genericService.postRequisicao('LoginBO', 'BuscarAssociadosUsuarioAutoComplete', { where: { nome: nome} }).then(
            function (result) {
                var associadosDoUsuario = [];
                angular.forEach(result.data, function (item) {
                    associadosDoUsuario.push({ codigo: item.codigo, nome: item.nome, cpfcnpj: item.cpfcnpj, cad: item.cad,
                        indicadorcnaerestritivo: item.indicadorcnaerestritivo ? item.indicadorcnaerestritivo : ''
                    });
                });
                return associadosDoUsuario;
            }
        );
    }


    $scope.carregaParametros();

} ]);
