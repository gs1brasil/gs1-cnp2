app.controller('PerfilCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', 'genericService', 'Utils', '$translate',
     function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, perfil, Utils, $translate) {

         //inicializando abas do form
         $('#tabs-perfil a').click(function (e) {
             e.preventDefault();
             $(this).tab('show');
         });

         var mensagem = $translate.instant('1.LBL_CadastroPerfil');
         $scope.titulo = typeof mensagem === 'object' || mensagem == '1.LBL_CadastroPerfil' ? 'Cadastro de Perfil' : mensagem;

         var jstree = null;
         var grid = {};

         //Cria árvore de permissões
         function geraArvore(atributo) {
             var jsonArr = atributo;

             var json_permissoes = { 'plugins': ["wholerow", "checkbox"], 'core': { 'data': jsonArr} };
             
             jstree = $("#arvore-permissoes").on('changed.jstree', function (e, data) {
                 $scope.habilitacampos = data.selected.length > 0;
             }).jstree(json_permissoes);
        }

         //Carrega árvore de permissões
         function carregaArvore(item) {
             if (jstree != undefined && jstree != null) {
                 $("#arvore-permissoes").jstree().destroy();
             }

             if (item == null || item == undefined) {
                 item = {};
                 item.codigo = 0;
             }
             
             var retorno = $.grep($rootScope.usuarioLogado.permissoes, function (obj) {
                return obj === 'VisualizarPerfil';
             });
                   
             if (retorno != undefined && retorno != null && retorno.length > 0) 
             {
                $scope.desabilitarArvore = true;   
             }

             perfil.postRequisicao('PerfilBO', 'CarregarArvorePermissoes', { campos: { codigo: item.codigo, codigotipousuario: item.codigotipousuario, disabled: $scope.desabilitarArvore} }).then(
                function (result) {
                    geraArvore(result.data);
                },
                $rootScope.TratarErro
            );
            
         }

         //Busca perfis cadastrados
         function carregaGrid(itemForm, paginaAtual, registroPorPagina, ordenacao, callback) {
             var dados = null;

             if ($scope.isPesquisa) {
                 dados = angular.copy(itemForm);
             }

             perfil.postRequisicao('PerfilBO', 'BuscarPerfis', { where: dados, paginaAtual: paginaAtual, registroPorPagina: registroPorPagina, ordenacao: ordenacao }).then(
                function (result) {
                    callback(result.data);
                },
                $rootScope.TratarErro
            );
         }

         //Carrega dropdown de TiposUsuario
         function carregaDropDown() {
             perfil.postRequisicao('PerfilBO', 'BuscarTipoUsuario', null).then(
                function (result) {
                    $scope.tiposUsuario = result.data;
                },
                $rootScope.TratarErro
            );
         }

         carregaDropDown();

         //Carrega Grid Perfil
         $scope.tablePerfil = new ngTableParams({
             page: 1,
             count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
             sorting: {
                 'nometipousuario': 'asc'
             }
         }, {
             counts: [],
             total: 0,
             getData: function ($defer, params) {

                 var ordenacao = params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '') + ' ';
                 ordenacao = ordenacao + params.sorting()[params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '')].toString();

                 var dados = carregaGrid($scope.itemForm, params.page(), params.count(), ordenacao, function (data) {
                     if (data && data.length > 0) {
                         params.total(data[0].totalregistros);
                     }
                     if (data.length == 0) {
                         params.total(0);
                     }
                     $defer.resolve(data);
                     $scope.items = data;

                 });
             }
         });

         //Construção de árvore ao selecionar aba de Permissões
         $scope.carregaArvoreAba = function () {
             if ($scope.itemForm.codigotipousuario == "" || $scope.itemForm.codigotipousuario == null) {
                 $('#tabs-perfil a:first').tab('show');

                 var mensagem = $translate.instant('1.MENSAGEM_NaoAlteraPermissoes');
                 $scope.alert(typeof mensagem === 'object' || mensagem == '1.MENSAGEM_NaoAlteraPermissoes' ? 'Não é possível alterar as permissões sem selecionar o tipo de usuário.' : mensagem);


             }
         }

         //Carrega informações de perfil
         $scope.carregaPerfil = function () {
             if ($('#arvore-permissoes').jstree(true).get_selected() == "") {
                 $scope.habilitacampos = false;
             }
         }

         //Modo de inserção
         $scope.onFormMode = function (item) {
             $rootScope.verificaPermissao('PerfilBO', 'Insert', function () {

                 var mensagem = $translate.instant('1.LBL_CadastrarPerfil');
                 $scope.titulo = typeof mensagem === 'object' || mensagem == '1.LBL_CadastrarPerfil' ? 'Cadastrar Perfil' : mensagem;

                 $scope.itemForm = {};
                 //$scope.tiposUsuario = {};
                 //carregaDropDown(); //TODO VALIDAR                 
                 $scope.editMode = true;
                 $scope.searchMode = false;
                 $scope.isPesquisa = false;
                 $('#tabs-perfil a:first').tab('show');
                 $scope.habilitacampos = false;
                 $scope.exibeVoltar = true;
                 Utils.bloquearCamposVisualizacao("VisualizarPerfil");

                 $('#formcontainer').focus();
             });
         };

         //Modo de pesquisa
         $scope.onSearchMode = function (item) {
             //$rootScope.verificaPermissao('PerfilBO', 'SelectFiltro', function () {
             var mensagem = $translate.instant('1.LBL_PesquisarPerfil');
             $scope.titulo = typeof mensagem === 'object' || mensagem == '1.LBL_PesquisarPerfil' ? 'Pesquisar Perfil' : mensagem;
             Utils.desbloquearCamposVisualizacaoPesquisa();
             $scope.itemForm = {};
             //carregaDropDown();
             $('#tabs-perfil a:first').tab('show');
             $scope.editMode = true;
             $scope.searchMode = true;
             $scope.habilitacampos = false;
             $scope.exibeVoltar = true;
             $scope.isPesquisa = false;
             $('#formcontainer').focus();
             //});
         };

         //Edição de informações de perfil
         $scope.onEdit = function (item) {
             $rootScope.verificaPermissao('PerfilBO', 'Update', function () {
                 
                 Utils.bloquearCamposVisualizacao("VisualizarPerfil");

                 var mensagem = $translate.instant('1.LBL_AlterarPerfil');
                 $scope.titulo = typeof mensagem === 'object' || mensagem == '1.LBL_AlterarPerfil' ? 'Alterar Perfil' : mensagem;
                 $scope.itemForm = angular.copy(item);
                 //$scope.tiposUsuario = {};
                 //carregaDropDown();
                 $scope.habilitacampos = false;
                 $scope.editMode = true;
                 $scope.searchMode = false;
                 $scope.exibeVoltar = true;
                 $scope.isPesquisa = false;
                 carregaArvore(item);
                 $('#tabs-perfil a:first').tab('show');
                 $('#formcontainer').focus();

             });
         };

         //Botão cancelar
         $scope.onCancel = function (item) {

             var mensagem = $translate.instant('1.LBL_CadastroPerfil');
             $scope.titulo = typeof mensagem === 'object' || mensagem == '1.LBL_CadastroPerfil' ? 'Cadastro de Perfil' : mensagem;
             $scope.itemForm = {};
             $scope.editMode = false;
             $scope.searchMode = false;
             $scope.form.submitted = false;
             Utils.moveScrollTop();
             $scope.tablePerfil.reload();
             $scope.tablePerfil.page(1);
             $scope.habilitacampos = false;
             $scope.exibeVoltar = false;
             $scope.isPesquisa = false;
         };

         //Chamado ao pressionar ESC na tela
         window.onkeydown = function (event) {
             if (event.keyCode === 27) {
                 $scope.onCancel();
             }
         };

         //Botão limpar
         $scope.onClean = function (item) {
             $scope.itemForm = {};
             $scope.habilitacampos = false;
         };

         //Recarregar árvore ao alterar o dropdown TipoUsuario
         $scope.change = function () {
             carregaArvore($scope.itemForm);
         }

         //Pesquisar perfis cadastrados
         $scope.onSearch = function (item) {
             //if ($scope.form.nome.$valid || $scope.form.descricao.$valid || $scope.form.status.$valid || $scope.form.codigotipousuario.$valid) {
             $scope.form.submitted = false;
             $scope.editMode = false;
             $scope.isPesquisa = true;
             $scope.exibeVoltar = false;
             Utils.moveScrollTop();
             $scope.tablePerfil.page(1);
             $scope.tablePerfil.reload();
             //} else {
             //$scope.alert('Preencha ao menos um campo de filtro.');
             //$scope.form.submitted = true;
             //}
             $scope.habilitacampos = false;
         };

         //Inserir permissões
         function salvarPermissao(item, alteracao, codigo) {
             var permissoes = $('#arvore-permissoes').jstree(true).get_selected();

             if (codigo != 1) {
                 perfil.postRequisicao('PerfilBO', 'InserePermissao', { campos: { codigo: codigo, permissoes: permissoes, atualiza: alteracao} }).then(
                    function (result) { },
                    $rootScope.TratarErro
                );
             }
         }

         //Verificar perfis
         $scope.verificaUsuario = function (callback) {
             perfil.postRequisicao('PerfilBO', 'verificaUsuario', { campos: { nome: $scope.itemForm.nome, codigo: $scope.itemForm.codigo} }).then(
                function (result) {
                    $rootScope.usuarioverificado = result.data;
                    callback();
                },
                $rootScope.TratarErro
            );
         }

         //Inserir ou alterar perfil
         $scope.onSave = function (item) {
             if ($scope.form.$valid) {
                 if (item.codigo != null || item.codigo != undefined) {
                     var parametro = { Nome: item.nome, Descricao: item.descricao, Status: item.status, CodigoTipoUsuario: item.codigotipousuario };
                     var dados = { campos: parametro, where: item.codigo };

                     $scope.verificaUsuario(function () {
                         if ($rootScope.usuarioverificado == 0) {
                             perfil.postRequisicao('PerfilBO', 'Update', dados).then(
                                function (result) {
                                    salvarPermissao(item, true, item.codigo);
                                    $scope.form.submitted = false;
                                    $scope.editMode = false;
                                    $scope.itemForm = {};
                                    //$scope.tiposUsuario = {};
                                    $scope.exibeVoltar = false;
                                    $scope.isPesquisa = false;
                                    Utils.moveScrollTop();
                                    $scope.tablePerfil.reload();

                                    var mensagem = $translate.instant('1.MENSAGEM_RegistroAlteradoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '1.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);

                                },
                                $rootScope.TratarErro
                            );
                         }
                         else {

                             var mensagem = $translate.instant('1.MENSAGEM_PerfilExistente');
                             $scope.alert(typeof mensagem === 'object' || mensagem == '1.MENSAGEM_PerfilExistente' ? 'Perfil já existente.' : mensagem);
                         }
                     });


                 }
                 else if (item.codigo == null || item.codigo == undefined) {

                     $scope.verificaUsuario(function () {
                         if ($rootScope.usuarioverificado == 0) {
                             perfil.postRequisicao('PerfilBO', 'Insert', { campos: item }).then(
                                function (result) {
                                    salvarPermissao(item, false, result.data[0].codigo);
                                    $scope.itemForm = {};
                                    //$scope.tiposUsuario = {};
                                    $scope.exibeVoltar = false;
                                    $scope.isPesquisa = false;
                                    Utils.moveScrollTop();
                                    $scope.tablePerfil.reload();
                                    $scope.form.submitted = false;
                                    $scope.editMode = false;

                                    var mensagem = $translate.instant('1.MENSAGEM_RegistroSalvoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '1.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);

                                },
                                $rootScope.TratarErro
                            );
                         }
                         else {

                             var mensagem = $translate.instant('1.MENSAGEM_PerfilExistente');
                             $scope.alert(typeof mensagem === 'object' || mensagem == '1.MENSAGEM_PerfilExistente' ? 'Perfil já existente.' : mensagem);
                         }
                     });

                 }
             } else {
                 $scope.form.submitted = true;

                 var mensagem = $translate.instant('1.MENSAGEM_CamposObrigatorios');
                 $scope.alert(typeof mensagem === 'object' || mensagem == '1.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);

                 $('#tabs-perfil a:first').tab('show');
             }
         };

         //Deletar perfil cadastrado
         $scope.onDelete = function (item) {
             $rootScope.verificaPermissao('PerfilBO', 'Delete', function () {

                 var mensagem = $translate.instant('1.MENSAGEM_DesejaRemover');
                 $scope.confirm(typeof mensagem === 'object' || mensagem == '1.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {

                     angular.forEach($scope.items, function (value, key) {
                         if (value.Codigo === item.Codigo) {
                             $scope.items[key] = item;
                         }
                     });

                     perfil.postRequisicao('PerfilBO', 'Delete', { where: item.codigo }).then(
                            function (result) {
                                var index = $scope.items.indexOf(item);
                                $scope.items.splice(index, 1);
                                Utils.moveScrollTop();
                                $scope.tablePerfil.reload();

                                var mensagem = $translate.instant('1.MENSAGEM_RegistroExcluidoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '1.MENSAGEM_RegistroExcluidoSucesso' ? 'Registro excluído com sucesso.' : mensagem);

                            },
                            $rootScope.TratarErro
                        );
                 });
             });
         };

         //$scope.onSearchMode($scope.item);
     } ]);