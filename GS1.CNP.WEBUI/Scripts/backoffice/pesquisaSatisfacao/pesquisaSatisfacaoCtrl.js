app.controller('PesquisaSatisfacao', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {

    var mensagem = $translate.instant('42.LBL_TitleCadastroPesquisaSatisfacao');
    $scope.titulo = typeof mensagem === 'object' || mensagem == '42.LBL_TitleCadastroPesquisaSatisfacao' ? 'Cadastro de Pesquisa de Satisfação' : mensagem;
    $scope.isPesquisa = false;

    $scope.tablePesquisaSatisfacao = new ngTableParams({
        page: 1,
        count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
            });
        }
    });

    //Busca Pesquisas de Satisfação
    function carregaGrid(item, isPesquisa, callback) {
        var dados = null;

        if (isPesquisa) {
            dados = angular.copy(item);

            genericService.postRequisicao('PesquisaSatisfacaoBO', 'PesquisarPesquisaSatisfacao', { campos: dados }).then(
                function (result) {

                    $scope.form.submitted = false;
                    $scope.editMode = false;

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }
        else {
            genericService.postRequisicao('PesquisaSatisfacaoBO', 'BuscarPesquisaSatisfacao', { where: dados }).then(
                function (result) {

                    $scope.papeis = result.data;

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }
    }

    $scope.onFormMode = function (item) {
        $rootScope.verificaPermissao('PesquisaSatisfacaoBO', 'CadastrarPesquisaSatisfacao', function () {

            var mensagem = $translate.instant('42.LBL_TitleCadastroPesquisaSatisfacao');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '42.LBL_TitleCadastroPesquisaSatisfacao' ? 'Cadastro de Pesquisa de Satisfação' : mensagem;

            Utils.bloquearCamposVisualizacao("VisualizarPesquisaSatisfacao");
            $scope.item = {};
            $scope.nomeimagem = undefined;
            $scope.isPesquisa = false;
            $scope.editMode = true;
            $scope.insertMode = true;
            $scope.searchMode = false;
            $scope.exibeVoltar = true;
        });
    };

    $scope.onEdit = function (item) {
        $rootScope.verificaPermissao('PesquisaSatisfacaoBO', 'EditarPesquisaSatisfacao', function () {

            Utils.bloquearCamposVisualizacao("VisualizarPesquisaSatisfacao");

            var mensagem = $translate.instant('42.LBL_TitleCadastroPesquisaSatisfacao');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '42.LBL_TitleCadastroPesquisaSatisfacao' ? 'Cadastro de Pesquisa de Satisfação' : mensagem;
            $scope.item = angular.copy(item);
            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.insertMode = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = true;
        });
    };

    $scope.onCancel = function (item) {
        var mensagem = $translate.instant('42.LBL_TitleCadastroPesquisaSatisfacao');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '42.LBL_TitleCadastroPesquisaSatisfacao' ? 'Cadastro de Pesquisa de Satisfação' : mensagem;
        $scope.item = {};
        item = {};
        $scope.editMode = false;
        $scope.searchMode = false;
        $scope.form.submitted = false;
        $scope.exibeVoltar = false;
        $scope.isPesquisa = false;
        $scope.tablePesquisaSatisfacao.page(1);
        $scope.tablePesquisaSatisfacao.reload();
    };

    //Chamado ao pressionar ESC na tela
    window.onkeydown = function (event) {
        if (event.keyCode === 27) {
            $scope.onCancel();
        }
    };

    $scope.onSearchMode = function (item) {
        var mensagem = $translate.instant('42.LBL_PesquisaSatisfacao');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '42.LBL_PesquisaSatisfacao' ? 'Pesquisar Pesquisa de Satisfação' : mensagem;
        Utils.desbloquearCamposVisualizacaoPesquisa();
        $scope.item = {};
        $scope.editMode = true;
        $scope.searchMode = true;
        $scope.isPesquisa = true;
        $scope.exibeVoltar = true;
    };

    $scope.onClean = function (item) {
        $scope.item = {};
    };

    //Pesquisa Pesquisa de Satisfação cadastrado
    $scope.onSearch = function (item) {
        $scope.exibeVoltar = false;
        $scope.tablePesquisaSatisfacao.page(1);
        $scope.tablePesquisaSatisfacao.reload();
    }

    function reinicializarVariaveis() {
        $scope.item = {};
        $scope.editMode = false;
        $scope.searchMode = false;
        $scope.form.submitted = false;
        $scope.exibeVoltar = false;
        $scope.isPesquisa = false;
        $scope.tablePesquisaSatisfacao.page(1);
        $scope.tablePesquisaSatisfacao.reload();
    }

    $scope.onSave = function (item) {

        if (!$scope.form.$valid) {
            var mensagem = $translate.instant('42.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '42.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            $scope.form.submitted = true;
        }
        else {
            var URL_REGEX = "((http\://|https\://|ftp\://)|(www.))+(([a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9%:/-_\?\.'~]*)?";
            var patt = new RegExp(URL_REGEX);

            if (item.url != undefined && item.url != '' && patt.test(item.url) == false) {

                var mensagem = $translate.instant('42.MENSAGEM_LinkFormatoInvalido');
                $scope.alert(typeof mensagem === 'object' || mensagem == '42.MENSAGEM_LinkFormatoInvalido' ? 'O Link informado não possui um formato válido. Por favor, verifique o Link informado.' : mensagem);
            }
            else {

                var dataInicio = Utils.dataMesDiaAno(item.inicio);
                var dataFim = Utils.dataMesDiaAno(item.fim);

                //Inserção de Pesquisa de Satisfação
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {

                    if (!Utils.validarPeriodoPesquisaSatisfacao(item.inicio, item.fim, true)) {
                        return false;
                    }
                    else {
                        var parametro = { nome: item.nome, descricao: item.descricao, url: item.url, inicio: dataInicio, fim: dataFim, status: item.status, codigopublicoalvo: item.codigopublicoalvo };

                        genericService.postRequisicao('PesquisaSatisfacaoBO', 'CadastrarPesquisaSatisfacao', { campos: parametro }).then(
                            function (result) {
                                reinicializarVariaveis();
                                var mensagem = $translate.instant('42.MENSAGEM_RegistroSalvoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '42.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                            },
                            $rootScope.TratarErro
                        );
                    }

                }
                //Atualização de Pesquisa de Satisfação
                else {
                    if (!Utils.validarPeriodoPesquisaSatisfacao(item.inicio, item.fim, false)) {
                        return false;
                    }
                    else {
                        var parametro = { codigo: item.codigo, nome: item.nome, descricao: item.descricao, url: item.url, inicio: dataInicio, fim: dataFim, status: item.status, codigopublicoalvo: item.codigopublicoalvo };

                        genericService.postRequisicao('PesquisaSatisfacaoBO', 'EditarPesquisaSatisfacao', { campos: parametro }).then(
                            function (result) {
                                reinicializarVariaveis();

                                var mensagem = $translate.instant('42.MENSAGEM_RegistroAlteradoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '42.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                            },
                            $rootScope.TratarErro
                        );
                    }
                }
            }
        }
    }

    $scope.onInit = function () {
        //Busca informações do Dropdwon de Publico Alvo
        genericService.postRequisicao('PublicoAlvoBO', 'BuscarPublicoAlvoAtivo', {}).then(
            function (result) {
                if (result.data != undefined && result.data != null) {
                    $scope.publicosalvo = result.data;
                }
            },
            $rootScope.TratarErro
        );
    }

    //Remover Pesquisa
    $scope.removePesquisa = function (item) {

        var titulo = $translate.instant('42.LBL_Confirmacao');
        var mensagem = $translate.instant('42.MENSAGEM_DesejaRemover');
        
        $rootScope.confirm(typeof mensagem === 'object' || mensagem == '42.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {
            genericService.postRequisicao('PesquisaSatisfacaoBO', 'ExcluirPesquisaSatisfacao', { campos: item }).then(
                function (result) {
                    reinicializarVariaveis
                    var mensagem = $translate.instant('42.MENSAGEM_DadosRemovidosSucesso');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '42.MENSAGEM_DadosRemovidosSucesso' ? 'Dados removidos com sucesso.' : mensagem);
                 },
                $rootScope.TratarErro
            );
        }, function () { }, typeof titulo === 'object' || titulo == '42.LBL_Confirmacao' ? 'Confirmação' : titulo);

    }
    $scope.onInit();

} ]);
