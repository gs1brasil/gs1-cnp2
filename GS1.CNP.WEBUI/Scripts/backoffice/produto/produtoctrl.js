app.controller('ProdutoCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$log', '$modal', 'genericService', 'ProdutoService', 'Utils', 'blockUI', '$q', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $log, $modal, genericService, produto, Utils, blockUI, $q, $translate) {

        //inicializando abas do form
        $('#tabs-produto a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover({ html: true,
                content: function () {
                    $translate($(this).attr('tooltipID')).then(function (headline) {
                        return headline;
                    });
                }
            });
        });
        var DEFAULT_IMPORT_CLASSIFICATION_TYPE;

        var titulo = $translate.instant('15.LBL_CadastroProduto');
        $scope.titulo = typeof titulo === 'object' || titulo == '15.LBL_CadastroProduto' ? 'Cadastro de Produtos' : titulo;

        $scope.isPesquisa = false;
        $scope.stepCalculation = 1;
        $scope.indicadorcompartilhadados = 0;
        $scope.imagensSelecionadas = false;
        $scope.itemForm = {};
        $scope.insertURLMode = true;
        $scope.listURL = [];
        $scope.listAgencias = [];
        $scope.agenciasExcluidas = [];
        $scope.isRequiredAgencia = false;
        $scope.listGTINInferior = [];
        $scope.isRequiredURL = false;
        $scope.isRequiredGDSN = false;
        $scope.isRequiredCNP = true;
        $scope.isRequiredAlways = true;
        $rootScope.tamanhoMaximum = 7, $rootScope.tamanhoMinimum = 7;
        $scope.listaBoleana = [{ 'codigo': 1, 'nome': 'Sim' }, { 'codigo': 0, 'nome': 'Não'}];
        $scope.bloquearGTIN = false;
        $scope.escondeCamposGTIN14 = true;
        $scope.showPalletDependency = false;
        $scope.showFieldsAgency = false;
        $scope.showInformacoesNutricionais = false;
        $scope.informacoesNutricionais = {};
        $scope.showAlergenos = false;
        $scope.alergenos = [];

        function montaArraySteps(hasHistorico) {
            $scope.steps = [];

            $translate(['15.ABA_InformacoesBasicas', '15.ABA_Hierarquias', '15.ABA_Midias', '15.ABA_InformacoesComplementares']).then(function (translations) {
                $scope.steps.push(translations['15.ABA_InformacoesBasicas']);
                $scope.steps.push(translations['15.ABA_Hierarquias']);
                $scope.steps.push(translations['15.ABA_Midias']);
                $scope.steps.push(translations['15.ABA_InformacoesComplementares']);

                if (hasHistorico) {
                    $translate('15.ABA_Historico').then(function (result) {
                        $scope.steps.push(result);
                    });
                }
            });
        }

        $scope.onExport = function (type) {
            var search = JSON.stringify($scope.itemForm);
            var dados = {
                campos: '{ campos: ' + search.replaceAll('"', "'") + ', where: { tipoarquivo: \'' + type + '\' }}'
            };

            $.ajaxFileDownload({
                url: 'ExportaProdutosHandler.ashx',
                secureuri: false,
                dataType: "json",
                data: dados,
                error: function (xhr, status, e) {
                    $scope.alert(e);
                }
            });

        }

        $scope.tableProdutos = new ngTableParams({
            page: 1,
            count: $rootScope.parametros != undefined ? $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina') : 10,
            sorting: {
                codigoproduto: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {

                blockUI.start();
                var ordenacao = params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '') + ' ';
                ordenacao = ordenacao + params.sorting()[params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '')].toString();

                var dados = carregaGrid($scope.itemForm, $scope.isPesquisa, function (data) {
                    if (data && data.length > 0) {
                        params.total(data[0].totalregistros);
                    }
                    if (data.length == 0) {
                        params.total(0);
                    }
                    $scope.moveScrollTop();
                    //var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    //$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    $defer.resolve(data);
                    blockUI.stop();
                }, params.page(), params.count(), ordenacao);
            }
        });

        $scope.tableProdutoURL = new ngTableParams({
            page: 1,
            count: 5,
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if ($scope.itemForm.productdescription != undefined && $scope.itemForm.productdescription != null) {
                    var dados = carregaGridURL($scope.itemForm, function (data) {
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                        $scope.listURL = data;
                    });
                }
            }
        });

        $scope.tableAgencias = new ngTableParams({
            page: 1,
            count: 5,
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if ($scope.itemForm.productdescription != undefined && $scope.itemForm.productdescription != null) {
                    var dados = carregaGridAgencias($scope.itemForm, function (data) {
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                        $scope.listAgencias = data;
                    });
                }
            }
        });

        $scope.tableGTINInferior = new ngTableParams({
            page: 1,
            count: 10,
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if ($scope.itemForm.productdescription != undefined && $scope.itemForm.productdescription != null) {
                    var dados = carregaGridGTIN($scope.itemGTIN, function (data) {
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                        $scope.listGTINInferior = data;
                    });
                }
            }
        });

        $scope.tableAtributoBrick = new ngTableParams({
            page: 1,
            count: 10,
            sorting: { LOGIN: 'asc' }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaAtributoBrick($scope.itemForm, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                });
            }
        });

        //Busca Atributo Brick
        function carregaAtributoBrick(item, callback) {
            var dados = null;

            dados = angular.copy(item);

            genericService.postRequisicao('ProdutoBO', 'BuscarTodosAtributosBrick', { campos: dados }).then(
                function (result) {

                    $scope.atributosBrickAntigos = angular.copy(result.data);

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        //Busca GTIN Inferior
        function carregaGridGTIN(item, callback) {
            var dados = null;

            dados = angular.copy(item);

            genericService.postRequisicao('ProdutoBO', 'BuscarGTINInferior', { campos: dados }).then(
                function (result) {

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        //Busca Produtos
        function carregaGrid(item, isPesquisa, callback, paginaAtual, registroPorPagina, ordenacao) {
            var dados = null;

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                    if (data.indicadorcnaerestritivo == 1) {
                        $scope.restricaoCNAEmedico = true;
                    }
                    else {
                        $scope.restricaoCNAEmedico = false;
                    }

                    if (isPesquisa) {
                        dados = angular.copy(item);

                        genericService.postRequisicao('ProdutoBO', 'PesquisaProdutos', { campos: dados, paginaAtual: paginaAtual, registroPorPagina: registroPorPagina, ordenacao: ordenacao }).then(
                            function (result) {

                                $scope.form.submitted = false;
                                $scope.editMode = false;

                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                        );
                    }
                    else {
                        genericService.postRequisicao('ProdutoBO', 'BuscarProdutos', { campos: dados, paginaAtual: paginaAtual, registroPorPagina: registroPorPagina, ordenacao: ordenacao }).then(
                            function (result) {
                                if (callback != undefined && callback != null) {
                                    callback(result.data);
                                }
                            },
                            $rootScope.TratarErro
                        );
                    }

                    $rootScope.flagAssociadoSelecionado = true;
                }
            });
        }

        //Indicador
        $scope.onChangeType = function (tipo) {

            if (tipo == 0 || tipo == false) {
                setFalseForms();
                $scope.isRequiredCNP = true;
                $scope.isRequiredGDSN = false;
            }
            else if (tipo == 1 || tipo == true) {
                setFalseForms();
                $scope.isRequiredCNP = false;
                $scope.isRequiredGDSN = true;
            }
            else {
                setFalseForms();
                $scope.isRequiredCNP = false;
                $scope.isRequiredGDSN = false;
            }
        }

        //Chamado ao alterar status de um produto
        $scope.showMessageStatus = function (novo, antigo) {

            if (novo != undefined && $scope.itemCompare != undefined) {

                //Cancelado
                if (novo == 4) {
                    $translate('15.MENSAGEM_OsCodigosExcluidos').then(function (result) {
                        $rootScope.confirm(result + ' "' + $scope.itemCompare.productdescription + '"?', function () {

                        }, function () {
                            $scope.itemForm.codigostatusgtin = parseInt(antigo);
                        }, 'Confirmação');
                    });
                }
                //Suspenso
                else if (novo == 2) {
                    $translate('15.MENSAGEM_AlteracaoStatusSuspender').then(function (result) {
                        $rootScope.confirm(result + ' "' + $scope.itemCompare.productdescription + '"?', function () {

                        }, function () {
                            $scope.itemForm.codigostatusgtin = parseInt(antigo);
                        }, 'Confirmação');
                    });
                }
                //Reativado
                else if (novo == 3) {
                    $translate('15.MENSAGEM_AlteracaoStatusReativado').then(function (result) {
                        $rootScope.confirm(result + ' "' + $scope.itemCompare.productdescription + '"?', function () {

                        }, function () {
                            $scope.itemForm.codigostatusgtin = parseInt(antigo);
                        }, 'Confirmação');
                    });
                }
                //Reutilizado
                else if (novo == 6) {
                    $translate('15.MENSAGEM_Reutilizacao48meses').then(function (result) {
                        $rootScope.confirm(result + ' "' + $scope.itemCompare.productdescription + '"?', function () {

                        }, function () {
                            $scope.itemForm.codigostatusgtin = parseInt(antigo);
                        }, 'Confirmação');
                    });
                }
            }
        }

        //Chamado ao alterar o tipogtin
        $scope.obterTipoGtin = function (tipogtin) {
            if (tipogtin == 4 && $scope.initMode) {
                $scope.escondeCamposGTIN14 = true;
                $scope.onGtinOrigem($scope.itemForm);
            }
            else {
                $scope.escondeCamposGTIN14 = false;
            }

            //Tipo de Código de Barras
            //$scope.carregarCodigosdeBarras(tipogtin);
        }

        //Altera Indicador Compartilha Dados
        $scope.alterarIndicador = function (item) {
            if (item === '1') $scope.itemForm.indicadorcompartilhadados = false;
            else if (item === '0') $scope.itemForm.indicadorcompartilhadados = true;
            else $scope.itemForm.indicadorcompartilhadados = item;
        }

        //Busca Produto URL
        function carregaGridURL(item, callback) {
            var dados = angular.copy(item);

            genericService.postRequisicao('ProdutoBO', 'BuscarURLsProduto', { campos: dados }).then(
                function (result) {
                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        //Busca agências reguladoras
        function carregaGridAgencias(item, callback) {
            var dados = angular.copy(item);

            genericService.postRequisicao('ProdutoBO', 'BuscarAgenciasReguladoras', { campos: dados }).then(
                function (result) {
                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
        }

        //Chamado ao selecionar a Licença
        $scope.alteraPrefixoGS1 = function (type) {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('ProdutoBO', 'CarregarDropdownPrefixo', { campos: { "licenca": type} }).then(
                    function (result) {
                        if (result != undefined && result.data != undefined) {
                            if (!$scope.editMode) {
                                $scope.itemForm.globaltradeitemnumber = "";
                                $scope.itemForm.nrprefixo = "";
                            }
                            $scope.prefixoGS1 = result.data;
                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }

        //Pegar GTIN valido
        $scope.PegarGTINValido = function (type) {
            return $q(function (resolve, reject) {
                genericService.postRequisicao('ProdutoBO', 'PegarGTINValido', { campos: { "codigotipogtin": type} }).then(
                    function (result) {
                        if (result != undefined && result.data != undefined) {
                            resolve(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            });
        }

        //Gera GTIN
        $scope.gerarGTIN = function (codigotipogtin) {
            $scope.isRequiredAgencia = false;
            $scope.isRequiredURL = false;

            $timeout(function () {
                if (!$scope.form.base.$valid || !$scope.form.hierarquia.$valid || !$scope.form.complementar.$valid || !$scope.form.outros.$valid) {
                    var mensagem = $translate.instant('15.MENSAGEM_CamposObrigatorios');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                    $scope.form.submitted = true;
                }
                else {
                    $scope.onSave($scope.itemForm, 'GerarGTIN');
                }
            });
        }

        //Gerador de Dígito Verificador do GTIN
        $scope.geraDigitoVerificador = function (gtin) {
            var resultado = 0;

            for (var i = 0; i < gtin.length; i++) {

                if (((i + 1) % 2 == 0 && gtin.length % 2 == 0)
                    || ((i + 1) % 2 == 1 && gtin.length % 2 == 1)) {
                    resultado += (parseInt(gtin[i]) * 3);
                }
                else {
                    resultado += parseInt(gtin[i]);
                }
            }

            var verificadorResultado = 10 - (resultado % 10);

            if (verificadorResultado == 10) verificadorResultado = 0;

            return verificadorResultado;
        }

        //Exclusão de imagem cadastrada
        $scope.ExcluirImagem = function (item) {
            $translate('15.MENSAGEM_ImagemSeraExcluidaBase').then(function (result) {
                $rootScope.confirm(result, function () {

                    genericService.postRequisicao('ProdutoBO', 'DeletarImagem', { campos: item }).then(
                        function (result) {
                            $scope.removerImagemPorId($scope.imagens, item.codigo);
                            item.nomeimagem = '';
                            $scope.nomeimagem = '';
                            var mensagem = $translate.instant('15.MENSAGEM_ImagemRemovidaSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_ImagemRemovidaSucesso' ? 'Imagem removida com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );

                }, function () { }, 'Confirmação');
            });
        }

        //Remove imagem do conjunto
        $scope.removerImagemPorId = function (itens, codigo) {
            for (var i in itens) {
                if (itens[i].codigo == codigo) {
                    itens.splice(i, 1);
                }
            }
        }

        //Modo de Inserção
        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('ProdutoBO', 'CadastrarProduto', function () {

                var titulo = $translate.instant('15.LBL_CadastrarProduto');
                $scope.titulo = typeof titulo === 'object' || titulo == '15.LBL_CadastrarProduto' ? 'Cadastro de Produto' : titulo;

                $scope.stepCalculation = 1;
                montaArraySteps(false);
                $('#tabs-produto a:first').tab('show');
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.isRequiredURL = false;
                $scope.isRequiredAgencia = false;
                $scope.initMode = true;
                $scope.exibeVoltar = true;
                $scope.insertURLMode = true;
                $scope.bloquearGTIN = false;
                $scope.imagens = [];
                $scope.itemUrl = {};
                $scope.listURL = [];
                $scope.listAgencias = [];
                $scope.listGTINInferior = [];
                $scope.itemForm = {};
                $scope.itemCompare = {};
                $scope.tiposetiquetas = {};
                $scope.escondeCamposGTIN14 = false;
                $scope.showPalletDependency = false;
                angular.element("#productdescription").removeClass("destacarDecricao");

                //Valor inicial como CNP
                $scope.itemForm.indicadorgdsn = 0;

                //Valores Default
                $scope.itemForm.heightmeasurementunitcode = 'CMT';
                $scope.itemForm.depthmeasurementunitcode = 'CMT';
                $scope.itemForm.widthmeasurementunitcode = 'CMT';
                $scope.itemForm.netcontentmeasurementunitcode = 'MAL';
                $scope.itemForm.netweightmeasurementunitcode = 'GRM';
                $scope.itemForm.grossweightmeasurementunitcode = 'GRM';
                $scope.itemForm.tradeitemcountryoforigin = 76;
                $scope.onSelectedCountry($scope.itemForm.tradeitemcountryoforigin);
                $scope.itemForm.countrycode = 76;
                $scope.itemForm.codigolingua = 348;

                if ($scope.glns != undefined && $scope.glns.length > 0)
                    $scope.itemForm.informationprovider = $scope.glns[0].nomegln;

                //Valor inicial do status como 'Em elaboração'
                $scope.itemForm.codigostatusgtin = 5;
                $scope.itemCompare.codigostatusgtin = 5;

                $scope.tipoprodutoselecionado = undefined;

                $scope.tableAtributoBrick.reload();
                $scope.tableAtributoBrick.page(1);
                $scope.onChangeType(0);
                resetDatepickerCss();
                $scope.verifyPaginationAgency();

                $scope.showInformacoesNutricionais = false;
                $scope.informacoesNutricionais = {};
                $scope.showAlergenos = false;
                $scope.alergenos = [];

                $('#form4 :input:first').focus();
                Utils.bloquearCamposVisualizacao("VisualizarProduto");
            });
        };

        //Modo de Pesquisa
        $scope.onSearchMode = function (item) {
            var titulo = $translate.instant('15.TOOLTIP_PesquisarProduto');
            $scope.titulo = typeof titulo === 'object' || titulo == '15.TOOLTIP_PesquisarProduto' ? 'Pesquisar Produto.' : titulo;

            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.initMode = false;
            $scope.exibeVoltar = true;
            $scope.isRequiredURL = false;
            $scope.isRequiredAgencia = false;
            $scope.bloquearGTIN = false;
            $('#tabs-produto a:first').tab('show');
            $scope.itemForm = {};
            $scope.limparUploadFiles();
            angular.element("#productdescription").removeClass("destacarDecricao")
            $('#form :input:first').focus();
        };

        //Limpa submit dos forms
        function setFalseForms() {
            $scope.form.submitted = false;
            if ($scope.form.base != undefined) $scope.form.base.submitted = false;
            if ($scope.form.midia != undefined) $scope.form.midia.submitted = false;
            if ($scope.form.complementar != undefined) $scope.form.complementar.submitted = false;
            if ($scope.form.hierarquia != undefined) $scope.form.hierarquia.submitted = false;
            if ($scope.form.outros != undefined) $scope.form.outros.submitted = false;
            if ($scope.form.pesquisa != undefined) $scope.form.pesquisa.submitted = false;
        }

        $scope.onCancel = function (item) {
            var titulo = $translate.instant('15.LBL_CadastroProduto');
            $scope.titulo = typeof titulo === 'object' || titulo == '15.LBL_CadastroProduto' ? 'Cadastro de Produto' : titulo;

            $scope.itemForm = {};
            $scope.itemUrl = {};
            $scope.listGTINInferior = [];
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.isRequiredURL = false;
            $scope.isRequiredAgencia = false;
            setFalseForms();
            resetDatepickerCss();
            $scope.limparUploadFiles();
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $scope.escondeCamposGTIN14 = false;
            $scope.stepCalculation = 1;
            $scope.moveScrollTop();
            $scope.tableProdutos.page(1);
            $scope.tableProdutos.reload();
            angular.element("#productdescription").removeClass("destacarDecricao");
        }

        $scope.selectStep = function (step) {
            if ($scope.stepCalculation == step) {
                return;
            }

            $scope.moveScrollTop();
            $scope.stepCalculation = step;
        }

        $scope.setClassComplete = function (step) {
            $scope.queroSelecionar = step;
            $scope.showComplete = true;
        }

        $scope.removeClassComplete = function (step) {
            $scope.showComplete = false;
        }

        $scope.irParaAba = function (step) {
            if ($scope.stepCalculation == step)
                return;

            $scope.moveScrollTop();
            $scope.stepCalculation = step;
        }

        $scope.voltarParaAba = function (step) {
            $scope.moveScrollTop();
            $scope.stepCalculation = step;
        }

        $scope.onClean = function (item) {
            $scope.itemForm = {};
            $scope.limparUploadFiles();
        }

        $scope.moveScrollTop = function () {
            $(window).scrollTop(0);
            $(".page-container").scrollTop(0);
        }

        $rootScope.buscarImagensProduto = function (item, callback) {
            genericService.postRequisicao('ProdutoBO', 'BuscarImagensProduto', { campos: item }).then(
                function (result) {
                    $scope.imagens = result.data

                    if (callback != null)
                        callback(result.data);
                },
                $rootScope.TratarErro
            );
        }

            $rootScope.buscarImagensProdutoPreview = function (item, callback) {
                genericService.postRequisicao('ProdutoBO', 'BuscarImagensProdutoPreview', { campos: item }).then(
                function (result) {
                    if (callback != null)
                        callback(result.data);
                },
                $rootScope.TratarErro
            );
            }

        function buscarURLsProduto(item) {
            genericService.postRequisicao('ProdutoBO', 'BuscarURLsProduto', { campos: item }).then(
                function (result) {
                    $scope.urls = result.data
                },
                $rootScope.TratarErro
            );
        }

        //Tipo de Código de Barras
        $scope.carregarCodigosdeBarras = function (tipogtin) {
            genericService.postRequisicao('BarCodeTypeListBO', 'buscarTipoCodigoBarras', { where: { codigotipogtin: tipogtin} }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        if (result.data.length > 0) {
                            $scope.tiposetiquetas = result.data;
                        }
                    }
                },
                $rootScope.TratarErro
            );
        }

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('ProdutoBO', 'EditarProduto', function () {
                $scope.moveScrollTop();
                var titulo = $translate.instant('15.LBL_AlterarProduto');
                $scope.titulo = typeof titulo === 'object' || titulo == '15.LBL_AlterarProduto' ? 'Alterar Produto.' : titulo;

                $scope.stepCalculation = 1;
                $scope.nomeimagem = undefined;
                montaArraySteps(true);
                $rootScope.buscarImagensProduto(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.initMode = false;
                $scope.bloquearGTIN = false;
                $scope.insertURLMode = true;
                $scope.isRequiredURL = false;
                $scope.isRequiredAgencia = false;
                $scope.exibeVoltar = true;
                $scope.escondeCamposGTIN14 = (item.codigotipogtin == 4);
                $scope.itemForm = angular.copy(item);
                $scope.itemCompare = angular.copy(item);
                $scope.verifyPaginationAgency();

                $scope.onChangeType($scope.itemForm.indicadorgdsn);

                var segmento = $scope.onSelectSegment(item.codesegment);
                segmento.then(function () {

                    var familia = $scope.onSelectFamily(item.codefamily);

                    familia.then(function () {
                        var classe = $scope.onSelectClass(item.codeclass);
                        classe.then(function () {

                            $scope.onSelectBrick();
                            Utils.bloquearCamposVisualizacao("VisualizarProduto");
                        }, $rootScope.TratarErro);

                    }, $rootScope.TratarErro);

                }, $rootScope.TratarErro);

                if (item.tradeitemcountryoforigin != undefined && item.tradeitemcountryoforigin != '') {
                    var pais = $scope.onSelectedCountry(item.tradeitemcountryoforigin);
                    pais.then(function () {

                    }, $rootScope.TratarErro);
                }

                //if (item.codigotipogtin != undefined && item.codigotipogtin != '') {
                //    $scope.carregarCodigosdeBarras(item.codigotipogtin);
                //}

                $scope.showHideTipoProduto(item.codigotipoproduto);

                $scope.limparUploadFiles();
                $scope.tableProdutoURL.reload();
                $scope.tableAgencias.reload();

                //Tabela de GTIN Inferior
                $scope.itemGTIN = {};
                $scope.itemGTIN.codigoprodutosuperior = item.codigoproduto;
                $scope.tableGTINInferior.page(1);
                $scope.tableGTINInferior.reload();

                loadInformacoesNutricionais(item.codigoproduto);
                loadAlergenos(item.codigoproduto);

                $('#form4 :input:first').focus();
            });
        };

        var loadInformacoesNutricionais = function (codigoproduto) {
            genericService.postRequisicao('ProdutoBO', 'BuscarProdutoNutrientHeader', { where: { 'codigoproduto': codigoproduto} }).then(
                function (result) {
                    if (result.data != undefined && result.data != null && result.data.length > 0 && result.data[0] != null) {
                        var dadosHeader = result.data[0];
                        var header = {
                            'servingSizeDescription': dadosHeader.servingsizedescription
                            , 'dailyValueIntakeReference': dadosHeader.dailyvalueintakereference
                        };
                        genericService.postRequisicao('ProdutoBO', 'BuscarProdutoNutrientDetail', { where: { 'codigoproduto': codigoproduto} }).then(
                            function (result) {
                                if (result.data != undefined && result.data != null) {
                                    var detail = result.data;
                                    detail = detail.map(function (d) {
                                        return {
                                            'quantityContained': d.quantitycontained
                                            , 'measurementPrecisionCode': d.measurementprecisioncode
                                            , 'dailyValueIntakePercent': d.dailyvalueintakepercent
                                            , 'codigo': d.codigo
                                            , 'nutrientTypeCode': {
                                                'codigo': d.nt_codigo,
                                                'nome': d.nt_nome,
                                                'sigla': d.nt_sigla,
                                                'tipounidademedida': d.nt_tipounidademedida
                                            }
                                        };
                                    });

                                    $scope.informacoesNutricionais.produtoNutrientHeader = header;
                                    $scope.informacoesNutricionais.produtoNutrientDetail = detail;
                                }
                            },
                            $rootScope.TratarErro
                        );
                    }
                },
                $rootScope.TratarErro
            );
        };

        var loadAlergenos = function (codigoproduto) {
            genericService.postRequisicao('ProdutoBO', 'BuscarProdutoAlergeno', { where: { 'codigoproduto': codigoproduto} }).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {

                        var containmentCodes = {
                            'CONTAINS': { 'id': 1, 'codigo': 'CONTAINS', 'nome': 'CONTÉM', 'descricao': 'Intencionalmente incluído no produto.' }
                            , 'FREE_FROM': { 'id': 2, 'codigo': 'FREE_FROM', 'nome': 'NÃO CONTÉM', 'descricao': 'O produto não contém a substância indicada.' }
                            , 'MAY_CONTAIN': { 'id': 3, 'codigo': 'MAY_CONTAIN', 'nome': 'PODE CONTER', 'descricao': 'A substância não é incluída intencionalmente. No entanto, devido às instalações de produção compartilhadas ou por outros motivos, o produto pode conter a substância.' }
                        };

                        var alergenos = result.data.map(function (d) {
                            return {
                                "allergenRelatedInformation": {
                                    "allergen": {
                                        "codigo": d.at_codigo,
                                        "sigla": d.at_sigla,
                                        "descricao": d.at_descricao
                                    }
                                },
                                "levelOfContainmentCode": containmentCodes[d.levelofcontainmentcode]
                                , 'codigo': d.codigo
                            };
                        });
                        $scope.alergenos = alergenos;
                    }
                },
                $rootScope.TratarErro
            );
        };

        $scope.onSearch = function (item) {
            var titulo = $translate.instant('15.LBL_CadastroProduto');
            $scope.titulo = typeof titulo === 'object' || titulo == '15.LBL_CadastroProduto' ? 'Cadastro de Produto' : titulo;

            $scope.exibeVoltar = false;
            $scope.escondeCamposGTIN14 = false;
            $scope.itemForm = angular.copy(item);
            $scope.moveScrollTop();
            $scope.tableProdutos.page(1);
            $scope.tableProdutos.reload();
        };

        $scope.limparUploadFiles = function () {
            $("#inputImagemProduto").val('');
            $scope.imagensSelecionadas = false;
        }

        $scope.alterImage = function () {
            if ($("#inputImagemProduto")[0].files.length > 0) {
                $scope.imagensSelecionadas = true;
            }
            else {
                $scope.imagensSelecionadas = false;
            }
            $scope.nomeimagem = undefined
        }

        //Verifica parâmetros e tbm se não existe outro com os mesmos dados
        $scope.canSave = function (items) {
            var hasAnotherOne = false;
            var contador = 0;

            angular.forEach($scope.listURL, function (val, key) {
                if (val.url == items.url && val.codigotipourl == items.codigotipourl && val.codigo != items.codigo) {
                    hasAnotherOne = true;
                }
            });

            if (!hasAnotherOne && $scope.insertURLMode) {
                if (items.codigotipourl == 1) {
                    angular.forEach($scope.listURL, function (val, key) {
                        if (val.codigotipourl == 1) contador += 1;
                    });

                    if (contador >= $scope.parametroURLFoto) {
                        return 2;
                    }
                }
                else if (items.codigotipourl == 2) {
                    angular.forEach($scope.listURL, function (val, key) {
                        if (val.codigotipourl == 2) contador += 1;
                    });

                    if (contador >= $scope.parametroURLReserva) {
                        return 2;
                    }
                }
                else if (items.codigotipourl == 3) {
                    angular.forEach($scope.listURL, function (val, key) {
                        if (val.codigotipourl == 3) contador += 1;
                    });

                    if (contador >= $scope.parametroURLLinkeddata) {
                        return 2;
                    }
                }
                else if (items.codigotipourl == 4) {
                    angular.forEach($scope.listURL, function (val, key) {
                        if (val.codigotipourl == 4) contador += 1;
                    });

                    if (contador >= $scope.parametroURLYoutube) {
                        return 2;
                    }
                }
                else if (items.codigotipourl == 5) {
                    angular.forEach($scope.listURL, function (val, key) {
                        if (val.codigotipourl == 5) contador += 1;
                    });

                    if (contador >= $scope.parametroURLProduto) {
                        return 2;
                    }
                }

                return 1;
            }
            else {
                return 1;
            }
        }

        //Modo de Alteração de URL
        $scope.onAlterURL = function (item) {
            $scope.itemUrl = angular.copy(item);
            $scope.insertURLMode = false;
        }

        //Remover URL da tabela provisória
        $scope.removeURL = function (item) {

            var mensagem = $translate.instant('15.MENSAGEM_DesejaRemover');

            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {

                for (var i in $scope.listURL) {
                    if ($scope.listURL[i].codigo == item.codigo) {
                        if ($scope.listURL[i].status == "novo" || $scope.listURL[i].status == "novo-e-editado")
                            $scope.listURL[i].status = "excluido2";
                        else
                            $scope.listURL[i].status = "excluido";
                    }
                }

                $scope.insertURLMode = true;
                $scope.itemUrl = {};

            }, function () { }, 'Confirmação');
        }

        //Alterar URL da tabela provisória
        $scope.atualizarURL = function (item) {

            if ($scope.form.midia.nomeURL.$valid && $scope.form.midia.tipoURL.$valid) {

                var URL_REGEX = "((http\://|https\://|ftp\://)|(www.))+(([a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9%:/-_\?\.'~]*)?";
                var patt = new RegExp(URL_REGEX);

                var canSave = $scope.canSave(item);

                if (item.url != undefined && item.url != '' && patt.test(item.url) == false) {

                    var mensagem = $translate.instant('15.MENSAGEM_URLFormatoInvalido');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_URLFormatoInvalido' ? 'A URL informada não possui um formato válido. Por favor, verifique a URL informada.' : mensagem);
                }
                else {
                    if (canSave == 1) {

                        for (var i = 0; i < $scope.listURL.length; i++) {
                            if (item.codigo == $scope.listURL[i].codigo) {
                                if ($scope.listURL[i].status == "novo")
                                    item.status = "novo-e-editado";
                                else
                                    item.status = "editado";
                                $scope.listURL[i] = item;
                            }
                        }

                        $scope.insertURLMode = true;
                        var mensagem = $translate.instant('15.MENSAGEM_URLAlteradaSucesso');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_URLAlteradaSucesso' ? 'URL alterada com sucesso.' : mensagem);
                        $scope.itemUrl = {};
                    }
                    else if (canSave == 2) {
                        var mensagem = $translate.instant('15.MENSAGEM_QuantidadeLinksSuperior');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_QuantidadeLinksSuperior' ? 'Quantidade de links superior ao permitido.' : mensagem);
                    }
                    else {
                        var mensagem = $translate.instant('15.MENSAGEM_URLJaExiste');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_URLJaExiste' ? 'A URL informada já existe.' : mensagem);
                    }
                }
            }
            else {
                $scope.form.midia.nomeURL.submitted = true;
                $scope.form.midia.tipoURL.submitted = true;
                $scope.form.midia.nome.submitted = true;
                var mensagem = $translate.instant('15.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            }
        }

        $scope.cancelarURL = function () {
            $scope.itemUrl = {};
            $scope.form.midia.nomeURL.submitted = false;
            $scope.form.midia.tipoURL.submitted = false;
            $scope.form.midia.nome.submitted = false;
            $scope.insertURLMode = true;
        }

        //Salvar URL na tabela provisória
        $scope.onSaveURL = function (items) {

            $scope.isRequiredURL = true;
            var URL_REGEX = "((http\://|https\://|ftp\://)|(www.))+(([a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9%:/-_\?\.'~]*)?";
            var patt = new RegExp(URL_REGEX);

            if (items != undefined && items.nome != undefined && items.codigotipourl != undefined && items.url != undefined) {

                if ($scope.listURL.length > 0) {
                    var canSave = $scope.canSave(items);

                    if (items.url != undefined && items.url != '' && patt.test(items.url) == false) {
                        var mensagem = $translate.instant('15.MENSAGEM_URLFormatoInvalido');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_URLFormatoInvalido' ? 'A URL informada não possui um formato válido. Por favor, verifique a URL informada.' : mensagem);
                    }
                    else {
                        if (canSave == 1) {
                            var maior = verificaMaiorCodigo();
                            items.codigo = maior + 1;
                            items.status = "novo";
                            $scope.listURL.push(items);
                            var mensagem = $translate.instant('15.MENSAGEM_URLInseridaSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_URLInseridaSucesso' ? 'URL inserida com sucesso.' : mensagem);
                            $scope.itemUrl = {};
                            $scope.form.midia.nomeURL.submitted = false;
                            $scope.form.midia.tipoURL.submitted = false;
                            $scope.form.midia.nome.submitted = false;
                        }
                        else if (canSave == 2) {
                            var mensagem = $translate.instant('15.MENSAGEM_QuantidadeLinksSuperior');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_QuantidadeLinksSuperior' ? 'Quantidade de links superior ao permitido.' : mensagem);
                        }
                        else {
                            var mensagem = $translate.instant('15.MENSAGEM_URLJaExiste');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_URLJaExiste' ? 'A URL informada já existe.' : mensagem);
                        }
                    }
                }
                else {
                    if (items.url != undefined && items.url != '' && patt.test(items.url) == false) {
                        var mensagem = $translate.instant('15.MENSAGEM_URLFormatoInvalido');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_URLFormatoInvalido' ? 'A URL informada não possui um formato válido. Por favor, verifique a URL informada.' : mensagem);
                    }
                    else {
                        var maior = verificaMaiorCodigo();
                        items.codigo = maior + 1;
                        items.status = "novo";
                        $scope.listURL.push(items);
                        $scope.itemUrl = {};
                        var mensagem = $translate.instant('15.MENSAGEM_URLInseridaSucesso');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_URLInseridaSucesso' ? 'URL inserida com sucesso.' : mensagem);
                    }
                }

            }
            else {
                $scope.form.midia.nomeURL.submitted = true;
                $scope.form.midia.tipoURL.submitted = true;
                $scope.form.midia.nome.submitted = true;
                var mensagem = $translate.instant('15.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            }
        }

        $scope.cancelarAgencia = function () {
            $scope.showFieldsAgency = false;
            $scope.itemAgencia = {};
        }

        $scope.onSaveAgenciaReguladora = function (item) {

            $scope.isRequiredAgencia = true;
            var hasAnotherOne = false;
            $scope.itemFormAgencia = angular.copy(item);

            if ($scope.itemFormAgencia != undefined && $scope.itemFormAgencia.alternateitemidentificationid != undefined && $scope.itemFormAgencia.alternateitemidentificationagency != undefined) {

                //Editar agência
                if ($scope.itemFormAgencia.editar != undefined && $scope.itemFormAgencia.editar == 1) {
                    $scope.editAgencia($scope.itemFormAgencia);

                }
                else { //Add agência
                    //Verifica se já existe outra agência igual
                    angular.forEach($scope.listAgencias, function (val, key) {
                        if (val.alternateitemidentificationagency == $scope.itemFormAgencia.alternateitemidentificationagency) {
                            hasAnotherOne = true;
                        }
                    });

                    if (!hasAnotherOne) {
                        $scope.addAgencia($scope.itemFormAgencia);
                    }
                    else {
                        var mensagem = $translate.instant('15.MENSAGEM_AgenciaReguladoraExiste');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_AgenciaReguladoraExiste' ? 'Esta Agência Reguladora já foi informada.' : mensagem);
                        $scope.itemAgencia = {};
                        $scope.showFieldsAgency = false;
                    }
                }

            } else {
                $scope.form.base.alternateitemidentificationid.submitted = true;
                $scope.form.base.alternateitemidentificationagency.submitted = true;
                var mensagem = $translate.instant('15.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            }
        }

        $scope.addAgencia = function (item) {
            $scope.itemFormAgencia = angular.copy(item);

            var maior = verificaMaiorCodigo($scope.listAgencias);
            $scope.itemFormAgencia.codigo = maior + 1;
            $scope.itemFormAgencia.novo = 1;

            var agenciaSelecionada = $.grep($scope.agencias, function (obj) {
                return obj.codigo == item.alternateitemidentificationagency;
            });

            if (agenciaSelecionada != undefined && agenciaSelecionada.length > 0) {
                $scope.itemFormAgencia.nomeagencia = agenciaSelecionada[0].nome;
                $scope.itemFormAgencia.alternateitemidentificationagency = agenciaSelecionada[0].codigo;
            }

            $scope.listAgencias.push($scope.itemFormAgencia);
            $scope.itemAgencia = {};
            $scope.form.base.alternateitemidentificationid.submitted = false;
            $scope.form.base.alternateitemidentificationagency.submitted = false;
            $scope.showFieldsAgency = false;
        }

        $scope.editAgencia = function (item) {

            $scope.itemFormAgencia = angular.copy(item);
            angular.forEach($scope.listAgencias, function (val, key) {

                if (val.codigo == $scope.itemFormAgencia.codigo) {
                    $scope.listAgencias[key] = $scope.itemFormAgencia;
                    $scope.itemAgencia = {};
                    $scope.form.base.alternateitemidentificationid.submitted = false;
                    $scope.form.base.alternateitemidentificationagency.submitted = false;
                }
            });

            $scope.showFieldsAgency = false;
        }

        $scope.onEditAgencia = function (item) {
            $scope.itemAgencia = angular.copy(item);
            $scope.itemAgencia.editar = 1;
            $scope.showFieldsAgency = true;
        }

        $scope.newAgency = function () {
            $scope.itemAgencia = {};
            $scope.showFieldsAgency = true;
        }

        $scope.removeAgencia = function (item) {
            var mensagem = $translate.instant('15.MENSAGEM_DesejaRemover');

            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {

                for (var i in $scope.listAgencias) {
                    if ($scope.listAgencias[i].codigo == item.codigo) {

                        if ($scope.listAgencias[i].novo == 1) {
                            $scope.listAgencias.splice(i, 1);
                        } else {
                            $scope.agenciasExcluidas.push($scope.listAgencias[i]);
                            $scope.listAgencias.splice(i, 1);
                        }
                    }
                }
                $scope.itemAgencia = {};
            }, function () { }, 'Confirmação');
        }

        //Percorre a tabela provisória e retorna o maior CODIGO
        // function verificaMaiorCodigo() {
        //     var maior = 0;

        //     //Pegar o maior CODIGO da tabela
        //     angular.forEach($scope.listURL, function (val, key) {

        //         if (val.codigo > maior) {
        //             maior = val.codigo;
        //         }
        //     });

        //     return maior;
        // }

        //Percorre a tabela provisória e retorna o maior CODIGO
        function verificaMaiorCodigo(item) {
            var maior = 0;

            //Pegar o maior CODIGO da tabela
            angular.forEach(item, function (val, key) {

                if (val.codigo > maior) {
                    maior = val.codigo;
                }
            });

            return maior;
        }

        function exibirAlertaErro() {
            var mensagem = $translate.instant('15.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            $scope.form.submitted = true;
        }

        $scope.onSave = function (itemChegada, tipoFuncionalidade) {

            $scope.isRequiredAgencia = false;
            $scope.isRequiredURL = false;

            var nomeimagem = angular.element("#inputImagemProduto")[0].files;
            var possuiArquivosCadastrados = ($scope.listURL != undefined && $scope.listURL.length > 0);
            var imagemFoiSelecionada = (nomeimagem.length + $scope.imagens.length) > 0;

            $timeout(function () {
                if ($scope.form.base.$invalid) {
                    $scope.moveScrollTop();
                    $scope.irParaAba(1);
                    exibirAlertaErro();
                }
                else if ($scope.form.hierarquia.$invalid) {
                    $scope.moveScrollTop();
                    $scope.irParaAba(2);
                    exibirAlertaErro();
                }
                else if (!possuiArquivosCadastrados && !imagemFoiSelecionada) {
                    $scope.moveScrollTop();
                    $scope.irParaAba(3);
                    exibirAlertaErro();
                }
                else if ($scope.form.complementar.$invalid) {
                    $scope.moveScrollTop();
                    $scope.irParaAba(4);
                    exibirAlertaErro();
                }
                else if ($scope.form.outros.$invalid) {
                    $scope.moveScrollTop();
                    $scope.irParaAba(5);
                    exibirAlertaErro();
                }
                else {
                    var item = angular.copy(itemChegada);

                    // Tratando campos do tipo Integer
                    if (!item.minimumtradeitemlifespanfromtimeofproduction && item.minimumtradeitemlifespanfromtimeofproduction !== 0) item.minimumtradeitemlifespanfromtimeofproduction = "";
                    if (!item.quantityoflayersperpallet && item.quantityoflayersperpallet !== 0) item.quantityoflayersperpallet = "";
                    if (!item.quantityoftradeitemsperpalletlayer && item.quantityoftradeitemsperpalletlayer !== 0) item.quantityoftradeitemsperpalletlayer = "";
                    if (!item.quantityoftradeitemcontainedinacompletelayer && item.quantityoftradeitemcontainedinacompletelayer !== 0) item.quantityoftradeitemcontainedinacompletelayer = "";
                    if (!item.quantityofcompletelayerscontainedinatradeitem && item.quantityofcompletelayerscontainedinatradeitem !== 0) item.quantityofcompletelayerscontainedinatradeitem = "";
                    if (!item.orderquantitymultiple && item.orderquantitymultiple !== 0) item.orderquantitymultiple = "";
                    if (!item.orderquantityminimum && item.orderquantityminimum !== 0) item.orderquantityminimum = "";
                    if (!item.totalquantityofnextlowerleveltradeitem && item.totalquantityofnextlowerleveltradeitem !== 0) item.totalquantityofnextlowerleveltradeitem = "";
                    
                    var contadorImagens = 0;

                    //Verifica quantidade máxima de imagens
                    if ((nomeimagem.length + $scope.imagens.length) > $scope.parametroNuMaxFoto) {
                        $scope.moveScrollTop();
                        $scope.irParaAba(3);

                        var mensagem = $translate.instant('15.MENSAGEM_QuantidadeImagensIncorreta');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_QuantidadeImagensIncorreta' ?
                            'Quantidade de imagens incorreta. O número máximo de imagens para um único produto é ' + $scope.parametroNuMaxFoto + '.' : mensagem + ' ' + $scope.parametroNuMaxFoto + ".");
                        return false;
                    }

                    //Converte datas
                    var dataInicioCompare = Utils.dataAnoMesDia(item.startavailabilitydatetime);
                    var dataFimCompare = Utils.dataAnoMesDia(item.endavailabilitydatetime);
                    item.endavailabilitydatetime = Utils.dataMesDiaAno(item.endavailabilitydatetime);
                    item.startavailabilitydatetime = Utils.dataMesDiaAno(item.startavailabilitydatetime);

                    if ((item.startavailabilitydatetime != undefined && item.startavailabilitydatetime != null && item.startavailabilitydatetime != '') &&
                            (item.endavailabilitydatetime != undefined && item.endavailabilitydatetime != null && item.endavailabilitydatetime != '') && (!Utils.isValidDate(dataInicioCompare) || !Utils.isValidDate(dataFimCompare))) {

                        var mensagem = $translate.instant('15.MENSAGEM_DataInvalida');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataInvalida' ? 'Data inválida.' : mensagem);

                        return false;
                    }
                    else if ((item.startavailabilitydatetime != undefined && item.startavailabilitydatetime != null && item.startavailabilitydatetime != '') &&
                            (item.endavailabilitydatetime != undefined && item.endavailabilitydatetime != null && item.endavailabilitydatetime != '') && (dataInicioCompare > dataFimCompare)) {

                        var mensagem = $translate.instant('15.MENSAGEM_DataInicialMaiorFinal');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataInicialMaiorFinal' ? 'A data inicial não pode ser maior que a data de final.' : mensagem);

                        return false;
                    }

                    //Validar se temperatura mínima de manuseio é maior que a máxima
                    if (item.deliverytodistributioncentertemperatureminimum != undefined && item.deliverytodistributioncentertemperatureminimum != ''
                        && item.storagehandlingtemperaturemaximum != undefined && item.storagehandlingtemperaturemaximum != '') {

                        if (item.deliverytodistributioncentertemperatureminimum > item.storagehandlingtemperaturemaximum) {
                            var mensagem = $translate.instant('15.MENSAGEM_MinimaMaiorMaxima');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_MinimaMaiorMaxima' ? 'A Temperatura mínima de armazenamento/manuseio não pode ser maior do que a Temperatura máxima de armazenamento/manuseio.' : mensagem);

                            return false;
                        }
                    }

                    //Lista de URLs
                    if ($scope.listURL != undefined && $scope.listURL.length > 0) {
                        item.urls = $scope.listURL;
                    }

                    //Lista de Agências
                    if ($scope.listAgencias != undefined && $scope.listAgencias.length > 0) {
                        item.agencias = $scope.listAgencias;
                    }

                    //Agências excluidas
                    if ($scope.agenciasExcluidas != undefined && $scope.agenciasExcluidas.length > 0) {
                        item.agenciasExcluidas = $scope.agenciasExcluidas;
                    }

                    //Lista de GTINs Inferior
                    if ($scope.listGTINInferior != undefined && $scope.listGTINInferior.length > 0) {
                        item.gtinsinferior = $scope.listGTINInferior;
                    }

                    //Lista de Atributos Brick
                    if ($scope.tableAtributoBrick.data != undefined && $scope.tableAtributoBrick.data.length > 0) {

                        item.atributosbrick = [];

                        for (var i = 0; i < $scope.tableAtributoBrick.data.length; i++) {
                            item.atributosbrick.push($scope.tableAtributoBrick.data[i]);
                        }
                    }

                    item.informacoesNutricionais = angular.copy($scope.informacoesNutricionais);
                    item.alergenos = angular.copy($scope.alergenos);


                    blockUI.start();

                    //Inserção de Produto
                    if ($scope.initMode) {

                        //Se não possuir imagem selecionada, insere na base normalmente.
                        if (item != undefined && nomeimagem.length == 0) {

                            cadastrarProduto(item, tipoFuncionalidade);
                        }
                        //Se possuir imagem selecionada, faz o upload e depois insere na base.
                        else {
                            Utils.upload('inputImagemProduto', 'blobAzure', 'ProdutoBO', function (result) {

                                if (result == undefined) {
                                    return false;
                                }
                                else {

                                    //Organiza imagens
                                    if (result.length > 0) {
                                        item.imagens = result;
                                    }

                                    cadastrarProduto(item, tipoFuncionalidade);
                                }

                            });
                        }
                    }
                    //Atualização de Produto
                    else {
                        //Altera o status do produto caso o campo "Descrição" tenha sido alterado (Não aplicado para Produtos com Status do GTIN REATIVADO, REUTILIZADO
                        //ou TRANSFERIDO.
                        if ($scope.itemForm != undefined && $scope.itemCompare != undefined && $scope.itemForm.productdescription != $scope.itemCompare.productdescription
                                && $scope.itemForm.globaltradeitemnumber != undefined && $scope.itemForm.globaltradeitemnumber != ''
                                && $scope.itemCompare.codigostatusgtin != 3 && $scope.itemCompare.codigostatusgtin != 6 && $scope.itemCompare.codigostatusgtin != 7 && !$scope.restricaoCNAEmedico) {

                            blockUI.stop();

                            var mensagemstatus = '';

                            var mensagemAlteracaoProdutoReativado = $translate.instant('15.MENSAGEM_AlteracaoProdutoReativado');
                            var mensagemPara = $translate.instant('15.MENSAGEM_Para');

                            if ($scope.itemCompare.codigostatusgtin == 1 || $scope.itemCompare.codigostatusgtin == 2) {
                                var mensagem = $translate.instant('15.MENSAGEM_REATIVADO');
                                mensagemstatus = typeof mensagem === 'object' || mensagem == '15.MENSAGEM_REATIVADO' ? 'REATIVADO' : mensagem;
                            }
                            else if ($scope.itemCompare.codigostatusgtin == 4) {
                                var mensagem = $translate.instant('15.MENSAGEM_REUTILIZADO');
                                mensagemstatus = typeof mensagem === 'object' || mensagem == '15.MENSAGEM_REUTILIZADO' ? 'REUTILIZADO' : mensagem;
                            }

                            $rootScope.confirm(typeof mensagemAlteracaoProdutoReativado === 'object' || mensagemPara === 'object' ||
                                (mensagemAlteracaoProdutoReativado == '15.MENSAGEM_AlteracaoProdutoReativado' || mensagemPara == '15.MENSAGEM_Para') ?
                                'Confirma a alteração do status do produto "' + $scope.itemCompare.productdescription + '" para ' + mensagemstatus + '?' :
                                mensagemAlteracaoProdutoReativado + ' "' + $scope.itemCompare.productdescription + '" ' + mensagemPara + ' ' + mensagemstatus + '?', function () {

                                    blockUI.start();

                                    if ($scope.itemCompare.codigostatusgtin == 1 || $scope.itemCompare.codigostatusgtin == 2 || $scope.itemCompare.codigostatusgtin == 3) {
                                        $scope.itemForm.codigostatusgtin = 3;
                                        item.codigostatusgtin = 3;
                                    }
                                    else if ($scope.itemCompare.codigostatusgtin == 4) {
                                        $scope.itemForm.codigostatusgtin = 6;
                                        item.codigostatusgtin = 6;
                                    }

                                    if ($scope.itemForm != undefined && $scope.itemCompare != undefined && $scope.itemCompare.globaltradeitemnumber != undefined && $scope.itemCompare.globaltradeitemnumber != ''
                                        && ($scope.itemForm.productdescription != $scope.itemCompare.productdescription ||
                                            $scope.itemForm.codigolingua != $scope.itemCompare.codigolingua || $scope.verificaAlteracoesEixos($scope.itemCompare, $scope.itemForm))) {

                                        blockUI.stop();
                                        var mensagemDetectadasAlteracoesProduto = $translate.instant('15.MENSAGEM_DetectadasAlteracoesProduto');
                                        var mensagemDeAcordoComPadroesGS1 = $translate.instant('15.MENSAGEM_DeAcordoComPadroesGS1');

                                        $rootScope.confirm(typeof mensagemDetectadasAlteracoesProduto === 'object' || mensagemDeAcordoComPadroesGS1 === 'object' || mensagemDetectadasAlteracoesProduto == '15.MENSAGEM_DetectadasAlteracoesProduto' || mensagemDeAcordoComPadroesGS1 == '15.MENSAGEM_DeAcordoComPadroesGS1' ?
                                            'Foi(ram) detectada(s) alteração(ões) no produto "' + $scope.itemCompare.productdescription + '" que, de acordo com os padrões GS1, recomenda-se utilizar um novo GTIN. Caso confirme as alterações, existirá no mercado 2 produtos diferentes com o mesmo GTIN. Para maiores informações, consulte http://gs1.org/gtinrules/. Deseja confirmar as alterações?' :
                                            mensagemDetectadasAlteracoesProduto + ' "' + $scope.itemCompare.productdescription + '" ' + mensagemDeAcordoComPadroesGS1, function () {
                                                blockUI.start();

                                                genericService.postRequisicao('ProdutoBO', 'SalvarStatusLog', { campos: item }).then(
                                                    function (result) {
                                                        fluxoCompletoAlteracaoProduto(item, nomeimagem, tipoFuncionalidade);
                                                    },
                                                    $rootScope.TratarErro
                                                );

                                            }, function () {
                                                item.endavailabilitydatetime = Utils.dataMesDiaAno(item.endavailabilitydatetime);
                                                item.startavailabilitydatetime = Utils.dataMesDiaAno(item.startavailabilitydatetime);
                                                blockUI.stop();
                                            }, 'Confirmação');
                                    }

                                }, function () {
                                    item.endavailabilitydatetime = Utils.dataMesDiaAno(item.endavailabilitydatetime);
                                    item.startavailabilitydatetime = Utils.dataMesDiaAno(item.startavailabilitydatetime);
                                    blockUI.stop();
                                }, 'Confirmação');


                        }
                        //Se foi alterado Descrição do produto, idioma, 20% de qualquer eixo ou peso ou quantidade de itens
                        else if ($scope.itemForm != undefined && $scope.itemCompare != undefined && $scope.itemCompare.globaltradeitemnumber != undefined && $scope.itemCompare.globaltradeitemnumber != ''
                                    && ($scope.itemForm.productdescription != $scope.itemCompare.productdescription ||
                                    $scope.itemForm.codigolingua != $scope.itemCompare.codigolingua || $scope.verificaAlteracoesEixos($scope.itemCompare, $scope.itemForm))
                                    && $scope.itemForm.globaltradeitemnumber != undefined && $scope.itemForm.globaltradeitemnumber != '' ||
                                    $scope.itemForm.totalQuantityOfNextLowerLevelTradeItem != $scope.itemCompare.totalQuantityOfNextLowerLevelTradeItem
                                    && (($scope.itemForm.dataalteracao != "" && $scope.itemForm.codigotipogtin == 1) || $scope.itemForm.codigotipogtin != 1) //Primeira edição GTIN-8
                                    ) {

                            blockUI.stop();
                            var mensagemDetectadasAlteracoesProduto = $translate.instant('15.MENSAGEM_DetectadasAlteracoesProduto');
                            var mensagemDeAcordoComPadroesGS1 = $translate.instant('15.MENSAGEM_DeAcordoComPadroesGS1');

                            $rootScope.confirm(typeof mensagemDetectadasAlteracoesProduto === 'object' || mensagemDeAcordoComPadroesGS1 === 'object' || mensagemDetectadasAlteracoesProduto == '15.MENSAGEM_DetectadasAlteracoesProduto' || mensagemDeAcordoComPadroesGS1 == '15.MENSAGEM_DeAcordoComPadroesGS1' ?
                                'Foi(ram) detectada(s) alteração(ões) no produto "' + $scope.itemCompare.productdescription + '" que, de acordo com os padrões GS1, recomenda-se utilizar um novo GTIN. Caso confirme as alterações, existirá no mercado 2 produtos diferentes com o mesmo GTIN. Para maiores informações, consulte http://gs1.org/gtinrules/. Deseja confirmar as alterações?' :
                                mensagemDetectadasAlteracoesProduto + ' "' + $scope.itemCompare.productdescription + '" ' + mensagemDeAcordoComPadroesGS1, function () {

                                    blockUI.start();

                                    genericService.postRequisicao('ProdutoBO', 'SalvarStatusLog', { campos: item }).then(
                                        function (result) {
                                            fluxoCompletoAlteracaoProduto(item, nomeimagem, tipoFuncionalidade);
                                        },
                                        $rootScope.TratarErro
                                    );

                                }, function () {
                                    item.endavailabilitydatetime = Utils.dataMesDiaAno(item.endavailabilitydatetime);
                                    item.startavailabilitydatetime = Utils.dataMesDiaAno(item.startavailabilitydatetime);
                                    blockUI.stop();
                                }, 'Confirmação');

                        }
                        //Se não houve nenhuma grande mudança na estrutura do produto, conclui o fluxo normalmente
                        else {

                            genericService.postRequisicao('ProdutoBO', 'SalvarStatusLog', { campos: item }).then(
                                function (result) {

                                    if (item != undefined && nomeimagem.length == 0) {

                                        item.imagens = "";
                                        editarProduto(item, tipoFuncionalidade);
                                    }
                                    else {

                                        //Verifica se a imagem já foi upada. Se sim, não upa novamente.
                                        if (nomeimagem.length == 0) {

                                            editarProduto(item, tipoFuncionalidade);
                                        }
                                        //Se não, upa a imagem novamente.
                                        else {
                                            Utils.upload('inputImagemProduto', 'blobAzure', 'ProdutoBO', function (result) {
                                                if (result == undefined) {
                                                    return false;
                                                }
                                                else {

                                                    //Organiza imagens em lacunas vazias
                                                    if (result.length > 0) {
                                                        item.imagens = result;
                                                    }

                                                    editarProduto(item, tipoFuncionalidade);
                                                }
                                            });
                                        }
                                    }
                                },
                                $rootScope.TratarErro
                            );
                        }
                    }
                }
            });
        }

        //Cadastrar Produto
        function cadastrarProduto(item, tipoFuncionalidade) {

            item.tipoFuncionalidade = tipoFuncionalidade;

            genericService.postRequisicao('ProdutoBO', 'CadastrarProduto', { campos: item }).then(
                function (result) {
                    blockUI.stop();
                    if (result.data != undefined) {
                        if (tipoFuncionalidade == 'Salvar') {
                            var mensagem = $translate.instant('15.MENSAGEM_RegistroSalvoContinuar');

                            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_RegistroSalvoContinuar' ? 'Registro salvo com sucesso. Deseja continuar a edição dos dados desse produto?' : mensagem, function () {

                                for (var i in $scope.listGTINInferior) {
                                    $scope.listGTINInferior[i].statusinferior = '';
                                    if ($scope.listGTINInferior[i].codigoproduto != undefined && $scope.listGTINInferior[i].codigoprodutoinferior == undefined)
                                        $scope.listGTINInferior[i].codigoprodutoinferior = $scope.listGTINInferior[i].codigoproduto
                                }

                                //Itens gerados no server side
                                $scope.itemForm.codigoproduto = result.data.codigoproduto;
                                $scope.itemForm.variantelogistica = result.data.variantelogistica;
                                $scope.itemForm.coditem = result.data.coditem;
                                $scope.itemForm.nrprefixo = result.data.nrprefixo;
                                $scope.itemForm.globaltradeitemnumber = result.data.globaltradeitemnumber;

                                $scope.tableProdutos.reload();

                                $scope.itemCompare = angular.copy($scope.itemForm);
                                $scope.initMode = false;
                            }, function () {
                                retornarTelaInicial();
                            }, 'Confirmação');
                        }
                        else if (tipoFuncionalidade == 'GerarGTIN') {

                            var tipoGtin = $.grep($scope.licencas, function (obj) {
                                return obj.codigo == $scope.itemForm.codigotipogtin;
                            });

                            //Itens gerados no server side
                            $scope.itemForm.variantelogistica = result.data.variantelogistica;
                            $scope.itemForm.coditem = result.data.coditem;
                            $scope.itemForm.nrprefixo = result.data.nrprefixo;
                            $scope.itemForm.globaltradeitemnumber = result.data.globaltradeitemnumber;

                            if ($scope.itemForm.globaltradeitemnumber != undefined && $scope.itemForm.globaltradeitemnumber != null && $scope.itemForm.globaltradeitemnumber != '') {

                                var mensagemO = $translate.instant('15.MENSAGEM_O');
                                var mensagemGeradoComSucesso = $translate.instant('15.MENSAGEM_GeradoComSucesso');

                                $scope.alert(typeof mensagemO === 'object' || mensagemGeradoComSucesso === 'object' || mensagemO == '15.MENSAGEM_O' || mensagemGeradoComSucesso == '15.MENSAGEM_GeradoComSucesso' ?
                                    'O ' + tipoGtin[0].nome + ' foi gerado com sucesso: ' + $scope.itemForm.globaltradeitemnumber + '.' :
                                    mensagemO + ' ' + tipoGtin[0].nome + ' ' + mensagemGeradoComSucesso + ': ' + $scope.itemForm.globaltradeitemnumber + '.', function () {
                                        if ($scope.itemForm.codigotipogtin != 4) {
                                            confirmGeracaoGTIN14(result.data.codigoproduto);
                                        }
                                        else {
                                            retornarTelaInicial();
                                        }
                                    });
                            }
                        }
                    }
                },
                $rootScope.TratarErro
            );
        }

        //Editar Produto
        function editarProduto(item, tipoFuncionalidade) {

            item.tipoFuncionalidade = tipoFuncionalidade;

            genericService.postRequisicao('ProdutoBO', 'EditarProduto', { campos: item, where: item.codigoproduto }).then(
                function (result) {

                    blockUI.stop();
                    if (result.data != undefined) {
                        if (tipoFuncionalidade == 'Salvar') {
                            var mensagem = $translate.instant('15.MENSAGEM_RegistroSalvoContinuar');

                            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_RegistroSalvoContinuar' ? 'Registro salvo com sucesso. Deseja continuar a edição dos dados desse produto?' : mensagem, function () {
                                var state = {};

                                for (var i in $scope.listGTINInferior) {
                                    $scope.listGTINInferior[i].statusinferior = '';
                                    if ($scope.listGTINInferior[i].codigoproduto != undefined && $scope.listGTINInferior[i].codigoprodutoinferior == undefined)
                                        $scope.listGTINInferior[i].codigoprodutoinferior = $scope.listGTINInferior[i].codigoproduto
                                }

                                state.codigo = $scope.itemForm.codigostatusgtin;
                                $scope.itemCompare = angular.copy($scope.itemForm);

                                $scope.tableProdutos.reload();

                                $scope.shouldShow(state);
                            }, function () {

                                // Se for GTIN-8, pergunta se vai criar GTIN-14 dele ao salvar, pois não é gerado o GTIN dele no CNP.
                                if ($scope.itemForm.codigotipogtin === 1) {
                                    var dados = { where: {} };

                                    genericService.postRequisicao('ProdutoBO', 'VerificaExistenciaGTIN14Superior', { where: { 'codigoProduto': item.codigoproduto} }).then(
                                        function (result) {
                                            if (!result.data || result.data.length === 0) {
                                                confirmGeracaoGTIN14();
                                            } else {
                                                retornarTelaInicial();
                                            }
                                        },
                                        $rootScope.TratarErro
                                    );
                                } else {
                                    retornarTelaInicial();
                                }
                            }, 'Confirmação');
                        }
                        else if (tipoFuncionalidade == 'GerarGTIN') {

                            //Itens gerados no server side
                            $scope.itemForm.codigoproduto = result.data.codigoproduto;
                            $scope.itemForm.variantelogistica = result.data.variantelogistica;
                            $scope.itemForm.coditem = result.data.coditem;
                            $scope.itemForm.nrprefixo = result.data.nrprefixo;
                            $scope.itemForm.globaltradeitemnumber = result.data.globaltradeitemnumber;

                            if ($scope.itemForm.globaltradeitemnumber != undefined && $scope.itemForm.globaltradeitemnumber != null && $scope.itemForm.globaltradeitemnumber != '') {
                                var tipoGtin = $.grep($scope.licencas, function (obj) {
                                    return obj.codigo == $scope.itemForm.codigotipogtin;
                                });

                                var mensagemO = $translate.instant('15.MENSAGEM_O');
                                var mensagemGeradoComSucesso = $translate.instant('15.MENSAGEM_GeradoComSucesso');

                                $scope.alert(typeof mensagemO === 'object' || mensagemGeradoComSucesso === 'object' || mensagemO == '15.MENSAGEM_O' || mensagemGeradoComSucesso == '15.MENSAGEM_GeradoComSucesso' ?
                                    'O ' + tipoGtin[0].nome + ' foi gerado com sucesso: ' + $scope.itemForm.globaltradeitemnumber + '.' :
                                    mensagemO + ' ' + tipoGtin[0].nome + ' ' + mensagemGeradoComSucesso + ': ' + $scope.itemForm.globaltradeitemnumber + '.', function () {
                                        if ($scope.itemForm.codigotipogtin != 4) {
                                            confirmGeracaoGTIN14();
                                        }
                                        else {
                                            retornarTelaInicial();
                                        }
                                    });
                            }
                        }
                    }
                },
                $rootScope.TratarErro
            );
        };

        function confirmGeracaoGTIN14(codigoproduto) {
            var mensagem = $translate.instant('15.MENSAGEM_DesejaCriarCaixa');

            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DesejaCriarCaixa' ? 'Deseja criar um GTIN-14 (caixa) do produto que acabou de salvar?' : mensagem, function () {
                if (codigoproduto) $scope.itemForm.codigoproduto = codigoproduto;

                $scope.tableProdutos.reload();
                $scope.initMode = true;
                $scope.bloquearGTIN = true;
                $scope.stepCalculation = 1;

                $scope.showInformacoesNutricionais = false;
                $scope.informacoesNutricionais = {};
                $scope.showAlergenos = false;
                $scope.alergenos = [];

                $scope.aproveitaCamposGTIN14($scope.itemForm);



            }, function () {
                retornarTelaInicial();
            }, 'Confirmação');
        };

        function retornarTelaInicial() {
            $scope.initMode = false;
            $scope.itemForm = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            setFalseForms();
            resetDatepickerCss();
            $scope.limparUploadFiles();
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $scope.bloquearGTIN = false;
            $scope.stepCalculation = 1;
            $scope.tableProdutos.page(1);
            $scope.tableProdutos.reload();
            angular.element("#productdescription").removeClass("destacarDecricao");
            $scope.moveScrollTop();
            var titulo = $translate.instant('15.LBL_CadastrarProduto');
            $scope.titulo = typeof titulo === 'object' || titulo == '15.LBL_CadastrarProduto' ? 'Cadastrar Produto' : titulo
        };

        //Filtrar status
        $scope.shouldShow = function (state) {

            if ($scope.itemCompare != undefined) {

                if (!$scope.restricaoCNAEmedico) {

                    if (($scope.itemCompare.codigostatusgtin == 1 && (state.codigo == 1 || state.codigo == 2 || state.codigo == 4)) ||
                                    ($scope.itemCompare.codigostatusgtin == 2 && (state.codigo == 2 || state.codigo == 3 || state.codigo == 4)) ||
                                    ($scope.itemCompare.codigostatusgtin == 3 && (state.codigo == 3 || state.codigo == 2 || state.codigo == 4)) ||
                                    ($scope.itemCompare.codigostatusgtin == 4 && (state.codigo == 4 || state.codigo == 6)) ||
                                    ($scope.itemCompare.codigostatusgtin == 5 && (state.codigo == 5 || state.codigo == 1 || state.codigo == 2 || state.codigo == 3 || state.codigo == 4 || state.codigo == 6 || state.codigo == 7)) ||
                                    ($scope.itemCompare.codigostatusgtin == 6 && (state.codigo == 6 || state.codigo == 2 || state.codigo == 4)) ||
                                    ($scope.itemCompare.codigostatusgtin == 7 && (state.codigo == 7))) {

                        return true;
                    }
                }
                //Se o Associado for restringido a GTIN médico, os únicos status disponíveis são Ativo, Cancelado, Em elaboração e Transferido
                else if ($scope.restricaoCNAEmedico) {

                    if (($scope.itemCompare.codigostatusgtin == 1 && (state.codigo == 1 || state.codigo == 4)) ||
                                    ($scope.itemCompare.codigostatusgtin == 4 && (state.codigo == 4)) ||
                                    ($scope.itemCompare.codigostatusgtin == 5 && (state.codigo == 5 || state.codigo == 1 || state.codigo == 4 || state.codigo == 7)) ||
                                    ($scope.itemCompare.codigostatusgtin == 7 && (state.codigo == 7))) {

                        return true;
                    }
                }
            }
        }

        //Verifica se as imagens já foram upadas e chama o método de edição
        function fluxoCompletoAlteracaoProduto(item, nomeimagem, tipoFuncionalidade) {

            if (item != undefined && nomeimagem.length == 0) {

                item.nomeimagem = "";
                editarProduto(item, tipoFuncionalidade);
            }
            else {
                //Verifica se a imagem já foi upada. Se sim, não upa novamente.
                if (nomeimagem.length == 0) {

                    editarProduto(item, tipoFuncionalidade);
                }
                //Se não, upa a imagem novamente.
                else {
                    Utils.upload('inputImagemProduto', 'blobAzure', 'ProdutoBO', function (result) {
                        if (result == undefined) {
                            return false;
                        }
                        else {

                            //Organiza imagens em lacunas vazias
                            if (result.length > 0) {
                                item.imagens = result;
                            }

                            editarProduto(item, tipoFuncionalidade);
                        }
                    });
                }
            }
        }

        //Valida input de unidade de medida de temperatura
        $scope.selectSameMeasurementMaximum = function (unidade, campo) {
            if (campo != undefined && campo != null) {
                if ($scope.itemForm.deliverytodistributioncentertemperatureminimum != undefined && $scope.itemForm.deliverytodistributioncentertemperatureminimum != '')
                    $scope.itemForm.storagehandlingtempminimumuom = unidade;
            }
        }

        $scope.selectSameMeasurementMinimum = function (unidade, campo) {
            if (campo != undefined && campo != null) {
                if ($scope.itemForm.storagehandlingtemperaturemaximum != undefined && $scope.itemForm.storagehandlingtemperaturemaximum != '')
                    $scope.itemForm.storagehandlingtemperaturemaximumunitofmeasure = unidade;
            }
        }

        $scope.$watch('itemForm.deliverytodistributioncentertemperatureminimum', function (newValue, oldValue) {
            if (newValue) {
                if ($scope.itemForm.storagehandlingtemperaturemaximumunitofmeasure != undefined && $scope.itemForm.storagehandlingtemperaturemaximumunitofmeasure != '') {
                    $scope.itemForm.storagehandlingtempminimumuom = $scope.itemForm.storagehandlingtemperaturemaximumunitofmeasure;
                    return;
                }
            }
        });

        $scope.$watch('itemForm.storagehandlingtemperaturemaximum', function (newValue, oldValue) {
            if (newValue) {
                if ($scope.itemForm.storagehandlingtempminimumuom != undefined && $scope.itemForm.storagehandlingtempminimumuom != '') {
                    $scope.itemForm.storagehandlingtemperaturemaximumunitofmeasure = $scope.itemForm.storagehandlingtempminimumuom;
                    return;
                }
            }
        });

        $scope.avaliaMaximum = function (campo) {

            if (campo != undefined && campo != undefined && campo != '') {
                if (campo.toString().indexOf("-") > -1) {
                    $scope.tamanhoMaximum = 7;
                }
                else {
                    $scope.tamanhoMaximum = 6;
                }
            }
        }

        $scope.avaliaMinimum = function (campo) {

            if (campo != undefined && campo.toString().indexOf("-") > -1) {
                $scope.tamanhoMinimum = 7;
            }
            else {
                $scope.tamanhoMinimum = 6;
            }
        }

        //Verifica se houve alguma alteração de 20% em qualquer um dos eixos de um produto na edição
        $scope.verificaAlteracoesEixos = function (old, novo) {

            var objAntigo = angular.copy(old);
            var objNovo = angular.copy(novo);

            //Os campos referentes a peso são convertidos em quilo e os campos referentes à comprimento são convertidos em metro
            objNovo.height = convertToTheSameMeasurement(objNovo.height, objNovo.heightmeasurementunitcode, 'comprimento');
            objAntigo.height = convertToTheSameMeasurement(objAntigo.height, objAntigo.heightmeasurementunitcode, 'comprimento');

            objNovo.depth = convertToTheSameMeasurement(objNovo.depth, objNovo.depthmeasurementunitcode, 'comprimento');
            objAntigo.depth = convertToTheSameMeasurement(objAntigo.depth, objAntigo.depthmeasurementunitcode, 'comprimento');

            objNovo.netweight = convertToTheSameMeasurement(objNovo.netweight, objNovo.netweightmeasurementunitcode, 'peso');
            objAntigo.netweight = convertToTheSameMeasurement(objAntigo.netweight, objAntigo.netweightmeasurementunitcode, 'peso');

            objNovo.netcontent = convertToTheSameMeasurement(objNovo.netcontent, objNovo.netcontentmeasurementunitcode, 'volume');
            objAntigo.netcontent = convertToTheSameMeasurement(objAntigo.netcontent, objAntigo.netcontentmeasurementunitcode, 'volume');

            objNovo.grossweight = convertToTheSameMeasurement(objNovo.grossweight, objNovo.grossweightmeasurementunitcode, 'peso');
            objAntigo.grossweight = convertToTheSameMeasurement(objAntigo.grossweight, objAntigo.grossweightmeasurementunitcode, 'peso');

            objNovo.width = convertToTheSameMeasurement(objNovo.width, objNovo.widthmeasurementunitcode, 'comprimento');
            objAntigo.width = convertToTheSameMeasurement(objAntigo.width, objAntigo.widthmeasurementunitcode, 'comprimento');

            objNovo.deliverytodistributioncentertemperatureminimum = convertToTheSameMeasurement(objNovo.deliverytodistributioncentertemperatureminimum, objNovo.storagehandlingtempminimumuom, 'temperatura');
            objAntigo.deliverytodistributioncentertemperatureminimum = convertToTheSameMeasurement(objAntigo.deliverytodistributioncentertemperatureminimum, objAntigo.storagehandlingtempminimumuom, 'temperatura');

            objNovo.storagehandlingtemperaturemaximum = convertToTheSameMeasurement(objNovo.storagehandlingtemperaturemaximum, objNovo.storagehandlingtemperaturemaximumunitofmeasure, 'temperatura');
            objAntigo.storagehandlingtemperaturemaximum = convertToTheSameMeasurement(objAntigo.storagehandlingtemperaturemaximum, objAntigo.storagehandlingtemperaturemaximumunitofmeasure, 'temperatura');

            if (
                (((objNovo.height * 100) / objAntigo.height) < 80 || ((objNovo.height * 100) / objAntigo.height) > 120) ||
                (((objNovo.netcontent * 100) / objAntigo.netcontent) < 80 || ((objNovo.netcontent * 100) / objAntigo.netcontent) > 120) ||
                (((objNovo.depth * 100) / objAntigo.depth) < 80 || ((objNovo.depth * 100) / objAntigo.depth) > 120) ||
                (((objNovo.netweight * 100) / objAntigo.netweight) < 80 || ((objNovo.netweight * 100) / objAntigo.netweight) > 120) ||
                (((objNovo.grossweight * 100) / objAntigo.grossweight) < 80 || ((objNovo.grossweight * 100) / objAntigo.grossweight) > 120) ||
                (((objNovo.width * 100) / objAntigo.width) < 80 || ((objNovo.width * 100) / objAntigo.width) > 120) ||
                (((objNovo.deliverytodistributioncentertemperatureminimum * 100) / objAntigo.deliverytodistributioncentertemperatureminimum) < 80 || ((objNovo.wideliverytodistributioncentertemperatureminimumdth * 100) / objAntigo.deliverytodistributioncentertemperatureminimum) > 120) ||
                (((objNovo.storagehandlingtemperaturemaximum * 100) / objAntigo.storagehandlingtemperaturemaximum) < 80 || ((objNovo.storagehandlingtemperaturemaximum * 100) / objAntigo.storagehandlingtemperaturemaximum) > 120)
            ) {

                return true;
            }
            else {
                return false;
            }

        }

        //Convert tudo para o mesmo Fator de Conversão
        function convertToTheSameMeasurement(campo, medida, tipo) {

            if (tipo == "temperatura") {
                if (medida == 'FAH') {
                    return (((campo - 32) / 1.8000) + 273.15)
                }
                else if (medida == 'KEL') {
                    return campo;
                }
                else if (medida == 'CEL') {
                    return (campo + 273.15)
                }
            }
            else {
                for (var i = 0; i < $scope['unidades' + tipo].length; i++) {
                    if ($scope['unidades' + tipo][i].commoncode == medida) {
                        return $scope['unidades' + tipo][i].fatorconversao * campo;
                    }
                }
            }
        }

        //Alterar status do brick no ng-change
        $scope.alterarStatusAtributoBrick = function (item) {

            for (var i = 0; i < $scope.atributosBrickAntigos.length; i++) {
                if ($scope.atributosBrickAntigos[i].codetype == item.codetype) {
                    if (($scope.atributosBrickAntigos[i].codevalue == undefined || $scope.atributosBrickAntigos[i].codevalue == '') && (item.codevalue != undefined && item.codevalue != '')
                        && (item.status == undefined || item.status == '')) {
                        item.status = "novo";
                    }
                    else if ($scope.atributosBrickAntigos[i].codevalue != undefined && item.status != "novo" && (item.codevalue == undefined || item.codevalue == '')) {
                        item.status = "excluido";
                    }
                    else if ($scope.atributosBrickAntigos[i].codevalue != undefined && $scope.atributosBrickAntigos[i].codevalue != item.codevalue && item.status != "novo") {
                        item.status = "editado";
                    }
                    else {
                        item.status = undefined;
                    }
                }
            }

        }

        //Gera XML do Produto selecionado
        $scope.onXMLGenerator = function (item) {
            if (item != undefined && item != null) {
                $("#xmldata").val(item.codigoproduto);
                $('#XmlHandler').submit();
            }
        }

        //Remove Produto Cadastrado
        $scope.onDelete = function (item) {
            $rootScope.verificaPermissao('ProdutoBO', 'RemoverProduto', function () {

                var mensagem = $translate.instant('15.MENSAGEM_DesejaRemover');

                $rootScope.confirm(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {
                    genericService.postRequisicao('ProdutoBO', 'RemoverProduto', { campos: item }).then(
                        function (result) {
                            setFalseForms();
                            $scope.tableProdutos.page(1);
                            $scope.tableProdutos.reload();
                            $scope.itemForm = {};

                            var mensagem = $translate.instant('15.MENSAGEM_DadosRemovidosSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DadosRemovidosSucesso' ? 'Dados removidos com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );

                }, function () { }, 'Confirmação');
            });
        }

        //Chamado ao selecionar um segmento
        $scope.onSelectSegment = function (segmento) {
            return $q(function (resolve, reject) {
                if (segmento != undefined && segmento != null && segmento != '') {
                    genericService.postRequisicao('GPCBO', 'buscarFamilia', { campos: { "codigosegmento": segmento} }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.familias = result.data;
                                $scope.classes = {};
                                $scope.bricks = {};
                                $scope.showInformacoesNutricionais = false;
                                $scope.showAlergenos = false;
                                resolve(result.data);
                            }
                        },
                        $rootScope.TratarErro
                    )
                }
            });
        }

        //Chamado ao selecionar um família
        $scope.onSelectFamily = function (familia) {
            return $q(function (resolve, reject) {
                if (familia != undefined && familia != null && familia != '') {
                    genericService.postRequisicao('GPCBO', 'buscarClasse', { campos: { "codigofamilia": familia} }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.classes = result.data;
                                $scope.bricks = {};
                                $scope.showInformacoesNutricionais = false;
                                $scope.showAlergenos = false;
                                resolve(result.data);
                            }
                        },
                        $rootScope.TratarErro
                    );
                }
            });
        }

        //Chamado ao selecionar um classe
        $scope.onSelectClass = function (classe) {
            return $q(function (resolve, reject) {
                if (classe != undefined && classe != null && classe != '') {
                    genericService.postRequisicao('GPCBO', 'buscarBrick', { campos: { "codigoclasse": classe} }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.bricks = result.data;
                                $scope.showInformacoesNutricionais = false;
                                $scope.showAlergenos = false;
                                resolve(result.data);
                            }
                        },
                        $rootScope.TratarErro
                    );
                }
            });
        };

        //Chamado ao selecionar um brick
        $scope.onSelectBrick = function () {

            if ($scope.itemForm && $scope.itemForm.codebrick) {
                genericService.postRequisicao('ProdutoBO', 'VerificaBrickInformacaoNutricional', { 'where': { 'brick': $scope.itemForm.codebrick} }).then(
                    function (result) {
                        var show = result.data[0] && result.data[0].count > 0;
                        $scope.showInformacoesNutricionais = show;
                        $scope.showAlergenos = show;
                    },
                    $rootScope.TratarErro
                );
            }

            return $q(function (resolve, reject) {
                $scope.tableAtributoBrick.reload();
                $scope.tableAtributoBrick.page(1);

                if ($scope.itemForm.codebrick != undefined && $scope.itemForm.codeclass != undefined && $scope.itemForm.codefamily != undefined && $scope.itemForm.codesegment != undefined) {
                    genericService.postRequisicao('ProdutoBO', 'BuscarDropdownAtributoBrick', { campos: $scope.itemForm }).then(
                        function (result) {
                            $scope.attributeValues = result.data;
                            resolve(result.data);
                        },
                        $rootScope.TratarErro
                    );
                }

            });
        }

        //Chamado ao selecionar um brick
        $scope.updateProductDescription = function (result) {
            if ($scope.itemForm.productdescription == undefined || $scope.itemForm.productdescription == null || $scope.itemForm.productdescription == '') {
                var filtro = result;

                function filterByID(obj) {
                    return obj.codigo == filtro;
                }

                if ($scope.bricks != undefined && $scope.bricks != null) {
                    var arrByID = $scope.bricks.filter(filterByID);
                    $scope.itemForm.productdescription = arrByID[0].nome;
                }
                angular.element("#productdescription").addClass("destacarDecricao");
            }
        }

        //Chamado ao selecionar um país
        $scope.onSelectedCountry = function (pais) {
            return $q(function (resolve, reject) {
                if (pais != undefined && pais != null && pais != '') {
                    genericService.postRequisicao('CEPBO', 'consultarUFByCode', { campos: { "pais": pais} }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.estados = result.data;
                                resolve(result.data);
                            }
                        },
                        $rootScope.TratarErro
                    );
                }
                else {
                    $scope.estados = "";
                }
            });
        }

        //Carrega produtos cadastrados 
        $scope.BuscarProdutosAutoComplete = function (nome) {
            return genericService.postRequisicao('ProdutoBO', 'BuscarProdutosAutoComplete', { where: { nome: nome} }).then(
                function (result) {
                    var produtosGTINOrigem = [];
                    angular.forEach(result.data, function (item) {
                        produtosGTINOrigem.push({ codigo: item.codigoproduto, nome: item.productdescription });
                    });
                    return produtosGTINOrigem;
                }
            );
        }

        function resetDatepickerCss() {
            $(".datepicker-days .day").removeClass("active");
        }

        //Carregar todos os parâmetros no load da página
        function carregarParametros() {
            $scope.parametroURLFoto = $rootScope.retornaValorParametroByFiltro('midia.nuurl.foto');
            $scope.parametroURLLinkeddata = $rootScope.retornaValorParametroByFiltro('midia.nuurl.linkeddata');
            $scope.parametroURLProduto = $rootScope.retornaValorParametroByFiltro('midia.nuurl.produto');
            $scope.parametroURLReserva = $rootScope.retornaValorParametroByFiltro('midia.nuurl.reserva');
            $scope.parametroURLYoutube = $rootScope.retornaValorParametroByFiltro('midia.nuurl.youtube');
            $scope.parametroNuMaxFoto = $rootScope.retornaValorParametroByFiltro('midia.numax.foto');
            DEFAULT_IMPORT_CLASSIFICATION_TYPE = $rootScope.retornaValorParametroByFiltro('produto.default.nucod.classificacao.comercio.ext');
        }

        //Gerar Caixa automaticamente a partir do Produto anterior
        $scope.aproveitaCamposGTIN14 = function (item) {

            var item = item;
            $scope.itemForm = {};
            setFalseForms();
            $scope.moveScrollTop();

            $scope.escondeCamposGTIN14 = true;

            //GTIN Origem
            $scope.itemForm.codigoprodutoorigem = item.codigoproduto;
            $scope.itemForm.codigogtinorigem = item.globaltradeitemnumber;
            $scope.itemForm.nomeprodutoorigem = item.productdescription;

            //Valores Default
            $scope.itemForm.heightmeasurementunitcode = 'CMT';
            $scope.itemForm.depthmeasurementunitcode = 'CMT';
            $scope.itemForm.widthmeasurementunitcode = 'CMT';
            $scope.itemForm.netcontentmeasurementunitcode = 'MAL';
            $scope.itemForm.netweightmeasurementunitcode = 'GRM';
            $scope.itemForm.grossweightmeasurementunitcode = 'GRM';
            $scope.itemForm.codigotipogtin = 4;

            //Valor inicial do status como 'Em elaboração'
            $scope.itemForm.codigostatusgtin = 5;

            // Padrão GDSN
            $scope.itemForm.indicadorgdsn = item.indicadorgdsn;
            $scope.onChangeType($scope.itemForm.indicadorgdsn);

            $scope.itemForm.productdescription = item.productdescription;

            $scope.itemForm.informationprovider = item.informationprovider;
            $scope.itemForm.startavailabilitydatetime = item.startavailabilitydatetime;
            $scope.itemForm.endavailabilitydatetime = item.endavailabilitydatetime;

            $scope.tipoprodutoselecionado = undefined;

            $scope.itemCompare = $scope.itemForm;
        }

        //Gerar Caixa automaticamente a partir do Produto anterior
        $scope.aproveitaCamposGTIN14DeOrigem = function (item) {

            var item = item;
            $scope.escondeCamposGTIN14 = true;

            //GTIN Origem
            $scope.itemForm.codigoprodutoorigem = item.codigoproduto;
            $scope.itemForm.codigogtinorigem = item.globaltradeitemnumber;
            $scope.itemForm.nomeprodutoorigem = item.productdescription;
        }

        $scope.filtraGTIN8 = function (licenca) {
            return $scope.itemForm.globaltradeitemnumber || licenca.codigo != 1;
        }

        $scope.showHideTipoProduto = function (tipoproduto) {

            if ($scope.tiposProdutos != undefined) {
                $scope.tipoprodutoselecionado = $.grep($scope.tiposProdutos, function (obj) {
                    return obj.codigo == tipoproduto;
                });

                if ($scope.tipoprodutoselecionado != undefined && $scope.tipoprodutoselecionado.length > 0 && $scope.tipoprodutoselecionado[0].codigotradeitemunitdescriptorcodes == 2)
                    $scope.showPalletDependency = true;
                else
                    $scope.showPalletDependency = false;
            }
        }

        $scope.verifyPaginationAgency = function () {
            if (angular.element('#table-agencias .ng-table-pager ul').is(':hidden')) {
                angular.element('#table-agencias .ng-table-pager').css("display", "none");
            }
            else {
                angular.element('#table-agencias .ng-table-pager').css("display", "");
            }
        }

        //Carregamento inicial
        $scope.onLoad = function () {

            $rootScope.verificaAssociadoSelecionado(function (data) {

                if (data != undefined && data != null && data.codigo != undefined && data.codigo != null) {

                    if (data.indicadorcnaerestritivo == 1) {
                        $scope.restricaoCNAEmedico = true;
                    }
                    else {
                        $scope.restricaoCNAEmedico = false;
                    }

                    //Tipos de Produto
                    genericService.postRequisicao('TipoProdutoBO', 'BuscarTipoProdutoAtivo', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.tiposProdutos = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Tipo GTIN
                    genericService.postRequisicao('LicencaBO', 'buscarTipoGtinAssociado', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.licencas = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Agencias Reguladoras
                    genericService.postRequisicao('AgenciasReguladorasBO', 'BuscarAgenciasReguladorasAtivas', {}).then(
                        function (result) {
                            if (result.data) {
                                $scope.agencias = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Segmentos
                    genericService.postRequisicao('GPCBO', 'buscarSegmentos', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.segmentos = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //GLN's
                    genericService.postRequisicao('LocalizacoesFisicasBO', 'BuscarGLNAssociado', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.glns = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Mercado Alvo e Países
                    genericService.postRequisicao('ISOBO', 'consultarPaisesISO', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.paises = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Língua
                    genericService.postRequisicao('LinguaBO', 'BuscarLinguagensAtivas', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.linguas = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Status GTIN
                    genericService.postRequisicao('StatusGTINBO', 'BuscarStatusGTINAtivos', { campos: data }).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.statusgtin = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Unidades de Medida de Volume
                    genericService.postRequisicao('UnidadesMedidaBO', 'BuscarMedidasVolume', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.unidadesvolume = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Unidades de Medida de Peso
                    genericService.postRequisicao('UnidadesMedidaBO', 'BuscarMedidasPeso', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.unidadespeso = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Unidades de Medida de Comprimento
                    genericService.postRequisicao('UnidadesMedidaBO', 'BuscarMedidasComprimento', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.unidadescomprimento = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Unidades de Medida de Temperatura
                    genericService.postRequisicao('UnidadesMedidaBO', 'BuscarMedidasTemperatura', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.unidadestemperatura = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Tipo URL
                    genericService.postRequisicao('TipoURLBO', 'BuscarTipoURLAtivos', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.tiposurl = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Unidade de Medida do Pedido
                    genericService.postRequisicao('UnidadesMedidaBO', 'BuscarMedidasPedido', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.orders = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Packaging Type Code
                    genericService.postRequisicao('TypeCodeBO', 'BuscarPackagingTypeCode', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.packagingtypecodes = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Pallet Type Value
                    genericService.postRequisicao('TypeCodeBO', 'BuscarPalletTypeCode', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.pallettypecodes = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    //Prefixos
                    genericService.postRequisicao('ProdutoBO', 'CarregarDropdownPrefixo', {}).then(
                        function (result) {
                            if (result.data != undefined && result.data != null) {
                                $scope.prefixos = result.data;
                            }
                        },
                        $rootScope.TratarErro
                    );

                    carregarParametros();

                    $scope.moveScrollTop();
                } else {
                    $rootScope.flagAssociadoSelecionado = false;
                    blockUI.stop();
                    angular.element("#typea").focus();
                }
            });
        }

        $scope.onLoad();

        // --------------------------------------------------------------------------------------------------------------------------
        //                                                   MODAL INBAR
        // --------------------------------------------------------------------------------------------------------------------------

        //Método para abertura do modal de InBar
        $scope.onPreviewInbar = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalImage',
                controller: ModalInbarCtrl,
                resolve: {
                    item: function () {
                        var inbar = angular.copy(item);
                        return inbar;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.produtos.reload();

            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        }

        // --------------------------------------------------------------------------------------------------------------------------
        //                                                   MODAL GTIN ORIGEM
        // --------------------------------------------------------------------------------------------------------------------------

        //Método para abertura do modal de GTIN Origem
        $scope.onGtinOrigem = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalGtinOrigem',
                controller: ModalGtinOrigemCtrl,
                resolve: {
                    item: function () {
                        var gtinorigem = angular.copy(item);
                        return gtinorigem;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                if (data.codigoproduto) {
                    genericService.postRequisicao('ProdutoBO', 'PesquisaProdutos', { "campos": { "codigoproduto": data.codigoproduto.toString() }, paginaAtual: 1, registroPorPagina: 10, ordenacao: '' }).then(
                        function (result) {
                            var item = result.data[0];
                            $scope.aproveitaCamposGTIN14DeOrigem(item);
                        },
                        $rootScope.TratarErro
                    );
                }

            }, function () { });
        }

        // --------------------------------------------------------------------------------------------------------------------------
        //                                                   MODAL GTIN INFERIOR
        // --------------------------------------------------------------------------------------------------------------------------

        //Método para abertura do modal de GTIN Origem
        $scope.onGtinInferior = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalGtinInferior',
                controller: ModalGtinInferiorCtrl,
                size: 'lg',
                resolve: {
                    item: function () {
                        var gtininferior = angular.copy(item);
                        return gtininferior;
                    },
                    listGTINInferior: function () {
                        return $scope.listGTINInferior;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $scope.listGTINInferior = data;

            }, function () { });
        }

        // --------------------------------------------------------------------------------------------------------------------------
        //                                                   MODAL NCM
        // --------------------------------------------------------------------------------------------------------------------------

        $scope.onNCM = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalNcm',
                controller: ModalNcmCtrl,
                resolve: {
                    item: function () {
                        var ncm = angular.copy(item);
                        return ncm;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $scope.itemForm.importclassificationvalue = data.ncm;
                $scope.itemForm.ncmdescricao = data.descricao;
                $scope.itemForm.importclassificationtype = DEFAULT_IMPORT_CLASSIFICATION_TYPE; //Valor fixo

            }, function () { });
        }

        // --------------------------------------------------------------------------------------------------------------------------
        //                                                   MODAL CEST
        // --------------------------------------------------------------------------------------------------------------------------

        $scope.onCEST = function (item) {
            var modalInstance = $modal.open({
                templateUrl: 'modalCest',
                controller: ModalCestCtrl,
                resolve: {
                    item: function () {
                        var cest = angular.copy(item);
                        return cest;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $scope.itemForm.codigocest = data.codigocest;
                $scope.itemForm.cestnumber = data.cest;
                $scope.itemForm.cestdescricao = data.descricao_cest;

            }, function () { });
        }

        // --------------------------------------------------------------------------------------------------------------------------
        //                                                   GTIN-8
        // --------------------------------------------------------------------------------------------------------------------------

        $scope.onGTIN8 = function () {
            var modalInstance = $modal.open({
                templateUrl: 'modalGtin8',
                controller: ModalGtin8Ctrl
            });

            modalInstance.result.then(function (data) {
                $scope.itemForm.globaltradeitemnumber = data.numeroprefixo;

            }, function () { });
        }

    } ]);

//Controller para métodos do modal
var ModalInbarCtrl = function ($scope, $rootScope, $modalInstance, item) {

    $scope.itemForm = item;

    $rootScope.buscarImagensProdutoPreview(item, function (data) {

        $scope.selected = {
            itemForm: angular.copy(item),
            imagens: data
        }
    });

    //Cancelar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }
}

//Controller para métodos do modal
var ModalGtinOrigemCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, genericService, item, Utils, $translate) {

    $scope.itemGTIN = {};
    $scope.initModal = true;

    $scope.tableGTINPesquisados = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if (!$scope.initModal) {
                var dados = carregaGridGTIN($scope.itemGTIN, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        }
    });

    //Busca GTIN Origem
    function carregaGridGTIN(item, callback) {
        var dados = null;

        dados = angular.copy(item);

        genericService.postRequisicao('ProdutoBO', 'PesquisaGTINOrigem', { campos: dados }).then(
            function (result) {

                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
    }

    //Chamado ao selecionar a opção "Pesquisar"
    $scope.onSearchGTINOrigem = function (item) {

        if ((item.globaltradeitemnumber == undefined || item.globaltradeitemnumber == null || item.globaltradeitemnumber == '') &&
            (item.productdescription == undefined || item.productdescription == null || item.productdescription == '') &&
            (item.startavailabilitydatetime == undefined || item.startavailabilitydatetime == null || item.startavailabilitydatetime == '') &&
            (item.endavailabilitydatetime == undefined || item.endavailabilitydatetime == null || item.endavailabilitydatetime == '')) {

            var mensagem = $translate.instant('15.MENSAGEM_PreenchaUmDosCampos');
            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_PreenchaUmDosCampos' ? 'Por favor, preencha um dos campos para realizar a pesquisa.' : mensagem);
        }
        else {
            if ((item.startavailabilitydatetime != undefined && item.startavailabilitydatetime != null && item.startavailabilitydatetime != '') &&
                (item.endavailabilitydatetime == undefined || item.endavailabilitydatetime == null || item.endavailabilitydatetime == '')) {

                var mensagem = $translate.instant('15.MENSAGEM_DataFinalRequerida');
                $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataFinalRequerida' ? 'Data Final requerida.' : mensagem);
            }
            else if ((item.startavailabilitydatetime == undefined || item.startavailabilitydatetime == null || item.startavailabilitydatetime == '') &&
                    (item.endavailabilitydatetime != undefined && item.endavailabilitydatetime != null && item.endavailabilitydatetime != '')) {

                var mensagem = $translate.instant('15.MENSAGEM_DataInicialRequerida');
                $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataInicialRequerida' ? 'Data Inicial requerida.' : mensagem);
            }
            else {

                var dataInicioCompare = Utils.dataAnoMesDia(item.startavailabilitydatetime);
                var dataFimCompare = Utils.dataAnoMesDia(item.endavailabilitydatetime);

                if ((item.startavailabilitydatetime != undefined && item.startavailabilitydatetime != null && item.startavailabilitydatetime != '') &&
                    (item.endavailabilitydatetime != undefined && item.endavailabilitydatetime != null && item.endavailabilitydatetime != '') && (!Utils.isValidDate(dataInicioCompare) || !Utils.isValidDate(dataFimCompare))) {

                    var mensagem = $translate.instant('15.MENSAGEM_DataInvalida');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataInvalida' ? 'Data inválida.' : mensagem);
                }
                else if ((item.startavailabilitydatetime != undefined && item.startavailabilitydatetime != null && item.startavailabilitydatetime != '') &&
                    (item.endavailabilitydatetime != undefined && item.endavailabilitydatetime != null && item.endavailabilitydatetime != '') && (dataInicioCompare > dataFimCompare)) {

                    var mensagem = $translate.instant('15.MENSAGEM_DataInicialMaiorFinal');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataInicialMaiorFinal' ? 'A data inicial não pode ser maior que a data de final.' : mensagem);
                }
                else {
                    $scope.initModal = false;
                    $scope.itemGTIN = angular.copy(item);
                    $scope.tableGTINPesquisados.page(1);
                    $scope.tableGTINPesquisados.reload();
                }
            }
        }
    }

    //Chamado ao selecionar um GTIN no grid
    $scope.onSelectedGTINOrigem = function (item) {
        $modalInstance.close(item);
    }

    //Cancelar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }

}

//Controller para métodos do modal
var ModalGtinInferiorCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, genericService, item, Utils, listGTINInferior, $translate) {

    $scope.itemGTIN = {};
    $scope.itemGTIN.codigoprodutosuperior = item.codigoproduto;
    $scope.listGTINInferior = [];
    $scope.listGTINInferior = angular.copy(listGTINInferior);
    $scope.initModal = true;
    $scope.GTIN = angular.copy(item);

    $scope.tableGTINPesquisados = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if (!$scope.initModal) {
                var dados = carregaGridGTIN($scope.itemGTIN, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        }
    });

    $scope.tableGTINSelecionados = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0
    });

    //Busca GTIN Inferior
    function carregaGridGTIN(item, callback) {
        var dados = null;

        dados = angular.copy(item);

        genericService.postRequisicao('ProdutoBO', 'PesquisaGTINInferior', { campos: dados }).then(
            function (result) {

                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
    }

    //Chamado ao selecionar a opção "Pesquisar"
    $scope.onSearchGTINInferior = function (item) {
        if ((item.globaltradeitemnumber == undefined || item.globaltradeitemnumber == null || item.globaltradeitemnumber == '') &&
            (item.productdescription == undefined || item.productdescription == null || item.productdescription == '') &&
            (item.startavailabilitydatetime == undefined || item.startavailabilitydatetime == null || item.startavailabilitydatetime == '') &&
            (item.endavailabilitydatetime == undefined || item.endavailabilitydatetime == null || item.endavailabilitydatetime == '')) {

            var mensagem = $translate.instant('15.MENSAGEM_PreenchaUmDosCampos');
            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_PreenchaUmDosCampos' ? 'Por favor, preencha um dos campos para realizar a pesquisa.' : mensagem);
        }
        else {
            if ((item.startavailabilitydatetime != undefined && item.startavailabilitydatetime != null && item.startavailabilitydatetime != '') &&
                (item.endavailabilitydatetime == undefined || item.endavailabilitydatetime == null || item.endavailabilitydatetime == '')) {
                var mensagem = $translate.instant('15.MENSAGEM_DataFinalRequerida');
                $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataFinalRequerida' ? 'Data Final requerida.' : mensagem);
            }
            else if ((item.startavailabilitydatetime == undefined || item.startavailabilitydatetime == null || item.startavailabilitydatetime == '') &&
                    (item.endavailabilitydatetime != undefined && item.endavailabilitydatetime != null && item.endavailabilitydatetime != '')) {
                var mensagem = $translate.instant('15.MENSAGEM_DataInicialRequerida');
                $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataInicialRequerida' ? 'Data Inicial requerida.' : mensagem);
            }
            else {
                var dataInicioCompare = Utils.dataAnoMesDia(item.startavailabilitydatetime);
                var dataFimCompare = Utils.dataAnoMesDia(item.endavailabilitydatetime);

                if ((item.startavailabilitydatetime != undefined && item.startavailabilitydatetime != null && item.startavailabilitydatetime != '') &&
                    (item.endavailabilitydatetime != undefined && item.endavailabilitydatetime != null && item.endavailabilitydatetime != '') && (!Utils.isValidDate(dataInicioCompare) || !Utils.isValidDate(dataFimCompare))) {
                    
                    var mensagem = $translate.instant('15.MENSAGEM_DataInvalida');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataInvalida' ? 'Data inválida.' : mensagem);
                }
                else if ((item.startavailabilitydatetime != undefined && item.startavailabilitydatetime != null && item.startavailabilitydatetime != '') &&
                    (item.endavailabilitydatetime != undefined && item.endavailabilitydatetime != null && item.endavailabilitydatetime != '') && (dataInicioCompare > dataFimCompare)) {

                    var mensagem = $translate.instant('15.MENSAGEM_DataInicialMaiorFinal');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_DataInicialMaiorFinal' ? 'A data inicial não pode ser maior que a data de final.' : mensagem);
                }
                else {
                    $scope.initModal = false;
                    $scope.itemGTIN = angular.copy(item);
                    $scope.itemGTIN.codigoprodutosuperior = $scope.GTIN.codigoproduto;
                    $scope.tableGTINPesquisados.page(1);
                    $scope.tableGTINPesquisados.reload();
                }
            }
        }
    }

    //Chamado ao selecionar um GTIN no grid
    $scope.onSelectedGTINInferior = function (item) {
        var hasAnotherOne = false;
        var contador = 0;

        angular.forEach($scope.listGTINInferior, function (val, key) {
            if (val.codigoproduto == item.codigoproduto) {
                if (val.statusinferior == "novo")
                    hasAnotherOne = true;
            }
            else if (val.codigoprodutoinferior == item.codigoproduto) {
                hasAnotherOne = true;
            }
        });

        if (!hasAnotherOne) {
            if (item.statusinferior == "excluido") {
                item.statusinferior = "novo";
            }
            else {
                item.statusinferior = "novo";
                item.codigoprodutosuperior = $scope.GTIN.codigoproduto;
                $scope.listGTINInferior.push(item);
            }
        }
    }

    //Remover GTIN do grid dos selecionados
    $scope.onDeleteGTINInferior = function (item) {
        for (var i in $scope.listGTINInferior) {
            if ($scope.listGTINInferior[i].codigoproduto == item.codigoproduto) {

                if (item.statusinferior == "novo") {
                    $scope.listGTINInferior.splice(i, 1);
                }
                else {
                    item.statusinferior = "excluido";
                }
            }
        }
    }

    //Salvar 
    $scope.save = function () {
        var canClose = true;

        for (var i = 0; i < $scope.listGTINInferior.length; i++) {
            if ($scope.listGTINInferior[i].quantidade == undefined || $scope.listGTINInferior[i].quantidade == null || $scope.listGTINInferior[i].quantidade == ''
                && $scope.listGTINInferior[i].quantidade > 0) {
                canClose = false;
            }
        }

        if (canClose) {
            $modalInstance.close($scope.listGTINInferior);
        }
        else {
            var mensagem = $translate.instant('15.MENSAGEM_QuantidadeObrigatoria');
            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_QuantidadeObrigatoria' ? 'Campo quantidade obrigatório. Preencha para prosseguir.' : mensagem);
        }
    }

    //Fechar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }
}
//Controller para métodos do modal
var ModalGtin8Ctrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, genericService, Utils, $translate) {

    $scope.item8 = {};

    $scope.tableGTIN8 = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            var dados = carregaGrid($scope.item8, function (data) {
                params.total(data.length);
                var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
            });
        }
    });

    //Busca GTIN-8
    function carregaGrid(item, callback) {
        var dados = null;

        dados = angular.copy(item);

        genericService.postRequisicao('ProdutoBO', 'BuscarGTIN8NaoUsado', {}).then(
            function (result) {

                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
    }

    //Chamado ao selecionar um GTIN 8 no GRID
    $scope.onSelected8 = function (item) {
        $modalInstance.close(item);
    }

    //Cancelar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }
}

//Controller para métodos do modal
var ModalNcmCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, genericService, item, Utils, $translate) {

    $scope.itemNCM = {};
    $scope.initModal = true;

    $scope.tableNCMPesquisados = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
            LOGIN: 'asc'
        }
    }, {
        counts: [],
        total: 0,
        getData: function ($defer, params) {
            if (!$scope.initModal) {
                var dados = carregaGridNCM($scope.itemNCM, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        }
    });

    //Busca NCM
    function carregaGridNCM(item, callback) {
        var dados = null;

        dados = angular.copy(item);

        genericService.postRequisicao('NCMBO', 'PesquisaNCMAtivos', { campos: dados }).then(
            function (result) {

                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
    }

    //Chamado ao selecionar a opção "Pesquisar"
    $scope.onSearchNCM = function (item) {

        if ((item.ncm == undefined || item.ncm == null || item.ncm == '') &&
            (item.descricao == undefined || item.descricao == null || item.descricao == '')) {

            var mensagem = $translate.instant('15.MENSAGEM_PreenchaUmDosCampos');
            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_PreenchaUmDosCampos' ? 'Por favor, preencha um dos campos para realizar a pesquisa.' : mensagem);
        }
        else {
            $scope.initModal = false;
            $scope.itemNCM = angular.copy(item);
            $scope.tableNCMPesquisados.page(1);
            $scope.tableNCMPesquisados.reload();
        }
    }

    //Chamado ao selecionar um NCM no grid
    $scope.onSelectedNCM = function (item) {
        $modalInstance.close(item);
    }

    //Cancelar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }

}

var ModalCestCtrl = function ($scope, $http, $rootScope, $modalInstance, ngTableParams, blockUI, $filter, genericService, item, Utils, $translate) {

    $scope.itemCEST = {};
    $scope.initModal = true;

    $scope.tableCESTPesquisados = new ngTableParams({
        page: 1,
        count: 5,
        sorting: {
            LOGIN: 'asc'
        }
    }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                if (!$scope.initModal) {
                    var dados = carregaGridCEST($scope.itemCEST, function (data) {
                        params.total(data.length);
                        var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    });
                }
            }
        });

    //Busca CEST
    function carregaGridCEST(item, callback) {
        var dados = null;

        dados = angular.copy(item);

        genericService.postRequisicao('CESTBO', 'PesquisaCEST', { campos: dados }).then(
            function (result) {

                if (callback != undefined && callback != null) {
                    callback(result.data);
                }
            },
            $rootScope.TratarErro
        );
    }

    //Chamado ao selecionar a opção "Pesquisar"
    $scope.onSearchCEST = function (item) {

        if ((item.cest == undefined || item.cest == null || item.cest == '') &&
            (item.descricao_cest == undefined || item.descricao_cest == null || item.descricao_cest == '')) {

            var mensagem = $translate.instant('15.MENSAGEM_PreenchaUmDosCampos');
            $scope.alert(typeof mensagem === 'object' || mensagem == '15.MENSAGEM_PreenchaUmDosCampos' ? 'Por favor, preencha um dos campos para realizar a pesquisa.' : mensagem);
        }
        else {
            $scope.initModal = false;
            $scope.itemCEST = angular.copy(item);
            $scope.tableCESTPesquisados.page(1);
            $scope.tableCESTPesquisados.reload();
        }
    }

    //Chamado ao selecionar um CEST no grid
    $scope.onSelectedCEST = function (item) {
        $modalInstance.close(item);
    }

    //Cancelar 
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }

}


