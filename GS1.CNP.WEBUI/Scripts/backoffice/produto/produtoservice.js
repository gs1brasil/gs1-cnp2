﻿app.factory('ProdutoService', ['$q', '$http', '$rootScope', function ($q, $http, $rootScope) {
    var url = 'requisicao.ashx';

    return {
        BuscarProduto: function (params, callback) {
            var dados = { where: params };

            $http.post(url + '/ProdutoBO/BuscarProduto', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        carregaGridEspecificacoes: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/ProdutoBO/carregaGridEspecificacoes', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarProdutosPesquisa: function (item, tags, callback) {

            if (item.nome == null || item.nome == undefined) {
                item.nome = '';
            }
            if (item.descricacao == null || item.descricacao == undefined) {
                item.descricacao = '';
            }
            if (item.codigoprodutotipo == null || item.codigoprodutotipo == undefined) {
                item.codigoprodutotipo = '';
            }
            if (item.codigostatuspublicacao == null || item.codigostatuspublicacao == undefined) {
                item.codigostatuspublicacao = '';
            }
            if (item.disponibilidade == null || item.disponibilidade == undefined) {
                item.disponibilidade = '';
            }
            if (item.aplicacao == null || item.aplicacao == undefined) {
                item.aplicacao = '';
            }
            if (item.modelo == null || item.modelo == undefined) {
                item.modelo = '';
            }
            if (item.fabricante == null || item.fabricante == undefined) {
                item.fabricante = '';
            }
            if (item.situacao == null || item.situacao == undefined) {
                item.situacao = '';
            }
            if (item.codigoprodutofornecedor == null || item.codigoprodutofornecedor == undefined) {
                item.codigoprodutofornecedor = '';
            }
            if (item.gtin == null || item.gtin == undefined) {
                item.gtin = '';
            }
            if (item.publicadocit == null || item.publicadocit == undefined) {
                item.publicadocit = '';
            }
            if (item.marca == null || item.marca == undefined) {
                item.marca = '';
            }

            //{ nome: itemCategoria.nome, status: itemCategoria.status, descricao: itemCategoria.descricao, codigocategoriasuperior: itemCategoria.codigocategoriasuperior}

            $http.post(url + '/ProdutoBO/CarregarProdutosPesquisa', { where: { nome: item.nome, descricao: item.descricao, codigoprodutotipo: item.codigoprodutotipo, codigostatuspublicacao: item.codigostatuspublicacao, disponibilidade: item.disponibilidade, aplicacao: item.aplicacao, modelo: item.modelo, fabricante: item.fabricante, situacao: item.situacao, tags: tags, codigoprodutofornecedor: item.codigoprodutofornecedor, marca: item.marca, gtin: item.gtin, publicadocit: item.publicadocit} }).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarImagens: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/ProdutoBO/CarregarImagens', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        ReordenarImagens: function (params1, params2, params3, callback) {

            var dados = { where: { codigoproduto: params3, ord1: params1.ordem, src1: params1.src, ord2: params2.ordem, src2: params2.src} };

            $http.post(url + '/ProdutoBO/ReordenarImagens', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarParametros: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/ProdutoBO/CarregarParametros', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        PublicarProduto: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/ProdutoBO/PublicarProduto', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarCategorias: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/ProdutoBO/CarregarCategorias', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CadastrarEspecificacaoTecnica: function (params, callback) {

            var dados = { where: { nome: params.nome, valor: params.valor, status: 1, codigoproduto: params.codigoproduto} };

            $http.post(url + '/ProdutoBO/CadastrarEspecificacaoTecnica', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        VerificaProdutoPublicado: function (params, callback) {

            var dados = { where: { codigo: params} };

            $http.post(url + '/ProdutoBO/VerificaProdutoPublicado', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        carregaGridCertificacoes: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/ProdutoBO/carregaGridCertificacoes', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CadastrarCertificacoes: function (params, callback) {

            var dados = { where: { codigotipocertificacao: params.codigotipocertificacao, nome: params.nome, empresa: params.empresa, codigoproduto: params.codigoproduto, ci: params.ci, status: params.status, gsrn: params.gsrn} };

            $http.post(url + '/ProdutoBO/CadastrarCertificacoes', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },


        InsertCertificacao: function (params, codigo, callback) {
            params.codigo = codigo;

            var dados =
            {
                //TODO HARDCODE params.codigotipocertificacao
                campos: {
                    codigotipocertificacao: params.codigotipocertificacao, nome: params.nome, empresa: params.empresa, codigoproduto: params.codigo, ci: params.ci, status: params.status, gsrn: params.gsrn
                }
            };

            $http.post(url + '/ProdutoBO/InsertCertificacao', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        DeleteCertificacao: function (params, callback) {

            //var dados = { campos: params };

            var dados =
            {
                campos: {
                    codigo: params
                }
            };

            $http.post(url + '/ProdutoBO/DeleteCertificacao', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        UpdateCertificacao: function (params, callback) {
            //var dados = { campos: params };

            var dados =
            {
                campos: {
                    codigotipocertificacao: params.codigotipocertificacao, nome: params.nome, empresa: params.empresa, codigo: params.codigo, ci: params.ci, status: params.status, gsrn: params.gsrn
                }
            };

            $http.post(url + '/ProdutoBO/UpdateCertificacao', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarTodosProdutos: function (callback) {

            $http.post(url + '/ProdutoBO/SelectProduto', { where: {} }).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },


        CarregarCategoriasMae: function (callback) {

            $http.post(url + '/CategoriaBO/CarregarCategoriasMae').success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarCategoriasFilhaId: function (params, callback) {
            var dados = { where: { codigo: params} };

            $http.post(url + '/CategoriaBO/CarregarCategoriasFilhaId', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarTodasCategoriasId: function (params, callback) {
            var dados = { where: { codigo: params} };

            $http.post(url + '/CategoriaBO/CarregarTodasCategoriasId', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarCategoriasId: function (params, callback) {
            var dados = { where: { codigo: params} };

            $http.post(url + '/CategoriaBO/CarregarCategoriasId', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarArvoreEdicao: function (params, callback) {
            var dados = { where: { codigo: params} };

            $http.post(url + '/ProdutoBO/CarregarArvoreEdicao', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarCategoriasFilha: function (params, callback) {
            var dados = { where: { codigo: params.codigo} };

            $http.post(url + '/CategoriaBO/CarregarCategoriasFilha', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        VerificaImagemBanco: function (params, callback) {
            var dados = { where: { codigo: params.src} };

            $http.post(url + '/ProdutoBO/VerificaImagemBanco', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        DeletarImgBanco: function (params, callback) {
            var dados = { where: params };

            $http.post(url + '/ProdutoBO/DeletarImgBanco', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarCategoriaMae: function (params, callback) {
            var dados = { where: { codigocategoriasuperior: params.codigocategoriasuperior} };

            $http.post(url + '/CategoriaBO/CarregarCategoriaMae', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CadastrarImagensProduto: function (params, params2, callback) {
            var dados = { where: { imagens: params, codigoProduto: params2} };

            $http.post(url + '/ProdutoBO/CadastrarImagensProduto', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CadastrarVideoProduto: function (params, params2, callback) {
            var dados = { where: { src: params.src, ordem: params.ordem, codigoProduto: params2} };

            $http.post(url + '/ProdutoBO/CadastrarVideoProduto', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CadastrarCategoriaProduto: function (params, params2, callback) {
            var dados = { where: { categorias: params, codigoProduto: params2} };

            $http.post(url + '/ProdutoBO/CadastrarCategoriaProduto', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        InsertEspecificacao: function (params, codigo, callback) {
            params.codigo = codigo;

            if (params.valor == undefined || params.valor == null || params.valor == '') {
                params.valor = ' ';
            }

            var dados =
            {
                campos: {
                    nome: params.nome, valor: params.valor, codigoproduto: params.codigo
                }
            };

            $http.post(url + '/ProdutoBO/InsertEspecificacao', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        UpdateEspecificacao: function (params, callback) {
            //var dados = { campos: params };
            if (params.valor == undefined || params.valor == null || params.valor == '') {
                params.valor = ' ';
            }
            var dados =
            {
                campos: {
                    nome: params.nome, valor: params.valor, status: params.status, codigoproduto: params.codigo
                }
            };

            $http.post(url + '/ProdutoBO/UpdateEspecificacao', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CarregarTipoUsuarioLogado: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/ProdutoBO/CarregarTipoUsuarioLogado', dados).success(function (data) {
                callback(data);
            }).error(function (data, status, headers, config) {
                $rootScope.alert(data);
            });
        },

        DeleteEspecificacao: function (params, callback) {

            //var dados = { campos: params };

            var dados =
            {
                campos: {
                    codigo: params
                }
            };

            $http.post(url + '/ProdutoBO/DeleteEspecificacao', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        CadastrarProduto: function (params, callback) {
            //params.Codigo = params.Codigo.toString();

            //var dados = { campos: params };

            var dados =
            {
                campos: {
                    codigo: params.codigo, nome: params.nome, descricao: params.descricao, codigoprodutotipo: params.codigoprodutotipo,
                    codigostatuspublicacao: params.codigostatuspublicacao, disponibilidade: params.disponibilidade, aplicacao: params.aplicacao,
                    modelo: params.modelo, fabricante: params.fabricante, situacao: params.situacao, codigoprodutofornecedor: params.codigoprodutofornecedor,
                    marca: params.marca, gtin: params.gtin
                }
            };

            $http.post(url + '/ProdutoBO/Insert', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        PesquisarProduto: function (params, callback) {
            //params.status = params.status.toString();

            var dados = { where: params };

            $http.post(url + '/ProdutoBO/SelectFiltro', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        RemoverProduto: function (params, callback) {
            var dados = { where: params };

            $http.post(url + '/ProdutoBO/Delete', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        EditarProduto: function (params, callback) {
            var parametro = {
                Nome: params.nome, Descricao: params.descricao, Status: params.status, CodigoProdutoTipo: params.codigoprodutotipo, CodigoStatusPublicacao: params.codigostatuspublicacao,
                Disponibilidade: params.disponibilidade, Aplicacao: params.aplicacao, Modelo: params.modelo, Fabricante: params.fabricante, Situacao: params.situacao,
                CodigoUsuarioFornecedor: params.codigousuariofornecedor, especificacoes: params.especificacoes, CodigoProdutoFornecedor: params.codigoprodutofornecedor, marca: params.marca,
                solicitadesbloqueio: params.solicitadesbloqueio, gtin: params.gtin
            };
            var dados = { campos: parametro, where: params.codigo };

            $http.post(url + '/ProdutoBO/Update', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },

        SalvarThumbnailVideo: function (params, callback) {
            var dados = { where: params };

            $http.post(url + '/ProdutoBO/SalvarThumbnailVideo', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },
        ValidarFornecedorPreenchido: function (callback) {
            $http.post(url + '/ProdutoBO/ValidarFornecedorPreenchido', '').success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        }
    };

} ]);
