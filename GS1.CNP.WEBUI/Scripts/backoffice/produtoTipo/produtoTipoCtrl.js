app.controller('ProdutoTipo', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {

        var mensagem = $translate.instant('7.LBL_CadastroTipoProduto');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '7.LBL_CadastroTipoProduto' ? 'Cadastro Tipo de Produto' : mensagem;
        $scope.isPesquisa = false;

        $scope.tableTipos = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        //Busca Tipos de Produtos
        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            if (isPesquisa) {
                dados = angular.copy(item);

                genericService.postRequisicao('TipoProdutoBO', 'PesquisaTipoProduto', { campos: dados }).then(
                    function (result) {

                        $scope.form.submitted = false;
                        $scope.editMode = false;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            }
            else {
                genericService.postRequisicao('TipoProdutoBO', 'BuscarTipoProduto', { where: dados }).then(
                    function (result) {

                        $scope.papeis = result.data;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                );
            }
        }

        $scope.listaStatus = function () {
            return [{ codigo: 1, descricao: 'Ativo' }, { codigo: 0, descricao: 'Inativo'}];
        }

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('TipoProdutoBO', 'CadastrarTipoProduto', function () {

                var mensagem = $translate.instant('7.LBL_CadastroTipoProduto');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '7.LBL_CadastroTipoProduto' ? 'Cadastro Tipo de Produto' : mensagem;
                Utils.bloquearCamposVisualizacao("VisualizarTipoProduto");
                $scope.item = {};
                $scope.nomeimagem = undefined;
                $scope.limparUploadFiles();
                $scope.isPesquisa = false;
                $scope.editMode = true;
                $scope.insertMode = true;
                $scope.searchMode = false;
                $scope.exibeVoltar = true;
            });
        };

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('TipoProdutoBO', 'EditarTipoProduto', function () {

                Utils.bloquearCamposVisualizacao("VisualizarTipoProduto");

                var mensagem = $translate.instant('7.LBL_AlterarTipoProduto');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '7.LBL_AlterarTipoProduto' ? 'Alterar Tipo de Produto' : mensagem;
                if (item != undefined && item.nomeimagem == "") {
                    $scope.nomeimagem = undefined;
                    item.nomeimagem = undefined;
                }
                else {
                    $scope.nomeimagem = item.nomeimagem;
                }
                $scope.limparUploadFiles();
                $scope.item = angular.copy(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.insertMode = false;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;
            });
        };

        $scope.onCancel = function (item) {

            var mensagem = $translate.instant('7.LBL_CadastroTipoProduto');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '7.LBL_CadastroTipoProduto' ? 'Cadastro Tipo de Produto' : mensagem;
            $scope.item = {};
            item = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.exibeVoltar = false;
            $scope.limparUploadFiles();
            $scope.isPesquisa = false;
            Utils.moveScrollTop();
            $scope.tableTipos.page(1);
            $scope.tableTipos.reload();
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        $scope.onSearchMode = function (item) {
            var mensagem = $translate.instant('7.LBL_PesquisarTipoProduto');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '7.LBL_PesquisarTipoProduto' ? 'Pesquisar Tipo de Produto' : mensagem;
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
            $scope.limparUploadFiles();
        };

        //Remove Tipo de Produto selecionado
        $scope.onDelete = function (item) {
            $rootScope.verificaPermissao('TipoProdutoBO', 'ExcluirTipoProduto', function () {

                var mensagem = $translate.instant('7.MENSAGEM_ImagemSeraExcluidaBase');
                var titulo = $translate.instant('7.LBL_Confirmacao');

                $rootScope.confirm(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_ImagemSeraExcluidaBase' ? 'A imagem selecionada será excluída da nossa base de dados. Deseja realmente remover a imagem?' : mensagem, function () {
                    genericService.postRequisicao('TipoProdutoBO', 'ExcluirTipoProduto', { campos: item }).then(
                            function (result) {
                                $scope.form.submitted = false;
                                Utils.moveScrollTop();
                                $scope.tableTipos.reload();
                                $scope.tableTipos.page(1);
                                $scope.item = {};

                                var mensagem = $translate.instant('7.MENSAGEM_DadosRemovidosSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_DadosRemovidosSucesso' ? 'Dados removidos com sucesso.' : mensagem);
                            },
                            $rootScope.TratarErro
                        );

                }, function () { }, typeof titulo === 'object' || titulo == '7.LBL_Confirmacao' ? 'Confirmação' : titulo);
            });
        }

        //Pesquisa Tipo de Produto cadastrado
        $scope.onSearch = function (item) {
            $scope.limparUploadFiles();
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableTipos.page(1);
            $scope.tableTipos.reload();
        }

        //Exclusão de imagem cadastrada
        $scope.ExcluirImagem = function (item) {

            var mensagem = $translate.instant('7.MENSAGEM_DesejaRemover');
            var titulo = $translate.instant('7.LBL_Confirmacao');
            $rootScope.confirm(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_DesejaRemover' ? 'Deseja remover o registro selecionado?' : mensagem, function () {
                genericService.postRequisicao('TipoProdutoBO', 'DeletarImagem', { campos: item }).then(
                        function (result) {
                            item.nomeimagem = undefined;
                            $scope.nomeimagem = undefined;

                            var mensagem = $translate.instant('7.MENSAGEM_ImagemRemovidaSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_ImagemRemovidaSucesso' ? 'Imagem removida com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );

            }, function () { }, typeof titulo === 'object' || titulo == '7.LBL_Confirmacao' ? 'Confirmação' : titulo);
        }

        $scope.alterImage = function () {
            $scope.nomeimagem = undefined
        }

        $scope.limparUploadFiles = function () {
            $("#imagemTipoProduto").val('');
        }

        //Altera Indicador Compartilha Dados
        $scope.alterarIndicador = function (item) {
            console.log(item);
            if (item === 1) $scope.item.campoobrigatorio = false;
            else if (item === 0) $scope.item.campoobrigatorio = true;
            else $scope.item.campoobrigatorio = item;
        }

        function reinicializarVariaveis() {
            $scope.item = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.exibeVoltar = false;
            $scope.isPesquisa = false;
            Utils.moveScrollTop();
            $scope.tableTipos.page(1);
            $scope.tableTipos.reload();
        }

        $scope.onSave = function (item) {

            if (!$scope.form.$valid) {

                var mensagem = $translate.instant('7.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);

                $scope.form.submitted = true;
            }
            else {
                //item.nomeimagem = nomeimagem;

                //Inserção de Tipo de Produto
                if (item.codigo == undefined || item.codigo == null || item.codigo == '') {

                    //Se não possuir imagem selecionada, insere na base normalmente.
                    if (item != undefined && item.nomeimagem == undefined) {
                        //item.nomeimagem = "";

                        genericService.postRequisicao('TipoProdutoBO', 'CadastrarTipoProduto', { campos: item }).then(
                            function (result) {
                                reinicializarVariaveis();

                                var mensagem = $translate.instant('7.MENSAGEM_RegistroSalvoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                            },
                            $rootScope.TratarErro
                        );
                    }
                    //Se possuir imagem selecionada, faz o upload e depois insere na base.
                    else {
                        Utils.upload('imagemTipoProduto', 'blobAzure', 'TipoProdutoBO', function (result) {

                            if (result == undefined) {
                                return false;
                            }
                            else {
                                if (result.length > 0) {
                                    item.nomeimagem = result[0];
                                }

                                genericService.postRequisicao('TipoProdutoBO', 'CadastrarTipoProduto', { campos: item }).then(
                                    function (result) {
                                        reinicializarVariaveis();

                                        var mensagem = $translate.instant('7.MENSAGEM_RegistroSalvoSucesso');
                                        $scope.alert(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);

                                    },
                                    $rootScope.TratarErro
                                );
                            }

                        });
                    }
                }
                //Atualização de Tipo de Produto
                else {
                    if (item != undefined && item.nomeimagem == undefined) {
                        item.nomeimagem = "";

                        genericService.postRequisicao('TipoProdutoBO', 'EditarTipoProduto', { campos: item }).then(
                            function (result) {
                                reinicializarVariaveis();

                                var mensagem = $translate.instant('7.MENSAGEM_RegistroAlteradoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);

                            },
                            $rootScope.TratarErro
                        );
                    }
                    else {
                        //Verifica se a imagem já foi upada. Se sim, não upa novamente.
                        if (item.nomeimagem.indexOf("/upload/") > -1 || item.nomeimagem.indexOf("blob.core.windows.net") > -1) {
                            genericService.postRequisicao('TipoProdutoBO', 'EditarTipoProduto', { campos: item }).then(
                                function (result) {
                                    reinicializarVariaveis();

                                    var mensagem = $translate.instant('7.MENSAGEM_RegistroAlteradoSucesso');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);

                                },
                                $rootScope.TratarErro
                            );
                        }
                        //Se não, upa a imagem novamente.
                        else {
                            Utils.upload('imagemTipoProduto', 'blobAzure', 'TipoProdutoBO', function (result) {
                                if (result == undefined) {
                                    return false;
                                }
                                else {
                                    if (result.length > 0) {
                                        item.nomeimagem = result[0];
                                    }

                                    genericService.postRequisicao('TipoProdutoBO', 'EditarTipoProduto', { campos: item }).then(
                                        function (result) {
                                            reinicializarVariaveis();
                                            var mensagem = $translate.instant('7.MENSAGEM_RegistroAlteradoSucesso');
                                            $scope.alert(typeof mensagem === 'object' || mensagem == '7.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
                                        },
                                        $rootScope.TratarErro
                                    );
                                }
                            });
                        }
                    }
                }

            }
        }

        $scope.onInit = function () {
            //Busca informações do Dropdwon de TradeItemUnitDescriptor
            genericService.postRequisicao('TradeItemUnitDescriptorCodesBO', 'BuscarTradeItemUnitDescriptorCodesAtivos', {}).then(
                function (result) {
                    if (result.data != undefined && result.data != null) {
                        $scope.tradeitens = result.data;
                    }
                },
                $rootScope.TratarErro
            );
        }

        $scope.onInit();

    } ]);
