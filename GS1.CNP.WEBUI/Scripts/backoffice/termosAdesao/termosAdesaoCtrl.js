app.controller('TermosAdesao', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {


        var mensagem = $translate.instant('9.LBL_TermosAdesao');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '9.LBL_TermosAdesao' ? 'Termos de Adesão' : mensagem;

        $scope.isPesquisa = false;
        $scope.isVisualize = false;
        $scope.isNovoRegistro = false;

        $scope.menuWysiwyg = [
            ['font'],
            ['font-size'],
            ['bold', 'italic', 'underline', 'strikethrough'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['paragraph']
        ];

        $scope.tableHistorico = new ngTableParams({
            page: 1,
            count: $rootScope.parametros != undefined ? $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina') : 10,
            sorting: {
                nomestatustermoadesao: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.item = data;
                });
            }
        });

        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            if (!isPesquisa) {
                genericService.postRequisicao('TermosAdesaoBO', 'BuscaTermosAdesao', { campos: dados }).then(
                function (result) {
                    $scope.termos = result.data;

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
            }
            else {
                if (item != undefined) {
                    dados = angular.copy(item);
                }

                genericService.postRequisicao('TermosAdesaoBO', 'PesquisarTermosAdesao', { campos: dados }).then(
                function (result) {
                    $scope.form.submitted = false;
                    $scope.editMode = false;

                    $scope.termos = result.data;

                    if (callback != undefined && callback != null) {
                        callback(result.data);
                    }
                },
                $rootScope.TratarErro
            );
            }
        };

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('TermosAdesaoBO', 'CadastrarTermoAdesao', function () {
                genericService.postRequisicao('TermosAdesaoBO', 'VerificaTermoEmElaboracao', {}).then(
                    function (result) {
                        if (result.data == 1) {

                            var mensagem = $translate.instant('9.MENSAGEM_ApenasUmTermo');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '9.MENSAGEM_ApenasUmTermo' ? 'O sistema permite apenas um \'Termo de Adesão\' com status \'em elaboração\'. Por favor, editar o registro já existente.' : mensagem);

                        }
                        else {

                            var mensagem = $translate.instant('9.LBL_TitleCadastrarTermo');
                            $scope.titulo = typeof mensagem === 'object' || mensagem == '9.LBL_TitleCadastrarTermo' ? 'Cadastrar Termo de Adesão' : mensagem;

                            $scope.item = {};
                            $scope.editMode = true;
                            $scope.searchMode = false;
                            $scope.isNovoRegistro = true;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = true;
                            $('#html').text('');
                        }

                        Utils.bloquearCamposVisualizacao("VisualizarTermosAdesao");
                    },
                    $rootScope.TratarErro
                );
            });
        };

        $scope.onEdit = function (item) {
            $rootScope.verificaPermissao('TermosAdesaoBO', 'AtualizarTermoAdesao', function () {
                var mensagem = $translate.instant('9.LBL_AlterarTermo');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '9.LBL_AlterarTermo' ? 'Alterar termo de adesão' : mensagem;

                $scope.item = angular.copy(item);
                $scope.searchMode = false;
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isVisualize = false;
                $scope.exibeVoltar = true;
                Utils.bloquearCamposVisualizacao("VisualizarTermosAdesao");
            });
        };

        $scope.onCancel = function (item) {

            var mensagem = $translate.instant('9.LBL_TermosAdesao');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '9.LBL_TermosAdesao' ? 'Termos de Adesão' : mensagem;

            $scope.item = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.isVisualize = false;
            $scope.isNovoRegistro = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            $(window).scrollTop(0);
            $scope.tableHistorico.page(1);
            $scope.tableHistorico.reload();
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        $scope.onSearchMode = function (item) {
            var mensagem = $translate.instant('9.LBL_PesquisarTermo');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '9.LBL_PesquisarTermo' ? 'Pesquisar termo de adesão' : mensagem;

            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isVisualize = false;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
            $scope.buscarStatusTermoAdesao();
            $('#html').text('');
            $scope.item = {};
        };

        $scope.onVisualize = function (item) {

            var mensagem = $translate.instant('9.LBL_VisualizarTermo');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '9.LBL_VisualizarTermo' ? 'Visualizar termo de adesão' : mensagem;

            $scope.item = angular.copy(item);
            $scope.editMode = true;
            $scope.isVisualize = false;
            $scope.isPesquisa = false;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
        };

        $scope.buscarStatusTermoAdesao = function () {
            genericService.postRequisicao('TermosAdesaoBO', 'BuscaStatusTermoAdesaoAtivos', {}).then(
            function (result) {
                $scope.statustermoadesao = result.data;
                $('#formcontainer').focus();
            },
            $rootScope.TratarErro
        );
        }

        $scope.onSave = function (item) {
            if (!$scope.form.$valid) {

                var mensagem = $translate.instant('9.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '9.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);

                $scope.form.submitted = true;
                return false;
            }

            if (!item.codigo || item.codigostatustermoadesao == 2) {

                genericService.postRequisicao('TermosAdesaoBO', 'VerificaTermoEmElaboracao', {}).then(
                function (result) {
                    if (result.data == 1) {

                        var mensagem = $translate.instant('9.MENSAGEM_ApenasUmTermo');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '9.MENSAGEM_ApenasUmTermo' ? 'O sistema permite apenas um \'Termo de Adesão\' com status \'em elaboração\'. Por favor, editar o registro já existente.' : mensagem);

                    }
                    else {
                        genericService.postRequisicao('TermosAdesaoBO', 'CadastrarTermoAdesao', { campos: item }).then(
                            function (result) {
                                $scope.item = {};
                                $scope.editMode = false;
                                $scope.form.submitted = false;
                                $scope.exibeVoltar = false;
                                $scope.tableHistorico.page(1);
                                $scope.tableHistorico.reload();
                                $(window).scrollTop(0);

                                var mensagem = $translate.instant('9.MENSAGEM_RegistroCadastradoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '9.MENSAGEM_RegistroCadastradoSucesso' ? 'Registro cadastrado com sucesso.' : mensagem);

                            },
                            $rootScope.TratarErro
                        );
                    }
                },
                $rootScope.TratarErro
            );

            } else {
                genericService.postRequisicao('TermosAdesaoBO', 'AtualizarTermoAdesao', { campos: item }).then(
                    function (result) {
                        $scope.item = {};
                        $scope.editMode = false;
                        $scope.form.submitted = false;
                        $scope.exibeVoltar = false;
                        $scope.tableHistorico.page(1);
                        $scope.tableHistorico.reload();
                        $(window).scrollTop(0);

                        var mensagem = $translate.instant('9.MENSAGEM_RegistroAlteradoSucesso');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '9.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);

                    },
                    $rootScope.TratarErro
                );
            }
        }

        $scope.onSearch = function (item) {
            $scope.exibeVoltar = false;
            $scope.tableHistorico.reload();
            $scope.tableHistorico.page(1);
        };

        $scope.ativarTermo = function (item) {

            genericService.postRequisicao('TermosAdesaoBO', 'VerificaTermoAtivo', {}).then(
            function (result) {
                if (result.data == 1) {

                    var mensagem = $translate.instant('9.MENSAGEM_DesejaAtivarTermoAdesao');
                    var titulo = $translate.instant('9.LBL_Confirmacao');
                    $rootScope.confirm(typeof mensagem === 'object' || mensagem == '9.MENSAGEM_DesejaAtivarTermoAdesao' ? 'Já existe um Termo de Adesão em uso. Deseja realmente ativar esta nova versão?' : mensagem, function () {

                        genericService.postRequisicao('TermosAdesaoBO', 'AtivarTermo', { campos: item }).then(
                                function (result) {
                                    $scope.item = {};
                                    $scope.editMode = false;
                                    $scope.form.submitted = false;
                                    $scope.exibeVoltar = false;
                                    $scope.tableHistorico.page(1);
                                    $scope.tableHistorico.reload();
                                    $(window).scrollTop(0);

                                    var mensagem = $translate.instant('9.MENSAGEM_OperacaoConcluidaTermoSelecionado');
                                    $scope.alert(typeof mensagem === 'object' || mensagem == '9.MENSAGEM_OperacaoConcluidaTermoSelecionado' ? 'Operação Concluída. Termo selecionado como Atual!' : mensagem);

                                },
                                $rootScope.TratarErro
                            );
                    }, function () { }, typeof titulo === 'object' || titulo == '9.LBL_Confirmacao' ? 'Confirmação' : titulo);
                }
                else {
                    genericService.postRequisicao('TermosAdesaoBO', 'AtivarTermo', { campos: item }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.form.submitted = false;
                            $scope.exibeVoltar = false;
                            $scope.tableHistorico.page(1);
                            $scope.tableHistorico.reload();
                            $(window).scrollTop(0);

                            var mensagem = $translate.instant('9.MENSAGEM_OperacaoConcluidaTermoSelecionado');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '9.MENSAGEM_OperacaoConcluidaTermoSelecionado' ? 'Operação Concluída. Termo selecionado como Atual!' : mensagem);

                        },
                        $rootScope.TratarErro
                    );
                }
            },
            $rootScope.TratarErro
        );


        }

        //Carrega dropdowns (TiposCompartilhamento e Idioma)
        function carregaDropDowns() {
            genericService.postRequisicao('TermosAdesaoBO', 'BuscarTipoAceite', null).then(
            function (result) {
                $scope.tiposAceite = result.data;
            },
            $rootScope.TratarErro
        );

            genericService.postRequisicao('TermosAdesaoBO', 'BuscarIdiomas', null).then(
            function (result) {
                $scope.idiomas = result.data;
            },
            $rootScope.TratarErro
        );
        }
        carregaDropDowns();

    } ]);