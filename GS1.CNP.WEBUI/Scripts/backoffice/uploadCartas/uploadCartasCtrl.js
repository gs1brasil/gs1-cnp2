app.controller('UploadCartas', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI', '$translate',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI, $translate) {

        var mensagem = $translate.instant('5.LBL_UploadCartas');
        $scope.titulo = typeof mensagem === 'object' || mensagem == '5.LBL_UploadCartas' ? 'Upload de Cartas' : mensagem;
        $scope.isPesquisa = false;
        $scope.uploadObrigatorio = false;

        $scope.tableCartas = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
            }
        });

        //Busca Tipos de Produtos
        function carregaGrid(item, isPesquisa, callback) {
            var dados = null;

            if (isPesquisa) {
                dados = angular.copy(item);

                genericService.postRequisicao('UploadCartasBO', 'PesquisaTipoCarta', { campos: dados }).then(
                    function (result) {

                        $scope.form.submitted = false;
                        $scope.editMode = false;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                 );
            }
            else {
                genericService.postRequisicao('UploadCartasBO', 'BuscarTipoCarta', { where: dados }).then(
                    function (result) {

                        $scope.papeis = result.data;

                        if (callback != undefined && callback != null) {
                            callback(result.data);
                        }
                    },
                    $rootScope.TratarErro
                 );
            }
        }

        $scope.onFormMode = function (item) {
            $rootScope.verificaPermissao('UploadCartasBO', 'AtualizarTemplate', function () {
                $scope.item = {};
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.exibeVoltar = true;
                Utils.bloquearCamposVisualizacao("VisualizarUploadCartas");
            });
        }

        $scope.onEdit = function (item) {
            //$rootScope.verificaPermissao('UploadCartasBO', 'AtualizarTemplate', function () {

                Utils.bloquearCamposVisualizacao("VisualizarUploadCartas");

                var mensagem = $translate.instant('5.LBL_UploadCartas');
                $scope.titulo = typeof mensagem === 'object' || mensagem == '5.LBL_UploadCartas' ? 'Upload de Cartas' : mensagem;
                angular.element("#carta").val('');
                $scope.carta = '';
                $scope.uploadObrigatorio = false;
                $scope.item = angular.copy(item);
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isPesquisa = false;
                $scope.exibeVoltar = true;
            //});
        };

        $scope.onCancel = function (item) {
            var mensagem = $translate.instant('5.LBL_UploadCartas');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '5.LBL_UploadCartas' ? 'Upload de Cartas' : mensagem;
            $scope.item = {};
            item = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableCartas.page(1);
            $scope.tableCartas.reload();
        };

        $scope.onSearchMode = function (item) {
            var mensagem = $translate.instant('5.LBL_PesquisarCartas');
            $scope.titulo = typeof mensagem === 'object' || mensagem == '5.LBL_PesquisarCartas' ? 'Pesquisar Cartas' : mensagem;

            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.item = {};
            $scope.editMode = true;
            $scope.searchMode = true;
            $scope.isPesquisa = true;
            $scope.exibeVoltar = true;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
        };

        //Pesquisa Tipo de Produto cadastrado
        $scope.onSearch = function (item) {
            $scope.exibeVoltar = false;
            Utils.moveScrollTop();
            $scope.tableCartas.reload();
            $scope.tableCartas.page(1);
        };

        //Chamado ao pressionar ESC na tela
        window.onkeydown = function (event) {
            if (event.keyCode === 27) {
                $scope.onCancel();
            }
        };

        $scope.onSave = function (item, carta) {

            if (!$scope.form.$valid) {

                var mensagem = $translate.instant('5.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '5.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
            }
            else {
                if (carta != undefined && carta != null && carta != '') {
                    item.caminhotemplate = carta;
                }

                blockUI.start();

                //Atualização de Template de Carta
                if (item != undefined && item.caminhotemplate == undefined) {
                    item.caminhotemplate = "";

                    genericService.postRequisicao('UploadCartasBO', 'AtualizarTemplate', { campos: item }).then(
                        function (result) {
                            $scope.item = {};
                            $scope.editMode = false;
                            $scope.searchMode = false;
                            $scope.form.submitted = false;
                            $scope.isPesquisa = false;
                            $scope.exibeVoltar = false;
                            Utils.moveScrollTop();
                            $scope.tableCartas.page(1);
                            $scope.tableCartas.reload();
                            blockUI.stop();

                            var mensagem = $translate.instant('5.MENSAGEM_RegistroSalvoSucesso');
                            $scope.alert(typeof mensagem === 'object' || mensagem == '5.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                        },
                        $rootScope.TratarErro
                    );
                }
                else {
                    //Verifica se a carta já foi upada. Se sim, não upa novamente.
                    //if (item.caminhotemplate.indexOf("/upload/") > -1) {
                    if (item.caminhotemplate.indexOf("/upload/") > -1 || item.caminhotemplate.indexOf("blob.core.windows.net") > -1)
                    {
                        genericService.postRequisicao('UploadCartasBO', 'AtualizarTemplate', { campos: item }).then(
                            function (result) {
                                $scope.item = {};
                                $scope.editMode = false;
                                $scope.searchMode = false;
                                $scope.form.submitted = false;
                                $scope.isPesquisa = false;
                                $scope.exibeVoltar = false;
                                Utils.moveScrollTop();
                                $scope.tableCartas.page(1);
                                $scope.tableCartas.reload();
                                blockUI.stop();

                                var mensagem = $translate.instant('5.MENSAGEM_RegistroSalvoSucesso');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '5.MENSAGEM_RegistroSalvoSucesso' ? 'Registro salvo com sucesso.' : mensagem);
                            },
                            $rootScope.TratarErro
                        );
                    }
                    //Se não, upa a carta novamente.
                    else {
                        Utils.upload('carta', 'blobAzure', 'UploadCartasBO', function (result) {
                            if (result.length > 0)
                                result = result[0];

                            if (result == undefined) {
                                blockUI.stop();
                                var mensagem = $translate.instant('5.MENSAGEM_CamposObrigatorios');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '5.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                                $scope.uploadObrigatorio = true;
                                return false;
                            }
                            else if (result == "Extensão não suportada") {
                                blockUI.stop();
                                var mensagem = $translate.instant('5.MENSAGEM_AtualizarTemplate');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '5.MENSAGEM_AtualizarTemplate' ? 'Por favor atualize o template e tente novamente.' : mensagem);
                                
                                return false;
                            }
                            else if (result.search("{") > -1 && result.search("}") > -1) {// == "Template inválido") {
                                //$scope.alert('Os campos do template estão fora do padrão. Por favor selecione um template válido.'); 
                                blockUI.stop();
                                var mensagem = $translate.instant(['5.MENSAGEM_CamposNaoEncontradosTemplate', '5.MENSAGEM_AtualizarTemplate']);
                                $scope.alert(
                                    typeof mensagem === 'object' || mensagem == ['5.MENSAGEM_CamposNaoEncontradosTemplate', '5.MENSAGEM_AtualizarTemplate'] ?
                                    'Os campos a seguir não foram encontrados no template: ' + result + '. Por favor atualize o template e tente novamente.' :
                                     mensagem['5.MENSAGEM_CamposNaoEncontradosTemplate'] + ' ' + result + '. ' + mensagem['5.MENSAGEM_AtualizarTemplate']
                                 );
                                     
                                //$translate(['5.MENSAGEM_CamposNaoEncontradosTemplate', '5.MENSAGEM_AtualizarTemplate']).then(function (resultado) {
                                //    $scope.alert(resultado['5.MENSAGEM_CamposNaoEncontradosTemplate'] + ' ' + result + '. ' + resultado['5.MENSAGEM_AtualizarTemplate']);
                                //});
                                return false;
                            }
                            else {
                                item.caminhotemplate = result;

                                genericService.postRequisicao('UploadCartasBO', 'AtualizarTemplate', { campos: item }).then(
                                    function (result) {
                                        $scope.item = {};
                                        $scope.editMode = false;
                                        $scope.searchMode = false;
                                        $scope.form.submitted = false;
                                        $scope.isPesquisa = false;
                                        $scope.exibeVoltar = false;
                                        Utils.moveScrollTop();
                                        $scope.tableCartas.page(1);
                                        $scope.tableCartas.reload();
                                        blockUI.stop();
                                        var mensagem = $translate.instant('5.MENSAGEM_UploadSucesso');
                                        $scope.alert(typeof mensagem === 'object' || mensagem == '5.MENSAGEM_UploadSucesso' ? 'Upload de carta efetuado com sucesso.' : mensagem);

                                    },
                                    $rootScope.TratarErro
                                );
                            }
                        });
                    }
                }

            }
        }

    } ]);
