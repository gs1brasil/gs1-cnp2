﻿app.factory('TipoUsuarioService', ['$q', '$http', '$location', function ($q, $http, $location) {
    var url = '/requisicao.ashx';

    return {
        buscarTiposUsuario: function (callback) {
            $http.get(url + '/TipoUsuarioBO/buscarTiposUsuario').success(function (data) {
                callback(data);
            }).error(function (data, status, headers, config) {
                $rootScope.alert(data);
            });
        }
    };
} ]);
