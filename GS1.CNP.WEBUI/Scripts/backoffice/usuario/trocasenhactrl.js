﻿app.controller('TrocaSenhaCtrl', ['$scope', '$http', 'ngTableParams', '$modal', '$rootScope', '$timeout', '$location', 'TrocaSenhaService', 'IndexService', '$window', '$location', '$translate',
    function ($scope, $http, ngTableParams, $modal, $rootScope, $timeout, $location, senha, IndexService, $window, $location, $translate) {

        var titulo = $translate.instant('37.LBL_TrocaSenha');
        $scope.titulo = typeof titulo === 'object' || titulo == '37.LBL_TrocaSenha' ? 'Troca de Senha' : titulo;

        $scope.carregaParametro = function () {
            senha.CarregarParametros('autenticacao.senha.diferente.n.ultimas', function (data) {
                $scope.qtdesenhas = data[0].valor;
            });
        }

        $scope.carregaParametro();

        $scope.onSave = function (item) {
            if ($scope.form.$valid) {
                if ($scope.item.nova_senha == $scope.item.senha_atual) {
                    var mensagem = $translate.instant('37.MENSAGEM_NovaSenhaIgualAtual');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '37.MENSAGEM_NovaSenhaIgualAtual' ? 'A Nova Senha é igual a Senha Atual. Por favor, informar outra senha.' : mensagem);
                }
                else if ($scope.item.nova_senha != $scope.item.confirmar_senha) {
                    var mensagem = $translate.instant('37.MENSAGEM_SenhaNaoConferem');
                    $scope.alert(typeof mensagem === 'object' ? 'Nova senha e senha de confirmação não conferem.' : mensagem);
                }
                else if ($scope.item.nova_senha.length < 7 || $scope.item.nova_senha.length > 15) {
                    var mensagem = $translate.instant('37.MENSAGEM_NovaSenhaMinimoMaximo');
                    $scope.alert(typeof mensagem === 'object' || mensagem == '37.MENSAGEM_NovaSenhaMinimoMaximo' ? 'A Nova Senha deve ser de no mínimo 7 e no máximo 15, permitindo o uso de caracteres especiais. Por favor, informar outra senha.' : mensagem);
                }
                else {
                    var score = $scope.passwordStrength($scope.item.nova_senha);

                    if (score > 3) {
                        senha.Alterarsenha($scope.item, function (data) {
                            if (data == 0) {
                                var mensagem = $translate.instant('37.MENSAGEM_SenhaNaoConferem');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '37.MENSAGEM_SenhaNaoConferem' ? 'Senha atual não confere.' : mensagem);
                            }
                            else if (data == 2) {
                                var mensagem = $translate.instant('37.MENSAGEM_NovaSenha60Porcento');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '37.MENSAGEM_NovaSenha60Porcento' ? 'A nova senha possui mais de 60% dos caracteres da senha anterior. Informe outra senha.' : mensagem);
                            }
                            else if (data == 3) {
                                var mensagem = $translate.instant('37.MENSAGEM_SenhaUtilizadaAnteriormente');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '37.MENSAGEM_SenhaUtilizadaAnteriormente' ? 'A Nova Senha informada já foi utilizada anteriormente, por favor, informe outra senha.' : mensagem);
                            }
                            else if (data == 4) {
                                var mensagem = $translate.instant('37.MENSAGEM_NaoAlterouSenha');
                                $scope.alert(typeof mensagem === 'object' || mensagem == '37.MENSAGEM_NaoAlterouSenha' ? 'Não foi possível alterar a senha.' : mensagem);
                            }
                            else if (data == 1) {

                                IndexService.RetornaUsuarioLogado("", function (data) {
                                    if ((data != null) && (data.nome != null)) {
                                        $rootScope.usuarioLogado = data;
                                    }

                                    $scope.geraMenu();

                                    var mensagem1 = $translate.instant('37.MENSAGEM_SenhaAlteradaSucesso');
                                    var mensagem2 = $translate.instant('37.MENSAGEM_Alerta');

                                    $scope.alert(typeof mensagem1 === 'object' || mensagem1 == '37.MENSAGEM_SenhaAlteradaSucesso' ?
                                        'Senha alterada com sucesso.' : mensagem1, function () {
                                        $location.path("#/backoffice/home");
                                    }, mensagem2);

                                });


                            }
                        });
                    }
                    else {
                        var mensagem = $translate.instant('37.MENSAGEM_SenhaConterMaiusculoMinusculo');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '37.MENSAGEM_SenhaConterMaiusculoMinusculo' ? 'A nova senha deve conter letra maiúscula, minúscula, número e caractere especial.' : mensagem);
                    }

                }
            } else {
                var mensagem = $translate.instant('37.MENSAGEM_CamposObrigatorios');
                $scope.alert(typeof mensagem === 'object' || mensagem == '37.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
                $scope.form.submitted = true;
            }
        };

        $scope.onCancel = function () {
            $location.path('/backoffice/home');
        }

        $scope.showHidePass = function (id) {

            if (angular.element("#" + id).get(0).type == "password") {
                angular.element("#" + id).get(0).type = 'text';
            }
            else {
                angular.element("#" + id).get(0).type = 'password';
            }
        }

        $scope.passwordStrength = function (password) {
            //$translate(['37.LBL_MuitoFraca', '37.LBL_Fraca', '37.LBL_Media', '37.LBL_Forte', '37.LBL_MuitoForte']).then(function (resultado) {
            var desc = new Array();
            desc[0] = "Muito Fraca"; //resultado["37.LBL_MuitoFraca"];
            desc[1] = "Muito Fraca"; //resultado["37.LBL_MuitoFraca"];
            desc[2] = "Fraca"; //resultado["37.LBL_Fraca"];
            desc[3] = "Média"; //resultado["37.LBL_Media"];
            desc[4] = "Forte"; //resultado["37.LBL_Forte"];
            desc[5] = "Muito Forte"; //resultado["37.LBL_MuitoForte"];

            var score = 0;

            //Se senha for maior do que 6 caracteres, ganha 1 ponto
            if (password.length > 6) score++;

            //Se senha tiver letras minúsculas e maiúsculas, ganha 1 ponto
            if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) score++;

            //Se senha tem pelo menos 1 número, ganha 1 ponto
            if (password.match(/\d+/)) score++;

            //Se senha possui pelo menos 1 caractere especial, ganha 1 ponto
            if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) score++;

            //Se senha tiver 15 caracteres, ganha 1 ponto
            if (password.length > 15) score++;

            document.getElementById("passwordDescription").innerHTML = desc[score];
            document.getElementById("passwordStrength").className = "strength" + score;

            return score;
            //});
        }

    } ]
);