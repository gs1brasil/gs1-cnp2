﻿app.factory('TrocaSenhaService', ['$q', '$http', '$location', '$rootScope', function ($q, $http, $location, $rootScope) {
    var url = '/requisicao.ashx';

    return {
        Alterarsenha: function (params, callback) {
            var dados = { campos: params };

            $http.post(url + '/LoginBO/Alterarsenha', dados).success(function (data) {
                callback(data);
            });
        },
        CarregarParametros: function (params, callback) {

            var dados = { where: params };

            $http.post(url + '/ParametroBO/CarregarParametroEspecifico', dados).success(function (data) {
                callback(data);
            }).error($rootScope.TratarErro);
        },
    };
} ]);
