app.controller('UsuarioCtrl', ['$scope', '$http', '$timeout', '$rootScope', 'ngTableParams', '$modal', '$log', '$filter', 'genericService', 'blockUI', 'Utils', '$translate',
function ($scope, $http, $timeout, $rootScope, ngTableParams, $modal, $log, $filter, genericService, blockUI, Utils, $translate) {

    $scope.mask = '999.999.999-99';
    $scope.codigoAssociado = undefined;
    $scope.associadosMarcados = [];
    $scope.errorCPFCNPJ = false;

    var titulo = $translate.instant('10.LBL_CadastroUsuario');
    $scope.titulo = typeof titulo === 'object' || titulo == '10.LBL_CadastroUsuario' ? 'Cadastro de usuário' : titulo;

    genericService.postRequisicao('LoginBO', 'VerificarAssociadoSelecionado', {}).then(
        function (result) {
            $scope.associadoSelecionado = result.data;
        }
    );

    //inicializando abas do form
    $('#tabs-user a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    function carregaUsuarios(params, paginaAtual, registroPorPagina, ordenacao, callback) {
        var parametro = null;

        if ($scope.isPesquisa && params != undefined && params != null) {

            if (params.empresaassociada != undefined && params.empresaassociada.codigo != undefined) {
                var empresaassociada = params.empresaassociada.codigo;
            }
            else {
                var empresaassociada = params.empresaassociada;
            }

            parametro = {
                NOME: params.nome || '',
                CODIGOSTATUSUSUARIO: params.codigostatususuario || '',
                CODIGOPERFIL: params.codigoperfil || '',
                CODIGOTIPOUSUARIO: params.codigotipousuario,
                EMAIL: params.email || '',
                NOMETIPOUSUARIO: params.nometipousuario || '',
                TELEFONE: params.telefone,
                EMPRESAASSOCIADA: empresaassociada || ''
            };
        }

        genericService.postRequisicao('UsuarioBO', 'BuscarUsuarios', { where: parametro, paginaAtual: paginaAtual, registroPorPagina: registroPorPagina, ordenacao: ordenacao }).then(
            function (result) { callback(result); },
            $rootScope.TratarErro
        );
    }

    function carregaAssociados(codigo, callback) {

        genericService.postRequisicao('UsuarioBO', 'BuscarAssociados', { campos: { "codigo": codigo || null} }).then(
            function (result) { callback(result); },
            $rootScope.TratarErro
        );

        $rootScope.buscarAssociados();
    }

    function InserirUsuario(params, callback) {

        if (params !== undefined && params !== null &&
            params.Email !== undefined && params.Email !== null && params.Email !== '') {

            genericService.postRequisicao('UsuarioBO', 'ValidarExistenciaUsuario', { where: { Email: params.Email} }).then(
                function (result) {

                    if (result !== undefined && result !== null && result.data !== undefined && result.data !== null && result.data == false) {
                        var dados = { campos: params };

                        genericService.postRequisicao('UsuarioBO', 'Insert', dados).then(
                            function (result) { callback(result.data); },
                            $rootScope.TratarErro
                        );
                    }
                    else {
                        var mensagem = $translate.instant('10.MENSAGEM_UsuarioJaExistente');
                        $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_UsuarioJaExistente' ? 'Usuário já existente.' : mensagem);
                    }
                },
                $rootScope.TratarErro
            );

        }
    }

    function AtualizarUsuario(codigo, params, callback) {

        var dados = { campos: params, where: codigo };

        genericService.postRequisicao('UsuarioBO', 'Update', dados).then(
            function (result) { callback(result.data); },
            $rootScope.TratarErro
        );
    }

    //Carrega associados cadastrados 
    $scope.BuscarAssociadosAutoComplete = function (nome) {
        if (isNumber(nome)) {
            nome = nome.replaceAll('.', '').replaceAll('-', '');
        }

        return genericService.postRequisicao('LoginBO', 'BuscarAssociadosUsuarioAutoComplete', { where: { nome: nome} }).then(
            function (result) {
                var associadosDoUsuario = [];
                angular.forEach(result.data, function (item) {
                    associadosDoUsuario.push({ codigo: item.codigo, nome: item.nome, cpfcnpj: item.cpfcnpj, cad: item.cad,
                        indicadorcnaerestritivo: item.indicadorcnaerestritivo ? item.indicadorcnaerestritivo : ''
                    });
                });
                return associadosDoUsuario;
            }
        );
    }

    function isNumber(obj) {
        return !isNaN(parseFloat(obj))
    }

    function isNumber(n) {
        return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
    }

    $scope.onFormMode = function (item) {
        $rootScope.verificaPermissao('UsuarioBO', 'Insert', function () {
            $('#tabs-user a:first').tab('show');

            var titulo = $translate.instant('10.LBL_CadastroUsuario');
            $scope.titulo = typeof titulo === 'object' || titulo == '10.LBL_CadastroUsuario' ? 'Cadastro de usuário' : titulo;

            $scope.item = {};
            $scope.itemForm = {};
            $scope.errorCPFCNPJ = false;
            $scope.formMode = true;
            $scope.searchMode = false;
            $scope.exibeVoltar = true;
            $scope.isPesquisa = false;
            $scope.initMode = true;
            $scope.itemForm.codigostatususuario = 1;

            genericService.postRequisicao('StatusUsuarioBO', 'buscarStatusUsuario', { campos: { codigo: $scope.usuarioLogado.id_tipousuario} }).then(
                function (result) {
                    $scope.statusUsuario = result.data;
                },
                $rootScope.TratarErro
            );

            $scope.itemForm.nometipousuario = $scope.typeUser;
            $scope.itemForm.telefone = '';

            if ($scope.usuarioLogado.id_tipousuario != 1) {
                $scope.itemForm.codigotipousuario = $scope.usuarioLogado.id_tipousuario;
            }

            $scope.onSelectTipoUsuario($scope.usuarioLogado.id_tipousuario, $scope.isPesquisa);

            $scope.tableAssociados.data = [];
            $scope.codigoAssociado = undefined;
            $scope.tableAssociados.reload();
            angular.element("#typea2").val('');
            $('#form :input:first').focus();
            Utils.bloquearCamposVisualizacao("VisualizarUsuario");
        });
    };

    $scope.onSearchMode = function (item) {
        $rootScope.verificaPermissao('UsuarioBO', 'BuscarUsuarios', function () {
            var titulo = $translate.instant('10.LBL_PesquisarUsuario');
            $scope.titulo = typeof titulo === 'object' || titulo == '10.LBL_PesquisarUsuario' ? 'Pesquisar Usuário' : titulo;
            Utils.desbloquearCamposVisualizacaoPesquisa();
            $scope.itemForm = {};
            $scope.isPesquisa = true;
            $scope.errorCPFCNPJ = false;
            $scope.initMode = false;

            genericService.postRequisicao('StatusUsuarioBO', 'buscarStatusUsuario', { campos: { codigo: $scope.usuarioLogado.id_tipousuario} }).then(
                function (result) {
                    $scope.statusUsuario = result.data;
                },
                $rootScope.TratarErro
            );

            $scope.onSelectTipoUsuario($scope.usuarioLogado.id_tipousuario, $scope.isPesquisa);

            if ($scope.usuarioLogado.id_tipousuario != 1) {
                $scope.itemForm.codigotipousuario = $scope.usuarioLogado.id_tipousuario;
            }

            $scope.formMode = true;
            $scope.searchMode = true;
            $scope.exibeVoltar = true;
            $('#tabs-user a:first').tab('show');
            angular.element("#typea2").val('');
            $('#form :input:first').focus();
        });
    };

    $scope.verificaAutoCompleteUsuario = function () {
        setTimeout(function () {
            if ($scope.itemForm.empresaassociada != '' && $scope.itemForm.empresaassociada != undefined && $scope.itemForm.empresaassociada.codigo == null) {

                var mensagem = $translate.instant('10.MENSAGEM_EmpresaAssociadaNaoEncontrada');
                $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_EmpresaAssociadaNaoEncontrada' ? 'Empresa Associada não encontrada.' : mensagem);
            }
        }, 300);
    }

    $scope.onCancel = function (item) {
        var titulo = $translate.instant('10.LBL_CadastroUsuario');
        $scope.titulo = typeof titulo === 'object' || titulo == '10.LBL_CadastroUsuario' ? 'Cadastro de Usuário' : titulo;

        $scope.item = {};
        $scope.itemForm = {};
        $scope.associadosMarcados = [];
        $scope.perfisUsuario = {};
        $scope.formMode = false;
        $scope.searchMode = false;
        $scope.form.submitted = false;
        $scope.exibeVoltar = false;
        $scope.statusUsuario = [];
        $scope.isPesquisa = false;
        $scope.initMode = false;
        Utils.moveScrollTop();
        $scope.tableParams.page(1);
        $scope.tableParams.reload();
        angular.element("#typea2").val('');
        angular.element("#cpfcnpj").val('');
    };

    //Chamado ao pressionar ESC na tela
    window.onkeydown = function (event) {
        if (event.keyCode === 27) {
            $scope.onCancel();
        }
    };

    $scope.onClean = function (item) {
        $scope.item = {};
        $scope.itemForm = {};
        angular.element("#typea2").val('');
        angular.element("#email").val('');
    }

    $scope.onKeydown = function () {
        return $scope.itemForm.mensagemobservacao.length > 500;
    }

    $scope.onEdit = function (item) {

        $rootScope.verificaPermissao('UsuarioBO', 'Update', function () {

            Utils.bloquearCamposVisualizacao("VisualizarUsuario");

            var titulo = $translate.instant('10.LBL_AlterarUsuario');
            $scope.titulo = typeof titulo === 'object' || titulo == '10.LBL_AlterarUsuario' ? 'Alterar Usuário' : titulo;

            $scope.perfisUsuario = {};
            $scope.statusUsuario = [];

            $timeout(function () {
                genericService.postRequisicao('StatusUsuarioBO', 'buscarStatusUsuario', { campos: { codigo: item.codigotipousuario} }).then(
                    function (result) {
                        $scope.statusUsuario = result.data;
                        $scope.itemForm = angular.copy(item);
                    },
                    $rootScope.TratarErro
                );
            }, 0);

            $scope.codigoAssociado = item.codigo;
            $scope.tableAssociados.page(1);
            $scope.tableAssociados.reload();

            $scope.formMode = true;
            $scope.searchMode = false;
            $scope.exibeVoltar = true;
            $scope.errorCPFCNPJ = false;
            $scope.initMode = false;

            $('#tabs-user a:first').tab('show');
            angular.element("#typea2").val('');
            $scope.isPesquisa = false;
            $scope.onSelectTipoUsuario(item.codigotipousuario, $scope.isPesquisa);
            $('#form :input:first').focus();
        });
    };

    $scope.onSelectTipoUsuario = function (tipousuario, isPesquisa) {

        genericService.postRequisicao('PerfilBO', 'buscarPerfilTipoUsuario', { where: { codigo: tipousuario} }).then(
            function (result) {
                $scope.perfisUsuario = result.data;

                if ($rootScope.usuarioLogado.id_tipousuario != 1 && $scope.associadoSelecionado) {
                    $scope.insereAssociadoLista($scope.associadoSelecionado);
                }

            },
            $rootScope.TratarErro
        );
    };

    $scope.onSearch = function (item) {

        if (item.empresaassociada) {
            if (!item.empresaassociada.codigo) {
                var mensagem = $translate.instant('10.MENSAGEM_EmpresaAssociadaNaoEncontrada');
                $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_EmpresaAssociadaNaoEncontrada' ? 'Empresa Associada não encontrada.' : mensagem);
                return false;
            }
        }

        $scope.validaEmail = false;
        $scope.form.submitted = false;
        $scope.formMode = false;
        $scope.isPesquisa = true;
        $scope.exibeVoltar = false;
        Utils.moveScrollTop();
        $scope.tableParams.page(1);
        $scope.tableParams.reload();
    };

    //Valida se CNPJ é válido
    $scope.validaCPF = function (cpf) {

        cpf = cpf.replace(/[^\d]+/g, '');
        if (cpf === '' || cpf === '00000000000' || cpf === '11111111111' || cpf === '22222222222' || cpf === '33333333333' || cpf === '44444444444' || cpf === '55555555555' || cpf === '66666666666' || cpf === '77777777777' || cpf === '88888888888' || cpf === '99999999999' || cpf.length !== 11) {
            return false;
        }
        function validateDigit(digit) {
            var add = 0;
            var init = digit - 9;
            for (var i = 0; i < 9; i++) {
                add += parseInt(cpf.charAt(i + init)) * (i + 1);
            }
            return (add % 11) % 10 === parseInt(cpf.charAt(digit));
        }
        return validateDigit(9) && validateDigit(10);
    };

    //Valida se CNPJ é válido
    $scope.validaCNPJ = function (c) {

        var b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
        c = c.replace(/[^\d]/g, '').split('');
        if (c.length !== 14) {
            return false;
        }

        for (var i = 0, n = 0; i < 12; i++) {
            n += c[i] * b[i + 1];
        }
        n = 11 - n % 11;
        n = n >= 10 ? 0 : n;
        if (parseInt(c[12]) !== n) {
            return false;
        }

        for (i = 0, n = 0; i <= 12; i++) {
            n += c[i] * b[i];
        }
        n = 11 - n % 11;
        n = n >= 10 ? 0 : n;
        if (parseInt(c[13]) !== n) {
            return false;
        }
        return true;
    };

    $scope.onSave = function (item) {
        $scope.form.submitted = true;

        var cpfcnpj = angular.element("#cpfcnpj").val();

        if (cpfcnpj != undefined && cpfcnpj != "" && !$scope.validaCNPJ(cpfcnpj) && !$scope.validaCPF(cpfcnpj)) {
            $scope.errorCPFCNPJ = true;
            var mensagem = $translate.instant('10.MENSAGEM_CPFCNPJInvalido');
            $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_CPFCNPJInvalido' ? 'Por favor, informar um CPF/CNPJ válido.' : mensagem);

            return false;
        }
        else {
            $scope.errorCPFCNPJ = false;
        }

        if (!$scope.form.$valid) {
            $('#liTabUser a').tab('show');
            var mensagem = $translate.instant('10.MENSAGEM_CamposObrigatorios');
            $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_CamposObrigatorios' ? 'Campos obrigatórios não preenchidos ou inválidos.' : mensagem);
            return false;
        }

        if (!verifyTableAssociados()) {
            return false;
        }

        var parametro = {
            Nome: item.nome,
            Email: item.email,
            CPFCNPJ: item.cpfcnpj,
            TelefoneResidencial: item.telefone,
            TelefoneCelular: item.celular,
            TelefoneComercial: item.comercial,
            Ramal: item.ramal,
            CodigoStatusUsuario: item.codigostatususuario,
            CodigoPerfil: item.codigoperfil,
            CodigoTipoUsuario: $scope.usuarioLogado.id_tipousuario == 1 ? item.codigotipousuario : $scope.usuarioLogado.id_tipousuario,
            Departamento: item.departamento,
            Cargo: item.cargo,
            MensagemObservacao: item.mensagemobservacao,
            arrayAssociados: $scope.associadosMarcados

        };

        if (item.codigo == undefined || item.codigo == null || item.codigo == 0) {

            InserirUsuario(parametro, function () {
                $scope.item = {};
                $scope.itemForm = {};
                $scope.perfisUsuario = {};
                $scope.tableParams.reload();
                $scope.form.submitted = false;
                $scope.formMode = false;
                $scope.exibeVoltar = false;
                $scope.isPesquisa = false;
                Utils.moveScrollTop();
                $scope.tableParams.page(1);
                $scope.tableParams.reload();

                var mensagem1 = $translate.instant('10.MENSAGEM_EmailInstrucoesAcesso');
                var mensagem2 = $translate.instant('10.MENSAGEM_CasoNaoRecebaEmail');

                $scope.alert(typeof mensagem === 'object' || (mensagem1 == '10.MENSAGEM_CamposObrigatorios' || mensagem2 == '10.MENSAGEM_CasoNaoRecebaEmail') ?
                    'Registro adicionado com sucesso. Um e-mail com instruções de acesso foi enviado para o endereço: ' + item.email +
                    '. Caso não receba o e-mail, entrar em contato com a GS1 Brasil.' : mensagem1 + ' ' + item.email + '. ' + mensagem2);
            });
        } else {
            AtualizarUsuario(item.codigo, parametro, function () {
                $scope.item = {};
                $scope.itemForm = {};
                $scope.perfisUsuario = {};
                $scope.tableParams.reload();
                $scope.form.submitted = false;
                $scope.formMode = false;
                $scope.exibeVoltar = false;
                $scope.isPesquisa = false;
                Utils.moveScrollTop();
                $scope.tableParams.page(1);
                $scope.tableParams.reload();
                var mensagem = $translate.instant('10.MENSAGEM_RegistroAlteradoSucesso');
                $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_RegistroAlteradoSucesso' ? 'Registro alterado com sucesso.' : mensagem);
            });
        }
    };

    $scope.onLoad = function (item) {

        genericService.postRequisicao('UsuarioBO', 'BuscarUsuarioLogado', '').then(
            function (result) {
                $scope.usuarioLogado = result.data;

                if ($scope.usuarioLogado.id_tipousuario == 1) {
                    $scope.typeUser = 'GS1';
                } else if ($scope.usuarioLogado.id_tipousuario == 2) {
                    $scope.typeUser = 'Associado';
                }
            },
            $rootScope.TratarErro
        );

        genericService.postRequisicao('TipoUsuarioBO', 'BuscarTipoUsuario', {}).then(
            function (result) {
                $scope.tiposUsuario = result.data;
            },
            $rootScope.TratarErro
        )

        $scope.tableParams = new ngTableParams({
            page: 1, count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'), sorting: { codigo: 'asc' }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {

                blockUI.start();
                var ordenacao = params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '') + ' ';
                ordenacao = ordenacao + params.sorting()[params.orderBy().toString().replace(/[^a-zA-Z 0-9]+/g, '')].toString();

                var dados = carregaUsuarios($scope.itemForm, params.page(), $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'), ordenacao, function (result) {
                    if (result.data && result.data.length > 0) {
                        params.total(result.data[0].totalregistros);
                    }
                    if (result.data.length == 0) {
                        params.total(0);
                    }

                    $defer.resolve(result.data);

                    blockUI.stop();
                });

            }
        });

        $scope.tableAssociados = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                //if ($scope.codigoAssociado != undefined) {
                var dados = carregaAssociados($scope.codigoAssociado, function (result) {
                    params.total(result.data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(result.data, params.orderBy()) : result.data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                });
                //}
            }
        });
    };

    $scope.onLoad($scope.item);

    function verifyTableAssociados() {

        $scope.associadosMarcados = [];

        for (var i = 0; i < $scope.tableAssociados.data.length; i++) {

            if ($scope.tableAssociados.data[i].checked == true) {
                $scope.associadosMarcados.push($scope.tableAssociados.data[i].codigo);
            }
        }

        if ($scope.associadosMarcados.length == 0 && $scope.itemForm.codigotipousuario != 1) {
            $('#liTabAssociados a').tab('show');
            var mensagem = $translate.instant('10.MENSAGEM_SelecioneUmAssociado');
            $scope.alert(typeof mensagem === 'object' || mensagem == '10.MENSAGEM_SelecioneUmAssociado' ? 'Selecione pelo menos uma empresa associada para continuar.' : mensagem);
            return false;
        }
        else {
            return true;
        }
    }

    //ASSOCIADOS USUARIO TIPO GS1
    $scope.insereAssociadoLista = function (associado) {

        var joinedData = {
            codigo: associado.codigo,
            nome: associado.nome,
            checked: 1
        }

        var exists = false;

        for (var i = 0; i < $scope.tableAssociados.data.length; i++) {
            if ($scope.tableAssociados.data[i].codigo == associado.codigo) {
                exists = true;
            }
        }

        if (!exists) {
            $scope.tableAssociados.data.push(joinedData);
            $scope.associadosMarcados.push(associado.codigo);
        }

        $timeout(function () {
            angular.element("#typea2").val('');
        }, 10);
    }

    $scope.onDeleteAssociadoTable = function (item) {
        for (var i in $scope.tableAssociados.data) {
            if ($scope.tableAssociados.data[i].codigo == item.codigo) {
                $scope.tableAssociados.data.splice(i, 1);
            }
        }
    }

    function findAndRemove(array, property, value) {
        $.each(array, function (index, result) {
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
            }
        });
    }

    //CONTROLE DE CHECKBOX
    $scope.flagMarcarAssociado = function (item) {
        if (item != undefined && item.checked === 1) {
            item.checked = false;
            $scope.removerPorId($scope.associadosMarcados, item.codigo);
        }
        else {
            if (item.checked) {
                $scope.associadosMarcados.push(item.codigo);
            }
            else {
                $scope.removerPorId($scope.associadosMarcados, item.codigo);
            }
        }
    }

    $scope.removerPorId = function (itens, codigo) {
        for (var i in itens) {
            if (itens[i] == codigo) {
                itens.splice(i, 1);
            }
        }
    }

    $scope.marcarDesmarcarAssociados = function (flag) {

        if (flag == true) {
            $scope.marcarTodosAssociados = true;
            $scope.associadosMarcados = [];
            angular.forEach($scope.tableAssociados.data, function (val, key) {
                $scope.associadosMarcados.push(val.codigo);
                val.checked = true;
            });
        } else {
            $scope.marcarTodosAssociados = false;
            $scope.associadosMarcados = [];
            angular.forEach($scope.tableAssociados.data, function (val, key) {
                val.checked = false;
            });
        }
    };

} ]);