﻿app.factory('UsuarioService', ['$q', '$http', '$location', function ($q, $http, $location) {
    var url = '/requisicao.ashx';

    return {
        buscarUsuarios: function (callback) {
            $http.get(url + '/UsuarioBO/BuscarUsuarios').success(function (data) {
                callback(data);
            })/*.error(function (data, status, headers, config) {
                console.log(config);
                $location.url('/page/403');
            })*/;
        }
    };
} ]);
