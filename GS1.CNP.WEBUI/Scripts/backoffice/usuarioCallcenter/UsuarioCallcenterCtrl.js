app.controller('UsuarioCallcenterCtrl', ['$scope', '$http', 'ngTableParams', '$timeout', '$location', '$rootScope', '$filter', '$modal', 'genericService', 'Utils', 'blockUI',
    function ($scope, $http, ngTableParams, $timeout, $location, $rootScope, $filter, $modal, genericService, Utils, blockUI) {
        $scope.isPesquisa = false;

        $scope.usuarios = [
            {codigo: 1, nome: 'SoftBox', cpf: 12345678910, email: 'softbox@softbox.com.br', perfil: 'Associado GS1', razao: 'GS1 BRASIL ASSOCIACAO BRASILEIRA DE AUTOMACAO', cnpj: 99999999999999, status: 'ativo', telResidencial: '(11) 1111-1111', telCelular: '(11) 1111-1111', telComercial: '(11) 1111-1111', ramal: 1111, departamento: 'IAE', cargo: 'Admin'},
            {codigo: 2, nome: 'SoftBox', cpf: 12345678910, email: 'softbox@softbox.com.br', perfil: 'Associado GS1', razao: 'GS1 BRASIL ASSOCIACAO BRASILEIRA DE AUTOMACAO', cnpj: 99999999999999, status: 'ativo', telResidencial: '(11) 1111-1111', telCelular: '(11) 1111-1111', telComercial: '(11) 1111-1111', ramal: 1111, departamento: 'IAE', cargo: 'Admin'},
            {codigo: 3, nome: 'SoftBox', cpf: 12345678910, email: 'softbox@softbox.com.br', perfil: 'Associado GS1', razao: 'GS1 BRASIL ASSOCIACAO BRASILEIRA DE AUTOMACAO', cnpj: 99999999999999, status: 'ativo', telResidencial: '(11) 1111-1111', telCelular: '(11) 1111-1111', telComercial: '(11) 1111-1111', ramal: 1111, departamento: 'IAE', cargo: 'Admin'},
            {codigo: 4, nome: 'SoftBox', cpf: 12345678910, email: 'softbox@softbox.com.br', perfil: 'Associado GS1', razao: 'GS1 BRASIL ASSOCIACAO BRASILEIRA DE AUTOMACAO', cnpj: 99999999999999, status: 'ativo', telResidencial: '(11) 1111-1111', telCelular: '(11) 1111-1111', telComercial: '(11) 1111-1111', ramal: 1111, departamento: 'IAE', cargo: 'Admin'},
            {codigo: 5, nome: 'SoftBox', cpf: 12345678910, email: 'softbox@softbox.com.br', perfil: 'Associado GS1', razao: 'GS1 BRASIL ASSOCIACAO BRASILEIRA DE AUTOMACAO', cnpj: 99999999999999, status: 'ativo', telResidencial: '(11) 1111-1111', telCelular: '(11) 1111-1111', telComercial: '(11) 1111-1111', ramal: 1111, departamento: 'IAE', cargo: 'Admin'}
        ];

        $scope.tableUsuarios = new ngTableParams({
            page: 1,
            count: $rootScope.retornaValorParametroByFiltro('grid.resultados.por.pagina'),
            sorting: {
                LOGIN: 'asc'
            }
        }, {
            counts: [],
            total: 0,
            getData: function ($defer, params) {
                var dados = carregaGrid($scope.item, $scope.isPesquisa, function (data) {
                    params.total(data.length);
                    var orderedData = params.sorting() ? $filter('orderBy')(data, params.orderBy()) : data;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()))
                    $scope.items = data;
                });
            }
        });

        function carregaGrid(item, isPesquisa, callback) {
            if (isPesquisa == false) {
                var data = $scope.usuarios;
                callback(data);

                //produto.CarregarTodosProdutos(function (data) {
                //    callback(data);
                //});
            }
            else if (isPesquisa == true) {
                var data = $scope.usuarios;
                callback(data);
                //produto.CarregarProdutosPesquisa($scope.item, $scope.tags, function (data) {
                //    callback(data);
                //});
            }
        };

        $scope.onFormMode = function (item) {

                $scope.item = {};
                $scope.editMode = true;
                $scope.searchMode = false;
                $scope.isCollapsedMidias = true;
        };

        $scope.onEdit = function (item) {
            $scope.item = angular.copy(item);

            $scope.editMode = true;
            $scope.searchMode = false;
            $scope.isPesquisa = false;
        };

        $scope.onCancel = function (item) {
            $scope.item = {};
            $scope.editMode = false;
            $scope.searchMode = false;
            $scope.form.submitted = false;
            $scope.isPesquisa = false;

            $scope.tableUsuarios.page(1);
            $scope.tableUsuarios.reload();
        };

        $scope.onSearchMode = function (item) {
            $scope.editMode = true;
            $scope.searchMode = true;
        };

        $scope.onClean = function (item) {
            $scope.item = {};
        };
}]);
