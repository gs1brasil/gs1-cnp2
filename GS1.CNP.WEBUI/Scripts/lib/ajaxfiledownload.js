if (typeof String.prototype.startsWith != 'function') {
    // see below for better implementation!
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) == 0;
    };
}

if (typeof handleError === 'undefined' || !$.isFunction(handleError)) {
    jQuery.extend({
        handleError: function (s, xhr, status, e) {
            // If a local callback was specified, fire it
            if (s.error)
                s.error(xhr, status, e);
            // If we have some XML response text (e.g. from an AJAX call) then log it in the console
            else if (xhr.responseText && window.console)
                console.log(xhr.responseText);
        }
    });
}

jQuery.extend({
    createDownloadForm: function (id, data) {
        //create form	
        var formId = 'jDownloadForm' + id;
        var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '"></form>');

        if (data) {
            for (var i in data) {
                jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
            }
        }

        //set attributes
        jQuery(form).css('position', 'absolute');
        jQuery(form).css('top', '-1200px');
        jQuery(form).css('left', '-1200px');
        jQuery(form).appendTo('body');

        return form;
    },

    ajaxFileDownload: function (s) {
        var id = new Date().getTime()
        var form = jQuery.createDownloadForm(id, (typeof (s.data) == 'undefined' ? false : s.data));
        var formId = 'jDownloadForm' + id;

        var requestDone = false;

        try {

            var form = jQuery('#' + formId);

            jQuery(form).attr('action', s.url);
            jQuery(form).attr('method', 'POST');

            jQuery(form).submit();

            setTimeout(function () {
                if (document.cookie.toLowerCase().indexOf('filedownload') > -1) {
                    //remove cookie
                    document.cookie = "filedownload=; path=/; expires=" + new Date(0).toUTCString() + ";";

                    //remove form
                    jQuery('#' + formId).remove();
                }
            }, 500);
        } catch (e) {
            jQuery.handleError(s, xml, null, e);
        }

        return { abort: function () { } };
    }
});