﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using System.Web.SessionState;
using System;
using GS1.CNP.BLL.Core;

namespace GS1.CNP.WEBUI
{
    public class UploadHandler : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection SelectedFiles = context.Request.Files;
                List<string> list = new List<string>();
                List<byte> listBlob = new List<byte>();

                string referer = context.Request.ServerVariables["HTTP_REFERER"],
                    fileName,
                    folder,
                    newFolderName,
                    tipoArquivo,
                    extensao;
                HttpPostedFile PostedFile;

                if (context.Request.Form.AllKeys.Contains("RefererFullUrl"))
                    referer = context.Request.Form["RefererFullUrl"];

                if (referer.ToLower().Contains("uploadcartas")) {
                    folder = @"/upload/Cartas/";
                    newFolderName = folder + System.Guid.NewGuid().ToString();
                    tipoArquivo = "template";
                }
                else if (referer.ToLower().Contains("banners"))
                {
                    folder = @"/upload/Banners/";
                    newFolderName = folder + System.Guid.NewGuid().ToString();
                    tipoArquivo = "imagem";
                }
                else {
                    folder = @"/upload/Imagens/";
                    newFolderName = folder + System.Guid.NewGuid().ToString();
                    tipoArquivo = "imagem";
                }

                if (!Directory.Exists(context.Server.MapPath(newFolderName)))
                    Directory.CreateDirectory(context.Server.MapPath(newFolderName));

                for (int i = 0; i < SelectedFiles.Count; i++)
                {
                    PostedFile = SelectedFiles[i];
                    extensao = Path.GetExtension(PostedFile.FileName);

                    //TODO: arrumar exceptions
                    if ((tipoArquivo == "imagem" && (extensao.ToLower() == ".png" || extensao.ToLower() == ".jpg")) || 
                        (tipoArquivo == "template" && (extensao.ToLower() == ".doc" || extensao.ToLower() == ".docx" || extensao.ToLower() == ".dot" || extensao.ToLower() == ".dotx")))
                    {

                        //Upload no banco de dados usando o campo tipo Blob
                        if (context.Request.QueryString["TipoUpload"] != null && context.Request.QueryString["TipoUpload"].ToLower().Equals("blob")) 
                        {
                            listBlob = Util.uploadBlobDatabase(context, SelectedFiles);
                        }
                        //Upload usando serviço de blob do Azure
                        else if (context.Request.QueryString["TipoUpload"] != null && context.Request.QueryString["TipoUpload"].ToLower().Equals("blobazure")) 
                        {
                            list.Add(Util.uploadBlobAzure(PostedFile));
                        }
                        //Upload de arquivo físico na pasta do projeto, inserindo o caminho da imagem salva no banco de dados   
                        else if (context.Request.QueryString["TipoUpload"] != null && context.Request.QueryString["TipoUpload"].ToLower().Equals("fisico"))
                        {
                            try
                            {
                                fileName = newFolderName + "/" + System.Guid.NewGuid().ToString() + "." + PostedFile.FileName.Split('.').Last();
                                PostedFile.SaveAs(context.Server.MapPath(fileName));
                                list.Add(fileName);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Não foi possível fazer o upload da imagem.");
                            }
                        }
                        else
                        {
                            throw new Exception("Não foi possível fazer o upload da imagem.");
                        }

                        //Verifica se o template é válido
                        if (tipoArquivo == "template")
                        {
                            Util util = new Util();
                            //List<string> erros = util.ValidateWordDocument(context.Server.MapPath(Convert.ToString(list[0])));
                            //bool verify = ValidateWordDocument(context, list);
                            List<string> erros;

                            try
                            {
                                erros = util.ValidateWordDocument(context.Server.MapPath(Convert.ToString(list[0])));
                            }
                            catch(Exception) {
                                erros = util.ValidateWordDocument(Convert.ToString(list[0]));
                            }

                            //if (!verify) {
                            if (erros != null && erros.Count > 0)
                            {
                                File.Delete(context.Server.MapPath(PostedFile.FileName));

                                list.Clear();
                                //list.Add("Template inválido");

                                var campos = String.Empty;

                                for (var j = 0; j < erros.Count; j++)
                                {
                                    campos += erros[j];

                                    if (j == erros.Count - 2)
                                    {
                                        campos += " ou ";
                                    }
                                    else
                                    {
                                        campos += " , ";
                                    }
                                }

                                campos = campos.Substring(0, campos.Length - 3);
                                list.Add(campos);
                                //throw new Exception("Os campos " + campos + "não foram encontrados no template. Por favor selecione um template válido.");
                            }
                        }

                    }
                    else
                    {
                        list.Add("Extensão não suportada");
                    }
 
                }

                context.Response.Clear();
                context.Response.ContentType = "text/plain";

                if (context.Request.QueryString["TipoUpload"] != null && context.Request.QueryString["TipoUpload"].Equals("blob")) {
                    context.Response.Write(JsonConvert.SerializeObject(listBlob));
                }
                else {
                    context.Response.Write(JsonConvert.SerializeObject(list));
                }

                context.Response.End();
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("[]");
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}

