﻿using System.Web;
using System.Web.SessionState;
using GS1.CNP.BLL;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for VcardHandler
    /// </summary>
    public class VcardHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            VcardGLNBO vcard = new VcardGLNBO();
            vcard.DownloadVcard(context.Request.Form["codigolocalizacaofisica"]);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}