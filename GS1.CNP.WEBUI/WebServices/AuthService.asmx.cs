﻿using System.ComponentModel;
using System.Web.Services.Protocols;
using System.Web.Services;
using SGN.Model.Acoes;
//using SGN.Model.Acoes.Decorators;
using SGN.Model.Core.Entities.Model.Headers;
using SGN.Model.Core.Entities;
using System.Data.SqlClient;
//using SGN.Model.Core.Util;
using System.Configuration;
using System.Web.Security;
using System.Linq;

namespace SGN.Web.WebServices
{
    [WebService(Namespace = "http://www.gs1br.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AuthService : WebService
    {

        public CnpServicesHeader RequestHeader { get; set; }

        public void ValidateHeader(CnpServicesHeader _header)
        {
            if (_header == null)
                throw new System.UnauthorizedAccessException();

            string _allowedGLN;
            string[] _allowedUsers;
            _allowedGLN = ConfigurationManager.AppSettings["SearchAllowedGLN"];
            _allowedUsers = ConfigurationManager.AppSettings["WSUSER"].Split(';');
            if (!_header.RequesterGln.Equals(_allowedGLN) || !_allowedUsers.Any(x => x.Equals(_header.WSUSER)))
                throw new System.UnauthorizedAccessException();
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        public bool Validate(string email, string password)
        {
            ValidateHeader(RequestHeader);
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["BD"].ConnectionString);
            try
            {
                sqlConn.Open();
                string sql = "SELECT CODIGO FROM USUARIO (Nolock) WHERE EMAIL = '" + email + "' AND SENHA = '" + FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1") + "'";
                SqlCommand slqCom = new SqlCommand(sql, sqlConn);
                SqlDataReader dr = slqCom.ExecuteReader();
                while (dr.Read())
                {
                    return true;
                }

                return false;
            }
            catch (System.Exception)
            {
                sqlConn.Close();
                throw;
            }

        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        public User GetUser(string email)
        {

            ValidateHeader(RequestHeader);
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["BD"].ConnectionString);
            try
            {
                sqlConn.Open();
                User user = new User();
                string sqlusuario = @"SELECT
	                                    cast(0 as bit) AS AlreadyProvedDelinquencyStatus
	                                    ,[Codigo] AS IdUsuario
                                        ,[Nome] AS NomeCompleto
	                                    ,[CPFCNPJ] AS CpfCnpj
	                                    ,[Email] AS Email
	                                    ,[CodigoPerfil] AS IdPerfil
	                                    ,cast(1 as bit) AS FlagTermoAceite
	                                    ,[QuantidadeTentativa] AS TentativaAcesso
	                                    ,[DataCadastro] AS DtInclusao
	                                    ,DATEADD(DAY,(SELECT CAST(Z.VALOR AS int) FROM Parametro Z WHERE Z.CHAVE = 'autenticacao.senha.nudias.validade'),[DataCadastro]) AS DtExpiraSenha
	                                    ,NULL AS IdUsuarioGestor
	                                    , (SELECT TOP 1 Z.CODIGOASSOCIADO FROM AssociadoUsuario Z WHERE Z.CodigoUsuario = A.CODIGO ORDER BY Z.IndicadorPrincipal DESC) as IdAssociado
	                                    ,[CodigoStatusUsuario] AS Status
	                                    ,[TelefoneResidencial] AS TelResidencial
	                                    ,[TelefoneCelular] AS TelCelular
                                        ,[TelefoneComercial] AS TelComercial
	                                    ,[Ramal]
                                        ,[Departamento]
                                        ,[Cargo]
	                                    , null as DtConvite
                                        , null  as DtConfirmacao          
                                    FROM [dbo].[Usuario] A (Nolock) 
                                    WHERE EMAIL = '" + email + "'";
                SqlCommand slqComUsuario = new SqlCommand(sqlusuario, sqlConn);
                SqlDataReader drUsuario = slqComUsuario.ExecuteReader();

                Usuario usuario = MontaUsuario(drUsuario);
                user.Usuario = usuario;
                if (usuario != null && usuario.IdAssociado != null)
                {
                    user.Associado = BuscaAssociado(usuario.IdAssociado.ToString());
                    return user;
                }

                return null;

            }
            catch (System.Exception ex)
            {

                throw;
            }

        }


        [WebMethod]
        [SoapHeader("RequestHeader")]
        public User GetUserByCpf(string CPFCNPJ)
        {
            ValidateHeader(RequestHeader);
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["BD"].ConnectionString);
            try
            {
                sqlConn.Open();
                User user = new User();
                string sqlusuario = @"SELECT
	                                    cast(0 as bit) AS AlreadyProvedDelinquencyStatus
	                                    ,[Codigo] AS IdUsuario
                                        ,[Nome] AS NomeCompleto
	                                    ,[CPFCNPJ] AS CpfCnpj
	                                    ,[Email] AS Email
	                                    ,[CodigoPerfil] AS IdPerfil
	                                    ,cast(1 as bit) AS FlagTermoAceite
	                                    ,[QuantidadeTentativa] AS TentativaAcesso
	                                    ,[DataCadastro] AS DtInclusao
	                                    ,DATEADD(DAY,(SELECT CAST(Z.VALOR AS int) FROM Parametro Z WHERE Z.CHAVE = 'autenticacao.senha.nudias.validade'),[DataCadastro]) AS DtExpiraSenha
	                                    ,NULL AS IdUsuarioGestor
	                                    , (SELECT TOP 1 Z.CODIGOASSOCIADO FROM AssociadoUsuario Z WHERE Z.CodigoUsuario = A.CODIGO ORDER BY Z.IndicadorPrincipal DESC) as IdAssociado
	                                    ,[CodigoStatusUsuario] AS Status
	                                    ,[TelefoneResidencial] AS TelResidencial
	                                    ,[TelefoneCelular] AS TelCelular
                                        ,[TelefoneComercial] AS TelComercial
	                                    ,[Ramal]
                                        ,[Departamento]
                                        ,[Cargo]
	                                    , null as DtConvite
                                        , null  as DtConfirmacao          
                                    FROM [dbo].[Usuario] A (Nolock) 
                                    WHERE CPFCNPJ = '" + CPFCNPJ + "'";
                SqlCommand slqComUsuario = new SqlCommand(sqlusuario, sqlConn);
                SqlDataReader drUsuario = slqComUsuario.ExecuteReader();

                Usuario usuario = MontaUsuario(drUsuario);
                user.Usuario = usuario;
                if (usuario != null && usuario.IdAssociado != null && usuario.IdAssociado.ToString() != string.Empty)
                {
                    user.Associado = BuscaAssociado(usuario.IdAssociado.ToString());
                    return user;
                }

                return null;

            }
            catch (System.Exception ex)
            {

                throw;
            }

        }

        private Associado BuscaAssociado(string IdUsuario)
        {
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["BD"].ConnectionString);
            sqlConn.Open();
            string sqlassociado = @"SELECT DISTINCT TOP 1 
	                                   A.[Codigo] AS IdAssociado
                                      ,A.[CPFCNPJ] AS CpfCnpj
	                                  ,A.[Nome] AS RazaoSocial
	                                  ,A.[CodigoStatusAssociado] AS Status
	                                  ,A.[CodigoSituacaoFinanceira] AS StatusPagto
	                                  ,B.Nome AS ContatoMaster
	                                  ,b.email AS EmailContato
	                                  ,NULL AS DtVencTaxa
	                                  ,[CEP]
	                                  ,[Endereco]
	                                  ,[Numero]
	                                  ,[Complemento]
	                                  ,[Bairro]
                                      ,[Cidade]
                                      ,[UF]
	                                  ,A.[DataAtualizacaoCRM] AS DtUltimaSinc
	                                  ,[CodigoTipoCompartilhamento] AS CompartilhaDado
                                      ,InscricaoEstadual
                                      ,Pais
                                  FROM [DBO].[ASSOCIADO] A (Nolock)
                                  LEFT JOIN CONTATOASSOCIADO B (Nolock) 
                                    ON B.CODIGOASSOCIADO = A.CODIGO AND B.INDICADORPRINCIPAL = 1
                                  WHERE A.CODIGO = " + IdUsuario;
            SqlCommand slqComAssociado = new SqlCommand(sqlassociado, sqlConn);
            SqlDataReader drAssociado = slqComAssociado.ExecuteReader();

            return MontaAssociado(drAssociado);
        }

        private Usuario MontaUsuario(SqlDataReader drUsuario)
        {
            Usuario usuario = new Usuario();
            while (drUsuario.Read())
            {

                usuario.AlreadyProvedDelinquencyStatus = drUsuario.GetBoolean(0);
                usuario.IdUsuario = drUsuario.GetInt64(1);
                usuario.NomeCompleto = drUsuario.GetString(2);

                if (!drUsuario.IsDBNull(3))
                    usuario.CpfCnpj = drUsuario.GetString(3);

                usuario.Email = drUsuario.GetString(4);
                usuario.IdPerfil = drUsuario.GetInt32(5);

                if (!drUsuario.IsDBNull(6))
                    usuario.FlagTermoAceite = drUsuario.GetBoolean(6);

                usuario.TentativaAcesso = drUsuario.GetInt32(7);
                usuario.DtInclusao = drUsuario.GetDateTime(8);
                usuario.DtExpiraSenha = drUsuario.GetDateTime(9);

                if (!drUsuario.IsDBNull(10))
                    usuario.IdUsuarioGestor = drUsuario.GetInt32(10);

                if (!drUsuario.IsDBNull(11))
                    usuario.IdAssociado = drUsuario.GetInt64(11);

                usuario.Status = drUsuario.GetInt32(12).ToString();

                if (!drUsuario.IsDBNull(13))
                    usuario.TelResidencial = drUsuario.GetString(13);
                if (!drUsuario.IsDBNull(14))
                    usuario.TelCelular = drUsuario.GetString(14);
                if (!drUsuario.IsDBNull(15))
                    usuario.TelComercial = drUsuario.GetString(15);
                if (!drUsuario.IsDBNull(16))
                    usuario.Ramal = drUsuario.GetString(16);
                if (!drUsuario.IsDBNull(17))
                    usuario.Departamento = drUsuario.GetString(17);
                if (!drUsuario.IsDBNull(18))
                    usuario.Cargo = drUsuario.GetString(18);

                if (!drUsuario.IsDBNull(19))
                    usuario.DtConvite = drUsuario.GetDateTime(19);

                if (!drUsuario.IsDBNull(19))
                    usuario.DtConfirmacao = drUsuario.GetDateTime(20);
            }
            drUsuario.Close();

            return usuario;
        }
        private Associado MontaAssociado(SqlDataReader drAssociado)
        {
            Associado associado = new Associado();
            while (drAssociado.Read())
            {

                associado.IdAssociado = drAssociado.GetInt64(0);
                associado.CpfCnpj = drAssociado.GetString(1);
                associado.RazaoSocial = drAssociado.GetString(2);


                if (drAssociado.GetInt32(3) == 1)
                    associado.Status = "A";

                if (drAssociado.GetInt32(3) == 2)
                    associado.Status = "S";

                if (drAssociado.GetInt32(3) == 3)
                    associado.Status = "C";

                if (drAssociado.GetInt32(4) == 1)
                {
                    associado.StatusPagto = "A";
                }
                else
                {
                    associado.StatusPagto = "P";
                }

                if (!drAssociado.IsDBNull(5))
                    associado.ContatoMaster = drAssociado.GetString(5);

                if (!drAssociado.IsDBNull(6))
                    associado.EmailContato = drAssociado.GetString(6);

                if (!drAssociado.IsDBNull(7))
                    associado.DtVencTaxa = drAssociado.GetDateTime(7);

                associado.CEP = drAssociado.GetString(8);
                associado.Endereco = drAssociado.GetString(9);


                if (!drAssociado.IsDBNull(10))
                {
                    decimal value;
                    if (decimal.TryParse(drAssociado.GetString(10), out value))
                    {
                        associado.Numero = value;
                    }
                }

                if (!drAssociado.IsDBNull(11))
                    associado.Complemento = drAssociado.GetString(11);

                associado.Bairro = drAssociado.GetString(12);
                associado.Cidade = drAssociado.GetString(13);
                associado.UF = drAssociado.GetString(14);


                if (!drAssociado.IsDBNull(15))
                    associado.DtUltimaSinc = drAssociado.GetDateTime(15);

                if (drAssociado.GetInt32(16) == 2)
                {
                    associado.CompartilhaDado = "S";
                }
                else if (drAssociado.GetInt32(16) == 1)
                {
                    associado.CompartilhaDado = "N";
                }

                if (!drAssociado.IsDBNull(17))
                    associado.InscricaoEstadual = drAssociado.GetString(17);

                if (!drAssociado.IsDBNull(18))
                    associado.Pais = drAssociado.GetString(18);

                //associado.DescStatus  = drAssociado.GetString(19);
                //associado.ErroIntegracao  = drAssociado.GetString(20);
            }
            drAssociado.Close();
            return associado;
        }




    }
}
