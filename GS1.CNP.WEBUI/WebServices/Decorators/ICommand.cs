﻿namespace SGN.Model.Acoes
{
    public interface ICommand<T>
    {
        void GerarChave(T request);
    }

    public interface IComputationCommand<T>
    {
        object Execute(T request);
    }
}