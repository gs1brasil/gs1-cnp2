﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using Intentor.Yamapper;
using Microsoft.Practices.Unity;
using SGN.Model.Core.Entities;
using SGN.Model.Core.Entities.Model.Gepir;
using SGN.Model.Core.Entities.Model.Headers;
using SGN.Model.Core.Util;
using SGN.Model.Core.Views;
using SGN.Model.Domain;
using SGN.Model.Facade;
using XPI.Common;

namespace SGN.Web.WebServices
{
    [WebService(Namespace = "http://www.gs1br.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class GepirServices : WebService
    {
        public GepirHeader RequestHeader { get; set; }
        public GepirResponseHeader ResponseHeader { get; set; }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public GepirItem GetItemByGTIN(string requestedGTIN)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            try
            {
                if (!ValidateHeader() || !ValidateGTIN(requestedGTIN))
                    return null;

                var criteria = new Criteria();
                criteria.Add(Expression.Equal("Gtin", requestedGTIN));

                var results = RelatorioFacade.GetRelProdutoItemByCriteria(criteria);

                ResponseHeader.numberOfHits = 0;
                if (IsResultEmpty(results)) 
                    return null;

                ResponseHeader.returnCode = 0;

                results = GetFilteredResults(results, requestedGTIN);

                var gepirItem = BuildGepirItem(results);
                ResponseHeader.numberOfHits = ((results != null)) ? results.Count : 0 ;
                return gepirItem;
            }
            catch (Exception)
            {
                ResponseHeader.numberOfHits = 0;
                ResponseHeader.returnCode = 99;
                return null;
            }
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public RelProdutoItemView GetCNPItemByGTIN(string gtin)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            try
            {
                if (!ValidateHeader() || !ValidateGTIN(gtin))
                    return null;

                var criteria = new Criteria();

                criteria.Add(Expression.Equal("Gtin", gtin));

                var gtinResult = RelatorioFacade.GetRelProdutoItemByCriteria(criteria).FirstOrDefault();

                if (gtinResult == null)
                {
                    ResponseHeader.returnCode = 8;
                    return null;
                }

                var list = new List<RelProdutoItemView> {gtinResult};
                list = GetFilteredResults(list, gtin);
                if (!list.IsEmpty())
                {
                    gtinResult = list.First();
                    ResponseHeader.numberOfHits = 1;
                }
                else
                    return null;

                if (gtinResult.TipoGtin.HasValue && gtinResult.TipoGtin.Value == 8 && string.IsNullOrWhiteSpace(gtinResult.NrPrefixo))
                    gtinResult.NrPrefixo = gtinResult.Gtin;
                
                ResponseHeader.returnCode = 0;
                
                return gtinResult;
            }
            catch (Exception)
            {
                ResponseHeader.numberOfHits = 0;
                ResponseHeader.returnCode = 99;
                return null;
            }
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public GepirExclusiveItem GetExclusiveItemByGTIN(string requestedGTIN)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };

            try
            {
                if (!ValidateHeader() || !ValidateGTIN(requestedGTIN))
                    return null;

                var criteriaGtin = new Criteria();
                criteriaGtin.Add(Expression.Equal("Gtin", requestedGTIN));
                var gtinResults = RelatorioFacade.GetRelProdutoItemByCriteria(criteriaGtin);
                var certificadoResults = RelatorioFacade.GetRelCertificadoByCriteria(criteriaGtin);

                ResponseHeader.numberOfHits = 0;
                if (IsResultEmpty(gtinResults)) return null;
                if (!RequestHeader.IsExclusive) return null;

                ResponseHeader.returnCode = 0;

                var company = ContainerFactory.GetDomainContainer().Resolve<AssociadoBiz>().GetById(gtinResults.First().IdAssociado);
                if (CompanyIsOverdue(company))
                {
                    // deixar a chamada pronta caso haja necessidade de mudar as informações de registro no RGC
                    //var gln = ContainerFactory.GetDomainContainer().Resolve<AssociadoGLNBiz>().GetAll().FirstOrDefault(x => x.IdAssociado == gtinResults.First().IdAssociado);
                    try
                    {
                        new RGC.WSGS1004().GS1GRAVACONSULTA("MSALPHA", company.CpfCnpj, string.Empty, "GTIN", requestedGTIN, string.Empty, string.Empty, string.Empty);
                    }
                    catch (Exception) { }
                }

                var gepirItem = BuildGepirExclusiveItem(gtinResults, certificadoResults);
                ResponseHeader.numberOfHits = gtinResults.Count;
                return gepirItem;
            }
            catch (Exception)
            {
                ResponseHeader.numberOfHits = 0;
                ResponseHeader.returnCode = 99;
                return null;
            }
        }

        [WebMethod]
        [SoapHeader("RequestHeader")]
        [SoapHeader("ResponseHeader", Direction = SoapHeaderDirection.Out)]
        public GepirParty GetPartyByGLN(string requestedGLN)
        {
            ResponseHeader = new GepirResponseHeader
            {
                responderGln = "7898357410008"
            };
            try
            {
                if (!ValidateHeader() || !ValidateGLN(requestedGLN))
                    return null;

                var criteria = new Criteria();
                criteria.Add(Expression.Equal("NrGln", requestedGLN));

                var results = RelatorioFacade.GetRelLocalizacaoFisicaByCriteria(criteria);

                if (IsResultEmpty(results)) return null;

                ResponseHeader.returnCode = 0;
                var company = ContainerFactory.GetDomainContainer().Resolve<AssociadoBiz>().GetById(results.First().IdAssociado);

                if (CompanyIsOverdue(company))
                {
                    // deixar a chamada pronta caso haja necessidade de mudar as informações de registro no RGC
                    //var gln = ContainerFactory.GetDomainContainer().Resolve<AssociadoGLNBiz>().GetAll().FirstOrDefault(x => x.IdAssociado == results.First().IdAssociado);

                    try
                    {
                        new RGC.WSGS1004().GS1GRAVACONSULTA("MSALPHA", company.CpfCnpj, string.Empty, "GLN", requestedGLN, string.Empty, string.Empty, string.Empty);
                    }
                    catch (Exception) { }
                }

                ResponseHeader.numberOfHits = results.Count;
                return BuildParty(results);
            }
            catch (Exception)
            {
                ResponseHeader.returnCode = 99;
                ResponseHeader.numberOfHits = 0;
                return null;
            }
        }

        private ItemTradeItemUnitDescriptor resolveItemTradeItemUnitDescriptor(int idTipoProduto)
        {
            switch (idTipoProduto)
            {
                case 1:
                    return ItemTradeItemUnitDescriptor.PALLET;
                case 2:
                    return ItemTradeItemUnitDescriptor.BASE_UNIT_OR_EACH;
                case 3:
                    return ItemTradeItemUnitDescriptor.CASE;
                default:
                    return ItemTradeItemUnitDescriptor.BASE_UNIT_OR_EACH;
            }
        }

        private GepirExclusiveItem BuildGepirExclusiveItem(IEnumerable<RelProdutoItemView> produtos, IEnumerable<RelCertificadoView> relCertificados)
        {
            var gepirItem = BuildGepirItem(produtos);
            var gepirExclusiveItem = new GepirExclusiveItem();
            var exclusiveItems = new List<ExclusiveItem>();

            foreach (var item in gepirItem.itemDataLine)
            {
                var certificados = relCertificados.Where(x => x.Gtin == item.gtin);
                foreach (var certificado in certificados)
                {
                    exclusiveItems.Add(new ExclusiveItem
                    {
                        gtin = item.gtin,
                        informationProviderGln = item.informationProviderGln,
                        itemName = item.itemName,
                        brandName = item.brandName,
                        tradeItemUnitDescriptor = item.tradeItemUnitDescriptor,
                        childItems = item.childItems,
                        ImageUrl = item.ImageUrl,
                        UrlMobilecom = item.UrlMobilecom,
                        netContent = item.netContent,

                        CertificateName = certificado.DescricaoStatus.Equals("SEM CERTIFICADO", StringComparison.InvariantCultureIgnoreCase)
                            ? certificado.DescricaoStatus : certificado.NomeCertificado,
                        AssignDate = certificado.DataAtribuicao,
                        ExpirationDate = certificado.DataExpiracao,
                        Status = certificado.Status,

                    });
                }
            }
            gepirExclusiveItem.itemDataLine = exclusiveItems.ToArray();
            return gepirExclusiveItem;
        }

        private GepirItem BuildGepirItem(IEnumerable<RelProdutoItemView> produtos)
        {
            var item = new GepirItem();
            var itemDataLines = new List<Item>();

            if (produtos != null)
                foreach (var produto in produtos)
                {
                    if (produto.TipoGtin.HasValue && produto.TipoGtin.Value == 8 && string.IsNullOrWhiteSpace(produto.NrPrefixo))
                        produto.NrPrefixo = produto.Gtin;

                    var inferioresResult = ContainerFactory.GetDomainContainer().Resolve<ProdutoGtinInferiorBiz>().GetByGtin(produto.Gtin);

                    var inferiores = inferioresResult.Select(i => new ItemChildItem
                    {
                        gtin = i.GtinInferior,
                        numberContained = i.QtdeItens.Value.ToString()
                    }).ToList();

                    var urlImage = string.IsNullOrEmpty(produto.UrlFoto1)
                        ? (string.IsNullOrEmpty(produto.UrlFoto2) ? produto.UrlFoto3 : produto.UrlFoto2)
                        : produto.UrlFoto1;

                    itemDataLines.Add(new Item
                    {
                        gtin = produto.Gtin,
                        informationProviderGln = "7898357410008",
                        itemName = produto.Descricao,
                        brandName = produto.MarcaProduto,
                        tradeItemUnitDescriptor = resolveItemTradeItemUnitDescriptor(produto.IdTipoProduto.HasValue ? produto.IdTipoProduto.Value : 0),
                        childItems = inferiores.ToArray(),
                        ImageUrl = urlImage,
                        UrlMobilecom = produto.UrlMobilecom,
                        netContent = produto.PesoLiquido.HasValue ? new ItemNetContent { uom = UomType.KGM, Value = produto.PesoLiquido.Value.ToString() } : null
                    });
                }

            item.itemDataLine = itemDataLines.ToArray();
            return item;
        }

        private GepirParty BuildParty(IEnumerable<RelLocalizacaoFisicaView> companies)
        {
            var gepirParty = new GepirParty();
            var partyDataLines = new List<Party>();
            foreach (var c in companies)
            {
                partyDataLines.Add(
                    new Party
                    {
                        returnCode = "0",
                        gln = c.NrGln,
                        additionalPartyId = new[] { c.CpfCnpj },
                        partyName = c.RazaoSocial,
                        streetAddress = new[] { c.Endereco },
                        postalCode = c.CEP,
                        city = c.Cidade,
                        countryISOCode = c.Pais
                    });
            }

            gepirParty.partyDataLine = partyDataLines.ToArray();
            return gepirParty;
        }

        private bool IsResultEmpty(ICollection results)
        {
            if (results.Count != 0) return false;

            ResponseHeader.returnCode = 8;
            return true;
        }

        private RelProdutoItemView FilterResultsBasedOnSharingInformation(RelProdutoItemView result)
        {
            if (result.CompartilhaDado != null && result.CompartilhaDado.Equals("S"))
                return result;
            
            ResponseHeader.returnCode = 12;
            return null;
        }        

        private List<RelProdutoItemView> FilterResultsBasedOnSharingInformation(IEnumerable<RelProdutoItemView> results)
        {
            var list = new List<RelProdutoItemView>();

            Parallel.ForEach(results, currentResult =>
            {
                var filteredResult = FilterResultsBasedOnSharingInformation(currentResult);

                if (filteredResult != null)
                    list.Add(filteredResult);
            });

            return list;
        }

        private static bool CompanyIsOverdue(Associado company)
        {
            return (company.Status == null || !company.Status.Equals("A")) || (company.StatusPagto == null || !company.StatusPagto.Equals("A"));
        }

        private static bool CompanyNeverShareInfo(Associado company)
        {
            return (company.CompartilhaDado != null && company.CompartilhaDado.Equals("N"));
        }

        private static bool CompanyAlwaysShareInfo(Associado company)
        {
            return (company.CompartilhaDado != null && company.CompartilhaDado.Equals("S"));
        }

        private bool ValidateGTIN(string value)
        {
            if (value.Length == 14 || value.Length == 13 || value.Length == 8)
                return true;

            ResponseHeader.returnCode = 1;
            return true;
        }

        private bool ValidateGLN(string value)
        {
            if (value.Length == 13)
                return true;

            ResponseHeader.returnCode = 1;
            return false;
        }

        private bool ValidateHeader()
        {
            if (RequestHeader == null || string.IsNullOrEmpty(RequestHeader.RequesterGln))
            {
                ResponseHeader.returnCode = 1;
                return false;
            }
            if (!InformedUserIsValid() || !InformedGlnIsValid())
            {
                ResponseHeader.returnCode = 1;
                return false;
            }

            return ValidateGLN(RequestHeader.RequesterGln);
        }

        private bool InformedGlnIsValid()
        {
            return ConfigurationManager.AppSettings["SearchAllowedGLN"].Equals(RequestHeader.RequesterGln);
        }

        private bool InformedUserIsValid()
        {
            return ConfigurationManager.AppSettings.Get("WSUSER").Split(';').Any(x => x.Equals(RequestHeader.WSUSER));
        }

        private List<RelProdutoItemView> GetFilteredResults(List<RelProdutoItemView> results, string requestedGTIN)
        {
            if (RequestHeader.IsExclusive)
                return results;

            var company = ContainerFactory.GetDomainContainer().Resolve<AssociadoBiz>().GetById(results.First().IdAssociado);

            if (CompanyIsOverdue(company))
            {
                // deixar a chamada pronta caso haja necessidade de mudar as informações de registro no RGC
                //var gln = ContainerFactory.GetDomainContainer().Resolve<AssociadoGLNBiz>().GetAll().FirstOrDefault(x => x.IdAssociado == results.First().IdAssociado);

                try
                {
                    new RGC.WSGS1004().GS1GRAVACONSULTA("MSALPHA", company.CpfCnpj, string.Empty, "GTIN", requestedGTIN, string.Empty, string.Empty, string.Empty);
                }
                catch (Exception) { }

                ResponseHeader.returnCode = 11;
                return null;
            }
            
            if (CompanyNeverShareInfo(company))
            {
                ResponseHeader.returnCode = 9;
                return null;
            }
            
            if (CompanyAlwaysShareInfo(company))
                return results;
            
            return FilterResultsBasedOnSharingInformation(results);
        }
    }
}
