﻿using System.Web.Services.Protocols;

namespace SGN.Model.Core.Entities.Model.Headers
{
    public class CnpServicesHeader : SoapHeader
    {
        public string RequesterGln { get; set; }
        public string WSUSER { get; set; }
    }
}