﻿namespace SGN.Model.Core.Entities.Model.Headers
{
    public class GepirHeader : CnpServicesHeader
    {
        public bool IsExclusive { get; set; }
    }
}