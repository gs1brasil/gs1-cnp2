﻿using System.Web.Services.Protocols;

namespace SGN.Model.Core.Entities.Model.Headers
{
    public class GepirResponseHeader : SoapHeader
    {
        public string responderGln { get; set; }
        public int numberOfHits { get; set; }
        public int returnCode { get; set; }
    }
}