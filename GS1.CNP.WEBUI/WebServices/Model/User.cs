﻿using SGN.Model.Core.Entities;

namespace SGN.Model.Acoes
{
    public class User
    {
        public Usuario Usuario { get; set; }
        public Associado Associado { get; set; }
    }
}