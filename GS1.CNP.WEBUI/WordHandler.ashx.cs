﻿using System;
using System.Web;
using System.Web.SessionState;
using GS1.CNP.BLL.Core;
using GS1.CNP.BLL;
using Newtonsoft.Json;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for WordHandler
    /// </summary>
    public class WordHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (context.Request.Form.AllKeys[0] == "worddatacartaprodutos")
                {
                    CartasCertificadasBO cartas = new CartasCertificadasBO();
                    cartas.GerarCarta(JsonConvert.DeserializeObject(context.Request.Form["worddatacartaprodutos"]));
                }
                else
                {
                    CartasCertificadasGs1BO cartasGs1 = new CartasCertificadasGs1BO();
                    cartasGs1.GerarCarta(JsonConvert.DeserializeObject(context.Request.Form["worddata"]));
                }
            }
            catch (Exception ex)
            {
                Log.WriteError(ex.Message, ex);
                context.Response.Write("<script>alert('"+ex.Message+"');</script>");      
            }
            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
