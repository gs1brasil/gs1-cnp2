﻿using System.Web;
using System.Web.SessionState;
using GS1.CNP.BLL;
using Newtonsoft.Json;

namespace GS1.CNP.WEBUI
{
    /// <summary>
    /// Summary description for XmlHandler
    /// </summary>
    public class XmlHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            ProdutoBO produto = new ProdutoBO();
            produto.GerarXMLProduto(JsonConvert.DeserializeObject(context.Request.Form["xmldata"]).ToString());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
