'use strict';
angular.module('ng-multiselecttree', [])
  .directive('tree', function($compile, $log) {
    return {
      restrict: 'E',
      terminal: true,
      scope: {
        maxnumberchildren: '@',
        root: '@',
        fieldid: '@',
        fieldlabel: '@',
        fieldchildren: '@',
        val: '=?',
        fetch: '=?',
        selected: '='
      },

      link: function(scope, element, attrs) {
        scope.onCheckChange = function() {
          scope.val.checked = !scope.val.checked;

          var _findChecked = function(node, result) {
            if (!angular.isDefined(result)) {
              result = []
            }

            if (node.checked === true && !node.isGrouping) {
              result.push(node.getId());
            }

            angular.forEach(node.children, function(child) {
              _findChecked(child, result);
            });

            return result
          };

          scope.val.rootScope.selected = _findChecked(scope.val.rootScope.val);
        };

        scope.expand = function(val) {
          val.show = !val.show;

          if (val.hasChildren && val.rootScope.fetch && val.children == null) {
            val.isLoading = true
            val.rootScope.fetch(val.getId(), function(fetchedItems) {
              var items = [];
              var index = 0;
              var fetchedItem = null;
              var label = null;
              var hasChildren = false;

              angular.forEach(fetchedItems, function(_fetchedItem) {
                fetchedItem = _fetchedItem[0];
                hasChildren = _fetchedItem[1];

                index = fetchedItem.lastIndexOf("/") > -1 ? fetchedItem.lastIndexOf("/") : fetchedItem.lastIndexOf("\\");
                label = fetchedItem.substr(index + 1);
                items.push(createTreeData(fetchedItem, label, null, val.rootScope, hasChildren, false, false));
              });

              _setChildren(val, items);
              val.isLoading = false;
            });
          }
        };

        function _subDivideChildren(val, items) {
          if (items.length > val.rootScope.maxnumberchildren) {
            var groups = _subDivideChildrenIntoGroups(val.rootScope, items, 1);
            var result = [];
            var label = ''

            angular.forEach(groups, function(items, group) {
              if (items.length == 1) {
                result.push(items[0]);
              } else {
                label = group + '... [' + items.length + ' items]';
                result.push(createTreeData(val.id + '.' + group, label, items, val.rootScope, true, false, true));
              }
            });

            return result.sort(function(a, b) {
              return _getLabel(a) - _getLabel(b);
            });

          } else {
            return items;
          }
        }

        function _subDivideChildrenIntoGroups(rootScope, items, numberChars) {
          var groups = {};

          angular.forEach(items, function(item) {
            var prefix = angular.lowercase(_getLabel(item).substr(0, numberChars));

            if (!(prefix in groups)) {
              groups[prefix] = [];
            }

            groups[prefix].push(item)
          });

          angular.forEach(groups, function(items, group) {
            if (items.length > rootScope.maxnumberchildren && numberChars < 10) {
              delete groups[group];
              var newGroups = _subDivideChildrenIntoGroups(rootScope, items, numberChars + 1)
              angular.forEach(newGroups, function(items, group) {
                groups[group] = items;
              });
            }
          });

          return groups
        }

        scope.$watch('root', function(root, oldRoot) {
          if (root) {
            initialiseRootScope(scope);
            scope.val = createTreeData(root, root, null, scope, true, true, false);
          }
        });

        function createTreeData(id, label, children, rootScope, hasChildren, isRoot, isGrouping) {
          var treeData = {
            isRoot: isRoot,
            rootScope: rootScope,
            hasChildren: hasChildren,
            _children: children,
            children: children,
            show: false,
            checked: false,
            isLoading: false,
            isGrouping: isGrouping,
            filterText: null,
            getId: function() {
              return treeData[treeData.rootScope.fieldid];
            }
          };

          treeData[rootScope.fieldid] = id;
          treeData[rootScope.fieldlabel] = label;

          return treeData;
        }

        function _refresh(treeData) {
          var filteredItems = []
          if (treeData.filterText != null) {
            angular.forEach(treeData._children, function(item) {
              if (_getLabel(item).indexOf(treeData.filterText) == 0) {
                filteredItems.push(item);
              }
            });
          } else {
            filteredItems = treeData._children;
          }
          treeData.children = treeData.rootScope.maxnumberchildren > 0 ? _subDivideChildren(treeData, filteredItems) : filteredItems
        }

        function _getLabel(treeData) {
          return treeData[treeData.rootScope.fieldlabel];
        }

        function _setChildren(treeData, children) {
          treeData._children = children;
          _refresh(treeData)
        }

        function initialiseStaticTree(val, scope) {
          if (!angular.isDefined(val.rootScope)) {
            initialiseRootScope(scope);
            val.rootScope = scope;

            var _recurse = function(node, rootScope) {
              if (!angular.isDefined(node.show)) {
                node.show = false;
              }
              if (!angular.isDefined(node.checked)) {
                node.checked = false;
              }
              if (!angular.isDefined(node.rootScope)) {
                node.rootScope = rootScope;
              }

              node.getId = function() {
                return node[node.rootScope.fieldid];
              }

              var items = node[node.rootScope.fieldchildren]
              if (angular.isDefined(items)) {
                node.hasChildren = true;
                _setChildren(node, items);
                for (var i in items) {
                  _recurse(items[i], rootScope);
                }
              }
            };
            _recurse(val, val.rootScope);
          }
        }

        function initialiseRootScope(rootScope) {
          rootScope.maxnumberchildren = angular.isDefined(rootScope.maxnumberchildren) ? parseInt(rootScope.maxnumberchildren) : 25;
          rootScope.fieldid = angular.isDefined(rootScope.fieldid) ? rootScope.fieldid : 'id';
          rootScope.fieldlabel = angular.isDefined(rootScope.fieldlabel) ? rootScope.fieldlabel : 'label';
          rootScope.fieldchildren = angular.isDefined(rootScope.fieldchildren) ? rootScope.fieldchildren : 'items';
        }

        scope.$watch('val.filterText', function(val, old) {
          if (val != null) {
            _refresh(scope.val);
          }
        });

        scope.$watch('val', function(val, oldVal) {
          if (val && angular.isDefined(val)) {
            initialiseStaticTree(val, scope);

            var nt = val.isGrouping ? 'span' : 'ul';
            var template = ''
            template += val.isRoot ? '<div class="treeView">' : '';
            template += '<div class="treeItem">';
            template += '  <input ng-if="!val.isGrouping" type="checkbox" ng-click="onCheckChange()" class="treeItemCheck">';
            template += '  <a ng-if="val.hasChildren " href="" ng-click="expand(val)" class="treeItemLink" title="{{ val.getId() }}">{{ val[val.rootScope.fieldlabel] }}</a>'
            template += '  <input ng-if="val._children.length > 5" type="text" placeholder="filter" ng-model="val.filterText" class="treeItemFilter">';
            template += '  <span ng-if="!val.hasChildren"  title="{{ val.getId() }}">{{ val[val.rootScope.fieldlabel]}}</span>'            
            template += '  <' + nt + ' ng-if="val.show" class="treeItemChildren">';
            template += '    <li ng-repeat="item in val.children track by item.getId()" class="treeItemChild_{{ item.hasChildren }}">';
            template += '      <tree val="item" fetch="fetch"></tree>';
            template += '    </li>';
            template += '  </' + nt + '>';
            template += '  <div class="treeItemLoading" ng-show="val.isLoading">Loading...</div>';
            template += '</div>';
            
            template += val.isRoot ? '</div>' : '';

            var newElement = angular.element(template);
            $compile(newElement)(scope);
            element.replaceWith(newElement);
            element = newElement;

          }
        });
      }
    };
  });

//todo - Delimiter isnt ideal
//todo - process the data on the background.