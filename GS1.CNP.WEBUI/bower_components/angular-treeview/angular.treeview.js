/*
	@license Angular Treeview version 0.1.6
	ⓒ 2013 AHN JAE-HA http://github.com/eu81273/angular.treeview
	License: MIT


	[TREE attribute]
	angular-treeview: the treeview directive
	tree-id : each tree's unique id.
	tree-model : the tree model on $scope.
	node-id : each node's id
	node-label : each node's label
	node-children: each node's children

	<div
		data-angular-treeview="true"
		data-tree-id="tree"
		data-tree-model="roleList"
		data-node-id="roleId"
		data-node-label="roleName"
		data-node-children="children" >
	</div>
*/

(function ( angular ) {
	'use strict';

	angular.module( 'angularTreeview', [] ).directive( 'treeModel', ['$compile', function( $compile ) {
		return {
			restrict: 'A',
			link: function ( scope, element, attrs ) {
                
				//tree id
				var treeId = attrs.treeId;

				//tree model
				var treeModel = attrs.treeModel;
                
				//node id
				var nodeId = attrs.nodeId || 'id';

				//node label
				var nodeLabel = attrs.nodeLabel || 'label';

				//children
				var nodeChildren = attrs.nodeChildren || 'children';         
                

				var template =  '<div ng-repeat="node in ' + treeModel + '"' +
                                '    ng-class="{\'tree-folder\' : node.' + nodeChildren + '.length > 0, \'tree-item\': node.' + nodeChildren + '.length == 0, \'tree-selected\' : node.selected}" ' +
                                '    ng-click="node.' + nodeChildren + '.length || ' + treeId + '.selectNodeLabel(node)" ng-if="node.' + nodeChildren + ' != null">' +
                                '	<div class="tree-folder-header" ng-if="node.' + nodeChildren + '.length" ng-click="' + treeId + '.selectNodeHead(node)">' +
								'		<i ng-class="{\'icon-plus\' : node.collapsed, \'icon-minus\': !node.collapsed}"></i> ' +
								'		<div class="tree-folder-name"> {{node.' + nodeLabel + '}} </div> ' +
								'	</div> ' +

                                '  <i ng-if="!node.' + nodeChildren + '.length" class="icon-circle"></i> ' +
                                '  <div ng-if="!node.' + nodeChildren + '.length" class="tree-item-name"> {{node.' + nodeLabel + '}} <span ng-if="node.total != null" class="pull-right badge badge-info">{{node.total}}</span> </div> ' +

								'	<div class="tree-folder-content"  data-ng-hide="node.collapsed" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + '> ' +
								'	</div> ' +
								'</div> ';

				//check tree id, tree model
				if( treeId && treeModel ) {
                    
					//root node
					if( attrs.angularTreeview ) {

						//create tree object if not exists
						scope[treeId] = scope[treeId] || {};

						//if node head clicks,
						scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function( selectedNode ){

							//Collapse or Expand
							selectedNode.collapsed = !selectedNode.collapsed;
						};

						//if node label clicks,
						scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function( selectedNode ){

							//remove highlight from previous node
							if( scope[treeId].currentNode && scope[treeId].currentNode.selected ) {
								scope[treeId].currentNode.selected = undefined;
							}

							//set highlight to selected node
							selectedNode.selected = 'selected';

							//set currentNode
							scope[treeId].currentNode = selectedNode;
						};
					}

					if($(element).closest(".tree").length == 0) {
                        
						var $elem = $('<div class="tree tree-selectable"></div>').append($compile( template )( scope ));
						element.html($elem);
					} else {
                        
						element.html('').append($compile( template )( scope ));
					}
				}
			}
		};
	}]);
})( angular );
