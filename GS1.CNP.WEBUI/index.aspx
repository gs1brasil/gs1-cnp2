﻿ <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="GS1.CNP.WEBUI.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base href="/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GS1 CNP</title>
    <meta name="description" content="O Cadastro Nacional de Produtos é uma ferramenta online desenvolvida pela GS1 Brasil - Associação Brasileira de Automação para auxiliar as empresas no registro de seus produtos, facilitando a gestão e o controle da numeração de mercadorias e impressão do código de barras.">
    <meta name="keywords" content="www gs1br org, códigos de barras, codigo EAN produto,
        gs1br org, gerador código de barras online, upc/ean code,
        gs1 databar, código de barra, ean bar codes, 
        gs1, codigo barra, código upc,
        gs1 128, codico de barra, upc bar code,
        GS1 datamatrix, criando codigo de barras, codes ean,
        GS1 codigo barras, como criar codigo de barra, EAN 8,
        gs1 cadastro nacional de produtos, como criar um codigo de barra, ean,
        gs1 código barras, colocar código barras, ean bar code, 
        gs1 enad, criar codigo barras, ean gs1,
        gs1 automação, codigo de bara, EAN brasil,
        cadastro gs1, modelo codigo barras, ean code boek,
        cadastrar empresa gs1, codigo barras oficial, Cadastro nacional de produtos,
        como filiar gs1, tipos codigos barra, gestão,
        gs1 cadastro online, codigode baras, administração,
        filiaçao online gs1, tipos codigos barras, GTIN, 
        gs1 filiação, codigo barra oficial, EAN-13,
        gs1 associar, codigo barras certificado, CNP
        como obter registro código de barras gs1, qrcode pro produto, produtos 
        registro gs1, codigo setor saude, item,
        pré cadastro gs1, aplicar codigo barra, item logístico,
        cadastrar empresa gs1, provedor codigo barras,	
        adquirir codigo de barras gs1, codigo remedio, 			
        empregar codigo barra, implantar codigo barra, codigo 2d, 
        fornecedor codigo barras, botar codigo barra, implementar codigo barra"> 
    <meta name="author" content="GS1 Brasil"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Language" content="pt-BR">   
    <link href="Styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Scripts/npm.js" rel="stylesheet">
    <link href="Styles/login.css" rel="stylesheet">
    <link href="Styles/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/bower_components/angular-block-ui/dist/angular-block-ui.min.css" />

    <style>
        .gotham-font-stack {
            font-family: "Gotham SSm A", "Gotham SSm B", Verdana, sans-serif;
        }

        .gotham-narrow-font-stack {
            font-family: "Gotham Narrow SSm A", "Gotham Narrow SSm B", Verdana, sans-serif;
        }

        .gotham-alt-font-stack {
            font-family: "Gotham A", "Gotham B", Verdana, sans-serif;
        }
        a:link {
	        color: #939598;
	        text-decoration: none;
        }
        a:hover {
	        color: #939598;
	        text-decoration: none;
        }
        a:visited {
	        text-decoration: none;
        }
        a:active {
	        text-decoration: none;
        }
        a:focus {
            outline: 0;
        }
        .header{
	        width:100%;
	        font-size:24px;
	        color:#6D6E71;
	        position: fixed;
	        background: #FFFFFF no-repeat scroll center center;
	        z-index: 200;
	        margin: 0;
        }
        .container.bg
        {
            background-position: center top;
            min-height: 535px;
        }
        .topTitleLogin {
            margin-top: 10px;
        }
        .form-box
        {
            top: 110px;
            position: relative;
        }
        .form-box .more-info-link
        {
            color: #428bca;
            text-decoration: underline;
        }
    </style>

</head>
<body ng-app="CNP.Backoffice" ng-controller="LoginCtrl" class="login-layout toolkit" ng-init="cnpjMode = false; viewForgot = false;">
    <div class="row-fluid">
    	<div class="row header" >
        	<div class="col-sm-12 col-md-3 col-lg-3 logoCadastro">
            	<center><img class="tamanhoImagemLogo" src="/images/login/logoCadastro.png"></center>
            </div>
            <div class="col-sm-12 col-md-9 col-lg-9 textoCadastro gotham-alt-font-stack">
            	<p class="textoLogo" style="vertical-align:middle;">Uma ferramenta simples e dinâmica que <strong>centraliza o cadastro de GTIN</strong></p>
            </div>
        </div>
    
    	<div class="container bg" ng-init="viewLogin = true; exibeCPFCNPJ = false; viewForgot = false; viewFAQ = false;" ng-cloak>
        	<div id="rowForm" class="row formularioLogin animate-show" style="display:none;">
            	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-7"></div>
                <div class="col-sm-8 col-md-7 col-lg-4">
            		<div id="login" class="form-box" ng-show="viewLogin && !viewForgot && !viewFAQ">
                        <div>
                            <h3 class="red"><strong>Fique atento!</strong></h3>
                            <p>Mantenha sempre os dados dos seus produtos completos e atualizados. A partir de setembro de 2017, a <strong>Secretaria da Fazenda</strong> passará a validar o GTIN informado na Nota Fiscal.</p>
                            <p><a class="more-info-link" href="http://www.gs1br.org/cadastro-centralizado-de-gtin">Quero saber mais</a></p>
                            <hr/>
                        </div>
                        <a class="pull-right" style="float:right;text-decoration: none;" href="#" ng-click="viewFAQ = true; viewLogin = false; viewForgot = false;"> <%--ng-show="faqs != undefined && faqs.length > 0"--%>
                            <i class="icon-question-sign" title="FAQ"></i>
                        </a>
                		<img src="/images/login/login.png"> <div class="tituloFormulario gotham-alt-font-stack">Login </div>
                        <p class="fontenormal gotham-alt-font-stack topTitleLogin">Entre com seus dados:</p>
                        <form>
  							<div class="form-group">
    							<label class="fontenormal gotham-font-stack">Usuário</label>
    							<input type="email" class="form-control" placeholder="E-mail" id="user" name="user" ng-model="usuario.login" maxlength="255" required />
  							</div>
  							<div class="form-group">
    							<label for="exampleInputEmail1" class="fontenormal gotham-font-stack">Senha</label>
    							<input type="password" class="form-control" placeholder="Senha" id="pwd" name="pwd" ng-model="usuario.senha" maxlength="255" on-enter="onLogin()" required />
  							</div>
                            <div class="form-group" ng-show="exibeCPFCNPJ == true" ng-cloak>
                                <label for="cpfcnpj" class="fontenormal gotham-font-stack">CPF/CNPJ</label>
                                <input type="text" class="form-control" placeholder="CPF/CNPJ" id="cpfcnpj" name="cpfcnpj" on-enter="onLogin()" ng-model="usuario.cpfcnpj" required  onkeydown="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, ''); return (event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" onblur="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, '');" onkeyup="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, '');" />
                            </div>
                            <div class="form-group" ng-show="exibeCAPTCHA == true">
                                <div vc-recaptcha key="'6LeVSAwTAAAAAGEN6LLRvqFuzMhquH_NJIBjZu7a'" on-success="setResponse(response)"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button class="btn btn-primary" ng-click="onLogin()" type="button" style="width:100%; margin-bottom:5px;">
                                        Entrar
                                    </button>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button type="button" class="btn btn-danger" ng-click="onClean()" style="width:100%;">
                                        Cancelar
                                    </button>
                                </div>
                           	    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 bot-pass-w" style="text-decoration: none; padding-top: 0;">
  									<button type="button" class="btn btn-link btn-xs" ng-click="e.preventDefault(); viewLogin = false; viewForgot = true;">
                                        <img src="/images/login/cadeado.png"/> 
                                        <span style="color: #939598">Esqueci minha senha</span>
                                    </button>
                                </div>
                                <div class="col-md-4"></div>
                           	</div>
                            <div class="row" ng-controller="CheckVersionCtrl">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-decoration: none; padding-top: 15px;" ng-if="showVersionMessage">
                                    <span style="color: #939598">Recomendado o uso dos seguintes navegadores: <br />Chrome (latest), Firefox (latest) ou IE 10+</span>
                                </div>
                            </div>
						</form>
                	</div>
                    
                    <div id="reset" class="form-box" ng-show="viewLogin != undefined && viewForgot != undefined && viewFAQ != undefined && viewForgot" ng-cloak>
                		<img alt="" src="/images/login/login.png" /> <div class="tituloFormulario gotham-alt-font-stack">Recuperar Senha</div>
                        <p class="fontenormal gotham-alt-font-stack">Insira o endereço de e-mail cadastrado:</p>
                        <form name="formRecuperar">
  							<div class="form-group">
    							<label class="fontenormal gotham-font-stack">E-mail</label>
    							<input type="email" class="form-control" on-enter="enviarEmail()" ng-pattern="/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/" ng-model="usuario.email" placeholder="E-mail" />
  							</div>
                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button class="btn btn-primary" ng-click="enviarEmail()" type="button" style="width:100%; margin-bottom:5px;">
                                        Enviar
                                    </button>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button type="button" class="btn btn-danger" ng-click="onClean(); viewLogin = true; viewForgot = false; viewFAQ = false;" style="width:100%;">
                                        Voltar
                                    </button>
                                </div>
                                <div class="col-md-4"></div>
                           	</div>
						</form>
                	</div>

                    <div id="faq" class="form-box" ng-show="viewFAQ != undefined && !viewLogin && !viewForgot" ng-cloak style="overflow-y: auto; max-height: 650px;">
                		<i class="icon-question-sign icon-4x" style="color: #6d6e71;">
                            <div class="tituloFormulario gotham-alt-font-stack" style="margin-top: -47px;">
                                Perguntas Frequentes - FAQ
                                <div class="btn-group pull-right btnHeader" style="margin-top: 7px;">
                                    <button type="button" class="btn btn-info dropdown-toggle btn-xs pull-right" data-toggle="dropdown" ng-cloak>
                                        <span class="hidden-xs hidden-sm"><i class="fa icon-comments"></i> {{nomeidioma}}</span> <i class="fa fa-chevron-down"></i>
                                    </button>
                                    <ul class="dropdown-menu dropdown-user" role="menu" style="padding: 5px 9px 16px; width: 180px;">                          
                                        <li ng-click="alteraIdioma(idioma.codigo, idioma.descricao);" style="cursor: pointer; margin-top: 8px;" ng-repeat="idioma in idiomas">
                                            &nbsp; {{idioma.nome}} ({{idioma.descricao}}) 
                                            &nbsp;<i class="fa icon-ok" ng-if="codigoidioma == idioma.codigo"></i> 
                                        </li>                          
                                    </ul>
                                </div>
                            </div>
                        </i> 
                        <form name="formRecuperar" style="margin-top: 25px;">
                            
                            <div class="row">
                                <%--<div style="margin-top: 12px; margin-bottom: 12px;" class="col-md-12">
                                    <label for="idioma">Idioma </label>
                                    <select class="form-control input-sm" id="codigoidioma" name="codigoidioma" ng-model="codigoidioma" ng-change="alteraIdioma(codigoidioma)"
                                        ng-options="idioma.codigo as idioma.nome for idioma in idiomas">
                                    </select>
                                </div>--%>
                                <div class="bs-example" ng-if="faqs != undefined && faqs.length > 0" style="padding: 8px">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default" ng-repeat="item in faqs" ng-init="isOpen = false">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#{{$index}}" target="_self" style="text-decoration: none;" ng-click="isOpen = !isOpen">
                                                    <h4 class="panel-title" ng-bind="item.pergunta" style="color: #454444"></h4>
                                                    <i class="pull-right" ng-class="{'font-orange': isOpen, 'icon-chevron-up': !isOpen, 'icon-chevron-down': isOpen}" style="margin-top: -17px;"></i>
                                                </a>
                                            </div>
                                            <div id="{{$index}}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p ng-bind-html="renderHtml(item.resposta)"></p>
                                                    <div ng-if="item.video != undefined && item.video != ''">
                                                        <br>
                                                        <h5>Vídeo Explicativo: </h5>
                                                        <iframe style="z-index:0!important;" id="iframe_youtube" width="523" height="388" ng-src="{{item.video}}" frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-example" ng-if="faqs == undefined || faqs.length == 0" style="margin: 12px;">
                                    <span>Nenhuma Pergunta Frequente Registrada</span>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <button type="button" class="btn btn-danger" ng-click="onClean(); viewLogin = true; viewForgot = false; viewFAQ = false;" style="width:100%;">
                                        Voltar
                                    </button>
                                </div>
                                <div class="col-md-4"></div>
                           	</div>
						</form>
                	</div>
                </div>
                <div class="col-sm-12 col-md-1 col-lg-1"></div>
            </div>

            <div id="Div1" class="row formularioLogin animate-show" style="display:none;">
            	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-7"></div>
                <div class="col-sm-8 col-md-7 col-lg-4">

                </div>
            </div>
        </div>

        <footer class="row-fluid text-right rodape">
            Powered by <img alt="" src="/images/login/rodape.png">
        </footer>
    </div>

    <!-- bower:js -->
    <script  type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/scripts/bootstrap.min.js" type="text/javascript"></script>

    <script  type="text/javascript" src="bower_components/angular/angular.js"></script>
    <script  type="text/javascript" src="bower_components/angular-cookies/angular-cookies.js"></script>
    <script  type="text/javascript" src="bower_components/angular-resource/angular-resource.js"></script>
    <script  type="text/javascript" src="bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script  type="text/javascript" src="bower_components/angular-route/angular-route.js"></script>
    <script  type="text/javascript" src="bower_components/angular-animate/angular-animate.js"></script>
    <script  type="text/javascript" src="bower_components/ng-table/ng-table.js"></script>
    <script type="text/javascript" src="/bower_components/angular-bootstrap/ui-bootstrap.js"></script>
    <script src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js" type="text/javascript"></script>

    <script  type="text/javascript" src="/bower_components/angular-block-ui/dist/angular-block-ui.min.js"></script> 
    <script type="text/javascript" src="/bower_components/ngScrollTo/ng-scrollto.js"></script>

    <script src="/Scripts/lib/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/Scripts/lib/ng-ckeditor/ng-ckeditor-1.0.1.min.js" type="text/javascript"></script>

    <script src="/bower_components/angular-x2js/dist/x2js.min.js" type="text/javascript"></script>
    <script src="/bower_components/x2js/xml2json.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/bower_components/angular-colorpicker/js/bootstrap-colorpicker-module.js"></script>
    <script src="/bower_components/angular-wysiwyg/dist/angular-wysiwyg.js" type="text/javascript"></script>

    <!-- Include the JS ReCaptcha API -->
    <script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>


    <%--<%= JSNLog.JavascriptLogging.Configure() %>--%>

    <script type="text/javascript">
        function show_box(id) {
            jQuery('.widget-box.visible').removeClass('visible');
            jQuery('.' + id).addClass('visible');
        }
    </script>

    <script>
        $('document').ready(function () {
            $(".sidebar").addClass("menu-min");

            $('.navbar-nav').on('click', 'li', function () {
                $('.navbar-nav li.active').removeClass('active');
                $(this).addClass('active');
            });

            $(".dropdown").on("mouseover", function () {
                $(this).addClass("open");
                $(this).addClass("active");
            });

            $(".dropdown").on("mouseout", function () {
                $(this).removeClass("open");
                $(this).removeClass("active");
            });
           
        });
    </script>

    <script src="/bower_components/angular-recaptcha/release/angular-recaptcha.js" type="text/javascript"></script>
    
    <!-- Angular - Google Maps API -->
    <script type="text/javascript" src="/bower_components/lodash/lodash.min.js" ></script>
    <script type="text/javascript" src="/bower_components/angular-simple-logger/dist/angular-simple-logger.min.js" ></script>
    <script type="text/javascript" src="/bower_components/angular-google-maps/dist/angular-google-maps.js" ></script>
    <script src="/bower_components/ngstorage/ngStorage.min.js" type="text/javascript"></script>
    <script src="/bower_components/angular-translate/angular-translate.min.js" type="text/javascript"></script>
    <script src="/bower_components/angular-translate-storage-local/angular-translate-storage-local.min.js" type="text/javascript"></script>
    <script src="/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js" type="text/javascript"></script>
    <script src="/bower_components/angular-translate-handler-log/angular-translate-handler-log.min.js" type="text/javascript"></script>
    <%--<script src="/bower_components/logToServer.js/logToServer.js" type="text/javascript"></script>--%>

    <!-- build:js({.tmp,app}) scripts/scripts.js -->
    <script src="/scripts/backoffice/app.js" type="text/javascript"></script>
    <script src="/scripts/backoffice/core/app.directives.js" type="text/javascript"></script>

    <script type="text/javascript" src="/scripts/lib/mask.js"></script>
    <script type="text/javascript" src="/assets/js/angular-input-masks/masks.js"></script>

    <script src="scripts/recaptcha.config.js" type="text/javascript"></script>   
    <script type="text/javascript" src="/scripts/backoffice/core/app.factory.js"></script>
    <script  type="text/javascript" src="scripts/backoffice/core/controllers/checkversionctrl.js"></script>
    <script  type="text/javascript" src="scripts/backoffice/core/controllers/loginctrl.js"></script>
    <script  type="text/javascript" src="scripts/backoffice/core/services/loginservice.js"></script>
    <script  type="text/javascript" src="scripts/backoffice/core/services/genericservice.js"></script>
    
    <script src="/bower_components/ngstorage/ngStorage.min.js" type="text/javascript"></script>
</body>
</html>
