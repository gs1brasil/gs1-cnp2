﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login_bkp.aspx.cs" Inherits="GS1.CNP.WEBUI._default" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>GS1 Trade</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- basic styles -->

        <link href="styles/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="styles/font-awesome.min.css" />
        <link rel="stylesheet" href="bower_components/angular-block-ui/angular-block-ui.min.css" />

        <!--[if IE 7]>
          <link rel="stylesheet" href="styles/font-awesome-ie7.min.css" />
        <![endif]-->

        <!-- page specific plugin styles -->

        <!-- fonts -->

        <link rel="stylesheet" href="styles/ace-fonts.css" />

        <!-- ace styles -->

        <link rel="stylesheet" href="styles/ace.min.css" />
        <link rel="stylesheet" href="styles/ace-rtl.min.css" />

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="styles/ace-ie.min.css" />
        <![endif]-->
        <link rel="stylesheet" href="styles/main.css" />

        <!-- inline styles related to this page -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script  type="text/javascript" src="scripts/lib/html5shiv.js"></script>
        <script  type="text/javascript" src="scripts/lib/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .login-layout { background-color: #fff; }
            .login-box .toolbar{ background-color: #002C6C; }            
            .login-box .forgot-password-link { color: #BEC8D6; }
            .header.blue, .blue { color: #002C6C !important; }
            .btn-primary, .btn-primary:focus { background-color: #002C6C !important; border-color: #002C6C; }
        </style>
        <script  type="text/javascript" src="bower_components/angular/angular.js"></script>
    </head>

    <body ng-app="CNP.Backoffice" ng-controller="LoginCtrl" class="login-layout" ng-init="cnpjMode = false">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center" style="padding-top:20px">
                                <img src="/images/logoGS1.png"/>
                                <!--<h1><span class="white">Guia de Parceiros</span></h1>-->
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="icon-barcode blue"></i>
                                                Entre com os seus dados
                                            </h4>
                                            <div class="space-6"></div>

                                            <form name="form" novalidate class="form" role="form">
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <input type="text" class="form-control input-sm" placeholder="E-mail" id="user" name="user" ng-model="usuario.login" required />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <input type="password" class="form-control input-sm" placeholder="Senha" id="pwd" name="pwd" ng-model="usuario.senha" required />
                                                        </div>
                                                    </div> 
                                                    
                                                    <div class="row" ng-show="exibeCPFCNPJ == true"  ng-cloak>
                                                        <div class="form-group col-md-12">
                                                            <input type="text" class="form-control input-sm" placeholder="CPF/CNPJ" id="cpfcnpj" name="cpfcnpj" ng-model="usuario.cpfcnpj" required  onkeydown="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, ''); return (event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" onblur="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, '');" onkeyup="this.value=this.value.replace(/[^a-zA-Z 0-9]+/g, '');" />
                                                        </div>
                                                    </div>  
                                                    
                                                    <div class="row" ng-show="exibeCAPTCHA == true" >
                                                        <div id="recaptcha"></div>                                                                                                        
                                                    </div>

                                                    <div class="space"></div>

                                                    <div class="clearfix">
                                                        <button class="width-35 pull-right btn btn-sm btn-primary" type="button" ng-click="onLogin()">
                                                            <i class="icon-key"></i>
                                                            Entrar
                                                        </button>
                                                    </div>

                                                    <div class="space-4"></div>
                                                </fieldset>
                                            </form>
                                        </div><!-- /widget-main -->

                                        <div class="toolbar clearfix">
                                            <div>
                                                <a href="#" onclick="show_box('forgot-box'); return false;" class="forgot-password-link">
                                                    <i class="icon-lock"></i>
                                                    Esqueci minha senha
                                                </a>
                                            </div>
                                        </div>
                                    </div><!-- /widget-body -->
                                </div><!-- /login-box -->

                                <div id="forgot-box" class="forgot-box widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header red lighter bigger">
                                                <i class="icon-key"></i>
                                                Recuperar Senha
                                            </h4>

                                            <div class="space-6"></div>
                                            <p>
                                                Insira o endereço e-mail cadastrado:
                                            </p>

                                            <form name="form.email" novalidate class="form" role="form">
                                                <fieldset>
                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right" >
                                                            <input type="email" style="text-transform:lowercase;" ng-pattern="/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/" class="form-control" ng-model="usuario.email" placeholder="E-mail" />
                                                            <i class="icon-envelope"></i>
                                                        </span>
                                                    </label>

                                                    <div class="clearfix">
                                                        <button type="button" ng-click="enviarEmail()" class="width-35 pull-right btn btn-sm btn-danger">
                                                            <i class="icon-envelope"></i>
                                                            Enviar
                                                        </button>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div><!-- /widget-main -->

                                        <div class="toolbar center">
                                            <a href="#" onclick="show_box('login-box'); return false;" ng-click="onClean()" class="back-to-login-link">
                                                <i class="icon-arrow-left"></i>
                                                Voltar
                                            </a>
                                        </div>
                                    </div><!-- /widget-body -->
                                </div><!-- /forgot-box -->
                            </div><!-- /position-relative -->
                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div><!-- /.main-container -->

        <!-- basic scripts -->
        
        <!--[if !IE]> -->

        <script type="text/javascript">
            //window.jQuery || document.write("<script type='text/javascript' src='scripts/lib/jquery-2.0.3.min.js'>"+"<"+"/script>");
        </script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script type="text/javascript">
         window.jQuery || document.write("<script type='text/javascript' src='scripts/lib/jquery-1.10.2.min.js'>"+"<"+"/script>");
        </script>
        <![endif]-->

        <script type="text/javascript">
            if ("ontouchend" in document) document.write("<script  type='text/javascript' src='scripts/lib/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>

        <!-- inline scripts related to this page -->

        <script type="text/javascript">
            function show_box(id) {
                jQuery('.widget-box.visible').removeClass('visible');
                jQuery('#' + id).addClass('visible');
            }
        </script>

        <!-- bower:js -->
        <script  type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
        <script  type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
        

        <%--<script  type="text/javascript" src="bower_components/angular/angular.js"></script>--%>
        <script  type="text/javascript" src="bower_components/angular-cookies/angular-cookies.js"></script>
        <script  type="text/javascript" src="bower_components/angular-sanitize/angular-sanitize.js"></script>
        <script  type="text/javascript" src="bower_components/angular-bootstrap/ui-bootstrap.js"></script>
        <script  type="text/javascript" src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>

        <!-- Include the JS ReCaptcha API -->
        <script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
        

        <script type="text/javascript" src="scripts/lib/ace.min.js"></script>
        <script type="text/javascript" src="scripts/lib/jquery.gritter.min.js"></script>
        <script type="text/javascript" src="scripts/lib/jquery-ui-1.10.3.custom.min.js"></script>
        <script type="text/javascript" src="scripts/lib/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="bower_components/angular-block-ui/angular-block-ui.min.js"></script>        

        <!-- build:js({.tmp,app}) scripts/scripts.js -->
        <script src="scripts/recaptcha.config.js" type="text/javascript"></script>   
        <script  type="text/javascript" src="scripts/backoffice/core/login.js"></script>             
        <script  type="text/javascript" src="scripts/backoffice/core/controllers/loginctrl.js"></script>
        <script  type="text/javascript" src="scripts/backoffice/core/services/loginservice.js"></script>
        <script  type="text/javascript" src="scripts/backoffice/core/services/genericservice.js"></script>



        <!-- endbuild -->
      </body>
</html>