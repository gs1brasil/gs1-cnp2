﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.FAQ" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="FAQCtrl" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; exibeMensagem = false; entity = 'papel'; itemFormEspec = { statusregistro: 'novo' };" style="height: 970px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <style>
        .font-orange { color: Orange; }
    </style>

    <div class="form-container" form-container style="width: 100%;">
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo"></label>
        </div>

        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" ng-show="searchMode" title="Limpar" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="69"></ajuda>
        </div>

        <div ng-if="faqs != undefined && faqs.length > 0">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="bs-example" style="padding: 8px">
                    <div class="accordion-style1 panel-group" id="accordion">
                        <div class="panel panel-default" ng-repeat="item in faqs" ng-init="isOpen = false">
                            <div class="panel-heading">
                                <h4 class="panel-title" style="cursos: pointer">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#{{$index}}" style="cursor: pointer;">
                                        <i class="bigger-110 ace-icon"></i> <%--ng-class="{'ace-icon fa fa-angle-down': isOpen, 'ace-icon fa fa-angle-right': !isOpen}"--%>
                                        {{item.pergunta}}
                                    </a>
                                </h4>

                                <%--<a data-toggle="collapse" data-parent="#accordion" href="#{{$index}}" target="_self" style="text-decoration: none;" ng-click="isOpen = !isOpen">
                                    <h4 class="panel-title" ng-bind="item.pergunta" style="color: #454444"></h4>
                                    <i class="pull-right" ng-class="{'font-orange': isOpen, 'icon-chevron-up': !isOpen, 'icon-chevron-down': isOpen}" style="margin-top: -17px;"></i>
                                </a>--%>
                            </div>
                            <div id="{{$index}}" class="panel-collapse collapse">
                                <div class="panel-body" style="overflow-y: scroll; max-height: 450px;">
                                    <p ng-bind-html="renderHtml(item.resposta)"></p>
                                    <div ng-if="item.video != undefined && item.video != ''">
                                        <br>
                                        <h5 translate="41.LBL_VideoExplicativo">Vídeo Explicativo: </h5>
                                        <iframe style="z-index:0!important;" id="iframe_youtube" width="523" height="388" ng-src="{{item.video | trusted}}" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-center">
                    <pagination ng-if="faqs.length != undefined && faqs.length != 0 && faqs.length != ''" total-items="totalregistros" boundary-links="true" max-size="10" rotate="false" ng-model="paginaatual" first-text="Primeira" previous-text="Anterior" last-text="Última" next-text="Próxima" ng-change="pageChanged(paginaatual)" items-per-page="quantidadeporpagina"></pagination>
                </div> 
            </div>
        </div>
        <div ng-if="faqs == undefined || faqs.length == 0">
            <h4 translate="41.LBL_NenhumaPerguntaFrequenteEncontrada">Nenhuma Pergunta Frequente Cadastrada.</h4>
        </div>
    </div>
</div>
