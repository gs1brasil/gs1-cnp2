﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="aceiteTermoAdesao.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.aceiteTermoAdesao" %>

<style type="text/css">
    
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak],
    .ng-cloak, .x-ng-cloak,
    .ng-hide {
        display: none !important;
    }
    
    
</style>

<div ng-controller="AceiteTermoAdesao" class="page-container" style="height: 780px;" ng-init="isVisualize = undefined">
    <div class="form-mobile" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo"></label>
        </div>
        
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" style="min-height: 720px;">
                <div class="widget-main no-padding">
                    <form name="form" novalidate class="form" role="form">
                        <div class="tab-content">
                            <div class="row"> 
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="row" ng-show="item.versao != undefined">
                                        <div class="col-md-12 center" style="margin-top: 10px; margin-bottom: 10px; border-bottom: 1px solid #dce8f1;">
                                            <h2>{{ '52.GRID_Versao' | translate }} {{item.versao}}</h2>       
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <fieldset id="termo"></fieldset>
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                            <div class="form-actions center ng-cloak" ng-show="isVisualize != undefined && isVisualize"  style="margin-bottom: 5px;">
                                <button id="Button2" name="retornar" class="btn btn-danger btn-sm" type="button" ng-click="retornarHome()">
                                    <i class="icon-remove bigger-110"></i><span translate="52.BTN_Fechar">Fechar</span>
                                </button>
                            </div>
                            <div class="form-actions center ng-cloak" ng-show="isVisualize != undefined && !isVisualize"  style="margin-bottom: 5px;" >
                                <button id="aceitar" name="aceitar" class="btn btn-info btn-sm" ng-click="aceitarTermo(item)">
                                    <i class="icon-ok bigger-110"></i><span translate="52.BTN_Aceitar">Aceitar</span>
                                </button>
                                <button id="recusar" name="recusar" class="btn btn-danger btn-sm" type="button" ng-click="rejeitarTermo()">
                                    <i class="icon-remove bigger-110"></i><span translate="52.BTN_Rejeitar">Rejeitar</span>
                                </button>
                            </div>
                        </div>  
                    </form>
                </div>
            </div>
        
        </div>
    </div>
    
    <script>
        $("#termo").slimScroll({
            height: '500px',
            width: '100%',
            disableFadeOut: true
        });
    </script>
</div>

