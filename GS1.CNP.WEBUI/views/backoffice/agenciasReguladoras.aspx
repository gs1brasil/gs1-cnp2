﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="agenciasReguladoras.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.agenciasReguladoras" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />
<script src="../../scripts/lib/jstree.min.js" type="text/javascript"></script>

<div ng-controller="agenciasReguladorasCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = 'agencia'; itemFormEspec = { statusregistro: 'novo' };" style="height: 820px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" class="truncate-text-mobile width-80" translate="3.LBL_CadastroAgenciasReguladoras"></label>
        </div>

        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" title="{{ '3.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="5" ng-show="searchMode" tooltip-ajuda="'3.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="6" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'3.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="7" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'3.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>
        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="agencia" class="tab-pane active">
                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                <div class="row" ng-show="!searchMode">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        (*) - <span translate="3.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                        <label for="nome" translate="3.LBL_NomeAgencia">Nome da Agência <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="255" id="nome" name="nome" ng-model="item.nome" required="required" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.3.LBL_NomeAgencia' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-6" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                        <label for="nome" translate="3.LBL_Status">Status <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="item.status" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.3.LBL_Status' | translate}}">
                                            <option value="1" ng-selected="item.status == 1">Ativo</option>
                                            <option value="0" ng-selected="item.status == 0">Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                        <label for="descricao" translate="3.LBL_Descricao">Descrição <span ng-show='!searchMode'>(*)</span></label>
                                        <textarea class="form-control input-sm" rows="4" id="descricao" name="descricao" sbx-maxlength="255" ng-model="item.descricao" style="width: 100%;resize: vertical;" required="required"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.3.LBL_Descricao' | translate}}"/>
                                        <span style="font-size: 80%;">(<span translate="3.LBL_TamanhoMaximo">Tamanho máximo</span>: 255)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span translate="3.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.descricao.$viewValue.length || 0 }}</span>
                                    </div>
                                </div>
                                <div class="form-actions center" ng-show="editMode">
                                   <button class="btn btn-info btn-sm" name="find" scroll-to="" ng-cloak ng-show="isPesquisa" ng-click="onSearch(item)" type="button">
                                        <i class="icon-search bigger-110"></i><span translate="3.BTN_Pesquisar">Pesquisar</span>
                                    </button>
                                    <button class="btn btn-info btn-sm" scroll-to="" ng-show="!isPesquisa" ng-click="onSave(item)" type="button">
                                        <i class="icon-save bigger-110"></i><span translate="3.BTN_Salvar">Salvar</span>
                                    </button>
                                    <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="3.BTN_Cancelar">Cancelar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" class="truncate-text-mobile width-80" translate="3.LBL_CadastroAgenciasReguladoras"></label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '3.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
            <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '3.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="4" tooltip-ajuda="'3.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 600px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="3.LBL_CliqueRegistroEditar">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableAgencia" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
                                <tr>
                                    <th ng-click="tableAgencia.sorting('codigo', tableAgencia.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                      <span>#&nbsp;&nbsp;</span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tableAgencia.isSortBy('codigo', 'asc'),
                                          'icon-sort-down': tableAgencia.isSortBy('codigo', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableAgencia.sorting('nome', tableAgencia.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                      <span translate="3.GRID_Nome">Nome&nbsp;&nbsp;</span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tableAgencia.isSortBy('nome', 'asc'),
                                          'icon-sort-down': tableAgencia.isSortBy('nome', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableAgencia.sorting('status', tableAgencia.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                      <span translate="3.GRID_Status">Status&nbsp;&nbsp;</span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tableAgencia.isSortBy('status', 'asc'),
                                          'icon-sort-down': tableAgencia.isSortBy('status', 'desc')
                                        }"></i>
                                    </th>
                                    <th>
                                      <span translate="3.GRID_Acao">Ação</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data | filter:itemProduto" ng-click="onEdit(item1)">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '60%' }" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '30%'}" sortable="'status'">{{item1.status | status}}</td>
                                    <td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Ação'">
                                        <a class="red" href="" title="{{ '3.TOOLTIP_Excluir' | translate }}" ng-click="onDelete(item1); $event.stopPropagation();"><i class="icon-trash bigger-130"></i></a>
                                    </td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="7" class="text-center" translate="3.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
