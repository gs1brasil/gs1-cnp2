﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ajudaPassoAPasso.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.ajudaPassoAPasso" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="AjudaPassoAPassoCtrl" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; exibeMensagem = false; entity = 'papel'; itemFormEspec = { statusregistro: 'novo' };" style="height: 970px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <style>
        .font-orange { color: Orange; }
    </style>

    <div class="form-container" form-container style="width: 100%;">
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo"></label>
        </div>

        <div ng-if="formularios != undefined && formularios.length > 0">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <!-- Formulários -->
                <div class="bs-example" style="padding: 8px">
                    <div class="accordion-style1 panel-group" id="accordion1">
                        <div class="panel panel-default" ng-repeat="item in formularios" ng-init="showItem = false">
                            <div class="panel-heading">
                                <h4 class="panel-title" ng-click="buscarAjudasPassoAPasso(item);" style="cursos: pointer">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" data-target="#collapse{{$index}}" style="cursor: pointer;">
                                        <i class="bigger-110 ace-icon"></i> <%--ng-class="{'ace-icon fa fa-angle-down': showItem, 'ace-icon fa fa-angle-right': !showItem}"--%>
                                        {{item.nome}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{$index}}" class="panel-collapse collapse">
                                <!-- Ajudas -->
                                <div class="bs-example" style="padding: 8px">
                                    <div class="accordion-style1 panel-group" id="accordion2">
                                        <div class="panel panel-default" ng-repeat="item in ajudas">
                                            <div class="panel-heading">
                                                <h4 class="panel-title" style="cursos: pointer">
                                                    <a class="accordion-toggle collapsed" style="cursor: pointer;" ng-href="/backoffice/detalhesAjudaPassoaPasso/{{item.codigo}}" target="_blank">
                                                        <i class="bigger-110 ace-icon"></i>  <%--fa fa-angle-right--%> <%--data-toggle="collapse" data-parent="#accordion2" data-target="#collapseInner{{$parent.$index}}{{$index}}"--%>
                                                        {{item.nome}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <%--<div id="collapseInner{{$parent.$index}}{{$index}}" class="panel-collapse collapse">
                                                <div class="panel-body" style="overflow-y: scroll; max-height: 450px; word-break: break-all;">
                                                    <p ng-bind-html="renderHtml(item.ajuda)"></p>
                                                    <div ng-if="item.video != undefined && item.video != ''">
                                                        <br>
                                                        <h5><span translate="51.LBL_VideoExplicativo">Vídeo Explicativo</span>: </h5>
                                                        <iframe style="z-index:0!important;" id="iframe1" width="523" height="388" ng-src="{{item.video | trusted}}" frameborder="0" allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row text-center">
                    <pagination ng-if="formularios.length != undefined && formularios.length != 0 && formularios.length != ''" total-items="totalregistros" boundary-links="true" max-size="10" rotate="false" ng-model="paginaatual" first-text="Primeira" previous-text="Anterior" last-text="Última" next-text="Próxima" ng-change="pageChanged(paginaatual)" items-per-page="quantidadeporpagina"></pagination>
                </div> 
            </div>

        </div>
        <div ng-if="formularios == undefined || formularios.length == 0">
            <h4 translate="51.LBL_NenhumaAjudaPassoaPassoEncontrada">Nenhuma Ajuda Passo a Passo Cadastrada.</h4>
        </div>
    </div>
</div>
