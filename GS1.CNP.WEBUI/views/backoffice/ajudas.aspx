﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ajudas.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.ajudas" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .wysiwyg-menu{
        text-align: center !important;
    }
</style>

<div ng-controller="AjudaCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false;" style="height:1050px;">
    <div class="form-container" form-container style="width: 100%;">
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo"></label>
        </div>
        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" ng-show="searchMode" title="{{ '39.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="64"></ajuda>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" novalidate class="form" role="form" >
                        <div class="tab-content" style="min-height:400px;">
                            <div id="telas" class="tab-pane active">
                                <div class="row">
                                    <div class="form-group col-md-4" ng-class="{'has-error': form.codigomodulo.$invalid && form.submitted}">
                                        <label for="codigomodulo" translate="39.LBL_Modulo">Módulo </label>
                                        <select class="form-control input-sm" id="codigomodulo" name="codigomodulo" ng-change="carregaFormularios(itemForm.codigomodulo)" ng-model="itemForm.codigomodulo" 
                                            ng-disabled="blockInputs && !searchMode" 
                                            ng-options=" modulo.codigo as modulo.nome for modulo in modulos">
                                            <option value="">&nbsp;&nbsp;Todos</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4" ng-class="{'has-error': form.codigoformulario.$invalid && form.submitted}">
                                        <label for="codigoformulario" translate="39.LBL_Formulario">Formulário </label>
                                        <select class="form-control input-sm" id="codigoformulario" name="codigoformulario" ng-model="itemForm.codigoformulario" ng-change="carregaCampos()" 
                                            ng-disabled="(itemForm.codigomodulo == undefined || itemForm.codigomodulo == '' || blockInputs)"
                                            ng-options="formulario.codigo as formulario.nome for formulario in formularios">
                                            <option value="">&nbsp;&nbsp;Todos</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4" ng-class="{'has-error': form.codigoidioma.$invalid && form.submitted}">
                                        <label for="idioma" translate="39.LBL_Idioma">Idioma </label>
                                        <select class="form-control input-sm" id="codigoidioma" name="codigoidioma" ng-model="itemForm.codigoidioma" ng-change="alteraIdioma(itemForm)"
                                            ng-options="idioma.codigo as idioma.nome for idioma in idiomas">
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4" ng-class="{'has-error': form.dataalteracao.$invalid && form.submitted}">
                                        <label for="inicio" translate="39.LBL_DataAlteracao">Data de Alteração </label>
                                        <input id="inicio" name="inicio" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" 
                                            ng-model="itemForm.dataalteracao" ng-change="carregaCampos()" />
                                    </div>
                                    <div class="form-group col-md-4" ng-class="{'has-error': form.codigostatus.$invalid && form.submitted}">
                                        <label for="inicio" translate="39.LBL_Status">Status </label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="itemForm.codigostatus" 
                                            ng-options="statusP.codigo as statusP.nome for statusP in statusPublicacao" ng-change="carregaCampos()">
                                            <option value="">&nbsp;&nbsp;Todos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row" ng-if="!isPesquisa">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                       <span translate="39.LBL_CliqueNoIcone">Clique no ícone <i class="fa fa-edit" title="{{ '39.TOOLTIP_Editar' | translate }}"></i> para editar um registro</span>
                                    </div>
                                </div>
                                <div class="table-responsive" ng-if="!isPesquisa">
                                    <table ng-table="tableCampos" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th ng-click="tableCampos.sorting('nomemodulo', tableCampos.isSortBy('nomemodulo', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="39.GRID_Modulo"> Módulo </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('nomemodulo', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('nomemodulo', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableCampos.sorting('nomeformulario', tableCampos.isSortBy('nomeformulario', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="39.GRID_Formulario"> Formulário </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('nomeformulario', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('nomeformulario', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableCampos.sorting('nome', tableCampos.isSortBy('nome', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="39.GRID_Nome"> Nome </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('nome', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('nome', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableCampos.sorting('descricao', tableCampos.isSortBy('descricao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="39.GRID_Descricao"> Descrição </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('descricao', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('descricao', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableCampos.sorting('dataalteracao', tableCampos.isSortBy('dataalteracao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="39.GRID_DataAlteracao"> Data/Hora de Alteração </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('dataalteracao', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('dataalteracao', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableCampos.sorting('codigostatus', tableCampos.isSortBy('codigostatus', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="39.GRID_Status"> Status </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('codigostatus', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('codigostatus', 'desc')
                                                  }"></i>
                                                </th>
                                                <th class="text-center">
                                                  <span translate="39.GRID_Acoes"> Ações </span>&nbsp;&nbsp;
                                                </th>
                                            </tr>
                                            
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="campo in $data" style="cursor:pointer;">
                                                <td ng-style="{ 'width': '18%' }" sortable="'nomemodulo'">{{campo.nomemodulo}}</td>
                                                <td ng-style="{ 'width': '20%' }" sortable="'nomeformulario'">{{campo.nomeformulario}}</td>
                                                <td ng-style="{ 'width': '10%' }" sortable="'nome'">{{campo.nome}}</td>
                                                <td ng-style="{ 'width': '20%' }" sortable="'descricao'">{{campo.descricao}}</td>
                                                <td ng-style="{ 'width': '12%' }" sortable="'dataalteracao'">{{campo.dataalteracao | date: 'dd/MM/yyyy HH:mm'}}</td>
                                                <td ng-style="{ 'width': '10%' }" sortable="'codigostatus'">{{campo.codigostatus | statuspublicacao}}</td>
                                                <td ng-style="{ 'width': '10%' }" class="text-center">
                                                    <a style="text-decoration: none" href="" ng-click="event.preventDefault(); geraDetalhes(campo.ajuda, campo.nomeformulario + ' - ' + campo.nome, campo.video)" ng-if="campo.ajuda != undefined && campo.ajuda != ''">
                                                        <i class="fa fa-eye" title="{{ '39.TOOLTIP_VisualizarAjuda' | translate }}"></i>
                                                    </a>
                                                    <a style="text-decoration: none" href="" ng-click="event.preventDefault(); onEditCampo(campo)">
                                                        <i class="fa fa-edit" title="{{ '39.TOOLTIP_Editar' | translate }}"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr ng-show="$data.length == 0">
                                                <td colspan="7" class="text-center">Nenhum registro encontrado</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <%--<div class="form-actions center">
                                <button name="salvar" class="btn btn-info btn-sm" type="button" ng-click="onSearch(itemForm)" ng-if="!initMode && isPesquisa">
                                    <i class="icon-search bigger-110"></i>Pesquisar
                                </button>
                                <button name="cancel" class="btn btn-danger btn-sm" type="button" ng-click="cancelar()">
                                    <i class="icon-remove bigger-110"></i>Cancelar
                                </button>
                            </div>--%>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%--<div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label>Gestão de Ajudas</label>
        </div>
        <div class="widget-box">
            <div class="pull-right action-buttons-header">
                <a class="btn btn-info btn-xs" title="Novo registro" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
                <a class="btn btn-success btn-xs" title="Pesquisar" ng-click="onSearchMode()"><i class="fa fa-search"></i></a>
                <a class="btn btn-warning btn-xs" title="Ajuda" href="/views/backoffice/ajuda/idiomas.html" style="margin-right:10px" target="_blank"><i class="icon-question-sign" title="Ajuda" ></i></a>
            </div>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 660px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableAjudas" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <tbody>
                                <tr ng-repeat="item1 in $data | filter:item" ng-click="onEdit(item1)">
                                    <td ng-style="{ 'width': '20%' }" data-title="'Módulo'" sortable="'nomemodulo'">{{item1.nomemodulo}}</td>
                                    <td ng-style="{ 'width': '80%' }" data-title="'Formulário'" sortable="'nomeformulario'">{{item1.nomeformulario}}</td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="8" class="text-center">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
    
    <div id="modal">
        <script type="text/ng-template" id="ModalEditaCampos">
            <style>
                .has-error .form-control {
                    border-color: #f09784;
                }

                .has-error .form-control:focus {
                    border-color: #db8978;
                }
            </style>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="bootbox-close-button close" ng-click="cancel()">x</button>
                    <h4 class="modal-title"><span translate="39.LBL_GerarPagina">Gerar Página</span></h4>
                </div>
                <div class="modal-body">
                    <form name="form.campo" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col md-12 text-center">
                                <span translate="39.LBL_ConfiguracoesGerarPaginaAjuda">Selecione as configurações para gerar a sua página de ajuda</span>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" ng-class="{'has-error': form.campo.nomemodulo.$invalid && form.campo.submitted}">
                                <label for="nomemodulo"><span translate="39.LBL_Modulo">Módulo</span> <span>(*)</span></label>
                                <input type="text" class="form-control input-sm" id="nomemodulo" name="nomemodulo" ng-model="campoForm.nomemodulo" disabled required/>
                            </div>
                            <div class="form-group col-md-6" ng-class="{'has-error': form.campo.nomeformulario.$invalid && form.campo.submitted}">
                                <label for="nomeformulario"><span translate="39.LBL_Pagina">Página</span> <span>(*)</span></label>
                                <input type="text" class="form-control input-sm" id="nomeformulario" name="nomeformulario" ng-model="campoForm.nomeformulario" disabled required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" ng-class="{'has-error': form.campo.nome.$invalid && form.campo.submitted}">
                                <label for="nome"><span translate="39.LBL_TipoAjuda">Tipo de Ajuda</span> <span>(*)</span></label>
                                <input type="text" class="form-control input-sm" id="nome" name="nome" ng-model="campoForm.nome" disabled required/>
                            </div>
                            <div class="form-group col-md-6" ng-class="{'has-error': form.campo.codigoidioma.$invalid && form.campo.submitted}">
                                <label for="codigoidioma"><span translate="39.LBL_Idioma">Idioma</span> <span>(*)</span></label>
                                <select class="form-control input-sm" id="codigoidioma" name="codigoidioma" ng-model="campoForm.codigoidioma" disabled required
                                    ng-options="idioma.codigo as idioma.nome for idioma in idiomas">
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" ng-class="{'has-error': form.campo.titulo.$invalid && form.campo.submitted}">
                                <label for="titulo"><span translate="39.LBL_Titulo">Título</span> <span>(*)</span></label>
                                <input type="text" class="form-control input-sm" id="titulo" maxlength="255" name="titulo" ng-model="campoForm.titulo" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" ng-class="{'has-error': (campoForm.ajuda == '' || campoForm.ajuda == undefined || campoForm.ajuda == null) && form.campo.submitted}">
                                <label for="codigomodulo"><span translate="39.LBL_ConteudoHtmlPagina">Conteúdo HTML da Página</span> <span>(*)</span></label>
                                <ng-ckeditor bind="campoForm.ajuda" skin="moono" remove-plugins="iframe,flash,smiley,scayt,wsc"></ng-ckeditor>
                                <!--<wysiwyg textarea-id="ajuda"
                                        name="ajuda"
                                        textarea-class="form-control"  
                                        textarea-height="300px" 
                                        textarea-name="ajuda"
                                        textarea-required 
                                        ng-model="campoForm.ajuda" 
                                        enable-bootstrap-title="true"
                                        textarea-menu="menuWysiwyg"
                                        ng-required="true">
                                </wysiwyg>-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" ng-class="{'has-error': form.campo.video.$invalid && form.campo.submitted}">
                                <span class="input-icon" style="width: 100%;">
                                    <label for="video"><span translate="39.LBL_VideoExplicativo">Vídeo Explicativo: </span></label>
                                    <input type="text" class="form-control input-sm" id="video" maxlength="255" name="video"
                                        placeholder="{{ '39.PLACEHOLDER_InformeURL' | translate }}" ng-model="campoForm.video"/>
                                        <i class="icon-youtube red bigger-120" style="padding-left: 0px; font-size: 21px; padding-top: 28px;"></i>
                                </span>
                            </div>
                            <div class="form-group col-md-6" ng-class="{'has-error': form.campo.status.$invalid && form.campo.submitted}">
                                <label for="status"><span translate="39.LBL_Status">Status</span> <span>(*)</span></label>
                                <select class="form-control input-sm" id="status" name="status" ng-model="campoForm.codigostatus" required
                                    ng-options="statusP.codigo as statusP.nome for statusP in statusPublicacao">
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer center">
                    <button name="salvar" class="btn btn-info" ng-click="ok()">
                        <i class="icon-save bigger-110"></i><span translate="39.BTN_Salvar">Salvar</span>
                    </button>
                    <button name="cancel" class="btn btn-danger" ng-click="cancel()">
                        <i class="icon-remove bigger-110"></i><span translate="39.BTN_Cancelar">Cancelar</span>
                    </button>
                </div>
            </div>
        </script>
    </div>

</div>