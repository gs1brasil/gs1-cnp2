﻿using GS1.CNP.WEBUI.Core;

namespace GS1.CNP.WEBUI.views.backoffice
{
    public partial class ajudas : BaseWeb
    {
        public override string PermissaoPagina { get { return "PesquisarAjuda"; } }
    }
}