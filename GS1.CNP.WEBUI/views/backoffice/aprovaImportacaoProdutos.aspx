﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="aprovaImportacaoProdutos.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.aprovaImportacaoProdutos" %>

<script type="text/javascript">
    if (typeof angular == 'undefined')
        window.location.href = "/#views/backoffice/importargpc";
    $(".widget-header a").attr("tabindex", "-1");
</script>
<style type="text/css">
.widget-toolbar
{
    float: left;
}
.tab-content
{
    padding: 0px;
}
.inputArquivo
{
    width: 400px;
    display: inline-block;
    height: 19px;
}
#lista
{
    overflow-y: scroll;
    height: 490px;
    width: 100%;
    position: absolute;
}

.strong
{
    font-weight: bold;
}
.grid-container
{
    background-color:#fff;
}
.grid-error
{
    color:#f00;
}

.text-nowrap
{
    white-space:nowrap;
}

a.disabled {
    pointer-events: none;
}

#formDownload table tr td
{
    padding-right: 10px;
}

.marginTable
{
    margin-top: 15px;
    margin-bottom: 5px;
}

.readOnlyCell
{
    background-color: #f5f5f5 !important;
}
</style>
<div ng-controller="AprovaImportacaoProdutosCtrl" class="page-container" ng-init="resultMode = false;verificacaoItensMode = true;" style="height: 1000px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container>
            <!-- Tela de Resultados -->
            <div ng-show="resultMode">
                <div class="page-header">
                    <i class="icon-align-justify"></i>
                    <label translate="55.LBL_AprovaImportacaoProduto">Aprovação de Importação - Webservice</label>
                </div>

                <div class="widget-box">
                    <div class="widget-header">
                        <h4><span translate="55.LBL_ResultadoImportacao">Resultado da Importação</span> - <b>{{item.codigo | numberFixedLen:5}}</b></h4>
                        <div class="pull-right" style="margin-top: -40px;">
                            <ajuda codigo="71" tooltip-ajuda="'55.TOOLTIP_Ajuda'"></ajuda>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <form name="form" id="form" novalidate class="form" role="form">
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_TipoItem">Tipo de Item</span>:</label> {{item.tipoitem}}
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.GRID_TipoImportacao">Tipo de Importação</span>:</label> {{item.tipoimportacao}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_Quantidade">Quantidade</span>:</label> {{item.itensimportados}} <span ng-show="item.itensimportados == 1" translate="55.LBL_Item">item</span><span ng-show="item.itensimportados > 1" translate="55.GRID_Itens">itens</span>
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_Resultado">Resultado</span>:</label> {{item.resultado}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_DataHora">Data/Hora</span>:</label> {{item.data  | date : 'dd/MM/yyyy HH:mm'}}
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_Usuario">Usuário</span>:</label> {{item.usuario}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_TempoImportacao">Tempo de Importação</span>:</label> {{item.tempoimportacao | date:'HH:mm:ss'}}
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-actions center">
                            <a target="_self" href="/relatorios/{{item.arquivorelatorio}}" scroll-to="" class="btn btn-info btn-sm fileDownloadSimpleRichExperience" ng-if="item.arquivorelatorio">
                                <i class="fa fa-download"></i><span class="hidden-xs">&nbsp; <span translate="55.BTN_BaixarLog">Baixar Log</span></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tela de Verificação dos Itens -->
            <div ng-show="verificacaoItensMode">
                <div class="page-header">
                    <i class="icon-align-justify"></i>
                    <label translate="55.LBL_AprovaImportacaoProduto">Aprovação de Importação - Webservice</label>
                </div>

                <div class="widget-box">
                    <div class="widget-header">
                        <h4><span translate="55.LBL_VerificacaoItens">Verificação dos Itens</span></h4>
                        <div class="pull-right" style="margin-top: -40px;">
                            <ajuda codigo="70" tooltip-ajuda="'55.TOOLTIP_Ajuda'"></ajuda>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <form name="form" id="form1" novalidate class="form" role="form">
                                <div class="row" ng-if="listaErros.length > 0">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_TipoItem">Tipo de Item</span>:</label> {{tipoitem}}
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0; display:none;">
                                        <label class="strong"><span translate="55.LBL_GDSN">GDSN</span>:</label> {{gdsn}}
                                    </div>
                                </div>
                                <div class="row" ng-if="listaErros.length > 0">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_Quantidade">Quantidade</span>:</label> {{qtdeitem}} itens
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="55.LBL_ErrosEncontrados">Erros encontrados</span>:</label> {{qtdeErros}}
                                    </div>
                                </div>
                                <div style="margin-top:15px; font-weight: bold; text-align: center;" ng-if="listaErros.length == 0">Nenhum registro aguardando aprovação foi encontrado.</div>
                                <div ng-if="listaErros.length > 0">
                                    <div class="panel panel-default">
                                        <div class="panel-head">&nbsp;<b translate="55.LBL_StatusImportacao">Status da Importação</b></div>
                                        <div class="panel-body"style="padding: 5px 15px 0px 15px; max-height: 100px;overflow-y: auto;margin-bottom: 5px;margin-right: 5px;">
                                            <span ng-if="listaErros.length > 0">
                                                {{qtdeErros}} <span translate="55.LBL_ErrosEncontradosArquivo">erros foram encontrados em seu arquivo</span>!<br />
                                                <span>
                                                    <span translate="55.LBL_CorrijaItensVermelho">Por favor, corrija os itens em vermelho ou retire-o(s) desta importação<br />.</span>
                                                    <ul>
                                                        <li ng-repeat="err in listaErros track by $index" style="margin-bottom: 10px;">
                                                            <span ng-bind-html="err.title"></span>
                                                            <div ng-repeat="message in err.messages" style="margin-left:15px" ng-bind-html="message"></div>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </span>
                                            <span ng-if="listaErros.length == 0" translate="55.LBL_ItensValidados">Itens validados. Pronto para inicar a importação.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding:10px; margin-top: 10px;">
                                    <div id="tableVerificacao" class="hot handsontable"></div>
                                </div>
                            </form>
                        </div>
                        <div class="form-actions center" style="margin-top: 0px;">
                            <button id="validate" name="validate" ng-click="onValidate()" ng-disabled="tableVerificacao.data.length == 0" scroll-to="" class="btn btn-info btn-sm" type="button" ng-if="exibeValidar">
                                <i class="icon-check bigger-110"></i><span translate="55.BTN_Validar">Validar</span>
                            </button>
                            <button id="import" name="import" ng-click="onImport()" ng-disabled="tableVerificacao.data.length == 0" scroll-to="" class="btn btn-info btn-sm" type="button" ng-if="exibeImportar">
                                <i class="icon-download-alt bigger-110"></i><span translate="55.BTN_Importar">Importar</span>
                            </button>
                            <button id="cancel" name="cancel" ng-click="onCancel()" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                <i class="icon-remove bigger-110"></i><span translate="55.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="flagAssociadoSelecionado != undefined && !flagAssociadoSelecionado">
        <h1 translate="55.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="55.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>
