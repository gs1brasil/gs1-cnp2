﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="banners.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.banners" %>

<style>
    .modal-dialog {
        margin: 30px auto;
        width: 700px;
        margin-top: 5%;
    }
    
    #upload {
        height: 180px;
    }
    
    .datepicker {
        cursor: pointer;
        z-index: 9999 !important;
    }
    
    .exibeVoltar .widget-main .table {
        width: 100% !important;
        display: block;
    }
    
    @media screen and (max-width: 700px) 
    {
        .modal-dialog {
            width: 100%;
        }
    }
    
</style>
<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/banners";
    }
</script>

<div ng-controller="EspacoBannerCtrl" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; importMode = false; apareceAba = true; entity = 'usuario';" style="height: 770px; overflow-y: auto;">
    <watch-fetch ids="item.id_perfil"></watch-fetch>

    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label> {{ titulo }}</label>
        </div>

        <div class="pull-right" style="margin-right: 210px; margin-top: -33px;">
            <ajuda codigo="92" ng-show="itemBanner != undefined && itemBanner.codigo != undefined" tooltip-ajuda="'50.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>

        <div class="widget-toolbar my-tab" style="margin-top: -36px;">
            <ul id="tabs-banner" class="nav nav-tabs">
                <li id="liTabBanner" class="active"><a data-toogle="tab" href="#banner"><span translate="50.ABA_Informacoes">Informações</span></a></li>
                <li id="liTabImagem"><a data-toogle="tab" href="#tab_imagem" ng-click="atualizaCampos(item)" ng-show="!searchMode"><span translate="50.ABA_Imagens">Imagens</span></a></li>
            </ul>
        </div>

        <div class="widget-box">
            <%--<div class="widget-header">
                <h4><span ng-show="!searchMode">Cadastro</span><span ng-show="searchMode">Pesquisa</span></h4>
                <div class="widget-toolbar">
                    <a ng-if="searchMode" href=""><i class="icon-eraser" title="Limpar" ng-click="onClean(item)"></i></a>
                    <a ng-show="!searchMode" href="/views/backoffice/ajuda/bannerInclusao.html" target="_blank"><i class="icon-question-sign" title="Ajuda" ></i></a>
                </div>
                <div class="widget-toolbar  my-tab">
                    <ul id="tabs-banner" class="nav nav-tabs">
                        <li id="liTabBanner" class="active"><a data-toogle="tab" href="#banner">Informações</a></li>
                        <li id="liTabImagem"><a data-toogle="tab" href="#tab_imagem" ng-click="atualizaCampos(item)" ng-show="!searchMode">Imagens</a></li>
                    </ul>
                </div>
            </div>--%>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" novalidate class="form" role="form" ng-submit="onSave(itemBanner)">
                        <div class="tab-content">
                            <div id="banner" class="tab-pane active">
                                <fieldset>
                                    <div class="row">
                                        <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="50.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                            <label for="nome" translate="50.LBL_Nome">Nome </label>
                                            <input type="text" class="form-control input-sm" id="nome" maxlength="60" name="nome" ng-model="itemBanner.nome" 
                                                ng-disabled="editMode" ng-readonly="editMode" ng-class="{'fieldDisabled': editMode}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.50.LBL_Nome' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-2" ng-class="{'has-error': form.quantidademaxima.$invalid && form.submitted}">
                                            <label for="descricao"><span translate="50.LBL_QtdeMaxImagens">Qtde. máx. imagens</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm" id="quantidademaxima" maxlength="2" onkeydown="return (event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" name="quantidademaxima" ng-model="itemBanner.quantidademaxima" required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.50.LBL_QtdeMaxImagens' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.urlpublicacao.$invalid && form.submitted}">
                                            <label for="descricao" translate="50.LBL_UrlPublicacao">URL para Publicação </label>
                                            <input type="text" class="form-control input-sm" id="urlpublicacao" ng-disabled="editMode" ng-readonly="editMode" ng-class="{'fieldDisabled': editMode}" maxlength="255" name="urlpublicacao" ng-model="itemBanner.urlpublicacao" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.50.LBL_UrlPublicacao' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-3" ng-class="{'has-error': form.altura.$invalid && form.submitted}">
                                            <label for="descricao" translate="50.LBL_Altura">Altura </label>
                                            <input type="text" class="form-control input-sm" ng-disabled="editMode" ng-readonly="editMode" ng-class="{'fieldDisabled': editMode}" id="altura" maxlength="50" name="altura" ng-model="itemBanner.altura" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.50.LBL_Altura' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-3" ng-class="{'has-error': form.largura.$invalid && form.submitted}">
                                            <label for="login" translate="50.LBL_Largura">Largura </label>
                                            <input type="text" class="form-control input-sm" id="largura" maxlength="50" ng-disabled="editMode" ng-readonly="editMode" ng-class="{'fieldDisabled': editMode}" name="largura" ng-model="itemBanner.largura" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.50.LBL_Largura' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label for="descricao" translate="50.LBL_Descricao">Descrição </label>
                                            <textarea type="text" class="form-control input-sm" style="resize: none;" ng-disabled="editMode" ng-readonly="editMode" ng-class="{'fieldDisabled': editMode}" id="descricao" maxlength="255" name="descricao" ng-model="itemBanner.descricao" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.50.LBL_Descricao' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                            <label for="login" translate="50.LBL_Status">Status </label>
                                            <select class="form-control input-sm" id="status" name="status" ng-model="itemBanner.status" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.50.LBL_Status' | translate}}">
                                                <option value="1" ng-selected="itemBanner.status == 1">Ativo</option>
                                                <option value="0" ng-selected="itemBanner.status == 0">Inativo</option>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center" ng-show="editMode">
                                    <button id="Button1" name="submit" ng-hide="searchMode" class="btn btn-info btn-sm" scroll-to="" type="submit">
                                        <i class="icon-ok bigger-110"></i>
                                        <span translate="50.BTN_Salvar">Salvar</span>
                                    </button>
                                    <button id="Button2" name="find" ng-click="onSearch(item)" ng-show="searchMode" class="btn btn-info btn-sm" scroll-to="" type="button">
                                        <i class="icon-ok bigger-110"></i>
                                        <span translate="50.BTN_Pesquisar">Pesquisar</span>
                                    </button>
                                    <button id="Button3" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i>
                                        <span translate="50.BTN_Cancelar">Cancelar</span>
                                    </button>
                                </div>
                            </div>
                            <div id="tab_imagem" class="tab-pane">
                               <%-- <fieldset>--%>
                                    <span style="float: left; font-size: 90%;" translate="50.LBL_CliqueRegistroEditar">Clique em um registro para editar</span>
                                    <div class="widget-toolbar" ng-show="itemsBanners.length < quantidademaxima">
                                        <a href="" title="{{'50.TOOLTIP_NovoRegistro' | translate }}"><i class="icon-plus ng-scope" name="NovoRegistro" ng-click="open()"></i></a>
                                    </div>
                                    <div class="table-responsive" style="height: 450px; overflow-x: auto; overflow-y: auto; clear: both;">
                                        <table ng-table="tableBanner" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th ng-click="tableBanner.sorting('nomeimagem', tableBanner.isSortBy('nomeimagem', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="50.GRID_Imagem">Imagem </span> &nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                          'icon-sort-up': tableBanner.isSortBy('nomeimagem', 'asc'),
                                                          'icon-sort-down': tableBanner.isSortBy('nomeimagem', 'desc')
                                                        }"></i>
                                                    </th>
                                                    <th ng-click="tableBanner.sorting('nome', tableBanner.isSortBy('nome', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="50.GRID_Nome">Nome </span> &nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                          'icon-sort-up': tableBanner.isSortBy('nome', 'asc'),
                                                          'icon-sort-down': tableBanner.isSortBy('nome', 'desc')
                                                        }"></i>
                                                    </th>
                                                    <th ng-click="tableBanner.sorting('link', tableBanner.isSortBy('link', 'asc') ? 'desc' : 'asc')" class="text-center" ng-show="false">
                                                      <span translate="50.GRID_Link">Link </span> &nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                          'icon-sort-up': tableBanner.isSortBy('link', 'asc'),
                                                          'icon-sort-down': tableBanner.isSortBy('link', 'desc')
                                                        }"></i>
                                                    </th>
                                                    <th ng-click="tableBanner.sorting('datainiciopublicacao', tableBanner.isSortBy('datainiciopublicacao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="50.GRID_InicioPublicacao">Início Publicação </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                         'icon-sort-up': tableBanner.isSortBy('datainiciopublicacao', 'asc'),
                                                         'icon-sort-down': tableBanner.isSortBy('datainiciopublicacao', 'desc')
                                                       }"></i>
                                                    </th>
                                                    <th ng-click="tableBanner.sorting('datafimpublicacao', tableBanner.isSortBy('datafimpublicacao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="50.GRID_FimPublicacao">Fim Publicação </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                         'icon-sort-up': tableBanner.isSortBy('datafimpublicacao', 'asc'),
                                                         'icon-sort-down': tableBanner.isSortBy('datafimpublicacao', 'desc')
                                                       }"></i>
                                                    </th>
                                                    <th ng-click="tableBanner.sorting('descricao', tableBanner.isSortBy('descricao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="50.GRID_Descricao">Descrição </span> &nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                          'icon-sort-up': tableBanner.isSortBy('descricao', 'asc'),
                                                          'icon-sort-down': tableBanner.isSortBy('descricao', 'desc')
                                                        }"></i>
                                                    </th>
                                                    <th ng-click="tableBanner.sorting('status', tableBanner.isSortBy('status', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="50.GRID_Status">Status </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                          'icon-sort-up': tableBanner.isSortBy('status', 'asc'),
                                                          'icon-sort-down': tableBanner.isSortBy('status', 'desc')
                                                        }"></i>
                                                    </th>
                                                    <th ng-click="tableBanner.sorting('ordem', tableBanner.isSortBy('ordem', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="50.GRID_Ordem">Ordem </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                          'icon-sort-up': tableBanner.isSortBy('ordem', 'asc'),
                                                          'icon-sort-down': tableBanner.isSortBy('ordem', 'desc')
                                                        }"></i>
                                                    </th>
                                                    <th ><span translate="50.GRID_Acoes">Ação</span></th>
                                                </tr>
                                            </thead>
                                            <tbody ng-drop="true">
                                                <tr ng-repeat="item2 in $data | filter:item" ng-click="open(item2, $data)" scroll-to="">
                                                    <td ng-style="{ 'width': '10%'}" sortable="'nomeimagem'"><img ng-src="{{item2.nomeimagem}}" style="width: 115px; height: 60px;"/></td>
                                                    <td ng-style="{ 'width': '14%'}" sortable="'nome'">{{item2.nome}}</td>
                                                    <td ng-style="{ 'width': '10%' }" ng-show="false" data-title="'Link'" sortable="'link'">{{item2.link}}</td>
                                                    <td ng-style="{ 'width': '11%'}" sortable="'datainiciopublicacao'">{{item2.datainiciopublicacao | date:'dd/MM/yyyy'}}</td>
                                                    <td ng-style="{ 'width': '10%'}" sortable="'datafimpublicacao'">{{item2.datafimpublicacao | date:'dd/MM/yyyy'}}</td>
                                                    <td ng-style="{ 'width': '15%'}" sortable="'descricao'">{{item2.descricao}}</td>
                                                    <td ng-style="{ 'width': '10%' }" sortable="'status'">{{item2.status | status}}</td>
                                                    <td ng-style="{ 'width': '10%' }" sortable="'ordem'">{{item2.ordem}}</td>
                                                    <td ng-style="{ 'width': '10%' }" class="action-buttons center"><a class="red" href="" ng-click="onDelete(item2); $event.stopPropagation();"><i class="icon-trash bigger-130"></i></a></td>
                                                </tr>
                                                <tr ng-show="itemsBanners.length == 0">
                                                    <td colspan="9" class="text-center" translate="50.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <%--</fieldset>--%>
                                <div class="form-actions center" ng-show="editMode">
                                    <button id="cancel" name="cancel" ng-click="onCancelImage(item)" scroll-to="" class="btn btn-default btn-sm" type="button">
                                        <i class="icon-rotate-left bigger-110"></i> <span translate="50.BTN_Voltar">Voltar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="modal" >
        <script type="text/ng-template" id="modalImage">
            <style type="text/css">
                .labelFile {
                    color: red; margin-top: -15px; font-size: 14px;
                }
            </style>
            <div class="modal-header">
                <h3 class="modal-title"><span translate="50.LBL_Imagem">Imagem</span> <popup-ajuda codigo="93" tooltip-ajuda="'50.TOOLTIP_Ajuda'"></popup-ajuda></h3>
            </div>
            <div class="modal-body">
                <form name="form.banner" novalidate class="form" role="form">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size:90%; ">(*) - <span translate="50.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.banner.nome.$invalid && form.banner.submitted}">
                            <label for="nome"><span translate="50.LBL_Nome">Nome</span> <span ng-show='!searchMode'>(*)</span></label>
                            <input type="text" class="form-control input-sm" id="nome" maxlength="100" name="nome" ng-model="selected.item.nome" required 
                            data-toggle="popover" data-trigger="hover" data-content="{{'t.50.LBL_Nome' | translate}}" ng-disabled="desabilitarCamposModal"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.banner.url.$invalid && form.banner.submitted}">
                            <label for="nome" translate="50.LBL_Link">Link </label>
                            <input type="text" class="form-control input-sm" id="link" maxlength="100" name="link" ng-model="selected.item.link" 
                            data-toggle="popover" data-trigger="hover" data-content="{{'t.50.LBL_Link' | translate}}" ng-disabled="desabilitarCamposModal"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.banner.descricao.$invalid && form.banner.submitted}">
                            <label for="descricao"><span translate="50.LBL_Descricao">Descrição</span> <span ng-show='!searchMode'>(*)</span></label>
                            <textarea type="text" class="form-control input-sm" style="resize: none;" id="descricao" maxlength="255" name="descricao" ng-model="selected.item.descricao" required 
                            data-toggle="popover" data-trigger="hover" data-content="{{'t.50.LBL_Descricao' | translate}}" ng-disabled="desabilitarCamposModal"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" ng-class="{'has-error': form.banner.datainiciopublicacao.$invalid && form.banner.submitted}">
                            <label for="descricao"><span translate="50.LBL_InicioPublicacao">Início da Publicação</span> <span ng-show='!searchMode'>(*)</span></label>
                            <input type="text" class="form-control input-sm datepicker" id="datainiciopublicacao" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="255" name="datainiciopublicacao" ng-model="selected.item.datainiciopublicacao" required 
                            data-toggle="popover" data-trigger="hover" data-content="{{'t.50.LBL_InicioPublicacao' | translate}}" ng-disabled="desabilitarCamposModal"/>
                        </div>
                        <div class="form-group col-md-6" ng-class="{'has-error': form.banner.datafimpublicacao.$invalid && form.banner.submitted}">
                            <label for="login"><span translate="50.LBL_FimPublicacao">Fim da Publicação</span> <span ng-show='!searchMode'>(*)</span></label>
                            <input type="text" class="form-control input-sm datepicker" id="datafimpublicacao" sbx-datepicker data-date-format="dd/mm/yyyy" maxlength="255" ng-click="datepicker()" name="datafimpublicacao" ng-model="selected.item.datafimpublicacao" required 
                            data-toggle="popover" data-trigger="hover" data-content="{{'t.50.LBL_FimPublicacao' | translate}}" ng-disabled="desabilitarCamposModal"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" ng-class="{'has-error': form.banner.status.$invalid && form.banner.submitted}">
                            <label for="login"><span translate="50.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>
                            <select class="form-control input-sm" id="status" name="status" ng-model="selected.item.status" required 
                            data-toggle="popover" data-trigger="hover" data-content="{{'t.50.LBL_Status' | translate}}" ng-disabled="desabilitarCamposModal">
                                <option value="1" ng-selected="selected.item.status == 1">Ativo</option>
                                <option value="0" ng-selected="selected.item.status == 0">Inativo</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6" ng-class="{'has-error': form.banner.ordem.$invalid && form.banner.submitted}">
                            <label for="descricao"><span translate="50.LBL_Ordem">Ordem</span> <span ng-show='!searchMode'>(*)</span></label>
                            <input type="text" class="form-control input-sm" id="ordem" maxlength="2" name="ordem" onkeydown="return (event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" ng-model="selected.item.ordem" required 
                            data-toggle="popover" data-trigger="hover" data-content="{{'t.50.LBL_Ordem' | translate}}" ng-disabled="desabilitarCamposModal"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6" ng-class="{'has-error': form.banner.nomeimagem.$invalid && form.banner.submitted}">
                            <label for="descricao"><span translate="50.LBL_Imagem">Imagem</span> <span ng-show='!searchMode'>(*)</span></label>
                            <input type="file" id="MultipleFilesUpload" name="nomeimagem" fileread="selected.item.nomeimagem" required 
                            data-toggle="popover" data-trigger="hover" data-content="{{'t.50.LBL_Imagem' | translate}}" ng-disabled="desabilitarCamposModal"/>
                        </div>
                        <div class="form-group col-md-6 col-xs-12 pull-right">
                            <ul class="ace-thumbnails" data-rel="colorbox" ng-hide="selected.item.nomeimagem == undefined || selected.item.nomeimagem == null || selected.item.nomeimagem == '' || selected.item.nomeimagem != item.nomeimagem"> 
                                <li>
                                    <img ng-src="{{selected.item.nomeimagem}}" style="height:60px; width:120px;" />
                                    <div class="tools tools-bottom" ng-show="!desabilitarCamposModal">
                                        <i class="icon-remove red" ng-click="onDeleteImage(selected.item)" style="cursor:pointer;" ></i>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <br>
            <div class="modal-footer">
                <button class="btn btn-info" ng-click="ok(selected.item)" ng-disabled="desabilitarCamposModal"><span translate="50.BTN_Salvar">Salvar</span></button>
                <button class="btn btn-danger" ng-click="cancel()"><span translate="50.BTN_Cancelar">Cancelar</span></button>
            </div>
        </script>
    </div>

    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label translate="50.LBL_CadsatroBanner">Cadastro de Banner</label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <ajuda codigo="91" tooltip-ajuda="'50.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>

        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" style="height: 659px;" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i>
                    <i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>

                <div class="widget-main">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="50.LBL_CliqueRegistroEditar">Clique em um registro para editar</div>
                    </div>
                    <div class="table-responsive" style="height: 600px; overflow-y: auto; overflow-x: auto;">
                        <table ng-table="tableEspacoBanner" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
                                <tr>
                                    <th ng-click="tableEspacoBanner.sorting('codigo', tableEspacoBanner.isSortBy('codigo', 'asc') ? 'desc' : 'asc')" class="text-center">
                                        <span># &nbsp;&nbsp; </span>
                                        <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEspacoBanner.isSortBy('codigo', 'asc'),
                                            'icon-sort-down': tableEspacoBanner.isSortBy('codigo', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableEspacoBanner.sorting('nome', tableEspacoBanner.isSortBy('nome', 'asc') ? 'desc' : 'asc')" class="text-center">
                                        <span translate="50.GRID_Nome">Nome </span>&nbsp;&nbsp;
                                        <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEspacoBanner.isSortBy('nome', 'asc'),
                                            'icon-sort-down': tableEspacoBanner.isSortBy('nome', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableEspacoBanner.sorting('altura', tableEspacoBanner.isSortBy('altura', 'asc') ? 'desc' : 'asc')" class="text-center">
                                        <span translate="50.GRID_Altura">Altura </span>&nbsp;&nbsp;
                                        <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEspacoBanner.isSortBy('altura', 'asc'),
                                            'icon-sort-down': tableEspacoBanner.isSortBy('altura', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableEspacoBanner.sorting('largura', tableEspacoBanner.isSortBy('largura', 'asc') ? 'desc' : 'asc')" class="text-center">
                                        <span translate="50.GRID_Largura">Largura </span>&nbsp;&nbsp;
                                        <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEspacoBanner.isSortBy('largura', 'asc'),
                                            'icon-sort-down': tableEspacoBanner.isSortBy('largura', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableEspacoBanner.sorting('urlpublicacao', tableEspacoBanner.isSortBy('urlpublicacao', 'asc') ? 'desc' : 'asc')" class="text-center" ng-show="false">
                                        <span translate="50.GRID_UrlPublicacao">URL Publicação </span>&nbsp;&nbsp;
                                        <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEspacoBanner.isSortBy('urlpublicacao', 'asc'),
                                            'icon-sort-down': tableEspacoBanner.isSortBy('urlpublicacao', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableEspacoBanner.sorting('descricao', tableEspacoBanner.isSortBy('descricao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                        <span translate="50.GRID_Descricao">Descrição </span>&nbsp;&nbsp;
                                        <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEspacoBanner.isSortBy('descricao', 'asc'),
                                            'icon-sort-down': tableEspacoBanner.isSortBy('descricao', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableEspacoBanner.sorting('status', tableEspacoBanner.isSortBy('status', 'asc') ? 'desc' : 'asc')" class="text-center">
                                        <span translate="50.GRID_Status">Status &nbsp;&nbsp; </span>
                                        <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEspacoBanner.isSortBy('status', 'asc'),
                                            'icon-sort-down': tableEspacoBanner.isSortBy('status', 'desc')
                                        }"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data | filter:item" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '5%' }" data-title="'#'" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '25%' }" data-title="'Nome'" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '10%' }" data-title="'Altura'" sortable="'altura'">{{item1.altura}}</td>
                                    <td ng-style="{ 'width': '10%' }" data-title="'Largura'" sortable="'largura'">{{item1.largura}}</td>
                                    <td ng-style="{ 'width': '10%' }" ng-show="false" data-title="'Url Publicação'" sortable="'urlpublicacao'">{{item1.urlpublicacao}}</td>
                                    <td ng-style="{ 'width': '30%' }" data-title="'Descrição'" sortable="'descricao'">{{item1.descricao}}</td>
                                    <td ng-style="{ 'width': '10%' }" data-title="'Status'" sortable="'status'">{{item1.status | status}}</td>
                                </tr>
                                <tr ng-show="items.length == 0">
                                    <td colspan="7" class="text-center" translate="50.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

