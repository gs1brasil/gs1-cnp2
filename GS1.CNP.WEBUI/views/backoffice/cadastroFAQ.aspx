﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cadastroFAQ.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.cadastroFAQ" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .wysiwyg-menu{
        text-align: center !important;
    }
    .has-error .form-control {
        border-color: #f09784;
    }
    .has-error .form-control:focus {
        border-color: #db8978;
    }
</style>

<div ng-controller="CadastroFAQCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false;" style="height:900px;">
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" class="truncate-text-mobile" ng-class="{'width-80': isPesquisa, 'width-100': !isPesquisa}" translate="40.LBL_CadastroFAQ"></label>
        </div>
        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" title="{{ '40.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="66" ng-show="searchMode" tooltip-ajuda="'40.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="67" ng-show="!searchMode && campoForm != undefined && campoForm.codigo == undefined" tooltip-ajuda="'40.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="68" ng-show="!searchMode && campoForm != undefined && campoForm.codigo != undefined" tooltip-ajuda="'40.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" novalidate class="form" role="form" >
                        <div class="tab-content" style="min-height:400px;">
                            <div class="row" ng-if="!searchMode">
                                <div class="form-group col md-12 text-center">
                                    <span translate="40.LBL_ConfiguracoesGerarFAQ">Selecione as configurações para gerar uma página no FAQ</span>
                                </div>
                            </div>
                            <div class="row" ng-show="!searchMode">
	                            <div class="form-group col-md-12" style="font-size: 90%;">
		                            (*) - <span translate="40.LBL_CamposObrigatorios">Campos obrigatórios</span>
	                            </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.codigoidioma.$invalid && form.submitted}">
                                    <label for="codigoidioma"><span translate="40.LBL_Idioma">Idioma</span> <span>(*)</span></label>
                                    <select class="form-control input-sm" id="codigoidioma" name="codigoidioma" ng-model="campoForm.codigoidioma" required
                                        ng-options="idioma.codigo as idioma.nome for idioma in idiomas"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.40.LBL_Idioma' | translate}}"><%--ng-disabled="!initMode && !searchMode" ng-readonly="!initMode && !searchMode"--%>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                    <label for="status"><span translate="40.LBL_Status">Status</span> <span>(*)</span></label>
                                    <select class="form-control input-sm" id="status" name="status" ng-model="campoForm.codigostatus" required
                                        ng-options="statusP.codigo as statusP.nome for statusP in statusPublicacao"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.40.LBL_Status' | translate}}">
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.pergunta.$invalid && form.submitted}">
                                    <label for="titulo"><span translate="40.">Pergunta</span> <span>(*)</span></label>
                                    <input type="text" class="form-control input-sm" id="pergunta" maxlength="255" name="pergunta" ng-model="campoForm.pergunta" required 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.40.LBL_Pergunta' | translate}}"/>
                                </div>
                            </div>
                            <div class="row" ng-if="!searchMode">
                                <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': (campoForm.resposta == '' || campoForm.resposta == undefined || campoForm.resposta == null) && form.submitted}">
                                    <label for="codigomodulo"><span translate="40.LBL_ConteudoHTMLResposta">Conteúdo HTML da Resposta</span> <span>(*)</span></label>
                                    <wysiwyg textarea-id="resposta"
                                            name="resposta"
                                            textarea-class="form-control"
                                            textarea-height="300px"
                                            textarea-name="resposta"
                                            textarea-required
                                            ng-model="campoForm.resposta"
                                            enable-bootstrap-title="true"
                                            textarea-menu="menuWysiwyg"
                                            ng-required="true"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.40.LBL_ConteudoHTMLResposta' | translate}}">
                                    </wysiwyg>

                                    <div text-angular ng-model="htmlVariable"></div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.video.$invalid && form.submitted}" ng-if="!searchMode">
                                    <span class="input-icon" style="width: 100%;">
                                        <label for="video" translate="40.LBL_VideoExplicativo">Vídeo Explicativo </label>
                                        <input type="text" class="form-control input-sm" id="video" maxlength="255" name="video"
                                            placeholder="{{ '40.PLACEHOLDER_InformeURL' | translate }}" ng-model="campoForm.video" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.40.LBL_VideoExplicativo' | translate}}"/>
                                            <i class="icon-youtube red bigger-120" style="padding-left: 0px; font-size: 21px; padding-top: 28px;"></i>
                                    </span>
                                </div>
                                <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.ordem.$invalid && form.submitted}" ng-if="!searchMode">
                                    <label for="ordem"><span translate="40.LBL_Ordem">Ordem</span> <span>(*)</span></label>
                                    <input type="text" class="form-control input-sm" id="Text1" maxlength="6" name="ordem" ng-model="campoForm.ordem" required integer 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.40.LBL_Ordem' | translate}}"/>
                                </div>
                                <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.codigostatusvisualizacaofaq.$invalid && form.submitted}">
                                    <label for="ordem"><span translate="40.LBL_DisponibilizarPagina">Visualização do FAQ</span> <span>(*)</span></label>
                                    <select class="form-control input-sm" id="Select1" name="codigostatusvisualizacaofaq" ng-model="campoForm.codigostatusvisualizacaofaq" required
                                        ng-options="tipos.codigo as tipos.nome for tipos in tiposDisponibilidade"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.40.LBL_DisponibilizarPagina' | translate}}">
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions center">
                                <button name="salvar" class="btn btn-info btn-sm" type="button" ng-click="onSave(campoForm)" ng-if="!isPesquisa">
                                    <i class="icon-save bigger-110"></i><span translate="40.BTN_Salvar">Salvar</span>
                                </button>
                                <button name="find" class="btn btn-info btn-sm" type="button" ng-click="onSearch(campoForm)" ng-if="isPesquisa">
                                    <i class="icon-search bigger-110"></i><span translate="40.BTN_Pesquisar">Pesquisar</span>
                                </button>
                                <button name="cancel" class="btn btn-danger btn-sm" type="button" ng-click="cancelar()">
                                    <i class="icon-remove bigger-110"></i><span translate="40.BTN_Cancelar">Cancelar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label class="truncate-text-mobile width-80" translate="40.LBL_CadastroFAQ">Cadastro de Perguntas Frequentes - FAQ</label>
        </div>
        <div class="widget-box">
            <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                <a class="btn btn-info btn-xs" ng-click="onFormMode()" name="NovoRegistro" title="{{ '40.TOOLTIP_NovoRegistro' | translate }}"><i class="fa fa-plus"></i></a>
                <a class="btn btn-success btn-xs" ng-click="onSearchMode()" name="NovaPesquisa" title="{{ '40.TOOLTIP_Pesquisar' | translate }}"><i class="fa fa-search"></i></a>
                <ajuda codigo="65"></ajuda>
            </div>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 860px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;">
                            <span translate="40.LBL_CliqueNoIcone">Clique no ícone <i class="fa fa-edit" title="{{ '40.TOOLTIP_Editar' | translate }}"></i> para editar um registro</span>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableCampos" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th ng-click="tableCampos.sorting('codigo', tableCampos.isSortBy('codigo', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span># &nbsp;&nbsp; </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tableCampos.isSortBy('codigo', 'asc'),
                                          'icon-sort-down': tableCampos.isSortBy('codigo', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableCampos.sorting('pergunta', tableCampos.isSortBy('pergunta', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="40.GRID_Pergunta">Pergunta &nbsp;&nbsp;
                                      </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tableCampos.isSortBy('pergunta', 'asc'),
                                          'icon-sort-down': tableCampos.isSortBy('pergunta', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableCampos.sorting('datacadastro', tableCampos.isSortBy('datacadastro', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="40.GRID_DataAlteracao">Data/Hora de Alteração &nbsp;&nbsp;
                                      </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tableCampos.isSortBy('datacadastro', 'asc'),
                                          'icon-sort-down': tableCampos.isSortBy('datacadastro', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tableCampos.sorting('statuspublicacao', tableCampos.isSortBy('datacadastro', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="40.GRID_Status">Status &nbsp;&nbsp;
                                      </span>
                                      <i class="text-center icon-sort" ng-class="{
                                         'icon-sort-up': tableCampos.isSortBy('statuspublicacao', 'asc'),
                                         'icon-sort-down': tableCampos.isSortBy('statuspublicacao', 'desc')
                                       }"></i>
                                    </th>
                                    <th ng-click="tableCampos.sorting('nomeusuario', tableCampos.isSortBy('datacadastro', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="40.GRID_CriadoPor">Criado Por &nbsp;&nbsp;
                                      </span>
                                      <i class="text-center icon-sort" ng-class="{
                                         'icon-sort-up': tableCampos.isSortBy('nomeusuario', 'asc'),
                                         'icon-sort-down': tableCampos.isSortBy('nomeusuario', 'desc')
                                       }"></i>
                                    </th>
                                    <th ng-click="tableCampos.sorting('nomestatusvisualizacaofaq', tableCampos.isSortBy('nomestatusvisualizacaofaq', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="40.GRID_VisualizacaoFAQ">Visualização do FAQ &nbsp;&nbsp;
                                      </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tableCampos.isSortBy('nomestatusvisualizacaofaq', 'asc'),
                                          'icon-sort-down': tableCampos.isSortBy('nomestatusvisualizacaofaq', 'desc')
                                        }"></i>
                                    </th>
                                    <th ><span translate="40.GRID_Acoes">Ações</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="campo in $data" style="cursor:pointer;">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{campo.codigo}}</td>
                                    <td ng-style="{ 'width': '20%' }" sortable="'pergunta'">{{campo.pergunta}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'datacadastro'">{{campo.datacadastro | date: 'dd/MM/yyyy HH:mm'}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'statuspublicacao'">{{campo.statuspublicacao}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'nomeusuario'">{{campo.nomeusuario}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'nomestatusvisualizacaofaq'">{{campo.nomestatusvisualizacaofaq}}</td>
                                    <td ng-style="{ 'width': '5%' }" class="text-center">
                                        <a style="text-decoration: none" href="" ng-click="event.preventDefault(); geraDetalhes(campo.pergunta, campo.resposta, campo.video)" ng-if="campo.pergunta != undefined && campo.pergunta != ''">
                                            <i class="fa fa-eye" title="{{ '40.TOOLTIP_DetalhesAjuda' | translate }}"></i>
                                        </a>
                                        <a style="text-decoration: none" href="" ng-click="event.preventDefault(); onEdit(campo)">
                                            <i class="fa fa-edit" title="{{ '40.TOOLTIP_Editar' | translate }}"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="7" class="text-center" translate="40.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
