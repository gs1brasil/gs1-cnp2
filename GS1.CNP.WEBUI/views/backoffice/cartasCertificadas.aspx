﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cartasCertificadas.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.cartasCertificadas" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="CartasCertificadasCtrl" class="page-container" ng-init="entity = 'cartasCertificadas'; searchMode = false;" style="height: 820px;">
    <div ng-cloak ng-if="flagAssociadoSelecionado">
        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label ng-bind="titulo"></label>
            </div>

             <div class="pull-right" style="margin-top: -40px;">
                <ajuda codigo="90" tooltip-ajuda="'48.TOOLTIP_Ajuda'"></ajuda>
                <a class="btn btn-danger btn-xs" name="limpar" title="{{ '48.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            </div>

            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" id="form" novalidate class="form" role="form">
                        <div class="tab-content">
                            <div id="cartasGs1" class="tab-pane active">
                                <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="gtin" translate="48.LBL_GTIN">GTIN</label>
                                            <input type="text" integer class="form-control input-sm" maxlength="14" id="gtin" name="gtin" ng-model="itemForm.gtin"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.48.LBL_GTIN' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="tipoGtin" translate="48.LBL_TipoGTIN">Tipo de GTIN </label>
                                            <select class="form-control input-sm" id="codigotipogtin" ng-change="change()" ng-model="itemForm.codigotipogtin" name="codigotipogtin"
                                            ng-options="tipo.codigo as tipo.nome for tipo in tiposGtin" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.48.LBL_TipoGTIN' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="descricao" translate="48.LBL_Descricao">Descrição</label>
                                            <input type="text" class="form-control input-sm" maxlength="255" id="descricao" name="descricao" ng-model="itemForm.descricao"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.48.LBL_Descricao' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="form-actions center">
                                       <button class="btn btn-info btn-sm" name="find" scroll-to="" ng-cloak ng-click="onSearch(itemForm)" type="button">
                                            <i class="icon-search bigger-110"></i><span translate="48.BTN_Pesquisar">Pesquisar</span>
                                        </button>
                                        <button id="cancel" name="cancel" ng-click="onCancel(itemForm)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                            <i class="icon-remove bigger-110"></i><span translate="48.BTN_Cancelar">Cancelar</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": searchMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label ng-bind="titulo"></label>
            </div>

            <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                <a class="btn btn-success btn-xs" title="{{ '48.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(itemForm)"><i class="fa fa-search"></i></a>
                <ajuda codigo="89" tooltip-ajuda="'48.TOOLTIP_Ajuda'"></ajuda>
            </div>

            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body">
                    <div class="widget-main" style="min-height: 300px;">
                        <div class="table-responsive">
                            <table ng-table="tableCartas" class="table table-bordered table-striped table-hover">
                              <thead>
	                                <tr>
                                        <th ng-click="tableCartas.sorting('gtin', tableCartas.isSortBy('gtin', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="17.GRID_GTIN"> GTIN </span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCartas.isSortBy('gtin', 'asc'),
                                            'icon-sort-down': tableCartas.isSortBy('gtin', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableCartas.sorting('descricao', tableCartas.isSortBy('descricao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="17.GRID_Descricao">Descrição </span>&nbsp;&nbsp;
                                          <i class="icon-sort" ng-class="{
                                            'icon-sort-up': tableCartas.isSortBy('descricao', 'asc'),
                                            'icon-sort-down': tableCartas.isSortBy('descricao', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableCartas.sorting('tipodegtin', tableCartas.isSortBy('tipodegtin', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="17.GRID_TipoGTIN">Tipo de GTIN </span>&nbsp;&nbsp; 
                                          <i class="text-center icon-sort" ng-class="{
                                              'icon-sort-up': tableCartas.isSortBy('tipodegtin', 'asc'),
                                              'icon-sort-down': tableCartas.isSortBy('tipodegtin', 'desc')
                                            }"></i>
                                        </th>
                                        <th class="text-center"><span translate="17.GRID_Selecionar">Selecionar</span></th>
	                                </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in $data">
                                        <td ng-style="{ 'width': '20%' }" sortable="gtin" style="vertical-align: middle;">{{item.gtin}}</td>
                                        <td ng-style="{ 'width': '60%' }" sortable="descricao" style="vertical-align: middle;">{{item.descricao}}</td>
                                        <td ng-style="{ 'width': '10%' }" sortable="tipodegtin" style="vertical-align: middle;">{{item.tipodegtin}}</td>
                                        <td ng-style="{ 'width': '10%' }" class="center">
                                            <label>
                      						    <input class="ace" id="id-disable-check" type="checkbox" ng-model="item.checked" ng-click="flagMarcarDesmarcar(item)">
                      							    <span class="lbl"></span>
                      					        </label>
                                        </td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="4" class="text-center" translate="48.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row pull-right text-right" style="margin-top:-63px; width:20%;">
                                <div class="col-md-12">
                                    <button class="btn btn-info btn-sm" scroll-to="" ng-cloak ng-click="gerarCarta()" type="button" style="padding-left:38px; padding-right:38px;">
                                        <i class="fa fa-download"></i>&nbsp;<span translate="48.BTN_Gerar"> Gerar</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="48.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="48.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>
