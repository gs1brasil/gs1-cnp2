﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cartasCertificadasGs1.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.cartasCertificadasGs1" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="CartasCertificadasGs1" class="page-container" ng-init="entity = 'cartasCertificadas';" style="height: 820px;">
    <div ng-cloak ng-if="flagAssociadoSelecionado">
        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false">
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label>{{ titulo }}</label>
            </div>

            <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                <ajuda codigo="32" tooltip-ajuda="'12.TOOLTIP_Ajuda'"></ajuda>
            </div>

            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body">
                    <div class="widget-main" style="min-height: 300px;">
                        <div class="table-responsive">
                            <table ng-table="tableCartasGs1" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th ng-click="tableCartasGs1.sorting('nomelicenca', tableCartasGs1.isSortBy('nomelicenca', 'asc') ? 'desc' : 'asc')">
                                          <span translate="12.GRID_TipoLicenca">Tipo de Licença</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCartasGs1.isSortBy('nomelicenca', 'asc'),
                                            'icon-sort-down': tableCartasGs1.isSortBy('nomelicenca', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableCartasGs1.sorting('datageracao', tableCartasGs1.isSortBy('datageracao', 'asc') ? 'desc' : 'asc')">
                                          <span translate="12.GRID_DataGeracao">Data/Hora da Geração</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCartasGs1.isSortBy('datageracao', 'asc'),
                                            'icon-sort-down': tableCartasGs1.isSortBy('datageracao', 'desc')
                                          }"></i>
                                        </th>
                                        <th><span translate="12.GRID_Acao">Ação</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in $data">
                                        <td ng-style="{ 'width': '25%' }" sortable="'nomelicenca'" style="vertical-align: middle;">{{item.nomelicenca}}</td>
                                        <td ng-style="{ 'width': '25%' }" sortable="'datageracao'" style="vertical-align: middle;">{{item.datageracao | date : 'dd/MM/yyyy HH:mm'}}</td>
                                        <td ng-style="{ 'width': '5%' }"  class="text-center">
                                            <button class="btn btn-info btn-xs" title="{{ '12.TOOLTIP_Download' | translate }}" ng-click="$event.stopPropagation(); gerarCarta(item)">
                                                <i class="fa fa-download"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="7" class="text-center" translate="12.LBL_NenhumRegistroEncontrado">Nenhuma carta disponível</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="12.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="12.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>
