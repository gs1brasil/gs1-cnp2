﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="certificacaoProdutos.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.certificacaoProdutos"%>
<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/certificacaoProdutos";
    }
</script>

<style type="text/css">
    .input-group-btn>button{
        padding: 2px;
    }

    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak],
    .ng-cloak, .x-ng-cloak,
    .ng-hide {
        display: none !important;
    }

    .has-error-label
    {
        color: #f09784;
    }
</style>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="CertificacaoProdutoCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = ''; gerar = false;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label class="truncate-text-mobile" ng-class="{'width-80': isPesquisa, 'width-100': !isPesquisa}" translate="45.LBL_TitleCodigoBarrasPesosMedidas">{{ titulo }}</label>
            </div>

            <div class="pull-right" style="margin-top:-35px; margin-right:2px;">
                <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" ng-click="onClean(item)" title="{{ '45.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="78" ng-show="searchMode" tooltip-ajuda="'45.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="80" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'45.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            </div>

            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="tab-content">
                        <div id="geracaoEpc" class="tab-pane active" ng-show="!isPesquisa">
                            <form name="form" id="form" novalidate class="form" role="form">
                                <div class="row" style="padding-top: 15px;">
                                    <div class="col-md-12 form-group" ng-class="{'has-error': form.produto.$invalid && form.submitted}">
                                        <label for="datacertificado">
                                            <span translate="45.LBL_Certificacao">Certificação</span>
                                        </label><br />
                                        <input type="text" id="Text5" name="produto" ng-model="item.produto" maxlength="255" class="form-control input-sm datepicker" disabled 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_Certificacao' | translate}}"/>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 15px;">
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.dataliberacao.$invalid && form.submitted}">
                                        <label for="dataliberacao">
                                            <span translate="45.LBL_DataLiberacao">Data de Liberação</span> 
                                        </label><br />
                                        <input type="text" name="dataliberacao" id="Text4" ng-model="item.dataliberacao" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" disabled
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_DataLiberacao' | translate}}"/>
                                    </div>
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.datavalidade.$invalid && form.submitted}">
                                        <label for="datavalidade">
                                            <span translate="45.LBL_DataValidade">Data de Validade</span> 
                                        </label><br />
                                        <input type="text" name="datavalidade" id="datavalidade" ng-model="item.datavalidade" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" disabled 
                                            ng-change="calcularValidade(item.dataliberacao)"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_DataValidade' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                        <label for="status"><span translate="45.LBL_Status">Status</span></label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="item.status" disabled required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_Status' | translate}}">
                                            <option value="Ativo" ng-selected="item.status == 'Ativo'">Ativo</option>
                                            <option value="Inativo" ng-selected="item.status == 'Inativo'">Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-actions center">
                                    <%--<button class="btn btn-info btn-sm" ng-click="onSave(item)" ng-show="!isPesquisa">
                                        <i class="icon-save bigger-110"></i><span translate="45.BTN_Salvar">Salvar</span>
                                    </button>--%>
                                    <button id="Button1" name="fechar" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="45.BTN_Fechar">Fechar</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div id="Div1" class="tab-pane active" ng-show="isPesquisa">
                            <div class="row" style="padding-top: 15px;">
                                <div class="col-md-3 form-group" ng-class="{'has-error': form.dataliberacao.$invalid && form.submitted}">
                                    <label for="dataliberacao" translate="45.LBL_DataLiberacao">Data de Liberação </label><br />
                                    <input type="text" id="Text1" name="datacertificado" ng-model="item.dataliberacao" maxlength="10" class="form-control input-sm datepicker"
                                        sbx-datepicker data-date-format="dd/mm/yyyy"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_DataLiberacao' | translate}}"/>
                                </div>
                                <div class="col-md-3 form-group" ng-class="{'has-error': form.datavalidade.$invalid && form.submitted}">
                                    <label for="datavalidade">
                                        <span translate="45.LBL_DataValidade">Data de Validade</span> <span ng-show="!searchMode">(*)</span>
                                    </label><br />
                                    <input type="text" name="datavalidade" id="Text2" ng-model="item.datavalidade" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_DataValidade' | translate}}"/>
                                </div>
                                <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                    <label for="status"><span translate="45.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>
                                    <select class="form-control input-sm" id="Select1" name="status" ng-model="item.status" required 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_Status' | translate}}">
                                        <option value="Ativo" ng-selected="item.status == 'Ativo'">Ativo</option>
                                        <option value="Inativo" ng-selected="item.status == 'Inativo'">Inativo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group" ng-class="{'has-error': form.produto.$invalid && form.submitted}">
                                    <label for="produto" translate="45.LBL_Produto">Produto </label><br />
                                    <input type="text" id="Text3" name="produto" ng-model="item.produto" class="form-control input-sm" 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_Produto' | translate}}"/>
                                </div>
                            </div>
                            <div class="form-actions center">
                                <button class="btn btn-info btn-sm" name="find" type="button" ng-click="onSearch(item)" ng-show="isPesquisa">
                                    <i class="icon-search bigger-110"></i><span translate="45.BTN_Pesquisar">Pesquisar</span>
                                </button>
                                <button id="Button2" name="fechar" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                    <i class="icon-remove bigger-110"></i><span translate="45.BTN_Fechar">Fechar</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label class="truncate-text-mobile width-80" translate="45.LBL_TitleCodigoBarrasPesosMedidas">{{ titulo }}</label>
            </div>
            <div class="widget-box">
                <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                    <%--<a class="btn btn-info btn-xs" ng-click="onFormMode()" name="NovoRegistro" title="{{ '45.TOOLTIP_NovoRegistro' | translate }}"><i class="fa fa-plus"></i></a>--%>
                    <a class="btn btn-success btn-xs" ng-click="onSearchMode(item)" name="NovaPesquisa" title="{{ '45.TOOLTIP_Pesquisar' | translate }}"><i class="fa fa-search"></i></a>
                    <ajuda codigo="77" tooltip-ajuda="'45.TOOLTIP_Ajuda'"></ajuda>
                </div>

                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                        <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i>
                        <i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                    </a>
                    
                    <div class="widget-main" style="min-height: 1310px;">
                        <div class="col-md-3 form-group" style="margin-top: 10px; padding-left:0px; padding-right:0px;">
                            <label for="quantidadecertificacoes">
                                <span translate="45.LBL_TotalQuantidadeCertificacoesDisponiveis">Total da Quantidade de Certificações Disponíveis</span>
                            </label><br />
                            <input type="text" id="Text6" name="quantidadecertificacoes" ng-model="certificacoes[0].totalquantidadecertificacoesdisponiveis" maxlength="6" class="form-control input-sm datepicker" disabled integer
                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_TotalQuantidadeCertificacoesDisponiveis' | translate}}"/>
                        </div>
                        <div class="table-responsive">
                            <table ng-table="tableCertificacaoProduto" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <thead>
                                    <tr>
                                        <th ng-click="tableCertificacaoProduto.sorting('produto', tableCertificacaoProduto.isSortBy('produto', 'asc') ? 'desc' : 'asc')" class="text-center">
                                            <span translate="45.GRID_Produto">Produto &nbsp;&nbsp;</span>
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableCertificacaoProduto.isSortBy('produto', 'asc'),
                                                'icon-sort-down': tableCertificacaoProduto.isSortBy('produto', 'desc')
                                             }"></i>
                                        </th>
                                        <th ng-click="tableCertificacaoProduto.sorting('dataliberacao', tableCertificacaoProduto.isSortBy('dataliberacao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                            <span translate="45.GRID_Liberacao">Liberação &nbsp;&nbsp;</span>
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableCertificacaoProduto.isSortBy('dataliberacao', 'asc'),
                                                'icon-sort-down': tableCertificacaoProduto.isSortBy('dataliberacao', 'desc')
                                            }"></i>
                                        </th>
                                        <th ng-click="tableCertificacaoProduto.sorting('datavalidade', tableCertificacaoProduto.isSortBy('datavalidade', 'asc') ? 'desc' : 'asc')" class="text-center">
                                            <span translate="45.GRID_Validade">Validade &nbsp;&nbsp;</span>
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableCertificacaoProduto.isSortBy('datavalidade', 'asc'),
                                                'icon-sort-down': tableCertificacaoProduto.isSortBy('datavalidade', 'desc')
                                            }"></i>
                                        </th>
                                        <th ng-click="tableCertificacaoProduto.sorting('quantidadecertificacoesdisponiveis', tableCertificacaoProduto.isSortBy('quantidadecertificacoesdisponiveis', 'asc') ? 'desc' : 'asc')" class="text-center">
                                            <span translate="45.LBL_QuantidadeCertificacoesDisponiveis">Quantidade de Certificações Disponíveis &nbsp;&nbsp;</span>
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableCertificacaoProduto.isSortBy('quantidadecertificacoesdisponiveis', 'asc'),
                                                'icon-sort-down': tableCertificacaoProduto.isSortBy('quantidadecertificacoesdisponiveis', 'desc')
                                            }"></i>
                                        </th>
                                        <th ng-click="tableCertificacaoProduto.sorting('status', tableCertificacaoProduto.isSortBy('status', 'asc') ? 'desc' : 'asc')" class="text-center">
                                            <span translate="45.GRID_Status">Status &nbsp;&nbsp;</span>
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableCertificacaoProduto.isSortBy('status', 'asc'),
                                                'icon-sort-down': tableCertificacaoProduto.isSortBy('status', 'desc')
                                            }"></i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)">
                                        <td ng-style="{ 'width': '40%' }" sortable="'produto'">{{item1.produto}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'dataliberacao'">{{item1.dataliberacao}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'validade'">{{item1.datavalidade}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'quantidadecertificacoesdisponiveis'">{{item1.quantidadecertificacoesdisponiveis}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'status'">{{item1.status}}</td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="5" class="text-center" translate="45.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="45.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="45.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>

    <div>
        <script type="text/ng-template" id="modalProduto">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content" on-enter="onFilter(itemForm)">
                <div class="modal-header">
                    <h3 class="modal-title" translate="45.LBL_SelecionarProduto">Selecionar Produto <popup-ajuda codigo="50" tooltip-ajuda="'45.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form id="form" name="form" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue" translate="45.LBL_PreenchaAoMenosUmFiltro">Preencha ao menos um filtro abaixo:</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <span translate="45.LBL_Filtros">Filtros</span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="pesquisaGTIN" translate="45.LBL_GTIN">GTIN</label>
                                <input type="text" id="pesquisaGTIN" class="form-control input-sm" ng-model="itemForm.gtin" maxlength="14" integer 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_GTIN' | translate}}"/>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="pesquisaProduto" translate="45.LBL_DescricaoProduto">Descrição do Produto</label>
                                <input id="pesquisaProduto" class="form-control input-sm" type="text" ng-model="itemForm.descricao" maxlength="255" 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_DescricaoProduto' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="tipogtin" translate="45.LBL_TipoGTIN">Tipo de GTIN</label>
                                <select class="form-control input-sm" id="tipogtin" name="tipogtin" ng-model="itemForm.codigotipogtin" ng-options="licenca.codigo as licenca.nome for licenca in licencas" 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_TipoGTIN' | translate}}">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="statusgtin" translate="45.LBL_StatusGTIN">Status do GTIN</label>
                                <select class="form-control input-sm" id="statusgtin" name="statusgtin" ng-model="itemForm.codigostatusgtin"
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.45.LBL_StatusGTIN' | translate}}"> <!--ng-options="status.codigo as status.nome for status in statusgtin"-->
                                    <option value=""> Selecione</option>
                                    <option ng-repeat="status in statusgtin" value="{{status.codigo}}" ng-if="status.codigo == 1 || status.codigo == 3 || status.codigo == 6">{{status.nome}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="center">
                            <button name="find" class="btn btn-info btn-sm" type="button" ng-click="onFilter(itemForm)">
                                <i class="icon-ok bigger-110"></i><span translate="45.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <div class="table-responsive" style="margin-bottom: 20px;">
                                <table ng-table="tableProdutos" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableProdutos.sorting('productdescription', tableProdutos.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                              <span translate="45.GRID_Produto">Produto</span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('productdescription', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('productdescription', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableProdutos.sorting('globaltradeitemnumber', tableProdutos.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                              <span translate="45.GRID_GTIN">GTIN</span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('globaltradeitemnumber', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('globaltradeitemnumber', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableProdutos.sorting('nometipogtin', tableProdutos.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')">
                                              <span translate="45.GRID_TipoGTIN">Tipo de GTIN</span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('nometipogtin', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('nometipogtin', 'desc')
                                              }"></i>
                                            </th>
                                            <th>
                                              <span translate="45.GRID_Selecionar">Selecionar</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data">
                                            <td ng-style="{ 'width': '35%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                            <td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Selecionar'">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedProduto(item1)">
                                                    <i class="icon-ok bigger-110"></i><span translate="45.BTN_Selecionar">Selecionar</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="4" class="text-center" translate="45.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" translate="45.BTN_Cancelar">Cancelar</button>
                </div>
            </div>
        </script>
    </div>
</div>
