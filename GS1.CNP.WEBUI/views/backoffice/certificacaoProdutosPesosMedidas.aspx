﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="certificacaoProdutosPesosMedidas.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.certificacaoProdutosPesosMedidas" %>
<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/certificacaoProdutosPesosMedidas";
    }

</script>

<style type="text/css">
    .input-group-btn>button{
        padding: 2px;
    }

    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak],
    .ng-cloak, .x-ng-cloak,
    .ng-hide {
        display: none !important;
    }

    .has-error-label
    {
        color: #f09784;
    }

</style>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="CertificacaoProdutoPesosMedidasCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = ''; gerar = false;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label class="truncate-text-mobile" ng-class="{'width-80': isPesquisa, 'width-100': !isPesquisa}" translate="46.LBL_TitlePesosMedidas">{{ titulo }}</label>
            </div>

            <div class="pull-right" style="margin-top:-35px; margin-right:2px;">
                <a class="btn btn-danger btn-xs" ng-show="searchMode" ng-click="onClean(item)" title="{{ '46.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="82" ng-show="searchMode" tooltip-ajuda="'46.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="83" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'46.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="84" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'46.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            </div>

            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="tab-content">
                        <div id="geracaoEpc" class="tab-pane active" ng-show="!isPesquisa">
                            <form name="form" id="form" novalidate class="form" role="form">
                                <div class="row text-center">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        <h4 translate="46.LBL_CertificarProduto">Certificar Produto</h4>
                                        <label translate="46.LBL_BusqueSelecioneCertificacao">Busque e selecione o item para gerar a certificação</label>
                                    </div>
                                </div>
                                <div>
                                    <h3 class="header smaller lighter blue">
                                        <span translate="46.LBL_ProdutoSelecionado">Produto <span class="hidden-xs">Selecionado</span></span>
                                        <span class="span5 pull-right" ng-if="initMode">
                                            <label class="pull-right inline">
                                                <div class="form-group">
                                                    <a class="btn btn-success btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto != undefined && item.codigoproduto != null"
                                                        ng-click="onProduto(item)" title="{{ '46.TOOLTIP_PesquisarProduto' | translate }}">
                                                        <i class="fa fa-pencil"></i> <span class="hidden-xs" translate="46.BTN_AlterarProduto">Alterar Produto</span>
                                                    </a>
                                                    <a class="btn btn-info btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto == undefined || item.codigoproduto == null"
                                                        ng-click="onProduto(item)" title="{{ '46.TOOLTIP_PesquisarProduto' | translate }}">
                                                        <i class="fa fa-search"></i> <span class="hidden-xs" translate="46.BTN_SelecionarProduto">Selecionar Produto</span>
                                                    </a>
                                                </div>
                                            </label>
                                        </span>
                                    </h3>
                                    <div ng-show="item.codigoproduto != undefined && item.codigoproduto != null">
                                        <label>Produto: {{item.productdescription}}</label><br />
                                        <label ng-show="item.globaltradeitemnumber != undefined && item.globaltradeitemnumber != null">GTIN: {{item.globaltradeitemnumber}}</label>
                                    </div>
                                    <div ng-show="item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == ''"
                                        ng-class="{'has-error-label': (item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == '') && form.submitted}">
                                        <label translate="46.LBL_NenhumProdutoSelecionado">Nenhum produto selecionado.</label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="row" style="padding-top: 15px;">
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.datacertificado.$invalid && form.submitted}">
                                        <label for="datacertificado" data-toggle="popover" data-trigger="hover" data-content="Selecionar a data de Certificação.">
                                            <span translate="46.LBL_DataCertificado">Data do Certificado</span> <span ng-show="!searchMode">(*)</span>
                                        </label><br />
                                        <input type="text" name="datacertificado" ng-model="item.datacertificado" maxlength="10" class="form-control input-sm datepicker"
                                            ng-change="calcularValidade(item.datacertificado)" sbx-datepicker data-date-format="dd/mm/yyyy" required/>
                                    </div>
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.datavalidade.$invalid && form.submitted}">
                                        <label for="datavalidade" data-toggle="popover" data-trigger="hover" data-content="Selecionar a data de Validade da certificação.">
                                            <span translate="46.LBL_DataValidade">Data de Validade</span> <span ng-show="!searchMode">(*)</span>
                                        </label><br />
                                        <input type="text" name="datavalidade" ng-model="item.datavalidade" maxlength="10" class="form-control input-sm datepicker"
                                            sbx-datepicker data-date-format="dd/mm/yyyy" ng-required="true" disabled/>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                        <label for="status"><span translate="46.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="item.status" required>
                                            <option value="1" ng-selected="item.status == 1">Ativo</option>
                                            <option value="0" ng-selected="item.status == 0">Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-actions center">
                                    <button class="btn btn-info btn-sm" ng-click="onSave(item)" ng-show="!isPesquisa">
                                        <i class="icon-save bigger-110"></i><span translate="46.BTN_Salvar">Salvar</span>
                                    </button>
                                    <button id="Button3" name="fechar" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="46.BTN_Fechar">Fechar</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div id="Div1" class="tab-pane active" ng-show="isPesquisa">
                            <div class="row" style="padding-top: 15px;">
                                <div class="col-md-3 form-group" ng-class="{'has-error': form.datacertificado.$invalid && form.submitted}">
                                    <label for="datacertificado" data-toggle="popover" data-trigger="hover" data-content="Selecionar a data de Certificação.">
                                        <span translate="46.LBL_DataCertificado">Data do Certificado</span> <span ng-show="!searchMode">(*)</span>
                                    </label><br />
                                    <input type="text" id="Text1" name="datacertificado" ng-model="item.datacertificado" maxlength="10" class="form-control input-sm datepicker"
                                        sbx-datepicker data-date-format="dd/mm/yyyy"/>
                                </div>
                                <div class="col-md-3 form-group" ng-class="{'has-error': form.datavalidade.$invalid && form.submitted}">
                                    <label for="datavalidade" data-toggle="popover" data-trigger="hover" data-content="Selecionar a data de Validade da certificação.">
                                        <span translate="46.LBL_DataValidade">Data de Validade</span> <span ng-show="!searchMode">(*)</span>
                                    </label><br />
                                    <input type="text" name="datavalidade" id="Text2" ng-model="item.datavalidade" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy"/>
                                </div>
                                <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                    <label for="status"><span translate="46.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>
                                    <select class="form-control input-sm" id="Select1" name="status" ng-model="item.status">
                                        <option value="1" ng-selected="item.status == 1">Ativo</option>
                                        <option value="0" ng-selected="item.status == 0">Inativo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group" ng-class="{'has-error': form.produto.$invalid && form.submitted}">
                                    <label for="produto" data-toggle="popover" data-trigger="hover" data-content="Preencha a descrição do Produto." translate="46.LBL_DescricaoProduto">Descrição do Produto </label><br />
                                    <input type="text" id="Text3" name="produto" ng-model="item.produto" class="form-control input-sm"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="tipogtin" translate="46.LBL_TipoGTIN">Tipo de GTIN</label>
                                    <select class="form-control input-sm" id="tipogtin" name="tipogtin" ng-model="item.codigotipogtin" ng-options="licenca.codigo as licenca.nome for licenca in licencas">
                                        <option value="">Todos</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions center">
                                <button class="btn btn-info btn-sm" type="button" ng-click="onSearch(item)" ng-show="isPesquisa">
                                    <i class="icon-search bigger-110"></i><span translate="46.BTN_Pesquisar">Pesquisar</span>
                                </button>
                                <button id="Button2" name="fechar" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                    <i class="icon-remove bigger-110"></i><span translate="46.BTN_Fechar">Fechar</span>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label class="truncate-text-mobile width-80" translate="46.LBL_TitlePesosMedidas">{{ titulo }}</label>
            </div>
            <div class="widget-box">
                <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                    <a class="btn btn-info btn-xs" ng-click="onFormMode()" name="NovoRegistro" title="{{ '46.TOOLTIP_NovoRegistro' | translate }}"><i class="fa fa-plus"></i></a>
                    <a class="btn btn-success btn-xs" name="NovaPesquisa" ng-click="onSearchMode(item)" title="{{ '46.TOOLTIP_Pesquisar' | translate }}"><i class="fa fa-search"></i></a>
                    <ajuda codigo="81" tooltip-ajuda="'46.TOOLTIP_Ajuda'"></ajuda>
                </div>

                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                        <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                    </a>
                    <div class="widget-main" style="min-height: 1310px;">
                        <div class="table-responsive">
                            <table ng-table="tableCertificacaoProduto" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <thead>
                                    <tr>
                                        <th ng-click="tableCertificacaoProduto.sorting('globaltradeitemnumber', tableCertificacaoProduto.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="46.GRID_GTIN">GTIN &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCertificacaoProduto.isSortBy('globaltradeitemnumber', 'asc'),
                                            'icon-sort-down': tableCertificacaoProduto.isSortBy('globaltradeitemnumber', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableCertificacaoProduto.sorting('descricaoproduto', tableCertificacaoProduto.isSortBy('descricaoproduto', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="46.GRID_DescricaoProduto">Descrição &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCertificacaoProduto.isSortBy('descricaoproduto', 'asc'),
                                            'icon-sort-down': tableCertificacaoProduto.isSortBy('descricaoproduto', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableCertificacaoProduto.sorting('nometipogtin', tableCertificacaoProduto.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="46.GRID_TipoGTIN">Tipo de GTIN &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCertificacaoProduto.isSortBy('nometipogtin', 'asc'),
                                            'icon-sort-down': tableCertificacaoProduto.isSortBy('nometipogtin', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableCertificacaoProduto.sorting('datavalidade', tableCertificacaoProduto.isSortBy('datavalidade', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="46.GRID_Validade">Validade &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCertificacaoProduto.isSortBy('datavalidade', 'asc'),
                                            'icon-sort-down': tableCertificacaoProduto.isSortBy('datavalidade', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableCertificacaoProduto.sorting('status', tableCertificacaoProduto.isSortBy('status', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="46.GRID_Status">Status &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCertificacaoProduto.isSortBy('status', 'asc'),
                                            'icon-sort-down': tableCertificacaoProduto.isSortBy('status', 'desc')
                                          }"></i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)">
                                        <td ng-style="{ 'width': '10%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                        <td ng-style="{ 'width': '20%' }" sortable="'descricaoproduto'">{{item1.descricaoproduto}}</td>
                                        <td ng-style="{ 'width': '10%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'validade'">{{item1.datavalidade}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'status'">{{item1.status | status}}</td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="5" class="text-center" translate="46.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="46.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="46.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>

    <div>
        <script type="text/ng-template" id="modalProduto">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content" on-enter="onFilter(itemForm)">
                <div class="modal-header">
                    <h3 class="modal-title" translate="46.LBL_SelecionarProduto">Selecionar Produto <popup-ajuda codigo="50" tooltip-ajuda="'46.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form id="form" name="form" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue" translate="46.LBL_PreenchaAoMenosUmFiltro">Preencha ao menos um filtro abaixo:</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <span translate="46.LBL_Filtros">Filtros</span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="pesquisaGTIN" translate="46.LBL_GTIN">GTIN</label>
                                <input type="text" id="pesquisaGTIN" class="form-control input-sm" ng-model="itemForm.gtin" maxlength="14" integer />
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="pesquisaProduto" translate="46.LBL_DescricaoProduto">Descrição do Produto</label>
                                <input id="pesquisaProduto" class="form-control input-sm" type="text" ng-model="itemForm.descricao" maxlength="255" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="tipogtin" translate="46.LBL_TipoGTIN">Tipo de GTIN</label>
                                <select class="form-control input-sm" id="tipogtin" name="tipogtin" ng-model="itemForm.codigotipogtin" ng-options="licenca.codigo as licenca.nome for licenca in licencas">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="statusgtin" translate="46.LBL_StatusGTIN">Status do GTIN</label>
                                <select class="form-control input-sm" id="statusgtin" name="statusgtin" ng-model="itemForm.codigostatusgtin"> <!--ng-options="status.codigo as status.nome for status in statusgtin"-->
                                    <option value=""> Selecione</option>
                                    <option ng-repeat="status in statusgtin" value="{{status.codigo}}" ng-if="status.codigo == 1 || status.codigo == 3 || status.codigo == 6">{{status.nome}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="center">
                            <button name="pesquisar" class="btn btn-info btn-sm" type="button" ng-click="onFilter(itemForm)">
                                <i class="icon-ok bigger-110"></i><span translate="46.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <div class="table-responsive" style="margin-bottom: 20px;">
                                <table ng-table="tableProdutos" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableProdutos.sorting('productdescription', tableProdutos.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                              <span translate="46.GRID_Produto">Produto</span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('productdescription', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('productdescription', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableProdutos.sorting('globaltradeitemnumber', tableProdutos.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                              <span translate="46.GRID_GTIN">GTIN</span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('globaltradeitemnumber', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('globaltradeitemnumber', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableProdutos.sorting('nometipogtin', tableProdutos.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')">
                                              <span translate="46.GRID_TipoGTIN">Tipo de GTIN</span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('nometipogtin', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('nometipogtin', 'desc')
                                              }"></i>
                                            </th>
                                            <th>
                                              <span translate="46.GRID_Selecionar">Selecionar</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data">
                                            <td ng-style="{ 'width': '35%' }" sortable="'productdescription'" translate="46.GRID_Produto">{{item1.productdescription}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'globaltradeitemnumber'" translate="46.GRID_GTIN">{{item1.globaltradeitemnumber}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'nometipogtin'" translate="46.GRID_TipoGTIN">{{item1.nometipogtin}}</td>
                                            <td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Selecionar'">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedProduto(item1)">
                                                    <i class="icon-ok bigger-110"></i><span translate="46.BTN_Selecionar">Selecionar</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="4" class="text-center" translate="46.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" translate="46.BTN_Cancelar">Cancelar</button>
                </div>
            </div>
        </script>
    </div>
</div>
