﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="codigoSerieUnidadeLogistica.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.codigoSerieUnidadeLogistica" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .wysiwyg-menu{
        text-align: center !important;
    }
</style>

<div ng-controller="CodigoSerieUnidadeLogisticaCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false;" style="height:1050px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container style="width: 100%;">
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo"></label>
        </div>
        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" ng-show="searchMode" title="{{ '67.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="98"></ajuda>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" novalidate class="form" role="form" >
                        <div class="tab-content" style="min-height:400px;">
                            <div id="telas" class="tab-pane active">
                                <div class="row">
                                    <div class="form-group col-md-4" ng-class="{'has-error': form.sscc.$invalid && form.submitted}">
                                        <label for="sscc" translate="67.LBL_SSCC">SSCC </label>
                                        <input id="sscc" name="sscc" type="text" class="form-control input-sm" ng-model="itemForm.sscc" integer maxlength="18" required/>
                                    </div>
                                    <div class="form-group col-md-4" style="margin-top: 29px;">
                                        <button name="consultar" class="btn btn-info btn-xs" type="button" ng-click="onSearch(itemForm)">
                                            <i class="icon-search bigger-110"></i> <span translate="67.BTN_Consultar">Consultar</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="table-responsive" ng-if="!isPesquisa && consultaefetuada" style="margin-top: 20px;">
                                    <table ng-table="tableCampos" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th ng-click="tableCampos.sorting('codigoserie', tableCampos.isSortBy('codigoserie', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_CodigoSerieUnidadeLogistica"> Código de Série Unidade Logística </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('codigoserie', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('codigoserie', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableCampos.sorting('codigoretorno', tableCampos.isSortBy('codigoretorno', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_CodigoRetorno"> Código de Retorno </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('codigoretorno', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('codigoretorno', 'desc')
                                                  }"></i>
                                                </th>

                                                <th ng-click="tableCampos.sorting('gcp', tableCampos.isSortBy('gcp', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_GCP"> GCP </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('gcp', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('gcp', 'desc')
                                                  }"></i>
                                                </th>                                               
                                           

                                                <th ng-click="tableCampos.sorting('subdivisao', tableCampos.isSortBy('subdivisao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_subdivisao"> Subdivisão </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('subdivisao', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('subdivisao', 'desc')
                                                  }"></i>
                                                </th>

                                                <th ng-click="tableCampos.sorting('postalcode', tableCampos.isSortBy('postalcode', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_postalcode"> Código Postal </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('postalcode', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('postalcode', 'desc')
                                                  }"></i>
                                                </th>

                                                <th ng-click="tableCampos.sorting('city', tableCampos.isSortBy('city', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_city"> Cidade </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('city', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('city', 'desc')
                                                  }"></i>
                                                </th>

                                                <th ng-click="tableCampos.sorting('countrycode', tableCampos.isSortBy('countrycode', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_countrycode"> Código ISO do País </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('countrycode', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('countrycode', 'desc')
                                                  }"></i>
                                                </th>

                                                <th ng-click="tableCampos.sorting('partyrole', tableCampos.isSortBy('partyrole', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_partyrole"> Regra de Acesso da Empresa </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('partyrole', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('partyrole', 'desc')
                                                  }"></i>
                                                </th>

                                                <th ng-click="tableCampos.sorting('additionalpartyidentification', tableCampos.isSortBy('additionalpartyidentification', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_additionalpartyidentification"> additionalpartyid </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('additionalpartyidentification', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('additionalpartyidentification', 'desc')
                                                  }"></i>
                                                </th>
                                             
                                                <th ng-click="tableCampos.sorting('streetaddressone', tableCampos.isSortBy('streetaddressone', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_streetaddressone"> Rua </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('streetaddressone', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('streetaddressone', 'desc')
                                                  }"></i>
                                                </th>

                                                <th ng-click="tableCampos.sorting('contato', tableCampos.isSortBy('contato', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_contato"> Contato </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('contato', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('contato', 'desc')
                                                  }"></i>
                                                </th>

                                                                                                                                      

                                                <th ng-click="tableCampos.sorting('gln', tableCampos.isSortBy('gln', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="67.GRID_gln"> Provedor de Informações GLN </span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableCampos.isSortBy('gln', 'asc'),
                                                    'icon-sort-down': tableCampos.isSortBy('gln', 'desc')
                                                  }"></i>
                                                </th>

                                             
                               
                                            </tr>
                                            
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="campo in $data" style="cursor:pointer;word-wrap: break-word;">
                                                <td ng-style="{ 'width': '5%' }" sortable="'codigoserie'">{{itemForm.sscc}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'codigoretorno'">{{campo.returnCode.Value}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'gcp'">{{campo.gS1CompanyPrefix}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'nomefantasia'">{{campo.gS1CompanyPrefixLicensee.partyName[0]}}</td>                                                
                                                <td ng-style="{ 'width': '5%' }" sortable="'postalcode'">{{campo.address.postalCode}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'city'">{{campo.address.city}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'city'">{{campo.address.countryCode.Value}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'city'">{{campo.gS1CompanyPrefixLicensee.partyRole[0].Value}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'additionalpartyid'">{{campo.gS1CompanyPrefixLicensee.additionalPartyIdentification[0].Value}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'endereco'">{{campo.address.streetAddressOne}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'contato'"><span ng-repeat="contact in campo.contact">{{contact.personName}} <span ng-repeat="communicationChannel in contact.communicationChannel" ng-if="communicationChannel.communicationChannelCode.Value == 'EMAIL'">- {{communicationChannel.communicationChannelCode.Value}}  {{communicationChannel.communicationValue}}</span></span></td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'gln'">{{campo.informationProvider.gln}}</td>


                                            </tr>
                                            <tr ng-show="$data.length == 0 && consultaefetuada">
                                                <td colspan="13" class="text-center"><span translate="67.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="17.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="17.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>

</div>

