﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="configuracoesGepir.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.configuracoesGepir" %>

<script type="text/javascript">
    
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/parametros";
    }

</script>
<style type="text/css">
    .form-group{ margin-bottom: 10px;}
    .form-actions{ margin-bottom: 0px;}
</style>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="ConfiguracaoGepirCtrl" class="page-container" ng-init="entity = 'parametrosSistema';" style="height: 800px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label>{{ titulo }}</label>
        </div>

        
        <div class="pull-right" style="margin-top: -32px; margin-right: 100px;">
            <ajuda codigo="96" tooltip-ajuda="'65.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" id="form" novalidate class="form" role="form">
                        <div class="tab-content"><%--style="height: 550px;"--%>
                            <div id="geral" class="tab-pane active">
                                <div style="max-height:705px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                    
                                    <div class="">
                                        <div class="form-group">
                                            <div class="widget-box">
											    <div class="widget-header widget-header-blue widget-header-flat">
												    <h5>
													    <i class="icon-signal"></i>
													    <span translate="65.LBL_PerfilEspecificoEmpresa">Perfil específico por empresa</span>
												    </h5>
                                                                                          
											    </div>
                                                <div class="widget-body">
												    <div class="widget-main">
                                                        

                                                        <div class="row">
                                                            <div class="form-group col-md-4 col-xs-12" ng-class="{'has-error': form.empresaassociada.$invalid && form.empresaassociada.submitted}" ng-show="showFieldsSearch">
                                                                <label>
                                                                    <span class="span-without-padding" translate="65.LBL_Empresa">Empresa</span>
                                                                </label>
                                                                <input id="Text1" type="text" name="empresaassociada" ng-model="itemParam.empresaassociada" maxlength="255" ng-blur="limpaEmpresa()"
                                                                    placeholder="{{ '10.PLACEHOLDER_PesquisarAssociado' | translate }}" typeahead="associadosDoUsuario as associadosDoUsuario.cad+'-'+associadosDoUsuario.nome for associadosDoUsuario in BuscarAssociadosAutoComplete($viewValue)"
                                                                    typeahead-min-length="3" class="typeahead form-control input-sm" ng-required="isRequiredGepir"
                                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.65.LBL_EmpresaAssociada' | translate}}"/>
                                                            </div>
                                                            <div class="form-group col-md-4 col-xs-12" ng-class="{'has-error': form.perfil.$invalid && form.perfil.submitted}" ng-show="showFieldsSearch">
                                                                <label>
                                                                    <span class="span-without-padding" translate="65.LBL_Perfil">Perfil</span>
                                                                </label>                                                                
                                                                <select class="form-control input-sm" id="Select1" name="perfilpadrao" ng-model="itemParam.perfilpadrao" ng-options="profile.id as profile.name for profile in  configurationprofile">
                                                                    <option value="">Todos</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-4 col-sm-6" style="margin-top: 28px;" ng-show="showFieldsSearch">
                                                                <button ng-cloak ng-show="insertGepirMode" name="salvar" ng-click="event.preventDefault(); onSearch(itemGepir);" class="btn btn-info btn-xs" type="button">
                                                                    <i class="fa fa-search"></i>
                                                                </button>
                                                                
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-4 col-xs-12" ng-class="{'has-error': form.empresaassociada.$invalid && form.empresaassociada.submitted}" ng-show="showFieldsEdit">
                                                                <label>
                                                                    <span class="span-without-padding" translate="65.LBL_Empresa">Empresa</span>
                                                                </label>
                                                                <input id="Text2" type="text" name="empresaassociada" ng-model="nomeassociado" maxlength="255"                                                                    
                                                                    class="typeahead form-control input-sm" readonly
                                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_EmpresaAssociada' | translate}}"/>
                                                            </div>
                                                            <div class="form-group col-md-4 col-xs-12" ng-class="{'has-error': form.perfil.$invalid && form.perfil.submitted}" ng-show="showFieldsEdit">
                                                                <label>
                                                                    <span class="span-without-padding" translate="65.LBL_Perfil">Perfil</span>
                                                                </label>                                                                
                                                                <select class="form-control input-sm" id="Select2" name="perfilpadrao" ng-model="itemParam.perfilpadrao" ng-options="profile.id as profile.name for profile in  configurationprofile">
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-4 col-sm-6" style="margin-top: 28px;" ng-show="showFieldsEdit">
                                                                <button ng-cloak  ng-click="event.preventDefault(); onEdit(itemParam);" name="atualizar" class="btn btn-info btn-xs" type="button">
                                                                    <i class="icon-save"></i>
                                                                </button>
                                                                <button ng-click="event.preventDefault(); cancelarEmpresa();" name="atualizar" class="btn btn-danger btn-xs" type="button">
                                                                    <i class="icon-remove"></i>
                                                                </button>
                                                                
                                                            </div>
                                                        </div>


                                                       
                                                        <div class="table-responsive">
                                                            <table ng-table="tableEmpresaGepir" class="table table-bordered table-striped table-hover" ng-class="tableNaoExibeVoltar">
                                                                <thead>
                                                                    <tr>
                                                                        <th ng-click="tableEmpresaGepir.sorting('empresa', tableEmpresaGepir.isSortBy('empresa', 'asc') ? 'desc' : 'asc')">
                                                                          <span translate="65.GRID_Empresa">Empresa</span>&nbsp;&nbsp;
                                                                          <i class="text-center icon-sort" ng-class="{
                                                                            'icon-sort-up': tableEmpresaGepir.isSortBy('empresa', 'asc'),
                                                                            'icon-sort-down': tableEmpresaGepir.isSortBy('empresa', 'desc')
                                                                          }"></i>
                                                                        </th>
                                                                        <th ng-click="tableEmpresaGepir.sorting('cad', tableEmpresaGepir.isSortBy('cad', 'asc') ? 'desc' : 'asc')">
                                                                          <span translate="65.GRID_CAD">CAD</span>&nbsp;&nbsp;
                                                                          <i class="text-center icon-sort" ng-class="{
                                                                            'icon-sort-up': tableEmpresaGepir.isSortBy('cad', 'asc'),
                                                                            'icon-sort-down': tableEmpresaGepir.isSortBy('cad', 'desc')
                                                                          }"></i>
                                                                        </th>
                                                                        <th ng-click="tableEmpresaGepir.sorting('perfil', tableEmpresaGepir.isSortBy('perfil', 'asc') ? 'desc' : 'asc')">
                                                                          <span translate="65.GRID_Perfil">Perfil</span>&nbsp;&nbsp;
                                                                          <i class="text-center icon-sort" ng-class="{
                                                                            'icon-sort-up': tableEmpresaGepir.isSortBy('perfil', 'asc'),
                                                                            'icon-sort-down': tableEmpresaGepir.isSortBy('perfil', 'desc')
                                                                          }"></i>
                                                                        </th>
                                                                        <th ng-click="tableEmpresaGepir.sorting('limitediario', tableEmpresaGepir.isSortBy('limitediario', 'asc') ? 'desc' : 'asc')">
                                                                          <span translate="65.GRID_LimiteDiario">Limite Diário</span>&nbsp;&nbsp;
                                                                          <i class="text-center icon-sort" ng-class="{
                                                                            'icon-sort-up': tableEmpresaGepir.isSortBy('limitediario', 'asc'),
                                                                            'icon-sort-down': tableEmpresaGepir.isSortBy('limitediario', 'desc')
                                                                          }"></i>
                                                                        </th>
                                                                        <th ng-click="tableEmpresaGepir.sorting('limitesemanal', tableEmpresaGepir.isSortBy('limitesemanal', 'asc') ? 'desc' : 'asc')">
                                                                          <span translate="65.GRID_LimiteSemanal">Limite Semanal</span>&nbsp;&nbsp;
                                                                          <i class="text-center icon-sort" ng-class="{
                                                                            'icon-sort-up': tableEmpresaGepir.isSortBy('limitesemanal', 'asc'),
                                                                            'icon-sort-down': tableEmpresaGepir.isSortBy('limitesemanal', 'desc')
                                                                          }"></i>
                                                                        </th>
                                                                        <th ng-click="tableEmpresaGepir.sorting('limitemensal', tableEmpresaGepir.isSortBy('limitemensal', 'asc') ? 'desc' : 'asc')">
                                                                          <span translate="65.GRID_LimiteMensal">Limite Mensal</span>&nbsp;&nbsp;
                                                                          <i class="text-center icon-sort" ng-class="{
                                                                            'icon-sort-up': tableEmpresaGepir.isSortBy('limitemensal', 'asc'),
                                                                            'icon-sort-down': tableEmpresaGepir.isSortBy('codlimitemensaligotipourl', 'desc')
                                                                          }"></i>
                                                                        </th>                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="item1 in $data" ng-click="onAlterGepir(item1)" ng-show="item1.status != 'excluido' && item1.status != 'excluido2'">
                                                                        <td ng-style="{ 'width': '50%' }">{{item1.nome}}</td>
                                                                        <td ng-style="{ 'width': '5%' }">{{item1.cad}}</td>
                                                                        <td ng-style="{ 'width': '15%' }">{{item1.codigoperfilgepir| perfilgepir:configurationprofile:'name'}}</td>
                                                                        <td ng-style="{ 'width': '10%' }">{{item1.codigoperfilgepir| perfilgepir:configurationprofile:'dailyqueries'}}</td>
                                                                        <td ng-style="{ 'width': '10%' }">{{item1.codigoperfilgepir| perfilgepir:configurationprofile:'weeklyqueries'}}</td>
                                                                        <td ng-style="{ 'width': '10%' }">{{item1.codigoperfilgepir | perfilgepir:configurationprofile:'monthlyqueries'}}</td>                                                                        
                                                                    </tr>
                                                                    <tr ng-show="$data.length == 0">
                                                                        <td colspan="7" class="text-center" translate="65.LBL_NenhumRegistroEncontradoPerfil">Nenhuma perfil encontrado</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
										    </div><!-- /widget-box -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>                   
                </div>
            </div>
        </div>

    </div>

</div>
