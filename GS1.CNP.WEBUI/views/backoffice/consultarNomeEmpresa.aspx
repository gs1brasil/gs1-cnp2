﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="consultarNomeEmpresa.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.consultarNomeEmpresa" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .wysiwyg-menu{
        text-align: center !important;
    }
</style>

<div ng-controller="GepirNomeEmpresaCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false;" style="height:1050px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container style="width: 100%;">
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label ng-bind="titulo"></label>
            </div>
            <div class="pull-right" style="margin-top: -34px;">
                <a class="btn btn-danger btn-xs" ng-show="searchMode" title="{{ '69.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="100"></ajuda>
            </div>
            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form name="form" novalidate class="form" role="form" >
                            <div class="tab-content" style="min-height:400px;">
                                <div id="telas" class="tab-pane active">
                                    <div class="row">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.nomeempresa.$invalid && form.submitted}">
                                            <label for="nomeempresa" translate="69.LBL_NomeEmpresa">Nome da Empresa </label>
                                            <input id="nomeempresa" name="nomeempresa" type="text" class="form-control input-sm" ng-model="itemForm.nomeempresa" required/>
                                        </div>
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.pais.$invalid && form.submitted}">
                                            <label for="pais" translate="69.GRID_Pais">País </label>
                                            <select class="form-control input-sm" id="pais" name="pais" ng-model="itemForm.pais" required
                                                ng-options="pais.sigla2 as pais.pais for pais in paises" ng-change="onSelectedCountry(itemForm.pais)">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4" style="margin-top: 29px;">
                                            <button name="consultar" class="btn btn-info btn-xs" type="button" ng-click="onSearch(itemForm)">
                                                <i class="icon-search bigger-110"></i> <span translate="69.BTN_Consultar">Consultar</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row" ng-if="false">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.cidade.$invalid && form.submitted}">
                                            <label for="cidade" translate="69.LBL_Cidade">Cidade </label>
                                            <input id="cidade" name="cidade" type="text" class="form-control input-sm" ng-model="itemForm.cidade" />
                                        </div>
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.endereco.$invalid && form.submitted}">
                                            <label for="endereco" translate="69.LBL_Endereco">Endereço </label>
                                            <input id="endereco" name="endereco" type="text" class="form-control input-sm" ng-model="itemForm.endereco" />
                                        </div>
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.codigopostal.$invalid && form.submitted}">
                                            <label for="codigopostal" translate="69.LBL_CodigoPostal">Código Postal </label>
                                            <input id="codigopostal" name="codigopostal" type="text" class="form-control input-sm" ng-model="itemForm.codigopostal" />
                                        </div>
                                    </div>
                                    <div class="table-responsive" ng-show="consultaefetuada" style="margin-top: 20px;">
                                        <table ng-table="tableCampos" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th ng-click="tableCampos.sorting('gln', tableCampos.isSortBy('gln', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="69.GRID_NumeroGlobalLocalizacaoFisica"> Número Global de localização Física </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('gln', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('gln', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('codigoretorno', tableCampos.isSortBy('codigoretorno', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="69.GRID_CodigoRetorno"> Código de Retorno </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('codigoretorno', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('codigoretorno', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('nomefantasia', tableCampos.isSortBy('nomefantasia', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="69.GRID_NomeFantasia"> Nome Fantasia </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('nomefantasia', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('nomefantasia', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('additionalpartyid', tableCampos.isSortBy('additionalpartyid', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="69.GRID_AdditionalPartyID"> AdditionalPartyID </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('additionalpartyid', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('additionalpartyid', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('endereco', tableCampos.isSortBy('endereco', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="69.GRID_Endereco"> Endereço </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('endereco', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('endereco', 'desc')
                                                      }"></i>
                                                    </th>
                                                </tr>
                                            
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="campo in $data" style="cursor:pointer;">
                                                    <td ng-style="{ 'width': '20%' }" sortable="'gtin'">{{campo.gS1CompanyPrefixLicensee.gln}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'codigoretorno'">{{campo.returnCode.Value}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'nomefantasia'">{{campo.gS1CompanyPrefixLicensee.partyName[0]}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'additionalpartyid'">{{campo.gS1CompanyPrefixLicensee.additionalPartyIdentification[0].Value}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'endereco'">{{campo.address.streetAddressOne}}</td>
                                                </tr>
                                                <tr ng-show="$data.length == 0">
                                                    <td colspan="5" class="text-center"><span translate="69.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="17.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="17.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>