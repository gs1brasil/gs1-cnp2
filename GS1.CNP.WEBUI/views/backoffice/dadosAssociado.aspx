﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dadosAssociado.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.dadosAssociado" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />



<div ng-controller="DadosAssociado" class="page-container" ng-init="entity = 'dadosAssociado'; " style="min-height: 837px;">
    <div class="form-container" form-container id="formcontainer"> <%--id usado para corrigir bug do ie no layout, no momento de mudar do grid para o form--%>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo"></label>
        </div>
        <div class="widget-toolbar my-tab" style="margin-top: -36px;">
            <ul id="tabs-associado" class="nav nav-tabs">
                <li id="liTabInfo" class="active">
                    <a data-toogle="tab" href="#info" translate="11.ABA_InformacoesGerais">Informações Gerais</a>
                </li>
                <li id="liTabRepresentante" ng-show="!searchMode">
                    <a data-toogle="tab" href="#representante" ng-clock translate="11.ABA_RepresentanteLegal">Representante Legal</a>
                </li>
                <li id="liTabLicencas" ng-show="!searchMode">
                    <a data-toogle="tab" href="#licencas" ng-clock translate="11.ABA_Licencas">Licenças</a>
                </li>
				
				<li id="li1" ng-show="!searchMode && usuarioGS1 === true">
                    <a data-toogle="tab" href="#usuarios" ng-clock translate="11.ABA_Usuarios">Usuários</a>
                </li>
				
				<!--<li id="liTabSistema" ng-show="!searchMode">
                    <a data-toogle="tab" href="#sistema" ng-clock translate="11.ABA_Sistema">Sistema</a>
                </li>-->            
			</ul>
        </div>

		<div class="pull-right" style="margin-right: 480px; margin-top: -32px;">
			<a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" ng-click="onClean(item)" title="{{ '11.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="30" ng-show="searchMode" tooltip-ajuda="'11.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="31" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'11.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>

        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="info" class="tab-pane active">
                            <div class="row">
                               <div class="form-group col-md-8">
                                    <label for="nome" translate="11.LBL_RazaoSocial">Nome/Razão Social</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="nome" name="nome" ng-model="item.nome" 
                                        ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_RazaoSocial' | translate}}"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cpfcnpj" translate="11.LBL_CPFCNPJ">CPF/CNPJ</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="cpfcnpj" name="cpfcnpj" ng-model="item.cpfcnpj" 
                                        ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}" ui-br-cpfcnpj-mask 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_CPFCNPJ' | translate}}"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="cad" translate="11.LBL_CAD">CAD</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="cad" name="cad" ng-model="item.cad" 
                                        ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_CAD' | translate}}"/>
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="email" translate="11.GRID_Email">Email</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="Text1" name="email" ng-model="item.email" 
                                        ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.GRID_Email' | translate}}"/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="statusassociado" translate="11.LBL_Situacao">Situação Cadastral</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="statusassociado" name="statusassociado" ng-model="item.statusassociado" ng-show="!searchMode" 
                                        ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_Situacao' | translate}}"/>

                                    <select class="form-control input-sm" id="Select1" name="statusassociado" ng-model="item.statusassociado" ng-show="searchMode">
                                        <option value="{{statusassociado.codigo}}" ng-repeat="statusassociado in statusassociados">{{statusassociado.nome}}</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="situacaofinanceira" translate="11.LBL_AcessoCNP">Acesso ao CNP</label>
                                    <input type="text" class="form-control input-sm fieldDisabled" maxlength="50" id="situacaofinanceira" name="situacaofinanceira" ng-model="item.situacaofinanceira" ng-show="!searchMode" ng-disabled="true" 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_AcessoCNP' | translate}}"/>

                                    <select class="form-control input-sm" id="situacaofinanceira" name="situacaofinanceira" ng-model="item.situacaofinanceira" ng-show="searchMode">
                                        <option value="1">Sim</option>
                                        <option value="0">Não</option>
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label for="endereco" translate="11.LBL_Endereco">Endereço</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="endereco" name="endereco" ng-model="item.endereco" 
                                        ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_Endereco' | translate}}"/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="numero" translate="11.LBL_Numero">Número</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="numero" name="numero" ng-model="item.numero" ng-disabled="!searchMode"  ng-class="{'fieldDisabled': !searchMode}"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_Numero' | translate}}"/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="bairro" translate="11.LBL_Bairro">Bairro</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="bairro" name="bairro" ng-model="item.bairro" ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_Bairro' | translate}}"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="complemento" translate="11.LBL_Complemento">Complemento</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="complemento" name="complemento" ng-model="item.complemento" ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_Complemento' | translate}}"/>
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="uf" translate="11.LBL_Estado">Estado</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="uf" name="uf" ng-model="item.uf" ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_Estado' | translate}}"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="cidade" translate="11.LBL_Cidade">Cidade</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="cidade" name="cidade" ng-model="item.cidade" ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_Cidade' | translate}}"/>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="pais" translate="11.LBL_Pais">País</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="pais" name="pais" ng-model="item.pais" disabled="true"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_Pais' | translate}}" />
                                </div>
                            </div>
                            <div class="row" ng-show="!searchMode">
                                <div class="form-group col-md-4">
                                    <label for="dataatualizacaocrm" translate="11.LBL_UltimaSincronizacao">Última Sincronização</label>
                                    <input type="text" class="form-control input-sm" maxlength="50" id="dataatualizacaocrm" name="dataatualizacaocrm" ng-model="item.dataatualizacaocrm" ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_UltimaSincronizacao' | translate}}"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="tipocompartilhamento"><span translate="11.LBL_CompartilharProduto">Compartilhar Produtos</span>?</label>

                                    <select class="form-control input-sm" id="tipocompartilhamento" ng-model="item.tipocompartilhamentocodigo" name="tipocompartilhamento" required ng-options="tipoCompartilhamento.codigo as tipoCompartilhamento.nome for tipoCompartilhamento in tiposCompartilhamento"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_CompartilharProduto' | translate}}">
                                    </select>
                                </div>
                                <div class="form-group col-md-2" ng-show="usuarioLogado.id_tipousuario == 1">
                                    <label for="numero" translate="11.LBL_DataMigracao">Data de Migração</label>
                                    <input type="text" class="form-control input-sm" id="Text2" name="datamigracao" ng-model="item.datamigracao" ng-show="!searchMode && item.codigo != undefined && item.codigo != ''" 
                                        ng-disabled="!searchMode" ng-class="{'fieldDisabled': !searchMode}"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.11.LBL_DataMigracao' | translate}}"/>
                                </div>
                            </div>
                        </div>
                        <div id="representante" class="tab-pane" style="min-height: 300px;">
                            <div class="form-group">
                                <div class="table-responsive">
                                    <table ng-table="tableRepresentantesLegais" class="table table-bordered table-striped table-hover" >
                                        <thead>
                                            <tr>
                                                <th ng-click="tableRepresentantesLegais.sorting('nome', tableRepresentantesLegais.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                                  <span translate="11.GRID_RepresentanteLegal">Representante Legal</span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableRepresentantesLegais.isSortBy('nome', 'asc'),
                                                    'icon-sort-down': tableRepresentantesLegais.isSortBy('nome', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableRepresentantesLegais.sorting('email', tableRepresentantesLegais.isSortBy('email', 'asc') ? 'desc' : 'asc')">
                                                  <span translate="11.GRID_Email">E-mail</span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableRepresentantesLegais.isSortBy('email', 'asc'),
                                                    'icon-sort-down': tableRepresentantesLegais.isSortBy('email', 'desc')
                                                  }"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in $data">
                                                <td ng-style="{ 'width': '50%' }" sortable="'nome'">{{item.nome}}</td>
                                                <td ng-style="{ 'width': '50%' }" sortable="'email'">{{item.email}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="licencas" class="tab-pane" style="overflow-y: scroll; max-height: 450px;">
                            <div class="form-group">
                                <div class="table-responsive">
                                    <table ng-table="tableLicencas" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th ng-click="tableLicencas.sorting('nome', tableLicencas.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                                  <span translate="11.GRID_Licenca">Licença</span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableLicencas.isSortBy('nome', 'asc'),
                                                    'icon-sort-down': tableLicencas.isSortBy('nome', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableLicencas.sorting('numeroprefixo', tableLicencas.isSortBy('numeroprefixo', 'asc') ? 'desc' : 'asc')">
                                                  <span translate="11.GRID_PrefixoGS1">Prefixo GS1</span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableLicencas.isSortBy('numeroprefixo', 'asc'),
                                                    'icon-sort-down': tableLicencas.isSortBy('numeroprefixo', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableLicencas.sorting('status', tableLicencas.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                                  <span translate="11.GRID_Status">Status</span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableLicencas.isSortBy('status', 'asc'),
                                                    'icon-sort-down': tableLicencas.isSortBy('status', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableLicencas.sorting('dataaniversario', tableLicencas.isSortBy('dataaniversario', 'asc') ? 'desc' : 'asc')">
                                                  <span translate="11.GRID_Expiracao">Expiração</span>&nbsp;&nbsp;
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableLicencas.isSortBy('dataaniversario', 'asc'),
                                                    'icon-sort-down': tableLicencas.isSortBy('dataaniversario', 'desc')
                                                  }"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in $data">
                                                <td ng-style="{ 'width': '25%' }" sortable="'nome'">{{item.nome}}</td>
                                                <td ng-style="{ 'width': '25%' }" sortable="'numeroprefixo'" ng-show="usuarioGS1 === true">{{item.numeroprefixo}}</td>
                                                <td ng-style="{ 'width': '25%' }" sortable="'status'">{{item.status}}</td>
                                                <td ng-style="{ 'width': '25%' }" sortable="'dataaniversario'">{{item.dataaniversario | date:'dd/MM/yyyy'}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="usuarios" class="tab-pane" style="overflow-y: scroll; max-height: 450px;">
                            <div class="form-group">
                                <div class="table-responsive">
                                    <table ng-table="tableUsuarios" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <th ng-click="tableUsuarios.sorting('nome', tableUsuarios.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                                <span translate="11.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                                <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableUsuarios.isSortBy('nome', 'asc'),
                                                    'icon-sort-down': tableUsuarios.isSortBy('nome', 'desc')
                                                }"></i>
                                            </th>
                                            <th ng-click="tableUsuarios.sorting('email', tableUsuarios.isSortBy('email', 'asc') ? 'desc' : 'asc')">
                                                <span translate="11.GRID_Email">Email</span>&nbsp;&nbsp;
                                                <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableUsuarios.isSortBy('email', 'asc'),
                                                    'icon-sort-down': tableUsuarios.isSortBy('email', 'desc')
                                                }"></i>
                                            </th>  
                                            <th ng-click="tableUsuarios.sorting('status', tableUsuarios.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                                <span translate="11.GRID_Status">Status</span>&nbsp;&nbsp;
                                                <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableUsuarios.isSortBy('status', 'asc'),
                                                    'icon-sort-down': tableUsuarios.isSortBy('status', 'desc')
                                                }"></i>
                                            </th>
                                            <th ng-click="tableUsuarios.sorting('indicadorprincipal', tableUsuarios.isSortBy('indicadorprincipal', 'asc') ? 'desc' : 'asc')">
                                                <span translate="11.GRID_Principal">Principal</span>&nbsp;&nbsp;
                                                <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableUsuarios.isSortBy('indicadorprincipal', 'asc'),
                                                    'icon-sort-down': tableUsuarios.isSortBy('indicadorprincipal', 'desc')
                                                }"></i>
                                            </th>
                                        </thead>
                                        
                                        <tbody>
                                            <tr ng-repeat="item in $data">
                                                <td ng-style="{ 'width': '25%' }" sortable="'nome'">{{item.nome}}</td>
                                                <td ng-style="{ 'width': '25%' }" data-title="'Email'" sortable="'email'">{{item.email}}</td>
                                                <td ng-style="{ 'width': '25%' }" data-title="'Status'" sortable="'status'">{{item.status}}</td>
                                                <td ng-style="{ 'width': '25%' }" data-title="'Principal'" sortable="'principal'">{{item.indicadorprincipal}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sistema" class="tab-pane" style="max-height: 450px;">
                            <div class="form-group">
                                <h3 class="header smaller lighter purple ng-scope" translate="11.LBL_Configuracao">Configuração de Webservices</h3>
                            </div>
                            <div>
                                Inclusão de produtos e localizações físicas (GTIN e GLN)
                            </div>
                            <div class="row" style="margin-left: 15px; padding: 5px;">
                                <div class="col-md-2">Ativado</div>
                                <div class="col-md-10">
                                    <select ng-model="item.ativado">
                                        <option></option>
                                        <option value="1" ng-selected="item.ativado == 1">Sim</option>
                                        <option value="0" ng-selected="item.ativado == 0">Não</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-left: 15px; padding: 5px;">
                                <div class="col-md-2">Aprovação</div>
                                <div class="col-md-10">
                                    <select ng-model="item.aprovacao">
                                        <option></option>
                                        <option value="1" ng-selected="item.ativado == 1">Sim</option>
                                        <option value="0" ng-selected="item.ativado == 0">Não</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center" ng-show="editMode" style="margin-bottom: 0px; margin-top: 0px;">
                            <button class="btn btn-info btn-sm" ng-cloak ng-show="!isPesquisa" ng-click="onSave(item)">
                                <i class="icon-save bigger-110"></i><span translate="11.BTN_Salvar">Salvar</span>
                            </button>
                            <button class="btn btn-info btn-sm" name="find" ng-cloak ng-show="isPesquisa" ng-click="onSearch(item)">
                                <i class="icon-search bigger-110"></i><span translate="11.BTN_Pesquisar">Pesquisar</span>
                            </button>
                            <button id="cancel" name="cancel" ng-click="onCancel(item)" class="btn btn-danger btn-sm" type="button">
                                <i class="icon-remove bigger-110"></i><span translate="11.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" translate="11.LBL_DadosAssociado"></label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-success btn-xs" title="{{ '11.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="29" tooltip-ajuda="'11.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 680px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="11.LBL_CliqueRegistroEditar">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableDadosAssociado" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
                                <tr>
                                    <th ng-cltableDadosAssociadoick="tableDadosAssociado.sorting('nome', tableDadosAssociado.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                      <span translate="11.GRID_RazaoSocial">Nome/Razão Social</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableDadosAssociado.isSortBy('nome', 'asc'),
                                        'icon-sort-down': tableDadosAssociado.isSortBy('nome', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableDadosAssociado.sorting('cpfcnpj', tableDadosAssociado.isSortBy('cpfcnpj', 'asc') ? 'desc' : 'asc')">
                                      <span translate="11.GRID_CPFCNPJ">CPF/CNPJ</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableDadosAssociado.isSortBy('cpfcnpj', 'asc'),
                                        'icon-sort-down': tableDadosAssociado.isSortBy('cpfcnpj', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableDadosAssociado.sorting('cad', tableDadosAssociado.isSortBy('cad', 'asc') ? 'desc' : 'asc')">
                                      <span translate="11.GRID_CAD">CAD</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableDadosAssociado.isSortBy('cad', 'asc'),
                                        'icon-sort-down': tableDadosAssociado.isSortBy('cad', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableDadosAssociado.sorting('email', tableDadosAssociado.isSortBy('email', 'asc') ? 'desc' : 'asc')">
                                      <span translate="11.GRID_Email">Email</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableDadosAssociado.isSortBy('email', 'asc'),
                                        'icon-sort-down': tableDadosAssociado.isSortBy('email', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableDadosAssociado.sorting('statusassociado', tableDadosAssociado.isSortBy('statusassociado', 'asc') ? 'desc' : 'asc')">
                                      <span translate="11.GRID_Situacao">Situação</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableDadosAssociado.isSortBy('statusassociado', 'asc'),
                                        'icon-sort-down': tableDadosAssociado.isSortBy('statusassociado', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableDadosAssociado.sorting('situacaofinanceira', tableDadosAssociado.isSortBy('situacaofinanceira', 'asc') ? 'desc' : 'asc')">
                                      <span translate="11.GRID_AcessoCNP">Acesso ao CNP</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableDadosAssociado.isSortBy('situacaofinanceira', 'asc'),
                                        'icon-sort-down': tableDadosAssociado.isSortBy('situacaofinanceira', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableDadosAssociado.sorting('tipocompartilhamentonome', tableDadosAssociado.isSortBy('tipocompartilhamentonome', 'asc') ? 'desc' : 'asc')">
                                      <span translate="11.GRID_CompartilharProdutos">Compartilhar Produtos</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableDadosAssociado.isSortBy('tipocompartilhamentonome', 'asc'),
                                        'icon-sort-down': tableDadosAssociado.isSortBy('tipocompartilhamentonome', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableDadosAssociado.sorting('quantidadeprodutos', tableDadosAssociado.isSortBy('quantidadeprodutos', 'asc') ? 'desc' : 'asc')">
                                      <span translate="11.GRID_QuantidadeProdutos">Quantidade Produtos</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableDadosAssociado.isSortBy('quantidadeprodutos', 'asc'),
                                        'icon-sort-down': tableDadosAssociado.isSortBy('quantidadeprodutos', 'desc')
                                      }"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '35%' }" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'cpfcnpj'">{{item1.cpfcnpj}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'cad'">{{item1.cad}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'cad'">{{item1.email}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'statusassociado'">{{item1.statusassociado}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'situacaofinanceira'">{{item1.situacaofinanceira}}</td>
                                    <td ng-style="{ 'width': '20%' }" sortable="'tipocompartilhamentonome'">{{item1.tipocompartilhamentonome}}</td>
                                    <td ng-style="{ 'width': '20%' }" sortable="'quantidadeprodutos'">{{item1.quantidadeprodutos}}</td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="8" class="text-center" translate="11.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
