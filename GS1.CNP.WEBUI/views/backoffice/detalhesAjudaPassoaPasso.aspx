﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detalhesAjudaPassoaPasso.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.detalhesAjudaPassoaPasso" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <head>
        <title>CNP</title>
        <base href="/" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Bootstrap -->
        <link href="/Styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/Styles/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="/bower_components/angular-block-ui/dist/angular-block-ui.min.css" />

    </head>
    <body ng-app="CNP.Backoffice" ng-controller="detalhesAjudaPassoaPassoCtrl">
        <div class="panel panel-primary" ng-if="item.codigostatus = 1" ng-cloak>
            <div class="panel-heading"><h1 ng-bind="'Ajuda Passo a Passo - ' + ajudas.nome"></h1></div>
            <div class="panel-body">
                <div>
                    <h4>Visão Geral: </h4>
                    <p ng-bind-html="renderHtml(ajudas.ajuda)"></p>
                </div>
                <div ng-if="ajudas.video != undefined && ajudas.video != ''">
                    <br>
                    <h4>Vídeo Explicativo: </h4>
                    <iframe style="z-index:0!important;" id="iframe_youtube" width="523" height="388" ng-src="{{ajudas.video | trusted}}" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/bootstrap.min.js"></script>

    <script type="text/javascript" src="/bower_components/angular/angular.js"></script>
    <script type="text/javascript" src="/bower_components/angular-cookies/angular-cookies.js"></script>
    <script type="text/javascript" src="/bower_components/angular-resource/angular-resource.js"></script>
    <script type="text/javascript" src="/bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script type="text/javascript" src="/bower_components/angular-route/angular-route.js"></script>
    <script type="text/javascript" src="/bower_components/angular-animate/angular-animate.js"></script>
    <script type="text/javascript" src="/bower_components/ng-table/ng-table.js"></script>
    <script type="text/javascript" src="/bower_components/angular-bootstrap/ui-bootstrap.js"></script>
    <script type="text/javascript" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>

    <script type="text/javascript" src="/bower_components/angular-block-ui/dist/angular-block-ui.min.js"></script> 
    <script type="text/javascript" src="/bower_components/ngScrollTo/ng-scrollto.js"></script>

    <script src="/Scripts/lib/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/Scripts/lib/ng-ckeditor/ng-ckeditor-1.0.1.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="/bower_components/angular-x2js/dist/x2js.min.js"></script>
    <script type="text/javascript" src="/bower_components/x2js/xml2json.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-colorpicker/js/bootstrap-colorpicker-module.js"></script>
    <script type="text/javascript" src="/bower_components/angular-wysiwyg/dist/angular-wysiwyg.js"></script>

    <!-- Include the JS ReCaptcha API -->
    <script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>

    <script type="text/javascript" src="/scripts/lib/mask.js"></script>
    <script type="text/javascript" src="/assets/js/angular-input-masks/masks.js"></script>

    <!-- Angular - Google Maps API -->
    <script type="text/javascript" src="/bower_components/lodash/lodash.min.js" ></script>
    <script type="text/javascript" src="/bower_components/angular-simple-logger/dist/angular-simple-logger.min.js" ></script>
    <script type="text/javascript" src="/bower_components/angular-google-maps/dist/angular-google-maps.js" ></script>

    <script type="text/javascript" src="/bower_components/angular-recaptcha/release/angular-recaptcha.js"></script>
    <script type="text/javascript" src="/scripts/recaptcha.config.js"></script>  

    <script src="/bower_components/ngstorage/ngStorage.min.js" type="text/javascript"></script>
    <script src="/bower_components/angular-translate/angular-translate.min.js" type="text/javascript"></script>
    <script src="/bower_components/angular-translate-storage-local/angular-translate-storage-local.min.js" type="text/javascript"></script>
    <script src="/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js" type="text/javascript"></script>
    <script src="/bower_components/angular-translate-handler-log/angular-translate-handler-log.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="/Scripts/backoffice/app.js"></script>
    <script type="text/javascript" src="/Scripts/backoffice/core/app.directives.js"></script>
    <script type="text/javascript" src="/Scripts/backoffice/core/app.filters.js"></script>
    <script type="text/javascript" src="/Scripts/backoffice/core/app.factory.js"></script>
    <script type="text/javascript" src="/Scripts/backoffice/core/services/genericservice.js"></script>
    <script type="text/javascript" src="/Scripts/backoffice/detalhesAjudaPassoaPasso/detalhesAjudaPassoaPassoCtrl.js"></script>

</html>

