﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="estatisticas.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.estatisticas" %>

<style>
    .btn-gray {
        color: Gray;
    }
    
    .angular-google-map-container { height: 550px; }
</style>

<div ng-controller="EstatisticasCtrl" class="page-container" ng-init="searchMode = true;" style="height: 820px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-mobile" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" translate="64.LBL_GestaoChaves"></label>
        </div>
        <div class="pull-right action-buttons-header" style="margin-top: -32px; margin-right: 258px;">
            <ajuda codigo="95" tooltip-ajuda="'64.TOOLTIP_Ajuda'" margin-right="'10px'"></ajuda>
        </div>
        <div class="widget-toolbar my-tab" style="margin-top: -36px;">
            <ul id="tabs-estatisticas" class="nav nav-tabs">
                <li id="liTabChavesGeradas" class="active">
                    <a data-toogle="tab" href="#estatisticas" translate="64.ABA_EstatisticasGepirInbar">Estatísticas Gepir e Inbar</a>
                </li>
                <li id="liTabGerarChaves" ng-show="usuarioLogado.id_tipousuario == 1" ng-click="showMap()">
                    <a data-toogle="tab" href="#mapa" ng-clock translate="64.ABA_Mapa">Mapa</a>
                </li>
            </ul>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="tab-content">
                        <div id="estatisticas" class="tab-pane active">
                            <div class="table-responsive">
                                <h3 class="header smaller lighter purple" translate="64.LBL_EstatisticasGepir">
                                    Estatísticas GEPIR
                                </h3>
                                <table ng-table="tableEstatisticasGepir" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableEstatisticasGepir.sorting('limitediario', tableEstatisticasGepir.isSortBy('limitediario', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_LimiteDiario">Limite Diário</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasGepir.isSortBy('limitediario', 'asc'),
                                                'icon-sort-down': tableEstatisticasGepir.isSortBy('limitediario', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableEstatisticasGepir.sorting('limitesemanal', tableEstatisticasGepir.isSortBy('limitesemanal', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_LimiteSemanal">Limite Semanal</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasGepir.isSortBy('limitesemanal', 'asc'),
                                                'icon-sort-down': tableEstatisticasGepir.isSortBy('limitesemanal', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableEstatisticasGepir.sorting('limitemensal', tableEstatisticasGepir.isSortBy('limitemensal', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_LimiteMensal">Limite Mensal</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasGepir.isSortBy('limitemensal', 'asc'),
                                                'icon-sort-down': tableEstatisticasGepir.isSortBy('limitemensal', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableEstatisticasGepir.sorting('limitediariodisponivel', tableEstatisticasGepir.isSortBy('limitediariodisponivel', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_LimiteDiarioDisponivel">Limite Diário - Disponível</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasGepir.isSortBy('limitediariodisponivel', 'asc'),
                                                'icon-sort-down': tableEstatisticasGepir.isSortBy('limitediariodisponivel', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableEstatisticasGepir.sorting('limitesemanaldisponivel', tableEstatisticasGepir.isSortBy('limitesemanaldisponivel', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_LimiteSemanalDisponivel">Limite Semanal - Disponível</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasGepir.isSortBy('limitesemanaldisponivel', 'asc'),
                                                'icon-sort-down': tableEstatisticasGepir.isSortBy('limitesemanaldisponivel', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableEstatisticasGepir.sorting('limitemensaldisponivel', tableEstatisticasGepir.isSortBy('limitemensaldisponivel', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_LimiteMensalDisponivel">Limite Mensal - Disponível</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasGepir.isSortBy('limitemensaldisponivel', 'asc'),
                                                'icon-sort-down': tableEstatisticasGepir.isSortBy('limitemensaldisponivel', 'desc')
                                              }"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data" scroll-to="">
                                            <td ng-style="{ 'width': '16.6%' }" sortable="'limitediario'">{{item1.limitediario}}</td>
                                            <td ng-style="{ 'width': '16.6%' }" sortable="'limitesemanal'">{{item1.limitesemanal}}</td>
                                            <td ng-style="{ 'width': '16.6%' }" sortable="'limitemensal'">{{item1.limitemensal}}</td>
                                            <td ng-style="{ 'width': '16.6%' }" sortable="'limitediariodisponivel'">{{item1.limitediariodisponivel}}</td>
                                            <td ng-style="{ 'width': '16.6%' }" sortable="'limitesemanaldisponivel'">{{item1.limitesemanaldisponivel}}</td>
                                            <td ng-style="{ 'width': '16.6%' }" sortable="'limitemensaldisponivel'">{{item1.limitemensaldisponivel}}</td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="6" class="text-center" translate="64.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <h3 class="header smaller lighter purple" translate="64.LBL_EstatisticasInbar">
                                    Estatísticas Inbar
                                </h3>
                                <table ng-table="tableEstatisticasInbar" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableEstatisticasInbar.sorting('produtosconsultados', tableEstatisticasInbar.isSortBy('produtosconsultados', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_ProdutosConsultados">Produtos Consultados</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasInbar.isSortBy('produtosconsultados', 'asc'),
                                                'icon-sort-down': tableEstatisticasInbar.isSortBy('produtosconsultados', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableEstatisticasInbar.sorting('existentescnp', tableEstatisticasInbar.isSortBy('existentescnp', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_ExistentesCNP">Existentes CNP</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasInbar.isSortBy('existentescnp', 'asc'),
                                                'icon-sort-down': tableEstatisticasInbar.isSortBy('existentescnp', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableEstatisticasInbar.sorting('naoexistentescnp', tableEstatisticasInbar.isSortBy('naoexistentescnp', 'asc') ? 'desc' : 'asc')">
                                              <span translate="64.GRID_NaoExistentesCNP">Não Existentes CNP</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableEstatisticasInbar.isSortBy('naoexistentescnp', 'asc'),
                                                'icon-sort-down': tableEstatisticasInbar.isSortBy('naoexistentescnp', 'desc')
                                              }"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data" class="center">
                                            <td ng-style="{ 'width': '33.3%' }" sortable="'produtosconsultados'">{{item1.produtosconsultados}}</td>
                                            <td ng-style="{ 'width': '33.3%' }" sortable="'existentescnp'">{{item1.existentescnp}}</td>
                                            <td ng-style="{ 'width': '33.3%' }" sortable="'naoexistentescnp'">{{item1.naoexistentescnp}}</td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="3" class="text-center" translate="64.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <div id="mapa" class="tab-pane">
                            <div class="row">                                        
                                <div class="col-md-12 col-xs-12">                                               
                                    <div class="ng-binding" style="height: 550px !important; padding-left: 0px; margin-top: -15px;">                                                    
                                        <div style="width:100%; height: 550px;" ng-show="displayed">
                                            <ui-gmap-google-map center="map.center" zoom="map.zoom">                                                      
                                                <ui-gmap-markers idkey="map.idkey" models="map.markers" coords="'self'" icon="'icon'" events="map.markersEvents">
                                                    <%--<ui-gmap-windows show="show">
                                                        <div ng-non-bindable>{{title}}</div>
                                                    </ui-gmap-windows>--%>
                                                </ui-gmap-markers>                                        
                                            </ui-gmap-google-map>                                                        
                                        </div>
                                    </div>                                                                                                                                                                                     
                                </div>       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="64.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="64.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>
