﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gepirCodigoBarras.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.gepirCodigoBarras" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .wysiwyg-menu{
        text-align: center !important;
    }
    .colunaTitulo
    {
        background-color: #f9f9f9;
        margin-bottom:3px;
    }
    .colunaDados
    {
        margin-bottom:3px;
    }
    
</style>

<div ng-controller="GepirCodigoBarrasCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false;" style="height:1050px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container style="width: 100%;">
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label ng-bind="titulo"></label>
            </div>
            <div class="pull-right" style="margin-top: -34px;">
                <a class="btn btn-danger btn-xs" ng-show="searchMode" title="{{ '66.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="97"></ajuda>
            </div>
            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form name="form" novalidate class="form" role="form" >
                            <div class="tab-content" style="min-height:400px;">
                                <div id="telas" class="tab-pane active">
                                    <div class="row">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.gtin.$invalid && form.submitted}">
                                            <label for="gtin" translate="66.LBL_GTIN">GTIN </label>
                                            <input id="gtin" name="gtin" type="text" class="form-control input-sm" ng-model="itemForm.gtin" integer maxlength="14" required/>
                                        </div>
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.tipoconsulta.$invalid && form.submitted}">
                                            <label for="tipoconsulta" translate="66.LBL_TipoConsulta">Tipo de Consulta</label>
                                            <select class="form-control input-sm" id="tipoconsulta" name="tipoconsulta" ng-model="itemForm.tipoconsulta"
                                                ng-options="tipo.codigo as tipo.nome for tipo in tiposConsulta" ng-change="alteraTipoConsulta();" required>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4" style="margin-top: 29px;">
                                            <button name="consultar" class="btn btn-info btn-xs" type="button" ng-click="onSearch(itemForm)">
                                                <i class="icon-search bigger-110"></i> <span translate="66.BTN_Consultar">Consultar</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="table-responsive" ng-show="consultaefetuada && dadosItem == null && erroWS == ''" style="margin-top: 20px;">
                                        <table ng-table="tableCampos" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th ng-click="tableCampos.sorting('gtin', tableCampos.isSortBy('gtin', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_GTIN"> GTIN </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('gtin', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('gtin', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('codigoretorno', tableCampos.isSortBy('codigoretorno', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_CodigoRetorno"> Código de Retorno </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('codigoretorno', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('codigoretorno', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('nomefantasia', tableCampos.isSortBy('nomefantasia', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_NomeFantasia"> Nome Fantasia </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('nomefantasia', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('nomefantasia', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('additionalpartyid', tableCampos.isSortBy('additionalpartyid', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_AdditionalPartyID"> AdditionalPartyID </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('additionalpartyid', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('additionalpartyid', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('endereco', tableCampos.isSortBy('endereco', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_Endereco"> Endereço </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('endereco', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('endereco', 'desc')
                                                      }"></i>
                                                    </th>
                                                </tr>
                                            
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="campo in $data" style="cursor:pointer;">
                                                    <td ng-style="{ 'width': '20%' }" sortable="'gtin'">{{campo.gepirRequestedKey.requestedKeyValue}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'codigoretorno'">{{campo.returnCode.Value}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'nomefantasia'">{{campo.gS1KeyLicensee != null ? campo.gS1KeyLicensee.partyName[0] : ''}} </td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'additionalpartyid'">{{campo.gS1KeyLicensee != null ? campo.gS1KeyLicensee.additionalPartyIdentification[0].Value : ''}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'endereco'">{{campo.address != null ? campo.address.streetAddressOne : ''}}</td>
                                                </tr>                                               
                                                <tr ng-show="$data.length == 0  && consultaefetuada && erroWS == ''">
                                                    <td colspan="5" class="text-center"><span translate="66.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div style="padding-left: 15px;margin-top: 20px;" ng-if="itemForm.tipoconsulta == 2 && dadosItem != null && consultaefetuada && erroWS == ''">
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">CPF/CNPJ</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.CpfCnpj}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Razão Social</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.RazaoSocial}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">GTIN</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.Gtin}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Número do Prefixo</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.NrPrefixo}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Tipo do GTIN</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.TipoGtin}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Descrição do Tipo GTIN</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.DescTipoGtin}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Descrição</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.Descricao}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Descrição da Impressão</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.DescricaoImpressao}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Data de Inclusão</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.DtInclusao | date :  "dd/MM/y"}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Data de Reativação</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.DtReativacao | date :  "dd/MM/y"}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">País</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.Pais}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Lingua</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.Lingua}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Compartilha Dado</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.CompartilhaDado}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Status</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.Status}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Descrição do Status</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.DescStatus}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">GPC - Code Segment</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.CodeSegment}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">GPC - Code Family</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.CodeFamily}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">GPC - Code Class</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.CodeClass}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">GPC - Code Brick</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.CodeBrick}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Status do Prefixo</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.StatusPrefixo}}</div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-2 colunaTitulo">Contato</div>
                                            <div class="form-group col-md-6 colunaDados">{{dadosItem.Contato}}</div>
                                        </div>
                                    </div>
                                    <div style="margin-top: 20px;" ng-if="itemForm.tipoconsulta == 2 && (dadosItem == null || dadosItem.length == 0) && consultaefetuada && (prefixoGTIN == '789' || prefixoGTIN == '790') && erroWS == ''">                                    
                                         <table class="table table-bordered table-striped table-hover">
                                            <tbody>                                         
                                                <tr>
                                                    <td colspan="5" class="text-center"><span translate="66.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="margin-top: 20px;" ng-if="itemForm.tipoconsulta == 2 && (dadosItem == null || dadosItem.length == 0) && consultaefetuada && (prefixoGTIN == '789' || prefixoGTIN == '790') && erroWS != ''">                                    
                                         <table class="table table-bordered table-striped table-hover">
                                            <tbody>                                         
                                                <tr>
                                                    <td colspan="5" class="text-center">{{erroWS}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="17.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="17.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>