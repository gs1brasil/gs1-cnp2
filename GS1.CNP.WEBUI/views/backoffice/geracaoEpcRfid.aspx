﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="geracaoEpcRfid.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.geracaoEpcRfid" %>
<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/geracaoepcrfid";
    }

</script>

<style type="text/css">
    .input-group-btn>button{
        padding: 2px;
    }

    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak],
    .ng-cloak, .x-ng-cloak,
    .ng-hide {
        display: none !important;
    }

    .has-error-label
    {
        color: #f09784;
    }

</style>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />
<script src="../../Scripts/teste/ckeditor.js" type="text/javascript"></script>
<script src="../../Scripts/teste/js/sample.js" type="text/javascript"></script>

<div ng-controller="GeracaoEpcRfidCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = ''; gerar = false;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label translate="16.LBL_TitleGeracaoEPCRFID">{{ titulo }}</label>
            </div>

            <div class="pull-right" style="margin-top:-35px; margin-right:2px;">
                <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" ng-click="onClean(item)" title="{{ '16.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="47" ng-show="searchMode" tooltip-ajuda="'16.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="48" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'16.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="49" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'16.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            </div>

            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" id="form" novalidate class="form" role="form">
                        <div class="tab-content">
                            <div id="geracaoEpc" class="tab-pane active">
                                <div class="row text-center">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        <h4 translate="16.LBL_ConfiguracaoEPC">Configurações para geração de EPC</h4>
                                        <label translate="16.LBL_BusqueSelecioneEPC">Busque e selecione o item para gerar o seu respectivo EPC</label>
                                    </div>
                                </div>
                                <div>
                                    <h3 class="header smaller lighter blue">
                                        <span translate="16.LBL_ProdutoSelecionado">Produto <span class="hidden-xs">Selecionado</span></span>
                                        <span class="span5 pull-right" ng-if="initMode">
                                            <label class="pull-right inline">
                                                <div class="form-group">
                                                    <a class="btn btn-success btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto != undefined && item.codigoproduto != null"
                                                        ng-click="onProduto(item)" title="{{ '16.TOOLTIP_PesquisarProduto' | translate }}">
                                                        <i class="fa fa-pencil"></i> <span class="hidden-xs" translate="16.BTN_AlterarProduto">Alterar Produto</span>
                                                    </a>
                                                    <a class="btn btn-info btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto == undefined || item.codigoproduto == null"
                                                        ng-click="onProduto(item)" title="{{ '16.TOOLTIP_PesquisarProduto' | translate }}">
                                                        <i class="fa fa-search"></i> <span class="hidden-xs" translate="16.BTN_SelecionarProduto">Selecionar Produto</span>
                                                    </a>
                                                </div>
                                            </label>
                                        </span>
                                    </h3>
                                    <div ng-show="item.codigoproduto != undefined && item.codigoproduto != null">
                                        <label>Produto: {{item.productdescription}}</label><br />
                                        <label ng-show="item.globaltradeitemnumber != undefined && item.globaltradeitemnumber != null">GTIN: {{item.globaltradeitemnumber}}</label>
                                    </div>
                                    <div ng-show="item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == ''"
                                        ng-class="{'has-error-label': (item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == '') && form.submitted}">
                                        <label translate="16.LBL_NenhumProdutoSelecionado">Nenhum produto selecionado.</label>
                                    </div>
                                </div>
                                <hr/>

                                <div class="row" style="padding-top: 15px;">
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.codigotipohash.$invalid && form.submitted}">
                                        <label for="codigotipohash" data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_Tamanho' | translate}}" translate="16.LBL_Tamanho">Tamanho </label><br />
                                        <input type="radio" name="codigotipohash" class="ace" ng-model="item.codigotipohash" value="1" ng-click="onChangeSerial(item.codigotipohash)" 
                                            ng-disabled="!initMode" ng-class="{'fieldDisabled': !initMode}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_Tamanho' | translate}}">
                                        <span class="lbl"> 96 bits &nbsp; &nbsp;</span>
                                        <input type="radio" name="codigotipohash" class="ace" ng-model="item.codigotipohash" value="2" ng-click="onChangeSerial(item.codigotipohash)" 
                                            ng-disabled="!initMode" ng-class="{'fieldDisabled': !initMode}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_Tamanho' | translate}}">
                                        <span class="lbl"> 198 bits &nbsp; &nbsp;</span>
                                    </div>
                                    <!-- SGTIN-96-->
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.serialinicio.$invalid && form.submitted}" ng-cloak ng-show="item.codigotipohash != 2">
                                        <label for="serialinicio" data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_SerialInicio' | translate}}">
                                            <span translate="16.LBL_SerialInicio">Serial inicial</span> <span ng-show="!searchMode">(*)</span>
                                        </label>
                                        <input type="text" class="form-control input-xs" id="serialInicio" name="serialinicio" ng-model="item.serialinicio" required
                                            ng-readonly="(item.codigotipohash == undefined || item.codigotipohash == null || !initMode) && !isPesquisa" ng-blur="removeZeros('serialinicio', item.serialinicio)" integer
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_SerialInicio' | translate}}"/>
                                    </div>
                                    <!-- SGTIN-198-->
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.serialinicio.$invalid && form.submitted}" ng-cloak ng-show="item.codigotipohash == 2">
                                        <label for="serialinicio" data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_SerialInicio' | translate}}">
                                            <span translate="16.LBL_SerialInicio">Serial inicial</span> <span ng-show="!searchMode">(*)</span>
                                        </label>
                                        <input type="text" class="form-control input-xs" id="Text3" name="serialinicio" ng-model="item.serialinicio" required
                                            ng-readonly="(item.codigotipohash == undefined || item.codigotipohash == null || !initMode) && !isPesquisa" ng-blur="removeZeros('serialinicio', item.serialinicio)" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_SerialInicio' | translate}}"/>
                                    </div>
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.serialfim.$invalid && form.submitted}">
                                        <label for="serialfim" data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_SerialFinal' | translate}}">
                                            <span translate="16.LBL_SerialFinal">Serial final</span> <span ng-show="!searchMode">(*)</span>
                                        </label>
                                        <input type="text" class="form-control input-xs" id="serialFim" name="serialfim" ng-model="item.serialfim" ng-required="item.codigotipohash != 2"
                                            ng-readonly="(item.codigotipohash == undefined || item.codigotipohash == null || !initMode || item.codigotipohash == 2) && !isPesquisa" ng-blur="removeZeros('serialfim', item.serialfim)" integer
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_SerialFinal' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-3" ng-class="{'has-error': form.filtro.$invalid && form.submitted}">
                                        <label data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_Filtro' | translate}}">
                                            <span translate="16.LBL_Filtro">Filtro</span> <span ng-show="!searchMode">(*)</span>
                                        </label>
                                        <select class="form-control input-sm" id="filtro" name="filtro" ng-model="item.codigotipofiltro" 
                                            ng-disabled="!initMode" ng-class="{'fieldDisabled': !initMode}" required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_Filtro' | translate}}">
                                            <option ng-repeat="filtro in filtros" value="{{filtro.codigo}}" ng-selected="item.codigotipofiltro == filtro.codigo">{{filtro.codigo}} - {{filtro.nome}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-actions center" style="border-top: none; background-color: white;">
                                    <div ng-show="gerar">
                                        <ul class="list-group">
                                            <li class="list-group-item list-epc" style="cursor: pointer"><a ng-click="downloadFile('formato1')">Download Formato1 txt (Exemplo EPC: URN:EPC:ID:SGTIN:789835741.0001.00000001)</a></li>
                                            <li class="list-group-item list-epc" style="cursor: pointer"><a ng-click="downloadFile('formato2')">Download Formato2 txt (Exemplo EPC: URN:EPC:ID:SGTIN-198:1.789835741.0001.00000001)</a></li>
                                            <li class="list-group-item list-epc" style="cursor: pointer"><a ng-click="downloadFile('hexadecimal')">Download Hexadecimal txt (Exemplo EPC: 362EF13EFDD000583060C183060C400000000000000000000000)</a></li>
                                            <li class="list-group-item list-epc" style="cursor: pointer"><a ng-click="downloadFile('binario')">Download Binário txt (Exemplo EPC: 1101100010111011110001001111110111111011)</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-actions center">
                                    <button class="btn btn-info btn-sm" ng-click="gerarRFID(item)" ng-show="!gerar && !isPesquisa">
                                        <i class="icon-cogs bigger-110"></i><span translate="16.BTN_Gerar">Gerar</span>
                                    </button>
                                    <button class="btn btn-info btn-sm" name="find" type="button" ng-click="onSearch(item)" ng-show="isPesquisa">
                                        <i class="icon-search bigger-110"></i><span translate="16.BTN_Pesquisar">Pesquisar</span>
                                    </button>
                                    <button id="Button1" name="fechar" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="16.BTN_Fechar">Fechar</span>
                                    </button>
                                    <%--<button class="btn btn-info btn-sm">
                                        <i class="icon-ok bigger-110"></i><span translate="16.BTN_Concluir">Concluir</span>
                                    </button>--%>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label translate="16.LBL_TitleGeracaoEPCRFID">{{ titulo }}</label>
            </div>
            <div class="widget-box">
                <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                    <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '16.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
                    <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '16.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
                    <ajuda codigo="46" tooltip-ajuda="'16.TOOLTIP_Ajuda'"></ajuda>
                </div>

                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                        <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                    </a>
                    <div class="widget-main" style="min-height: 1310px;">
                        <div class="table-responsive">
                            <table ng-table="tableEpcRfid" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <thead>
                                    <tr>
                                        <th ng-click="tableEpcRfid.sorting('dataalteracao', tableEpcRfid.isSortBy('dataalteracao', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_DataCriacao">Data Criação</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEpcRfid.isSortBy('dataalteracao', 'asc'),
                                            'icon-sort-down': tableEpcRfid.isSortBy('dataalteracao', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableEpcRfid.sorting('globaltradeitemnumber', tableEpcRfid.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_GTIN">GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEpcRfid.isSortBy('globaltradeitemnumber', 'asc'),
                                            'icon-sort-down': tableEpcRfid.isSortBy('globaltradeitemnumber', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableEpcRfid.sorting('productdescription', tableEpcRfid.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_Descricao">Descrição</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEpcRfid.isSortBy('productdescription', 'asc'),
                                            'icon-sort-down': tableEpcRfid.isSortBy('productdescription', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableEpcRfid.sorting('nomehash', tableEpcRfid.isSortBy('nomehash', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_Tamanho">Tamanho</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEpcRfid.isSortBy('nomehash', 'asc'),
                                            'icon-sort-down': tableEpcRfid.isSortBy('nomehash', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableEpcRfid.sorting('serialinicio', tableEpcRfid.isSortBy('serialinicio', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_SerialInicio">Serial Início</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEpcRfid.isSortBy('serialinicio', 'asc'),
                                            'icon-sort-down': tableEpcRfid.isSortBy('serialinicio', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableEpcRfid.sorting('serialfim', tableEpcRfid.isSortBy('serialfim', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_SerialFim">Serial Fim</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEpcRfid.isSortBy('serialfim', 'asc'),
                                            'icon-sort-down': tableEpcRfid.isSortBy('serialfim', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableEpcRfid.sorting('codigotipofiltro', tableEpcRfid.isSortBy('codigotipofiltro', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_Filtro">Filtro</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEpcRfid.isSortBy('codigotipofiltro', 'asc'),
                                            'icon-sort-down': tableEpcRfid.isSortBy('codigotipofiltro', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableEpcRfid.sorting('usuario', tableEpcRfid.isSortBy('usuario', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_Usuario">Usuário</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableEpcRfid.isSortBy('usuario', 'asc'),
                                            'icon-sort-down': tableEpcRfid.isSortBy('usuario', 'desc')
                                          }"></i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)">
                                        <td ng-style="{ 'width': '10%' }" sortable="'dataalteracao'">{{item1.dataalteracao | date : 'dd/MM/yyyy HH:mm'}}</td>
                                        <td ng-style="{ 'width': '10%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                        <td ng-style="{ 'width': '20%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                        <td ng-style="{ 'width': '10%' }" sortable="'nomehash'">{{item1.nomehash}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'serialinicio'">{{item1.serialinicio}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'serialfim'">{{item1.serialfim}}</td>
                                        <td ng-style="{ 'width': '10%' }" sortable="'codigotipofiltro'">{{item1.codigotipofiltro}} - {{item1.nomefiltro}}</td>
                                        <td ng-style="{ 'width': '20%' }" sortable="'usuario'">{{item1.usuario}}</td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="8" class="text-center" translate="16.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="16.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="16.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>

    <div>
        <script type="text/ng-template" id="modalProduto">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content" on-enter="onFilter(itemForm)">
                <div class="modal-header">
                    <h3 class="modal-title"><span translate="16.BTN_SelecionarProduto">Selecionar Produto</span> <popup-ajuda codigo="50" tooltip-ajuda="'16.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form id="form" name="form" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue" translate="16.LBL_PreenchaUmAbaixo">Preencha ao menos um filtro abaixo:</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <span>Filtros</span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="pesquisaGTIN" translate="16.GRID_GTIN">GTIN</label>
                                <input type="text" id="pesquisaGTIN" class="form-control input-sm" ng-model="itemForm.gtin" maxlength="14" integer 
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.16.GRID_GTIN' | translate}}"/>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="pesquisaProduto" translate="16.LBL_DescricaoProduto">Descrição do Produto</label>
                                <input id="pesquisaProduto" class="form-control input-sm" type="text" ng-model="itemForm.descricao" maxlength="255" 
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_DescricaoProduto' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="tipogtin" translate="16.LBL_TipoGTIN">Tipo de GTIN</label>
                                <select class="form-control input-sm" id="tipogtin" name="tipogtin" ng-model="itemForm.codigotipogtin" ng-options="licenca.codigo as licenca.nome for licenca in licencas"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_TipoGTIN' | translate}}">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="statusgtin" translate="16.LBL_StatusGTIN">Status do GTIN</label>
                                <select class="form-control input-sm" id="statusgtin" name="statusgtin" ng-model="itemForm.codigostatusgtin"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_StatusGTIN' | translate}}"> <!--ng-options="status.codigo as status.nome for status in statusgtin"-->
                                    <option value=""> Selecione</option>
                                    <option ng-repeat="status in statusgtin" value="{{status.codigo}}" ng-if="status.codigo != 7">{{status.nome}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="center">
                            <button name="find" class="btn btn-info btn-sm" type="button" ng-click="onFilter(itemForm)">
                                <i class="icon-ok bigger-110"></i><span translate="16.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <div class="table-responsive" style="margin-bottom: 20px;">
                                <table ng-table="tableProdutos" class="table table-bordered table-striped table-hover">
                                    <thead>
	                                    <tr>
                                        <th ng-click="tableProdutos.sorting('productdescription', tableProdutos.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_Produto">Produto</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('productdescription', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('productdescription', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableProdutos.sorting('globaltradeitemnumber', tableProdutos.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_GTIN">GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('globaltradeitemnumber', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('globaltradeitemnumber', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableProdutos.sorting('nometipogtin', tableProdutos.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')">
                                          <span translate="16.GRID_TipoGTIN">Tipo de GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('nometipogtin', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('nometipogtin', 'desc')
                                          }"></i>
                                        </th>
		                                    <th><span translate="16.GRID_Selecionar">Selecionar</span></th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data">
                                            <td ng-style="{ 'width': '35%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                            <td ng-style="{ 'width': '5%' }" class="action-buttons center">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedProduto(item1)">
                                                    <i class="icon-ok bigger-110"></i>Selecionar
                                                </button>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="4" class="text-center" translate="16.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" translate="16.BTN_Cancelar">Cancelar</button>
                </div>
            </div>
        </script>
    </div>
</div>
