﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="geracaoEtiquetas.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.geracaoEtiquetas" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }
</script>

<style type="text/css">
    .input-group-btn>button{
        padding: 2px;
    }

    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak],
    .ng-cloak, .x-ng-cloak,
    .ng-hide {
        display: none !important;
    }

    .blocotexto
    {
        position: absolute;
        overflow: hidden;
        padding-left: 2px;
        padding-bottom: 3px;
        word-wrap: break-word;
    }

    .blocoimagem
    {
        position:relative;
    }

    .descricaoetiqueta
    {
        font-weight:bolder;
    }

    .textoetiqueta { }

    #etiqueta
    {
        font-family: Arial !important;
    }

    [data-toggle="buttons"] > .btn > input[type="radio"],
    [data-toggle="buttons"] > .btn > input[type="checkbox"] {
        clip: rect(1px 10px 1px 1px);
    }

    .alinhamento .btn .btn-link .active
    {
        color: #ffb752 !important;
    }

    .iconealinhamentoF
    {
        margin-left:3px;
    }
    
    <%--.linhaGS1128
    {
        margin-top: -1px;
        display: block;
        border: solid 1px;
        width:104mm;
        height: 11mm;
        text-align: center;
    }
    
    .div128
    {
        width: 104mm;
        height: 12mm;
        border: solid 1px;
        margin-top: -1px;
    }
    
    .celulaGS1128Left
    {
        margin-top: 0px;
        display: block;
        outline: solid 1px;
        width: 52mm;
        text-align: center;
        margin-left: 0mm;
        height: 12mm;
        float: left;
        margin-left: 1px;
        outline: none;
    }
    
    .celulaGS1128Right
    {
        margin-top: -12mm;
        display: block;
        outline: solid 1px;
        width: 52mm;
        text-align: center;
        margin-left: 0mm;
        height: 12mm;
        float: right;
    }--%>

    .linhaGS1128 { margin-top: -1px; display: block; border: solid 1px; width:101mm; height: 14mm; text-align: center; margin-bottom: -1px;}
    .div128 { width: 101mm; height: 16mm; border: solid 1px; margin-top: -1px; font-size: 12.5px !important; }
    .celulaGS1128Left { margin-top: 0px; display: block; outline: solid 1px; width: 51mm; text-align: center; margin-left: 0mm; height: 13mm; float: left; margin-left: 1px; outline: none; }
    .celulaGS1128Right { margin-top: -13mm; display: block; outline: solid 1px; width: 51mm; text-align: center; margin-left: 0mm; height: 15.5mm; float: right; }
    .linhaGS1128-bottom { height: 80mm !important; padding-top: 4mm; }
    .blocodescricao { height: 8mm; display: table-cell; height: 8mm; vertical-align: middle; }
    .blocosscc { height: 6mm; }
    .div128.div128ClassRow { height: 14.5mm !important; }
    .div128.div128ClassRow .celulaGS1128Right { height: 14mm !important; margin-top: -10mm; }
    .div128.div128ClassRow .celulaGS1128Left { height: 10mm !important; }
    .blocodatavencimento.celulaGS1128Left label { font-size: 14px; line-height: 18px; }
    #urletiqueta { position: relative; }
</style>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="GeracaoEtiquetasCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = ''; itemFormEspec = { statusregistro: 'novo' }; gs1_128 = false;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container id="formcontainer"> <%--id usado para corrigir bug do ie no layout, no momento de mudar do grid para o form--%>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label ng-bind="titulo" translate="17.LBL_GeracaoEtiqueta"></label>
            </div>

            <div class="row-fluid">
				<div class="span12">
					<div class="widget-box">
						<div class="widget-header widget-header-blue widget-header-flat">
							<h4 class="lighter" translate="17.LBL_Etiqueta">Etiqueta</h4>

							<div class="pull-right" style="margin-top:2px; margin-right:2px;">
                                <a class="btn btn-danger btn-xs" ng-show="searchMode" name="limpar" title="{{ '17.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
                                <ajuda codigo="52" ng-show="searchMode" tooltip-ajuda="'17.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                                <ajuda codigo="53" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'17.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                                <ajuda codigo="54" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'17.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                            </div>
						</div>

						<div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
							<div class="widget-main" ng-show="!isPesquisa && initMode">
								<div id="fuelux-wizard" class="row-fluid" data-target="#step-container">
									<ul class="wizard-steps">
                                        <li ng-repeat="step in steps track by $index"
                                            ng-click="(stepCalculation > ($index+1)) && selectStep(($index + 1))"
                                            ng-class="{active: stepCalculation == ($index + 1), complete: stepCalculation > ($index + 1)}">
                                            <span class="step produtos">{{($index + 1)}}</span>
                                            <span class="title">{{step}}</span>
                                        </li>
									</ul>
								</div>
								<hr />
								<div class="step-content row-fluid position-relative" id="step-container">
									<div class="step-pane" id="step1" ng-class="{'active': stepCalculation == 1}">
										<form name="form.base" id="form4" novalidate class="form" role="form">
                                            <div style="min-height:330px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                                <div class="row col-md-12 col-xs-12">
                                                    <div>
                                                            <h3 class="header smaller lighter blue">
                                                                <span translate="17.LBL_ProdutoSelecionado">Produto <span class="hidden-xs">Selecionado</span></span>
                                                                <span class="pull-right" ng-if="initMode">
                                                                    <label class="pull-right inline">
                                                                        <div class="form-group">
                                                                            <a class="btn btn-success btn-sm" style="border: medium none;" ng-cloak ng-if="item.codigoproduto != undefined && item.codigoproduto != null && item.codigoproduto != ''"
                                                                                ng-click="onProduto(item)" title="{{ '17.TOOLTIP_PesquisarProduto' | translate }}">
                                                                                <i class="fa fa-pencil"></i> <span class="hidden-xs" translate="17.BTN_AlterarProduto">Alterar Produto</span>
                                                                            </a>
                                                                            <a class="btn btn-info btn-sm" style="border: medium none;" ng-cloak ng-if="item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == ''"
                                                                                ng-click="onProduto(item)" title="{{ '17.TOOLTIP_PesquisarProduto' | translate }}">
                                                                                <i class="fa fa-search"></i> <span class="hidden-xs" translate="17.BTN_SelecionarProduto">Selecionar Produto</span>
                                                                            </a>
                                                                        </div>
                                                                    </label>
                                                                </span>
                                                            </h3>
                                                            <div ng-show="item.codigoproduto != undefined && item.codigoproduto != null">
                                                                 <div class="row">
                                                                    <div class="form-group col-md-12 col-xs-12">
                                                                        <label translate="17.LBL_Produto">Produto</label>
                                                                        <input type="text" class="form-control input-sm fieldDisabled" id="Text3" name="productdescription" ng-model="item.productdescription" ng-disabled="true"
                                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Produto' | translate}}"/>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col-md-3">
                                                                        <label ng-show="item.globaltradeitemnumber != undefined && item.globaltradeitemnumber != null" translate="17.LBL_GTIN">GTIN</label>
                                                                        <input type="text" class="form-control input-sm fieldDisabled" id="Text4" name="globaltradeitemnumber" ng-model="item.globaltradeitemnumber" ng-disabled="true"
                                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_GTIN' | translate}}"/>
                                                                    </div>
                                                                    <div class="form-group col-md-3" ng-show="item.alternateitemidentificationid != undefined && item.alternateitemidentificationid != null && item.alternateitemidentificationid != ''">
                                                                        <label translate="17.GRID_CodigoInterno">Código Interno</label>
                                                                        <input type="text" class="form-control input-sm fieldDisabled" id="Text5" name="alternateitemidentificationid" ng-model="item.alternateitemidentificationid" ng-disabled="true"
                                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_CodigoInterno' | translate}}"/>
                                                                    </div>
                                                                    <div class="form-group col-md-3">
                                                                        <label translate="17.LBL_TipoGTIN">Tipo GTIN</label>
                                                                        <input type="text" class="form-control input-sm fieldDisabled" id="Text6" name="nometipogtin" ng-model="item.nometipogtin" ng-disabled="true" 
                                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_TipoGTIN' | translate}}"/>
                                                                    </div>
                                                                    <div class="form-group col-md-3">
                                                                        <label translate="17.LBL_StatusGTIN">Status GTIN</label>
                                                                        <input type="text" class="form-control input-sm fieldDisabled" id="Text7" name="nomestatusgtin" ng-model="item.nomestatusgtin" ng-disabled="true" 
                                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_StatusGTIN' | translate}}"/>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div ng-show="item.codigoproduto == undefined && item.codigoproduto == null">
                                                                <label translate="17.LBL_NenhumProdutoSelecionado">Nenhum produto selecionado.</label>
                                                            </div>
                                                        </div>
                                                        <hr/>

                                                </div>
                                            </div>
                                            <div class="form-actions center" ng-show="editMode">
                                                <button class="btn btn-info btn-sm" ng-click="irParaAbaConfiguracao('base', 2)" type="button" ng-disabled="$scope.item.globaltradeitemnumber != undefined">
                                                    <i class="icon-forward bigger-110"></i><span translate="17.BTN_Avancar">Avançar</span>
                                                </button>
                                                <button id="Button1" name="cancel" ng-click="onCancel(itemForm)" class="btn btn-danger btn-sm" type="button">
                                                    <i class="icon-remove bigger-110"></i><span translate="17.BTN_Cancelar">Cancelar</span>
                                                </button>
                                            </div>
                                        </form>
									</div>


									<div class="step-pane" id="step2" ng-class="{'active': stepCalculation == 2}">

                                            <div style="min-height:330px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                                <div ng-show="item.codigoproduto != undefined && item.codigoproduto != null">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label translate="17.LBL_Produto">Produto</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="productdescription" name="productdescription" ng-model="item.productdescription" ng-disabled="true"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Produto' | translate}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <label ng-show="item.globaltradeitemnumber != undefined && item.globaltradeitemnumber != null" translate="17.LBL_GTIN">GTIN</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="globaltradeitemnumber" name="globaltradeitemnumber" ng-model="item.globaltradeitemnumber" ng-disabled="true"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_GTIN' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-3" ng-show="item.alternateitemidentificationid != undefined && item.alternateitemidentificationid != null && item.alternateitemidentificationid != ''">
                                                            <label translate="17.GRID_CodigoInterno">Código Interno</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="alternateitemidentificationid" name="alternateitemidentificationid" ng-model="item.alternateitemidentificationid" ng-disabled="true"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_CodigoInterno' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label translate="17.LBL_TipoGTIN">Tipo GTIN</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="nometipogtin" name="nometipogtin" ng-model="item.nometipogtin" ng-disabled="true"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_TipoGTIN' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label translate="17.LBL_StatusGTIN">Status GTIN</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="nomestatusgtin" name="nomestatusgtin" ng-model="item.nomestatusgtin" ng-disabled="true"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_StatusGTIN' | translate}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <label>
                                                                <span translate="17.GRID_TipoCodigoBarras">Tipo de Código de Barras</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <select class="form-control input-sm" ng-change="limpaVisualizacao();preparaVisualizacao();" id="tipoetiqueta" name="tipoetiqueta" ng-model="item.tipoetiqueta" ng-options="tipoetiqueta.codigo as tipoetiqueta.name for tipoetiqueta in tiposetiquetas | validaUPCE: item.codigotipogtin:item.globaltradeitemnumber" required 
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_TipoCodigoBarras' | translate}}">
                                                                <option value=""></option>
                                                            </select>
                                                        </div>  
                                                        <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 1'>
                                                            <label>
                                                                <span translate="17.GRID_ModeloCodigoBarras">Modelo</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <select class="form-control input-sm" id="modelo128" name="modelo128" ng-model="item.modelo128" required 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_ModeloCodigoBarras' | translate}}" ng-change="OnModelo128(item)" >
                                                                <option value=""></option>
                                                                <option value="1" ng-if="item.codigotipogtin == 4">Padrão</option>
                                                                <option value="2" ng-if="item.codigotipogtin == 3">Anatel</option>
                                                            </select>
                                                        </div> 
                                                        <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 1 && item.modelo128 == 2'>
                                                            <label>
                                                                <span translate="17.GRID_CodigoHomologacao">Código Homologação</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <select class="form-control input-sm" id="Select23" name="codigohomologacao" ng-model="item.codigohomologacao" ng-options="homologacaoanatel.codigo as homologacaoanatel.alternateitemidentificationid for homologacaoanatel in homologacoesanatel" required  
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_CodigoHomologacao' | translate}}">
                                                                <option value=""></option>
                                                            </select>
                                                        </div>                                                        
                                                        <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 9'>
                                                            <label>
                                                                <span translate="17.GRID_RegistroAnvisa">Registro ANVISA</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <select class="form-control input-sm" id="Select25" name="codigohomologacao" ng-model="item.codigohomologacao" ng-options="homologacaoanvisa.codigo as homologacaoanvisa.alternateitemidentificationid for homologacaoanvisa in homologacoesanvisa" required  
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_RegistroAnvisa' | translate}}">
                                                                <option value=""></option>
                                                            </select>
                                                        </div>   
                                                    </div>
                                                    <div class="row" ng-show='(item.tipoetiqueta == 1 && item.modelo128 == 1) || item.tipoetiqueta == 10 || item.tipoetiqueta == 12 || item.tipoetiqueta == 9'>
                                                         <div class="form-group col-md-3">                                                                                                                       
                                                            <label for="nrlote" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_NumeroLote' | translate}}">
                                                                <span translate="47.LBL_NumeroLote">Lote</span> <span ng-show="!searchMode">(*)</span>
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="nrlote" id="nrlote" ng-model="item.nrlote" maxlength="20"  required ng-disabled="true" ng-readonly="true" 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataVencimento' | translate}}"/>                                                                                                                              
                                                        </div>
                                                        <div class="form-group col-md-3">                                                                                                                       
                                                            <label for="datavencimento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataVencimento' | translate}}">
                                                                <span translate="47.LBL_DataVencimento">Data de Vencimento</span> <span ng-show="!searchMode">(*)</span>                                                                
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="datavencimento" id="datavencimento" ng-model="item.datavencimento" maxlength="10"  required ng-disabled="true" ng-readonly="true" 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataVencimento' | translate}}"/>                                                                                                                              
                                                        </div>


                                                      

                                                        <div class="form-group col-md-4">
                                                            <label>&nbsp;</label>
                                                            <br>
                                                           


                                                             <a class="btn btn-success btn-sm" style="border: medium none;" ng-cloak ng-if="item.nrlote != undefined && item.nrlote != null && item.nrlote != ''"
                                                                ng-click="onLote(item)" title="{{ '17.TOOLTIP_PesquisarLote' | translate }}">
                                                                <i class="fa fa-pencil"></i> <span class="hidden-xs" translate="17.BTN_Alterarlote">Alterar Lote</span>
                                                            </a>
                                                            <a class="btn btn-info btn-sm" style="border: medium none;" ng-cloak ng-if="item.nrlote == undefined || item.nrlote == null || item.nrlote == ''"
                                                                ng-click="onLote(item)" title="{{ '17.TOOLTIP_PesquisarLote' | translate }}">
                                                                <i class="fa fa-search"></i> <span class="hidden-xs" translate="17.BTN_SelecionarLote">Selecionar Lote</span>
                                                            </a>
                                                            <br>
                                                        </div>
                                                    </div>


                                                    <div class="row" ng-show='item.tipoetiqueta == 9'>
                                                         <div class="form-group col-md-3">                                                                                                                       
                                                            <label for="nrlote" data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_SerialInicio' | translate}}">
                                                                <span translate="16.LBL_SerialInicio">Serial Inicial</span> <span>(*)</span>
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldEnable" name="serialinicial" id="Text22" ng-model="item.serialinicial" maxlength="13"  required 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_SerialInicio' | translate}}" ng-blur="removeZeros('serialinicio', item.serialinicio)" integer/>                                                                                                                              
                                                        </div>
                                                        <div class="form-group col-md-3">                                                                                                                       
                                                            <label for="datavencimento" data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_SerialFinal' | translate}}">
                                                                <span translate="16.LBL_SerialFinal">Serial Final</span>                                                                
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldEnable" name="serialfim" id="Text23" ng-model="item.serialfim" maxlength="13"  required 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_SerialFinal' | translate}}" ng-blur="removeZeros('serialfim', item.serialfim)" integer/>                                                                                                                               
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>&nbsp;</label>
                                                            <br>
                                                            <button name="visualizar" class="btn btn-info btn-xs pull-left" type="button" style="" ng-click="visualizaCodigoBarras(item)" ng-disabled="item.tipoetiqueta == undefinded || item.tipoetiqueta == null || item.tipoetiqueta == ''">
                                                                <i class="icon-eye-open bigger-110"></i><span translate="17.BTN_Visualizar">Visualizar</span>
                                                            </button>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-12" ng-show="item.tipoetiqueta != undefinded && item.tipoetiqueta != null && item.tipoetiqueta != ''">
                                                            <div class="row">
                                                                <div class="form-group col-md-12 pull-left">
                                                                    <br>
                                                                    <span ng-show="true">
                                                                        <img data-ng-src="data:image/png;base64,{{urlcodigobarras}}" ng-if="urlcodigobarras != undefined && urlcodigobarras != null && urlcodigobarras != ''" style="float: left;"/>
                                                                        <span id="Span5" ng-if="urlcodigobarras != undefined && urlcodigobarras != null && urlcodigobarras != '' && item.tipoetiqueta == 9" style="margin-top: -3px; float:left;">
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: 0px;" class="descricaoetiqueta">(01) <span ng-if="item.globaltradeitemnumber.toString().length == 13">0</span>{{item.globaltradeitemnumber}}</label>
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -18px;" class="descricaoetiqueta">(713) {{item.registroanvisa[0].alternateitemidentificationid}}</label>
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -28px;" class="descricaoetiqueta serialinicial">(21) {{item.serialinicial}}</label>
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -43px;" class="descricaoetiqueta">(17) {{item.datavencimento}}</label>
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -58px;" class="descricaoetiqueta">(10) {{item.nrlote}}</label>
                                                                        </span>
                                                                    </span>
                                                                    <span ng-show="false">
                                                                        <img id="urletiqueta" data-ng-src="data:image/png;base64,{{urlcodigobarras}}" ng-if="urlcodigobarras != undefined && urlcodigobarras != null && urlcodigobarras != ''"/>
                                                                        <span id="Span4" class="blocotexto" ng-if="urlcodigobarras != undefined && urlcodigobarras != null && urlcodigobarras != '' && item.tipoetiqueta == 9" style="margin-top: -3px;">
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: 0px;" class="descricaoetiqueta">(01) <span ng-if="item.globaltradeitemnumber.toString().length == 13">0</span>{{item.globaltradeitemnumber}}</label>
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -18px;" class="descricaoetiqueta">(713) {{item.registroanvisa[0].alternateitemidentificationid}}</label>
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -28px;" class="descricaoetiqueta serialinicial">(21) {{item.serialinicial}}</label>
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -43px;" class="descricaoetiqueta">(17) {{item.datavencimento}}</label>
	                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -58px;" class="descricaoetiqueta">(10) {{item.nrlote}}</label>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions center">
                                                <button name="voltar" class="btn btn-info btn-sm" type="button" ng-click="voltarParaAba(1)">
                                                    <i class="icon-backward bigger-110"></i><span translate="17.BTN_Voltar">Voltar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" ng-click="configurarImpressao()" type="button">
                                                    <i class="icon-forward bigger-110"></i><span translate="17.BTN_Avancar">Avançar</span>
                                                </button>
                                                <button name="cancel" ng-click="onCancel(itemForm)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                                    <i class="icon-remove bigger-110"></i><span translate="17.BTN_Cancelar">Cancelar</span>
                                                </button>
                                            </div>

									</div>


                                    <div class="step-pane" id="step3" ng-class="{'active': stepCalculation == 3}">
                                        <div style="min-height:710px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                                <div ng-show="item.codigoproduto != undefined && item.codigoproduto != null">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label translate="17.LBL_Produto">Produto</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="Text8" name="productdescription" ng-model="item.productdescription" ng-disabled="true"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Produto' | translate}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <label ng-show="item.globaltradeitemnumber != undefined && item.globaltradeitemnumber != null" translate="17.LBL_GTIN">GTIN</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="Text9" name="globaltradeitemnumber" ng-model="item.globaltradeitemnumber" ng-disabled="true"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_GTIN' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-3" ng-show="item.alternateitemidentificationid != undefined && item.alternateitemidentificationid != null && item.alternateitemidentificationid != ''">
                                                            <label translate="17.GRID_CodigoInterno">Código Interno</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="Text10" name="alternateitemidentificationid" ng-model="item.alternateitemidentificationid" ng-disabled="true"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_CodigoInterno' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label translate="17.LBL_TipoGTIN">Tipo GTIN</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="Text11" name="nometipogtin" ng-model="item.nometipogtin" ng-disabled="true" 
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_TipoGTIN' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label translate="17.LBL_StatusGTIN">Status GTIN</label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" id="Text12" name="nomestatusgtin" ng-model="item.nomestatusgtin" ng-disabled="true" 
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_StatusGTIN' | translate}}"/>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row">   
                                                        <div class="form-group col-md-3">
                                                            <label><span translate="17.GRID_TipoCodigoBarras">Tipo de Código de Barras</span> </label>
                                                            <span class="row">
                                                                <select class="form-control input-sm fieldDisabled" ng-disabled="true" ng-change="preparaVisualizacao();" id="Select12" name="tipoetiqueta" ng-model="item.tipoetiqueta" ng-options="tipoetiqueta.codigo as tipoetiqueta.name for tipoetiqueta in tiposetiquetas | validaUPCE: item.codigotipogtin:item.globaltradeitemnumber" required 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_TipoCodigoBarras' | translate}}">
                                                                    <option value=""></option>
                                                                </select>
                                                            </span>
                                                        </div>
                                                        <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 1'>                                                            
                                                            <label>
                                                                <span translate="17.GRID_ModeloCodigoBarras">Modelo</span>
                                                            </label>
                                                            
                                                            <select class="form-control input-sm" id="Select21" name="modelo128" ng-model="item.modelo128" required ng-disabled="true" ng-readonly="true"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_ModeloCodigoBarras' | translate}}">
                                                                <option value=""></option>
                                                                <option value="1" ng-if="item.codigotipogtin == 4">Padrão</option>
                                                                <option value="2" ng-if="item.codigotipogtin == 3">Anatel</option>
                                                            </select>
                                                            
                                                        </div>                                                     
                                                        <div class="form-group col-md-3" ng-show='(item.tipoetiqueta == 1 && item.modelo128 == 1) || item.tipoetiqueta == 10 || item.tipoetiqueta == 12 || item.tipoetiqueta == 9'>
                                                            <label for="nrlote" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_NumeroLote' | translate}}">
                                                                <span translate="47.LBL_NumeroLote">Lote</span> <span ng-show="!searchMode">(*)</span>
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="nrlote" id="Text18" ng-model="item.nrlote" maxlength="20"  required ng-disabled="true" ng-readonly="true" 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataVencimento' | translate}}"/>                                                                                                                              
                                                        </div>
                                                        <div class="form-group col-md-3" ng-show='(item.tipoetiqueta == 1 && item.modelo128 == 1) || item.tipoetiqueta == 10 || item.tipoetiqueta == 12 || item.tipoetiqueta == 9'>
                                                            <label for="datavencimento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataVencimento' | translate}}">
                                                                <span translate="47.LBL_DataVencimento">Data de Vencimento</span> <span ng-show="!searchMode">(*)</span>                                                                
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="datavencimento" id="Text19" ng-model="item.datavencimento" maxlength="10"  required ng-disabled="true" ng-readonly="true" 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataVencimento' | translate}}"/>                                                                                                                              
                                                        </div>
                                                         <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 1 && item.modelo128 == 2'>
                                                             <label>
                                                                <span translate="17.GRID_CodigoHomologacao">Código Homologação</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <select class="form-control input-sm" id="Select24" name="codigohomologacao" ng-model="item.codigohomologacao" ng-options="homologacaoanatel.codigo as homologacaoanatel.alternateitemidentificationid for homologacaoanatel in homologacoesanatel" required ng-disabled="true" ng-readonly="true"  
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_CodigoHomologacao' | translate}}">
                                                                <option value=""></option>
                                                            </select>                                                                                                                             
                                                        </div> 
                                                        <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 9'>
                                                            <label>
                                                                <span translate="17.GRID_RegistroAnvisa">Registro ANVISA</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <select class="form-control input-sm" id="Select26" name="codigohomologacao" ng-model="item.codigohomologacao" ng-options="homologacaoanvisa.codigo as homologacaoanvisa.alternateitemidentificationid for homologacaoanvisa in homologacoesanvisa" required ng-disabled="true" ng-readonly="true"  
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_RegistroAnvisa' | translate}}">
                                                                <option value=""></option>
                                                            </select>
                                                        </div>                                                                                                                                                             
                                                    </div>
                                                  
                                                    <div class="row" ng-show='item.tipoetiqueta == 9'>
                                                         <div class="form-group col-md-3">                                                                                                                       
                                                            <label for="serialinicio" data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_SerialInicio' | translate}}">
                                                                <span translate="16.LBL_SerialInicio">Serial Inicial</span>
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="serialinicial" id="Text24" ng-model="item.serialinicial" maxlength="13"  required  ng-disabled="true" ng-readonly="true"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_SerialInicio' | translate}}" integer/>                                                                                                                              
                                                        </div>
                                                        <div class="form-group col-md-3">                                                                                                                       
                                                            <label for="serialfim" data-toggle="popover" data-trigger="hover" data-content="{{'t.16.LBL_SerialFinal' | translate}}">
                                                                <span translate="16.LBL_SerialFinal">Serial Final</span>                                                                
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="serialfim" id="Text25" ng-model="item.serialfim" maxlength="13"  required  ng-disabled="true" ng-readonly="true"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.16.LBL_SerialFinal' | translate}}" integer/>                                                                                                                               
                                                        </div>
                                                    </div>

                                                     <div class="row" ng-show="item.tipoetiqueta != undefined && item.tipoetiqueta != null && item.tipoetiqueta != '' && item.tipoetiqueta != 10 && item.tipoetiqueta != 18">
                                                        <div class="form-group col-md-4">
                                                            <label>
                                                                <span translate="17.LBL_Fabricante">Fabricante</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <span class="row">
                                                                <select ng-change="buscaEtiquetasFabricante(item)" class="form-control input-sm" id="codigofabricante" name="codigofabricante" ng-model="item.codigofabricante" ng-options="fabricante.codigo as fabricante.nome for fabricante in fabricantes" data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fabricante' | translate}}">
                                                                    <option value=""></option>
                                                                </select>
                                                            </span>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>
                                                                <span translate="17.LBL_Etiquetas">Etiquetas</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <span class="row">
                                                                <select ng-change="visualizaEtiqueta(item)" class="form-control input-sm" id="codigoetiqueta" name="codigoetiqueta" ng-model="item.codigoetiqueta" ng-options="etiqueta.codigo as etiqueta.nome + ' (' + etiqueta.largura + ' x ' + etiqueta.altura + ' - ' + etiqueta.quantidadelinhas+ ' linhas x ' + etiqueta.quantidadecolunas+ ' colunas)'  for etiqueta in etiquetas"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Etiquetas' | translate}}">
                                                                    <option value=""></option>
                                                                </select>
                                                            </span>
                                                        </div>
                                                        <div class="form-group col-md-4" ng-if="item.tipoetiqueta != 9">
                                                            <label>
                                                                <span translate="17.LBL_Quantidade">Quantidade</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <span class="row">
                                                                <select class="form-control input-sm" id="Select2" name="quantidadeetiqueta" ng-model="item.quantidadeetiqueta" ng-disabled="item.codigoetiqueta == null || item.codigoetiqueta == undefined"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Quantidade' | translate}}">
                                                                    <option ng-repeat="qtd in item.quantidadesetiquetas" ng-selected="{{qtd == item.quantidadeetiqueta}}" value="{{qtd}}">{{qtd}}</option>
                                                                </select>
                                                            </span>
                                                        </div>
                                                        <div class="form-group col-md-4" ng-if="item.tipoetiqueta == 9">
                                                            <label>
                                                                <span translate="17.LBL_Quantidade">Quantidade</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="quantidadeetiqueta" id="Text26" ng-model="item.quantidadeetiqueta" maxlength="13"  required  ng-disabled="true" ng-readonly="true"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Quantidade' | translate}}" />
                                                        </div>

                                                    </div>

                                                    <h3 class="header smaller lighter purple" ng-show="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta != 1  && item.tipoetiqueta != 9  && item.tipoetiqueta != 10  && item.tipoetiqueta != 12  && item.tipoetiqueta != 18 " translate="17.LBL_OpcoesFormato">
                                                        Opções de formato (Opcional)
                                                    </h3>
                                                    <div class="row" ng-show="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta != 1  && item.tipoetiqueta != 9  && item.tipoetiqueta != 10  && item.tipoetiqueta != 12  && item.tipoetiqueta != 18 ">
                                                        <div class="form-group col-md-2">
                                                            <label data-toggle="popover" data-trigger="hover" data-container="body" data-viewport="#step3" data-placement="right" data-content="{{'t.17.LBL_Tipo' | translate}}">
                                                                <span translate="17.LBL_Tipo">Tipo</span> <span ng-show='!searchMode'>(*)</span>
                                                            </label>
                                                            <select class="form-control input-sm" id="Select11" ng-model="item.codigotipogeracao" name="codigotipogeracao" required
                                                                ng-options="tipo.codigo as tipo.nome for tipo in tiposgeracao" ng-change="atualizaPosicao()"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Tipo' | translate}}">
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-2" ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">
                                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Posicao' | translate}}" translate="17.LBL_Posicao">Posição</label>
                                                            <select ng-change="atualizaPosicao()" class="form-control input-sm" id="Select1" ng-model="item.codigoposicao" name="codigoposicao" required ng-options="posicao.codigo as posicao.nome for posicao in posicoestexto" 
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Posicao' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                        </div>
                                                    </div>
                                                    <div class="row" ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">
                                                        <div class="form-group col-md-5">
                                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_DescricaoEtiqueta' | translate}}" translate="17.LBL_DescricaoEtiqueta">
                                                                Descrição da etiqueta
                                                            </label>
                                                            <br>
                                                            <textarea class="col-md-12" id="descricao" maxlength="255" name="descricao" ng-model="item.descricao" rows="2" style="resize: vertical;" ng-disabled="item.copiar"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_DescricaoEtiqueta' | translate}}"/>
                                                        </div>
                                                         <div class="form-group col-md-2">
                                                                    <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Fonte' | translate}}" translate="17.LBL_Fonte">Fonte</label><br>
                                                                    <select class="form-control input-sm" id="Select15" name="fontenomedescricao" ng-model="item.fontenomedescricao" ng-change="atualizaAlinhamento()"
                                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fonte' | translate}}">
                                                                        <option label="Arial" value="Arial" selected="selected">Arial</option>
                                                                        <option label="Arial Black" value="Arial Black">Arial Black</option>
                                                                        <option label="Comic Sans MS" value="Comic Sans MS">Comic Sans MS</option>
                                                                        <option label="Courier New" value="Courier New">Courier New</option>
                                                                        <option label="Georgia" value="Georgia">Georgia</option>
                                                                        <option label="Helvetica" value="Helvetica">Helvetica</option>
                                                                        <option label="Helvetica Neue" value="Helvetica Neue" >Helvetica Neue</option>
                                                                        <option label="Impact" value="Impact">Impact</option><option label="Lucida Console" value="Lucida Console">Lucida Console</option>
                                                                        <option label="Lucida Sans Unicode" value="Lucida Sans Unicode">Lucida Sans Unicode</option>
                                                                        <option label="Palatino Linotype" value="Palatino Linotype">Palatino Linotype</option>
                                                                        <option label="Tahoma" value="Tahoma">Tahoma</option><option label="Times New Roman" value="Times New Roman">Times New Roman</option>
                                                                        <option label="Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
                                                                        <option label="Verdana" value="Verdana">Verdana</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-1">
                                                                    <label>&nbsp;</label><br>
                                                                     <select class="form-control input-sm" id="Select16" name="fontetamanhodescricao" ng-model="item.fontetamanhodescricao" ng-change="atualizaAlinhamento()"
                                                                     data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fonte' | translate}}">
                                                                        <option label="8px" value="8" ng-selected="item.fontetamanhodescricao == 8">8px</option>
                                                                        <option label="9px" value="9" ng-selected="item.fontetamanhodescricao == 9">9px</option>
                                                                        <option label="10px" value="10" ng-selected="item.fontetamanhodescricao == 10">10px</option>
                                                                        <option label="11px" value="11" ng-selected="item.fontetamanhodescricao == 11">11px</option>
                                                                        <option label="12px" value="12" ng-selected="item.fontetamanhodescricao == 12">12px</option>
                                                                        <option label="13px" value="13" ng-selected="item.fontetamanhodescricao == 13">13px</option>
                                                                        <option label="14px" value="14" ng-selected="item.fontetamanhodescricao == 14">14px</option>
                                                                        <option label="15px" value="15" ng-selected="item.fontetamanhodescricao == 15">15px</option>
                                                                        <option label="16px" value="16" ng-selected="item.fontetamanhodescricao == 16">16px</option>
                                                                        <option label="18px" value="18" ng-selected="item.fontetamanhodescricao == 18">18px</option>
                                                                        <option label="24px" value="24" ng-selected="item.fontetamanhodescricao == 24">24px</option>
                                                                        <option label="32px" value="32" ng-selected="item.fontetamanhodescricao == 32">32px</option>
                                                                        <option label="48px" value="48" ng-selected="item.fontetamanhodescricao == 48">48px</option>
                                                                        <option label="60px" value="60" ng-selected="item.fontetamanhodescricao == 60">60px</option>
                                                                        <option label="72px" value="72" ng-selected="item.fontetamanhodescricao == 72">72px</option>
                                                                    </select>
                                                                </div>
                                                        <div class="form-group col-md-3">
                                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Alinhamento' | translate}}" translate="17.LBL_Alinhamento">Alinhamento</label>
                                                             <div class="input-group ">
                                                                <div class="btn-group">  <%--data-toggle="buttons"--%>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentodescricao" id="option1" ng-model="item.alinhamentodescricao" value="left" ng-change="atualizaAlinhamento()"><i class="icon-align-left bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentodescricao" id="option2" ng-model="item.alinhamentodescricao" value="center" ng-change="atualizaAlinhamento()"><i class="icon-align-center bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentodescricao" id="option3" ng-model="item.alinhamentodescricao" value="right" ng-change="atualizaAlinhamento()"><i class="icon-align-right bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentodescricao" id="option4" ng-model="item.alinhamentodescricao" value="justify" ng-change="atualizaAlinhamento()"><i class="icon-align-justify bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row" ng-if="item.codigotipogeracao == 3 || item.codigotipogeracao == 2">
                                                        <div class="form-group col-md-3">
                                                            <span style="font-size: 80%;">(<span translate="17.LBL_TamanhoMaximo">Tamanho máximo</span>: 255)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span translate="17.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ item.descricao.length || 0 }}</span>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <input type="checkbox" class="ace" name="copiar" ng-click="item.descricao=item.productdescription"  ng-init="item.descricao=item.productdescription; item.copiar = true" ng-model="item.copiar" ng-checked="true">
                                                            <label class="lbl" data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_CopiaDescricao' | translate}}" data-container="body" data-viewport="#step3">&nbsp; <span translate="17.LBL_CopiaDescricao">Copia descrição</span>?</label>
                                                        </div>
                                                    </div>
                                                    <div class="row" ng-if="item.codigotipogeracao == 3 || item.codigotipogeracao == 2">
                                                        <div class="form-group col-md-5">
                                                            <span ng-if="item.codigotipogeracao == 3">
                                                                <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_TextoLivre' | translate}}" translate="17.LBL_TextoLivre">Texto livre na etiqueta</label><br>
                                                                <textarea class="col-md-12" id="textolivre" maxlength="300" name="textolivre" ng-model="item.textolivre" rows="2" style="resize: vertical;"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_TextoLivre' | translate}}"/>
                                                            </span>
                                                        </div>
                                                        <div class="form-group col-md-2" ng-if="item.codigotipogeracao == 3">
                                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Fonte' | translate}}" translate="17.LBL_Fonte">Fonte</label><br>
                                                            <select class="form-control input-sm" id="Select17" name="fontenometextolivre" ng-model="item.fontenometextolivre" ng-change="atualizaAlinhamento()"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fonte' | translate}}">
                                                                <option label="Arial" value="Arial" selected="selected">Arial</option>
                                                                <option label="Arial Black" value="Arial Black">Arial Black</option>
                                                                <option label="Comic Sans MS" value="Comic Sans MS">Comic Sans MS</option>
                                                                <option label="Courier New" value="Courier New">Courier New</option>
                                                                <option label="Georgia" value="Georgia">Georgia</option>
                                                                <option label="Helvetica" value="Helvetica">Helvetica</option>
                                                                <option label="Helvetica Neue" value="Helvetica Neue" >Helvetica Neue</option>
                                                                <option label="Impact" value="Impact">Impact</option><option label="Lucida Console" value="Lucida Console">Lucida Console</option>
                                                                <option label="Lucida Sans Unicode" value="Lucida Sans Unicode">Lucida Sans Unicode</option>
                                                                <option label="Palatino Linotype" value="Palatino Linotype">Palatino Linotype</option>
                                                                <option label="Tahoma" value="Tahoma">Tahoma</option><option label="Times New Roman" value="Times New Roman">Times New Roman</option>
                                                                <option label="Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
                                                                <option label="Verdana" value="Verdana">Verdana</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-1" ng-if="item.codigotipogeracao == 3">
                                                            <label>&nbsp;</label><br>
                                                                <select class="form-control input-sm" id="Select18" name="fontetamanhotextolivre" ng-model="item.fontetamanhotextolivre" ng-change="atualizaAlinhamento()"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fonte' | translate}}">
                                                                <option label="8px" value="8" ng-selected="item.fontetamanhotextolivre == 8">8px</option>
                                                                <option label="9px" value="9" ng-selected="item.fontetamanhotextolivre == 9">9px</option>
                                                                <option label="10px" value="10" ng-selected="item.fontetamanhotextolivre == 10">10px</option>
                                                                <option label="11px" value="11" ng-selected="item.fontetamanhotextolivre == 11">11px</option>
                                                                <option label="12px" value="12" ng-selected="item.fontetamanhotextolivre == 12">12px</option>
                                                                <option label="13px" value="13" ng-selected="item.fontetamanhotextolivre == 13">13px</option>
                                                                <option label="14px" value="14" ng-selected="item.fontetamanhotextolivre == 14">14px</option>
                                                                <option label="15px" value="15" ng-selected="item.fontetamanhotextolivre == 15">15px</option>
                                                                <option label="16px" value="16" ng-selected="item.fontetamanhotextolivre == 16">16px</option>
                                                                <option label="18px" value="18" ng-selected="item.fontetamanhotextolivre == 18">18px</option>
                                                                <option label="24px" value="24" ng-selected="item.fontetamanhotextolivre == 24">24px</option>
                                                                <option label="32px" value="32" ng-selected="item.fontetamanhotextolivre == 32">32px</option>
                                                                <option label="48px" value="48" ng-selected="item.fontetamanhotextolivre == 48">48px</option>
                                                                <option label="60px" value="60" ng-selected="item.fontetamanhotextolivre == 60">60px</option>
                                                                <option label="72px" value="72" ng-selected="item.fontetamanhotextolivre == 72">72px</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-3" ng-if="item.codigotipogeracao == 3">
                                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Alinhamento' | translate}}" translate="17.LBL_Alinhamento">Alinhamento</label>
                                                             <div class="input-group ">
                                                                <div class="btn-group"> <%--data-toggle="buttons"--%>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentotextolivre" id="option5" ng-model="item.alinhamentotextolivre" value="left" ng-change="atualizaAlinhamento()"><i class="icon-align-left bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentotextolivre" id="option6" ng-model="item.alinhamentotextolivre" value="center" ng-change="atualizaAlinhamento()"><i class="icon-align-center bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentotextolivre" id="option7" ng-model="item.alinhamentotextolivre" value="right" ng-change="atualizaAlinhamento()"><i class="icon-align-right bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentotextolivre" id="option8" ng-model="item.alinhamentotextolivre" value="justify" ng-change="atualizaAlinhamento()"><i class="icon-align-justify bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" ng-if="item.codigotipogeracao == 3">
                                                        <div class="form-group col-md-10">
                                                            <span style="font-size: 80%;">(<span translate="17.LBL_TamanhoMaximo">Tamanho máximo</span>: 300)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span translate="17.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ item.textolivre.length || 0 }}</span>
                                                        </div>
                                                    </div>

                                                    <h3 class="header smaller lighter purple" translate="17.LBL_PreVisualizacaoEtiqueta">
                                                        Pré-Visualização da Etiqueta
                                                    </h3>


                                                    
                                                    <div class="row" ng-if="item.tipoetiqueta == 10 || item.tipoetiqueta == 18">                                                      
                                                            <div class="form-group col-md-12" ng-show="item.tipoetiqueta != undefinded && item.tipoetiqueta != null && item.tipoetiqueta != ''">

                                                                <div class="row">
                                                                    <div class="form-group col-md-12 pull-left">
                                                                        <br>
                                                                            <img data-ng-src="data:image/png;base64,{{urlcodigobarras}}" ng-show="urlcodigobarras != undefined && urlcodigobarras != null && urlcodigobarras != ''"/>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <div class="row" ng-if="item.tipoetiqueta != 10 && item.tipoetiqueta != 18">                                                    
                                                        <div class="form-group col-md-12">

                                                            <div ng-show="item.codigoetiqueta == undefined || item.codigoetiqueta == null || item.codigoetiqueta == ''">
                                                                <label translate="17.LBL_NenhumaEtiquetaSelecionada">Nenhuma etiqueta selecionada.</label>
                                                            </div>
                                                            <div id="etiqueta" class="formatoEtiqueta" ng-if="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta != 1 && item.tipoetiqueta != 9">
                                                                <div id="acima" ng-show="item.codigoposicao == 4">
                                                                    <div class="blocotexto">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;" class="descricaoetiqueta" ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">{{item.descricao}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:8px !important;" class="textoetiqueta" ng-if="item.codigotipogeracao == 3">{{item.textolivre}}</label>
                                                                    </div>
                                                                </div>
                                                                <div id="meio">
                                                                    <span id="esquerda" class="blocotexto" ng-show="item.codigoposicao == 2">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;" class="descricaoetiqueta"ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">{{item.descricao}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:8px !important;" class="textoetiqueta" ng-if="item.codigotipogeracao == 3">{{item.textolivre}}</label>
                                                                    </span>
                                                                    <span class="blocoimagem">
                                                                        <img id="urletiqueta" class="blocoimagem" data-ng-src="data:image/png;base64,{{urletiqueta}}" ng-show="urletiqueta != undefined && urletiqueta != null && urletiqueta != ''" style="max-width:100%"/>
                                                                    </span>
                                                                    <span id="direita" class="blocotexto" ng-show="item.codigoposicao == 1">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;" class="descricaoetiqueta"ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">{{item.descricao}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:8px !important;" class="textoetiqueta" ng-if="item.codigotipogeracao == 3">{{item.textolivre}}</label>
                                                                    </span>
                                                                </div>
                                                                <div id="abaixo" ng-show="item.codigoposicao == 3">
                                                                    <div class="blocotexto">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;" class="descricaoetiqueta" ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">{{item.descricao}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:8px !important;" class="textoetiqueta" ng-if="item.codigotipogeracao == 3">{{item.textolivre}}</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="etiqueta" class="formatoEtiqueta" ng-if="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta == 1 && item.modelo128 == 1 && item.tipoetiqueta != 9">
                                                                <div class="bloconomeempresa linhaGS1128">{{item.nomeassociado}}</div>
                                                                <div class="blocodescricao linhaGS1128">{{item.productdescription}}</div>
                                                                <div class="blocosscc linhaGS1128">
                                                                    <label>&nbsp;</label>
                                                                </div>
                                                                <div class="div128 div128ClassRow">
                                                                    <span class="blococonteudo celulaGS1128Left">
                                                                        <span><label>CONTENT/Conteúdo</label></span><br>
                                                                        <span><label>{{item.globaltradeitemnumber}}</label></span>
                                                                    </span>
                                                                    <span class="blocoquantidade celulaGS1128Right" style="float:right">
                                                                        <span><label>COUNT/Quantidade</label></span><br>
                                                                        <span><label>{{item.quantidadeporcaixa}}</label></span>
                                                                    </span>
                                                                </div>
                                                                <div class="div128" style="clear:both">
                                                                    <span class="blocodatavencimento celulaGS1128Left">
                                                                        <span><label>EXPIRY/Data de Durabilidade Máxima</label></span><br>
                                                                        <span><label>{{item.datavencimento}}</label></span>
                                                                    </span>
                                                                    <span class="blocolote celulaGS1128Right">
                                                                        <span><label>BATCH/Lote</label></span><br>
                                                                        <span><label>{{item.nrlote}}</label></span>
                                                                    </span>
                                                                </div>
                                                                 <div style="clear:both"></div>
                                                                <div class="linhaGS1128 linhaGS1128-bottom" style="margin-top: -1px;height: 80mm;"><img id="urletiqueta" class="blocoimagem" data-ng-src="data:image/png;base64,{{urletiqueta}}" ng-show="urletiqueta != undefined && urletiqueta != null && urletiqueta != ''" style="max-width:100%"/></div>
                                                                   
                                                            </div>
                                                            <div id="etiqueta" class="formatoEtiqueta" ng-if="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta == 1 && item.modelo128 == 2">
                                                                <div style="text-align:center"><img class="logoAnatel" style="width: 34.04mm;" data-ng-src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAcEAAAHACAYAAADa59NNAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAARpWSURBVHja7P1pkCxZdt+J/c657rFk5ttq7epaekM3uhsASaABEBhspAgC4DZjEkmQNI5kI8k0IzMZTfokfR8zLSZ9kI1oGtJIYkjTcDBjWAYAFwADEgAbIACCAHsBqrurq2vr2qtevS1fZkaEu9979OFe9/CIjHxrLpGZ95RF5cuMzZd7z/+s/yNmZmTJkiVLliznUDRfgixZsmTJkkEwS5YsWbJkySCYJUuWLFmyZBDMkiVLlixZMghmyZIlS5YsGQSzZMmSJUuWDIJZsmTJkiVLBsEsWbJkyZIlg2CWLFmyZMmSQTBLlixZsmTJIJglS5YsWbJkEMySJUuWLFkyCGbJkiVLliwZBLNkyZIlS5YMglmyZMmSJUsGwSxZsmTJkiWDYJYsWbJkyXJcUpyFkzCz7t8isjbHskpO+viyZMn7O0v/up6la2pm930+Z8ITzBsjS5azK3l/ZznKtVKcpZO/mxeWN2yWLHl/Z8lyop7gnRbywy7yDECnS8wsK7Zj3F/ZI8yS1/8agGCWLFmycZEly7oYSnpSm/RBETxv7rNnseV7mr3CvBaynJQUp3ETPEgF0HFtwhyyOd9K9qjv/7qDxGEc31Hu73W/Pg973utenX4c539uqkOzxZgly9ndJ3l/ZzkuI1Qsr7asFLMne2jXNV+/9V7z+f7kPXVmPMEsWbJkOSrvIMv5kSJfgqwQsmQ5D+s+B72yZE8wS5Ys2QDMkiWDYJYsWbJkyZJBMEuWLGdIcsgzy/1Kzgke8ybMIZksWY5+D+Z9liV7glmyZMnGaJYsd5Fj7xO8W89O7unJkuXkQCPvuSyneR2dKsaYux1o3oxZsmTJkuUwcWWVFOt4oOsMgDnnlyVLlixnR44dBE+7B5hBLhsR+fo93PE/6DHmgpcsR7FGcnVolixZTgTI71VZ5SKXLEfppOg6bo68GbJkyV5t3vNZjkOK07IhDmvo7t0shXV//1FbSmd9Htm6zxN82ONb9+uTgf/0h7PX/fhOZTj0NHmAWUlkyXI8SivvtSzHYYQeS5/gYfb+nZQlv5zHeNB+mJOeLH3Sns5JGyEnPbn7ft6/ao2t+/o56vVx1j35hz2+0x4JO4zru5ae4FkaY7J8HgeB43n1NE+7El0HJXi3NXbWPcMs63t9z+L9ObZw6L0C4Wm6yP1Qznkp3z7p+3PSBtVR59Tu9P7D2D+5xeBkjbizDkKnMees63SBTpv0zycrl+O97vl6Z8mS5VD0iZ0y/3YdcoLLx7BOCvms5wTvdr6nvbrupHOC2ZOTM33+p706/F7O/8RzguchLJi9kCzHsXbyOst787jP/7Rfv7XhDj3LQLjKUjpMSz0Xxsh9v/5+vvOkC2MelCUlA2KW7CkejRRHebEf5IKcRhC4n+q98w5yR2X9tcd1Vq/vupBFnLQncdpbRM5buuI0nP+pywmeB0vtTnnJdbfUTquleK+GTL6+6xUJOGvX96B1eJTr8376uLMnmCXLKbdEs2TJsn/PnqU+7jxFIi/ofBHO8PU9j4UL+fiO/vjvFQjPon5RsmTJkiXLWgPZcYDPWQC4Uz9KKUuWLFmyZMmeYJYsWbJkL/DQPbWzXgf5IOeXQTBLlixZzpHkhoBFyYUxWY51g+XCnSxZ1mOfnsW9eOb6BM/TCJksZ2Mt5DWbJcvpkhwOzZIlS5YsGQSzZMmSJUuW8yY5J3jOJOfssmTJkiV7glmyZMmSJUv2BM+bZE8vS5YsWTIIPrDkcOLZvn75/mbJcrr1SybQzpLlSCWs+NvqrIIBkn52IHrXz1vTDEX/ZFb93HdOel/X6kEOJUuWw5BTN0/wrPZh5f6y03BNQ3osq2HtKWi9IxTowjvDwSAYlo5ZQgcA7eva8zKbPyGSfmf+e/uFZun3VQht6Xm9C/KE3vt0//GFEEACggMJ6W2rrpUufG/3rK4wDkw7CyJYvDRqgBhi8bXSP2HTRavDwOQgI+QgTyIc8JzLG+sh9tpxDEXOnmCWLEdvOx7o2fRxpa/uW0DE2ueN/XtV5+9qwStYVOAJZAS37xikB3L7dEzvOQDvezCqPXiS+DnB+6RENFnJSycmvd9DeoGEBGaWQEy7FxtGsNC7Gk36/IiigiAyP4cQ5oaBqEUAlTkQiu33JfddRgmLhkm2K7NkEMyS5TBEuXs4Lywp5ajQHT1wMJ0DBzL36myutDvMUVnwACOggIgtHJd0QCYsvMG6kA+Gx7keOFjAJHTWuZnhCpfeFFaC/NwrC0tuYgSyuk4gqoKIQySCnCHpk5p0fSLKB7FovSMIhmhgMYBsWPK+LQgqOr++Hbq5Jae172GHnk+nKHqPoVTd75GeIo/sqDythzYfj/j7H+TzMwhmyXJfWuYubkgLcMvKtAs/6gKYtCC3rNdtH5a5bpPL0pcKYR4uDT56ZNKPfUYQEXz63SdPtA+o7Wf0Qcj3TtrHc5H2nGTJMHCAUpYu/V50QGcm8XkRzHsQid8pEZZI4Udb4U9bhEaQg8ArzK9rZ0dogq7QO/72GuSusCwZBLNkeXAMlDtjYAdwtgI8ZelN0irqEHOJMgfOsO/tEVDUUohyASQNM4/QpBBiYDF/mYCCGVABNdCkR937vcaYIFQYDUYNeAyfjtEQSmKWz6XHACiBITBIP4fAKP5uJSJlel+BaAFSpoMP3UW1lLfrLPmUnDQkPi8dVs6vnTS989N0jYp56DkBtNDw4AU5p8sjzDUFGQSzZDlhLcTKopN9QLhPfO8DAkqBLflGYr0cHR7wiERgiz9nCcyq+LAZhBoLNSE0BJsQmlsE2yP4hsZPCaHGhxnBaoJNQWqQBsEj4kH8AgDMw22KUGAUEQitBBvgdAOVIc5t4nRM4YY4N6RwQ9ARJhuIjkCG8cEQZIBQIhRJJc29SyGCmy6X2CaP1JKHGpB9QDe/VprOg1xamiWDYJYsD4dxYaVX0K8KFTnY6wi9IpC5h2HMizZTxaNZr2glJOXtQasEdjNgCuyC7QI7IDtMJ9ewsEPwezRhl+CnBKsIwYPtUrrd9L743SJQlnNwC0296FHY3BsSMQJVPI6uAlMQEUwicJkJIQhmDo9jlv4uIgQrsWID001Kd5FBcZlSH6UsHge9AlwEPwDdANlMHmYZi4EshkRR18tY6sJ1b/FR+wDYq5xFBDQ8oFeo7Hfvs5yJPZ1bJPJ5nVU52haJRdVrC8py8d/9b+9K/OelLr1/t+WdTfqeOnp00oYtZ8DNCHzNLnWzQ13dpGq28c1NvG2jMiXYHjBFrEK1SR5dQKVBbQeVuneR+oUykspGEpRYvxAo1bhKxbwQRua5yF4edN66Yfiu2icCWG0DPEOwMYQxZhcQu4BwCZEtLmx8GFdcohhcBr0EjFkMtZZYCq0GSvpXcgEEOwBMBkTKGS71qKxcMwetl9Mwg++kCmNOs/46UhDMiv3sbZLzzaiSQNBsvrZVO0Ucn226wpX5z9D9O7SVjF3VpqLdHxqwKcgU9Hb07rgOdp1mdoO6+YDJ9F1gJ3l3FSI1yIxCGpAKoQbxqKV+PWt9pvTTfO9cVvs7i+Cod7gW/XbGcJfXayrNGWC49D6NOVbT+DcGCCPMxsBFCnmEQfk44+FjyPAKcAXYih4jW8CIEDYxFJXkZRuIhHkRUABCOUdIARM71DXb3xNZ1528frvfe5BBMEuWe17QS+Ahc6UfLNVP9kDBEjRKD4SaEHCiaFeJ6eceHzvAHjTXqGfvMavep/bvY+EWwbYJ3GZYTjGmCAFLHqMSc3hIE8Gv55tKv5LH2qpNVgBhuCOEzd+vC++5Owj2fjfBGKR/NiCeIKErcDERvDlCGGB+A9hEwiVELuNkC5OLbG49jSsfRYsngEcSIF4AX9AEoyhLjBALhcSlPKNCAO/BlVnXZckgmCXLg4NgS7uyVGDRbqI6RNYTEZa4YzwheJxKAr1dYBu4jvfvU9dX8Xad2eR9QriJNbeBPUQmiNaoBNSaBG4BJ5baDAJmAaHBzKOySNSmNm9lCCiBcs64Ar12DlsARTnwEuhKj/Bg6eVKTaLnayTwi99pySBoq0MDBZgj2BDzAwJDLBQQhngbUJRXGA6eYDD4EEXxYSg+BPpYBMMUPvUhxCpUmYdL59HQB80LHr4XkiWDYJYspwsEQ5hzkfUoyoTF3j4LBjbDSQOu7bObAbfA38I316mbq9TNu1ThXZr6A8xuADuoTHCuxmnASYjh1JBCsN51+2mekwuIgeG759SWQMiUIEojrtfmEVK7he0HMwlLnxN9zjasKUQmG7GYD5S2xcEOBkG19rH4XaEFYgn4Nlwrmjy5AQFFgiNYCVZiYUAII4wtVB9jMPgQ5egpXPEEUn4IpA2bDjGGNGjHtBNLaYw8RS5LBsEsWR4UCPsKvNXfXdVnzNHF4pZJBD7bhWYPs6vsTb4GdpXa79CEW5jtYLKDY4a4irIIiDVYqDALbbA1xlsBlTI1n6/YzD1eS+ma8uevNYHG2Rx0OpALSx7h3BdcBDXFKAipZSGILVDCrfSvet8veFznzUoHzvOXBkICwcigIx1siSlYidMNLDgMTzDDm2I6RuQCJpcZjT7GYPA0OvgI8CRwGdjE2wgzoVDHfjbXLBkEMwhmyXLPa3r/em56nt4eMbe3A2yDv0Y9vc7e3gdU9bsU7m2QGwSrQGqcVog2OE0hQd90eyaSqmhiibG5pxnicZhoBEdx+0Bw5bwKsYUqzsXw52oQbIFCU1WrreyqCisUi+4HQ/E46jlcWmxuX4BZjZ+3qJZaQC9oGktUbIl7VaNX563A2yYWLoM8RlE8y3DwHIPhc1A8SSyq2SSGS90isPdo41T1Ae5/lvXen6cMBPM8tyzrLCG0uasY4tQF8NsGbuHDO0z2XqeavUXw7yPcjLk9ZgylAYv5O1JxSMcaY1GhqxaRjUVi352Zxe81T1EKIVU+ehPUNPUeyqLXl+KYHTxJSGwzuth4vg8M2eeh9QHR7hA6XdzIKwpkJLV+iIG59Bq3wEdq1uvD7GjdevRvhY8Ea77AgkPNdSWqhmLB4W2AtxQqLZ9lNH6WYvA0FM8Cz2F2aZ8uab3re62Oznoog2AGwVO8SB7m+p70/Vn17fc0mqjP4bmkuO0OIbH9ZNhhhdd3Ax+uEbjBzesvg11DuI5yA9Ft1O0h7EEIFDZATCMIpjCkSAQ6Q1FcPJ6Q6koteoKqBaIeb5HRJew7L8HUkCAdAEbvb04kraa4oCnEKdyp+Ts+K71cnyYv0noco/cii+HOeUWpghVdyHP+6h7V29J9CuIxreK1sxESShwFShviNLAacQUwovFjmnoT4zKD8nGkeJaNy98LPJa8wo30KOd9hHdZeNb7wzKJeZbTKYcGgvcLeKtefy+HcuRK9oh6fu50vmcd2A/jmsbwX59bZZX6aXkwQ2/kUCJytjRnTiTGEyV0hMse6UJkVYBC5z3VLQ+1EMCiF4dOiE3r7+Dr15hOX6OavUUTruJ0D5EpziKzitDzflKBixzghcV+t5QxM13iKRWWKziXr0KQkDzDcOCeckjP81nd4A6k49AOBNuRTiIPqS7aY7tjH2I48L3zc1M07G/ZwKwj0cZi7yFWYji8XUCLpxiMPsbG+NOgHwc+DOFSXCep3inQkpBrJNxOl90bONc63KFbOSTO0lXk6nfiWz+N+3/dnZQ8T/AwrYPscR7qtTzMgMPB9I/9MUBtBWCf/SQ5cYmB2iTMCajTbLuBztWwhYqBhgRkM5Bd4BbUHzCbvMFk9gZNeAN4F6e3GLhdVKrozXQTI7QD4SDKfDTQag/CFrym+wrUdkUu/XCnHaiFwz143KE31cJW+sb3f/Put8Vi8b16l/db/+8SUOrukIUdGrvN3t41qr332dh4l8HGt4M+DXaF0JSgJerKdP1jvtBMUF0e2NRKS86tkdZtcQrUqm6aLNkTXH9P8Ci+4zx7godxbbu1IUJIHW8dfVca49OfUr7va5Z7uWWu6CM2WtfQLt3YoRldgUu4Rt28Q12/Su3fo55do2luILJN4aYUWqVevpayy3VfZp0H1DLNhGNb38t7atnjO+jvy8dxWtbqnXRIoCT4LZrEWqN6iXLwNMPRpxgOvg3ko2CPA8O4jgRM/Jx4oO/ttZXANL2FFStYF9C4I/tuQ8hz7zV7gush2RPMHuCpuraSAFA6j28p13eHPdoNq12Ytq4IhhMfKcfCLJFUJ68vvEs1fZfZ7AOa8A6z+jW0uIXTitHYJ8+vAfMp1FksMauEVNnZzA8iy4mIEhDXMFAhyC4zf5OdvfeYVO9xcfM249Ft0E9CeARhDAzivWv9d2twUiy51cWiUSNhnl+0vgXm8w3InuDp8ASP2oI6z57gw1+/gLRg0hYztMDXB7b+PNmWL5K68wZa4hdHPxjZROCTW8B7EF6nmn2T2fQNZtVVsAnoBJEdVKeI+th0bfV8xh+KWk9Jik/0YHWqjFTMhrEg5Jgs9TuRQT+IJ/iw+/Nh1c3D7RWNrDXiMAl4ExqEwCbBHiGED3Fh8zsYDj6FKz9FbKsY45uYJFbtTxFZHI58kO/Zzwd36zZ7gmsVbVo7T3CdF0TuEVqXa6QrAXDBQN83rqidZxDDVU5a5pAU9rRbwAcwfZ1Z/Rqz2cv48C5wE+cmqKV2BmlQSZ5oaFLrQgzJqrp9nmfXbS4pF5in8ZygtOTnAUNwIhROaLjNzO/i/XV2d29TN1fZHG9TDD4JPIUrLszDnH2vz5Yrjnve4HKVa7tus6ydnLpw6EmPCslykkCoYMP9gNcDnXkfW3+QXPICkDTRoVVm0wh+/h1C8xqN/yZ7ey8R/HsQbuHcDFcEnLTT1aGpI22aOEGsRDWwqmAyTkeIPXCmbj7JICvCEzayY3mnUGDB8CFO3RgWMBg0+KZiVl+jqt9jPHqTjdF3oMXHQR6BZhRLh5c9QJnf816ch4VEdC9HnKtkMgieSqWeZR2AUFb8Gpacq9a7mysh6fgufQS+rsfvBjRvM6teZm/vRarqNcryNs7tUJQBlUDwcfKDiOBcQVEU0fvzDSYBtUSIaUrAo5L6/JAOCEObBxTbx5uZ5Zh9QUtDi4l9mi6N/QihIoQJZTkjNHvU9ZTp3i4SJmyMbyPlx6F4nJaTdKXRw2KxlSxXJ0sGwKM3cu7/Ap/aobpH6fWdd6Bfh6GiB+a2eqGnQAVdkUyckkDX35XUkTVpsoIk4Is5v3r6GpPZK1T1q5i9h5NtCpkiVCi+RyMWrfg4Aij08nsx1yM2zxGZgAQX6cys6IYoWZo0LxYSw0w4FevgQfbYaagujUfQ9O4f3f3z1hBwBBmBXaTxj1HIh7mw+WmKjW8DPgH2GHHIb5ojGQxxIY5wigO1gMhm04uGR3Gn13g9q+mgYt034WEifpbTb9EtTjJqa0R7hNBSRs8vNBhxsnpHZM01pjvfwPs3qJq3COFt1L2Pyi0KqaJK81EhimnHJBM9uqaX6wlLHkBvaJLEvFMHgL2+jXuZXZAZk45Yv+BjblZ8mrzRAygDJyVqRtAKZAfM45tddvdu4KoP2LpkieruEoQh2BDn4n2uwoyi4x4tu/IZyYGkDILr4PKuC4gfJ+PNWfx+b4ZKO6e9jBybnYWvXSpQUEQDcB14l1n9GlX1Os30ZcyuEvwuIlMKt4dTj5jHfEBahhEbpA9tgBrcTqT7snJe3Wks8WbGgovFvrDlC+Sy1jkxxREi76oEMIe2jDg9oFIpIjOMLxAdRKJuvUXwN6n9DZqbM7a2/hRF+RnQp5CmBK+IKyh1hFHtq5PpumIkBuWzZBA8swCY5fBA9F7DaS2/Zfx7gNCA+mjFcxWrX2Fv+g0m1av45h2G5Q2c7SZrvqHAIPjIvWmSWivm+URLFaFB2z6//pbR/SXytsh7OS/C6RbwXcrqsxz9Imt5S1Ou2NJ0DdPOSAleUQuoCqWbAZ4g19mdfRkf9ri8OcONPBQC9QWQAlXFJ5UqfRSkv6ayEZRB8IyD3Ekf/9n+fkG7AruUf7E23ChABW4Wwa96jen0Jar6FWr/FqI3GBZ7iJ8hNGn+X3IDzAEOp2lCg1RpJmAvzBpGCc5k3hDdmflLkxk6fsy+Apx7i9nIOznwc4x6flqvWrPjko3UdqgR8IhPnqIUiKvYGF9lMpty81bFhWbKYKOB8jkIF8APUFf2ughtRVRgkTA8SwbBQ/cishI53XLQ/euauRfuNYj4WKzCBNjG/DtMp68w2XuRpnkdlasUehsnNYUaPlRpKoPGfjETLMTfUYV2svkyzVVqd1BppyDcw7l04DyfxhCLZHKF6MlIbJZv/23LRgvgrU7rQzrHXkLKQFtAbcagqPHVS+zsNoxDxXhrAvoxxK4gYRxz1UI3Jksye2gGwQcJh92vksxtDOcEJAEz31MpNXCdEF6nDt9kb+erNP51sPdwxQ4D9TjzqAdrAkURx+5YpwiFOLnIMJuTIWvohbX6/txKEud0PEuFM9Ij0daOmTTkfvkTWzshtS2kddOb8hCSN+h9HauL2xmNJAMpefx+KgwHig1vMm2+zs5sG88NtjZmUHwarESs1xOaCnDmv2cvMIPgCQDnaQDxs/f9YbUlfq+0eoG5khJLpNaxCV6ogZbf8zpV9So7kxeYTl9E3NuUcoNiMKGUCH7BG2YFqhAsNr6HINHiFzcnuDbrjWDSxaq+Lr/Hir7E0HkTcViupYIL6XjcPCn0is6BtX3PGt2/sy7W8/SNpf6+lAMEI5iHENeriaXlIQwGY7yvoZgwKKdUzYzdSQVWsrXlQEsIj/bWUTsRxKXoaC+Uvo/VaHGvdAcm/ecziB66cXS/fYK5T++cK5Fu4dxpVYUDQTDOBWw5HNlPI9ZuepvTDquC0aBUib5sAlwD/xq3Jy8wmbwMvI26WzjZRWSGC8ynQvSJjO9Wr94juJYlj2+5HeJgBbakzHqfI30C7cRuY7L/M4yD5u3lUOrDRhJYacywVOm7bJSl3K+UGB4vVYwcqMRJ9eE5Qv1JHnnkx0A+DvoY6AC0TvQN8T4W7VxLtMdrC5G71ubzCRcqkElVytCfQnEadfY64scDe4IZALMsaBW7kxLRhXUjbRvBQqokbnIfBCcuvZY0M7xGmKFsx5l+zVvs7b7MdPYqQb5J6a7GCe62F2f/GYnIWtPUiJDyMyBBescUFk7Dln9K6Ibb9oG0/b2buN5+z4rPiV/V9LSA9sA19DC3PZ5s6R+HEbf6Sb2DvotOfZDQG9clQIPKbZB3EVeyd/v32diYwsZnwS5j5rBu8oTNUU9WHU9rPBaLU1Hkno7+rrp6nSJm64QfxWk/gSzHHTpgxYw0623UtvqtWOE+Lu1jieEpE08MbxIHmhKtZQkgrkGZEJle3saqrzKbvMJ0720af42ivE3hJpifYL6icAVzrkbrsf4nVheGvVFHSz1+6fgsVY1ayhNZe6zJww0SYS9IrAqNP6X7hiCgFgiafNmeZ6xtE36vQGNxyvpqz08eSg1mOSzFHRKQuHaMUhgQcBgV6PvsVr+PuR02iwYGn0XsaQglqnWKRAjo4giwIDFrTMcm1IC4xZFM91hQdafiwHXR2+uGH8VpP4Eshx+uuOM9XphM7lk1LHTfjL8+GC51EpsEQgoFxUo6F1sY0MjJyTbwAaH6JtXsG0x2v4jIBxRuQlFOcG6GWEUINWYheVnthG9J1Zhp4K4shxh7Hln7miUPzfqDcQ2CkoAw/j3I0u/tJ7ZFMd0Aw/nz1jX7t5ySbYvFnaAu54TWyhLsDBeHmhJ0hjGjGO2yV3uqbeXyxQIZbCJcgZD4glS64pjFGROLMyhXR1PCPe/hddXV63hMmUA7y/0D4b6N2m5gwfpLSpb+aT0FnwAngoiDjv8TCgKiNbAD4ZuE6TfY2fsK1exlnF6j0AnqDGMGvsGCxznQYkDwLbjG44ogW/ZGGvkUGr0DmCwMvu3/u50G0ZsRty//qXNPM7VV9IfsmlYEQq+vUFeovdAv4M+yNhKix9a7dzEs7hGbIeopBzWeiqpWdiaOC+qg+DTCIxDG8wjDSkgT7pTzi/GHbAStNQjmPr3zAIRhadM67i1Z38SXhxBDPV3PXNERYEcGl5q26jPMXme2+wKz6mWCvclwsB1Dn1SE0HTsLJIq+qxl/ug8OB+7tFr6DpmHR9U0hS1TOCpoAmQ9uHjGekrKQi9UFXreW/y7hgjoapHYW81FXlF0aZJEr4+s/712UKFMlhPdF9QxVN/uDUlk6xpD4L6eMCgEr+8zmf0RFpSLFwH3WeIsy+JAXy8C3HC/x98Vz9zbGjjNg8dP4tiLwzyBLGffEl4My2nfsVvtPKVQZ0spbS42HVtqXI4EwxFcRD2wDeFlppM/Zjb7Ok3zJmLXKUvPsIQqVBg1JoITjfPhSOz/vsEVcaSRifR8qnisQsBZ/Euc6BALW6IPG3+/JwfMNLVUtGNyisWiINP0eQHBxwgYgooR2gkUvS9Swor+MUnPFJ3rsCqIluUYFbGEZPyEVL1ZR8NFLBZcyQA8qCjmbqOuYdo0uB1jczSAwQC41FO7K/ZTWLEM7gMX7gQi6+yknCR+FKf9BLKclOj+ubWywvvrvWA+lSEVvlhbaGPJ+3ufMPs6e9M/ZjL7I0TeYljuosMKq43JrEaK1LclDm+aBqQqSIk418svWsrLhYWCHDVN43PaPkBdGHdjy0pvvyuw2lNMOaI+V2jbQN85om0YVNqZdisKc+5g8efa0XXySNqIQ4jDmqVETBmoMqumhGJKOQj4+gMmu1+hDBcZDEfAR4HLKGVrdiVjaXmNtY86rcnygZlH110/n/TxHQoI9hdMbvY92gVxVNfvnpPpqYvdUnLDlrwn0ej3zfue2r6ogoARJBa/FO22lwa4Cv5F6umL3Lr1BbR4j9JdR2UXrAbfIM4onBHEYxhGIrwO85FHIpp4Hg1RPwcMC7GAVRwWBJFioRx+4ZqrrPC0QvcaVcGsiYC54l65osBMIRiG31ee7iiwIHEwr/nF6y19ejjtgLhfuR/SDN/7vY8nVSzxsPMJj/p77+v4TBPBeujmVcZ2BsVSX59valwhlG5ACIYPM9RdY9Z8mfrmhM3Lfy6t/MuJWUa70H23X6RvhIVD1c/rKId5fA/i7ebCmCwPstT2h2zSzwhPqScPwUwRi2wZKoJ2pd41yHYEwPpVJnt/xN7uVxmUVxG5CTJDaRbtbmn9pGQmi+I05iTbKRI+hDkrh0Xw8tFW70KyoZsWIakJeu59BVsG/GUbQObWuy5Xcwamszp6fxab/EUiM41ZzFn64HE4VBTVIgK3GU0I8cuddm5pkBR6k9DzAjVPoTjRpV+wL4zZFkBJiMQOIa00czgLBLYxXqX2DY1/gsIJUKJcjilyAVGXPlNWbq97mUWZ5QQ9wezpnafr127JNo8l81giTTeRvYUKMTefLJSmFmk5A3kHeIlq8jyz6Uv46l1UrlEQRxx1+kBiH17MIQbUkhXeNRM3EWCS5ew0TYcg0lzNK+oiUHrxKZIbP9ySdxhwnVUfPd15sU+bd4ymvkfE8IlmTWlAfCyYQCjGRaTmSvRcbcTYzFADdRInPlmcSN6qNwuuu0a2zwOwlIciq8ITFe0mjlhLjm3a89o8oh4LARqHiFKI4p3HuIoPNbd3ttjcGjBwl4AxGoa9wRKpp3QhP9zbY1mORH9mTzDL/SuChdaA1rOzJcewLZrxsRo0KQplB3gPwgvsTb/MZPLHmH+XgZswHEBorMvjWfvZpkTuRYfgU06v9b38vOcwCI1PLhiCEAfgmhSIlBgF07pJs98KhBKhwKREGCAMYyhTyhTGLSIAWj/cn/oRxTAagtUEq+PfqQgWCDbFQvybaHytOlAJKBVok9KNikoE4/YSzQHQuspVkBWk3VlObv3DYobW5gDWvUZjz6sYhXgCFXCbyeQlnFxkMH4cyjHoh1LUov9+z7zq2i0EW7KcAk8wy1m3hPsz0ow4xWFuscpCn2Cazdd6d2wDbxKqb3B792vM6ldQfZei3EXM03iLnqNEIDRCDKciGC4BVMyf2CLiEnCIK1EtCQywUODDgBAcwRcYjmCbjMZPI7KFuhLnhqgMcTpCZQQyRAdbySN0CbxTdaakFoYw7QG+TwN5a7AKqJhMrhFsF+9v420Hb7fxTKibKcIu2DVUDSegRZzJKviYH1wY45SCn0FTI3763da7MvSkcoDHd3zp3qTwp8kigbpYARbi54rHUuhUxFGIUMhNqunL7NmjbGwNwDmwR7o9ZDTMSdZ1qfAsl0ZlEMyyHuFQcz2rNfRCNgkwUigTqRPZ9Q5wC+wd9ra/jK9fpWreQuQGTm/jtI4hQkg5w3kodF7VSSxAECGYw8xS+4OLzfCMMBtT1SWqWwyKK5SDiwy2LjIoN6EYAxeAy0ACuq5va9D73cUvl3J+Xn3uRm0WvQCxFPpsQCrGgwbYS49bBH+Tqr5N1dwmhNuE8A4hbNP4XayaUMsMkQmFGCohFfQkb7gd62Qyz0WtINvOclwS5kDUywPu46GVtrQzDn4OPsR2HjWGRU1VX2VWfZ1ytkm5sZEMu4tp/S15nF2l6HI7RZYMgllOyJROm9K0x7yiYGUvAdbu1QDcBnuLvekbNNUrVLefp9CrDIodpJghUscZbhSoG+B7+l2TkokpvgYTxYcY1jQZACNUt3B6CeVRTC5z5ZFnEa4gegFkExj1AK0kNiMrfZabLiljmoYA9k83LJkA5ZIi6lfzGfhZBEppgBp1NSNXM2IGTAjNNqG5QV1fp6rfI/j3CHYNk+sE2cbZhJZHVbrrWQDFilxhluO1/0Ia4KwQhmBtm0PoAFKkiVXLPlUpWiSDiBF+oRDB3JTG3mBnVrJZbDEYjIENzIaL+cAFp9WTm2QyCGY5TBDbR3smSyEf7sPqlLnSbjsMZAZ8ALxO07zEZPICk92XeGRjF8Itgs1i35wKhAJvRgh1KkKJlrYXRcIgWd1DPGMau4zIJqpjyvISg8EjDAaPIe5x4JFkUV8ENhPgua6asq3QbCs2ly8LsujbhoUBuPHkdIl3VFI1XzcWShNIWgpxaiJETopMiwYtJhSjPcZcx/z7zKavM6vexDfvEoitISpThCZ+i7Q9lak3LfUcdqlK6wG6ydxr6Ai/FfFulWbNct+hUOagtwBKc28wtr4oIvPqX7GW1ShQaEMIN6lnrzFzVxgMHgWupBz0sLMjpbcm433NAHgkto3lTvfzhYEtQxdNsi51ASjiqmh623BxrllTeYpBi3QeT0ApUhEKWB2QwQR4D+xldrf/gJ3JVxHeYzicMLQarCGEkJhdAqaWcivgjVSYsgFhg+C3kHAJ0YuYPclg8zMUxRMMh2NENpN313p4g/RwvdAmdwV0s/nwI5G299EW5vxJN+lNH1BpxosYcz5tn1lqFeEWcA24ys7OizTN21SzNxBuUuiUgWsQm6ViG0NdrIINZqnf0KEyRGWEDwJ4gttN4egGo0D8KIZWO48iy533yYrc4RIr0OJ6CnNvsXt+ee0FQgg4V2Ki1PUY+DDj8Xcw3PhBcJ8CfwHcFr4zLy2Fx9t8vBxJlcw6k25nEMxy/5v1vkDQsdB7Ju10iFbKfawwTVNheIqi7PZj8B6VGegMwttMJ1+jmr2AhZcx3sa5Wwxcw2xSoQjiCkQj7ZnXOP29ocRsSLANJFxEeITCPcHG+CnKjQ8BTxHsCUwu4yi7MKZ1BTmarsfyBPv79WyPpsDDktnQHkfiukFoMPYQtoGbwDs0s28ymbxK07yN+Q9AblPIlEEBwVf4MEEUiqJAGNBUjqaBoiyjEaMTgtbpex2EcaKIqzMIntD97/aXtT2pY8y2UP0EG+PvRze+E3gGs0uxEEwSpZ4ZElyPRSaDYAbBvEEPvqF3BUE72GuRsGJ/uQXy5kDscWsH3sbeP4/qLuhN4FVme1/l9u5XsOZNnLtJ6SaIVWA16kqQQOR9UcwEb2MIW/hwGeUJyuJpxuPnKAdPQfFoCm0Oknc3Tj+1Y40BQaUPgsJBl+FeJ1sflRK0ZIgYTWS26WyLtkciAFPgOvg38fU3mNavMKtfJ/j3cDJBdReRCSKxAklDZKkxM5x6THujNGyQeiCjJ5I9wZMFQVWlaWJ4XIsxTTOirh9lNPoTbF38Hij+FGaPEExRHXZLQmmpgvRIz/c8gmBx1hfv/YLESX/+w37/XcNz0irIsqeWQy/U45aU9rwiLQC1h8L1UhUB1E1ArgKvsX3j9/HhFYS3cOU2hTTJ+2giAKijtoLgC0IYgF3AuUcpiw8j+iSbG5+IvVP6CMjlGOYMRZo+ESd7oyQOGDcv0ukXqtyL9XdCm10Wkj2WKOaST2jgK8WxBW4MxRauuMLm+GmK6avMZq8z23uTwt0AdxPYQazBE8OjpROCrztuVMNF5pqekZNlfUTxCDVmt6maN9nbu8LGxWcQhjjdAALBIhWgSZ+qTU+t/swgmOXkXf82mS8Lvgkts6XZ4giXxeG5RWxraqM6VkcA5D2a2ZfZ2f0yjX8BlfcpZIZQIUEBQwuPFiW3JgGTizgep9APU7qPMio/ihs+C8UTxKKWIQSdT9VThxatJ9P2J6bJE+k11vbYy8qTvusmPjZLWNqIVmQeaUtvWro2N6DLSQqbwNPAYwxHn2Q4eJ/KfYNZ/QrT2at43sEV1ymLKYhPlHHaeX2CR2QWv9QGaZZdBsKTlMaiIag0BKtAAmWpBH2XyWTIcPgcbjBKkQ96xExz2r8sGQTPnaWyfH4P5w0uT68OHQTumwi/MEVeE5engjWoeER3gLeZ7n6F3b0vEJqXKAfXEbkNwVIlZfT4mnqENVsUxeO44ilGxYcZDp4D/QjwFPAIhM2FNKXIvOOiIXp8Ra8uwHpMLkIi736AS3OcGQHzqZFa20IbSXlaS7TkoTshQwl+HJv75XHQxxhsXWFQP0lRPMFe9Q0a/xJ1/T5Bdykk4JL3J10vp0+OZ9V5zDn/cXK+nwUQjfQPZrF/0JU1dbhJ5R17u1/jglyAwWWwDQyJrELEQih3xBWiORy6BpKnUNzfQr0/Bb68gVqvKiSvZL/npF0rQGRqceyB7AETCG8x2/sSO7tfAl5jOLiNisesiPk6SowRJhcRexz4MJc3vh2KJ9HiInG22iYwjhPbpYZBO0XBOl79toE9gqJgYY7Sdy2Wk3Xa+KnPTNqq25abtK0X9TShRjVed0URp1jQRNSzCfI0lBcYl89Szp5hd3KJaf0CyhuY7hCCT/fMISHlACXymwbxtPnULCe7f0XAgo9r2ARHg3O3mNUvMKguM3TPgFzA6TARQigm5bx1IsuB+JGnSJxjQLw3PTznO7REBRbnrUefq/957dSC+V9mwHXgBvi3uL39VWazryL6BmVxHZEaC0MIYyxs0tgFgl2iKJ9ia/NjuNHHwZ5Nub4igpul3jZtwS90zqh0323tkJmYC9QV52ypYKdzC+9uZJ2EQRWnBfQPu+92K4WWHfTHa+C69rDgQRgjWoJsUgwHXCoKBtMtprMBVfU6pZuC+TQwGII0afSS5ZzgCUtIw5Vjy6DEnlgf22ZUHGU5YVa9Q928yrB+C4ZXUjhkgIl2bTpZTtgTPCpP7bhyMg97/A9efXm0ivf+5qIl9yORUrcTIKAmznx3KMWcpUs0MWXsAFdh9jy3bn+Fpnkdpx+gupNm9pXUtcPsAspHGY4+xXjj0+jwyeSBbIJcATbos2wgLcXUvNWhx5jW81rv0Kc3x9HVHlh3XfSB7t/hrE/d54XN+w8Xn5eFkHQKa7qYFZVu614C91nGm1co9DK3d79CU7+EDnaAGU2YIUHRYgxSYH6WyMXPtidwGPvvaPZpTPCZha6qub8endWMBrvMZq+C/ypboyfAF4gbJv7c1X7gYRh2pyXCdidP70HTGufOEzzqm30qFlM7jm9BFTugRmgQPMEbTsr02kn0/niL2fYXqWcv0jQvI+4Gzs1iUUpziRC2GJRPMhg+x2D4GRh8C/BhYJNAQ6BAGSXqsZRf7FWf2hLILQ5w1x4o3IOyOfBvJwwCK/Otc55QYQn9bfG1kmoE4/1qCQKGlOMxj5SPsLu3RVW9RN28Q+E8pTOaUMX3qsv5wBMUNQjaJFIlXTEZJKTWl2sE+yZh+g10dAVsM9mtxUqV/fB1AudbvxfH8SVZ1gwA+/+2PjS0iriIfJtNgGIK8gHYi0z2vsLu7ldRuYort0EamiAQLlLqRxiW38Jg/BkonobyieTxDakpCGwlBe5ThWfBfohrp030kaJY4iPlPsJ66xf+s323o6287U3jkB6LTyIObyt6Y160SQ9DQoHIRZAxFBfZ3NxE5SJ74Qs0vAncRtwMDRq9jjOuLNdaPy1wvxaErrY5zCP45nE6obFX2N7Z4vLoU2BbqMTdIyuiCRkIH05yTvDcSeiB4eLwTkl9SZEEv4FiBvI+VC+zN/sCu7MXKcobmO0RrETYonSXKAZPMRp+Bim/FfTjEC5DHUcA+cIjqdx77sEsWsft8UhHyt07xhYA7rsiYBUArkkosDuPJj3Cfng02X+8BsECorHqNnKhOhwKNowf7JTxlkNdYGdPqJpvMihAioCFhlwUc8LeYNvo0N3f/hSKuGZVary9T9O8hk1fQYabqGxFw+gOJNrZQcmFMVnuCRjC4u9S9PRyEfNqfgoyBb0F9grbu/+B6exrFIMbeGloqhLxjzEaPsfmxifR0UeJYc/L4C/HrZ5SjkoMwTmg8cTJ7/2Na30d3yoG6wFhO227OAC8DwK99VT2snC8nsXpAG3VqGMh/CshNUEa+JB6/sp4r1wLn7HCV7gM8kmG4xLzG0ymI3x4DfgAET/nocyRnpMDQltkldX++g2G04C5CV5ucGP7eR55/FHg2UhwIdnby55glkO1S/teRtSCNbjbwLv4yYvs7L1AXb+O2C18ZQR3mcHwKTZGn2Q0/DjIR4HHgE2w4YID04Z42grIgUov7MP+Kk6jx5QvLLh/crecnt7B+1ujETSyfGyuB3o6/7csAXxSfuraitoY2Qwt2xo+etlW4KQAGTDaGlO6gtu7jrppKIttQheOznIit9/SsJEeF1OQZAJZpMETNdTA3B63py9xsX6WYvAIyIXeUOssh2XEZRA8l6C3OLG6WzbSEAmc36L2X+LGzhepZm+wMVSGepGmvsJg/GnK8ccp3NPR8+MK3o8iRZeDID7lLiKGmU+8Lq5FuSZqAukpfJsrfeuOU1f6ee5eQX1dvULrH1M743DpJbJ83vOMbTdjMDnMwcCk6QwNlQLzjoIRyBA3Ui6Ggt2p0PhXcHIzFTqd3XDYYSvJwz1AmVf8pr7X1AqPqUEIkbXJHCFUFOVVdqcvssXHcYOnunPMHnv2BNd2k5zMJlzOKemBrzMMsZ6H1fcA5RbwNjs7X+D29MuoXmM02iA0Iwr3cTYf+TQMPg08AjYk2DjN9ovWbOi+I45ocqJIkQpbFujXbIV3pCvPatm/O1PNwrbfQ9wPgEvXJsyrSUUjvblfmHMokdquAQlbUD6LbhlDPLNtwL0KLjbtq4X9zmk3CqiIHoplr/Hw7reu9Azn6993+iEE8NWM8UbNdPoWQ16NICgFWJlmFrZdtLpiT9z/5JSzot9PPCf4oP1Ux2XZnPYWiU4nScuTGZXi4tcaq4anGoEghknAWdENhIUG7DrwBrdv/jZVeBXCTTxjBsXH2Lz0KQbDj4M+SWR5GYIUHZ3Tou/mOn9N+greNOX1dLVnSqp87C3iNkvWa+9/4Ibhe+2jPPL71698XfFVcgfVJStcYe3aW6L44EnomIyOIfAMOhoylkvs7HiKUOF0G3EzXDsWBEXEMGpMlGAlRkGQOHpJjRW9m2cjHHasRnKfjckWLTsJkbTCgsfMGAwG+Cog9S188ZW09zYwP0aLmDf3Zpg4HA7fRHL7uFAWJ9Fb0D4nxaFcn6PYK3f7/Dt954MeTw6HnlK5+/3WlVZhiB1KiLSxyhrkJsxeYrrzMnt7b2KupnSPMxo/y9boM+C+BXgC/Aa4YqVlKXezOmURKu/pHJc/+wy4gfezUeUBXuck3VtCrCSlQNhCixFD8Xj/Bk29R+MrkBqnFvNU1hYkac9FaQkM+tWLubr0cDzCJdIEaw3BmBfEYtWwEyNIhdlbMHsJhp9F9UoKq0rHrxtWGEj7kgiHtH+Ow2E5znBvBsFTp0XvBIAHgUw7VUApiWX2hAbcFLiKhVe4NfkyOzvvIgwZD5/j0uYncKOniVMdLgJj0EG+/qdMQgg4FZQCdVe4uPVt7O7usDe9iQ8TgsZqRPGKhbaktwaZRa87jDuFagdOTc/ywJu5Z9/FvS2JSzZgVKiUaDGjabbZ2XmPreE1kCtYIkoQChTBW/LybJU+0Psyqh4UnA4rV3nc+c4MgqfaA1yumFyaArFv5cdSe7UanAeuU1cvcv32C9T1NYrRBhfHn2Q0/ChaPAtcABTvC0SGkT0t34L1Bz0XLX8VsG4EjyRD5qOMR7dpwlXqakpd30IcqJQpXWuIeVQnBDS+B828o8dz9wgW75/gMV9h4hBnhMaYVtcYz97GDR9DeAILknpGY2h1f6hTuVsp2aE4tofYpH8SBT8ZBE/pZlnt7XGXpI3FHsCiBm7S1K9wbftrTKY32dp4hkcufwKnH4/gF2KZPYxQV8YqxLTRMhCus5HkFtZDy0xqGBaGqD6Jlg0XNrbZ9hV1/QLOJggFJgGljqAZFJUUBm0HSLb1/UeoEE+66vHIv9/uzl0rYomwosJQVEKkW7Nb7E1e5cLwKeASYqM4a1pXUcLKfB3k0RMZBM8LAB44VLarzKwiANp1qr3XubHzJuiAxx/9NFvjjwNPAleSx2BYKuHPPsDpkVYZWqoAFmlD4YLoKM1r/BBafobhcBvvb+F5JxUhxZFLsTyjSO0YholPH5o16RHfPcyaeO/aXhipExGQJ+guk+oNNpt30eIZkItpdmS8NSFEMorVRvF8ZNpRGQ5HTdt2VK0hGQTPwMaJC4SFn3F1ttMhIienMAVuU++9zc3dd1AZcXHrkwzHzwGXIZRxioTO6zINw4cYTNNzkAo6O/Ms2wTRYjVuLG25CHyc8XCP4K9R1w0+XMVphUkV+zxDmYwgEijqPXkyWe52W0IPnNp9Jp3HJihmQrA4a1C7PeyAXUzeZzJ7nU39BMgjaepKqpmW0LtnsK8+oC26eYjipjsB0VHujaME2AyCZ0Jxs4qZuecB1mkjVUx23mVvbxvnLnD58odx7qPgtzA/REpFpSEQYphFQEUo23l2Fu46iijLyYJs+/Eic1UYQvu9FmcxWgnyCBSfYDB8h6q6ThNuptygdQpaTLCWpiuD3yEBYFi0UTogjP9WLbDQ238aQdGHBpMJhd6irt+C8gMYPBVVuBUglqqClzzBO0aQHm4NH5dBeNQe5qGD4FmfJ7gunt/iMe8HwBAajAoncUJBbVP29j5gunOLCxceY2PzWeAihGEsgCjAOrYXwWnfOgWkwYml66dHtj6Oog/oft67jn1mi71Tdzv+FSsmufDWM4vUSkQuUw6/nc3mNrcn7+FlGwsNAxUsRDCUpISNWLARQnNGPOUTwsHWptjX15tQ0Qog4Nq4pgcxoVSHl0DDbaZ7b+GHV3HcAhlijUe0IPgmMTPpvAd4IR9oB67Tk52zeLLfmT3BM7O7lhVf+8fAzE+ZTG9RzWZcfuwphsVF4FGwcSwI1ICIh5bhnlhSn7l61+TWHmLOxSwBpUCcRfg4ZfkxyuY5quoWqmDU6UU9o8ckeZnKUY6oOjvh6Hv0DK1v2PaLWfo5vZbZx+NkSuFuU1VvMh5/BGyMuCvxXR0Z/uom87ydMwieG0vVLE5eEKAONVVVISJsbl1JADgg5vs8JhKr0Qh4DJeem082TxMMLNKfiZArzY7Yk7xzzuWwLn4TM312ERl8jI3wbcymN/HicbKNqhGa1FKjGpVoyPf+0D3Cpb+3cKdL/X5iKXAqM4LsMJ2+znjjY+Augl7AgqAac/qxonfVEOp881ZJDvSfOUkMniFQ+5qmDjgt2Ny4xObgCoEBgRKfQjIqKXSCJv7JA5ZENiOP0YixI8mDCOCENJveEt3aCHiMYvCtDAefovEXqa3A2yKgq2rihM3y4Dd2sXl9cc8u5fNW8YwmBhmVCT68Q1V9E3QHmCU+YGWlv9cOYumBYP/envcQdg6HnhngS7PZveGc4GQQYU0VGMaBuYBQAkVSgC0BbyyWWPAzZGnj6t1GGWU5FR4IAR88qhYrC6UAtkA+xub4JtP6FXy4hQqo1lgIoAFRMB84an159hXyiopNU4L0KOkszIFrn6towBRxN5hVbzDY3AEq0M1FvJX+Nu7v3bx/syd45sDvoL7BOI5FZYiTMcIA3xREQuWyu/WGh2ALNWr0iLkXkTF3DB4LUIkc4bQRj3al8kXM84UhcAU3epbx8BnMLuOtwPq5SGkQrfPNOQxv0JY8PQm9ytHQm5fV9gqGNOYsYGYoHnW3mTXvQbgFYdJVRC2M57SUGjlg3x7dOsueYJZjAb9VoKSRMsvaDeBAXKI7c5SFxvRey1ovPk6CWDY3W4aQ9B3W4yDMW+bkPKE7NwuHe1w3M1QcZoNuPmvszS7AXWBr46M09VtYuBG9v0YJwaPUsXEbl9slHsrnkN4mY79X2H+qtwcllXkGD0XhCExo2Gaye5XxRnXw/ZfzOVIpe4LnAgA5MMTRppNUI81ZCGkMXVJ6MdTVU7giBO8J3vZtHMse4Mk7D4eYIwwhrhkRwfx8CQWJPKFSfhyRZ8EuEfOF0ZuYf384lnM96HEmPcNVwLXE1dqGN80imYX5BicVs9lbINfj1Ilepanv7V/DkveZ99Jae4JHOZvqbLn8uvL3rhlahOV+dpWD7J3537RY/fd1uHJHPcNs3deciNwDANybPas6Sqg3p9gKYgQJBDYoeI6LV76bq2+/hrLD0DWY1HhvoGU38uc06YF1MmBDt6PCHJRsXrKyfP62vAkVGm+oDIGKqv4G+I9A+QnwFzA1fGoR9AKu5Q7tzSG1O/Q+ncfwaA6HnjdHXo7ws49QMZ5JL+AE1svCFHIBJOaMLPFKGpdQnmA8fBZrrmM6jS+TIhVQ5eDRw61lu3/z0rQHiC0nrENCg+pNav8OpbsF8uQyfKa72vMCNVeDrq+my5Ll3HoIx+uNLIpDKCPBdjulXje5cOEjsUAmSJwjaJoBcG0MwhBJDaxGtWG6dwOaGyC7yMo2luUCtywZBLNkIMzSdxh6qsABG8joOZw8TrAhJoqpy+piHfaAGmYe0YARK3Zn9U0a/wHIdqoi1SUAbPsQczQlg2CWLOddifa9wX4VPmmCAQJsAE/hiqcJskGggEycvh42i8WWiQiEHqEmhG18eA+4AXjUZIk6rW2zyNcvg2CW07HJsxyRLE0x2MeOEMmyYQj2COPBczh9hMYSzV5WoidswaQ+QYVgTez3FI/IhLp5F7gKVPMimwUFf1BPcZZcGHPMSvy8h/TuBeSOanhmNhLSxPIew9B89JJLz1sctWRbyPhZ1D9J3bxJ0F2w1CeY5UTvv6qAb2KDfKhwbkbt3yP491EXKdQUIchSCCDbl9kTzJI9wizdBZ4rR4hl+qYIGkOfTQGDx4EnsHA5qgoJBM3exMOu6wfug0yFSapKCJFHlFCjUhHCNRp/FZj1egxbX7D9GYdkn6s+zKPwBE+6Z+t++rDu5/gOq7/rpDyYh5lTd5zHfJRTFpafO43nd7Sf23MHNJXat88YII44fBnQEhiztfVxZjdeJdjVbsL8na7rg86pOy5j6m7Hc9T6bV8f4F2AZ/l5lYKm8RRxcCDqhCZMceUes9lVBoNdkAbzimpspYi3vPX0z3aE5UGiSLpumz1LlizHISEBXs1Cocxc2wIlyOO44knMhnnPr42mj16dIqgFVGos7ND4m0CcKqH9AbqyPK8wy0ODYN4MWbKc1u0+b7y2jkokdFWicYq8JU/RgXucwfBpzI8iS8k57hVch3BhzN32mJ5EEAuEsEdd3wR/C5gg6s9lrutBsEmP40uyZMmybkC4tPXbVgmLbRJGwJuBXmQweJRgm1gYcF7KCO43bHmMR9bdx/aYFA9WYWGP0NwAdlO/4Iq3ZTkcTzBLliyn1Z3pA2GviL5rlxCEEo/SIMAI5y6jcgkfRmDrXR26XOjRjgs6K2ODrDuPAOYRC4gYhQZUKqrqGrAN1PuAL7e4HAMIPqi1dN6rk7JkOX4ghNA1x69+LogDBohsMiiuYH4Edva7qtbXCyRxvC4pcRFEDJfYYwi36XK95tNMwSxHDoIPA4BZsqyrJ3HmSsg7kOt7hPO/tK+JnYRFcg8d4+EjiG1w1oNH6+4thhBzuCKx59OIICcYWEPV3MLbTgeCxnyobs5kHSEIZiA7b9If6nsvD064UTf3ts21/PLGV1imRZPYauYo0vNjXPkIxoXYSH8nVbJvFt7SBPUsD61rg7QFMv12II8xxYfriMTimFj9a+Ss152leNgbci/Pr+rdetDPPIk+rFW9Qw/aD/Wg53dUc+oOcBRWehDSKdFVk+2tPdBFbds+1z69YkMuHIfdyzGHlRt7+Xzmrz2a6drrNK/yXg1RSQgn+DRUqUgzy+OlMWpAGGgRG+fNgV7Eig9RhSs4fbvjH42fVXa33cwQbZvwLYVOY95RaMBCzEsdYYXpYd+Lozbw73a8/ecjA0wMiAYThGKB+S64GtMb3N59mUsXvgPwqAzxcbAgIcRh2mfdSDi2PsHTEDo4LyGS4/QgOtvJlqsM2yn1iXFEJD3c/OcKyq3ju7aZO3HujfkEYos2Snx4oI4vC4BXYIDoRdRdJHSeYP96Lt3D/tBWawtwwuLfszyUPgqyONoqmhoNsU+w9QRjLlA1NsxnNXYEnuBRAMq6hlYPa7Dr+gFqWMK6cLBvKA4sEilL8urEYjNuy0XS/7nKs1suxJA7gu3dj/febb1wSu/PYa8vY5la+V6unzCkHG4iVRmVr0RwM/MpNHeXbxUlGyGHeX/Daj7Q0BB8DTYDqWn5Yu3cXZ9j8gTPm5fUP9eD/n16pW/Zt36BJ+YV2odP4ay0qWxedm3s/2nE5/uPvqdw8GW7S37xgUTJuRF6HvxBxkdYGizRevBDymILrECkWPiMuXGYr+867GPvK6zZo88GlMs2jtgTPChPlmVdwe5u9lBYZWLO/yV38jNsxffEHJ4mxaqkUOm+Ywl9sqcV36+J7bLvVdxN8WbFvBII+0UqPQDULqzdf7IARhS6SSMDjAKjAfGLgYJ9Vk04dTG4deM+vXNUI+zbt6IQQkVdbzMoZ91rQzCcO/sOy7HnBM+iV3g/QH76GnAP8qp06VEk69+lf7cPWflqFn6GpEz7f9fe73IHMNZMbHGsylTvrBa6reCAEa7YTGOWHCLa2ytLa8mWDZxsHB+6SJhHYWQeYFEMbEYTdoBpjOZYdlCO3BPM1sSpOrsl839J4XVPr1aSIvU+pRezQ5J+ajelvD+3Vfpfe9B+lD4QhlVProDdLA/jUcjCbQ+dNy8LcW0BGaJuExhiUmLMMBQnoVegEccxQVLQC89lOZqIjnaevZlH1cAqfLMNtCHR8wOCJ5ITPMtAeEaXyb1jpa0AxxUeZQqG9yZa68I3LRQF2j1854HeabbZDh8IVyhVlAUnL8QKUSdbnSd4Z9VxBw8zg+KD3y2Le0lafvOeF9jpK/EQppjtRhC0puvdzGVJJ+AJnsQ8v6MGwnutbj3s8zicz0shLGkrWlrUkXkJtdwZpKwRxA3iCwUwi54DEsfvhPS5Ir28U2JbMUXaRqWOgSUBpqz2Ei30HFMgWBwqukrquqYsy7W2Otf1WJbf3YXYivg/o0R0E1duUc2McuRw6gjNFOfKGHUzS+0xoTN8LOWt2jyyrrlteVojQCKCAwINztVU1XUikXYMhxZFgbeAk7NthDxIFC+b1ufR+rfV1YEWsQmRZXKAeQJeXEE3gUfmWAhgvkc8Ykaw6CW2bYOxcs0jIqhq5zG2ABtS1bf3qTBxicikr6TMwPuGEAKqSlEUxwqAZ0v2kwlI3yAxBRxIicoQYQDMsGTzuI6Wq32T9iIF6d/LxThZHlDLy2J+oX+PBMQahBphF2wntUnEzSqii+1LWTIIniVL8MFs/nYHJYJdCdGr6zP8tEoyuWpC2deVLFBW9PN+Il1eMGBgnmCGc7GnrAk1IelEVUW1QJwk0AyIhe5zLYEnCE6HEaiJ1u3DWoFZlgEw9NzA/nIpgSGqY9ABmCxOa+i//0AFnkOhh2DCpuBNmG85aTExVVdLjYVdaLbBtb2CGvlFzzgEPsj+zyB43kC+7/l1oDUviNBlBn2M0FV3Fgs4Z+1n9PITvmmBLf6v+zhxIFAl6ibRYoHCqf2YYKCimChGoAkNEBi4IoGwYAF8wmUzCBYZUFS1A9ks92ADHahm91Ee0LZKFMUQkQLr3fzoDR70fbk45pAh8ICbaslBrFEqAlPqZoey8MxDN/k+ZBDMcrCR3vu3B8wcZqkFRBZBKgg4mYNgXcF0AtMZOBd/39uDvUlNNZuHQEU9Fy45yoEwHJYUhcM5KEsYDePPjt8wgJlSFIPumKbTGcOiwEn8nrlqcCtYS+61jzDL8nWKxsViZZShCA5xQxCHR3rAZ/s9PjnoO3JI9Ci9erHICysyw/tdSrknyyeDYJbzvXHm+RpHwLXRzHntTIjgV/v40rfegZdffJlXX3+D99+7xs0bO9y6ucfuXsWNG9vUXvCzQB0MCwLqKNThCsOHPYrSGJUDRuMBm5ubPPLIZR5/7FEuXdrk2aef4iMffYaPfmSDzS2YRT5nigJGoyHOunqclL8kNQiD93XOCx5qWKkdxdMCmEPFdTMF4zQDWQF0bXg9LR7JqubI9u6SkWcEnHg8U0LTTpKYR3WyZBDMsqDorEenVIAJPsQiTzOYTeH99+Dr33idF772Cq+/eZU//sprTGsheI+oYzAYITjq2jOrAkXxGAFwImgR2V0CRjDwjRHqy/gaahFubwfeC4FX5BpOrqMuQJhh1AwHwhNPXuFbP/kxPvtt38q3fPKjPPm449FLMBjMiUjanzG06ljFQnNvIaXzpDjvcB2W2kjnvyamHo0jfCzEnkDRnIc9kbs470daqLiNBB4B8w11M4l3MOYY8kXLIHi2LcC7vz70lFvqBYv8EpEkLcSfN27Aq6/e5MtfeoHnn3+Jt958n53bM6oGQhhgeglPGe39IEx2Q/QIcKgKPhVV1KnqJUjolKTDoUUJQfAhMdwTEAzD432gcOD9lOms5s23Kl5/83l+/Te/xIULmzx6echnP/shvuOzH+dP/Mlv54knlSJVkEZKtjtUv60ygu+LrPusgKfe3SiQVe9JuUIr0jisVNtrmvRrr7GwzTHfiRghy8NJxwEbCKJdJrfda8EqxE8hTMCmwJgcFl1zEDwr1uTdz+PhclWhmxQdS9H30ZCZYiF0/XiNr3AFCDUBj1ISKPCpWMGA3V34o+e3+cIXXuTf/8HzXL8+4fZ2heFQ2cL7TQigriSY9vroY9+fLHgNi32SDtebJWhAvXDqZu3ftSucQUbAAJ/cVLPAzW24td3w8isv8q/+9Ys8+tjv8ulvfZbv/t5v40/+yY/yxBNQuJirlHSAqskQbpK13M4hDenfKRTcJwMXWTQc5Lx5kbJ4vrpwzook0oLY9xmnzluIoQMRj3VDdSOBXguIGtoPz3nBhzF+JU1xwcrUBJ/Cna1xYvG6lwNo6l3gdqoQHeBSquNM2wa5T/A86Cgh4JY4WfriESeYj9RVRVEkb08QHFVIbJ4C716F3/j1/8Cv/tq/47U3riFcoBhcYDIpaOoBRVGClFGxqcVCGMKCY2X3vasWXQOTVSCzSOXWZqXEwBVbTGYz3nqn4r2r3+Dzv/0lHn10xA/+4J/kh37oc3z2M1cYDSPweW+MR4K6lD9sP95F9PVNPBdXRAD2YbHgZuUxnenowh0AP7nYInEddYrGdN6z1psqH/YRPWcv5HAUQDIqrG9kzKd4iEjyEgNGRZwrWCGMs1OeQXBdw1H3//4Y+oieSrR8ejRWEjAaxEXvqgqxqk9R6iZ2Knzhi9v82q/+Fr/777/AjRszxpuPMxp+iFvbFVIbgQJxCqoECQT1SDC8hXuaHXdkVp6AqeBrMF+CDRAd8u67U37xF77Ir/zyH/KR5x7nJ37iB/nxP/+tbGwJuzNjNDKUmmmzS6GOQkfAEJf6DL2HuqkYjQbMCwnSuKCF+Oo5LjPvqFs15wDX8eZIiERN5pBgmIv6ITQVOjg/A6Vzn+B5uMkLBNf7QakJNcE84lwMZ6oSAty4CV/5yh6/9Ev/iq+/+BrXrm8z2rjMxuaQnZ2aJtQMRxfwppQplGpmXQ8e4lNT9P0OZD1cqeqa0cYmYspsNoM6MCi2EDy7uzt8/es3eOXVn+MXf2mDH/ux7+cn/tL3UYyEWW1slBcA3+VCqypQOsU5cMWApqkoCt3vtHZAmNstSJNTxFy3Ruaeuy54KytdySzHYzCaYeapmynDQZMvSAbB0xyWWvIg+5Sf6AKrSpwnpiglPr1+MoXf+91r/Mt/+du8/I132d41QtikKLeYzaAJSjHconQldV1HKqw0MTyYRyzE3JoDC3byhQ5O2Z1OUFOKoqQoNgiJtUQLh/cDxFe8/tYe/+i/+Zf86r/+Pf7iX/phfvhHPoc+AgNXMpnWbIygHCpN1cRrJhC8BzdYrVSWHKLzq111aU1mw+DkI0thBQh6RAO+nhAnSdgiTWGWDIKnFyNtcVBpN91dU7mMsjeD4QBefMn46X/6L/jDP/gas6pgMhGcu4TJIFKOiYEFai8QfGxUd8ypsERQUVTB8OgJM38ECZRFiThwMkDF4euG2axGRBiPBjEcoiU+OFwx5puv7/L/+bu/yOc//3X+yl/+s/yZH36K0bikiadOMShSaBkGg/HKSFOWBe2aejT7BVD3epFyVurIgNB034IVNRofc4J5IWcQPMUe4NLzGuYWuHV6iQbwFgHr5k34pV/8ff7FL/9bbt/yDIYX8RYYDAtMCqq6wXxAiwIpEjGaxp6vbltJJNxVg+CbXqjrZIGwaRoCgSbsYWaUbsD4whAzo65mAFS1R3WIsQF+A2eP8dXn9/jq8z/Lr/+r5/jrP/mjfM/3bNFGkmvz+GnFeLy5qKeln0s5616PLq3DsHoNhrA4XcVkaQp9m0fVBY7LLIfoicvd9YWmVpVgM6J2yPchg+DZ2QW0BRptH5YP0KQpDL/xb17jf/zF3+BLX3qN8fhxysEm2ztTVJXheERoDJGAJ2CtQlOJJNXB40Qxi1S7hWgMNfoIiM45vPmTU9OmmFryTjV6xQTqepbyl1CqYzjapGkaqiowKDZRdTSNx2nJH/zha7z00j/gx37s+/iP/+P/iGeeAZWSclx2EynmyjusMFL0nK89j1nI7CPrLNJOjfAJBOsMgscBgoc1R69vZa76rAf5nrt95rp5gP1xQ5Z6COY0VpEotwkzRIc0idLsg/fgp3/6t/i1f/37NH7IaPgkdSiZ+QZXjBBxNHVUZOqipRgb2dvDUJy0QdXk9YRYCOOSyxQCSzN2TkANd/1poLY4bd4lVoymiYUARamxsMc84hLFsA25tV3zMz/7eV544XX+1t/8Cb77ey5TunhtShpEDJ8AUXCEEBA1fKgptDwRILzXIc8Pv771gHXZki1YnBcoFkduaWrbsWUyhizHAna9YiQxTWtVEIzGT8H2gFny4sHE1lQPHt4+OTN9god5k0TkVE2Kn09CUGRfH53QmKdqBE0sVn/wu9f5b37ql3jl1RsglwlW4EmjbtqBfhSxss+azpNUO9g6lDSjZZ4HTNWo9iC9gYfpDR6ktGW15yJ+QZEPhheopnuMhls8//y7/N/+r/+Qv/pX/xx/7Se/i60tR2OBxk8YFXFwsPce5xyzesqgHJxTBd8LkVrrIc8ZYiKHa2yduLOHmClkjspwFlvxB/HJC6wP3iJnzS44Ky0SR2GlnBYgbJVuCBBCg3NFRxiNwsw3FG5MMYi5v5/5H36HX/zF3+L2dsFo9BiNd3iJHJrRWiflbRKwmkNseQAgi7mbtR59o/s3/IHnsvxTub09YXNzi8lkhsoW01nDP/7Hv8wff+Vr/Bf/+7/Kc8+OGI028dQ0YcrQjZjMpoyHG1nhJkJtMw8aYkWyrWp9yGN7jsft0SXPPXqDWOwjFmsb5mfZ+LiH2MeZBsDj+OzD9QI1hUOL+bzatIbVjWmA579yi7/7X/0iP/9zv8V0MmY0eoyqVhpSHq/jekxjhqSX07IiTQJwdFxi5uKmWqoyCxK5QUOfD/Ik14etCAktPBoWuFK7ZR4fw/GYWeVx5Yiy3GQ2LQlc4htfv8l/+V/+Q373d1+h8TCplUI3qM0YDsdgpHDyudW4gBFC08uXpikTibVE7liwkasTD1NtBw1313XiMWbMOZfyfTg1nuC5DnS0Q2dF5qygab2HVATzh1+4wU/9o5/jlZc+wOwSTSgpRyNm011KJfKGmmDiUm9QgOAxfOJ+3L+pbNkbXLAcD/r7CajihZxk2Of1zUGyraBtqYXTWOCiJPiKWT0luAIdDDEKtm/P2N7d5e///X/BG2/+R/zkX/9ufBmLg6pZzaAoKc7tbrGUiw54X2MW0HZMUnvdNXt+x7P+lwzAcACDkwSMuhcSPR+FMWeeO/RewpkPchHW7RxDGmcUQ6KRGBpgWsHv/8Et/qv/7//A22/eRGQTYcDG5kW2d3YYbYzxPk6SDl3SvGV8sZhPk4blBttF725+jdtxLSY+hllYlZM7EX9wfpzdLMT2nHo5zD4Aptfd2r7BhQtbjIoNZrMZtU/8oTJm4Ea88eZ7/Hc//etUVeBv/63vpVAYj8tzYz8v7p+wZPQYPjQxHCqGqEUChf7+lFMRcDrFcre8qrKYr62JLRLWGYmWncHTCYL3k887zUDYFcWkKQjtJIT339/lD/7wVf7ff/fX2J1sMhp+iFldMWsqrN5FHFS+6Z13nNigtoRtElZvAtP5jLIlcLSehxjQEwXC0G30OAoijvNxPY+lVRLScwtDF7K7sDVEpWZ3d0IIMBxvxB7DWYOJsnnhaXy9y0//t7/Jm6++z//x//SXGY9gWu0wHjqgPOUBlPtv9TAiBZdIDIca8d/dopI7MQllADzJ+2yxg/jQ9Oy669Uzyx36IAUt6wuEy0pov5JovOGcxB5Ag8ku/NZvv8Df/we/yKx5FthiMp0ixYDNC5HurLHAsBgmT5B5qKodPovrYeH+OXlBDlaOYut4DRMQtp7g8s+VVnOgCR4xoxzEHKgPDRZACoc3mE4B79gcP8nnP//HNM0e/4e/85M8/cwWPs1GiMRyuuw4L4ST7Q5zPtZzTR5UZStRiUoDVGATlFl6pkj3QBKv7J2hlH1rL8sDaMOliIx1602Swdry+4q1r5uHrbMXeIQguC4FLettqQQaq3Di8E3AuQLzhqoQgqVJ7CBO2J01DIYF0xn83C98mZ//2X/P7vQp0JIgDVoUmEA1awBh4AZY43sQpktbp99Tp3ew13UfXPf/ftLhUF1QpjYfJbP884B3SzvnztrX2ZwExwQLnsFwxKzZww0u8Lu//wK1/Qz/l//zT3LlUaUQEBrwgjVCUaa3+gYpW79p7rG2Adl5A76eoCUeKzqlxza0zIpjadaipRFJ2g3iqoFtmuoqw6ImeCGEgkI0DkWWSLbeZrL3td90c++O1jN82Arwdfd0WmPVmS55fGnOZxp35swRKInZEQNpCDYfcH2Qk3C383/Y9XnUnuaDOD85VnHM4iSyrnhfIwLqIu2UpkbvybQCYDAsqD384j9/nn/yT3+Z23sj6mZMwGFiPYvuTnReuuJ5vcuj90prH9r9e90s4oN/HgQDkszh1U3hzjma4MEKAgO8bfJ7/+4F/p//r1/i/asxsOQpUDcHQBqQQplNJ3fY3WG9rpwtPTolEn83m68VITZfwwyRGUqNmqBWpFD0vaiR4xnnI+2UiwMep1mW0xViIBYW13aX+1PEtEcosZjWyOOwjsATzHJvNoegWKMMB8kzrH0cd4ShKIPRgMZD1cAv/NKX+Xt/73/E6SOYBQbjYVTQWR7iDiQEsMStuKxoQpwvWJYl3nsKN6YcDvmd3/0SwWb8nb/z13nuWcGHFCWkwZUKoWE4Gt+DYbIOnoje4fvposiLhS4BrOl6BCNKhkVwl+RGZjkxif3QvscvamB+4T49zBp72PV51Ov7QT4/e4LHbc35yG8ZlUVAXUALoSgUUWVaBQz4lV/+Cv/4p/4ZKo/SNCNMBE9FzqkcyV3ptkNRFJgJTVDKckxdKb4uKIqL/Lvff55/8o//Odc+gMpHJ8iVSmim4IRQ1WAFksKgus8LOul7dwA4yzyA2x9PqWjP/fCEpsKoMev3ClpSuuHBvjvLISKArV5jyWDpqO6yZBA8yTiU9gg2fNOgLrYxNMHTWOS7/PznX+Ef/cN/RvCPULhHafyAJsQQapaH1RO2UOijqdhFLf57NqsZDceIOKYzQ3TE7dse1S2Ggyf5zX/zPP/4n/wbvI9eU+0DPrWhhLah01KoCtKYpjU1XIQl8AqLHiH9HE6Db6bJ/Y0KtQPCfq+mhBNlH2rHgB30OMvh0r5H3lbzGpE7NwbyMwBmEFwjMIyh/Hj5q6YBjYUu33hxj7/393+G3cmIuh6xNzGKcowWA3D5dh3dFkiekBhN0yCiVFVDUW6ydfFRLAyYTAThMr/yq/+OX/xnX2VvBsEKynJE8EYxHHb3d+23m7AS/FrHoY2Exv7QADQ0fjcqVSIzjyyAaDgYRCyv2+OJZIQDDYN4yx+uaCWDYJbDWa4+KiARcG4AOFwxBIRvfnPC/+P//g+4dWOAcBGRTZpg4GB7e4eyGOZbdkjaX1jV+mFcuLBJVc+o6prxxiZV45nOPN6UwfASlR+BXOQf/Tc/y+//wTvgIFiJ6JAQrGtRXARD7ar31mrLy70qBgMqmrCbmq/bytpUmWj+3CrQtVvdfaIDaantwtx4yQCYQfDEL/hgWe3GgNy7Vz3/9Kf/JW++NWE2G1PXJd6E4bCkaSouXXmEqsmK5tCA0Pq0anPZvn2TCxfGIB51ksJKgmjBrK5RN6D2JdPJgJ/6Rz/HH335eutDolKuAJbTkAvTBa9CZB4OjYq0AXYxv4dI3fMG1y/Uey7CnXcLNN0noJ13A0Yf5ALf6+Mwbua63aD7Oef9z3mQGrQGYptD42OD9q/96h/y+d98nrq+gMkG4kpMAl4qRAN1XZ+7zXwcQNiF/JJCLwpHVU9whdH4vdgzp57GGsQ5TIRginMXef31bf5//+2v8N77idcVCD4ViYQ4tLhNE1Z1cypszv4opDZqgc2ACbPZDVoKrlVrvvu3Kae9EOawddlJGAILeVD2j7lalSc9TP137j3BrKhXKRgAT9PMQI0A1AH++I9v8bM/91v48AjBNgkWAbA/EUFsHZlbTuE9WBj6KnNPsAeE8ZGKCcTTXfzUEB48lIOL1M0mX/3ae/z8L/x7plUEQnWO4D110yCqaGo3GJQDGt+slQ2w+MsBqkACSAXcIshOImQOB6uTnP9bWy9Q8gSJ4wPBDIB31j5FUeKBuoHtW/Bf/9c/w/Vrjtl0g0AEwKCe0A2EjcUJmkHwkIAw8qd2D1j0CPsPFh9qgcFgQFULwcbs7pX8s3/+O/z6b7yBCcw8iJaUgwG+aZbuvFtjFaAd1Vv3TBu9YEoINzC7PWd+OT57OsuD3ldTRNIoNdy5uEcP4o0e+lXJAHj3S+5NCCizBv67//63eenlaxTloxTlJbxookZqOoXTsrZkeXgJYoR9LrUueoTzV+9/SKyCnE0bXDFmNH6U69crfvbnf4033wLnoKpIxU9CU8fGZQtEPtg1XZO2rBICKTHYADPq+gYme8kT1Ax+a2rI9MGgC3F2DAhZsvm2Bpd7VhvIgLqBL3/pff6n/+n3UL1IUV6gbmKoydQwXSS5FpMcDj0cGIyedgeGMn90OcL5dZ9ntjQaIsGoZjPKskSlYG9SM9p8hFdfv8bP/PznaSKfNHUVQ6OqUSHFn+uqApZrQdOBOpInWFOHW4hOU8P1csGJW8opaVa6J+oOzXOygksMCPmerAUInvcqJBMoykiLNpvCT/3Uz7O76yiKLbZ3biOFppxV6Ka8q0UA1MQDKJkG/iG8wGXPbl8cY7FqtP/oWdulljgF7z0hGD4IZmM+//kv8Fu/9R5FET0/3zSRFN1OF9Vdb5xzCgnPqOrbOJ0mYmxhZf7PVhXD6L7Bx1mOUMGgB+jZ01+sdOpBMPcRxUVqxAKK//6nf4+XX/oA5y6wO51RjhymkRbNFhZsf8RRpnp9eA0fQHwiE+5PnFgslmkJiOcT6gVwFJQ45wgh9l6VowFVE6gbZXu74ed/9pe5dh3KAczqirbAxnuPyuHso6OuyG4vQ8StGsIM3+wg2hxcnbUAiNlQO7HlLX0DpTdUGggWzsn5ryEInk0AXPYoZLVn0bOqg8RXf/3FKb/+G3+A6BZNLYyGG9R1okSTVXPZsgFxKBaf9XwdW3U/VynxxXxKEJjMKpxzlGXBbLbLYFikZvkxL750lV/+lT+iDjDe2OjG2zhdQ0eoN0ViVZo05v/2wHYJzS5qVcpT2wHgt2q9ht5rshdyOOs3XtdY4NUfgjZnagj4+JC5hyi5ruBwQPBBmlEftL9tHXviBEl5kYBIsowlBpBEBIIRGj9XLD69SxqCQV3Dz/7c5/ngesXuXk1RDqnrho3hBqGxGPZMSlp7gzCDNARp8lDMh1zuGgokFCm83A0STOOp+o+2nSKihHbjiA1XFjTBE0JDWQQIs6SCtpjVl/iFf/Z7vHcT9gKEtiJUJDbePaQ9cxjN4NIH/aVRSogyn60xBZ0y23uHAXsQZog17AsnrywmiowynZcaBAsPv3iPqy9tXZvsW47brsKZdoBzQAiYDzhxmDM8HnEC6oASRRMlnj2wfl13MoK1qA49PxJWG8AiqHPg07QejUqnSuGwb7wEv/d7zzOdwsVLj1JVTSphViTMQxfL7RCLSjnLQwGhPWhuxNpGiZ7iDx0QGA4fRmzvGD/3C79DkDTxWxzNtI6lo+u2hleMYbSuNKYBdvDhJjCloL4P8rec/zvSOycxxx1ECTInau97e9F4btW8rvb4z5jkUUrHZm6sUKKyrAJsIQYvNmJWw8/93K9w4+YuZTlMU+EVEaGua4pCI4laxrl13WJzhdIWe1gZH23eJQSa2viNf/XveOUbkzh3EI0E6LaGpyOr12/81QO38c02FqZ5DM+pXLFFVvMZBI9aIS7hY9KNWqTZXdIyiShf+eOr/Nbnf5/Llx5DpWRvb0YxGHQURwtW9IHz2XI8dK22jBXxkXwoVUfwyo1rFb/56/+exkMIijqJBNtrcPus76XtayHrV3J6rLlBE24RbEZMP2UP71SsUYvtEUIJlN1NzumUDIKHeumsX0re6hRlPnXbebzFZycT+NVf+T3ENpjsNcwqGA03usVZFErdzMjhztMQBehvn5aNozV+HFUjmG3ye7/zFd5911P7Bw/VHL4sg1giZZCYU1qMcdT45jpm24nv9u4AeLf5fVke3vReXH+L+dnFXk0XJz9TxHfmy59B8DBXot2h58b7WBbfVisj8PLLFf/hD1+kKC8xq2A42EB1wGw2Q1Wp65qyLFlXdv4s7b0P+0GxB4zeewblBoQxb75xk3/721/EuWTayLosX1067t5YpK4IyAMTZs01cLdx0qTpEVnWwRBbzSC1WIIslAiD5AlmySB4JKbYaiAUEXzwtNwbkxn8m89/kes3jd09z2jjElqMqJo5BVXdzCgKl6/vaVwMvSb84KEohhhDGj/k33z+P/DW26zNfHm5Sy67K6ywGsJtZtUHCLuoa2KjvGTjbK3xsasYlWioyzCBYFb1GQSPSyemSQLt5W08XLsGv/O7X8b7DcyGOB2wvX2bohgwGo2o6xnD4YC6nh2sZHKPzzqoGGLFZK/3TfoN9zAYDNjd28M3gdH4It989QO+/Ecv0do7tk6nYgepAAP2wN8i1DeBPTSD39qD337V7lLRVrl0b8/wdXiAFonisL/kQeP+/d6VO33f3T7/qI7vXpeQpdoHERd7xAS++MUXefedHUQeQdRRN4FyMMKAuq5RVUyWG7YWlVPbYpWh8KQNnejhx1wLxHFL8zaJyleUZUnwNT4o3pf82r/6PX78x78lBhm9UTghhEAIAefcvtlvD3V4d3t/iA3V1vZVm8x/YtFTDBXolN2dt3FuBlRxenwIiNMj3V9HrfSOWn88bO/iPV+/BSMsLcl4Z9NagsKNCDbqPME+UB5Vj+W6X9/sCR6ZLBbGxJCEI6AEg3/9m7+HZ4hRYl3Jcr/PzFjdd9inPgrnwpJb//u8FNhcyt+aGUEC3jxVVVOUF3j9tWu88soM7+ebXFVR1e73SLKtx9uM3CYql5vdpQFu4sM1sF2UCgmWIxKnShQzQcjh0EMHweNmDDhVjAY29wZVCjB4883AF7/0dYwCc0XPq5uzwbTGuLRKpkfa3FKtZVkzMFwmhU73yxWKEXBOMBFER1y/vsfv/u6X9pkvqnrf1u1Dr2+1NOapN2En/SKSyL5lCuF96uY9YBoLZczn6s61VN2rq0MDijfFaesJtrdZjvRx5IGYIziGU2EerG+JdVx8spTsCX4envit3/oiIptUjWEqCwC4nE9avC3JA8yNPWsq/fB1MliIazSEJnGLDplOasrhRX77336JqprfzhDCAvgdD79uOCCQ0FOoFucHhvodgr0PNp3vQ1pmkiynYoUGSSA4yBfjDpLHEjw0AC4vvIBqZP2cTeDf/tsvMBpfZlop3tfde0MaXBfZYfrMMhH89mFfy9aRo6FrEjhJ3qAVCQAdEPC+RhzUwVOm/O9Ihrz6yru8+mrg2z+jBxp5x2NR+gR2Rbd4Ay1PrU+h0D1m9ZuYXAetcSIYgsjJL7+jvk5HnpM9xrVqQdFiRNsnaIT91cFZ8hU5DCBc2AQaB6gGD2+9DW+/dYPJnkeLYj6+R1oSv1VExDL/d86/rKF5nQaUStPBR/c3FG8N5cDhvcfMKEdjdvcqgo35g99/PkYJmIdC+8rzbmOSDsdbTNGHFGQI+57z4G8xq97HuduoNWum3LPccXn2CgzNhLIDwfWwnteRgDtr2QeGvgMKWiR6ccHgi1/4CnWVpoyrIk7i1Pj+cNeVped5+OV6SjurLfXCtFWiVnYGi3MuVXlGYHROUkXoJl/88gvs7sZQ6DKwHE/OpV13zSr12T3v612m9QdoMYmeYxBELYch1hf6upx0R5qdxiepK7M+ySB4qEttxdQ5XShXDtZQNeAD/NEfvUTjS1w5jPMCLYaiFniIu+ZkvauXmWUdQXHxninCdHeGcw4QZlVDMRhSNQVvvXWD27ehqtv1VGNpTdwduA5ruy8RgC+ciwfdJvj3MH+dQmdgNd4aLFU6W1YZa7Tq2okzPfrGMM9TCyVI9ASz+XKKQPBeQkLrMaS3LVzZH9ZC4OoH8Oo3bxLCgBCgKBWxCIDtLDvtTS5vq0TjLLvkIUpAE42VGnm6xBqYQUpI1ZJlyqsFkBqVKrLJBmFYDFEDs4BqAVLireD9D/b4+jc+SDljj1HFmZQEfB2LVizpsWB0Ewy7ghY7hDUbCqBMBl2Yf6TX6CHaW0z2vs6omKA2RakBw3RIbXkiwXrEoUICP9AQCdy7oiXzOISmCTgdgG5Bok5r+vf7rO7QPE/wpG2I0C3Rt96Bm7dqgjl8MCQYISyPSdI7+Jw5/LS+8YC+V2XzBvqFvy/e52jKlDz//Cv40PaReppQxVeoEvrth7K8Fg5LXGe4tVMDY4pagRk0byPyPsIe2hEBpPCtZlq/9dVBIc33TveUAnWpPSIRO0hm0M4gePSXLnqHzsE3XnyZ7du7qSm6QMTl4oIsvPC1bzCbtqulwGnq4dJVAHhEIC5LTf9iIBWwy2z6Lk24mYBdUVzMb4rlUUrrbp4JmEQjTCgp3CgaPeISBJ59/ZOH6p4EEO4z1CMH/9dffJmm8ZgJzjlCCF2xRJbzu16uX9vmxi1oAmnem8NCIPhYVSwrl8fhKq+ugrDLahtoBeEm0/pdgt1ECFjQntdYo5KnSKyfhIX1JSIpYO/QYky/ZsFyZCmD4HFcTo8ymcDrr79N4UY0jUeloKlDvtzn3lRXtncmvPvONk09LzLxFgihWQF54dABsB+PVzTVxwSQHax5n8a/B7oDNKnMsG3d8Jg1eYrEWnh7i+aR9FS5oZgp2IjCbaShzxn8MggewQVbpZosPf/++zU3rm9TlsMe/ZnmcGhePezu1LzxxlWGg5gXNGKkoCiKHvCF1avukHRZfy5gTCHVwDWms9cxbqA6jQ3yQVqoRPIYpbUypg7+uxDMASOK4kL8m+g8/5uvXgbB45C33nqX6aQBKcAVhBAoXJkp0M69CD44Xnv17W6MkbW5mpUGki4C4CGAoAHBorkmaFKKM/Dvsjd7DWGXwqVZmN1k+NgbqZm84cQlJD7h5b/OvURHsBJjA3GbILlPMIPgoaqwpYtmizGJ1oa/+v4tZnWIIVBTmqZBtUgT5zMQnmcLviw2eOfda8zqGKUya6dINHhfsarwxMwOyQsM+8BVzAM7mLxD1byNyKw32SItb+mTBGRZJ0hcXi8RCAtgDLKZ5gkqkjLQWfbL2nCHrnuosOs/kWhFW1iEMzPDEOoG3rt6HTOH9/EVzjnqpqEsHQt18FnOjwEl0eOz4Pjma29TltEekjgBDhFNDfa2em/ova/T/l5antNpGE5dcgkDuAb8NXZ2X8TpB6h4rBFUFHWxm9CMVNQVzi2V30HzTk8WAOcqKdon0VgRLTEbg1witkgIIkbAMgxmT/DwvcJVT9y8sYOFFG5Km0ZkXZr8s5yY2hKlMahmDTu70DSR6b8lgAhHbCBZX5mbRf5T2cX8e3h7L3GFBtSKHquRpYHPmsjCz68Rs446qLtPiWs4oAQKVLeiN8igp62yAZ5B8Ci0iixvFnj3natxfqA60MgXqqoZBLMgKLPauHFjvgNVih43qO4HrjtaXQ+ybH2iG6nAPqCavYFv3kd0F+nxUAI9nlshIOdaZawHEN45v2cmYCXOXQA2Uk5w0Zs9ao95vZm+MggeSghCWu3UM8na0mWn8N7VD2KDfBqvE/kh20WYgfBcW50pN3zzxg7aL/oUQbrJEnqf4Yd7B+A4ONcAD+yCf5Np9TrB36CQKnmH80ILJBA0ZB9iLSIJy2nZkEZgzSMNLb+rK7eAYcoJxjWVq9MzCB5aCGI1OM4X4+3t3dgE3Vo/0h+flOV8m1ExN3Nz+1aXXgsWUpXMcY3PMqACdgjN63j/BirT5GPUCE3PVOuN/8qyFrJ/1mhYWl+OsrwQQRDXU/YnP/k9g+AZ8gYPkukMqqoBFQLW9WS13mCWc25EJUWws7OzpJLcAkYtYs7hTZIQBJXkCYYbTOvXMa7ipE6kf03yEuOrQwp7mARMjZxXWhcE1NVPmYI4ymJMrHvMKj6D4DEDYZwXF5OF84qymFuRbE1nISAi7O3tzTeh6D2uuUMAIGuBrMLPrjGbvQVyK1L7eQOpI4/ovsb4wxzptMYYc8rz9i1XcZwon2gaz5HayVMk1gAaJ5NojZlZYnVPYYBgOSafhRCgDp7JZIIP4IN1DXm+Cfe5VcPB4GQHgZem36/RhLeo/fuITHDS61nUdohTW4QhqSymyYr0xEMJtv+emkbAs1iMJ5TABtgQzOL4b7PEGhOO/NqdtsKYIqule1x70rL0pYWk0cKKpS5CwKeFCCYDfGOoFgRTJAQKVcyHxOie5VyKKQj4IExmNaKkBuZYfenEt9UoEcMMTNvfBTGJxDJGNwmiz9cQdaTMvT2RLvrQdolJEHATaL7O3t4fUbBNqR6rDRXXqUlTSwMnhiA1jln8PtEz3Su4/v3KdZwbKK5n6Ggc2YYSTCnLEbAJMo5LReP9l2PKCZ467zlrpkNbnp3d3U6LmJe867xAJss5N6Zc5OTUuUqSFgidO3ib2hwY77idReaPCGfEFvkEotoAt6j9G5hcxekUZw2YX1ivkPizUdRczxHJa/jkFk+IALhPhSfDxAqEIeousL9HMEsGwaNfoUnJtRdWOu/RgpxaKynLYVvyPk7/FkdIgBZ75A/aiivCnV3vnqZgzooCiK6Pp4nFWa0XKduE5h0m03fBbqPapKKtgJMW5HqPrmJVyRyUaxBJaEkLbOleSJo4EsaUxUUg5gT7NksepZRB8Fgum5OkuCR0ecF2AQs5FJo9wWgcleW8iTmEgIVwV0o9WQDAe41P+J6J1gA3qGbfZFa9D+xFz9DC6hHRXdiVFKbVTB96klrHWDRIbKFLMD4XNhi4R4h0adpbOy7lfPXM768MgkcOfouXLLb/SfdX58BJ6KzrGArNmiPLfJM65xiPx2gbNWiJFbS4S5yh9QytW49t+2lLw2C2uLXboGs3LYK3aPw3sfA+onsofdJuXfRAF6qZi8QIkVXGiesgk4UVYRIIEhJx9mWcPkZslM9XK4PgsV42RYHhCIpCQXyvGkrypc4CEvC+xjlhNBr2lBirRynJfr+Oe6jwi9MprFt3EQg9cJ169hpN8zoit3A6BYstGypF4i5V1Ei5p/gI0g4AznV0Jy7JoF7VR2o4CrkC8ggwxFL93sN4SadNHoR/N2vmQ75kG2MYlBqr8lIoab748uU+7xJ8TaHCxmgUy2FiKeYc41a/697XYXIUgglmLnoEALYN9h57ey8R/Ds43UWlThPtY2/ZcrgsKtqwAMFZ1g0TQ3pAoKAsHgMeWSA7N5ubQ+ch0pJB8ERW4vyfgwEUJagLaJuVThOfM4F2FlWhLB2j0WD+N1lWbNbzAg8CwMUm6AXPoP2lC18GTN4HeZOqehMfbuK0oh2cC3FwrnWArPN8oPQ9z6wuTl7L9+iE0v0Jkip5rcS5y8CF6LUvjIC8Q09pBsE1wRE7HaOGFo5Q5j9aveU9PPb4FULT4ENDUcQJAU3T5OrQ7AdSFkrwM555+snFtFsXvWx5FuPv7fz32PUgXV/qIoF7WADNQJiHwTxAjch73L79FZzeZFjUcaa9GU4HCCW1j32tkQ1+ufKwVbTrr0BPQ3P2w4hzgvd1Wg+x+1NVEUrMhozGT4Avu37kOAsyrYsHpG48TdczM8YcJxD2FYLML6VTuHLlAq4wVOcx6jxKKQtA46eIBsbjduByKmYRlsrZD7R12V8aHxaANnUUMp/MdBvfvMZ09iqitxAqlBAb5ynAYl+r3S8rTZaTAUKkqz6PU2qEYAXKGNggtkfIncz3I/euTlV0Ji+pB7fq+0DYDc9V+PCHHkM09EBQcU4zifY5lzjy1HNhc8jFC3M1td84Cks/WWAH2f/aOZVWWGhmr4njkt5jOn0F37yJY5q8QO0A0ER7KnJefj8PifYeWU5UxPzSfVAsOAhj1F0C3QAd0raz9JfWw4DZaQHCBznOXO71sEDYKaWAS9O9PvTU4xAqRC2275ghKmT2hnMeQbCAc8KTTzzKcNTfuCkvdzeTNHFgBWIeUdpiUWk9wNSHCJhViMxAruMnr1LN3mRU7uGsQtp5geicCbBVrBnoTo2ib/O5gQLYoiyvRBCk7L22H0GwQ/ne7Alm6WkkW3lBH3/iSlRsIU6OMDM8lnggs5w/8JvnU4yap595Mq4VgcY3qC715y08bKWPuGDH2lzBCWWkSMMD28BrTGYv4Jv3GZQ+/X1xHQf1BPUgvqcw596g5ij+mno7GikaQ4nYJsPiUWI1cPTmu4KZ9rVZ3WcQPByF1ldUPTzsyZNPPMrm5rhj6+grwyznDwCXQeeZZ57qKNNaUgULYTF2dV+22DygE4u0NHl7OwT/KlX9TcxvI77al0MM7e/SpH/bPiDMsl7rSXBxUg2RhSowAC4wGF6JIGjSo1bvq/q7096dRx2VV/gD+YC20i5vyW2ffPICjz72CGYe6Y1QyiB43iUwHg145tkPzzlmkxcoIl2hA/c78saY18Jb6yl4gv+A3cnLBHmXsmgIjd/fZC0B0wYTD9J039syIcXPPD1qYnmS+ZmrFu3uRUyviMS8rsoYigt0fRHWZwrivu7jab5OuTr0uDaaaSxTx7VrMVrzKS9z6TJcubyBio9/E0thUU+usjuja2IhOpCI07uRRmkOnwQuXnB86PGtOL8PUluCprFH0nv/6m2aAl1xtl/bGiEQw6KtRzgDPmA2+yaT6SuoXGNQprCsrAxeLIGjZRWxtga4R2VunAQAGYBdAq4AA+LkwLDfUDpiMMme4DEi/UneIJW2+GBAV1fUMfZHkJtO4HPf9VkK9YQQ+wNNG0x9L0af5XT6crJwD9veZaWJUyV9ACsINsAbmGvitHYAq7lyWXjumQEu5dokCN7HJlOTOcxJl8NJ08Gl930hrjWjppaGpt83bzXwAfhv4KdfRcJbFHobYwba9hn2QDUoYnFcktq8yMvEYtN+UqYaFA2nDxCXPcPTbWkFBI+IJ4SAxwhOqHzJ1oWPAY8Bm4gUqHiEOhlMD3a9TuUlyowxx2mRzaNQiyUynvEQnnn6CcrSKJzR+AqzQDEo84U7zQCYWBH6fOhq/XKDgNMSEYdIyr9IbJVRMVQCH//YkwyHLXBGcXqncLmuWHhtRCGGTheCpzoDu4pVL2G8gdPdqDjtYNYXDdobmTQ/lxy1WMcoVJo6ImCi+FAgOkblEtgFsLIzmCIAhkVP8Iw7eDkcesIBsZadzyl85LnHuXR5jDrDLGBBsSa7gWfvnreQMQe9EJq0GRXxkZczhIpCPd/+HZ+mnaIUia7v04qN/Q8J+ub0aRFHG2AXZu+wO3mZJryLEwEryenoM7LiRAghpGiUw9cFoltIeSGGRTugc+mRu+AyCB7rpZzzgz76GHz8o0/hmz2Ggxhy8j5PkzjVdzhVBkevSube4ZKrZuY7mjOsQHFYmHLhYsknPvHRxDjUA0FJVX93A8H0dFCHURBIfJ8+xBCp7EF4j+nsNWb16xg3UQwL2inOLKcdBONoNhEwKfB+FOcH6gZd6LzTR8V+e03O+vXJ4dBj9QFk4feYw9HU8by1Cd/+J76FJuzgJOAocJRoni14imW5NUa7v7ZemSqIBgoNFKIoBcEbyIynn32MJz+01c3OFaErYJhXht4l3CNAAkFw8Vu7ys7b+Nk3qPxLmHyAyi6iDWIBC5KHOp92SYO5RRxBQMRhdpFB+TiRLm2F17e6nTlLBsFDVoYLlzOVvCt85jMfYWtDmc62CY1R6jBfurNy//sTvhPpdNQ3qe8Pj5mPvI5NQ1E0fPazH+PC1pzFQ3Q+703k7ppqXnoVIwrxP8G5AHIb7G32Jl/H7C1Ud1FtECpEfWoXyAbYqcdBmxetBHOoXGI4eILIF+p6XHznE/xyTvDYAHDFv7vEs2EhhsOefnaLT33qWbAKCT56BLn5+JSHAMIBHloEJjMjSCAEj4UGCYKYcfnCgO/57k+jLoIfC32CodcneG8rsJ0iH6F3AuE9mr0XqeqXgauUElBTzOrEY5sJ3M+E9gnEXKCFSJrtrqCDx8BG8xYZmU+2ma9bMmtjBsGjBMM5EPZj0pcvwWc+/RybI6EshOCbzM14Zk1Q7cBQ1BKwQemUsoBLl0o+85lHaMnTQgj7QqD3MhU79NZa8A2RJHsH/Kvs7X4VC++iukuhbc+FoqqoO2DNZjmVno4F8KGIVaFcARsklFtxj+84m/KM2ainOSd4r70p69HD0hJn68pL2oa2nMIP/dDnEJkgUiNWI9kYP1tKCRZaJmazGYPBgGBNZPynoiwavus7v5XNTXBpDpzq/hCoc25pa2pP+aVHuwINhBp0CnxANf0KjX+ZQTFFfewjUxyScpIhRI8wA+Ep91pUaULAlWN8KNncfBLYBDfaD3ayrLPy/c+e4KGA390XkRPFNxOcg098cotPfPxDWNghtglmFDybJmhcF8PhkNneBCfgioAxxWyXP/fnvv9Q9E9b/+ekwRWpMX72CnX9KiLvIzKNh9Nyitog/pQAUuX7dAY8QcFhYYCTLcRdjiBo7i7qPINfBsHjuHQC5iNQqosjbTbG8MM//N2oTjGbzCm0spxSLaQ90JvP2AvEdokQwLky5gb9DJVdPvktH+Kzn7nYM8z1LutKV/ucyXMMNsOJB26Df5nJ3vPU9duI7KGJTs1EMFxSjo5ujH0Ox58Bb3BACCNUr1CWkSXGcIu1MLI6apElg+AhK8RFAOxLIY5ggaaGH/iB7+TJJzYQ9tLEwSxnYtt0gDIHKO+Nwg1SHs6jbo+f+As/gNOujvShIhGOBmEK7AAfUE9fZFq9DNxCO4q2fgFW3uJnLuhAgfkRhbsCchEY9qjw2gHJIav6DIJHjHwHmFWiSl01BDMKgbKADz8Dn/vcp1GdAnm6/GneLmqaODaNxTr06BkOyiF17VEnIBVPPrXJ937vJ3DK0qgkPeBxB+VnIHhKaYBrEF5hUn2dIO/iXJ0MrDRhXkKaJ9cQ2txjbpE4/QAoglASwphB8QixP3CI4easRfuMNO55jWUQzPJQVnprfRVu0BXvqMZiiB/5ke9hOA45HHomt8z83oeQiheahrqa8CM//N1cuJTyeO4hQahTahXwFnuTl2iatxHdnnuAEgiaQp/0fnYUa3nLn+q1Z4KFAicXGY0eA8bAAKToOGRlaU3SFVTle59B8NDAjhXeYLLAvUc0Mjv4pqH1GT792Uf43Hd+OzlBfept8cX7LYvUad4bqg584MNPP8GP/JnvY1BC8DxkUiakiKsCe0ynr7I7/QZmt3HiUWaI+jkRtjSY1rEYRlJzv2UeybOhsgsK3UQHjwKDSJ/XhUE5N2TZGQTX4ZLJ6nBFrGVXimKADx6ncPEC/PiP/wAis8jiQdXzCiUVMKRBmDZ/dCpQ2ingWU4UAnv3pQU/E+0ijeNByXRvj9FQ+N7PfTvPPF1SFsQ+vRVG00JE1eZW+/zP8XXx42vgKoQ3qWav4pv3Ua1wGscqxZaLuccn+7Z4Xj/rvbhC58m3j2WdE0xobAD6KPA4MFxgAtI+EopfWHP57h8xCB7FnL/lqdBHxXhx75+/NN9N+kQMaQ6c00iaFWKvVqGR39E8fPfnHuNPf+5bKXQCtos6j6lQe6EJDmGImKCE7iEmBASTgEnIQHjCAOgMaAIqgnOOyjdQOLxFsrTpdJtLFxyl7vI3/uoPsDlKswYlEAgJ3EICtHofEAaLysobNAaeGqxJi20H+CqTW5+n2v0mI1dTuji1ApmvXzVBQ4n6YfwZomcYH6d7/azSCYepH45a30gab9Tqjfb3tto4aI1JjYlP+z2WU4nFn0EH1GGTjfGnOhBUmQe853A4i+tL4vqyQJ4kkj3BOyzM+2q+P7hJvuMNXRpMGccrBYoC/vpf/TEK3SGEPYQ4cLcoh7H/Z6FZen/lYb5l67FWBoMBFoTZrJ7fY1eiCuMh7G6/w1/48e/jscfauxVWPA74fF2MMigSLXqZgF3HJl+l8a8g3MJsgoVp4iuN75UubFrER9fSsbyOTu/1v9PjNKvgroBJ9q8RSXy1nmGkSnOPAxcB1wGg9T3K/lqTZS8xSwbBhwbCO1uS/c3aXWhVBgV87ruH/Knv/ASjYUEza8AHmnpGOXRU9R4mvdl0tA3Obdg053ROWkKoMTGCKSolRVHiqxrxAQ0Njb/J089u8Bf+8veztZWsb0sDBMUzrw4egg3pT41fDK9bmlYvyWP8AJo3uL3zJk2zDToBqaMXSECSJxgjBrYEtpqmP+fq0PVQuXqgnzjP3TrENAGXdfvfbMxgcAnKjUXDu101whK5e29p5dt/OkDwqEOfxwGEB1mlonGCwN/6Gz/BxQslo6GjrvYoS8FCRQhNJEeWeQ7QxFJYVJEg2Zo7UUspgAqzZoaZUJZDnBQE7ylUUK0R2eEv/aXv57mPyLxSvc31JnCSVq8t9Zm2YVKjihR7tBPkJ/j6m+zsfY3Gv4/JLq6scNKAeFQlDVtt5lEEaeg380fFmqtD10PJ6fzRGSlt641DzCGh9eJjuDSCm8PCBYryCdALQEFouUT3YVxbY5Dvd/YE1wTU5x4h/Ik/eZE/80PfhYQ9tjZKCDWIUZTzcGi06PP1W6t7CUjhCGZ4jOChnjUMtaSUgDW3+Y5v/zA/+uc/x2CQcohph9U+RAXXn/YtvUfMBAIVSoVgMRRKDeE69exr7M2+CsUHiNvB6bSr/BTRNFtOesDnk+dpC4r2rOyno8oJLodXDz1HuHQPFmwhK9BQxDxuAkXpt98wAB6hGCS+UEpic7wdoNoLepZYltMMgocZ8z9qD7N/jP3vipx/aWkK/LW/9sM89eFNquomZjOsqXEalePitPI0CcCyxbIO26W2gJYDikIj2HgYqFJPbzEsp/zlv/iDfPhDBU7AaJbWQgJAWwWwrbJLw5nR2Ptn17HqZWb1y5i9gehNhD2CzRY+HxOkWyFhKS+Ubd410mZzRFpVpGQueYmul8JtBy+PcMUTOPcksT8wzuWyLmKw7AnqItxKJurInuAxAeFB4G1mNPWMwsEzz8Df+ps/yng0ZWMEdVUlS167vGAg5Qe6/q4cCz1JCQJ1Y0icS0SpkkYW1QyLCT/6Z7+LP/19n51TpKUYlQ8e58rkjfVuo7S/NrS5QmGIhHFqmdmF+kV2Jl/C1y9TFjdQdhGZoeJRia04wSsW4sTx3tFmYoY1jimYhJW7WVge/ZcAEIe3MYPBk6BPxPmBoUiRhVVfUSQgnH+OrMEUieOo7s0guCZAuPz5LRCWZdmNCP+z/7NP8mf+7Heyt/suly9u4Ku6s+rnt6ctkSaPYTrxRSMJaJSqmiJ4CmnA7/DEYwP+9t/+Kzx6OfpjwRtmglm00rUt+7TFUNj8X22uMBWxMAF7n6Z5gVn1Ak14n0FRoVIjEsPqKjHcZUHjxPG+9d81m56t8TmnvzrUun7ABU9wyXOPFHnxHyZKsBFmmxTFE8BlIlVa1BHLusZaFO2HXnNr1eGB4EHgcdhFJUe1sI9rw6wsjJFY/SUGRQGbY/hf/u2/yIeeHAK7FEVAgqG42BtogqpLRQ+2kKPIckIbJlGiDYoCpcHJLsJ1/nf/+V/jqSfn5QhOBZfu3QJdWqegOp+g+w/TWAhqgF6n2f0KO5MvoMU3GRY1voreohOJjqMHlRLVwXxaxL68X1S22Yg6HH308B+42L4wZxxqQdEw36AKTagJGCYls3qAKx+jHD8LtglW0h6Ok3J1yq/nVkr676wbMad6qO55Eu8D0OCbmueegf/t//p/QaHbqO1F76IocLTg5zELjMoBTeORXC1zgjvYcE4ITU2hyt7edYryNn/+z38XP/QDzzEomVd+HqCTWqVkCQits+fL+IQC3ITJ15nVX8XsDZAbSMr/aespWtsarfOSeLsDSfKy53GGIzxHGW57eGl792zeF9gtrwDWkq+nqk9RghWIXKIsngS2gA0Myc3vhxThyyB43FvAgyuioirUIQJ/7kc/xV/8C98H4QbCFCVQNxXe16hrCFbRNAGnw3wBT9ILNLBQMxo4QjPl0lbJM08P+U//059gPO7lcWx5ixU9UuO2daG10C1a6JbC5O4mNF9jb/oHVNVXMW5SYIhYNIxMkV67Q1swtcBhavO+wOgB5oni62FEzQFwTnWdjJbkqpt5HNYNzxVKmjACeYzR6BlgK86J7K017f5b+HOuIMgguOZX3BRVpZpWjAfwN//Gj/Cnv/9TeH8L3+wyLAXVuJxFhFlTUxRFbps4aSs+VEynN3E65eIl+M//i7/Bcx+JoSlR29+wZQdvudho0cQXBQ/sAt+kbr7CrH4ez1s4akQcrvt83T/Yd+GxnE/ue4ZnHwjXPWe4OFhZ0zDcOTUiEmJU23wajBzHJql7Eh18mDg6yS27P/cQ6Dwfo5RyOPQ0YKAGGt90CnI8GiDAE0/A/+Y/+0/49Lc+ig9XUZ0yLJW6juDnnCOEbM2fVIjFzDA8gufSxQK4yd/+W3+B7/7up3AKdT2LHl1vD3YGS8eIrT3lV2FM0fSZuAnIezSTLzOrv0zgdQq9jarirMS31VRd6AzEUtm71OlnDLEF6Tdj5y2+vgurZYcpFq0mCZ0n6K3E7CKlewrkCbqCmOQ5xh+xzHh/ZWnfI8xrIYPgmngSRkALWwDClpb7Wz894H/1n/0VHn9iwPbt9/BhRgiCb4SiUIJVOchxAgA4B55AaKYQdvnrP/mj/Nkf/TiDAfimwimYNV2+b8Fj7wNjmN/CGAo1YAK8B/41dveep25eReU2hfOoBUKIJNl+ubKwI8Vu5gwxy5a/na9hquucE+xSgGmBmChBlEDRTYY3SecgYOoIoUB1i8HwaeBS1BQLtHh2h6jD+d6vGQTXcBMGjFmYYoRUOg80EKLupKngh37oCf6T//kP8tyzjxJ8jdMhvoEQfG52XQMZDhyf+67P8pM/+X1cuJgaWERQ5xL5dQ39Jvbl6Ew34LRMHoABt6mbl9nZ+2Oq5g2Q2ziJTfNGnAkoanOybksLR/z8gV9qh+j5BX16riwnLrF4qkiN8S5ZTEoQUjFcwEJseTHc/5+9/4yW5Mrue8HfPudEZOY15VAoFICCqYJrAO272c02ZHuS3aQoiZQoieIauTHrrRk9vdGbNfM+z3qfZq2ZWWPWm9GbeRqJkuhEkaLIZhu2Y3sDNDxQMGVQDoXy12ZGxDlnz4dzIjLy3lvw3Q1zo1auvHVNZmSYvc/e+2+wbgfl8LqMCrW0WrEimnQYtZ8PX7pY+/a2fVf8TDZrEmm6KAp8E8AmzpcPkdEgjYZ+5x98gF/+5Q8yKCtK6ymcYPCdWPLs7ZQeLWp0CjjbqjnyZgkhcpXP92IB4MWOSf9nqdJqvR+FGiNj7r37Rv7FP/9VrskOEXXtsbYgeiEGnf37mX2aveOMtvouK8AZ6vpxVscPY8wFrJngTM8EVxRjzFR04aooz+2A9/qeCabKXGdmtC1QxmCinVYzEhFTEHURY/eA2Qs6j2I3U122o/jM+X+52yuyJfhJtBVe6mv+rC/kF9vPF9q/vrqDqmALSRB5ASsBJVBaR0T4x7/zAWJd85df+CGTtQIx8/gqIk4IMTIs5zBSsr6+jjGG0bBkbbJGYQ1RpbvRphykkIPvG/mOkc5hw/QlpST2xKZzUNGeeHCPThC8Yq0F61ENhAw8ElxnTioaKErDZLxK6SJl4VhfX+W9772d/+7/8Bvs3jm9cQqX3sNYC9Gm2YwkYINIAr1EzfqefTJ7W83pWSbrD1HX91PYYxSmQggEtZiOC+Y7a3rt0ISzLTAz0xKL3XdUtpPja3m/X+3+7rvHXPW1IMmcSSSK767m5BYhiJZAlUDChaEKgvd7uPbatwFzICXTl4+znYZ8ieuWyLmpuuhW+zfT8n+R+Ppq4t92Jbi95QCbVOJnv5mCpaBoqLDAoIR/8o8+ykc/fDfWrlKNLzIcpNA7N7dAVVUsLV9mOCoZDAsmk3XKsuyGUa1Rp+mtNN8EIelVtHrS57fWJpQlMqvtahI8PYSQvh8DzkSMqanq87zzHTfyL/+3v8Hea2Bhvo05SRWmc4RvCfAh8b06SMIGOTPVClgHzhHrJ5mMHyf6kxTuCiLjTu4sGapms+YsstDb4a0fW1bF29vrpQLRGSHzvHiT0JnmtgvloELQkkG5H3RvSoKbrmezocf606ue3iiLlp9IJfizUFp5o5fbmzpusEG9wdI29Z21mQ5RMDcH/+2//Czew9e/dh+wjrO7uXJ5hZ27FihHNePJEs655GweBNPun4SsFUhn0ZIeb+TpeY/wPeObRq/FZHofsUcobw+/DUmfRfNxVxAjCbFilFFRMlkf42OkLAtGA2X/9dfzz//F3+PGm2Fgc2XW8vR6XoBiyRD3gGp7jl17dyb2hIKYBngOXz/K2vqDVOEYVlbo+BVq0uJF2Sx/tb29wWPDBmurjd83Fo1CVEPUEYujG0F2petIfnLxqVWjeislzO1K8HXb8IOBE6rKY3KR8d/+y8/y2//ws5hiDd8ssXPXAkvL5wmhpiwtk8k6w7nRFFUmsxWTtMG3awu+kQ9QANOQgCAbUZF2w2PDZ5WIapqtqsZNN7VRGK+vMiiVhYWSplnhxgOL/Pf//f+SQwcdzkLy+/MomriBstUCh+lihJiNddu5bQNcAX2K1fH9jOsnKNxFBmVMFWS/6pUeoXo7Eb5JbvA2CU5BMUbbezYLolMQ4hCjOxgObwR2A8VPXCHmzZDgfmozwe3tNe7wdTOekL8VGQ4cChQ2t0b/8ftp/ITPf+HHXLryHAsLQwSom8BouEgdPOIMMaNO21lDG9zTDRazFuEbN4CkCiluWL+ZnOrN1qtuE2baJSmBxQyWsImTFdMxWpgrCX6N8dp5fuGj9/KP/9Gvce21UDgQmvT+vUpeBaK2vK4MwMDmZNcmsWI6DzYrEI+wtvYATfM4zp6mcBNMtlCSbKQ6rWptSt7byOA3SQJM165pK0Gpp4slAcQSY4GySGH3g22pEQ7Ns+XtbbsSfJMlwNk6ULOZqmqNECkceA9R4Z/+04/yO//wl9h7TUT9Moaa0PjkbBAiIfQCZYtC67dd3ugKyt3sy71AhbTh/zNVse8hBVtB8gB5hmclAOvUk/N8+pfew3/9z3+dO+4yOAchTlBtUqLqmZVqdoI3JmIk4Vd0JiH7BJARQCrgWdaWH2Jl7WFEnmNYVmicEJsNsnh9BGinELNtjfTGT4S6xbnNprkCAUWlRHQPo9FNwB4IC3A1y6SXewu9yQVHfyozwe3t1W69KqbnJtDOt0Q8ja8pXEHlx/gmMBztwIeIWMNv/dbb2H/9Av+P/9vvcfHiCrt3Xs+ly2MWFncxrqtuRRkzBD99Pc24b2zZNYPEwczniDPwuF71N3O8p7PEFqHXEhQkJiWW0ghF4bF2jc/95kf5X//zT1O6RJB3EhGTbYuCSXxASQkQAra3uDBGWtJKqwpKQvzVEE9RTX5IVT+I6Bkca8l9PpZoLEAcgs8UCDNjrmzETxP6dmv0ZxpkX33bcDNYqT3XQUhJUPZSDG8FXQCKbOOlr6PP8ObZtpPgzzoRbhXoc4QfuJKBg0BFaS11iDjr+NhHD7Bv97/g//s//Sn333eMxflrqepVnCkIWTQ5vYuZrY0k8sYGxmys/vrOCPGFmx3dYiPNXlTAIknBRzyu8AyHNf/0n/xtPvdrt+MMOBsxeEJocHYEsWeKK21LdMotDDGJGwig6pIajNTAGlHP48NTLK3ez8CdYuTGqHq0NjgzAlMQfIO4lxY0t7c3aiKMTIFwsxV/mucLyhDRfWBugDhKGdK8tjO7N2sifCWfSfR1Vh+/GXkom6qSLjibq+ekTJuYOTYkPlvMwMHTz8Hv/97X+OJf3cekGmHtItGU+AghNGAEY0y6idSlr3WSkkBWsRHJvwPEGF/nx1eQWAKSgQSeaMJMkmhCZFAO0RDxPlK6RDIPIQFoYowMBgOaylNYoSgC4/HzvOfdN/NP/umv8+53LWIMWdbY55vE9LiG/dwaMyp1mohD8DhbQjCpBHfLICcZ+/u4dPl7DMxxSrmAELAKEouOuxkloqaaVoK4TiHESJ4x6va69acZf17t/TDzehJBGqwtIA4IIaIaSbeoSf4i4qibW9g7/xsU8x+BcA3EOSiEEMGIvqp9+1nH19djfN++o17Hrb9pcNWuDqrGEwbDIXWt3Hid8C/+609y8NAB/vCPv8rJU+eJcchofhcrkwmjhXlCFFbW11hc2E3dNJSWLvG1Ek3tLNFa+waaGcRZFwVJbcKFhTlWltcpbMmoHLC+vo6zltFoiPcNahqaep3SWhq/yqCI/Prf+Ai/+Xd+kUOH2rquyZqegsQ8f9QNhWUGNCl0YAUBnHV0vH3jQS7h6ydZrx7A2iNYWUoAm+gyMrBf1YbuDWJ3/nNQ6JLfdlX4Rr+vY4xJK5G2xWkzS7hAdY6yuBZjrwd2pOvBtdXjm6UYeH1V6duV4M+0EtyiXbdVB7Af+KKZdlMFJhW4ARx+suFf/5s/5aEHj7O8CqPRNUw8rI0bdu3cjS1KVlaWKI3pKr426YUQuv+/vp0qpEO8qiSKRJTQJUAQqklgYX4n6pUQlGE5wNeTRCMZDQh+QjlQQr3CTbfs4rf+7mf4pV++hcEQoldK56cJUDM6cyaG5dli7CuyMOULdqe4Bncemge5svptqnAfzl2gUMFGOsEEqxEIxHZmKSmxRpkNnCba1GaVsJ0I30CV4eZKMN1rEgXVLOUmBR6Hj3MEdrMw/0FGg78JchuZyIrmDpCRV9fGfLNXgttJ8A2TCDdWfC+WBHsFYcZGVJMJg/khZODLuIa6hi//1WH+6I++zPPnK4zdgY8OHyLLa6vs3r0bvCEE7RKhMcnXUFXfAO3QbBuDosan5GFa3lVKgs4OqcdNSjBisvKL4ApAPdGvY1zNr372I/ydv/dh9l8HzoGzEPyYwtn8Hlu0QCFzFElAlpkkmPejUbAezAWYPMrS2nepwyMY+yzWrWPDKCU0NVnurela36kacFkphpkK14YMCDLNdhJ8AyfBFtzSdjVFLIrDa0kTdxK5hb17PobYT0Hch5omg6wST9DIGzu+bifB7ST4AklxA1BG2ZLX7uuAK/rVSciIx6RE2DRw/LjnP/7Hr/L1v/4xjS8RN6DyDUYKVEcIg5mk11aArXfh6zsJtjkvz/i6GUmae0pMib2wDg0Vdb2Ks4GijGic8PZ77+Rv/Non+chHdjOcy4h1GqzJ2q7aX5T0OJUqyb8t0xTaJNjyO5M6T+vmcBn8oywv/5Cqvh/rzlLaGiVALDDq0ueQiFDlNmhMVaAWXRJUiel11WBj0h3drgTfwEkQMJZOli+9lkOx1AzwYT+FfTe7rvkE8C6iX0Rcvt5azVzkDR1ft5PgdhJ86ZXh1Y5P+xyT+0T0AaXBOosPAWuHRIX1Cdz/48v88R9/icceP0ITwIcBVT3EuQWcc8myRbUDxrwR5oFdJZg99aZHLyfvoBAjGscMSsWYCbVf4rZD1/Oxj32QX/70e7nuukR+DxFK2yBEmtpTuCHd7KUnhxa7ox7zFDC7AWg/CQaQVWCZWD/Fytr3aZonsOYEpVlB1GR3AEskzQIla0dqpj8kOkuRk116PyFidArOubqDxPb2RkiC7axfOrstRwgWb4bEeCuL87/CYOHngZsIYYixrYBGEmGQV5kA3uxJMMbYxbPtJPh6znnaFhzKLMyfHITNll1RJRIJNKHBiU22TJpmhGIgeI8YJapgnaOJsLYGf/3NY/zhH36Bs+dqvO6iqqS7WFrNwLYi9N6/jo+/zCaDLgn2tEGjYoyncDUhLLF7j+HTn/4An/3sL3DgAFiB0qS/0JiOl3R9zS3I90KuNpt8wxRpP3ouDumKHSNyjqCPsbp2P5O1B3HmPENXYaJHQuplh6JOI91YQGeLk93EJaY0K9PPJJpoGtLyPPvV6vb2BkuCpruTTVYA0mgJ0RLdHMJd7Nz198G8C2QR1RJEMzwqKxJdZZH8UlwstivB7ST4uir8UqC7WhLcXBG2STDg84wALDYZrwboxkjagAEfPNaO8CHpW66vwzf++gJ/9Cdf5+zzyywtreBcibNDvA/4aHC2JMa+rJpuqlZFNitXREnSbFs9X63qnf6e2fR3/U+8KQl2FInQO3a5HSmBwirVZInr9s/zmU9/kF/+lfdx4EA6Bs6AxWMIgBCjYo1tu8qptFYzfeuWkJ+TYASMlvk40LUqYQJyEXiWS1e+QuBxiKdxrFKIwUYwUQkEQlFnIe0i0VbywY6ZCN/SIzYmwQ45qrbjf25vb6wkaNQgajEoYjwYJUSH1yFi94C8gx27fhvibWCGtOjjQEBUMT2Rh6u9x1s9Cb6y7tLPMAlu9dYv9SS+0oP1ak/CT+Yk9gizLybp0ucJ0W9jpsg9qyph6MRiNMX51XX43vfO81df+RaHnzjG8nIg+gJkHmNHhCj4mBwRjAWvDSKKdbnVIANipEORGrEpaUYlolgj7fgs6ZRmLdNWqkyzPqpomnMYLCqJUxc1gVnS55lSRFQVMSS5Ml/k/YhY5zFSEeMqImMKV3Pw4PV84uMf4BOfeDf792fKg/SpD1dZTeuU+6CyhR08kSBpkRLyUsURgQo4D+FxquoRlta+h7VnERljqdOcUkmmyKJgDBHTo0bMKt1sXDgY3dgyf2MnwI3gq/7Xr0U770UD3s8wSdhokNowdJZxWMGLpxguMmkWCNzGNbs/hrEfAz2QKDHSLpb9dFH0KivBn0T8fjnvuV0JvgYrrTdnEvzpXJAKNJl6tr4Ghw8v8b3vPMQDDzzNmTMr1LXFuDkmlQex2HJAJBBCQ5RErk+UgWRqq8Zmweip63lLtxCRBGWTlpgf0oOAMYLJrceUSw1GLMYk5KpzDtFAXdeAZrBOxHuPMwVIpLAR1XWaZpmdOwzvf/89fPAD9/Ce99zOjh2wMEofOGpD6fIcEZ3R/Xy551cF6qg4Y5Iplb+CkYtgTlCt3c/K8o8pR2cx5kqqyHtJS3K1H0LnMsiU6/JWeeZF5zVvZm1LGw3OGwoj1KwTrCXYEVW9D2vfxZ7dHwd5F+i1U1etLgmma2Y7Cb7JKsGfRRJ8s7duXjgJRmKncjJFQZ55Du677wkefvgIDz70NFeuTJhUFmvniVoQQ3JbwJokPG0TSk0xhBAy5SIlu7Is0ZjmjIHZNk2qBH2qLPPsM4Fz6NwXfKhxzmFIMmSqIc8uFaHG2nXq+gpzQ8cdd97Khz/8Xn7u/W/npgOG0Wg6dUF6XxMho2GNLV7lQW6/WEstUH2GZvVBxuNHUH0OwzIiFULad7JbBepS5WvjWzD59ZzuW9PnDRXgSz/+b9xKWHqVfbSBYIY0cTdBb2F+9EHm5j8M3ApxfvoHXRJsuz1my0XcGyUJvi7Py+thJvhKetrbSfAVNaMIVAkUgtBExcgAkWQQ1NRw6RI8/vgZfnzfEzz2+HFOnbrEuIKBHeLKOWrfEFFimFY51jqMOIxxeB/R2BI3pqv/tjoMIVEFNApilCLP5GL0hNjgnBBig5FAURhCrPC+oRw4FhYM97ztet77nrt43/vfxf79lsKlUZ7LccE3CfxibeZUKWiMaQVtetHolSbAECGugLsCnKBav5/lKw+gnGZ+rk4gmG7uGBOhHwcZ9Rm1QsUntKjEt9hzqmXadnn/mTyLvtrPp89m6jX8RnvOC6KgHrUGr3NU9T4Gg7eza+GjiLsXuA7iYAadnK/sXmdhOwm+6ZLgK5kJbCfBV58Qu5kbLfg/LT1Dsh5kbQ3OnPY8+eSzPPbIkxw5cprnz4/xjVD5kOY7uET4jUKMBtu6LWAQsfk8yQwdQzVZF6VKKSAScDYgpgaZELViOIRr9u5g//W7OXjoJt7+jru5646bWJxPIcAVieA+6yERZ+JG9LnxaDbTHl7Z5sFXOQEeYW3pPtbGj2DtWQo3xugE8THz//p1sEUYdK+h5q2aBGN2NombkgSqb9zk9jKeo01XhhqhqhfwzS3s2vlzLMx9FDgA7AIGL008YzsJvqIc8rpOgm+l1cdP8yRvrGY0JCNYYwAbkoivtPgZk9GHNv2uJmCJb2BlFZaW4Nw5z9FnT/Ds8ZOcO3ee5eVVxusNTeOpq0jTBKra433IFSNdW8w5RzlwDIoS68BaZTRXsmvHkPmFktvuOMD+66/h1lv2c+2+RRYWoRxM1785TXdzxhQXtKs0m6ahKMpEfI8yEzVCgFfXDfXABJqTrKw/wOrao0ROULo1rGmIvqFoz00m1UdcmkNm7c8WGPTWbdmHt+z9q5J6MWqEaCze70A4xDW738+geDfE3WDmmZF01t6zbCfB7SS4nQRf/ecPmysiVZ/Qnq16DNo5obdJUBWqhoQS7bEXGg+TCaytesbjirXVirpuGI/H1LUnRDoHdxEF8QwGJfOjOYbDIaPRiMUdjl07UguTmFCpolA3EGOu+lxn3NDRQVRb1Y1UAdZVleaJbaZTk0zo6VFIXtVl4wnNCstLz7BSPYFx53FmBfVrEAVnk0mvIRBbZRkKhCIhYDGUtnwLp0CX2u8UM9fwW+dejmkUIBGMAxYwZj/zC7cjHEizQNMyQtMcVXRDMjTbSfBN3Q7dToIvDyTwio5By1PsVXliZtstLYy9pVtE9RgxaIbWaDfv6z82fo7p6rf9f1Sw2T1qoyqnxpTwrE3UP2M256t2f2OY3W/fNBgLxtpe8otgEiFiI+/v1VWCDcoyQZ/DyBqGST6ofZeHLJ+W2ZzpZwUvBGx4a2wGKPNxaD31Nj4Lb36AUNsNGALzJLeIhSTKbmZvVKHnYbmdBLeT4Pb2ai/IvgHtFi2XTVdH/+/a+aFu+AUzQ96+Wq5p72PzAuFxelFe5QW2epMXmPNtnMm8Fiv5iAd8IrDnpDjlceU2cldy91F9PUm27Y23Kjo2iTEIsy4yBiiIOkU2S7foyr+TXUu2dRJ+Ev2J7e0ttvUT4cu/o2QDFCWJ+k6TTVutbZ2or/4z2fifTYkwTOvHq2VZ2Tpf9p9ffTIUoECZ6wUqnVYz0fUOT8wzQNm+1ba8Bt9am2w00ZbpAlOJ+eaQLa573dCu2d62k+D29opX37F/k21RcXW4jg0V4AxPaQNXS3LSenHwpfaS6AtUei9YK/Zl3WbfTTe8/suTJn/pKRA13WdOwaldUOj0OGXT3Rly8zag+TVIgOYN/dnV+HyNtu3fprdQKpAsy0enH5uts2RbM3Y7CW5vr2kYMrCh7xh7kTpeJfybrW9EfSlVZ/sa/UpyQ7uoNcfd8lmmDgrS9xBMr3U1tTnzimverV9NVGePQas1Sm/f1G4le/qWroLeHEnstd/0aleo9hNhnjPL9vH7iVTo2zPBN8nN9BJngropEOuWCU+6gLXVzRmvuirVGf3SOPMeqVIzV0+CL/CsKpl/t1VmMVsG2RRH4oaZ4KsIItr7SGajD2TsPezUkb4roFvAzFu5HDRv7Womo6yR3vqz35jRLb4vntlm/lsZWLVdCW5vr1kduDGRbAUdeZGbre98oS8Ed4kb3mOrhLX1s2YLUcWgrSP3pv01V01ur5ob/xKC2nQN0bpPaCaD2w3crpcogt0jjfNmpQ682RnxV3u+2sJqU/XX/57rJcPtbbsSfA0rpqsekDdo4HnpcOX4Ciqil5DwelQIeQFCbzezk6vHgv67tmlUO8XTlzlTlLhFAnqlK+mkdDOLAt34+afyX1O6RP+X3AvHSZ39fyti8ObKB/EtmQe7e0B068pYAdP0yka39apue9uuBN8UyXjL67oVt24BGC9QechVgr++8E1zVaumtk2z6e9mK8dU5ZjN7xs3vOWG/VPJsJgORrpx8TG7KxuniZ3vXrd6u8pH3MoFaQa88upaSdpVdfYqh1F6La1+Ap6aJUsvNZrefuZCEgS8TAWXhR7fMv962JDWNwqLoD3AkmxGyW58ZsP7bNy/mdffcOxFXuDyyzsUNiYDpmxB6V1joXe0XpgFY3r3y8t5vkoh3/1/eq3IVZ43vt7V9nOz40PcYg5v2PrKt1e9R7e313Z72REhyVXpi/68/3gtqpzX6rVaea2rPX5a2S92FU6GR+fntXqd0P40NFAHiBDr7MHjc0EC+NgkLbCYzVhFpsWHbnzLSIx+w470HtKLhFu2RVuDT6hinZjtTUNsqvS1QhIdzcFMI0F9fvlAow1REtW+Y+q3DHrtDsY0aTLLD24fKq38FJ0aDEDT+CkDPwTwmnyjokAUVLOmqSohTFNIjHHm/y92uwgWwRLbUxGn57OJgRhiinOBZDVIkQS0s/mxNumYeA3pV3wz1ToN2vkftnT7zne5PUQ6vXbq3qHzPk6TTndMZ581e0qqMjvCjLOXQstw9N2lpJsul9lfSuex5ZBWk8n0tXxyCalJOMf2TydEfFsJVWnGLBm9HEnnDJ8fQbrzOL0Ael/3HpIRuVd7+JAXcRhiULJlcbpeNHFeJ96n/YqSCn8MockJ6yrvS8z7mf8u+Nh93f48hOwmoiY/JBt8pSOnAqouP6T7ftdS/2mEp9cw1r5p26Ev1nZ7tU7Mr77d9wYoAWUavExvFZxTREqKjVK4AoLSaCQUBeNYMW8KFEOVo9eQpJ05BhyWUQCJycuvvT+nLujaVZqvpB3c+qv7MGEkJWIMFdDgcRhCDIhxOYkns6QieJwYogEfI4UpkD4AQNnIy5gmuV610Mm0BZBe/8ICa5MxdjjswokhUmBzxWXw3cpdGXaCcFdp176EdnJSo3HTukAjPkacdVigrmusLVArrMQKZwrqWDM0lgJLTXKWCEQGOBxJR6XfqZ7kzz5oFzQuJ0WfVHWCTNdEkhcmzrgtjTK0q7CmlajZorGgbF5D2dy6nMJ/zAZZgNjtR8gfIKAUuSHcWm41RBwGCRFnTU6Iyiin90oDtUDEIkQGGFw+b639l75wo/4l334ezwBHzJ8mxIZ5SQLnUVLLXnrqDnUMqLFEQl4AxbwonAK17IaK0mLQGNAARWFfVpz7Wca4t6Is5RumHSoib47ViUzbeZbUqlNiF6RsbqNMqjWstVROWCJwkTWcGeFZJSI0+TacQ4h4PMI8Q661AwZGcBvbkTGmm1vkVa0oLYAdMiZyMSwzsZY1GkpKMGlfaiocloLIdXaOeaDEIE2DlMUMnWFT+1U2w2f6W5F/UGlARTgf1xmXDSMKllhJ7vbYnDdsFnlL6bSIkT2hZFcx6qrA1uR1xgz4BaOEQcRAE7AinYeTMyk5TKInlo5lhPNcyRqRnmACZV4YTJhQMGLMOnOU7GMB7z0jNYhxoNkGasOxMb3Oro0KheBRJijP+WVsOegSkdmQKKZtRtNrQ24uwqd5OG75ezEv22YZp9pLoBFLmUTNick7owosmpKdxQjnYzp+JlWGE5QVnbAca8SWrLEOKEkCPaUZRQk9vPKrccICw5gJI0bd/bPHzKXEXze4oki/WDdQFngCY6NcYJUJPp/BmF1X4qZmdyRSIAyD4Rq7gDWSTLVype7cK1+E/jRj7Vtpc2+kg/lmODkzbf1uGT5dZ4emwbmC4cI860QuUPGvPv/7PHTmGKEQSlcSUSoizgjDIEiIGOe4tlzgf/9b/3OulXmkxVFKDmRRktHeq8zfkgPTqfoyv/vF/8zRlQsshQlqhEYj1lpElZEtWAyOv/MLn+HDN7+dPRjKYjCdLUn/WWcajpvmPzNjlLQqH9Pw2HPH+Pdf/DMuxwnBRLxG1IE4S90EnHO4BqwKxhj2ypD/5ld/m/kd+yiKorueNBvuFsVLsJjoBExzP9YaTD5/DZ7GwAqe//zw1/nGAz9ErWFpZYn5HYvExiN5v9QaTIRddshvfvxX+OhNb0PUdRnJbpEEuwVDzF5XCGM833jqfj7/w29xOU5SXSYRm8dPwaRjHPLr2JhEmW3vmAaZ/p5omkNajd3vxVx1BpNmZul3Y1fRG0A0ogITH3DDASEEbBD2xILr3Bz/4JO/yuL1tyBumJJ8rhifvnSKP/7q5zly5XmqUvA23QcumC7dBhMJkqyYXLyKrN5L3JrgwThUYJGSD99wF3/3E7/CHrtAWRbTFYFJPl2X63UevfAs/+Zrf8GK1pg4JRmF3O0E8n4ly7BFW3K9zvHf/M4/Y4cZYrAvmvxeT8XGW21z2wfyp71tJHrTa1EZxA4gNKxb5bJ4fve7f8Hv/eivqBdKJhKTDRKBoBErhkEUCMmiaL9b5JnLZ5nffSuCpdBUVkXAmjbKvbq9DyHQWMfJaom/evIBjq9dZDIALRMjL8aIU0GqyEJtmN+xyN0338l8hKG6GfBFZDMwoRt9bE1fxBPwKLU47nv+KF989kHGw9wOLAq8RqIzrPuawjlGjaEI4ILwrmtuIZS2q/761+vG71398yu2de1tP0cIiIUxDWMKvnX2Ef7t1/+C40vPE1BM4TBrDu89VqF0BVVT4wIMarjhwI186KZ7MpAmgY5s/xD0zlnUCBIwNrV5Kww/Ov00Xz7+MJdtjXGpUnFxc4JrOxApGfbyev6dtsKzEYo4myR9K12pJo3r8v+tpt837SzYWqIzTOqKYbDsWA3cMdzDP5j/21hjISSd1SpGKmN4ZnKBv3z6fs6ENdaLiNhUT4lmIEr2IfTGzCT3VxzwrMVay/q4Ysc6vP+WOyltOW2/t/1lKwSJ2HLEw6eP8tdHHmTFxqkJdD5m7XEoQjo2xhgGqw2fOnAPQztPgcFoqgJjfHH1s5/UOGl7+ykmwVd70n7WPfGfUjd0NurTm3upUhOZiOEbxx7k97/9RS4MPGMJhOyWbowhoFg8FYI2iiFQeMNTzx3nHbsPQvbYixsrmFdbxVrLOg2PnDnOqWqJlZFSFUp0mqlyQiEWjGILwzeffJBf+8RnuH7hNtop/xTNOEvdb3F5dquyWdKs1DrHGM8VGr73zCNcHkWqeYsPBq9NaptbhRK8RLwVhlXE+cj1N9/ArsVdqV2aATLOuZd13drCTBEquWfpfY3YgoDjkbXj/Nsv/ylPXDqF2TOXW4iBKo6JRnEIqjVmZChx1BXc/+yTrNMw8lCaQVog6QYpyRa9a8gcxMg41pyTVR489hTn4hr14rDzK7Qxpgpu4/Bvy5PaY3DHVP21SbKxOYF26B2dHYJlpE6bdD0+WYCU0LgClYLr9t/INbt255Oc+JRRIxM8D514mmebK9S7R6wbTwwhizGEXJWG/Dk0f58XlNh7sfMoGlE/QQrDfFlw7733YnMYnDSeUb4eVIU1rbgikW89fj+rA2XZeVzmx7Y4mGDTgtYFsKo4J1Rrnnve8y7ShNNgMZuBoW/AGd2bNTa/Lmv0twQyqRdYOjRhJoatO8Nj1XP8j1/4T5y1E8aLllAEMIFolWgVsRCM0lgllEptI+ta8dSzR0j4tymjLqa7iJcg0PkS6lihQXjg6SeYDAxmrkh0JhNBPRpqaj8mWMUPhGeXL/Dn3/06F1hPLbkWtp9bbbOgiw3BuRfwAlPavcGwxJgj505TO2HSTPAWZGCnwdJmPKUo1gjzYnjP7W9jlCAo3Q3dOt63X7+k69NA5VIlQE6kFXDUn+dPf/A1fnTySZoRrPh1xlSsVat4E4hDSz2yNCVUxrMiFWtD5cilM1zU1WScKMwIEczEfIXgfXd/lKbk4pXLXLh0kcFoiHMuVbU9k2FMQkYJgovCIKQqr3MrkDRbdgouKgNVClUsipjcGzXTjFwoFDpbphqRbB0sDNwA50oIik5qqDz33H4nc5RpUutTknbi8EQeO/4MTWFYoyY261NOSEbfSH6k3mu7w5sfLzbP7YCcZet0r1yzuMiNu/ZS5mpgWDi8T4u5aEGl5DmWOHrlHDJyxNJQF1AVUBcQCpJDVqH4Qmmcsu5rFnYscueh24hUabYYgKAv+xb8qaLW3yQx+ZXsp3kzfZg31GxQpuTovu7ystYsI/yf/+Bf88iVU1xxDcEkwLwZFoAmArVGNDYE9QQTiRYaA0dPnaCiJhA6FF9sq5ZXCYqJQEVDTeCJE0eY0FCHGg0+tQgFrLOgAWOFRgKya8SX7v8Ox5bOMmEDdbyXCHmBBDj9VvIJrGh45sJJzly5SLACgzTLU+9T4NbcVvSBGCM2CjsouOuGWxKsIQNirE2t0TYJvpSWaONzMpZkfqpGCVY4zxpfe+SH/KdvfYmwo0QHFikNXgJ253yunDw061AKOIGBpXKRpTjm1KWzTGhSAuxxAjumYT4e1pXEnJAiypnTz7Fy6QpDCsx6zahWRrUwaITSK7YRXJO+N1crwxpKn/kSGXIS0WQGHDNlJSbijm9BRSpIUAaNMvIw18CoEYYNDBthlJ+HjWBWJszXhjJaFkLBDim45+CdGBJ9AZuOd0PNCus8feoYOkyJieEANCIxJFECDenctDPQGLFeKRulbKR7HtT5uVGGjWHoYdgYBls8m3HDvCnZoyV37b2BHW7AEJOKTAVbCnVMn31M4PEzx7ni11mt1lENRImoiWnRZ/K5Eu1UgazATdddx56FReYpcdblgWrsOj36Ao/XU+J7q8Til90O/UmfnLdCD7wt/GIMlJLltWKgcYY1cfyfPv8/8sClZ1keRKI24EqIFh3XOCzi080WjZ3OeiQJTJ94/jSXWWEf1yCA94HC2Vd18bfnJIXLgqcuPcn51aXUAg2B+dGAtfUVykFBbBoGtiB4TyOGUDqWas/vf+Xz3P6b/xV7W/KCD2BtZzLkfQIEXa1qDh22DywF9z/xKKHMbvVVkwAqxkBQrLHEBqK6BIGvau695Z1cN787ofg2JLuXOg+E5HBfB2gPaU1kgvDoxeP82y/+CdWcYS1WabSXW9ChqaZMeOeSK7APMChQIhMNPHXqWX7umtvx0CF7wyZfjMS3A0MQWKPmiSefYmRLBmsNu12aA0sMaV6FUlvBimHYmARyIRAXh6yNr8COuW4GGSWkagUoyiJx5UShLKCOyHrNnmIeO24wCo2RJGWngqAUGSEStECXPHNiGWG5YbSLG6+5DoNLvEACiGVC5KGnnmDd1zRFykBGLE4DGgJYk4kRKSFEjdgG5rxjqIamCQwGBU0TsJKWe6lgncroRZnOMNv1vkEpfGBndHzwxrtYiEUqLrXHz9TYtWoePnaYK80axgpDMTRW8KFOSc1mb8CoSDQMoqFo4Obd13JgeG0Krj4mkE1h20P9kullr5um1RsoJr+Sfd1WjPkpJz8fPMamw+5Mni8Fj5aGZQJfPPYDvvHMQ5yul5A9c7BWw6RKN3pQxLTc25hbPLGrqhTLqjacvHCWg3t3McJQGJuqwdg6R7xItdejDWy8oBRYp+bp088yjlWqFLITvZGEOiytY1zXOFsSjdBYWJKKJy6e4qELT/PRvXfjNOJcTv5VA4OCgSum7dCrGOS2CMV1PIdPHmfJT1BXYFxBjKlq0BiRmAKqwSa3eaMcuvFW9tiFV4ULUhIpvXTps6pEKhM5Ul3k//kf/x0XZULllJB5mdpvbep0hpYWLCZLsAmBwJlL55kQmKfIVHzTVd9ui5myAeYY8ZkPf5w77r2bsDCgwVN4xagSUJoM3DDAyAtCpCnh8OXT/P++9QUuMwHvM03HpEQo4OsxjOZyJo7QwM3DPfyjz/4tbpzbg9Upf72blcZ2QeGY1BVFUVCqZS4a7hjcSJGTujGJM9jgeOzUM6znTgYaiE0SjBCRjEiVjGpVrAoDDx+749188r0fZjgcJqOhpkEUCus6mstWXZe2l2ENDNVgl2vuveEQi2aYhQRSQg8KxiZyxjoTDj/7DN4mqbOmbtJ5kwyMMlPChlVwTWTeW95+0yHmcOmkO5uOoTFdO397e4NXgtvbq1yp2JJJUzEsBmlqFz2UhlUiD68c53e/8XmOVZfSvKGpwRU4QGqPtZY6t1G1T5jSFuGnjI3nqedO8PN778yo0PwrUV8yRWLjALxdoaqkBPTQM49nnl6rXBNQa2hEiGJSC62wxJBhIRaeunSarz38Q979ybsoTOZDKimQBEWczNY9snU7tsFwXld48uxJKgsWxVpLDA1KSnhB0xxQY1rZl2K4/fY7KfI88NUtZEJq6wWoQ8XqQPj3X/tzHjp3nGpnkaoIyfyxDdY3omBiyAwLQ/QJBuQ18szpE4wJ3eyzPRYba/iyXSkExRXCu/bfyl3X34RlQEPDHBabeaetqEvytU/t0wvULE/WCcsTKOp0gbhMpzG5PVzXqWL1Hhpl1Bjetuc6fvttv8yODCOJnT7MbII2WBoaBhQIBotSYnAoGiJqDWMCKyg/euYJ1k1EC9vJ2ASNiLOEdvbXHkgVygi/ePO9/P2DH0EQAh6Xj5DD4PGZyr/5upl2YDxDLILPc8qk3mNzae+yIk9N5FJ1haMnnkVLgzEC3hOCgjMYLDFonpKDCeCisIuC9992NwVCjCFVfzESxXSgr+1EuJ0E3/JbURQoShMCzhku+lXWXMHvffXPeei5I0wWBYoSmgorBaUITWwwzqGEaQLcQIgPBioLT589RfUOj1KgmpU7zEubB7Yzsq2SYgCWqXj85DNEk3D8EiUrTqXVcwwBGQwxxkE9Tuz2oWVlpeJ7Tz3KE+97lvfuvA0IzIlNK2rVzI2TKVRdNgdYzcHpqVPPsjRexe4oiU2NjTajKQVjLCEEjKTkQoCFYsTtN95C0c6lXs25c0VmSgaageVPfvxXfPHhH7A+J6xLk0jkunmR0p6jdnxkRVIQtYCxPPv8GVZo2Mesqk8HHGp106RX3jTKzqJkkOe/imWoklAdMuUHCoLVhJa9RMOpo8dp6hoZDZLjkyoaQ3p2+ZyEAE0DWrIDyzv3H+JaCuYwbY3TKaf0IU2qipEy0xkDhRSEGHBiUes66bSzXObIudPEUWqRWrFpdg2JJhECmCLNAlUQr+w0I95/813swaXlQjCU1nWqPVbKmWu3T8XpnwaLQaJN17Rql5xiCFhrabRhIp6nTx1jramII0E10zdMWoHGPKNEBBGHMQaHcGDHtdy6sD/p3iQl9Y6jeXXF2e3tZ7ltG1P9tLegGTYtNOpZxdO4Eb//48/zlUe/RzMvxHoMGjHlkNA0TCY1xhbUMaIiW9qKSZZ3HJvI8QtnWcMnz+o8kH858lIb1XmmM0E4ufo8p5bPJ1J627aKMUPfU2Y2RUlV1/n/BkxCih6/cpbP/+CbrBKpEWrNRqEiCRrPrGpJvw/ZftQxngcefzTNixRiaBCNOGNxMUJUTFBMVIoIc164Y9+NHHL7mMuKPK9ui6yFmjVn+c6pJ/gP3/pLnrcT1iQQo59pEaZ97ymfqunmuGp6lbYIF9aWOb9+pRN460uTdZrjPb5fliNNrcCY/lswPQdEi0SLixab9yFgWA01h48exRVFAgcpEEJaHhib5nFtFRhT63ABx8/deS+OhjJD/h2Govco86OIpBowCqWkGa/tz64zZ+7w8WdYa6p0rdWeMkDhFad2qhMak26ZCYZhBQcW9nLT3usRhSEFIzvAqkmcwkYhGkRt9zD54fKjUEsZS2x0+Q60iLFYYzt8C0SiRNYJ/Oipx9DSUsfETW2MwdgizfiMBeNSAhSbZOxUefutt7NIkZWV8lwzjz+2g+12EtxuhQIStQvExpWsAT+8+Dh/8LUvsFxGJrGBYQkhEqsqJZcitYdC35KhnbK3dIOcOWqjnFm6yHIcJ6pERlm0OsovK9xnykCLVqtpeOToYZZ9lQjYrdBuW2WKgSLNZqhqnCswxkLT0DhhlZrvPHIfR6uzNJhENNa45exto2Bw6yGxRs3jzz4DRaqOsZlDZpOAtaqm+ElEgjKH5Z233sECBaXqq06CjW8IFk6xyv/rS3/M8WqJZdtg5odgbaeXvFkUXLrk57OeZoL5pucqKifOnknnNeoUcJj/XpnO4XzrMt62qUMkaCBoyIoxPXHyLG4dFcbAySvnOXb+NL4wBF8RfIOJib8o7QzQ2sQXNA5pAnOu5Pabb2WAnapwx9QFkKhJjSi/l8lALw396jDNUskVsCfw4FNP4EcF6kzqHrQLOck2yEIWZDc4FQbecPcth5i3Q+q6np6PuoUKSU+Q/QUeEaiUGGVWQEBTtyTWFYJhhYaHnn2S4DJg1lrUSBJMb+8LkyT0Qgg0UdEA773rHQxztWmxaBRMVm2ymlRltrftJPjW3ZQEcIkpmFYo51nn//4H/5rLUlE7wdcTFgYDRCNMJlBYzGCAz1WViWYGOy+ZqNxRDZzl4soSF5avZOqhdOhKlRenKV2NnBtjZKwTHnr6MKFMJreiERMFETtNzGKhCRgsRRCk8TlvRxonnF1b4qv3fY8qNcFS5dQTANjo/tdRA3LsPTO+yPGl55kUiroIQ0cjST0VCUQTwSRNTYwyNI53HLqTgoh4fdVUycIVVBj+9Xc/z3cvHud84fESCMFnAn2me6rBRYMLqSqS3B9Vk8q3IJrRhenYqjMcO3UizwK3WJD0nCV8jFMbco1JoFksUSwNgjeZgtHj0dUG1oFHzhznsvXE+RIKi0UYiAXfpEWXaEL/q1AmWwNuuOEGdox2pFSTiIX5IVPuXgrz7cXSXWtNnoEWpjVIFq7EdR49eZTJwDAxCoUhWCGIwdskYk1hO26gwzKk4O133I2jxA0GScgdRW1OZqVLf2c3cwi198ACQ8G7pF0qrftJE3IiTOLexyfnOXb5HF5jd21i7Oy9J9kJIiiIZTQacceBW5hLkgipkm8BsT4zKnQ7DP5EQ+ybiSf4Os5jM+Tlq/7CFpZAXUe0alBXcJF1fu+7f8Gx8WWeq5aobWQwP8fqygrqPWZ+AXzE52SoPX3Htj8mOjXmVJJu5spklUvLS0xouqqqq+q4yn5txcuTnvVN9Cz5CcfPP4fOlak61FTZmJYvkFAyqY02HNLEQKhrKAowhrpQ1gfwpR99g2f1eSoiMSuIkAnXmw5tb7amwJNnT3JmfJmJ1hlpYnI1mekHrTKOEawx7DYD7tl/C0UaeG59Dl/gfPX/H4isEPjec4/xh9/6EsuDSHARSpfkwJzrylbt9amNMpva+4Z/uXrzA+HUpfPZ8WLDQci/b3JgdW3rWSTNzrpGbZxtJ+fyqiFSE6jw/PjYEyxRUYU66bBqQmyKtbiyyNdWFg/wyoIU3HPLHcxRYjCbi6u+NRaJMqMC1rk0k+3tSwhKRcOF9RVOXH6esfpkB2ayFJvotN1r7XSOKsI8jtv3HwCaRLGJGUTlLCFzGmcWbjIFBoX+XFCnjl/tNW6sTDsP1rLOhKPnTnHZr1FJWqAkuTcwNl+QksyZWqPoRTdg3/xO9s3tTHAbjWk5s6kFs50F3/SV4Kv1otIXIZP+rBPgjM/bhuDZkWJjIm4n2xbNN6HmQbrCKCXAb594lN//zpc4I+vEuZJaPVVoclsxmSRJHXBqkNKCDaiJqQqLrYqUwRtDbQ11bgkWgwGHjz3T2b1FlKGzCa2fI0JIEJuEI9QwJUn7qSOAimEt1Gm26ByHn3uW45fPUaHEoBgV1AohgyokKC5ECknCzt6REmArOmxhbRR5ZvUMf/7dL7NOYJ1m2mKrYmeY08SQNCvzAxHWqPne4w/h58uMwrRQhaTsn+dIpoaBOkDwqxXv3n8btxa7M0fEdI4Kna9eYOrRqMlWrnV3nCGrE1nH80hzlv/rn/8HrsiYiiqVqEYSyTt4xBRQe9QavEtB2seKojQQG5jUGDdMxzzrmzY0jF3k5Mp5ng/LNGbKjQuSrHyMj7igOMCZaYIga4UaEkKSmKS6Gp+RiSbRMQThSrzC488eoVicSzM/JSGOY0MtkUY0VfWQrK/qhj2UfODg25jHYVVnfJhSHs4Vl83lemEJMWZBAYMY27KAwAoNhh8dfoQ19cQYcNmRo6WyECNibCJj2oRD9SFww569HNy1n0UKXIRSbHZuSH4WnUNKr5WgvQRotQUlKdbAgGRfpWTLwsISs/2YR7jviUeIwyJVqKHBDYbgfVpMBo9RzV2dpIIzGHs+cte72c18WqzkY557wNNC2cgLzuJ/GpXSzyK+vp49Crcrwddw3tdf6IlNN3+j2dwmpqolWlgn8sz6OX7va3/OkvFJggmPLUvEFunmF0tsIouDEcZH9MpSasfkN+gUyGTWuxOUoJELVy4xZpJB99PZIVdbk3aKJClR+Iyacza5uq1S8fjxI1SWrPTfrzbo/HhtXx9cNlc0E61ohobvPPEAx8JZGnKbqklJyrWWp8YSMv4woDTAUj3m5MVzrKtPfaYQ0zwri1sKMCyGxMajTWS+GPGOm29jHoszJa1Xq17t5OV8FjW3KNE0AyQyJrBMwx9966944vIpxiYiVlJy9Q1iDFYM2jQwGqXF0HqNryqKwYB6fR3rHBiL6ZkPK4nbVovn4niV5WqNhqarBzXPnoyVRATXDWVyj1FigTLzQtuA6jXiCTR4jj93ijU/odE4neG29aP02pukHD0/GHLtaCcHFvckf0BpJ7MvElRaeyoNHeIziZ/DMjWHTx+nsgEGhhg9ztlEQFdNAgp5vpja18IAw9033MKezL7bWk3o6pX9dChO5m36mfsh9hJmBVxuVjl6+iST0BBM4vol4YPWxyr/nW8AmCuH7Iglb7/pUHbUMJtDq2zXgNszwTdJoutNPrZ2e+6AgGnAbyQhElsLljHKKZb4T9/+Kx46+XRyPyAFuKQLGdPqvvFJeaUOlKZgNFzAhpdwJ+WV+bkL5xmHOseDbE+0xdmOWzRoDAnYkNCJSR5smTGPHD3ctatCB/2ejUjBZE3NrhuoSK9/ZqJinOWRU0f5zuGHaXKqi+K7q1FzGy2EkIsOSfPTlSucOXc2fd+mBUHsk7ZF8JIqXImwoxxx9213dB3V2FtAtJVBN9/K51I0K7bEpFYycJaJ1qxi+eLhH/LVH3+fK5P1BIlvk1L+XR9DB9Mt1z173Bw7hvN4n9h6hiTN1mmUZhSutSnILi0tsby+RmI/bqUYoFfpUPQpFWmOZ/O+uQ5S5Hj06FOMq0kCLmWZuNiq8eTrUzXz/2KkFMst+65n38Kemfc3L9BWb9urJi/8rEyvvYbIhbjCoyeO0KS+bkrBRaK1YNIxiiFJuNmgDBpYaIT3HryLubZPkM37uq7MC9yv5kWCnGz4PAE4c/kcz51/fqpCRNJsTUAY6ZDQTe6AFGKYMwV33HRrJw4/ExpeR1iYvizbVo83fIx+BZ9hOwm+3IOsU9H9TfbdbSDWkHUXk5C1AaIPNKKsAF9+/Ed8/r5v4hcGeEmDd6eS9QUFmoZBOSBOamwTuXHHNfzKBz9GOfadZc0LXwjKxaUrjJuqC6YzgUpeYEKRv+EkC3CHQJPnOCcvnesSnMImOkD7vbgB1WmzpnXStU6BdzIwfO2h73NifI6KiAyKzYvnLAYN0CA8dvIZliarSNHaIRmk5TXGpDRSNzWuKBli2b+wixt2X4vNH7hNPqZXBfisqhI7W6RsJptX/QHwYnlo/Tj/7mt/wVm/gl0YpRcJsfP1S5JtJIm7cc01seBzP/dR9u3YgzYRVw5ToGcqp0YW7W4D0NpkzIUrl7KRbM9zsStr9Crth9lgn3idbXWeEktF5OEjT1LHMCMU3jlEtDNeaaXHBOrI3QdvZydzacGhuuW6b2Pbq921wtrObFY1jQVOXHyesysXk0pMTNJsIc90jckVVyehllChu82Au286iCEQMdN29tWymrxYu2b2uLWL2wiMiTx+4ihrvqYcDLpri6xG0+5fzHZaYgxUkQN7ruWGwd6Oh/rqGamv7xbidiX4Vt2u1k7st2YyMrENZBCJGpHCURF5rDrB733z85wzE64wSQLQREIIlFk7U1xBqBuG6phvLH/rY5/mQ3e9gwXcjA9cm5RnTWcjAWW5WufS2sp0thX1qoFgJilq29LKbcmsNPLM+TOcr9cIkgJWMJq0Izd8/iibA1FHEM/J0GvEz5c8dPooX3/4PsZEoiiqvtNWlDz7ij4QiKxSc/+Tj+EzJSJmYIlYQ8jWRiLJOFicpfDKHdffxJ5yIXm65dfrLyL6wJ/OHNUmrVExieO1HCYs4fk3f/VfePDiSdbKSBgaquCTcWzMx0jT/M+qMKiFDx68l9/66OdYkAEDW6Zxl0+LnH4S1Gx5lEQKAqfOPpflz6dAj84PRIQXzUK9dmQ7o47AiXiex08fTQ4kKStNVV9MRvWSIP8iQpm1P999210MEyTmBTVW+0CT1qS4nYppJpUHhMefeRIvqSOAb+kYUxUAycdFXCKzFwEO7L6WA3uumyZ5ro5Ja0u79ty257fNdrrFotbmVqkSuULND59+jMYmDdEQQmbY2+ntk9G5Yi2FsbigvO/2u9nNMAsbyHZg3U6Cb/5EqFdpJbZBqI1VMUaiCI01nGONf/+VP+eJCyep55LDQCygih7nXJJkEkm8qzoyCsIHbrubX73jY1znFtllRtgoL1qqqsAk1py7cmnaLrpKm6DDEbRx1kyBfgksJ3jg4WNPcrkZzzjCb6Q86RYJcKvKVa0hEFiWmq88+H2e0+VuCtbKhSWgR27ZYTgfVjhy/hTB5qTufdcOVU1BNgowKKiqCjfxvPOWO9nBgEJMR8afmaVuDOI5qYumo1ITCLbk849/i689fj+TOcMET01EjSLOpkAfhRCSZJdbC9xY7OQ3fuGXOMh+rhvuxDZgxXbtz2mkbluQed8Ky5kL59LrZ7iH0qsGYtxU1XSjLqaiMtpbECHJJ+KJ545zoVknuulsD9XM5Zcs4adoCMlnMRpu2L2XW665nrLrJ5gX9fMzkhYP2romxKyVamBCzQNPPU5jItqhLe20FZttrZK1ryYFl6DcdfNB5pnDZZG0/sPlRyfDd5XbI/SuWe3NUvvHMwDnWeOp508RnKHyaU5prMU6l9q0xoBxiQaiCRSzYAredesdlFnwO24sPF9Bl3FG+Wa7GtxOgq+vLQXImAPVppsthk7z0QLOJJL7Cp4vPvhdvvbQ92nmLFVskjWSyc2+0hGaJulgrq6xZ7TA9aPd/JNf/g32M2RvscC8HU4FlHXrBGOdw8dITeS5i+dpWnErs7l99kKJvp3ptKLZPz7yOJNSiaL5No8zvndXu9mj9CougWha0n9AhwWHL57irx7+AWMCYgyhbae1AdQkB/UjZ09wcbxCk2d+XQkcEy9QTJH4Wrn6uXa4wNtvOkSZ24EbZ5TtobPaznjz2fQBUyYQzQTlkckpfv+vv8iS8TSt8Rwx6Udam5QjM0jHBcO1jPjcuz/Chw+8C4fnll3XUtQ5YDqX242Z2N0pvEDIakBnr5xnkqj+veI+yZPpS4BWxJ5Qdzu7qgg8dPxpmnmLF50uitpZrWy6iHFN5B0H72AnA4o8G95Yhs1OBUxOXrmNbfNx0jQn9USeWz/P8fOnqcXnXUjHDUlVpsbYaSeE/MXIOO65/c5uUWQVJEwfNiQwlsTcms4LmP56TF5sUdu2xoGnzp3kbL1MKHJr2OReSDfXNgnglI+xDcr+xd3ctvd6yvy+2l5x27lrOwm+OYeCL3xXtRqChJBcsREmKE+sneE/fvNLrJaRSagoS5ermQQIaGLAjYZogGE5j1yZ8On3fJCfu+ZOdmJZCIY984svOhMUSQCNMYHnly9RUyf7nJlCQl/wc7XKW0EjY/UshzFHzp1B59yUp/hC7eJemdwmvw7FagXvkzC4jw2rReAv7/smqxkTKT3ERdM0iaNI5KnTx7hcr1MRMwnaTAO5CNZmtf7GM3QDbtp1HTfv3pfCcqYC9BfUsnFm2d4MVkAiq3HCEg1/8PW/5LGLJ2nKnCadzcFWaYInaMwqKYaRNxya28tvf+Jz7KJkJ0N2lwvMuSHVJNk9tQawLSVFjElOHBrwolxYXWLcwYWmOqOQEMcvvOgwUwR+zOqeGljSNZ48fYyJS9qrfSBRmwg1z/uMczgcwyjcfeA2BoDL+icvxXQ45oQhbe8xo4zX8Tx74TmWdcLYpnZ64iJW02PSe1hJuqAL5ZBbbzjQLTy3Sl5d5pTNbc9+1ShXn2wAyabqsZNHueIn1EkyJ+m7Nz5VtsYQ6rprCxssczLgpj372Te/i1HXeH/RScr29mZOgq8WZfSzRi+9IGdGUruujzKcYQ7n+U0buJLUWOQsy/xPX/xPPLF0hsZGisGAWDUJsCGpkogmzcpMUIYNvOfAbfz2x36VXVjmgT3DRUbFaFMCEmKvAQO+aSiHA4IVjp893VsT55W5tDO31Gxrv2qDj2/CdBVrLZVEvvvIfTQlSdJts6bLbOKrAgNTYj1YNWgImEEBvk4UBY0ZBp90PqtSeeTMUb5++Ies4bOBbNrPYlBmZZDIQ888wVhrcPRmkTqtpEJMHLcolFG49+ZDzKvNxGU6bnoTfPb4a5DYOzoxdu20mkBtSv70gW/whYe+S7OjyHzKTCYkUyOsQQYDmmrCUAp2esv/4lf/LgftXuaxDBGuXdiJacI0yDMLjGmvs9RmUyoNnFk+37WxBUHjtH05S6Dv38QbbmWblWms4fz6Mk+efpbKKGo3oo9sJ7YASUS6xDBq4H133sMiA2jCjMXWVovAqcWTw4ibrrisoUGpUR469hTn44T1QvGFJIUXl8S+QwYIEZMOrEFwEW64Zh97hzsxJAsmL4FoA5ofmPQcTcBLoCHgiaw3YyAmPl/bmvUB0XwNAnVVpcPqY+fP+NcPfJ9y9yIT38yoJ2kP0RtjhKJIwLXK87677mU0I/Ww1eDyKvfNS4yDb/ja4acYv7crwdfwpG15kOUlXMptMNWGCZG/fuw+/vqpB5ksFGmFmaHjplVYaUulCAsyYId3/MPP/Do3sRupaoYUzDFMIIyXQJGIEplIZLlZp0pU99QimvlMccsLwOZqQwUalHUCT545wTg0tDpQ9irHRhSG4ljEMWiU0ivGFUk1Zm7UljV57qlYgbE2rJeRP/veVzkdl6nzfiodh51z8QpPnzmOL6aK/Fd7f8RSNsI7b7ub3bKAJcnUtcWCLRxJaMR2BqwaY1rpixLw1CI8snaCP/neV7hsaybiiRIpbQEhYk2SKbMIWlUMB/OUyw2ffc9HeO8tb2MHjkIjhsi+XdcwVwySyksOultWULl6WW4mLFfjDFrZIGG3hb9jl8T7s+pcQqpAReTpMydY9RUx1qjoZn5ovzKOUKpw85597Lc7kxS0KTBiZippvUpIbxMMxkAWjAgoYyI/PnKYysXkXLFFr1Jztauq+Kqmqire+e534JijAlaJLBG5TOSSRC5L5IpEllCuoCyhXKBhGQhFQd0uWI3pTJz7x7NVOpLCsE7DBVY4v77Mih+nma8x0zZwXoQYl7Vx64Y5V7K7GHHb/puYZ9jN0a9WBsbtivBnFrdfaNu2Uno5CbB3IUvX+Jh67bWwdM13wzqRZ+tL/Lsv/SnPxzUmJhGBbVSsbaH9Ai71cGxUwsoan/3gZ/iFA+9kJ5bSDlGSav6uhUW4+AIJoE3SqjQCl8errPkxwY0y6lI7NEt/eN8325Vkm4YtUvC6QsWjzz6Dl5AlyrRr02krk6NpCloqLEbLh9/2Ho489TRnVy5S4WHkOtmphLyJieju0uv5wvCD44f50YnD3HTrR1EEL62vGxx//hTPLV8g7CmTNqj0MkA/gSsUAUYe3nHwDgZIrmBsL0kItW8oNVUgkQDO4LKazLoELhH43a/9Fx67fIrxKL3GwBb4rK2JSR55vmpSVTuuuWPHfn7j5z/JftmZnBZUMOpZnF9IAtFKEkFQ0xkhd92FHDyjwPJklQtrVwjXplmgNRZrZFP5p1dfexGzQEkQYY3Ij488Tk0SGNAteoLttWNyQ8N55R23HWJPy8zL1W+v8N4yyEvrCNK2cK0hiuKB51nhidNHCfMbW+dmZlGmMaDWYQqHUWVile+de5BdoaSUgonVbiFkABfoZtOtwbMLhrlGWAgFHzyUBK2DSOJ/9hKSWNN5ca4TeOrMsyyN1/CjkCvpiGparIZWi1QEmkS2H6iwf24nt+27Id1DmtC7snE0sK2Z/bretpPgy1xJaK+Eli2CUdRIMMntYIzhd7/4Jzx96Qxx3xxRamwmxRtNA3ejgqhFG2UuOg7t2MPf++TnWMxJBTFUvgEn7Nu9FzmeFXmv3g9IP7XKcjVmaW0Fdu656niz/zmmSTwQsXjg1Noljl16njgSrIAPIQVGmf1ryQloMVr+5oc+zuM79/EHX/gvuKEhWEcY1ynZy9RiIUawztKEQLVQ8oX7vsNHbn0XA3YSNBmRrtPw6NGnYVTSSNxcxcwgUQ3z0XHzzmu4rtiDoEQMRmQmDqXWpOloAoKgIjREKhxffep7fP7H3yXsnSOG9XxQYueYEUOgUANVYMfCPGVd8zc//nHevfP25AofFdTirGVYJI87CRHnCnzTX37MXj3BwDh6Lq4s0SIki955YVMdn+H9vfyovZPboFxknUefPZL0L4tiuhjRKXWlX42W4hhE4X2335uSR+MxUmRzCZlJIltfU0lGr02CgcgaDU+cOco6YeqwoZsXb62KTYwBIkxi5A+/+Of8WRVZwBEDrJiAt+1aJOI27IhzDtYa9tsF3n/zXRy68SauHeygcDZTKCySYW2toEDI1l4/OvwIjWivbR0JIbWBp7Pb1Fot7RBd8dx5yy3sdYtpXilu61UJL8/KbHt7de3Ql1sNbrdDX26STLofM99Rkc4iRYxhnUBNyZ8//m3+4v5v4fcMmUgCRqgVQotwVIMVh6kDbtywlxG/88lf467iBhY12dlEHxCX5g2LCwstm2qLHWvFRBNFwlthHBsuLy9NF6Vb9Ms3FgZpZGc7QMpjp45yJYxTW8snp4S4BTJGomK9sL/YwTtHh/jc+36Bmxf2MKeWOKlTaYnrwCAUFh8bYvR4POOh8L1nHuPbhx9inaRb2hBZpuKBZw4ThoZa2yAap7PRXkA1ERbU8t477mGOIhEMJFsYtQjA4BPAxKTWrMtIv5rAmMjh+jn+/dc+z8pIWGrGGYBjaJqGwWCQIP25NTgQS7lS8dFD9/K3PvQJ5oEBLgNlFIulGA7YubgDEwJxXE/bjltwThWIznDhymXYyMvb8vfjTF9yajqfHBYahOPLz3PyyvPpp20C7J34tsXeLe4i7ByMeMfBOyjIc0BrXvKspZ2Zt6o/CRTTcP9Tj6ODZIIoMVVXru+A0kqNtWLsVhjsXuCSjrlQNlycV86UFZcWlIvzyoWF6eNiflxYUM6PlCsLhsvSsO+2mxkNdiJpadIz1+3N/AUqPGOUh54+TJKdbTNXtrTK11CrP4sxDF2BTJruWtOeS4vZatW83Qp93W7bSfDlLTNmVLa22nz2rj4WzvFvv/SnrMxblsiOB5oxfyLdPEpEUB/ZyYB33XCQX37nz7MTlyTSAFMkCa0BBTtG8zMghqtVsqoRNcm099KVy1smwY1ai22QaFWhIrBGxcNHn8IPcmAKMQM60kysg/pnmZgywt03HGQPQw6yl89+8GO4BkyTrGRsC04QSYkw8TBwg5K19RVWTMMXf/hNnmelgx5dbFY5cu40a7FJ88TemlpmqolkEFt65T133Jth/dnvrQ16CqV1ufKb8hx9rBGEZRr+8Gtf4OFzJ2ChhFhlFkNqgfrYdBSHgDJvBlwTBvzdj/wSB8w12BiZxtDcFrSW+fn5rMCjV28ttN00azh3+WIuIvSq0TP2VaI3vJRkPowHnjr9LCu+Sp+/CTMJUHILtC/AEJqGG/dez43ze3EhYqzNFZBsvetb0XSMzXSX5IkxpuGpE8cYZ5TyTADaqGmb+Z94z9rqKnFUMBkYlmxgdRCpXCS6QHQR75RJoUycMi6USams2YZ6IDQl3HH32zqySWh8Ng/e3PEJwBl/npOXziWbqhg6fuNUeECng2VVQtWwYEvefugOysxU9FdNc5GXC4zZ3n56M8HtJPgyyuw24Ipu0dLJXzcaWKHiP3zxzzi2dp61oRJKk/hSml0kXDrsMSohw/sP7r2ev/GhT3IDO5gDYpOcvTEkTUpgYWHhpe5trhOUy0tXNleMW0DNe96v3bevVCscfvYoWiRagBUz9Wlrj0meA1lrGZgyIwoNI+BTH/gIN127n0IMUntMw9StQuh0TjEKBehcyUPPHeO7hx/KF6fw1MnjXK7WM2R98+qjH0CNws7hPLffeDOtD0QgaVaa3Mprf39CpAE0epwYKiLffvLHfOnH36NeKFlvJjA318llmcLR+CZVOjGgGEZFya+850N85OZ7GJF4bOpbBA4EDM5ZCiO4DPl/MT85j3Lu0kUCYUoz4Cq996t2K8CTuKIPP/MkjWUGnTqTgDYcUoNw9x13Zmq6yZXdhuCyVVu6E1iQzj3BiMGjnL18ntOXnmdMyIo/iS/qjSRzZukJrouAK7BF2VWEWKhpYJD0YltkMRoxGqcI6XanYmTvNddwcP/NlCS3iJG47DbSsvj6mGrh0SNPsRomudMwRezanlRaWh3GtqXALdffyA0Le1PLmuRD+EKLnO0UuF0JvuET4FYXuOjU3t0TacTwrSMP8tXHfsTaQFjXzA9rg4iR6WwsRMpg2MWQu/fdzKdu/XkWcEgdcWWR3NKb0DkDzJdz2NwKbB/aqWsyff125iawMl7tzGr7n0NmTr6ZUbfwwJiGi5NlTl5KCibTGUoqoaY+cumvB2KZNwUHrz/AAGGeghvsNXzozncyHCvzxSDFNJfMTzVm9ceo+KrBLi6yXK9xkYpvPvFjLjJhHc/Dzz7FuguJoBzjzP53yi+J44Co4ZY9+7lxcC2us9jJFkOQVEo6KovJgiaKFzgdlvi9r/4Fl4uGFdtMUSIhpPawRkxZJCePEJkXx26G/J2Pf5Y9zKGhwYmlsAbNdlRKYN4MGZrkv+h9Rulqj+YhszO5WgNX1paoW24ncYvedZztRqjpuqJNPvt19CyzxpPPHWdiEi/P9nmGW1SQNkryXzxwMIOlXG5TdsDKTcd/k0QeEGLdAagmeI6fP82V9dWk9jMDCtrKCkJgXDMcR66JI0ZXPLvDkFFlmV8Xdk6EnWObnieWHb3Hzomwu3LsWBfu2XcL17KDgqzJK9NuTnu9J/U4T0A4fOIotQXjUiukc+FAp76IJrW5S1MwwnLw+gMsMMS2BrqbVmjTm8xgElJ5e3vdbdvAmJdRYrewfY1K2c4IJCUbNbBC4CTr/Ksv/TFnZJ3xvIE6BXpnS3xTp7lMOSSYyADHNWHAjXaR/82v/88YYdIJye7zCBSk+VwB7NuxG3ygkGTb08SQgXsZuWcMavKA3gfUN5xbusSEyIjk7t3Nz1r/tTy8t/mzBY3Jww/h24/cjzeJQ2Wco4o+lTgdNl/Tjb8+Bg0cvOFmDu2/hSJXELsY8Nsf/iW+//3vc8wvI4Nkr4O1UNeppRoihhxsDawNPH/+4Lf4zc/8EnsXruFHzz6Od0lCS6xLyVMMNivFRFHElejamIEM+MBd72SQU18kYmKgMBkKb23SdlXFZRpGsJbnWeP/87U/5f7lZ7k0UnD5A4YIrkjefCQXcTRCNOxYV/7Zb/5tbp2/HlCGdtDN5yTEZFKLsEjBjmKItQVNrDOmeDOWvpNxc5a1yZgxNQ0FVpKebPSazV9baoumY5avk76rQ0WSQzt88hnOrl5KSj/aigyQjj0O50qqcZWEBIIy8pZby928a9/BbomsPmKNSa4orZKQms5JJeQkaHMSM0I63nVAS8MEww+eeJimTF0EE/PsLCsPSadOZAgiqDGUOuCg2ckdu68HVSZVhVsoO/TnVp2ArqptAnv27OBTt72XIRFHicenaz8EYtbEJSS/xWCUs9VFHjn5NPWAZDOVz0eg7XrkJUdIeqejsmDgA++/616EhgLXCQME06Jj2dR6tj/lRfqbgV+4nQRfj1Vhe3Fl6xdUEKOs4Vkh8h++/Wc8vfI8y64mqksB3ytBsvO4U2hqwlrDkBFycZV/9k/+KftYYC7fzG0bFJKvn3QVjVBYhwaPRsFm66AWmt21bWQqSHxldYWm39LaKJg5Q7pOAaomcIWaI+dOMqHBx9BJSHVEb1VEDC4qRVGy6Etuu/FmFplD0URSRzlQ7ObXfv5j/A/f/FO0kNzeMmDLVBSr4r1HGw/OUGtksGj5/a9/nk994hOcXD5PKAV81qO0U83NFJUV9R6xBQMv3LrveubT+j+BmKRtk6VWdBMNzghGFS+BCfDjM0/zracf4bLxyR2gL8Ip00iWRoOO+RD40C1v41N3/xyLuPxuUE0mDAZDJFM/ojQUpFaoR5BRmQLp1SZHRgkh0uBZixOimctnzswENCUBSaSD9PZbbkok0qAceS7NA5tB7NqE3dCX5OZhrMVYy0AsRRO5Y+8B9hZzWArCVoG7j+6UWWtDMhHdioBzVFScY5ljF86y7ieprb6BRa4mEjMwRgCtPTvdTv7+x36VT931PhaHC1PeaAtq2qKd1SJdiwBxUrNnYQfzDNKMtp3rWZtcQkxyCVFJJP5Ty+c5t3yZShuibPYB7KYIgBqbvCrNgDtvvIU5Bmlh1CTiv2FWizfkKlAyqvSlCKC/mjlYn+C/vW0nwde+IswVGdntu3QuITiBBstDl57i8z/6FpepkzJHiAytowlNIti6smvJ7JhbYGFF+dCdb+fTd36Q3ZTYEBJ3KesWRqFzo7DAoLCUZUnQGlQwztHSJUQEDdnPLg/0RYTzly/ldmhfmrpdiW89Z1LgvL/CM6dOIM6m6lIjxllCaJnYMUlKKQyUZHx66x2UmKRrmouORUZ8/Oc/yp89+C1Wx+dp5loj2vRZxSQZ/6gpkUlILcFvP/YAzVzBcj1mYj2uLAhNnRYSuRqYOokHFhlw3fxObr/xZsrOzibpU/oYMkdfuiMgCA3KaVb4s+9+nacunMHPSQbYKj1KZU6AQhmgnAT2hYK/98nPsp/FLJqcBZqLIlWZuQywWEaMGI1GU2SrvPiq3gfPlZVlws7dBHTTTdpyATfOF01bIebP9tiRp6h9gwyzUz0hLxwSwtVrJJpEBShJKN63HbqdBTPXWSD1k2yU6dhu68UhMxyicWw4eeUcZy6dx4siNq8BenPn0FtnmAhDN2BndLz34F0cHF2XBdtSaskF/TTRMONglgKaM7AQKNIEMLWz8yIoeo8ZlImaI8kdZYLy9JkTXFhZQnbkmeMGsFJ/lixYaALX7dzDzQs39LSYknyebJgxmU2t7Neu6rtaonsrJ8CfOUXiraB23tkCFS7b0AjreJ5nnT/+1l9xslqGhTQDwqd2TwKU5KF6UIgGGQcWTcmvf+JXKIhYoLBp9V0bGAtMgJpENvYk65bBaIhmEWrd6sJvhadz0Lq8uozvORJsDFwbV6Yx4+mOPneSc8uXkdKlxJxh8iJJHJwM1gkhICrsLue444abEZIZKyHNSByGA4Nr+cS7P8BitFnBJbWItfYpyLgUfLT1BCSy4iJ//eiPWJemUx3p4PNtcMrkZYejWA/cc+Ot7B/uyfiZqdVs6EVul7leKpEJga8+8kO+/vh9NHMWGSYL4Slsf7ZwMQrDceDjb38fP3/ju1BqTH6ndTzRWRorVMBarKhIMl7WuaSXGvUl30eXl67Qh59IC9jooYNnXDs6/dP0OS+yylOnniVmXc4uOGTYP5oAPpj0MxOVEQW333QLA4pu2dTPeJtcKmRGkC9VPa2LBBCM5fDJIyzHCThJotj9660VSjJppmkVyipyy469HNq5nwUM8wgDAiMiIyI7MSxi2IlhR/56EcNCfpQaGWJxaJJMMzZ1Y4zBDEpEIOayLll0JXHxsfppxchmJZ72/NsINijvOnQniwyJ+HQcrO06FFcVz34Nq8DtBPjabWY7Ab7MXmhiOiTSO0pNknP60hPf56uP/5jxwDCxKcCYrFWpAkVR5JmCMGdKSq985P0f4P23vocCR0VNRWAdzxqeZTyreNYJTAhMaFAnUGR6QWsd1G9/6DQJtkFv4hsmNDlITeGAMxZK0gfWJSPZh44+yTJNNgfWLJcZUrXZA8SopGNxaN8NHJy7jrLF3klqMxQoCxT8yvt/gYOL+yjqLM1VlBlwotOKK/u3mdKyWkYuxCQlbYclfjxOZO3ezd62f0sMxXrDu299G6Nst1PkFnIf2dgZopKUaA5fPMkfffPLnGVCM7QEMw100qIOWwCSKEbh1r37+c3PfA7BM2BATVIbaUiI0xWSVF5lhApDQNixY0eaC6q8KDrUmHRdXVleetkRs3VaaAg8ffYUZ9audNJgGkNWKOpr3aaFhbWWQRRuvGYfN127v7MmSn1K6WbO9BIePdWWDfz3VM2ieIQHnznM2MTUZo6baR2I6ZKhUbCrDffecCt7mWMIDBFGahhiKJPAbveQkNWX8sNkl/fWVkk1XbshBprQdNd9241pCJxlhcdOH0cGRZqxy5Q6YjYmwPwYYnnfXfdSEjvQlWT935nPp6999/NqGpzbBryvfBHgthPgKzjQUcEm89wa5en15/mjb3+JC2ZCY3LpIVC4gqqpEWsxOSlpkxLLcGGRndfv49HJMeYrIa5XDFyZnL9dIoYbY5DMTbReOemXmESfVu9GOruZTohZJI3rYpySlq2wVK+i5a50o2zhBt9v6VjjWGPMo88eoSmgCj5FxFzJpuQTsCT9T2KqIu6++XYWKCgQmqqmKEqiV6xT5rHcM38r77/tXp5+8Hkm2mCGAygcjZ90BGQADZFoHaGuYFhgZETtazCtA7nppLla7p+Jyi47x9sO3IrtyRlk4f+uERwJnZLKGOU/f+erPH7xJHHngIBP+pJdu3h2BIZAbZWbbr+FK82YH1x5gjlTIMYxaZok9t00OCkoB46qqQmirGrFc+PLNBoJVY0ZuM0HvtdGbAEySysrtKox0x7gBu3QnlTftGiLVNQ8fOxJ1m1yQE+BIYApKDQvPLKPIBqRaGASuOu2Q+wd7O44lsb09jPjauKG66Zt/81oGElSirmoqzx15gQTo9NZZI9WpH3RIdLiY9/cDj5w1zsS5rJpEDcg1E3SfI3aJU22aDMKWVzCmhlKiNj0QVo3eiuS0dzCkQtnOL16GR1aYprczu7fhi6QDcqecoE7992cwGa5CV3HkKrOn0EofMlCBq+gVbg9E9zerrLklqw5KDzPGn/2/W/w8PMnCLsHEGpEBWdsSh4xooUh1B6MwS3MUU8qLug6/+E7X+YPv/YFGPtMg7DZi80SYpNWfXlFakRZDmMuj6ZtJDa4EUhOgmlyIURNrgEXlq+gew+84A3QsqYCcHb1AicuPk8YOkJoMiAm8fRS4murxgQ+cWozaThT+YzpqsqCJFe1AHzqXR/kR88+waOXT9BIjY7srHKzWjBCiB7KFKgKa6nW12FQphZs1i9FbNonkwL4bftv5cCea3H0qq2QkI2BtDCQ3EZew/PVow/wlSd+jJ8r8CZ2YJ8gEG2cJsE2GNpEP/j64Qd54MnHWbBD1lbWccMBqsKABFTxtUespBmgBTt0LIcxq1JTLs7RVHVnR6VbzIqCJjWh5fFaDto6m/S6jKibW225ShvT8OCxp2iGQhVqzKAg2NZrIrVEo6XTwdTgcFXk3bfdxQ6GCDoFxHQGFiG7ZLKp5SeyOSfVwJOnjnNuskKTZ9xWBA15UReTTJw3/WrLsCADbth5DYaIs8nX0Q7KLGUEUWZdPDe2siRXvu0CUXPlb63r0nf2u6dBeejIkyzRJLm03NmYSYAzc2GD9XDohpu4brSTuW5aqWnubLbgkWiv4n0JCeuVJKmX8jdvlkLlJ3H8tpPgK2mJSpLjGhM5NrnIf/r2l/GLFq8NRoQCQ4wNVePTHVU6qCqMcfj1McVwyHhSM169gCtKZJCql+h9spBRSwx+hgHonKO2nrGP6UY3ub2Ub7y21WcyQkGQBHxwhqXxWkbSS8/yZnOrRoExFacvnePC2hX8TktsfEIKxgbEpomhJnCFKAxsyZwOueOGW5nHETVQuoK6aijLIrePG4bO8cEb38F7brqTs2uXOe2XUXXJpduSRbVrYJA+23AAdUO1ssJwOGIS66mHYFRwGTCUfQRvuf5GdtuF5OzQImrzbNQqSAyosayjCcX7tc/zPOuMjU/JNFtHqcbNi/n2IFm4EhsmRvHry5SjIRM/xtqCoonYmKtO6whWCOoJ6zXGGRoJyUWiP2PbotpQVYIok6bOBZ7OLnA2yqdtoEc0BMZ4jp47Q+WgHle4ketWLRoSNxJMV5kNxLLDDrj75kOU7bw3c+KQzcXNVrJgUx/IQEPDmihPnjrKRDze5msmKFaTso/Jx9z3rkajcNv+A9y85zosye2jCR5jXWpPGzNjHNYCZGbAKDlI2jbpoJ3riQ8+oas1oKKMqXn85DHGxlOHCEUSd+9GAxs4nAYYieXOG25mN0McQl17irJMALKt2is/wSTwUgP+W6lT9zMDxryWvld9P78Xevw0Vxsz/xfNYJjA6XiFf/Vf/oDztsFnRRLXRFyMqX0TfEKEBoXgUdVEQZtUGCNJP1MCjQtUNjmX+wImWhOs4h3UJlI5ZdUFxlanNIUQsEWRqq4Y6Q9t1AcCiljDRANr9WT284RcEejUMDTNT4QGw3cfup9GEknYFC5JR+XkanJVaJwlNB4blHffeTc7ZIjD4MQlisSw6ALB0KU26QKGv/+Jz7LTGxZHcyn5SFtm+SkjW4EqAUkKW6D1VPSZELHZnslaiwTFBOWdb7uHEWUKiq3pqUvtKYlJ7aNBWcbzJw99hYeeP8qFsDb112uFUzW5xotLtklSFCnphhQJ1UTGWtOUwho1oVBq46lsZFJEqlJZZcLYNUxMgxkVePUQPFa27OL1qi3FFo6gylo9yRQF111/mikI2pngpsQYNBC878jyjx1/mguTZSZEzHCQuHX5mATNpPmOTmMwTeDG3Xu5ace1GKA0ZQbgTAO7tTYvmrTdkam/oZK9KTMyWRwVcN+TjxGdmUHzarbKClmEoAP6hIDFcveh25lnkOUF02QhAGoMdfQzCbDl3rUzwO4RyQC0rP0ZFYlKIUn5SLJY+pWwzmNHnkrUjdxhSDKGHikcMfipQpIRNESGtfCBu9+FaMi8yCkSeCspu01iAq+hX+pLjYtvRl/C19JzdlvC4CWW1H2JsAmeJTx//fgD/PDkYeqRSe3LGCkFtPFMmhrKojN3M/MLlNbhsLhoMviit3ptRxg9SKIaIVohuCwzZlIwcmIQYwneo00Wa8qu5f39jwJelCr4LOzbO935/a1zXdLwRJapOXruFBTtDE1TEJWE6tQQMS4FSWMsw2C5+8ZDLDLqglF3g7bNvCyUPMBw8/xePv3uD2HGARqlsGVKeLacVeGPCTJvNwirtC4cbjBI7zGp2TNa4O5DdyJELKn9mQZEKUgHXxNFqYAnJ6f5ix9+g0tSIYsD1GgKZE2VjW1Tha1VTTGcI1YVRgrKWGQ9V5Nnr6l6J8+XohWiEWIhqDNdAPV5wVAszE3Rkf3ct7EdGgIhBMZNTYWfVYyBjvoimgxwNSc1Wzg8kTGex449w8QosUhJs9N4zfsdNGakMhgV5rTgnpsOcg07kt7qhms/bpiLbQL39BWMYsQD53WJE5fOsVZNuusyxpjk0iQt0HwLlGkC1g0wPnLnzYcYUU4BJyJd89caO+MUb9nCtmgjD3azzQsRpUJ5+vSJtKDpsK3a3ecRhcJlZad8nCPctPtaDuy8hjkZdIuDrl2cQWnkhcKmBBW3gSuvy+nW9iF4eZ3QIJEKwzPV8/zpD77Oc+NlvFEGxjLISSK20aIoUqCpIqw3VFdWE3FWDE4Ng2gY+vxohGEjDLxQBkn8qJhcCxNTKqmDhKrGV/U0ELWgkuzK3V8NRk1O5eOmpuU/aQ/o0A8SMVMxnvNXOPzcCdRIN7eKnet3ojEYk7h3ybqo4N0H72KIg0AHvlGJWSJrKgJtVbiWRX71w59kR7DMiUOrJiXYEGYCrQuCjZvRlG0wMcYiXimC4cCeazlQXte1e5M8GbQGcnZQsqaeK3j+4kff4v5Tz+AHlloD3nuctckpPHiIQmEHlMWQZmUdNJn0LqwpO9YNRRCMGkbBMO8tc01aCJSaW9pNzFV2OodhXNH4hsZonnO286+rX2cRTcCaXgpMRbD2eHFmSpgnKcV6YIma+55+HG/pSejl4CyKcTa3+RLKpQxJdPydB+9kSJtwrh4WNiXAXOn0Fz8eeOrsSZ5bvYLaXilkEiDHGyU6l7Q2bVpcDbDsGS1w2w034aBzobBKR8mxSIfQ3Ehf2bL66r31tO2sRIQK4YGnn0iI6fY+6qQNTVootB2QkCQDCxUOXXsj+0a7EAzqe0bVUXEbiPatpJ/2OZ2vMg++1lXQ9rY9E3xJveX2OQBjIlfwfOH+b/PI88dgxwBsxISIRfAakOEgNaZiwKmhDEJYq7lh9z6qpdVOtb9d5coGb7WgKYBpVoSJRogGxlEZ7Fzk8tpyavXYRJeIXWtrczD1KOtNlWd5qUoyJr9JpyATiAYmBB49fZznqyX8aJgI1jkYGZNncdmoFgwSlOvndnH7npuSVmf0GGezZkmOJ3mpLio4kpjZ23bcwofufCffOPowV1yEQYEPdUZnTsEFRqeiAajJNI3c2mwaXFR22AFvu/EQDmWUL+dBns9oluTyQGMKfvT8w3zhh99Edo3wVtGQErD3Hlc4fFUloeylZQo3ZE5KBlqyyACzusrcYMhSaFCJzDVpttVkcEdoYf7GJe1Q1SQmwBzBWc5duQK7FujjKI1uFlW21oJVxk2qUITUgo5KryWahHfEyswcscJzZnKJ45eeI5SRoIkCobmiFxKtJgF9LDYIhVcWzZC7DhzMlZW5+kp5i3nkxgoxmtQpuf+ZJ1i3nnI0wIcqOTvanIBjam9q7REc2AKpGu685V72uMXEaM3nu6OVRGUmx2ycu8nW+6U9wFDSahc8kSVqHjx+OAl461Sl3JhU0SdFKJPRqIYBhjIE3n7LbSymySmNRpwmv8oit4s3Jt248fht56nX3UxwOwm+jJaoJyaZrfNP8l9++E3WhoYgDdY5mvUJxhqCVcTl6DapGJg5hnXk2tE1/MOP/zq/8fZfYtBr5aS2zux7hbxaTUCHiJC4Y5eY8N/9yf+FB04/wzj4pN5ishV8YcA5tEl2P0aSrqYKVE3doQzbYb+VWYcCxbBGxX3HDlPPFfiYDH9DDyWZ9EYdjU+tokKUe244xD52JG6Ztfk+lx5YQ1IizKthJ44h8Hc+/BmePPIMk7DMarbsSW3XnlpODlrRTKtJ42yaYcZIaR1zjfD+O+6hINkY4SNiDVVMrvGltZxvVlktSv7jN7/MqbWL2Ot3EVZXkKJIgbuZENViiwTkGc3vYBQdds3znkN38r/7e/8VN7ETqHA4HDDMwa3OSTZ0h1Lz2TN4kqPI/eee5P/4b/4Hzq9MqAfSHYst3Ik6d41JU2d6RE8SyyT0Ysyo1/bcxRhRm/ReD58+zioeT1LlsS3SOIsd+BiyJZhQxFQF3nTNPm655vqEcM2B2/Zyy9WMjFWmii+uo3gIK1Q8fOxp6iK3YUNEXF5QtS3nvEqxQRmIQ1bH/Nzb3s4O5igRbNva7yFz0hx0Q9LbYCassllNpt/W1cztfba+wNGLZ5MIhY/ZxUJRlXSsMh82yRVaqD0DNbz/jnuYJwlluLLs3iUB20LymyQ3Zl6b4m97264EXz+riiYrTPzZd77CmfFlJnPpJgz1hGFpaXwgFKkiSmomDhOgGEfeddshfv3tH+U6LKPUFJ1xdp8i3aSTOItMnc9DTol7B4uYqB2Z1xpLg59WgkamOodAtELjfZdoO6Fmmc5IMAmwcFlXeeTE0+jCgDgZp1V6hiSq5tlZ1FSxOWXkhrzz0J3MIzkIZp5i3+k1t9c6aSygRHjfjXfw4TveztFH/ppibsS4HmMGruNTt8TjuCEUi0hSOhFD4WFBS+656TYWGODilPmvJF1MDxTFAl955lv86OjjyMKQlfE6uCQKrZMxbjjEV3XSJgWqSY00jgODXfzWhz/DbexiDmWReUyWMSvye1QyTYAtSR/1aLZnWmfIudEeWKmY21XSqJ86SGzVCo2RoJFJXdHkV9aZqnrTHyCiKMmE+KGnD9M4zfM2j7oNsP22oo+K+ogLhjtvPsgiQ2a43i9i3dQmwC4JQhKTRzi/dpmTV85TWTBNothYZ5NqDi15Py2yTB2Sbqm4jmbj+s6LeZZprZ22K2cWb7PVdNyiWzs7CDc0BB49fYzLzRgGgs3HT9GU/MyUY2hVKBRiVXPt4nXcuvf6bNecFj+uQyNvHp3MVIK6nQ1/VsXL9kzwNdpijFR4vn/iQb772IMwNyBqUqGg8UjpaIym4B0TJ5CiIPrAtYu7+PS7PsABFljEMcIxwOKyMqJg0fxsMDgsBYYBlqE6hljm1DGkQKuGUDfTFmpLQhY6KbP+BWGMmVHej2yQvsqtII9y5uI5Tl0+z0QSulWidvIZmoEm0gloCwuDOe6++XZKBA29md2GC6tVGNEMsiyxXMOQz73/F9m3uGvWnLcd5JgcLzc4GIcQOppEbDy333AT1xU7U4PKZ6pDpcnxCFj1Y5ZY58+++RWWjEedoM0kkdZjA0Sscwk52ARiCJSjIXPlgHcfvJNP3fQe9iHsVss8hpFaCrWJ0yhtHW8psAyx2EawvsBFR4EjAB6DF0tjzKYW4sZIHUkLr8a3Wj2bpe5mHOdDmtEqyhVd4ckTx5i0adk5ok9yYBry/KpFOqriVBjZgntvvzPXNnFTUtkcZaYVfkQSaHZDF+PwsSNcqdZRm0QdWhPiTmw2pOqrxFBEcHXg5muu48DcdSm1tKIwQTtUayeQ0Jv5bYHP6caANlenrXt9K4WnwCo1Dx97mrFruyIm24TlkxJ8LwmCyfv6tkO3M0eBIy2umrYGzAhXk1G37b/Yic8pL35gt7ef1faWS4IzCzLd/LN2ddv9KEuQTSSySsMXf/RNzvtVxrHCDAYwGVMOR9STFl1o0hLRC6ZRbK3ctv9mfumdv5AUJjRXajp11W4f7Yqxk1iM+dH+XJXCFFixKbkFTxOyE7dpYd5ZRlmT1U4LbO+UMJhNVuTbdY2a566cZ62aEOo0G/MapmR8gcammc+wHDKIht3FiAN7rqHcUKVormRbNGoXBCQFSQEWKHjXLbfz3lvvxCyNWZhbzBVeD3641co5RAaDYfosHt5x6M7ET/S+40wmn6vkZeddwZce/i6PnT3OqmmoY5MCagbimLn5RMaPkYEbUBQDtI4MauE3PvErLAAjHxlg03nd4EksXQXYy/YRqJpc86SWZDE3SIjh9mOY9EDNjFpMLu87fROTVw+q/STIDK1CgQk155cuc27tMhMNGCcUroDYoC7z39rZcZ5jDaxjd7nA7QduweUlmVwt720QlQ45CbWi3aRLnhVqHj1xhNV6HYwwcAXqAzqpOsJ4e08ZY7FqMA3csmc/e1joPPdU08w7VaTZaQLZdA/Hl3XTp5n4ChOOPXeKxiiNxsw9VIgRI266SgzZuinAggy455Y78lFKHpVeE+paBYL3006LbLFf8iLRdrtK3E6C/eplI9LptUJB9ZNc3w27BRu0q7sGqKKfWiYRqcTwxUe+x9cev49mwdLYQPQVUpZoU1NgKKIgk4Ar5qBR7CSyIxT8zud+g50MKGKWczJdbunTkLJFn6b2Vpa2UpP2JxjwkpCHvm7SrHA0SFY7zqS5YH9YExNnzwUzBWAYSW2o7oCEZF8EjBG+99CPUw5yBROfgqeEXKElIVC8s9R1zfwk8oGDd7Fosn2ttNk76Y2YLGDWlXL5ybV9thhZ1IL/1a/8XfasJYBGx9MjYIJi6oBUPiECbSLnlxHsxCNRGQ0GvOfOtzOPo1TXXdFq6WqoI1zgd7/xF6wOIk0pSGlSpa4Raw26OqFUgzUFIURiEylq+KX3fYh37LmFXZTYKHht8CZsunNKkotG5xVrSTjbMjHHBggXzp5hEsYwzKWtSKZUdMO+rpWNSXJ8McZMFEj8OzFTcebYC6wtj65GePzEUS5OVhM3D/DeJ5qOhvQsCcFjMVg1+Dpw8zXXc2C0N8+oZZrQdcOjTf4iBBGavC9DVQY+HU+Pcpo1fvDsEwznBhACTV1RWEdhC2h8aoEaS7DCRBU1BaUM+Pm3vx+HZr5dAmK1b6vGYKPB5MwbpwyP7gozuakREYKmSrW7yXLnwud54KkLz/Ps6VOYskgyhIMiV+gJ9FWWQ5j4ZI8kBcY4dsgc77z1bRQMiLn9OydlbvMLpnB51JAqZM3Z0LQLUJv+KEoWF89UmOkx1k1Gx3rV1fv2tl0JvtpEOzNJ790rvZZKBxFwhrF4Dq+e4KuP3sfKILJeQDQt5yuZzgYDQQwiqTUmk8B8dHz43vdy++4DFJgOQr3lxb3FRR5JNw1ZKqptjbVcsRZGPg1UcbYvronSkMjzyR2+ML3VvrVgEqT9+bjEsXNnUImIxm52aHV2JY0IBY6FYHjnTbeyk7mphkdLUt9w00qvEau5gSjimJeSm+eu4ZPv+ACsNYgrEzfLJiVrZwTnXA4aSdkltU4jpSnYu7iHa3fupsQm5/ncjmsnaTWBL9//HY6unqNykYCn0ZCoEyEQGs9gMEirfyTB4Gu4YbSTX/vAx9jLCJdLMGdsAmvk8rw/D5s5d4Ykw9bLHpqpKzH6jHTsVnddtT9zccp0EirEGSufto3aWhLZwhGIVMBDzzyeTJBN3MxlyP911kETExUkGO6+5RCLlDjSImMT924j3aAnZZoWbZLawkapiBxdfp4LfpVJaGZa2E0Mycmk8QkdbQy2LFBV5ufnOXDjjTgcDUIjaUFak9xUKoFg06ORhMatjebFW3pUQJAO/kVVNylRtk7Ajjyn9Rw/e4px9HiNqEsAKu0+VAK4FEXBcDDEo4zrimt272b/zuu6hXLbBvb5vev83O577FXL7e9UJPBSQ4Ts49jJ3cXtXunPanNvteTXqcNv0F4SSbHRZB6calK1CNayhuXrTzzAD48/zniYKo1pa8jMtvvVUPmGkS1YCI6/8YufYi/z6UBHfXF76VYEu1MJmdrZKIEYGgoR6hg7hQ5M7KS/kuOD7VwfYjfBaR+SRpb57muI3er43JVLtAVV255tSf1eTeq0hkAhBbtHC9x588HMSc9tpKussmaBC9qBdAyGvbKDX/rwL/L1Yw+z3jRUWZEnqqe2klqcMSeVwlIFxflIGYRbrr+Ra4a7O3J89DV2UOJJ+pkn1s/y5e98HV+YlPw0pkKzHOZSwhOHhtrXFOIoI4xq4aNvfzs/t+dtzLdt3FbZJMbu/Pl+O1Rmc43LPpP93OiMxcZUJ4TcLhPVjvPm23ZoZFZIYcNm85uY/LtiknfgKg2PHnuaIFOXdZvHpLTtdU03QAiBoZQMMbzrzrspciCQDJLqJ712NCAmv3dI8zVMy+WbCo5PUJ589ijLk3Uao0hhiaqJf2lBrWWgEaNKU9XgIARlfs8887sXWCfxWcepF0DfX9p1iymdQU+HDRWsaRp2FXMUzmbB73YBKdR4Vqh45OjjVHGczqEFH30XHMQ4QhPSQjCCGsWOCvYfPEDEssR6NtCaLpj7zybv11bfN/maL0jC2zE0aeFl7PS4bxwTb1d/20nwJ1IFttF542pXs6qKD4gqaiNjhON6ga889AMu0ySXiI0XZ9fvN6CCTjyLLPD+Q2/jPde+Lbd5mFVEebHWrcwiRjUnxElVpSpNFYkBcTahRw2IKDFqMsHFpCoSQYxOpz1Z4qpNrFXwNNbx2PFnWNX0+VTSO7dQeZujkSoEn6D1t91wA9eN9tBN+dp92mKqZLqayPQqjdT+LAy89+Z7eO+td/K1ow9RYWBoUvILfvrGSIrEJlWTrgq8++CdzDPoKtEWdFJRs07gSz/6DieWLuB3JuUWss5oW0mHJlLHtB/GGlylHBju5Fd/7mPMtzeHkVR2ZREA2TAz3koEOy1MNM+ahLppwAg2B2WjU7i/6Gx3IsViMzsW7dkWaZxy4NtOQEXg+OpzPD9ZIQymOpcwJeUnbqqhicnf0CHsXdjBrftvTO3sVhXoKiOELsGb6X5q79qvSBzax44+zbqvicPEc006s6bLzBHwsYHCElz6YOfqZf7sO1+hXG4Y+nS9eTGJw9ea5Yb8ntmcWEk/j1nUoQww1wh37NjH3/z4L7PDDtL7hYiYtksQOa/LPH7qCLWNeCGL1dep+yCCZrCU2OTgIUVqz564+Bz/76/9O+bHMPCvrIFWBGFn4/jcL3ya2264FWuL7j6LGmcVnba37ST4kwLFhJzw7Ia2o+ZEJTEHPWMwRlkj8sX7v8vDJ4/gd9oUVAIUITsl5PZMh/oLMIiWXTj+1i9+mp04FilSh0teTpbevDUxsOLH1DYSRHNVlSWbMnhlq9dIjuqak4VBep443iYd1AeeOcxY4sxnUYRgklxUC+KxGGwdePtNhzoRYSOZhN+HsLd562qLWREkKKWBfczzN973izx5/CihXmNSGkKuxvEx29UkjcxYpDnZToT33nIHg3ZVHSO2LKlDRXAFR8bP8fn7vkm9WLAyuYLMDdMaJSjUHiuOINmRoiwosBR1zYff9Q5+bt9dDFAcljoHT2dsXuXHmUO8UQPUa8yskphnZ8pytU5EE3ldILZoW92gJsJmWbKNa63Y6wwQI9EaKpQfPfM4q04zeKkvJWu6DkgLvjXlAFlTDt58I3vdDuYoOi3ZF9pivn+6S1mmXNYxUyPfRhSDIeTFpEhM5zIE/CAhVSksMXpiKZyoL/NHP/wqrFTM2xLTFyHIeaHI1XEkXeuRxDf0Js2t5xrYtS78+j0f4HMSUj7XiHHSqxsNJy6d4+TqJXxh0LYtFPOypkVZqya1NO9BhTp6njl/ikeffIy5wdwrjj87JobbJgt86hc+0XUSLAavAZeNhbe59NtJ8KdSCW7FLWqBfU4zvcGkNuFJLvGX932TlUHEF3ZGmEU2zVwEqZVhY3jHwdt53w13s0hB2a7xwss/4imBperNx8B6PSG0oIrYmycYi2YTVMHmSlFzW3MK2u7wdTGCS797jos8+/wZ6kyu79sShFzVtBJmTpO02z033sYiRVKKwVCHhtIW0xkiszNX7Q+h2ygqFhsjIwMfvvM9vOeW7/H80QeYjBt06GYcF5wYfHblcA0c3HEdt++8gYJISZG4ma7EO8OEyH/+ztd4dv0yq7scDMtkpRQT3yw0TRK1zk4JRixx7LlmsMjnPvQJ5pFEdwCaGLG26Ko2wWAlzn6eDck95uo7kRwMy6vroAaROJ1b9ThupkuAgu2qQJOXLW1gzpD9LNSOpFadAhWeHz/9BGMTc6fCzEjWSU6A2lbTUWFcc/etdzJKE9WpO4fM9natbK7qRZlZ9DTZQPfIuVNcXL6CKV0rqZoWTiJ4aU11TU80uwY3gIFlua5Z3D1itUl1Z2PpZu1dJSu5sZiBLq02rkQl1IZhhJvvup2hG/XudyGQ5uLRWR49/hQr2uQP1hcB78mnaeh0gttWvHeC7hixnPfrZS/AM+Dt2n3Xs++G69M+ERNRSrIlUysqvr1tJ8GfZAK0W6x2jSR0XSSt0o0kZOZFKr700A944tIZdOccUep8QQvVVqofwTAIgWuHC3zmAx9hNwOGgDQ+WQbpFms9eYm9f02znNWmwWdeGJJaZxpTcJN+NREEFyVxsXCYnBhVfdKcRAkklN/hZ4+wUo0T0b4NCj3Hb5+NV51CGQzXz+/irv03McyzGEU3+r1usaQ1XTtvyo/LfolErmGBj//ch7jv+SNcri4nV4lRcnJogOjayhDmGsN77riDfcyxoAaJmoWzIx7LE2vP8qUff4/1Ocs41AwW5vGra8QYCK4AV3TqNADRK64KfOAd7+Ad195GEQI2Zw4ntrtutA7gLIjB9Whlsx+7RTUmAETEsLy+ll/LEai7HJBayLk+j9Pr0/ZczbeUx2yrR5sq8MvVMsfPnmbSV0noaXjFDQsbU0XmtOA9d93DoCMl9FYsvTeTjUEi5weT7azaUYJHeeypw0x8g513aGgQI1iTDKJTRrXIeo0RC9YRjE0IypA1dxvfWRl5zcCSflu42yfFxuk9kH7HgbHcdutBIpHGx3QMizLPaUsaKh566gkqkygnEhWjhohLrxFNcmIOQmkL1Eo6phJZnYynkruvZE6nio9www03ZBiS6dFBItLnKl611bC9bSfB16If2r+rOm3QaUUo1tKEGsVxxa/xje99M83GNN1YCQGZlDYScm8aQgbBsNgY7thzPR+4/e0sUCTal03i0puW1VddMU5Nck2324p6CBOPiYKzoB4KKzif6AgKnR3M/5+9a+ux5Dqr6/v2rjrndM+MR74xkJCMHKLEtoyNDGJiQAQpgbzkIcg88M4/44cEJJAixIMfg5KQkChCCBFnxjPT3VX7wsO3d1Wd6nP6Nt0zp7vXkqxWT1vnVO3b2t9tfZIEPgoWuUWrTUl8UwSx+j3natZaj5/+7D+RQoRrjOR0kvFqt3DTyWyCYhUcHr71O/jq/ttoAYSYoM6j0cbcs1lO1EjU4fC3pA5khYegRcKjr3+Er735Q/zmvw/wmz7ALRuEkJBSgJcWLjm00eNeFHzzwVexgqKVxtxZ3qFDxFMk/NO//wiPwyEOUkCjHv1vn+Oeb9GnDkfP7XNTtuT1nASLXvC2LvHdP/oEKwB33cpevgTqFAA6WxvzrOL5+hIBIiLK6oBA0B8coukTFr0WeTMjSZ2IQddYoGnNAi6NerWxWIMCDKUJvtRcdkj4v88/x+OnX0A1YyEOSY18syQkUWg23VhXLMy9TvB2u4d37z9EC0UKPeAWJS422y9pgwtF113eqVijv/ivX9qFJAik69E2itY5xL5DiEDbtFiKh8vAwUFE4xR9MjWjhV9YvFYUUbPV02aTMYPkMh522dIk8MkuEJ0DGngs1WHPNfjdtx+Y2IS3PXGIBC2FOV8cPcOvfvVrqEgpgAd8zmjVoytiDU48kCP8QUbfd2hbB9curWO9ZOQzJnFOy7hqkluTBR9/+BFWaBHQo0UDQbKs8cl5tOnYIhfuKAnWyb2sPoLzz7p0VfSpwq1grTShuuxiIcIE4F9++M/47a//B/dbh4PHneVrFHeT5OLizGaB+WgB89XzgL/72+/iLeyhQemm3SeIV+PfmvV5Cgk6USSM6it9CJA+In3+HHf2HHIAJHu4PkGyRwqKJFZ+EEIwF0uw1k739++Vd1P0AxklRCie4RA/+8lPsUiCVZfROIuzRCs1LPqdlhG47BXLg4BvvfcRcn8E8XvwarqmGXlzqfU84y1VV17JmiuybC2At3EHn/753+Dn//hzSCpZhL1AtUEKGZoUethjr/N49OEfQ+FwEHssm6akqwv+t3uMf/23HyEcdXjt7hK5661m8ihi33n0Ja552AU0TqEJyL99jk8+eIQ/+f1v4A6aMkfF0hYxwvZYlyTBdpLPXYBrGyzg8BhfoDmIuJ9byEFA0yo6N16e3CQGqFmRQ8C+LPHg9TcLkYqRZjaCCEWIIKImUiR89tln8H3CXqPwveWFRgWSCJJYMYNkhU+AHEbcix5/9t77eMMcyWh9g8PnB1iuVkM2pcqk08WUCGs6rBPElIt71+EAT/EfP/4xFk6RDwLu6wLoElzKWGqDLgMSgba0o1J1iAlYiLl/bRzMqk8pIYqilVSuIMnGRhKiE0hI2IND7BP2Wo/QRSzg8fG7H+IOVqUvpnkH6idIAn75k18gPDnCas+jVYulVzH7rnbdSBkKBx8BoEXogNhbitho9adTz7F6fllpTIaq4n5q8dE77yOjL8ozthZlWis408azfTv29dBT6PBFe62eduZu+vxNhP8yv/+8n3GrLcGMiYD0LPNg4qHCUR8QJOKdB1/C9x/9FcLdxnr7ZdugpgihtrPKDdsnYBEEbzZ7ePTwm7gPazIrAJIrHWNiPlf+lyUCFFJ0DvcXe/j7v/genizykHlX3buhHHqS8hBX0ChYiMMHX/4Di/xkgS/JDCgJC/vw+OTdP8Q777yDrinqF5qG7Lsa//DJstteX+7jT7/xAe42qyFFv6iLDi7OuUtn7SZbDtI0uYtIVngB9pDwrYfv4h++8wN8Ea39UEpW39ilCAXw2mIfdzqHL++9jgWcteaBdfBoIOifPMO3P3qED1cJnSa4mNGqFUHHaBaaOEUQO5jarLibGnzw4CEelBSRwcLRtZK+0u9x4sqcXt4n7sOFq93wAvbh8fHX38Pd1+/jcKEILiINh/vMPZ9LW6Eo+MrdN7BfxLoVCQvRISWnqu7YGol4//ce4tNPvoOnbbbnL5+fJCHoSCKagZU02O+BD9/6Cu4AcMEEExaLRYka1w71tmZ9wnp2kxekmvCpUuoyA1zI+MG3/xqPNaB3NRaZ4DLsfbV0sEgOWaQIpI9udFcaT8pEPWcaatBkv/diyhb3pEHoemhb+vpF4IMvfQ33sEADN6jY5KISkzLw5r3X8elffg9PlnGU90vj/qlJSjr5vqlHZIhNYszcnv/0qta4uMi8SbaEKK+KO9HjNTRFMrHUZKY88XnPBNOxrie8MQZNvLhtlF+Auq/aErxsRKA00Bzrdaabzo4YK24WVRzmDk+kh8NiSE4xZyEQB4FrqWIQ8FDE/gB3mtWgFJEAhLIpKvme9Ia1d9/QGLfULQLAQeyRnCutVsvhhrFoNw/2nRvasSYk3EeLe/V/Qhhz7J1ZfE/CM8C3k6tBtTbmfKZ4gud4DXt4A4AEExMYDk3omBgz03ecyrXlmei0HUR2uehE8CwfIIlDRMCiKHSEoiZyFJ9jiQavuWXRgrTkH4QEeIegCU9L7WOHgCUEDUwVp9Yo9gjoS78Hh4h9NLgLZ41cy6kctJZCjHJzOjmEpp7BWpOHDOQinyXeYla9CJ4h4qjECq1cps7fzFNfCkxMQTZiDw5tVuCgA9rWVIG8Rx8DmqaspJwQpccXquhKRLLmGaaSsDKRMC91dQlvoEGbMarVwLJgUTJ9K/k3M6LOCQgpopkIc2cBnuIIPQQR1krLF0Xc6ZhVHdWIuTZEGmJ9OpFGm96bxrpW23krNIjoYRpKDY7QYR9LrGCaqLH0m6qF7XsAnhwcAKsF+qLoqbOQfCUcnXl/p8/isC54P/9ZSx7G8qZcSo1k6EBSE+Uk5aE92fAUTtcuWWltjeTi2H2FhsQpliBJ8BqQYCh3Kl9r1mZvH2M3PIM6N5baDl2jzUIIoiXDS4fkAZ/GfmwpRGjbGPGmBF+UWvQM/v0p8a2RYkapnRvdiXWzhmK1eEyVU4zIGgDLg9Krr5n4JWMwi8g5U83IeWzRM0bDhtu6XSJsA6+QobHoXObSPEgmrp2ZhqLL40ae/rtOSNDGLUDbZs3BmCekUccv9iZrJ96NJKiKhB7ZOXMb5h7L6JBzRFdIwwMIiIiwGi0PNfHtCODgEFitLNFDK1HLmqtTNjjEFDOllYxSmB8QkBEbhwBBRMBeaQw7c0QgFdGFPkU0arEi9BGNtuXdHNbYWGBJHF1nRe1LjyCxtl8e1kU/HN7VNrWUqFVQoI+AK5TsvXkdVIsM2+gqchOSn57XOZVsY1U8P3qGxXI1uWRaHF1LEta4JvyG7Mo0/JTZcSSTkAMAEwNPFuNErW10lp2MlNFqC6j1ncxeBrnXtjxTyNbsuZaNbNx/MikpmV0Eh16gxT07/6lwa78jyfDv1dVpur7YnPkyuzxO98ku5MmcRhfXkRBvFQnagp7UeeUJm4x6acVSsXuYz0DsjuB8O1G0rzfaNNxRB6syA0jR3JUley+miEbd9rjZnKxjHDpADJtm1FcqG0XXZCmqZaU5o+s6qGuQvOKoHGD7w46yUgvxtW9AKL3rx6Sacefr2ubMALrcQ8VSDUZVi4yUjQTXxnrSb25KghhKNUaiHSzxaAfxMM4xI0mCqkMXAlrvh5t2X8ZT6oFb9CIjTAVkoQpJCiAiqjU89tHcoEEdqsqp3cgnjRMnHcE1rb/PtJtBmlkIpqgy8R9KWW+qg1U+uFuPnYFasiItU7COVVUakUo6SYBGcFTsxqbafKV+cLBOZ+vC5XqhyfZOofhyPY5lKGcpMmQzEpxaRykGqPNIMcA7jz70JoWmbjI2qTQNS8XHoMfjE8BMN25DDAPrWZM5BCuAn8Zo51nWYpatKToVC2o2RltC18fmdk5AmxRhzvJzLe6/LRFGNrRhwu4kxtxEErx1LuZjeQ2zf0g5lZtwKrdoB9csCwGW/8rNtKru63wRF2XsqrLfaBEKC2dLLdu6kGqR4uS2mGc1eJIFCzg0ahaOlHYuEbAmvDKKNA9i4uW91/ULdd0fVL56kX3JaUOxtuxgUdH13HGZtW0aEpDyUOeFbC5jKbV1qcTrkDNSHJsAaxm/hTdnUIzVRht7MmpJJEoDtZa/pAiI2UcSAactBPYOSHko+gbSYGHlCR+uxfuKULbLIznosfNaMSkqHOKvmnMRgB7XkD2fDhcxK20xwuhCh4iMI8Qi6m4qJvXg7HLAEWIp5leEwyMjyiIrVlXhq1he/V2zrInlHoXOrKWUzJUbx7jYscbthYgVQEip9Gy0S0nrG7Ri0nAu58Fur9mtw2Wn6KQPi6NeQGqHi9OOqFw6ZsVU1QNM3GJYbGmtV6YvLmiUeP7WZg6z+tb666iRevzvm37GNArym3bp+HtM681/N+3tuobThrHfBQm1F21isIu4lU118+ygdpPrWlYt8bVp/ZQixICQeiz9Yrj5Z5nd8Oovxa8fkyViVAtJVc+c7zz0jBsO6nVuipPYQ/3DQLRdBMRDNGPlBR1iKYg21vNi1X0WZ/PIOZkbdUOZEuZusGSDlr3FRQV5sDI23WyPxzWq20vXT5kJYdbSEGRT8BEFcm+qZ1CgcSWxII4WD2B94VCLwtVZ7CkXKz0BrTa1E2ptT4BUaj3V67H4j9ZnHUwAXcuUdHp8DRi3Wpsgu6ToEIusPQ6BmYTs5Et9sXCWrkUqMTCdHoQWkLY1Ja5Y1oLGL41QXZ5pFazPSUjWHFkLG4SiVLLQYnOWm5Hz667eegjXtdw4V7ocReRoRd+1y4SoyatFHTs9yNz6zdv3ZZqMvs7q5RJgzZfrHBXxAKm17wml52Uu+zpBxNymzsloyYgcs7Y2OWjdnIDktH07JYw5gWz+Q8J4YcrDrMn6vG8zKwlaghcmwHUj53hjU0w0LgXWeLVkok33ssvjICYMjRyGQ3m4MdVmsOe0BNfcDzrtx7DFnVOa+VYCdQBcLBabjod4rSzUEocbbqNzC1nWraPxz2OPwjxXwJ+VRei2ZTdzYSmAENJ4+mTAewxqGlXAw8EK3GXDLT6FvPYqvjG6CUdx+NwczFpwzqTmQkzl4rP5wr31Ep43JMzUOs8Sl83Vypo3xxVsLrcoleJau+8UfbWh8XD5fxZqF4GYQpGtNQt6mJdi7Q3PVh7UOUGIcXhoB49QCi+mckhu5gYdGcrMGa/O2lGps76FpWxi+j5rsdLJ+S/Td9f1lkdpAxlN/wsxDfMxeDJkdLWLavFOlC+rZlgdu5SPr/WpNTaNVV/gfJluhZTWvybGPDxnbfWVsVlzd75eSHw7GhO8TP9yPfhfps85z90NON4Qc5oQsKYQIps/T870ZS82ZmMmqxzfJzXlWmTNbTpvjDrP3Nwak83YSFhZZq+z4f3Wvm7L52x6lgvdaGaHh1losl2NX7Z+xMnnTsaZD6dNob+tljZOfr5NjW2n2anb5uW0cY3zdT6bEznpZc7wDvmsY3mG+ThpnjZmLQ41wWkrpeVTXkfGOAnWTdOrvaBvXSNy8ll61jq7m+TOnL/fed/tVpPgdZ3kk8bhoov8pmyOm77JidPPkquY/11fVyTBi4O1l9fRfOcC5vgQnH+CJMgNTnB8CM4/QRIkCIIgbj0uEt0jCd7yBcDxITj/xG2G5xBc7kZ6Ve4YbnCOD8H5v+24yPlLS5AbnONDcP6J20uceUdWCFN8z7+hr2IMmEJNEMSunv9XAVqCt8jsJwiCINbBmCAJ8MKWKMmZ2FWLguuT40tLkARIEARB0BIkSMIE1yfB8aUleOPBLDiCIAiSIAmQIAiCOBPoDiUBXup30R1F7PJe4Prk+B575suuE2SdGRfxdXv/S+1ReUXrf1f2FUmGoCVIXGvwkCK4fghiBGOCBEEQBEmQOB1MPrm581r/IwjidoHu0FtGfozpEFw/BEESpPV3SzE9pDm3BEHsHAnypvnySGAXLw2c393eH5wf4qaBMUFagQRBELfXMMjX4KTf9ogXuZWyjvF2X2iu+7xfdT9JjinHlJbgLjK1yLFFwUVC3OrbK9c/x5K4FOxkTHDbohSRE/9OEDy0iYuO6VmcYsxZoCX40ojwMl2gBEECJDi2Nx8Xie6xRIIgCIJESUtw1xbaiyS9EARvwQTHiJeUa0mCJ73ESQuYi5vg4U5wrIjzwt+ERctFTfBQJ847ZlfhbaK79Bpaj7vST5D1McSrIo9tUmqvYi3ykH35c/4y55Tzt3ugYgxBkGwIriWSIEEQPLQIrimSIEHwsCIIri2SIEEQBEGQBIlrBHZJv9qxJQiukZuFW9dP8Lpn351nk21KA+f48nC7ySSzC+vrVeob73o/z12cP1qCBEECJLhmbi2uRT/B63QT2nVL67ZbgqzDu9rx5Phd3Eo8y9l10y21V/H9tAQJgiAIWoIEQRAvenunJfhiliDH7+WDrZQIgnixmzQPbuIag+5QgiAIgiRIEARBECRBgiAIgiAJEgRBEMTNBrNDCYIgzghmwu7+/Jx3XmgJEgRBnNd6IAHemHkhCRIEQZAAby1IggRBECRAkiBBEARBkAQJgiCIc4M5hiRBgiAIEiBBEiQIgiABEtdhLi6tTrB+zFkDx6d1PT+t59Z5A9Qv2vds0/fuUs3QpmeZP/NZamguOr6XtU5OmqerVNnf9r1nGcPpv13V+J32fPXfzvJ8J/39sub1qvpevuqO7S/z+y/yna/iOa/TmF4pCRIEQRDEdQPdoQRBEARJkCAIgiBIggRBEARBEiQIgiCIm43/HwBPza7kn7VaVQAAAABJRU5ErkJggg=="></div>

                                                                <div class="logoAnatel"  style="text-align:center"> <label class="textoHomologacao" style="width:34.04mm;text-align:center;">{{item.textocodigohomologacao[0].alternateitemidentificationid}}</label></div>
                                                                <div class="blocoimagem"><img id="urletiqueta" data-ng-src="data:image/png;base64,{{urletiqueta}}" ng-show="urletiqueta != undefined && urletiqueta != null && urletiqueta != ''" style="max-width:100%;display:block;margin:auto;"/></div>
                                                                
                                                            </div>
                                                            <div ng-show="true" ng-if="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta == 9">
                                                                <img data-ng-src="data:image/png;base64,{{urletiqueta}}" ng-show="urletiqueta != undefined && urletiqueta != null && urletiqueta != ''" style="max-width:100%; float:left;"/>
                                                                <span id="Span3" style="float:left;margin-top: -3px;">
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: 0px;" class="descricaoetiqueta">(01) <span ng-if="item.globaltradeitemnumber.toString().length == 13">0</span>{{item.globaltradeitemnumber}}</label>
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -18px;" class="descricaoetiqueta">(713) {{item.registroanvisa[0].alternateitemidentificationid}}</label>
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -28px;" class="descricaoetiqueta serialinicial">(21) {{item.serialinicial}}</label>
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -43px;" class="descricaoetiqueta">(17) {{item.datavencimento}}</label>
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -58px;" class="descricaoetiqueta">(10) {{item.nrlote}}</label>
                                                                </span>
                                                            </div>
                                                            <div ng-show="false" id="etiqueta" class="formatoEtiqueta" ng-if="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta == 9">
                                                                <span style="float:left;">
                                                                    <img id="urletiqueta" class="blocoimagem" data-ng-src="data:image/png;base64,{{urletiqueta}}" ng-show="urletiqueta != undefined && urletiqueta != null && urletiqueta != ''" style="max-width:100%"/>
                                                                </span>
                                                                <span id="Span8" class="blocotexto" style="float:left;">
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: 0px;" class="descricaoetiqueta">(01) <span ng-if="item.globaltradeitemnumber.toString().length == 13">0</span>{{item.globaltradeitemnumber}}</label>
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -18px;" class="descricaoetiqueta">(713) {{item.registroanvisa[0].alternateitemidentificationid}}</label>
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -28px;" class="descricaoetiqueta serialinicial">(21) {{item.serialinicial}}</label>
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -43px;" class="descricaoetiqueta">(17) {{item.datavencimento}}</label>
                                                                    <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -58px;" class="descricaoetiqueta">(10) {{item.nrlote}}</label>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- <button name="gerarpdf" class="btn btn-info btn-sm" type="button" ng-click="gerarPDF()">
                                                        <i class="icon-backward bigger-110"></i>Gerar PDF
                                                    </button>--%>
                                                    <form id="PDFHandler" action='/PDFHandler.ashx' method='POST'>
                                                        <input type='hidden' name='pdfdata' id='pdfdata' value='' />
                                                    </form>
                                                    <form id="ImageHandler" action='/ImageHandler.ashx' method='POST'>
                                                        <input type='hidden' name='imagedata' id='imagedata' value='' />
                                                    </form>

                                                </div>
                                        </div>

                                        <div class="form-actions center">
                                            <button name="voltar" class="btn btn-info btn-sm" type="button" ng-click="voltarParaAba(2)">
                                                <i class="icon-backward bigger-110"></i><span translate="17.BTN_Voltar">Voltar</span>
                                            </button>
                                            <button name="salvaretiqueta" class="btn btn-info btn-sm" type="button" ng-click="salvarCodigoBarras();">
                                                <i class="icon-download-alt bigger-110"></i><span translate="17.BTN_BaixarImagem">Baixar Imagem</span>
                                            </button>
                                            <button class="btn btn-info btn-sm" ng-click="onSave(item)" type="button" ng-disabled="item.tipoetiqueta == 10 || item.tipoetiqueta == 18">
                                                <i class="icon-save bigger-110"></i><span translate="17.BTN_ImprimirEtiqueta">Imprimir Etiqueta</span>
                                            </button>
                                            <button name="cancel" ng-click="onCancel(itemForm)" ng-show="!wasGenerated" class="btn btn-danger btn-sm" type="button">
                                                <i class="icon-remove bigger-110"></i><span translate="17.BTN_Cancelar">Cancelar</span>
                                            </button>
                                            <button id="Button4" name="fechar" ng-click="onCancel(itemForm)" ng-show="wasGenerated" class="btn btn-danger btn-sm" type="button">
                                                <i class="icon-remove bigger-110"></i><span translate="17.BTN_Fechar">Fechar</span>
                                            </button>
                                        </div>
                                    </div>

								</div>
							</div><!-- /widget-main -->

                            <div class="widget-main no-padding" ng-show="isPesquisa && !initMode">
                                <form name="form.pesquisa" id="form" novalidate class="form" role="form">
                                    <div class="tab-content">
                                        <div id="papel" class="tab-pane active">
                                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="globaltradeitemnumber" translate="17.LBL_NumeroGTIN">Número de Identificação GTIN </label>
                                                        <input type="text" class="form-control input-sm" maxlength="14" id="Text1" name="globaltradeitemnumber" ng-model="itemForm.globaltradeitemnumber" integer
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_NumeroGTIN' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.codigotipogeracao.$invalid && form.submitted}">
                                                        <label for="codigostatusproduto">
                                                            <span translate="17.GRID_TipoGeracao">Tipo de Geração</span> <span ng-show='!searchMode'>(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="Select5" name="codigotipogeracao" ng-model="itemForm.codigotipogeracao" ng-options="tipogeracao.codigo as tipogeracao.nome for tipogeracao in tiposgeracao"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_TipoGeracao' | translate}}">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="codigotipocodigobarras" translate="17.GRID_TipoCodigoBarras">Tipo de Código de Barras </label>
                                                        <select class="form-control input-sm" id="codigotipocodigobarras" name="codigotipocodigobarras" ng-model="itemForm.codigotipocodigobarras" ng-options="codigobarra.codigo as codigobarra.name for codigobarra in codigosbarra"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_TipoCodigoBarras' | translate}}">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="brandname" translate="17.LBL_TipoEtiqueta">Tipo de Etiqueta </label>
                                                        <select class="form-control input-sm" id="Select4" name="codigoetiqueta" ng-model="itemForm.codigoetiqueta" ng-options="modeloetiqueta.codigo as modeloetiqueta.nome for modeloetiqueta in modeloetiquetas"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_TipoEtiqueta' | translate}}">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="codigofabricante" translate="17.LBL_Fabricante">Fabricante </label>
                                                        <select class="form-control input-sm" id="Select3" name="codigofabricante" ng-model="itemForm.codigofabricante" ng-options="fabricante.codigo as fabricante.nome for fabricante in fabricantes"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fabricante' | translate}}">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-12 col-xs-12">
                                                        <label for="productdescription" translate="17.LBL_DescricaoProduto">Descrição do Produto </label>
                                                        <textarea class="form-control input-sm" id="productdescription" sbx-maxlength="300" name="productdescription" rows="3" ng-model="itemForm.productdescription" style="resize: vertical;" required
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_DescricaoProduto' | translate}}"></textarea>
                                                        <span style="font-size: 80%;">(<span translate="17.LBL_TamanhoMaximo">Tamanho máximo</span>: 300)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <span translate="17.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.pesquisa.productdescription.$viewValue.length || 0 }}</span>
                                                    </div>
                                                </div>
                                                <div class="form-actions center" ng-show="editMode">
                                                    <button class="btn btn-info btn-sm" name="find" scroll-to="" ng-cloak ng-show="isPesquisa" ng-click="onSearch(itemForm)" type="button">
                                                        <i class="icon-search bigger-110"></i><span translate="17.BTN_Pesquisar">Pesquisar</span>
                                                    </button>
                                                    <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                                        <i class="icon-remove bigger-110"></i><span translate="17.BTN_Cancelar">Cancelar</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="widget-main no-padding" ng-if="!isPesquisa && !initMode">
                                <form name="form.edicao" id="form1" novalidate class="form" role="form">
                                    <div class="tab-content" style="min-height:330px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                        <div ng-show="item.codigoproduto != undefined && item.codigoproduto != null">
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label translate="17.LBL_Produto">Produto</label>
                                                    <input type="text" class="form-control input-sm fieldDisabled" id="Text2" name="productdescription" ng-model="item.productdescription" ng-disabled="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Produto' | translate}}"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <label ng-show="item.globaltradeitemnumber != undefined && item.globaltradeitemnumber != null" translate="17.LBL_GTIN">GTIN</label>
                                                    <input type="text" class="form-control input-sm fieldDisabled" id="Text13" name="globaltradeitemnumber" ng-model="item.globaltradeitemnumber" ng-disabled="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_GTIN' | translate}}"/>
                                                </div>
                                                <div class="form-group col-md-3" ng-show="item.alternateitemidentificationid != undefined && item.alternateitemidentificationid != null && item.alternateitemidentificationid != ''">
                                                    <label translate="17.GRID_CodigoInterno">Código Interno</label>
                                                    <input type="text" class="form-control input-sm fieldDisabled" id="Text14" name="alternateitemidentificationid" ng-model="item.alternateitemidentificationid" ng-disabled="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_CodigoInterno' | translate}}"/>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label translate="17.LBL_TipoGTIN">Tipo GTIN</label>
                                                    <input type="text" class="form-control input-sm fieldDisabled" id="Text15" name="nometipogtin" ng-model="item.nometipogtin" ng-disabled="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_TipoGTIN' | translate}}"/>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label translate="17.LBL_StatusGTIN">Status GTIN</label>
                                                    <input type="text" class="form-control input-sm fieldDisabled" id="Text16" name="nomestatusgtin" ng-model="item.nomestatusgtin" ng-disabled="true" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_StatusGTIN' | translate}}"/>
                                                </div>
                                                
                                            </div>

                                            <div class="row"> 
                                                            
                                                 <div class="form-group col-md-3">
                                                    <label translate="17.GRID_TipoCodigoBarras">Tipo de Código de Barras</label>
                                                    <input type="text" class="form-control input-sm fieldDisabled" id="Text17" name="nometipoetiqueta" ng-model="item.nometipoetiqueta" ng-disabled="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_TipoCodigoBarras' | translate}}"/>
                                                </div>

                                                <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 1'>                                                            
                                                    <label>
                                                        <span translate="17.GRID_ModeloCodigoBarras">Modelo</span>
                                                    </label>
                                                  
                                                    <select class="form-control input-sm" id="Select22" name="modelo128" ng-model="item.modelo128" required ng-disabled="true" ng-readonly="true"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.GRID_ModeloCodigoBarras' | translate}}">
                                                        <option value=""></option>
                                                        <option value="1" ng-selected='item.modelo128==1'>Padrão</option>
                                                        <option value="2" ng-selected='item.modelo128==2'>Anatel</option>
                                                    </select>
                                                   
                                                </div>  
                                                                                                                                           
                                                <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 1 || item.tipoetiqueta == 10 || item.tipoetiqueta == 12'>                                                                                                                       
                                                    <label for="nrlote" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_NumeroLote' | translate}}">
                                                        <span translate="47.LBL_NumeroLote">Lote</span> <span ng-show="!searchMode">(*)</span>
                                                    </label>
                                                    
                                                    <input type="text" class="form-control input-sm fieldDisabled" name="nrlote" id="Text20" ng-model="item.nrlote" maxlength="20"  required ng-disabled="true" ng-readonly="true" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataVencimento' | translate}}"/>                                                                                                                                                                                
                                                </div>

                                                <div class="form-group col-md-3" ng-show='item.tipoetiqueta == 1 || item.tipoetiqueta == 10 || item.tipoetiqueta == 12'>                                                                                                                       
                                                    <label for="datavencimento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataVencimento' | translate}}">
                                                        <span translate="47.LBL_DataVencimento">Data de Vencimento</span> <span ng-show="!searchMode">(*)</span>                                                                
                                                    </label>
                                                    
                                                    <input type="text" class="form-control input-sm fieldDisabled" name="datavencimento" id="Text21" ng-model="item.datavencimento" maxlength="10"  required ng-disabled="true" ng-readonly="true" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataVencimento' | translate}}"/>                                                                                                                              
                                                   
                                                </div>  
                                                
                                                                                              
                                            </div>

                                             <div class="row" ng-show="item.tipoetiqueta != undefined && item.tipoetiqueta != null && item.tipoetiqueta != '' && item.tipoetiqueta != 10 && item.tipoetiqueta != 18">
                                                <div class="form-group col-md-4">
                                                    <label><span translate="17.LBL_Fabricante">Fabricante</span> <span ng-show='!searchMode'>(*)</span></label>
                                                    <span class="row">
                                                        <select ng-change="buscaEtiquetasFabricante(item)" class="form-control input-sm" id="Select6" name="codigofabricante" ng-model="item.codigofabricante" ng-options="fabricante.codigo as fabricante.nome for fabricante in fabricantes"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fabricante' | translate}}">
                                                            <option value=""></option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="form-group col-md-4">
                                                     <label><span translate="17.LBL_Etiquetas">Etiquetas</span> <span ng-show='!searchMode'>(*)</span></label>
                                                    <span class="row">
                                                        <select ng-change="visualizaEtiqueta(item)" class="form-control input-sm" id="Select7" name="codigoetiqueta" ng-model="item.codigoetiqueta" ng-options="etiqueta.codigo as etiqueta.nome + ' (' + etiqueta.largura + ' x ' + etiqueta.altura + ' - ' + etiqueta.quantidadelinhas+ ' linhas x ' + etiqueta.quantidadecolunas+ ' colunas)'  for etiqueta in etiquetas"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Etiquetas' | translate}}">
                                                            <option value=""></option>
                                                        </select>
                                                    </span>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label><span translate="17.LBL_Quantidade">Quantidade</span> <span ng-show='!searchMode'>(*)</span></label>
                                                        <select class="form-control input-sm" id="Select9" name="codigofabricante" ng-model="item.quantidadeetiqueta" ng-options="qtd as qtd for qtd in item.quantidadesetiquetas"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Quantidade' | translate}}">
                                                            <option value=""></option>
                                                        </select>
                                                    </span>
                                                </div>
                                            </div>


                                           



                                             <h3 class="header smaller lighter purple" ng-show="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta != 1  && item.tipoetiqueta != 9  && item.tipoetiqueta != 10  && item.tipoetiqueta != 12  && item.tipoetiqueta != 18 " translate="17.LBL_OpcoesFormato">
                                                Opções de formato (Opcional)
                                            </h3>
                                            <div class="row" ng-show="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta != 1 && item.tipoetiqueta != 9"">
                                                <div class="form-group col-md-2">
                                                    <label><span translate="17.LBL_Tipo">Tipo</span> <span ng-show='!searchMode'>(*)</span></label>
                                                    <select class="form-control input-sm" id="Select8" ng-model="item.codigotipogeracao" name="codigotipogeracao" required
                                                        ng-options="tipo.codigo as tipo.nome for tipo in tiposgeracao" ng-change="atualizaPosicao()"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Tipo' | translate}}">
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-2" ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">
                                                    <label translate="17.LBL_Posicao">Posição</label>
                                                    <select ng-change="atualizaPosicao()" class="form-control input-sm" id="Select10" ng-model="item.codigoposicao" name="codigoposicao" required ng-options="posicao.codigo as posicao.nome for posicao in posicoestexto" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Posicao' | translate}}"/>

                                                </div>
                                                <div class="form-group col-md-6">
                                                </div>
                                            </div>
                                            <div class="row" ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">
                                                <div class="form-group col-md-5">
                                                    <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_DescricaoEtiqueta' | translate}}" translate="17.LBL_DescricaoEtiqueta">Descrição da etiqueta</label>
                                                    <br>
                                                    <textarea class="col-md-12" id="Textarea1" maxlength="255" name="descricao" ng-model="item.descricao" rows="2" style="resize: vertical;" ng-disabled="item.copiar"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_DescricaoEtiqueta' | translate}}"/>
                                                </div>
                                                    <div class="form-group col-md-2">
                                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Fonte' | translate}}" translate="17.LBL_Fonte">Fonte</label><br>
                                                            <select class="form-control input-sm" id="Select13" name="fontenomedescricao" ng-model="item.fontenomedescricao" ng-change="atualizaAlinhamento()"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fonte' | translate}}">
                                                                <option label="Arial" value="Arial" selected="selected">Arial</option>
                                                                <option label="Arial Black" value="Arial Black">Arial Black</option>
                                                                <option label="Comic Sans MS" value="Comic Sans MS">Comic Sans MS</option>
                                                                <option label="Courier New" value="Courier New">Courier New</option>
                                                                <option label="Georgia" value="Georgia">Georgia</option>
                                                                <option label="Helvetica" value="Helvetica">Helvetica</option>
                                                                <option label="Helvetica Neue" value="Helvetica Neue" >Helvetica Neue</option>
                                                                <option label="Impact" value="Impact">Impact</option><option label="Lucida Console" value="Lucida Console">Lucida Console</option>
                                                                <option label="Lucida Sans Unicode" value="Lucida Sans Unicode">Lucida Sans Unicode</option>
                                                                <option label="Palatino Linotype" value="Palatino Linotype">Palatino Linotype</option>
                                                                <option label="Tahoma" value="Tahoma">Tahoma</option><option label="Times New Roman" value="Times New Roman">Times New Roman</option>
                                                                <option label="Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
                                                                <option label="Verdana" value="Verdana">Verdana</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-1">
                                                            <label>&nbsp;</label><br>
                                                                <select class="form-control input-sm" id="Select14" name="fontetamanhodescricao" ng-model="item.fontetamanhodescricao" ng-change="atualizaAlinhamento()"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fonte' | translate}}">
                                                                <option label="8px" value="8" ng-selected="item.fontetamanhodescricao == 8">8px</option>
                                                                <option label="9px" value="9" ng-selected="item.fontetamanhodescricao == 9">9px</option>
                                                                <option label="10px" value="10" ng-selected="item.fontetamanhodescricao == 10">10px</option>
                                                                <option label="11px" value="11" ng-selected="item.fontetamanhodescricao == 11">11px</option>
                                                                <option label="12px" value="12" ng-selected="item.fontetamanhodescricao == 12">12px</option>
                                                                <option label="13px" value="13" ng-selected="item.fontetamanhodescricao == 13">13px</option>
                                                                <option label="14px" value="14" ng-selected="item.fontetamanhodescricao == 14">14px</option>
                                                                <option label="15px" value="15" ng-selected="item.fontetamanhodescricao == 15">15px</option>
                                                                <option label="16px" value="16" ng-selected="item.fontetamanhodescricao == 16">16px</option>
                                                                <option label="18px" value="18" ng-selected="item.fontetamanhodescricao == 18">18px</option>
                                                                <option label="24px" value="24" ng-selected="item.fontetamanhodescricao == 24">24px</option>
                                                                <option label="32px" value="32" ng-selected="item.fontetamanhodescricao == 32">32px</option>
                                                                <option label="48px" value="48" ng-selected="item.fontetamanhodescricao == 48">48px</option>
                                                                <option label="60px" value="60" ng-selected="item.fontetamanhodescricao == 60">60px</option>
                                                                <option label="72px" value="72" ng-selected="item.fontetamanhodescricao == 72">72px</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Alinhamento' | translate}}" translate="17.LBL_Alinhamento">Alinhamento</label>
                                                             <div class="input-group ">
                                                                <div class="btn-group">  <%--data-toggle="buttons"--%>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentodescricao" id="Radio1" ng-model="item.alinhamentodescricao" value="left" ng-change="atualizaAlinhamento()"><i class="icon-align-left bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentodescricao" id="Radio2" ng-model="item.alinhamentodescricao" value="center" ng-change="atualizaAlinhamento()"><i class="icon-align-center bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentodescricao" id="Radio3" ng-model="item.alinhamentodescricao" value="right" ng-change="atualizaAlinhamento()"><i class="icon-align-right bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentodescricao" id="Radio4" ng-model="item.alinhamentodescricao" value="justify" ng-change="atualizaAlinhamento()"><i class="icon-align-justify bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row" ng-if="item.codigotipogeracao == 3 || item.codigotipogeracao == 2">
                                                        <div class="form-group col-md-3">
                                                            <span style="font-size: 80%;">(<span translate="17.LBL_TamanhoMaximo">Tamanho máximo</span>: 255)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span translate="17.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ item.descricao.length || 0 }}</span>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <input type="checkbox" class="ace" name="copiar" ng-click="item.descricao=item.productdescription"  ng-init="item.descricao=item.productdescription; item.copiar = true" ng-model="item.copiar" ng-checked="true">
                                                            <label class="lbl">&nbsp; <span data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_CopiaDescricao' | translate}}"  translate="17.LBL_CopiaDescricao">Copia descrição</span>?</label>
                                                        </div>
                                                    </div>
                                                    <div class="row" ng-if="item.codigotipogeracao == 3 || item.codigotipogeracao == 2">
                                                        <div class="form-group col-md-5">
                                                            <span ng-if="item.codigotipogeracao == 3">
                                                                <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_TextoLivre' | translate}}" translate="17.LBL_TextoLivre">Texto livre na etiqueta</label><br>
                                                                <textarea class="col-md-12" id="Textarea2" maxlength="300" name="textolivre" ng-model="item.textolivre" rows="2" style="resize: vertical;"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_TextoLivre' | translate}}"/>
                                                            </span>
                                                        </div>
                                                        <div class="form-group col-md-2" ng-if="item.codigotipogeracao == 3">
                                                                    <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Fonte' | translate}}" translate="17.LBL_Fonte">Fonte</label><br>
                                                                    <select class="form-control input-sm" id="Select19" name="fontenometextolivre" ng-model="item.fontenometextolivre" ng-change="atualizaAlinhamento()"
                                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fonte' | translate}}">
                                                                        <option label="Arial" value="Arial" selected="selected">Arial</option>
                                                                        <option label="Arial Black" value="Arial Black">Arial Black</option>
                                                                        <option label="Comic Sans MS" value="Comic Sans MS">Comic Sans MS</option>
                                                                        <option label="Courier New" value="Courier New">Courier New</option>
                                                                        <option label="Georgia" value="Georgia">Georgia</option>
                                                                        <option label="Helvetica" value="Helvetica">Helvetica</option>
                                                                        <option label="Helvetica Neue" value="Helvetica Neue" >Helvetica Neue</option>
                                                                        <option label="Impact" value="Impact">Impact</option><option label="Lucida Console" value="Lucida Console">Lucida Console</option>
                                                                        <option label="Lucida Sans Unicode" value="Lucida Sans Unicode">Lucida Sans Unicode</option>
                                                                        <option label="Palatino Linotype" value="Palatino Linotype">Palatino Linotype</option>
                                                                        <option label="Tahoma" value="Tahoma">Tahoma</option><option label="Times New Roman" value="Times New Roman">Times New Roman</option>
                                                                        <option label="Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
                                                                        <option label="Verdana" value="Verdana">Verdana</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-1" ng-if="item.codigotipogeracao == 3">
                                                                    <label>&nbsp;</label><br>
                                                                     <select class="form-control input-sm" id="Select20" name="fontetamanhotextolivre" ng-model="item.fontetamanhotextolivre" ng-change="atualizaAlinhamento()"
                                                                     data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.17.LBL_Fonte' | translate}}">
                                                                        <option label="8px" value="8" ng-selected="item.fontetamanhotextolivre == 8">8px</option>
                                                                        <option label="9px" value="9" ng-selected="item.fontetamanhotextolivre == 9">9px</option>
                                                                        <option label="10px" value="10" ng-selected="item.fontetamanhotextolivre == 10">10px</option>
                                                                        <option label="11px" value="11" ng-selected="item.fontetamanhotextolivre == 13">11px</option>
                                                                        <option label="12px" value="12" ng-selected="item.fontetamanhotextolivre == 16">12px</option>
                                                                        <option label="13px" value="13" ng-selected="item.fontetamanhotextolivre == 18">13px</option>
                                                                        <option label="14px" value="14" ng-selected="item.fontetamanhotextolivre == 24">14px</option>
                                                                        <option label="15px" value="15" ng-selected="item.fontetamanhotextolivre == 32">15px</option>
                                                                        <option label="16px" value="16" ng-selected="item.fontetamanhotextolivre == 48">16px</option>
                                                                        <option label="18px" value="18" ng-selected="item.fontetamanhotextolivre == 60">18px</option>
                                                                        <option label="24px" value="24" ng-selected="item.fontetamanhotextolivre == 72">24px</option>
                                                                        <option label="32px" value="32" ng-selected="item.fontetamanhotextolivre == 72">32px</option>
                                                                        <option label="48px" value="48" ng-selected="item.fontetamanhotextolivre == 72">48px</option>
                                                                        <option label="60px" value="60" ng-selected="item.fontetamanhotextolivre == 72">60px</option>
                                                                        <option label="72px" value="72" ng-selected="item.fontetamanhotextolivre == 72">72px</option>
                                                                    </select>
                                                                </div>
                                                        <div class="form-group col-md-3" ng-if="item.codigotipogeracao == 3">
                                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_Alinhamento' | translate}}" translate="17.LBL_Alinhamento">Alinhamento</label>
                                                             <div class="input-group ">
                                                                <div class="btn-group"> <%--data-toggle="buttons"--%>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentotextolivre" id="Radio4" ng-model="item.alinhamentotextolivre" value="left" ng-change="atualizaAlinhamento()"><i class="icon-align-left bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentotextolivre" id="Radio5" ng-model="item.alinhamentotextolivre" value="center" ng-change="atualizaAlinhamento()"><i class="icon-align-center bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentotextolivre" id="Radio6" ng-model="item.alinhamentotextolivre" value="right" ng-change="atualizaAlinhamento()"><i class="icon-align-right bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                    <label class="btn btn-link alinhamento">
                                                                        <input type="radio" name="alinhamentotextolivre" id="Radio7" ng-model="item.alinhamentotextolivre" value="justify" ng-change="atualizaAlinhamento()"><i class="icon-align-justify bigger-110 iconealinhamento"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" ng-if="item.codigotipogeracao == 3">
                                                        <div class="form-group col-md-10">
                                                            <span style="font-size: 80%;">(<span translate="17.LBL_TamanhoMaximo">Tamanho máximo</span>: 300)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span translate="17.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ item.textolivre.length || 0 }}</span>
                                                        </div>
                                                    </div>

                                                    <h3 class="header smaller lighter purple" translate="17.LBL_PreVisualizacaoEtiqueta">
                                                        Pré-Visualização da Etiqueta
                                                    </h3>



                                                    <div class="row" ng-if="item.tipoetiqueta == 10 || item.tipoetiqueta == 18">                                                      
                                                            <div class="form-group col-md-12" ng-show="item.tipoetiqueta != undefinded && item.tipoetiqueta != null && item.tipoetiqueta != ''">

                                                                <div class="row">
                                                                    <div class="form-group col-md-12 pull-left">
                                                                        <br>
                                                                            <img data-ng-src="data:image/png;base64,{{urlcodigobarras}}" ng-if="urlcodigobarras != undefined && urlcodigobarras != null && urlcodigobarras != ''"/>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <div class="row" ng-if="item.tipoetiqueta != 10 && item.tipoetiqueta != 18">

                                                        



                                                        <div class="form-group col-md-12">

                                                            <div ng-show="item.codigoetiqueta == undefined || item.codigoetiqueta == null || item.codigoetiqueta == '' ">
                                                                <label translate="17.LBL_NenhumaEtiquetaSelecionada">Nenhuma etiqueta selecionada.</label>
                                                            </div>
                                                            <div id="Div1" class="formatoEtiqueta" ng-show="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta != 1 && item.tipoetiqueta != 9" >
                                                                <div id="Div2" ng-show="item.codigoposicao == 4">
                                                                    <div class="blocotexto">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;" class="descricaoetiqueta" ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">{{item.descricao}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:8px !important;" class="textoetiqueta" ng-if="item.codigotipogeracao == 3">{{item.textolivre}}</label>
                                                                    </div>
                                                                </div>
                                                                <div id="Div3">
                                                                    <span id="Span1" class="blocotexto" ng-show="item.codigoposicao == 2">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;" class="descricaoetiqueta"ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">{{item.descricao}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:8px !important;" class="textoetiqueta" ng-if="item.codigotipogeracao == 3">{{item.textolivre}}</label>
                                                                    </span>
                                                                    <span class="blocoimagem">
                                                                        <img id="urletiqueta" class="blocoimagem" data-ng-src="data:image/png;base64,{{urletiqueta}}" ng-show="urletiqueta != undefined && urletiqueta != null && urletiqueta != ''" style="max-width:100%"/>
                                                                    </span>
                                                                    <span id="Span2" class="blocotexto" ng-show="item.codigoposicao == 1">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;" class="descricaoetiqueta"ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">{{item.descricao}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:8px !important;" class="textoetiqueta" ng-if="item.codigotipogeracao == 3">{{item.textolivre}}</label>
                                                                    </span>
                                                                </div>
                                                                <div id="Div4" ng-show="item.codigoposicao == 3">
                                                                    <div class="blocotexto">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;" class="descricaoetiqueta" ng-if="item.codigotipogeracao == 2 || item.codigotipogeracao == 3">{{item.descricao}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:8px !important;" class="textoetiqueta" ng-if="item.codigotipogeracao == 3">{{item.textolivre}}</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="Div6" class="formatoEtiqueta" ng-if="item.codigoetiqueta != undefined && item.codigoetiqueta != null && item.codigoetiqueta != '' && item.tipoetiqueta == 9">
                                                                    <span class="blocoimagem">
                                                                        <img id="Img1" class="blocoimagem" data-ng-src="data:image/png;base64,{{urletiqueta}}" ng-show="urletiqueta != undefined && urletiqueta != null && urletiqueta != ''" style="max-width:100%"/>
                                                                    </span>
                                                                    <span id="Span4" class="blocotexto">
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: 0px;" class="descricaoetiqueta">(01) {{item.globaltradeitemnumber}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -18px;" class="descricaoetiqueta">(713) {{item.registroanvisa[0].alternateitemidentificationid}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -28px;" class="descricaoetiqueta serialinicial">(21) {{item.serialinicial}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -43px;" class="descricaoetiqueta">(17) {{item.datavencimento}}</label>
                                                                        <label style="width: 100%;text-align:left;font-family:Arial !important;font-size:10px !important;margin-top: -58px;" class="descricaoetiqueta">(10) {{item.nrlote}}</label>
                                                                    </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    

                                            <form id="Form2" action='/PDFHandler.ashx' method='POST'>
                                                <input type='hidden' name='pdfdata' id='Hidden1' value='' />
                                            </form>
                                           </div>
                                    </div>


                                </form>
                                <div class="form-actions center" ng-show="editMode">
                                    <button name="salvaretiqueta" class="btn btn-info btn-sm" type="button" ng-click="salvarCodigoBarras();">
                                        <i class="icon-download-alt bigger-110"></i><span translate="17.BTN_BaixarImagem">Baixar Imagem</span>
                                    </button>
                                    <button class="btn btn-info btn-sm" ng-cloak ng-show="!isPesquisa && item.tipoetiqueta != 10 || item.tipoetiqueta != 18" ng-click="onSave(item)" type="button" id="ImprimirEtiqueta">
                                        <i class="icon-save bigger-110"></i><span translate="17.BTN_ImprimirEtiqueta">Imprimir Etiqueta</span>
                                    </button>
                                  
                                    <button id="Button2" name="cancel" ng-click="onCancel(item)" ng-show="!wasGenerated" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="17.BTN_Cancelar">Cancelar</span>
                                    </button>
                                    <button id="Button3" name="fechar" ng-click="onCancel(itemForm)" ng-show="wasGenerated" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="17.BTN_Fechar">Fechar</span>
                                    </button>
                                </div>

                            </div>
						</div><!-- /widget-body -->
					</div>
				</div>
			</div>
        </div>
        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" translate="17.LBL_GeracaoEtiqueta"></label>
        </div>
        <div class="widget-box">
            <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '17.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
                <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '17.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
                <ajuda codigo="51" tooltip-ajuda="'17.TOOLTIP_Ajuda'"></ajuda>
            </div>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 1910px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="17.LBL_CliqueVisualizarEtiqueta">
                            Clique em um registro para visualizar e/ou imprimir a etiqueta
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableEtiquetas" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
	                            <tr>
                                <th ng-click="tableEtiquetas.sorting('codigo', tableEtiquetas.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                  <span>#</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('codigo', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('codigo', 'desc')
                                  }"></i>
                                </th>
		                            <th ng-click="tableEtiquetas.sorting('dataimpressao', tableEtiquetas.isSortBy('dataimpressao', 'asc') ? 'desc' : 'asc')">
                                  <span translate="17.GRID_DataGeracao">Data de Geração</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('dataimpressao', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('dataimpressao', 'desc')
                                  }"></i>
                                </th>
		                            <th ng-click="tableEtiquetas.sorting('globaltradeitemnumber', tableEtiquetas.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                  <span translate="17.GRID_GTIN">GTIN</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('globaltradeitemnumber', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('globaltradeitemnumber', 'desc')
                                  }"></i>
                                </th>
		                            <th ng-click="tableEtiquetas.sorting('nometipogeracao', tableEtiquetas.isSortBy('nometipogeracao', 'asc') ? 'desc' : 'asc')">
                                  <span translate="17.GRID_TipoGeracao">Tipo de Geração</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('nometipogeracao', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('nometipogeracao', 'desc')
                                  }"></i>
                                </th>
                                <th ng-click="tableEtiquetas.sorting('productdescription', tableEtiquetas.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                  <span translate="17.GRID_Produto">Produto</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('productdescription', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('productdescription', 'desc')
                                  }"></i>
                                </th>
                                <th ng-click="tableEtiquetas.sorting('nometipogtin', tableEtiquetas.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')">
                                  <span translate="17.GRID_TipoGTIN">Tipo de GTIN</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('nometipogtin', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('nometipogtin', 'desc')
                                  }"></i>
                                </th>
                                <th ng-click="tableEtiquetas.sorting('nometipoetiqueta', tableEtiquetas.isSortBy('nometipoetiqueta', 'asc') ? 'desc' : 'asc')">
                                  <span translate="17.GRID_TipoCodigoBarras">Tipo de Código de Barras</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('nometipoetiqueta', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('nometipoetiqueta', 'desc')
                                  }"></i>
                                </th>
                                <th ng-click="tableEtiquetas.sorting('nomemodeloetiqueta', tableEtiquetas.isSortBy('nomemodeloetiqueta', 'asc') ? 'desc' : 'asc')">
                                  <span translate="17.GRID_Etiqueta">Etiqueta</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('nomemodeloetiqueta', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('nomemodeloetiqueta', 'desc')
                                  }"></i>
                                </th>
                                <th ng-click="tableEtiquetas.sorting('nomeusuario', tableEtiquetas.isSortBy('nomeusuario', 'asc') ? 'desc' : 'asc')">
                                  <span translate="17.GRID_Usuario">Usuário</span>&nbsp;&nbsp;
                                  <i class="text-center icon-sort" ng-class="{
                                    'icon-sort-up': tableEtiquetas.isSortBy('nomeusuario', 'asc'),
                                    'icon-sort-down': tableEtiquetas.isSortBy('nomeusuario', 'desc')
                                  }"></i>
                                </th>
	                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '7%' }" sortable="'dataimpressao'">{{item1.dataimpressao | date: 'dd/MM/yyyy'}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'nometipogeracao'">{{item1.nometipogeracao}}</td>
                                    <td ng-style="{ 'width': '23%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                    <td ng-style="{ 'width': '7%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'nometipoetiqueta'">{{item1.nometipoetiqueta}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'nomemodeloetiqueta'">{{item1.nomemodeloetiqueta}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'nomeusuario'">{{item1.nomeusuario}}</td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="10" class="text-center" translate="17.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div ng-cloak ng-show="!flagAssociadoSelecionado">
    <h1 translate="17.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
    <h4 translate="17.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
</div>

    <div id="modal">
        <script type="text/ng-template" id="modalProdutoGeracaoEtiqueta">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content" on-enter="onFilter(itemForm)">
                <div class="modal-header">
                    <h3 class="modal-title"><span translate="17.LBL_SelecionarProduto">Selecionar Produto</span> <popup-ajuda codigo="55" tooltip-ajuda="'17.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form name="form.gtin" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue" translate="17.LBL_PreenchaUmAbaixo">Preencha ao menos um dos campos abaixo:</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label translate="17.LBL_ProdutoGTIN">Produto/GTIN </label>
                                <!--<input id="typea" type="text" ng-model="itemForm.pesquisaProduto" maxlength="320" placeholder="{{ '17.PLACEHOLDER_DigiteGTIN' | translate }}"
                                            typeahead="produtosAutoComplete as produtosAutoComplete.globaltradeitemnumber +' - '+ produtosAutoComplete.productdescription for produtosAutoComplete in BuscarProdutosAutoComplete($viewValue)"
                                            typeahead-min-length="3" class="typeahead form-control input-sm"  typeahead-on-select="onFilter(itemForm)"/> -->
                                <input id="typea" type="text" ng-model="itemForm.pesquisaProduto" maxlength="320" class="typeahead form-control input-sm"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_ProdutoGTIN' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label translate="17.LBL_TipoGTIN">Tipo de GTIN </label>
                                <select class="form-control input-sm" id="tipogtin" name="tipogtin" ng-model="itemForm.codigotipogtin" ng-options="tipogtin.codigo as tipogtin.nome for tipogtin in tiposgtin"
                                ata-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_TipoGTIN' | translate}}">
                                   <option value="">Todos</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label translate="17.LBL_StatusGTIN">Status do GTIN </label>
                                <select class="form-control input-sm" id="statusgtin" name="statusgtin" ng-model="itemForm.codigostatusgtin" 
                                ata-toggle="popover" data-trigger="hover" data-content="{{'t.17.LBL_StatusGTIN' | translate}}"> <!--ng-options="status.codigo as status.nome for status in statusgtin"-->
                                    <option value="">Todos</option>
                                    <option ng-repeat="status in statusgtin" value="{{status.codigo}}" ng-if="usuarioLogado.id_tipousuario != 2 || (usuarioLogado.id_tipousuario == 2 && status.codigo != 7)">{{status.nome}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="center">
                            <button name="find" class="btn btn-info btn-sm" type="button" ng-click="onFilter(itemForm)">
                                <i class="icon-ok bigger-110"></i><span translate="17.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <div class="table-responsive" style="margin-bottom: 20px;">
                                <table ng-table="tableProdutos" class="table table-bordered table-striped table-hover">
                                    <thead>
	                                    <tr>
		                                    <th ng-click="tableProdutos.sorting('globaltradeitemnumber', tableProdutos.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                          <span translate="17.GRID_GTIN">GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('globaltradeitemnumber', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('globaltradeitemnumber', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableProdutos.sorting('productdescription', tableProdutos.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                          <span translate="17.GRID_Produto">Produto</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('productdescription', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('productdescription', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableProdutos.sorting('nometipogtin', tableProdutos.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')">
                                          <span translate="17.GRID_TipoGTIN">Tipo de GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('nometipogtin', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('nometipogtin', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableProdutos.sorting('nomestatusgtin', tableProdutos.isSortBy('nomestatusgtin', 'asc') ? 'desc' : 'asc')">
                                          <span translate="17.LBL_StatusGTIN">Status GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('nomestatusgtin', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('nomestatusgtin', 'desc')
                                          }"></i>
                                        </th>
		                                    <th><span translate="17.GRID_Selecionar">Selecionar</span></th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data">
                                            <td ng-style="{ 'width': '15%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                            <td ng-style="{ 'width': '55%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                            <td ng-style="{ 'width': '15%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                            <td ng-style="{ 'width': '15%' }" sortable="'nomestatusgtin'">{{item1.nomestatusgtin}}</td>
                                            <td ng-style="{ 'width': '5%' }" class="action-buttons center">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedProduto(item1)">
                                                    <i class="icon-ok bigger-110"></i><span translate="17.GRID_Selecionar">Selecionar</span>
                                                </button>
                                            </td>

                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="5" class="text-center" translate="17.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()"> <i class="icon-remove bigger-110"></i><span translate="17.BTN_Fechar">Fechar</span></button>
                </div>
            </div>
        </script>
    </div>





    <div id="Div5">
        <script type="text/ng-template" id="modalLoteGeracaoEtiqueta">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content" on-enter="onFilter(item)">
                <div class="modal-header">
                    <h3 class="modal-title"><span translate="17.LBL_SelecionarLote">Selecionar Lote</span> <popup-ajuda codigo="55" tooltip-ajuda="'17.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form name="form.gtin" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue" translate="17.LBL_PreenchaUmAbaixo">Preencha ao menos um dos campos abaixo:</div>
                        </div>

                        <div class="row">                                                                                   
                            <div class="col-md-3 col-xs-12 form-group" ng-class="{'has-error': form.dataprocessamento.$invalid && form.submitted}">
                                <label for="dataprocessamento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataProcessamento' | translate}}" translate="47.LBL_DataProcessamento">Data de Processamento </label><br />
                                <span translate="47.LBL_De">De</span> 
                                <input type="text" id="Text1" name="datacertificado" ng-model="item.pesquisaLote.de_dataprocessamento" maxlength="10" class="form-control input-sm datepicker"
                                    sbx-datepicker data-date-format="dd/mm/yyyy"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_De' | translate}}"/>
                            </div>
                            <div class="col-md-3 col-xs-12 form-group" ng-class="{'has-error': form.datavencimento.$invalid && form.submitted}" style="margin-top: 29px;">
                                <span translate="47.LBL_Ate">Até</span> 
                                <input type="text" name="datavencimento" id="Text2" ng-model="item.pesquisaLote.ate_dataprocessamento" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy"
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_Ate' | translate}}"/>
                            </div>
                        
                            <div class="col-md-3 col-xs-12 form-group" ng-class="{'has-error': form.dataprocessamento.$invalid && form.submitted}">
                                <label for="datavencimento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.GRID_DataVencimento' | translate}}" translate="47.GRID_DataVencimento">Data de Vencimento </label><br />
                                <span translate="47.LBL_De">De</span> 
                                <input type="text" id="Text7" name="datacertificado" ng-model="item.pesquisaLote.de_datavencimento" maxlength="10" class="form-control input-sm datepicker"
                                    sbx-datepicker data-date-format="dd/mm/yyyy" 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_De' | translate}}"/>
                            </div>
                            <div class="col-md-3 col-xs-12 form-group" ng-class="{'has-error': form.datavencimento.$invalid && form.submitted}" style="margin-top: 29px;">
                                <span translate="47.LBL_Ate">Até</span> 
                                <input type="text" name="datavencimento" id="Text8" ng-model="item.pesquisaLote.ate_datavencimento" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy"
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_Ate' | translate}}"/>
                            </div>
                        </div>

                        <div class="row">   
                            <div class="col-md-6 col-xs-12  form-group" ng-class="{'has-error': form.lote.$invalid && form.submitted}">
                                <label for="produto" translate="47.LBL_NumeroLote">Número do Lote </label><br />
                                <input type="text" id="lote" name="lote" ng-model="item.pesquisaLote.nr_lote" class="form-control input-sm" 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_NumeroLote' | translate}}"/>
                            </div>
                            <div class="col-md-6 col-xs-12  form-group" ng-class="{'has-error': form.lote.$invalid && form.submitted}">
                                <label>&nbsp;</label>
                                <br>
                                 <button name="find" class="btn btn-info btn-xs" type="button" ng-click="onFilter(item)">
                                <i class="icon-ok bigger-110"></i><span translate="17.BTN_Pesquisar">Pesquisar</span>
                            </button>
                            </div>
                        </div>                          
                        <br>
                        <div>
                            <div class="table-responsive" style="margin-bottom: 20px;">
                                <table ng-table="tableLotes" class="table table-bordered table-striped table-hover">
                                    <thead>
	                                    <tr>
		                                    <th ng-click="tableLotes.sorting('nrlote', tableLotes.isSortBy('nrlote', 'asc') ? 'desc' : 'asc')">
                                          <span translate="17.GRID_NRLote">Número Lote</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('nrlote', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('nrlote', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableLotes.sorting('dataprocessamento', tableLotes.isSortBy('dataprocessamento', 'asc') ? 'desc' : 'asc')">
                                          <span translate="47.LBL_DataProcessamento">Data Processamento</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('dataprocessamento', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('dataprocessamento', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableLotes.sorting('datavencimento', tableLotes.isSortBy('datavencimento', 'asc') ? 'desc' : 'asc')">
                                          <span translate="47.GRID_DataVencimento">Data Vencimento</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('datavencimento', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('datavencimento', 'desc')
                                          }"></i>
                                        </th>
                                       
		                                    <th><span translate="17.GRID_Selecionar">Selecionar</span></th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data">
                                            <td ng-style="{ 'width': '45%' }" sortable="'nrlote'">{{item1.nrlote}}</td>
                                            <td ng-style="{ 'width': '25%' }" sortable="'dataprocessamento'">{{item1.dataprocessamento}}</td>
                                            <td ng-style="{ 'width': '25%' }" sortable="'datavencimento'">{{item1.datavencimento}}</td>
                                            
                                            <td ng-style="{ 'width': '5%' }" class="action-buttons center">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedLote(item1)">
                                                    <i class="icon-ok bigger-110"></i><span translate="17.GRID_Selecionar">Selecionar</span>
                                                </button>
                                            </td>

                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="5" class="text-center" translate="17.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()"> <i class="icon-remove bigger-110"></i><span translate="17.BTN_Fechar">Fechar</span></button>
                </div>
            </div>
        </script>
    </div>
</div>

