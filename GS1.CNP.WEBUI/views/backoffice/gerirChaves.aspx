﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gerirChaves.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.gerirChaves" %>

<style>
    .btn-gray {
        color: Gray;
    }
</style>

<div ng-controller="GerirChavesCtrl" class="page-container" ng-init="searchMode = true;" style="height: 820px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-mobile" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" translate="13.LBL_GestaoChaves"></label>
        </div>
        <div class="pull-right action-buttons-header" style="margin-top: -32px; margin-right: 258px;">
            <ajuda codigo="34" tooltip-ajuda="'13.TOOLTIP_Ajuda'" margin-right="'10px'"></ajuda>
        </div>
        <div class="widget-toolbar my-tab" style="margin-top: -36px;">
            <ul id="tabs-chavesGeradas" class="nav nav-tabs">
                <li id="liTabChavesGeradas" class="active">
                    <a data-toogle="tab" href="#chavesGeradas" translate="13.ABA_ChavesGeradas">Chaves já Geradas</a>
                </li>
                <li id="liTabGerarChaves" ng-show="usuarioLogado.id_tipousuario == 1">
                    <a data-toogle="tab" href="#gerarChaves" ng-clock translate="13.ABA_GerarChaves">Gerar Chaves</a>
                </li>
            </ul>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="tab-content">
                        <div id="chavesGeradas" class="tab-pane active">
                            <div class="table-responsive">
                                <table ng-table="tableChavesGeradas" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableChavesGeradas.sorting('cpfcnpj', tableChavesGeradas.isSortBy('cpfcnpj', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_CPFCNPJ">CNPJ/CPF</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableChavesGeradas.isSortBy('cpfcnpj', 'asc'),
                                                'icon-sort-down': tableChavesGeradas.isSortBy('cpfcnpj', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableChavesGeradas.sorting('prefixos', tableChavesGeradas.isSortBy('prefixos', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_NumeroPrefixo">N° Prefixo</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableChavesGeradas.isSortBy('prefixos', 'asc'),
                                                'icon-sort-down': tableChavesGeradas.isSortBy('prefixos', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableChavesGeradas.sorting('datacriacao', tableChavesGeradas.isSortBy('datacriacao', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_DataCriacao">Data Criação</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableChavesGeradas.isSortBy('datacriacao', 'asc'),
                                                'icon-sort-down': tableChavesGeradas.isSortBy('datacriacao', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableChavesGeradas.sorting('dataexpiracao', tableChavesGeradas.isSortBy('dataexpiracao', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_DataVencimento">Data Vencimento</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableChavesGeradas.isSortBy('dataexpiracao', 'asc'),
                                                'icon-sort-down': tableChavesGeradas.isSortBy('dataexpiracao', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableChavesGeradas.sorting('chave', tableChavesGeradas.isSortBy('chave', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_Chave">Chave</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableChavesGeradas.isSortBy('chave', 'asc'),
                                                'icon-sort-down': tableChavesGeradas.isSortBy('chave', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableChavesGeradas.sorting('status', tableChavesGeradas.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_Status">Status</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableChavesGeradas.isSortBy('status', 'asc'),
                                                'icon-sort-down': tableChavesGeradas.isSortBy('status', 'desc')
                                              }"></i>
                                            </th>
                                            <th><span translate="13.GRID_Acoes">Ações</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data" scroll-to="">
                                            <td ng-style="{ 'width': '15%' }" sortable="'cpfcnpj'">{{item1.cpfcnpj}}</td>
                                            <td ng-style="{ 'width': '10%' }" sortable="'numeroprefixo'">{{item1.prefixos}}</td>
                                            <td ng-style="{ 'width': '15%' }" sortable="'datacriacao'">{{item1.datacriacao | date: 'dd/MM/yyyy'}}</td>
                                            <td ng-style="{ 'width': '15%' }" sortable="'dataexpiracao'">{{item1.dataexpiracao | date: 'dd/MM/yyyy'}}</td>
                                            <td ng-style="{ 'width': '25%' }" sortable="'chave'">{{item1.chave}}</td>
                                            <td ng-style="{ 'width': '10%' }" sortable="'status'">{{item1.status}}</td>
                                            <td ng-style="{ 'width': '10%' }" class="action-buttons center" data-title="'Ações'">
                                                <a href="" ng-click="enviarChave(item1)" title="{{ '13.TOOLTIP_Enviar' | translate }}" ng-show="exibirEnviar(item1)">
                                                    <i class="fa fa-send-o" ng-class="{'btn-gray': item1.dataexpiracao != null && item1.dataexpiracao != ''}"></i>
                                                </a>
                                                <a href="" ng-click="expirarChave(item1)" title="{{ '13.TOOLTIP_Expirar' | translate }}" ng-show="exibirExpirar(item1)">
                                                    <i class="fa fa-clock-o" ng-class="{'btn-gray': item1.dataexpiracao != null && item1.dataexpiracao != ''}"></i>
                                                </a>
                                                <a href="" ng-click="baixarCarta(item1)" title="{{ '13.TOOLTIP_Baixar' | translate }}" ng-show="exibirBaixar(item1)">
                                                    <i class="fa fa-download" ng-class="{'btn-gray': item1.dataexpiracao != null && item1.dataexpiracao != ''}"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="7" class="text-center" translate="13.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <div id="gerarChaves" class="tab-pane">
                            <div class="table-responsive">
                                <table ng-table="tableGerarChaves" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableGerarChaves.sorting('cpfcnpj', tableGerarChaves.isSortBy('cpfcnpj', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_CPFCNPJ">CNPJ/CPF</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGerarChaves.isSortBy('cpfcnpj', 'asc'),
                                                'icon-sort-down': tableGerarChaves.isSortBy('cpfcnpj', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableGerarChaves.sorting('razao', tableGerarChaves.isSortBy('razao', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_RazaoSocial">Razão Social</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGerarChaves.isSortBy('razao', 'asc'),
                                                'icon-sort-down': tableGerarChaves.isSortBy('razao', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableGerarChaves.sorting('numeroprefixo', tableGerarChaves.isSortBy('numeroprefixo', 'asc') ? 'desc' : 'asc')">
                                              <span translate="13.GRID_NumeroPrefixo">N° Prefixo</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGerarChaves.isSortBy('numeroprefixo', 'asc'),
                                                'icon-sort-down': tableGerarChaves.isSortBy('numeroprefixo', 'desc')
                                              }"></i>
                                            </th>
                                            <th><span translate="13.GRID_Acao">Ação</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data" scroll-to="">
                                            <td ng-style="{ 'width': '20%' }" data-title="'CNPJ/CPF'" sortable="'cpfcnpj'">{{item1.cpfcnpj}}</td>
                                            <td ng-style="{ 'width': '50%' }" data-title="'Razão Social'" sortable="'razao'">{{item1.razao}}</td>
                                            <td ng-style="{ 'width': '20%' }" data-title="'N° Prefixo'" sortable="'numeroprefixo'">{{item1.numeroprefixo}}</td>
                                            <td ng-style="{ 'width': '10%' }" class="action-buttons center" data-title="'Ação'">
                                                <a href="" ng-click="gerarChave(item1);" title="{{ '13.TOOLTIP_Gerar' | translate }}" ng-show="exibirGerar(item1)">
                                                    <i class="fa fa-cog"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="7" class="text-center" translate="13.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="13.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="13.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>
