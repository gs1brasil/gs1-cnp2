﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gestaoPerfis.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.gestaoPerfis" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />
<script src="../../scripts/lib/jstree.min.js" type="text/javascript"></script>

<div ng-controller="GestaoPerfilCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = 'agencia'; itemFormEspec = { statusregistro: 'novo' };" style="height: 820px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label>Gestão de Perfil e Permissões</label>
        </div>
        <div class="widget-box" style="margin-bottom: 0px">
            <div class="widget-header">
                <h4><span ng-show="!searchMode">Cadastro</span><span ng-show="searchMode">Pesquisa</span></h4>
                <div class="widget-toolbar" ng-if="searchMode">
                    <a href=""><i class="icon-eraser" title="Limpar" ng-click="onClean(item)"></i></a>
                    <a ng-show="searchMode" href="/views/backoffice/ajuda/gestãoPerfil.html" target="_blank"><i class="icon-question-sign" title="Limpar"></i></a>
                </div>
                <div class="widget-toolbar" ng-if="!searchMode">
                    <a ng-show="!searchMode" href="/views/backoffice/ajuda/gestãoPerfil.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
                <div class="widget-toolbar  my-tab">
                    <ul id="tabs-definicoes" class="nav nav-tabs">
                        <li id="liTabDefinicoes" class="active">
                            <a data-toogle="tab" href="#definicoes">Perfil</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="definicoes" class="tab-pane active">
                            <div style="overflow-x: hidden; overflow-y: auto; clear: both; max-height: 555px;">
                                <div class="row" ng-show="!searchMode">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        (*) - Campos obrigatórios
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="nome">Nome Perfil <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="nome" name="nome" ng-model="item.nome" required="required" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="descricao">Descrição <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="descricao" name="descricao" ng-model="item.descricao" required="required" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="tipoUsuario">Tipo de Usuário <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" ng-model="item.tipoUsuario" ng-options="tipoUsuario.codigo as tipoUsuario.nome for tipoUsuario in tiposUsuario" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="status">Status <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" name="status" ng-model="item.status" ng-options="status.status as status.status for status in statusPerfis" />
                                            <%--<option ng-repeat="status in statusPerfis" value="{{status}}" ng-selected="{{item.status === status}}">{{status}}</option>--%>                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="">Permissões <span ng-show='!searchMode'>(*)</span></label>
                                        <ul class="list-group" ng-init="collapseChildren = false" style="margin-bottom: 15px; margin-left: 0px;">
                                            <li class="list-group-item" ng-repeat="item1 in arvore" style="margin-bottom: 15px;">
                                                <i class="icon-caret-down pull-right" ng-show="item1.children != ''"></i>
                                                <input type="checkbox" class="ace" name="form-field-checkbox" ng-checked="collapseChildren" ng-click="collapseChildren = !collapseChildren">
                                                <span class="lbl">&nbsp;{{ item1.descricao }}</span>
                                                <ul style="list-style: none;" ng-show="collapseChildren">
                                                    <li ng-repeat="children in item1.children">
                                                        <input type="checkbox" class="ace" ng-checked="collapseChildren">
                                                        <span class="lbl">&nbsp;{{ children.descricao }}</span>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions center" ng-show="editMode">
                                <button class="btn btn-info btn-sm" scroll-to="" ng-click="">
                                    <i class="icon-save bigger-110"></i>Salvar
                                </button>
                                <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                    <i class="icon-remove bigger-110"></i>Cancelar
                                </button>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label>Gestão de Perfil e Permissões</label>
        </div>
        <div class="widget-box">
            <div class="widget-header">
                <h4>Perfis</h4>
                <div class="widget-toolbar">
                    <a href=""><i class="icon-plus" title="Novo registro" ng-click="onFormMode()"></i>
                    </a><a href=""><i class="icon-search" title="Pesquisar" ng-click="onSearchMode(item)"></i></a>
                    <a href="/views/backoffice/ajuda/produtoListagem.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
            </div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 708px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tablePerfil" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <tbody>
                                <tr ng-repeat="item1 in $data | filter:itemProduto" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '20%' }" data-title="'Nome'" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '20%' }" data-title="'Tipo de Usuário'" sortable="'tipoUsuario'">{{item1.tipoUsuario}}</td>
                                    <td ng-style="{ 'width': '50%' }" data-title="'Descrição'" sortable="'descricao'">{{item1.descricao}}</td>
                                    <td ng-style="{ 'width': '5%' }" data-title="'Quantidade'" sortable="'quantidade'">{{item1.quantidade}}</td>
                                    <td ng-style="{ 'width': '5%' }" class="action-buttons center">
                                        <a class="red" href="" ng-click="onDelete($index); $event.stopPropagation();"><i class="icon-trash bigger-130"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




