﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="idiomas.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.idiomas" %>


<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />


<div ng-controller="IdiomaCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false;" style="height:900px;">
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo"></label>
        </div>
        <div class="widget-toolbar my-tab" style="margin-top: -36px;" ng-show="!initMode">
            <ul id="tabs-idioma" class="nav nav-tabs">
                <li id="liTabTelas" class="active"><a data-toogle="tab" href="#telas" translate="49.ABA_Telas">Telas</a></li>
                <li id="liTabMenu" ng-show="!isPesquisa"><a data-toogle="tab" href="#menu" translate="49.ABA_Menu">Menu</a></li>
            </ul>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="tab-content" style="min-height:400px;" >
                        <div id="telas" class="tab-pane active">
                            <div ng-if="!initMode && !isPesquisa">
                                <div class="row">
                                    <div class="form-group col md-12 text-center">
                                        <h3 class="modal-title">{{itemForm.nome}} ({{itemForm.descricao}}) </h3>    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="codigomodulo" translate="49.LBL_StatusIdioma">Status do Idioma</label>
                                        <select class="form-control input-sm" id="Select2" name="status" ng-model="itemInsert.status" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.LBL_StatusIdioma' | translate}}">
                                            <option value="1" ng-selected="itemInsert.status == 1">Ativo</option>
                                            <option value="0" ng-selected="itemInsert.status == 0">Inativo</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="codigomodulo" translate="49.LBL_Modulo">Módulo</label>
                                        <select class="form-control input-sm fieldEnable" id="codigomodulo" name="codigomodulo" ng-change="carregaFormularios(itemForm.codigomodulo)" ng-model="itemForm.codigomodulo" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.LBL_Modulo' | translate}}">
							                <option ng-repeat="modulo in modulos" value="{{modulo.codigo}}" ng-selected="{{modulo.codigo == itemForm.codigomodulo}}">{{modulo.nome}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="codigoformulario" translate="49.LBL_Formulario">Formulário</label>
                                        <select class="form-control input-sm" id="codigoformulario" name="codigoformulario" ng-model="itemForm.codigoformulario" ng-change="carregaCampos()" 
                                            ng-disabled="itemForm.codigomodulo == undefined || itemForm.codigomodulo == ''" ng-class="{'fieldDisabled': itemForm.codigomodulo == undefined || itemForm.codigomodulo == ''}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.LBL_Formulario' | translate}}">
							                <option ng-repeat="formulario in formularios" value="{{formulario.codigo}}" ng-selected="{{formulario.codigo == itemForm.codigoformulario}}">{{formulario.nome}}</option>
                                        </select>    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" style="font-size: 90%;" translate="49.LBL_CliqueRegistroEditar">
                                        Clique em um registro para editar
                                    </div>
                                </div>
                                <div class="table-responsive" style="height: 400px; overflow-y: auto;">
                                    <table ng-table="tableCampos" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th ng-click="tableCampos.sorting('nome', tableCampos.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                                    <span translate="49.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                                    <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('nome', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('nome', 'desc')
                                                    }"></i>
                                                </th>
                                                <th ng-click="tableCampos.sorting('nometraducao', tableCampos.isSortBy('nometraducao', 'asc') ? 'desc' : 'asc')">
                                                    <span translate="49.GRID_Traducao">Tradução</span>&nbsp;&nbsp;
                                                    <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('nometraducao', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('nometraducao', 'desc')
                                                    }"></i>
                                                </th>
                                                <th ng-click="tableCampos.sorting('comentario', tableCampos.isSortBy('comentario', 'asc') ? 'desc' : 'asc')">
                                                    <span translate="49.GRID_ComentarioTooltip">Comentário/Tooltip</span>&nbsp;&nbsp;
                                                    <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('comentario', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('comentario', 'desc')
                                                    }"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="campo in $data" ng-click="onEditCampo(campo);" style="cursor:pointer;">
                                                <td ng-style="{ 'width': '30%' }" sortable="'nomecampo'">{{campo.nomecampo}}</td>
                                                <td ng-style="{ 'width': '30%' }" sortable="'nometraducao'">{{campo.nometraducao}}</td>
                                                <td ng-style="{ 'width': '40%' }" sortable="'comentario'">{{campo.comentario}}</td>
                                            </tr>
                                            <tr ng-show="$data.length == 0">
                                                <td colspan="8" class="text-center" translate="49.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <form name="form" novalidate class="form" role="form" >
                                <div ng-if="initMode && !isPesquisa">
                                    <div class="row">
                                        <div class="col-md-offset-3"></div>
                                        <div class="form-group col-md-offset-3 col-md-6 col-xs-12" ng-class="{'has-error': form.idiomabase.$invalid && form.submitted}">
                                            <label for="idiomabase" translate="49.LBL_IdiomaBase">Idioma Base </label>
                                            <input type="text" class="form-control input-sm" maxlength="255" id="idiomabase" name="idiomabase" ng-model="itemInsert.idiomabase" required="required" ng-readonly="true" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.LBL_IdiomaBase' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-offset-3 col-md-6 col-xs-12" ng-class="{'has-error': form.idiomatraducao.$invalid && form.submitted}">
                                            <label for="idiomatraducao" translate="49.LBL_IdiomaNovo">Idioma Novo </label>
                                            <select class="form-control input-sm fieldEnable" id="idiomatraducao" name="idiomatraducao" ng-model="itemInsert.idiomatraducao" required 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.LBL_IdiomaNovo' | translate}}">
                                                <option ng-repeat="idioma in idiomasTraducao" value="{{idioma.codigo}}">{{idioma.nome}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 100px;">
                                        <div class="form-group col-md-offset-3 col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                            <label for="status" translate="49.LBL_Status">Status </label>
                                            <select class="form-control input-sm" id="status" name="status" ng-model="itemInsert.status" required disabled 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.LBL_Status' | translate}}">
                                                <option value="1" ng-select>Ativo</option>
                                                <option value="0">Inativo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div ng-if="isPesquisa && !initMode">
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                        <label for="nome" translate="49.LBL_Idioma">Idioma <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="255" id="Text1" name="nome" ng-model="itemSearch.nome" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.LBL_Idioma' | translate}}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                        <label for="descricao">Descrição </label>
                                        <textarea class="form-control input-sm" id="descricao" sbx-maxlength="300" name="descricao" rows="3" ng-model="itemSearch.descricao" style="resize: vertical;" required
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.lbl_Descricao' | translate}}"></textarea>
                                        <span style="font-size: 80%;">(<span translate="49.LBL_TamanhoMaximo">Tamanho máximo</span>: 300)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span translate="49.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.descricao.$viewValue.length || 0 }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu" class="tab-pane">
                            <div class="row">
                                <div class="form-group col md-12 text-center">
                                    <h3 class="modal-title">{{itemForm.nome}} ({{itemForm.descricao}}) </h3>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="codigomodulo" translate="49.LBL_Modulo">Módulo</label>
                                    <select class="form-control input-sm fieldEnable" id="Select1" name="codigomodulo" ng-change="recarregarMenus(itemForm.codigomodulomenu)" ng-model="itemForm.codigomodulomenu" required 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.49.LBL_Modulo' | translate}}">
							            <option ng-repeat="modulo in modulos" value="{{modulo.codigo}}" ng-selected="{{modulo.codigo == itemForm.codigomodulomenu}}">{{modulo.nome}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12" style="font-size: 90%;" translate="49.LBL_CliqueRegistroEditar">
                                    Clique em um registro para editar
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table ng-table="tableMenus" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableMenus.sorting('nome', tableMenus.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                                <span translate="10.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                                <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableMenus.isSortBy('nome', 'asc'),
                                                    'icon-sort-down': tableMenus.isSortBy('nome', 'desc')
                                                }"></i>
                                            </th>
                                            <th ng-click="tableMenus.sorting('nometraducao', tableMenus.isSortBy('nometraducao', 'asc') ? 'desc' : 'asc')">
                                                <span translate="10.GRID_Traducao">Tradução</span>&nbsp;&nbsp;
                                                <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableMenus.isSortBy('nometraducao', 'asc'),
                                                    'icon-sort-down': tableMenus.isSortBy('nometraducao', 'desc')
                                                }"></i>
                                            </th>
                                            <th ng-click="tableMenus.sorting('comentario', tableMenus.isSortBy('comentario', 'asc') ? 'desc' : 'asc')">
                                                <span translate="10.GRID_ComentarioTooltip">Comentário/Tooltip</span>&nbsp;&nbsp;
                                                <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableMenus.isSortBy('comentario', 'asc'),
                                                    'icon-sort-down': tableMenus.isSortBy('comentario', 'desc')
                                                }"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="menu in $data" ng-click="onEditMenu(menu)" style="cursor:pointer;">
                                            <td ng-style="{ 'width': '30%' }" sortable="'nome'">{{menu.nome}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'nometraducao'">{{menu.nometraducao}}</td>
                                            <td ng-style="{ 'width': '40%' }" sortable="'comentario'">{{menu.comentario}}</td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="8" class="text-center" translate="49.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <button name="salvar" class="btn btn-info btn-sm" type="button" ng-click="onSave(itemInsert)" ng-if="!isPesquisa">
                                <i class="icon-save bigger-110"></i><span translate="49.BTN_Salvar">Salvar</span>
                            </button>
                            <button name="voltar" class="btn btn-danger btn-sm" type="button" ng-click="voltar()" ng-if="!initMode && !isPesquisa">
                                <i class="icon-remove bigger-110"></i><span translate="49.BTN_Voltar">Voltar</span>
                            </button>
                            <button name="find" class="btn btn-info btn-sm" type="button" ng-click="onSearch(itemSearch)" ng-if="!initMode && isPesquisa">
                                <i class="icon-search bigger-110"></i><span translate="49.BTN_Pesquisar">Pesquisar</span>
                            </button>
                            <button name="cancel" class="btn btn-danger btn-sm" type="button" ng-click="cancelar()" ng-if="initMode || isPesquisa">
                                <i class="icon-remove bigger-110"></i><span translate="49.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label translate="49.LBL_TitleGestaoIdiomas">Gestão de Idiomas</label>
        </div>
        <div class="widget-box">
            <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '49.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
                <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '49.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode()"><i class="fa fa-search"></i></a>
                <%--<ajuda codigo="25" tooltip-ajuda="'49.TOOLTIP_Ajuda'"></ajuda>--%>
            </div>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 790px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="49.LBL_CliqueRegistroEditar">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                            <table ng-table="tableIdiomas" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <thead>
                                    <tr>
                                        <th ng-click="tableIdiomas.sorting('codigo', tableIdiomas.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                            <span>#</span>&nbsp;&nbsp;
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableIdiomas.isSortBy('codigo', 'asc'),
                                                'icon-sort-down': tableIdiomas.isSortBy('codigo', 'desc')
                                            }"></i>
                                        </th>
                                        <th ng-click="tableIdiomas.sorting('nome', tableIdiomas.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                            <span translate="10.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableIdiomas.isSortBy('nome', 'asc'),
                                                'icon-sort-down': tableIdiomas.isSortBy('nome', 'desc')
                                            }"></i>
                                        </th>
                                        <th ng-click="tableIdiomas.sorting('nomeusuario', tableIdiomas.isSortBy('nomeusuario', 'asc') ? 'desc' : 'asc')">
                                            <span translate="10.GRID_CriadoAlteradoPor">Criado/Alterado Por</span>&nbsp;&nbsp;
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableIdiomas.isSortBy('nomeusuario', 'asc'),
                                                'icon-sort-down': tableIdiomas.isSortBy('nomeusuario', 'desc')
                                            }"></i>
                                        </th>
                                        <th ng-click="tableIdiomas.sorting('dataalteracao', tableIdiomas.isSortBy('dataalteracao', 'asc') ? 'desc' : 'asc')">
                                            <span translate="10.GRID_DataHoraAlteracao">Data/Hora de Alteração</span>&nbsp;&nbsp;
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableIdiomas.isSortBy('dataalteracao', 'asc'),
                                                'icon-sort-down': tableIdiomas.isSortBy('dataalteracao', 'desc')
                                            }"></i>
                                        </th>
                                        <th ng-click="tableIdiomas.sorting('status', tableIdiomas.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                            <span translate="10.GRID_Status">Status</span>&nbsp;&nbsp;
                                            <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableIdiomas.isSortBy('status', 'asc'),
                                                'icon-sort-down': tableIdiomas.isSortBy('status', 'desc')
                                            }"></i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in $data | filter:item" ng-click="onEdit(item1)">
                                        <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                        <td ng-style="{ 'width': '40%' }" sortable="'nome'">{{item1.nome}} ({{item1.descricao}})</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'nomeusuario'">{{item1.nomeusuario}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'dataalteracao'">{{item1.dataalteracao | date:'dd/MM/yyyy hh:mm'}}</td>
                                        <td ng-style="{ 'width': '15%' }" sortable="'status'">{{item1.status | status}}</td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="8" class="text-center" translate="49.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="modal">
        <script type="text/ng-template" id="ModalEditaCampos">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="bootbox-close-button close" ng-click="cancel()">x</button>
                    <h4 class="modal-title"><span translate="49.LBL_EditandoCampo">Editando o campo</span> <b>{{campoForm.nome}}</b></h4>
                </div>
                <div class="modal-body">
                    <form name="form.campo" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12" ng-class="{'has-error': form.campo.traducao.$invalid && form.campo.submitted}">
                                <label for="traducao" translate="49.LBL_Traducao">Tradução</label>
                                <!--<input type="text" class="form-control input-sm" name="traducao" ng-model="campoForm.nometraducao" required 
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.49.LBL_Traducao' | translate}}"
                                ng-disabled="desabilitarCamposModal"/>-->

                                <textarea class="form-control input-sm" id="traducao" sbx-maxlength="{{campoMenu ? 50 : 1000}}" name="traducao" rows="3" ng-model="campoForm.nometraducao" style="resize: vertical;"
                                    data-toggle="popover" data-trigger="hover" data-content="{{'t.49.LBL_Traducao' | translate}}" ng-disabled="desabilitarCamposModal" required>
                                </textarea>
                                <span style="font-size: 80%;">(<span translate="49.LBL_TamanhoMaximo">Tamanho máximo</span>: {{campoMenu ? 50 : 1000}})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span translate="15.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.campo.traducao.$viewValue.length || 0 }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" ng-class="{'has-error': form.campo.comentario.$invalid && form.campo.submitted}">
                                <label for="comentario" translate="49.LBL_ComentarioTooltip">Comentário/Tooltip</label>
                                <!--<input type="text" class="form-control input-sm" id="comentario" name="comentario" ng-model="campoForm.comentario" required
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.49.LBL_Comentario' | translate}}"
                                ng-disabled="desabilitarCamposModal"/>-->

                                <textarea class="form-control input-sm" id="comentario" sbx-maxlength="{{campoMenu ? 255 : 4000}}" name="comentario" rows="3" ng-model="campoForm.comentario" style="resize: vertical;"
                                    data-toggle="popover" data-trigger="hover" data-content="{{'t.49.LBL_Comentario' | translate}}" ng-disabled="desabilitarCamposModal" required>
                                </textarea>
                                <span style="font-size: 80%;">(<span translate="49.LBL_TamanhoMaximo">Tamanho máximo</span>: {{campoMenu ? 255 : 4000}})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span translate="15.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.campo.comentario.$viewValue.length || 0 }}</span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer center">
                    <button class="btn btn-info" ng-click="ok()" ng-disabled="desabilitarCamposModal"><span translate="49.BTN_Salvar">Salvar</span></button>
                    <button class="btn btn-danger" ng-click="cancel()" name="cancel"><span translate="49.BTN_Cancelar">Cancelar</span></button>
                </div>
            </div>
        </script>
    </div>

</div>