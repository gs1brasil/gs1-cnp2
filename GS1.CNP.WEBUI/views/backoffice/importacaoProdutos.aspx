﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="importacaoProdutos.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.importacaoProdutos" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/importacaoProdutos";
    }

</script>

<style type="text/css">     
    .input-group-btn>button{
        padding: 2px;    
    }
    
    .control-label{
        font-size: 13px; 
    }
    
</style>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="ImportacaoProdutosCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = ''; importacaoManual = false;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label ng-bind="titulo"></label>
            </div>
            <div class="widget-toolbar my-tab" style="margin-top: -36px;">
                <ul id="tabs-importacao" class="nav nav-tabs">
                    <li id="liTabAssistente" class="active">
                        <a data-toogle="tab" href="#assistente">Assistente</a>
                    </li>
                    <li id="liTabResultado">
                        <a data-toogle="tab" href="#resultado">Resultado</a>
                    </li>
                </ul>
            </div>
            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form name="form" id="form" novalidate class="form" role="form">
                            <div class="tab-content">
                                <div id="assistente" class="tab-pane active">
                                    <div ng-show="!importacaoManual">
                                        <div class="row text-center">
                                            <div class="form-group col-md-12">
                                                <h3>Verificação dos Itens</h3>
                                                <div class="pull-right" style="margin-top: -40px;">
                                                    <ajuda codigo="70"></ajuda>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label><strong>Tipo de item:</strong> GTIN-13 (Itens Comerciais)</label>    
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label><strong>Quantidade:</strong> 6 itens</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label><strong>Erros encontrados:</strong> 2</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div>
                                                            <h4>Status importação</h4>
                                                        </div>
                                                        <div ng-show="errosImportacao > 0">
                                                            <span class="red">{{ errosImportacao }} erro(s) encontrado(s) em seu arquivo! Por favor corrija o(s) item(ns) em vermelho ou retire-o(s) desta importação</span>
                                                            <ul ng-repeat="linha in importacoesArquivo">
                                                                <li class="red" ng-repeat="erro in linha.erros" style="list-style-type: asterisks;">{{ erro.descricao }}</li>
                                                            </ul>
                                                        </div>
                                                        <div ng-show="errosImportacao <= 0">
                                                            <span class="green">Itens validados prontos para importação</span>    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table ng-table="tableImportacoesArquivo" class="table table-bordered table-striped table-hover">
                                                <tbody>
                                                    <tr ng-repeat="item1 in $data" scroll-to="">
                                                        <td ng-style="{ 'width': '5%' }" data-title="'Status (*)'" sortable="'status'" ng-class="{'red': item1.erros[0].campo == 'status'}">{{item1.status}}</td>
                                                        <td ng-style="{ 'width': '10%' }" data-title="'Data de Criação (*)'" sortable="'data'" ng-class="{'red': item1.erros[0].campo == 'data'}">{{item1.data}}</td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'GTIN (*)'" sortable="'gtin'" ng-class="{'red': item1.erros[0].campo == 'gtin'}">{{item1.gtin}}</td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'Código Interno (*)'" sortable="'codigoInterno'" ng-class="{'red': item1.erros[0].campo == 'codigoInterno'}">{{item1.codigoInterno}}</td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'Descrição Produto (*)'" sortable="'descricaoProduto'" ng-class="{'red': item1.erros[0].campo == 'descricaoProduto'}">{{item1.descricaoProduto}}</td>
                                                        <td ng-style="{ 'width': '20%' }" data-title="'GTIN Nível Inferior'" sortable="'gtinInferior'" ng-class="{'red': item1.erros[0].campo == 'gtinInferior'}">{{item1.gtinInferior}}</td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'Quantidade de Itens'" sortable="'quantidadeItens'" ng-class="{'red': item1.erros[0].campo == 'quantidadeItens'}">{{item1.quantidadeItens}}</td>
                                                        <td ng-style="{ 'width': '5%' }" class="action-buttons center">
                                                            <a class="red" href=""><i class="icon-remove bigger-130"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr ng-show="$data.length == 0">
                                                        <td colspan="8" class="text-center">Nenhum registro encontrado</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="form-actions center">
                                            <button id="Button1" name="cancel" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                                <i class="icon-remove bigger-110"></i>Cancelar
                                            </button>
                                            <button class="btn btn-info btn-sm" ng-show="errosImportacao > 0" ng-click="onValidate()">
                                                <i class="icon-ok bigger-110"></i>Validar
                                            </button>
                                            <button class="btn btn-info btn-sm" ng-show="errosImportacao <= 0">
                                                <i class="icon-upload-alt bigger-110"></i>Importar
                                            </button>
                                        </div>
                                    </div>
                                    <div ng-show="importacaoManual">
                                        <div class="row text-center">
                                            <div class="form-group col-md-12">
                                                <h3>Importação Manual</h3>
                                                <div class="pull-right" style="margin-top: -40px;">
                                                    <ajuda codigo="57"></ajuda>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="row text-center">
                                            <form class="form-horizontal" >
                                                <div class="form-group">
                                                    <label for="tipoItem" class="col-md-1 control-label">Tipo de Item:</label>
                                                    <div class="col-md-8" style="padding-left: 0px;">
                                                        <select class="form-control input-sm" id="Select1" name="itemGtinTemplate" ng-model="itemGtinTemplate">
                                                            <option value="GTIN-13 - Itens Comerciais" ng-selected="true">GTIN-13 - Itens Comerciais</option>
                                                            <option value="GTIN-14 - Itens Logísticos">GTIN-14 - Itens Logísticos</option>
                                                            <option value="3">GTIN-12 - Itens Comerciais (USA/Canada)</option>
                                                            <option value="GTIN-12 - Itens Comerciais (USA/Canada)">GTIN-8 - Itens Comerciais</option>
                                                            <option value="GLN - Localizações Físicas">GLN - Localizações Físicas</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button class="btn btn-info btn-xs" type="button">
                                                            <i class="icon-globe bigger-110"></i>Carregar Template Online
                                                        </button>
                                                    </div>
                                                </div>    
                                            </form>        
                                        </div>
                                        <div class="row text-center" style="margin-top: 40px;">
                                            <h5>Novos - GTIN-13 - Itens Comerciais</h5>
                                        </div>
                                        <div class="table-responsive">
                                            <table ng-table="tableImportacoesManual" class="table table-bordered table-striped table-hover">
                                                <tbody>
                                                    <tr ng-repeat="item1 in $data" scroll-to="">
                                                        <td ng-style="{ 'width': '10%' }" data-title="'Status (*)'" sortable="'status'">
                                                            <select class="form-control input-sm" id="Select1" name="tipoGtin">
                                                                <option value="{{item1.status}}" ng-selected="{{item1.status}}">{{item1.status}}</option>
                                                            </select>
                                                        </td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'Data de Criação (*)'" sortable="'data'">{{item1.data}}</td>
                                                        <td ng-style="{ 'width': '10%' }" data-title="'GTIN (*)'" sortable="'gtin'">{{item1.gtin}}</td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'Código Interno (*)'" sortable="'codigoInterno'">{{item1.codigoInterno}}</td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'Descrição Produto (*)'" sortable="'descricaoProduto'">{{item1.descricaoProduto}}</td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'GTIN Nível Inferior'" sortable="'gtinInferior'">{{item1.gtinInferior}}</td>
                                                        <td ng-style="{ 'width': '15%' }" data-title="'Quantidade de Itens'" sortable="'quantidadeItens'">{{item1.quantidadeItens}}</td>
                                                        <td ng-style="{ 'width': '5%' }" class="action-buttons center">
                                                            <a class="red" href=""><i class="icon-remove bigger-130"></i></a>
                                                        </td>
                                                    </tr>
                                                    <tr ng-show="$data.length == 0">
                                                        <td colspan="8" class="text-center">Nenhum registro encontrado</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                        <div class="row text-right" style="margin-top: 5px;">
                                            <div class="col-md-12">
                                                <button class="btn btn-info btn-xs" type="button">
                                                    <i class="fa fa-plus bigger-110"></i>Novo Registro
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-actions center">
                                            <button id="Button1" name="cancel" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                                <i class="icon-remove bigger-110"></i>Cancelar
                                            </button>
                                            <button class="btn btn-info btn-sm" ng-click="onValidate()">
                                                <i class="icon-ok bigger-110"></i>Validar
                                            </button>
                                        </div>        
                                    </div>
                                </div>
                            
                                <div id="resultado" class="tab-pane">
                                    <div class="row text-center">
                                        <div class="form-group col-md-12" style="font-size: 90%;">
                                            <h3>Resultado da Importação</h3>
                                        </div> 
                                    </div>
                                    <div class="row text-center">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="panel panel-default">
                                                <div class="panel-body text-left">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <strong>Tipo de Item: </strong>
                                                            <span>GTIN-13 (Itens Comerciais)</span>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <strong>Tipo de Importação: </strong>
                                                            <span>Serviço</span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <strong>Quantidade: </strong>
                                                            <span>1 item</span>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <strong>Resultado: </strong>
                                                            <span class="red">Não Importado</span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <strong>Data/Hora: </strong>
                                                            <span>27/07/2015 - 15:52</span>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <strong>Usuário: </strong>
                                                            <span>wladimir@softbox.com.br</span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <strong>Tempo de Importação: </strong>
                                                            <span>0:00:02</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>
                                    <div class="form-actions center">
                                        <button class="btn btn-info btn-sm">
                                            <i class="icon-ok bigger-110"></i>Detalhes
                                        </button>
                                        <button id="Button1" name="cancel" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                            <i class="icon-remove bigger-110"></i>Fechar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            
            </div>
        </div>
        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label ng-bind="titulo"></label>
                <div ng-show="!editMode">
                    <div class="panel panel-default" style="margin-top: 20px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <span>Nova Importação</span>
                                    <div class="pull-right" style="margin-top: -40px;">
                                        <ajuda codigo="56"></ajuda>
                                    </div>
                                </div>
                            </div>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <span for="arquivo" class="col-md-2 control-label">Selecione o Arquivo:</span>
                                    <div class="col-md-6 no-padding-left">
                                        <input type="text" name="arquivo" class="form-control" placeholder="Arquivo Excel ou .txt (no formato do template)" />    
                                    </div>
                                    <div class="col-md-4">
                                        <button id="Button1" name="iniciarAssistente" class="btn btn-info btn-xs" type="button" ng-click="editMode = !editMode; importacaoManual = false;">
                                            <i class="bigger-110"></i>Iniciar Assistente
                                        </button>
                                        <button id="Button2" name="importacaoManual" class="btn btn-info btn-xs" type="button" ng-click="editMode = !editMode; importacaoManual = true;">
                                            <i class="bigger-110"></i>Importação Manual
                                        </button>
                                    </div>
                                   
                                </div>
                            </form> 
                        </div>
                    </div>

                    <div class="panel panel-default" style="margin-bottom: 50px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <span>Modelos para Importação</span>
                                </div>
                            </div>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <span for="template" class="col-md-2 control-label">Selecione Template:</span>
                                    <div class="col-md-6 no-padding-left">
                                        <select class="form-control input-sm" id="Select1" name="tipoGtin">
                                            <option value="1">GTIN-13 - Itens Comerciais</option>
                                            <option value="2">GTIN-14 - Itens Logísticos</option>
                                            <option value="3">GTIN-12 - Itens Comerciais (USA/Canada)</option>
                                            <option value="4">GTIN-8 - Itens Comerciais</option>
                                            <option value="5">GLN - Localizações Físicas</option>
                                        </select>    
                                    </div>
                                    <div class="col-md-4">
                                        <input type="radio" class="ace" name="form-field-radio"><span class="lbl">&nbsp;Excel&nbsp; </span>
                                        <input type="radio" class="ace" name="form-field-radio"><span class="lbl">&nbsp;Txt </span>
                                        <button id="Button1" name="cancel" class="btn btn-info btn-xs" type="button" style="padding-left: 38px; padding-right: 38px; margin-left: 30px;">
                                            <i class="bigger-110"></i>Download
                                        </button>
                                    </div>    
                                </div>
                            </form>    
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box">
                <div class="pull-right action-buttons-header">
                    <a class="btn btn-success btn-xs" title="Pesquisar" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
                    <ajuda codigo=""></ajuda>
                </div>
            
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <div class="widget-main" style="min-height: 960px;">
                        <div class="table-responsive">
                            <table ng-table="tableImportacoes" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <tbody>
                                    <tr ng-repeat="item1 in $data" scroll-to="">
                                        <td ng-style="{ 'width': '10%' }" data-title="'Data'" sortable="'data'">{{item1.data}}</td>
                                        <td ng-style="{ 'width': '20%' }" data-title="'Arquivo'" sortable="'arquivo'">{{item1.arquivo}}</td>
                                        <td ng-style="{ 'width': '10%' }" data-title="'Tipo Importação'" sortable="'tipoImportacao'">{{item1.tipoImportacao}}</td>
                                        <td ng-style="{ 'width': '10%' }" data-title="'Itens'" sortable="'itens'">{{item1.itens}}</td>
                                        <td ng-style="{ 'width': '10%' }" data-title="'Erros'" sortable="'erros'">{{item1.erros}}</td>
                                        <td ng-style="{ 'width': '20%' }" data-title="'Resultado'" sortable="'resultado'">{{item1.resultado}}</td>
                                        <td ng-style="{ 'width': '20%' }" data-title="'Usuário'" sortable="'usuario'">{{item1.usuario}}</td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="8" class="text-center">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1>Empresa Associada não foi selecionada</h1>
        <h4>Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>