﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="importarGPC.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.importarGPC" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#views/backoffice/importargpc";
    }

    $(".widget-header a").attr("tabindex", "-1");
</script>
<%--<link href="/styles/style.min.css" rel="stylesheet" type="text/css" />--%>
<script src="/Scripts/lib/jstree.min.js" type="text/javascript"></script>
<style type="text/css">
.widget-toolbar
{
    float: left;
}
.tab-content
{
    padding: 0px;
}
.inputArquivo
{
    width: 400px;
    display: inline-block;
    height: 19px;
}
#lista
{
    overflow-y: auto;
    min-height: 100px;
    max-height: 490px;
    width: 100%;
    position: absolute;
}

.strong
{
    font-weight: bold;
}
.grid-container
{
    background-color:#fff;
}
</style>
<div ng-controller="ImportarGPCCtrl" class="page-container" ng-init="resultMode = false;" style="height: 725px;">
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label> {{ titulo }} </label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span translate="8.LBL_ResultadoImportacao">Resultado da Importação</span></h4>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <form name="form" id="form" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-6" style="margin-bottom: 0;">
                                <label class="strong" translate="8.LBL_NomeArquivo">Nome do arquivo</label>: {{item.arquivo}}
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom: 0;">
                                <label class="strong" translate="8.LBL_Resultado">Resultado</label>: {{item.resultado}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" style="margin-bottom: 0;">
                                <label class="strong" translate="8.LBL_Itens">Itens</label>: {{item.itens}}
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom: 0;">
                                <label class="strong" translate="8.LBL_Usuario">Usuário</label>: {{item.usuario}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" style="margin-bottom: 0;">
                                <label class="strong" translate="8.LBL_Importados">Importados</label>: {{item.itensimportados}}
                            </div>
                            <div class="form-group col-md-6" style="margin-bottom: 0;">
                                <label class="strong" translate="8.LBL_TempoImportacao">Tempo de Importação</label>: {{item.tempoimportacao | date:'HH:mm:ss'}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" style="margin-bottom: 0;">
                                <label class="strong" translate="8.LBL_DataHora">Data/Hora</label>: {{item.data  | date : 'dd/MM/yyyy HH:mm'}}
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form-actions center">
                    <%--<button class="btn btn-info btn-sm" scroll-to="" ng-click="irParaAba('#liTabPesosMedidas a')">
                        <i class="fa fa-file-text-o"></i> Detalhes
                    </button>--%>
                    <button id="cancel" name="cancel" ng-click="onCancel()" scroll-to="" class="btn btn-danger btn-sm" type="button">
                        <i class="icon-remove bigger-110"></i><span translate="8.BTN_Cancelar">Cancelar</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": resultMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='resultMode = !resultMode'></i>
            <label ng-bind="titulo" translate="8.LBL_ImportacaoGPC"></label>
        </div>
        <div class="alert alert-warning" ng-show="existeimportacaopendente > 0">
			<button type="button" class="close" data-dismiss="alert">
				<i class="icon-remove"></i>
			</button>
			<strong translate="8.LBL_Pendente">Pendente!</strong>
            <span translate="8.LBL_ArquivoProcessando">Arquivo em processamento. Favor aguardar até que este seja finalizado.</span>
			<br>
		</div>
        <div class="widget-box">
            <div class="widget-header">
                <h4 translate="8.LBL_NovaImportacao">Nova Importação</h4>
                <div class="pull-right" style="margin-top: -40px;">
                    <ajuda codigo="20" tooltip-ajuda="'8.TOOLTIP_Ajuda'"></ajuda>
                </div>
            </div>

            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <div class="widget-main">
                	<form class="form-inline">
			            <label translate="8.LBL_SelecioneArquivo">Selecione o arquivo</label>:&nbsp;
			            <span class="inputArquivo">
				            <input type="file" id="arquivo" name="arquivo" 
                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.8.LBL_SelecioneArquivo' | translate}}"/><%-- ng-model="arquivo"--%>
			            </span>
			            &nbsp;
			            <button type="button" class="btn btn-info btn-sm" ng-click="onImport()" ng-disabled="existeimportacaopendente > 0" ng-class="{'fieldDisabled': existeimportacaopendente > 0}" style="padding: 0px 5px;"><i class="icon-upload bigger-110"></i>
                            <span class="hidden-xs" translate="8.BTN_Importar">Importar</span>
                        </button>
		            </form>
                </div>
            </div>
        </div>

        <div class="widget-box">
            <div class="widget-header"></div>

            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="height: 528px;">
                	<div class="widget-toolbar my-tab" style="margin-top: -51px;">
	                    <ul id="tabs" class="nav nav-tabs">
		                    <li id="liTabLista" class="active"><a data-toogle="tab" href="#listaGPC" translate="8.ABA_Lista">Lista</a></li>
		                    <li id="liTabHistorico"><a data-toogle="tab" href="#historico" ng-clock translate="8.ABA_Historico">Histórico</a></li>
	                    </ul>
                    </div>

                    <div class="tab-content">
	                    <div id="listaGPC" class="tab-pane active">
                            <div style="float:right; margin-top: -49px;">
                                <input type="text" class="input-sm" id="pesquisa" name="inputSearch" placeholder="{{ '8.PLACEHOLDER_Pesquisar' | translate }}" style="width: 200px;"
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.8.PLACEHOLDER_Pesquisar' | translate}}"/>
			                    <button id="btnPesquisarLista" type="button" name="find" class="btn btn-info btn-sm" style="padding: 0px 5px; margin-top: 2px;"><i class="fa fa-search"></i>
                                    <span class="hidden-xs" translate="8.BTN_Pesquisar">&nbsp;Pesquisar</span>
                                </button>
                            </div>
		                    <div id="lista" class="demo" style="margin-top:1em;"></div>
	                    </div>
	                    <div id="historico" class="tab-pane">
                            <div style="float:right; margin-top: -49px;">
                                <input type="text" class="input-sm" id="Text1" placeholder="{{ '8.PLACEHOLDER_Pesquisar' | translate }}" ng-model="filter.$" style="width: 200px;"
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.8.PLACEHOLDER_Pesquisar' | translate}}"/>
			                    <button id="btnPesquisarHistorico" type="button" class="btn btn-info btn-sm" ng-click="onSearch()" style="padding: 0px 5px; margin-top: 2px;"><i class="fa fa-search"></i>
                                    <span class="hidden-xs" translate="8.BTN_Pesquisar">&nbsp;Pesquisar</span>
                                </button>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12 information" translate="8.LBL_CliqueRegistroImportacao">Clique em um registro para ver resultado da importação</div>
                            </div>

                            <div class="table-responsive" style="height: 470px; overflow-y: auto; ">
                                <table ng-table="tableParams" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableParams.sorting('codigo', tableParams.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                              <span>#</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('codigo', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('codigo', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('data', tableParams.isSortBy('data', 'asc') ? 'desc' : 'asc')">
                                              <span translate="8.GRID_Data">Data</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('data', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('data', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('arquivo', tableParams.isSortBy('arquivo', 'asc') ? 'desc' : 'asc')">
                                              <span translate="8.GRID_Arquivo">Arquivo</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('arquivo', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('arquivo', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('itens', tableParams.isSortBy('itens', 'asc') ? 'desc' : 'asc')">
                                              <span translate="8.GRID_Itens">Itens</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('itens', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('itens', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('itensimportados', tableParams.isSortBy('itensimportados', 'asc') ? 'desc' : 'asc')">
                                              <span translate="8.GRID_Importados">Importados</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('itensimportados', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('itensimportados', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('resultado', tableParams.isSortBy('resultado', 'asc') ? 'desc' : 'asc')">
                                              <span translate="8.GRID_Resultado">Resultado</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('resultado', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('resultado', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('usuario', tableParams.isSortBy('usuario', 'asc') ? 'desc' : 'asc')">
                                              <span translate="8.GRID_Usuario">Usuário</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('usuario', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('usuario', 'desc')
                                              }"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data" ng-click="onResult(item1)" scroll-to="">
                                            <td ng-style="{ 'width': '2%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                            <td ng-style="{ 'width': '15%' }" sortable="'data'">{{item1.data  | date : 'dd/MM/yyyy HH:mm'}}</td>
                                            <td ng-style="{ 'width': '20%' }" sortable="'arquivo'">{{item1.arquivo}}</td>
                                            <td ng-style="{ 'width': '10%' }" sortable="'itens'">{{item1.itens}}</td>
                                            <td ng-style="{ 'width': '10%' }" sortable="'itensimportados'">{{item1.itensimportados}}</td>
                                            <td ng-style="{ 'width': '15%' }" sortable="'resultado'">{{item1.resultado}}</td>
                                            <td ng-style="{ 'width': '10%' }" sortable="'usuario'">{{item1.usuario}}</td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="7" class="text-center" translate="8.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
