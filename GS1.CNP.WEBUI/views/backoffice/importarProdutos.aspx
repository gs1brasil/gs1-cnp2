﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="importarProdutos.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.importarProdutos" %>

<script type="text/javascript">
    if (typeof angular == 'undefined')
        window.location.href = "/#views/backoffice/importargpc";
    $(".widget-header a").attr("tabindex", "-1");
</script>
<style type="text/css">
.widget-toolbar
{
    float: left;
}
.tab-content
{
    padding: 0px;
}
.inputArquivo
{
    width: 400px;
    display: inline-block;
    height: 19px;
}
#lista
{
    overflow-y: scroll;
    height: 490px;
    width: 100%;
    position: absolute;
}

.strong
{
    font-weight: bold;
}
.grid-container
{
    background-color:#fff;
}
.grid-error
{
    color:#f00;
}

.text-nowrap
{
    white-space:nowrap;
}

a.disabled {
    pointer-events: none;
}

#formDownload table tr td
{
    padding-right: 10px;
}

.marginTable
{
    margin-top: 15px;
    margin-bottom: 5px;
}

.readOnlyCell
{
    background-color: #f5f5f5 !important;
}
</style>
<div ng-controller="ImportarProdutosCtrl" class="page-container" ng-init="formMode = false;resultMode = false;verificacaoItensMode = false;importacaoManualMode = false;itemModelo = {};" style="height: 1000px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container>
            <!-- Tela de Resultados -->
            <div ng-show="resultMode">
                <div class="page-header">
                    <i class="icon-align-justify"></i>
                    <label translate="18.LBL_ImportacaoProduto">Importação de Produtos</label>
                </div>

                <div class="widget-box">
                    <div class="widget-header">
                        <h4><span translate="18.LBL_ResultadoImportacao">Resultado da Importação</span> - <b>{{item.codigo | numberFixedLen:5}}</b></h4>
                        <div class="pull-right" style="margin-top: -40px;">
                            <ajuda codigo="71" tooltip-ajuda="'18.TOOLTIP_Ajuda'"></ajuda>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <form name="form" id="form" novalidate class="form" role="form">
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_TipoItem">Tipo de Item</span>:</label> {{item.tipoitem}}
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.GRID_TipoImportacao">Tipo de Importação</span>:</label> {{item.tipoimportacao}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_Quantidade">Quantidade</span>:</label> {{item.itensimportados}} <span ng-show="item.itensimportados == 1" translate="18.LBL_Item">item</span><span ng-show="item.itensimportados > 1" translate="18.GRID_Itens">itens</span>
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_Resultado">Resultado</span>:</label> {{item.resultado}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_DataHora">Data/Hora</span>:</label> {{item.data  | date : 'dd/MM/yyyy HH:mm'}}
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_Usuario">Usuário</span>:</label> {{item.usuario}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_TempoImportacao">Tempo de Importação</span>:</label> {{item.tempoimportacao | date:'HH:mm:ss'}}
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="form-actions center">
                            <a target="_self" href="/relatorios/{{item.arquivorelatorio}}" scroll-to="" class="btn btn-info btn-sm fileDownloadSimpleRichExperience" ng-if="item.arquivorelatorio">
                                <i class="fa fa-download"></i><span class="hidden-xs">&nbsp; <span translate="18.BTN_BaixarLog">Baixar Log</span></span>
                            </a>
                            <button id="Button5" name="cancel" ng-click="onCancel()" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                <i class="icon-remove bigger-110"></i><span class="hidden-xs" translate="18.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tela de Verificação dos Itens -->
            <div ng-show="verificacaoItensMode">
                <div class="page-header">
                    <i class="icon-align-justify"></i>
                    <label translate="18.LBL_ImportacaoProduto">Importação de Produtos</label>
                </div>

                <div class="widget-box">
                    <div class="widget-header">
                        <h4><span translate="18.LBL_VerificacaoItens">Verificação dos Itens</span></h4>
                        <div class="pull-right" style="margin-top: -40px;">
                            <ajuda codigo="70" tooltip-ajuda="'18.TOOLTIP_Ajuda'"></ajuda>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <form name="form" id="form1" novalidate class="form" role="form">
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_TipoItem">Tipo de Item</span>:</label> {{tipoitem}}
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0; display:none;">
                                        <label class="strong"><span translate="18.LBL_GDSN">GDSN</span>:</label> {{gdsn}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_Quantidade">Quantidade</span>:</label> {{qtdeitem}} itens
                                    </div>
                                    <div class="form-group col-md-6" style="margin-bottom: 0;">
                                        <label class="strong"><span translate="18.LBL_ErrosEncontrados">Erros encontrados</span>:</label> {{qtdeErros}}
                                    </div>
                                </div>
                                <div>
                                    <div class="panel panel-default">
                                        <div class="panel-head">&nbsp;<b translate="18.LBL_StatusImportacao">Status da Importação</b></div>
                                        <div class="panel-body"style="padding: 5px 15px 0px 15px; max-height: 100px;overflow-y: auto;margin-bottom: 5px;margin-right: 5px;">
                                            <span ng-if="listaErros.length > 0">
                                                {{qtdeErros}} <span translate="18.LBL_ErrosEncontradosArquivo">erros foram encontrados em seu arquivo!</span><br />
                                                <span>
                                                    <span translate="18.LBL_CorrijaItensVermelho">Por favor, corrija os itens em vermelho ou retire-o(s) desta importação.</span>
                                                    <ul>
                                                        <li ng-repeat="err in listaErros track by $index" style="margin-bottom: 10px;">
                                                            <span ng-bind-html="err.title"></span>
                                                            <div ng-repeat="message in err.messages" style="margin-left:15px" ng-bind-html="message"></div>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </span>
                                            <span ng-if="listaErros.length == 0" translate="18.LBL_ItensValidados">Itens validados. Pronto para inicar a importação.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding:10px; margin-top: 10px;">
                                    <div id="tableVerificacao" class="hot handsontable"></div>
                                </div>
                            </form>
                        </div>
                        <div class="form-actions center" style="margin-top: 0px;">
                            <button id="validate" name="validate" ng-click="onValidate()" ng-disabled="tableVerificacao.data.length == 0" ng-class="{'fieldDisabled': tableVerificacao.data.length == 0}" scroll-to="" class="btn btn-info btn-sm" type="button" ng-if="exibeValidar">
                                <i class="icon-check bigger-110"></i><span translate="18.BTN_Validar">Validar</span>
                            </button>
                            <button id="import" name="import" ng-click="onImport()" ng-disabled="tableVerificacao.data.length == 0" ng-class="{'fieldDisabled': tableVerificacao.data.length == 0}" scroll-to="" class="btn btn-info btn-sm" type="button" ng-if="exibeImportar">
                                <i class="icon-download-alt bigger-110"></i><span translate="18.BTN_Importar">Importar</span>
                            </button>
                            <button id="cancel" name="cancel" ng-click="onCancel()" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                <i class="icon-remove bigger-110"></i><span translate="18.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tela de Importação Manual -->
            <div ng-show="importacaoManualMode">
                <div class="page-header">
                    <i class="icon-align-justify"></i>
                    <label translate="18.LBL_ImportacaoProduto">Importação de Produtos</label>
                </div>
                <div class="widget-box">
                    <div class="widget-header">
                        <h4><span translate="18.BTN_ImportacaoManual">Importação Manual</span></h4>
                        <div class="pull-right" style="margin-top: -40px;">
                            <ajuda codigo="57" tooltip-ajuda="'18.TOOLTIP_Ajuda'"></ajuda>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <form class="form-inline">
                                <div class="row">
                                    <div class="form-group col-md-12" style="margin-bottom: 0;">
			                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.18.LBL_TipoItem' | translate}}">
                                            <span translate="18.LBL_TipoItem">Tipo de item</span>:
                                        </label>&nbsp;
                                        <select class="form-control input-sm" id="Select3" name="modelo" ng-model="itemModelo.codigo" ng-required=""
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.18.LBL_TipoItem' | translate}}">
                                            <option ng-repeat="modelo in listaModelos" value="{{modelo.codigo}}" ng-selected="{{itemModelo.codigo === modelo.codigo}}">{{modelo.descricao}}</option>
                                        </select>
                                        <div style="display: none/*inline-block*/" ng-if="itemModelo.codigo != 5">
                                            &nbsp;&nbsp;
                                            <label data-toggle="popover" data-trigger="hover" data-content="{{'t.18.LBL_PadraoGDSN' | translate}}"><span translate="18.LBL_PadraoGDSN">Padrão GDSN</span>:</label>
                                            &nbsp;&nbsp;
                                            <input type="radio" name="padrao" ng-model="itemModelo.padraoGDSN" value="1" /> <span translate="18.LBL_Sim">Sim</span>
                                            &nbsp;&nbsp;
                                            <input type="radio" name="padrao" ng-model="itemModelo.padraoGDSN" value="2" /> <span translate="18.LBL_Nao">Não</span>
                                        </div>
                                        &nbsp;&nbsp;
			                            <button type="button" class="btn btn-info btn-sm" ng-click="onLoadTemplate()" style="padding: 0px 5px;"><i class="icon-table bigger-110"></i>
                                            <span translate="18.BTN_CarregarTemplateOnline">Carregar template online</span>
                                        </button>
                                    </div>
                                </div>
                                <div>
                                    <div class="panel panel-default" style="margin-top: 20px;" ng-if="listaErros.length > 0 || exibeMensagem">
                                        <div class="panel-head">&nbsp;<b translate="18.LBL_StatusImportacao">Status da Importação</b></div>
                                        <div class="panel-body"style="padding: 5px 15px 0px 15px; max-height: 100px;overflow-y: auto;margin-bottom: 5px;margin-right: 5px;">
                                            <span ng-show="listaErros.length > 0">
                                                {{qtdeErros}} <span translate="18.LBL_ErrosEncontradosArquivo">erros foram encontrados em seu arquivo!</span><br />
                                                <span>
                                                    <span translate="18.LBL_CorrijaItensVermelho">Por favor, corrija os itens em vermelho ou retire-o(s) desta importação.</span>
                                                    <ul>
                                                        <li ng-repeat="err in listaErros track by $index" style="margin-bottom: 10px;">
                                                            <span ng-bind-html="err.title"></span>
                                                            <div ng-repeat="message in err.messages" style="margin-left:15px" ng-bind-html="message"></div>
                                                        </li>
                                                    </ul>
                                                </span>
                                            </span>
                                            <span ng-show="listaErros.length == 0" translate="18.LBL_ItensValidados">Itens validados. Pronto para inicar a importação.</span>
                                       </div>
                                    </div>
                                </div>
                                <div class="row" ng-show="columnsTemplate.length > 0" style="padding:10px; margin-top: 10px;">
                                    <div id="tableManual" class="hot handsontable"></div>
                                </div>
		                    </form>

                        </div>
                        <div class="form-actions center">
                            <button id="Button1" name="validate" ng-click="onValidateTemplate()" ng-disabled="tableManual.data.length == 0" ng-class="{'fieldDisabled': tableManual.data.length == 0}" scroll-to="" class="btn btn-info btn-sm" type="button" ng-if="exibeValidar">
                                <i class="icon-check bigger-110"></i><span translate="18.BTN_Validar">Validar</span>
                            </button>
                            <button id="Button3" name="import" ng-click="onImportTemplate()" ng-disabled="tableManual.data.length == 0" ng-class="{'fieldDisabled': tableManual.data.length == 0}" scroll-to="" class="btn btn-info btn-sm" type="button" ng-if="exibeImportar">
                                <i class="icon-download-alt bigger-110"></i><span translate="18.BTN_Importar">Importar</span>
                            </button>
                            <button id="Button2" name="cancel" ng-click="onCancel()" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                <i class="icon-remove bigger-110"></i><span translate="18.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Tela de Hitórico -->
        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": formMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='formMode = !formMode'></i>
                <label translate="18.LBL_ImportacaoProduto">Importação de Produtos</label>
            </div>
            <div class="widget-box">
                <div class="widget-header">
                    <h4 translate="18.LBL_NovaImportacao">Nova Importação</h4>
                    <div class="pull-right" style="margin-top: -40px;">
                        <ajuda codigo="56" tooltip-ajuda="'18.TOOLTIP_Ajuda'"></ajuda>
                    </div>
                </div>

                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <div class="widget-main">
                        <form name="formImportar" novalidate role="form" class="form-inline">
			                <label data-toggle="popover" data-trigger="hover" data-content="{{'t.18.LBL_SelecioneArquivo' | translate}}">
                                <span translate="18.LBL_SelecioneArquivo">Selecione o arquivo</span>:
                            </label>&nbsp;
			                <span class="inputArquivo">
				                <input type="file" id="arquivo" name="arquivo" 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.18.LBL_SelecioneArquivo' | translate}}"/>
			                </span>
			                &nbsp;
			                <label data-toggle="popover" data-trigger="hover" data-content="{{'t.18.LBL_SelecioneArquivo' | translate}}">
                                <span>Tipo de item</span>: <%--translate="18.LBL_SelecioneArquivo"--%>
                            </label>&nbsp;
                            <select class="form-control input-sm" id="modelo" name="modelo" ng-model="itemImportacao.codigo" ng-required="">
                                <option ng-repeat="modelo in listaModelos" value="{{modelo.codigo}}" ng-selected="{{itemImportacao.codigo === modelo.codigo}}">{{modelo.descricao}}</option>
                            </select>
			                <br />
                            <div class="text-center" style="margin-top:15px">
			                    <button type="button" class="btn btn-info btn-sm" ng-click="onAssistant()" style="padding: 0px 5px;"><i class="icon-magic bigger-110"></i>
                                    <span class="hidden-xs" translate="18.BTN_IniciarAssistente">Iniciar Assistente</span>
                                </button>
                                &nbsp;&nbsp;&nbsp;
			                    <button type="button" class="btn btn-info btn-sm" ng-click="onImportManual()" style="padding: 0px 5px;"><i class="icon-edit bigger-110"></i>
                                    <span class="hidden-xs" translate="18.BTN_ImportacaoManual">Importação Manual</span>
                                </button>
                            </div>
		                </form>
                    </div>
                </div>
            </div>

            <div class="widget-box">
                <div class="widget-header">
                    <h4 translate="18.LBL_ModeloImportacao">Modelos para Importação</h4>
                </div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <div class="widget-main" style="padding: 4px 12px 0px 12px;">
                        <form id="formDownload" name="formDownload" novalidate role="form"  class="form-inline" style="padding-bottom: 10px; padding-top: 10px;">
                            <div style="display: inline-block; vertical-align: top; /*margin-top: 9px;*/">
			                    <label data-toggle="popover" data-trigger="hover" data-content="{{'t.18.LBL_SelecioneTemplate' | translate}}">
                                    <span translate="18.LBL_SelecioneTemplate">Selecione o template</span>:
                                </label>&nbsp;
                                <div ng-class="{'has-error': formDownload.modelo.$invalid && formDownload.submitted}" style="display: inline-block;">
                                    <select class="form-control input-sm" id="modelo" name="modelo" ng-model="itemModelo.codigo" ng-required=""
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.18.LBL_SelecioneTemplate' | translate}}">
                                        <option ng-repeat="modelo in listaModelos" value="{{modelo.codigo}}" ng-selected="{{itemModelo.codigo === modelo.codigo}}">{{modelo.descricao}}</option>
                                    </select>
                                </div>
                            </div>
                            &nbsp;&nbsp;
                            <div style="display: inline-block;" ng-class="{'marginTable': itemModelo.codigo == 5}">
                                <table>
                                    <tr>
                                        <td data-toggle="popover" data-trigger="hover" data-content="{{'t.18.LBL_TipoArquivo' | translate}}"><span translate="18.LBL_TipoArquivo">Tipo de Arquivo</span>:</td>
                                        <td><input type="radio" name="tipo" ng-model="itemModelo.tipoModelo" value="1" /> Excel</td>
                                        <td><input type="radio" name="tipo" ng-model="itemModelo.tipoModelo" value="2" /> CSV</td>
                                    </tr>
                                    <tr ng-if="itemModelo.codigo != 5" style="display: none">
                                        <td data-toggle="popover" data-trigger="hover" data-content="{{'t.18.LBL_PadraoGDSN' | translate}}"><span translate="18.LBL_PadraoGDSN">Padrão GDSN</span>:</td>
                                        <td><input type="radio" name="padrao" ng-model="itemModelo.padraoGDSN" value="1" /> <span translate="18.LBL_Sim">Sim</span></td>
                                        <td><input type="radio" name="padrao" ng-model="itemModelo.padraoGDSN" value="2" /> <span translate="18.LBL_Nao">Não</span></td>
                                    </tr>
                                </table>
                            </div>
			                &nbsp;&nbsp;
                            <div style="display: inline-block; vertical-align: top; /*margin-top: 9px;*/">
			                    <button type="button" class="btn btn-info btn-sm" ng-click="onDownload()" style="padding: 0px 5px;"><i class="icon-download bigger-110"></i>
                                    <span class="hidden-xs" translate="18.BTN_Download">Download</span>
                                </button>
                            </div>
		                </form>
                        <p class="red" translate="18.MENSAGEM_AlertaDownloadTemplate"></p>
                    </div>
                </div>
            </div>

            <div class="widget-box">
                <div class="widget-header"></div>

                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}" >
                    <div class="widget-main" style="height: 580px;">
                        <div class="widget-toolbar my-tab" style="margin-top: -51px;">
	                        <ul id="tabs" class="nav nav-tabs">
		                        <li id="liTabHistorico" class="active"><a data-toogle="tab" href="#historico" ng-clock translate="18.ABA_Historico">Histórico</a></li>
	                        </ul>
                        </div>
                        <div class="tab-content">
	                        <div id="historico" class="tab-pane active">
                                <div style="float:right; margin-top: -49px;">
                                    <input type="text" class="input-sm" id="Text1" name="inputSearch" placeholder="{{ '18.PLACEHOLDER_Pesquisar' | translate }}" ng-model="filter.$" style="width: 200px;"/>
			                        <button id="btnPesquisarHistorico" name="find" type="button" class="btn btn-info btn-sm" ng-click="onSearch()" style="padding: 0px 5px; margin-top: 2px;"><i class="fa fa-search"></i>
                                        <span class="hidden-xs">&nbsp;<span translate="18.BTN_Pesquisar">Pesquisar</span></span>
                                    </button>
                                </div>

                                <div class="table-responsive" style="height: 560px; overflow-y: auto; ">
                                    <table ng-table="tableParams" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                        <thead>
	                                        <tr>
                                            <th ng-click="tableParams.sorting('codigo', tableParams.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                              <span>#</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('codigo', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('codigo', 'desc')
                                              }"></i>
                                            </th>
		                                        <th ng-click="tableParams.sorting('data', tableParams.isSortBy('data', 'asc') ? 'desc' : 'asc')">
                                              <span translate="18.GRID_Data">Data</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('data', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('data', 'desc')
                                              }"></i>
                                            </th>
		                                        <th ng-click="tableParams.sorting('arquivo', tableParams.isSortBy('arquivo', 'asc') ? 'desc' : 'asc')">
                                              <span translate="18.GRID_Arquivo">Arquivo</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('arquivo', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('arquivo', 'desc')
                                              }"></i>
                                            </th>
		                                        <th ng-click="tableParams.sorting('tipoimportacao', tableParams.isSortBy('tipoimportacao', 'asc') ? 'desc' : 'asc')">
                                              <span translate="18.GRID_TipoGeracao">Tipo Importação</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('tipoimportacao', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('tipoimportacao', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('itensimportados', tableParams.isSortBy('itensimportados', 'asc') ? 'desc' : 'asc')">
                                              <span translate="18.GRID_Itens">Itens</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('itensimportados', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('itensimportados', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('itensnaoimportados', tableParams.isSortBy('itensnaoimportados', 'asc') ? 'desc' : 'asc')">
                                              <span translate="18.GRID_Erros">Erros</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('itensnaoimportados', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('itensnaoimportados', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('resultado', tableParams.isSortBy('resultado', 'asc') ? 'desc' : 'asc')">
                                              <span translate="18.GRID_Resultado">Resultado</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('resultado', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('resultado', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableParams.sorting('usuario', tableParams.isSortBy('usuario', 'asc') ? 'desc' : 'asc')">
                                              <span translate="18.GRID_Usuario">Usuário</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableParams.isSortBy('usuario', 'asc'),
                                                'icon-sort-down': tableParams.isSortBy('usuario', 'desc')
                                              }"></i>
                                            </th>
	                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item1 in $data" ng-click="onResult(item1)" scroll-to="">
                                                <td ng-style="{ 'width': '2%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                                <td ng-style="{ 'width': '15%' }" sortable="'data'">{{item1.data  | date : 'dd/MM/yyyy HH:mm'}}</td>
                                                <td ng-style="{ 'width': '15%' }" sortable="'arquivo'">{{item1.arquivo}}</td>
                                                <td ng-style="{ 'width': '5%' }" sortable="'tipoimportacao'">{{item1.tipoimportacao}}</td>
                                                <td ng-style="{ 'width': '10%' }" sortable="'itensimportados'">{{item1.itensimportados}}</td>
                                                <td ng-style="{ 'width': '10%' }" sortable="'itensnaoimportados'">{{item1.itensnaoimportados}}</td>
                                                <td ng-style="{ 'width': '15%' }" sortable="'resultado'">{{item1.resultado}}</td>
                                                <td ng-style="{ 'width': '10%' }" sortable="'usuario'">{{item1.usuario}}</td>
                                            </tr>
                                            <tr ng-show="$data.length == 0">
                                                <td colspan="8" class="text-center" translate="18.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
	                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="flagAssociadoSelecionado != undefined && !flagAssociadoSelecionado">
        <h1 translate="18.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="18.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>
