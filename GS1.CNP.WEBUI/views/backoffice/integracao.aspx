﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="integracao.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.integracao" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/integracao";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="IntegracaoCtrl" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; exibeMensagem = false; entity = 'papel'; itemFormEspec = { statusregistro: 'novo' };" style="height: 820px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label ng-bind="titulo" translate="53.LBL_Integracao"></label>
            </div>

            <div class="pull-right" style="margin-top: -34px;">
                <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" ng-click="onClean(item)" title="{{ '53.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="9" ng-show="searchMode" tooltip-ajuda="'53.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="10" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'53.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="11" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'53.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            </div>

            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" id="form" novalidate class="form" role="form">
                        <div class="tab-content">
                            <div id="papel" class="tab-pane active">
                                <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row" ng-show="!searchMode">
                                        <div class="form-group col-md-12" style="font-size: 90%;">
                                            (*) - <span translate="53.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.token.$invalid && form.submitted}">
                                            <label for="token"><span translate="53.LBL_Token">Token</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm" maxlength="255" id="token" name="token" ng-model="item.token" ng-readonly="!searchMode" ng-disabled="!searchMode" />
                                        </div>
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.statustoken.$invalid && form.submitted}">
                                            <label for="statustoken" style="display: block"><span translate="53.LBL_StatusToken">Status do Token</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <select class="form-control input-sm" id="statustoken" name="statustoken" ng-model="item.codigostatustoken" ng-required="" ng-options="status.codigo as status.nome for status in statusToken" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.base.dataexpiracao.$invalid && form.submitted}">
                                            <label for="dataexpiracao"><span translate="53.LBL_DataExpiracao">Data de Expiração</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm datepicker" id="dataexpiracao" sbx-datepicker data-date-format="dd/mm/yyyy"
                                                ng-click="datepicker()" maxlength="10" name="dataexpiracao" ng-model="item.dataexpiracao" ng-required="true"/>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-12 col-xs-12">
                                            <div class="table-responsive">
                                                <table ng-table="tableFuncionalidadeIntegracao" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                <label>
                                                                   <input ng-model="item.confirmar"
                                                                        ng-change="marcarDesmarcarIntegracoes(item.confirmar)"
                                                                        type="checkbox"
                                                                        class="ace"/>

                                                                    <span class="lbl"></span>
                                                                </label>
                                                            </th>
                                                            <th ng-click="tableFuncionalidadeIntegracao.sorting('codigo', tableFuncionalidadeIntegracao.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                                              <span>#</span>&nbsp;&nbsp;
                                                              <i class="text-center icon-sort" ng-class="{
                                                                'icon-sort-up': tableFuncionalidadeIntegracao.isSortBy('codigo', 'asc'),
                                                                'icon-sort-down': tableFuncionalidadeIntegracao.isSortBy('codigo', 'desc')
                                                              }"></i>
                                                            </th>
                                                            <th ng-click="tableFuncionalidadeIntegracao.sorting('descricao', tableFuncionalidadeIntegracao.isSortBy('descricao', 'asc') ? 'desc' : 'asc')">
                                                              <span translate="53.GRID_Descricao">Descrição</span>&nbsp;&nbsp;
                                                              <i class="text-center icon-sort" ng-class="{
                                                                'icon-sort-up': tableFuncionalidadeIntegracao.isSortBy('descricao', 'asc'),
                                                                'icon-sort-down': tableFuncionalidadeIntegracao.isSortBy('descricao', 'desc')
                                                              }"></i>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="item1 in $data">
                                                            <td ng-style="{ 'width': '4%' }" class="center" data-header="'checkboxheader.html'">
                                                                <div class="" stop-event='click'>
                                                                    <label>
                                                                        <input ng-model="item1.checked" ng-checked="{{item1.checked == 1}}"
                                                                               ng-click="flagMarcarIntegracao(item1)"
                                                                               type="checkbox"
                                                                               class="ace">

                                                                            <span class="lbl" ng-show="{{usuarioLogado.id != itemForm.codigo}}"></span>
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td ng-style="{ 'width': '5%' }" data-title="'#'" sortable="'codigo'">{{item1.codigo}}</td>
                                                            <td ng-style="{ 'width': '55%' }" data-title="'Descrição'" sortable="'descricao'">{{item1.descricao}}</td>
                                                        </tr>
                                                        <tr ng-show="$data.length == 0">
                                                            <td colspan="7" class="text-center" translate="53.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions center" ng-show="editMode">
                                        <button class="btn btn-info btn-sm" name="find" scroll-to="" ng-cloak ng-show="isPesquisa" ng-click="onSearch(item)" type="button">
                                            <i class="icon-search bigger-110"></i><span translate="53.BTN_Pesquisar">Pesquisar</span>
                                        </button>
                                        <button class="btn btn-info btn-sm" scroll-to="" ng-cloak ng-show="!isPesquisa" ng-click="onSave(item)" type="button">
                                            <i class="icon-save bigger-110"></i><span translate="53.BTN_Salvar">Salvar</span>
                                        </button>
                                        <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                            <i class="icon-remove bigger-110"></i><span translate="53.BTN_Cancelar">Cancelar</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label ng-bind="titulo" translate="53.LBL_Integracao"></label>
            </div>

            <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '53.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
                <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '53.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
                <ajuda codigo="8" tooltip-ajuda="'53.TOOLTIP_Ajuda'"></ajuda>
            </div>

            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                        <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                    </a>
                    <div class="widget-main" style="min-height: 600px;">
                        <div class="row">
                            <div class="form-group col-md-12" style="font-size: 90%;" translate="53.LBL_CliqueRegistroEditar">
                                Clique em um registro para editar
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table ng-table="tableIntegracao" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <thead>
                                    <tr>
                                        <th ng-click="tableIntegracao.sorting('codigo', tableIntegracao.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                          <span>#</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableIntegracao.isSortBy('codigo', 'asc'),
                                            'icon-sort-down': tableIntegracao.isSortBy('codigo', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableIntegracao.sorting('token', tableIntegracao.isSortBy('token', 'asc') ? 'desc' : 'asc')">
                                          <span translate="53.GRID_Token">Token</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableIntegracao.isSortBy('token', 'asc'),
                                            'icon-sort-down': tableIntegracao.isSortBy('token', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableIntegracao.sorting('status', tableIntegracao.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                          <span translate="53.GRID_Status">Status</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableIntegracao.isSortBy('status', 'asc'),
                                            'icon-sort-down': tableIntegracao.isSortBy('status', 'desc')
                                          }"></i>
                                        </th>
                                        <%--<th>
                                          <span translate="53.GRID_Acao">Ação</span>
                                        </th>--%>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                        <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                        <td ng-style="{ 'width': '55%' }" sortable="'token'">{{item1.token}}</td>
                                        <td ng-style="{ 'width': '35%'}" sortable="'status'">{{item1.status | status}}</td>
                                        <%--<td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Ação'">
                                            <a class="red" href="" ng-click="onDelete(item1); $event.stopPropagation();" title="{{ '53.TOOLTIP_Excluir' | translate }}"><i class="icon-trash bigger-130"></i></a>
                                        </td>--%>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="7" class="text-center" translate="53.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="53.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="53.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>

</div>
