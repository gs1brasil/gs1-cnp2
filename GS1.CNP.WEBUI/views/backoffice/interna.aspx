﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="interna.aspx.cs" Inherits="GS1.CNP.WEBUI.interna" %>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CNP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Language" content="pt-BR">

    <style>
        .splash {
            display: none;
        }

        [ng-cloak].splash {
            display: block !important;
        }

        .splash {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            filter: alpha(opacity=50);
            opacity: 0.5;
            background: #fff;
            cursor: wait;
        }

        .initblock-ui-overlay {
            z-index: 10001;
            border: 0;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
        }

            .initblock-ui-overlay.block-ui-visible {
                background-color: #fff;
                opacity: .5;
                -ms-filter: "alpha(opacity=50)";
                cursor: wait;
            }

        .initblock-ui-message-container {
            position: fixed;
            top: 50%;
            left: 0;
            right: 0;
            height: 0;
            text-align: center;
            z-index: 10002;
            font-family: Calibri;
        }

        .initblock-ui-message {
            cursor: wait;
            display: inline-block;
            text-align: left;
            background-color: #2b669a;
            color: #f5f5f5;
            padding: 20px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            font-size: 20px;
            font-weight: 700;
        }
        
        .img-responsive{
	        width: 100%;
	    }
	    
	    .linkNotificacao:hover {
	        color: #f26334 !important;   
	    }
	    
	    .boleto:hover {
	        color: #f26334 !important;  
	    }
	    
	    .animatedIcon {
	        animation: 3s ease 1s normal none 5 running ringing !important; 
	        display: inline-block !important; 
	        transform-origin: 50% 0 0 !important;
	    }
    </style>
</head>

<body ng-app="CNP.Backoffice" ng-init="userGS1 = true; userAssociado = false;" ng-controller="IndexCtrl">

    <div class="splash initblock-ui-overlay initblock-ui-overlay.block-ui-visible" ng-cloak=""></div>
    <div ng-show="initblock-ui-overlay.block-ui-visible" class="initblock-ui-message-container">
        <div class="initblock-ui-message"><span translate="38.MENSAGEM_Aguarde">Aguarde</span> ...</div>
    </div>

    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.css" />
    <%--<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />--%>
    <link rel="stylesheet" href="/Styles/index/bootstrap.min.css" />
    <link href="/Styles/index/bootstrap-theme.min.css" rel="stylesheet" type="text/css" /> 
    <link href="/Styles/index/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" /> 


    <%--<link href="/Styles/login.css" rel="stylesheet">--%>

    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap-wysiwyg.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular/angular.js"></script>

    <script src="/Scripts/lib/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/Scripts/lib/ng-ckeditor/ng-ckeditor-1.0.1.js" type="text/javascript"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- basic styles -->

    <link rel="stylesheet" href="/bower_components/ng-table/ng-table.css" />
    <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" href="/bower_components/angular-block-ui/dist/angular-block-ui.min.css" />
    <link rel="stylesheet" href="/bower_components/angular-treeview/css/angular.treeview.css">
    <link href="/bower_components/angular-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <link href="/bower_components/angular-colorpicker/css/jquery.minicolors.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/bower_components/handsontable/dist/handsontable.full.js"></script>
    <link type="text/css" rel="stylesheet" href="/bower_components/handsontable/dist/handsontable.full.css">

    <!--[if IE 7]>
    <link rel="stylesheet" href="styles/font-awesome-ie7.min.css" />
    <![endif]-->

    <!-- page specific plugin styles -->

    <!-- fonts -->
    <link rel="stylesheet" href="/styles/ace-fonts.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="/styles/ace.min.css" />
    <link rel="stylesheet" href="/styles/ace-rtl.min.css" />
    <link rel="stylesheet" href="/styles/ace-skins.min.css" />
    <link rel="stylesheet" href="/styles/jquery.gritter.css" />
    <link href="/assets/css/ng-tags-input.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/styles/main.css" />

    <link rel="stylesheet" href="/styles/select2/select2.css" />

    <link rel="stylesheet" href="/assets/css/mobile.css" />

    <link href="/Styles/cnp-theme.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/cnp.css" rel="stylesheet" type="text/css" />
    <link href="/Scripts/lib/jquery-plugin-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="styles/ace-ie.min.css" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/lib/html5shiv.js"></script>
    <script type="text/javascript" src="scripts/lib/respond.min.js"></script>
    <![endif]-->

    <!--[if IE 9]>
    <link rel="stylesheet" href="/styles/main-ie.css" />
	<![endif]-->

     <%= JSNLog.JavascriptLogging.Configure() %>

    <header class="cabecalho-site">
        <div class="posicao-cabecalho">
            <h1>
                <div class="logoCNP">
					<a id="logoCNP" href="/backoffice/home">
						<img id="Img1" class="ms-siteiconCNP-img" name="onetidHeadbnnr0" src="/images/logo_cnp.png" alt="CNP">
					</a> 
				</div>

                <div class="welcome hidden-xs">
					<div class="difer"><span translate="38.LBL_BemVindoCNP">Bem vindo ao</span> CNP</div>
					<div class="slogan" translate="38.LBL_CadastroNacionalProdutos">Cadastro Nacional de Produtos</div>
				</div>
				<div class="ancillary-nav" style="margin-top: -25px; margin-right: 15px">  <%--hidden-xs hidden-sm--%>
                     <div class="btn-group pull-right btnHeader">
                        <button type="button" class="btn btn-info dropdown-toggle btn-xs hidden-xs" data-toggle="dropdown" ng-cloak>
                           <i class="fa icon-comments"></i> <span class="hidden-xs hidden-sm">{{idiomaAtual}}</span> <i class="fa fa-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-user" role="menu" style="padding: 5px 18px 16px;">                          
                            <li ng-click="alteraIdioma(idioma.codigo, idioma.descricao);" style="cursor: pointer; margin-top: 8px;" ng-repeat="idioma in idiomas">&nbsp; {{idioma.nome}} ({{idioma.descricao}}) &nbsp;<i class="fa icon-ok" ng-if="codigoidioma == idioma.codigo"></i> </li>                          
                        </ul>
                    </div>
                    <div class="btn-group pull-right btnHeader" style="margin-right: 1px">
                        <button type="button" class="btn btn-warning dropdown-toggle btn-xs" data-toggle="dropdown">
                            <%-- <i class="fa icon-question-sign"></i><i class="fa fa-chevron-down"></i> --%>
                            <i class="fa icon-question-sign"></i>&nbsp;<span translate="38.LBL_Ajuda" class="hidden-xs hidden-sm">Ajuda</span>&nbsp;<i class="fa fa-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-user" role="menu" style="padding: 5px 18px 16px;">
                            <%-- <li><h4 translate="38.LBL_Ajuda">Ajuda</h4></li> --%>
                            <li ng-click="abreAtendimentoOnline();" style="cursor: pointer;"><i class="fa icon-comment"></i> &nbsp; <span translate="38.LBL_AtendimentoOnline">Atendimento Online</span></li>
                            <li ng-click="abreFAQ();" style="cursor: pointer; margin-top: 8px;"><i class="fa icon-question"></i> &nbsp;&nbsp; <span translate="38.LBL_FAQ">FAQ</span></li>
                            <li ng-click="abrePassoaPasso();" style="cursor: pointer; margin-top: 8px;"><i class="fa icon-info-sign"></i> &nbsp;&nbsp; <span translate="38.LBL_AjudaPassoaPasso">Ajuda Passo a Passo</span></li>                          
                        </ul>
                    </div>
                    <div class="btn-group pull-right btnHeader" style="margin-right: 1px;">
                        <button type="button" class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" descricao>
                            <i class="fa fa-user"></i><span class="hidden-xs" ng-if="usuarioLogado != null" ng-cloak class="hidden-xs hidden-sm"> &nbsp; {{usuarioLogado.nome | cortaTexto: 20}} &nbsp; </span><i class="fa fa-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-user" role="menu" style="padding: 5px 18px 8px;">
                            <li><h4 translate="38.LBL_MinhaConta">Minha Conta</h4></li>
                            <li><a href="/backoffice/meusDados"><i class="fa fa-user"></i> &nbsp; <span translate="38.LBL_MeusDados">Meus dados</span></a></li>
                            <li><a href="/backoffice/troca_senha"><i class="fa fa-key"></i> &nbsp;<span translate="38.LBL_TrocarSenha">Trocar senha</span></a></li>
                            <li class="divider"></li>
                            <li><a href="/backoffice/aceiteTermoAdesao"><i class="fa fa-file-text-o"></i> &nbsp;<span translate="38.LBL_TermoAdesao">Termo de Adesão</span></a></li>
                            <li class="divider"></li>
                            <li><a href="/backoffice/logout"><i class="fa fa-sign-out"></i> &nbsp;<span translate="38.LBL_Sair">Sair</span></a></li>
                        </ul>
                    </div>
                    <div class="btn-group pull-right purple btnHeader" style="margin-right: 1px;" ng-cloak ng-class="{'open': (notificacoes[0].mensagem != null && notificacoes[0].mensagem != undefined && notificacoes[0].mensagem != '') || (boletosDisponiveis.length > 0) }">
                        <button class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" style="background-color: #892e65 !important;">
                            <i class="icon-bell-alt" ng-class="{'animatedIcon' : boletosDisponiveis.length >= 1}"></i>
                            <span ng-show="notificacoes.length + boletosDisponiveis.length <= 1" translate="38.LBL_Notificacao" class="hidden-xs hidden-sm">Notificação</span>
                            <span ng-show="notificacoes.length + boletosDisponiveis.length > 1" translate="38.LBL_Notificacoes" class="hidden-xs hidden-sm">Notificações</span>
                            <span class="badge badge-important" ng-bind="notificacoes.length + boletosDisponiveis.length" style="color: #FFF;">0</span>
                        </button>
                        <ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close" style="margin-right: 6px;">
                           <%-- <li class="dropdown-header">
                                <i class="icon-warning-sign" ng-bind=""></i>
                                <span ng-bind="notificacoes.length">0</span>&nbsp;
                                <span ng-show="notificacoes.length <= 1">Notificação</span>
                                <span ng-show="notificacoes.length > 1">Notificações</span>
                            </li>
                            <li ng-if="notificacoes.length > 0">
                                <div class="clearfix">
                                    <span class="pull-left" style="line-height: 20px; padding: 5px">
                                        <a href="/backoffice/notificacoes" ng-show="notificacoes.length <= 1">Você possui {{notificacoes.length}} notificação</a>
                                        <a href="/backoffice/notificacoes" ng-show="notificacoes.length > 1">Você possui {{notificacoes.length}} notificações</a>
                                    </span>
                                </div>
                            </li>
                            <li ng-if="notificacoes.length == 0">
                                <div class="clearfix">
                                    <span class="pull-left" style="line-height: 20px; padding: 5px">
                                        <span>Você não possui notificação</span>
                                    </span>
                                </div>
                            </li>--%>

                            <li class="dropdown-header">
                                <i class="icon-warning-sign" ng-bind=""></i>
                                <span ng-bind="notificacoes.length + boletosDisponiveis.length">0</span>&nbsp;
                                <span ng-show="notificacoes.length + boletosDisponiveis.length <= 1" translate="38.LBL_Notificacao">Notificação</span>
                                <span ng-show="notificacoes.length + boletosDisponiveis.length > 1" translate="38.LBL_Notificacoes">Notificações</span>
                            </li>
                            <li ng-if="boletosDisponiveis.length > 0">
                                <div class="clearfix" style="padding-top: 5px; padding-bottom:5px;">
                                    
                                    <div class="text red" style="margin-bottom: 10px;"> A(s) fatura(s) abaixo consta(m) em aberto em nossos sistemas.</div>
                                    
									<div class="" ng-repeat="boleto in boletosDisponiveis" style="margin-bottom: 12px;">
                                        <a href="#" ng-click="AbrirBoletos(boleto.numbco)" class="boleto">
                                            Valor: {{'R$ ' + boleto.valortitulo}}<br>
                                            Vencida em: {{boleto.datavencimento}}<br>
                                            Tipo de Título: {{boleto.tipotitulonegocio}}&nbsp;<i class="icon-only ace-icon fa icon-external-link" title="Gerar Boleto"></i>
										</a>                                       
									</div>
                                </div>
                            </li>
                            <li ng-if="notificacoes.length > 0" ng-repeat="noticacao in notificacoes">
                                <div class="clearfix">
                                    <span class="pull-left" style="line-height: 20px; padding: 5px">
                                        <a class="linkNotificacao" href="{{noticacao.linkurl}}">{{noticacao.mensagem}}</a>
                                    </span>
                                </div>
                            </li>
                            <li ng-if="notificacoes.length + boletosDisponiveis.length == 0">
                                <div class="clearfix">
                                    <span class="pull-left" style="line-height: 20px; padding: 5px">
                                        <span translate="38.LBL_SemNotificacoes">Você não possui notificação</span>
                                    </span>
                                </div>
                            </li>
                        </ul>                        
                    </div>

                    <div class="btn-group pull-right green" style="margin-top: 7px; margin-right: 1px;" ng-cloak>
                        <button type="button" class="btn dropdown-toggle btn-xs green" data-toggle="dropdown" style="background-color: #2c7659 !important;">
                            <i class="icon-envelope icon-animated-vertical" ng-class="{'animatedIcon' : mensagensDisponiveis.length >= 1}"></i>                            
                            <span ng-show="mensagensDisponiveis.length <= 1" translate="38.LBL_Mensagem">Mensagem</span>
                            <span ng-show="mensagensDisponiveis.length > 1" translate="38.LBL_Mensagens">Mensagens</span>
                            <span class="badge badge-success" ng-bind="totalMensagensNaoLidas" style="color: #FFF;">0</span>
                        </button>
                        <ul class="pull-right dropdown-navbar navbar-green dropdown-menu dropdown-caret dropdown-close">

                            <li class="dropdown-header">
                                <i class="icon-envelope-alt" ng-bind=""></i>
                                <span ng-bind="totalMensagensNaoLidas">0</span>&nbsp;
                                <span translate="38.LBL_Naolidas">Não Lidas</span> &nbsp; /
                                <span ng-bind="totalMensagens">0</span>&nbsp;
                                <span ng-show="mensagensDisponiveis.length <= 1" translate="38.LBL_Mensagem">Mensagem</span>
                                <span ng-show="mensagensDisponiveis.length > 1" translate="38.LBL_Mensagens">Mensagens</span>                                
                            </li>
                            <li ng-if="mensagensDisponiveis.length > 0" ng-repeat="mensagem in mensagensDisponiveis">
                                                            
									<a href="/backoffice/mensagem/{{mensagem.codigo}}" style="color:#002c6c">										
										<span class="msg-body">
											<span class="msg-title" ng-class="{bolder : mensagem.dataleitura == ''}">												
												    {{mensagem.assunto}}
											    </span>
											<span class="msg-time">
												<i class="icon-time"></i>
												<span>{{mensagem.datasincronizacao}}</span>
											</span>
										</span>
									</a>
								

                            </li>
                            <li ng-if="mensagensDisponiveis.length > 0">
									<a href="/backoffice/mensagem" translate="38.LBL_VejaTodasMensagens">
										Veja todas as mensagens
										<i class="icon-arrow-right"></i>
									</a>
								</li>
                            <li ng-if="mensagensDisponiveis.length == 0">
                                <div class="clearfix">
                                    <span class="pull-left" style="line-height: 20px; padding: 5px">
                                        <span translate="38.LBL_SemMensagens">Você não possui mensagens.</span>
                                    </span>
                                </div>
                            </li>


                        </ul>
                    </div>

                    <div class="input-group col-md-3 pull-right selecionarAssociado hidden-mobile-xs" ng-cloak ng-if="usuarioLogado.id_tipousuario == 1">
                        <input id="typea" type="text" ng-model="associadoatual" maxlength="255" ng-readonly="bloqAssociado"
                            placeholder="{{ '38.PLACEHOLDER_PesquisarAssociado' | translate }}" typeahead="associadosDoUsuario as associadosDoUsuario.cad+'-'+associadosDoUsuario.nome for associadosDoUsuario in BuscarAssociadosAutoComplete($viewValue)"
                            typeahead-min-length="3" typeahead-on-select="alteraAssociado(associadoatual);" class="typeahead form-control input-sm"
                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.38.PLACEHOLDER_PesquisarAssociado' | translate}}"/>

                            <span class="input-group-addon input-sm" ng-show="bloqAssociado;" ng-click="disableBloqAssociado()" style="cursor: pointer">
                                <i class="icon-remove"></i>
                            </span>
                    </div>
                    <div class="input-group col-md-3 pull-right" style="margin-top: -4px; margin-right: 1px;" ng-cloak ng-if="usuarioLogado.id_tipousuario != 1">
                        <select id="select" class="input-sm" style="padding: 0; width: 100%" name="produto" ng-model="associadoatual" ng-change="alteraAssociado(associadoatual)"
                            ng-options="associado.codigo as associado.nome for associado in associadosDoUsuario" ng-selected="$index == 0"
                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.38.PLACEHOLDER_PesquisarAssociado' | translate}}">
                        </select>
                    </div>
                </div>
            </h1>
            <div class="clearfix"> </div>
            <span class="col-md-12 col-xs-12" style="color: gray; font-family: Calibri; font-size: 12px;" ng-show="ultimoAcesso.acesso != undefined">
                <span class="pull-right">Último acesso: {{ultimoAcesso.acesso}}</span>
            </span>
        </div>
    </header>

    <div class="main-container" id="main-container">
        <div class="main-container-inner">
            <a class="menu-toggler" id="menu-toggler" href="#">
                <span class="menu-text"></span>
            </a>
            <div class="input-group col-md-3 pull-right selecionarAssociado hidden-mobile-md" ng-cloak ng-if="usuarioLogado.id_tipousuario == 1">
                <input id="Text1" type="text" ng-model="associadoatual" maxlength="255" ng-readonly="bloqAssociado"
                    placeholder="{{ '38.PLACEHOLDER_PesquisarAssociado' | translate }}" typeahead="associadosDoUsuario as associadosDoUsuario.nome for associadosDoUsuario in BuscarAssociadosAutoComplete($viewValue)"
                    typeahead-min-length="3" typeahead-on-select="alteraAssociado(associadoatual);" class="typeahead form-control input-sm"
                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.38.PLACEHOLDER_PesquisarAssociado' | translate}}"/>

                    <span class="input-group-addon input-sm" ng-show="bloqAssociado;" ng-click="disableBloqAssociado()" style="cursor: pointer">
                        <i class="icon-remove"></i>
                    </span>
            </div>
            <div class="sidebar menu-min" id="sidebar" resize-sidebar>
                <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                    <div class="sidebar-shortcuts-large" ng-cloak ng-if="usuarioLogado.id_tipousuario == 1">
                        <a class="btn btn-danger" title="Usuários" ng-href="/backoffice/usuario">
                            <i class="icon-user"></i>
                        </a>
                        <a class="btn btn-success" title="Produtos" ng-href="/backoffice/produtos">
                            <i class="icon-shopping-cart"></i>
                        </a>
                        <a class="btn btn-info" title="Etiquetas" ng-href="/backoffice/geracaoEtiquetas">
                            <i class="icon-file-text"></i>
                        </a>
                        <a class="btn btn-warning" title="Relatórios" ng-href="/backoffice/relatoriosGS1">
                            <i class="icon-signal"></i>
                        </a>
                    </div>

                    <div class="sidebar-shortcuts-large" ng-cloak ng-if="usuarioLogado.id_tipousuario == 2">
                        <a class="btn btn-danger" title="Usuários" ng-href="/backoffice/usuario">
                            <i class="icon-user"></i>
                        </a>
                        <a class="btn btn-success" title="Produtos" ng-href="/backoffice/produtos">
                            <i class="icon-shopping-cart"></i>
                        </a>
                        <a class="btn btn-info" title="Etiquetas" ng-href="/backoffice/geracaoEtiquetas">
                            <i class="icon-file-text"></i>
                        </a>
                        <a class="btn btn-warning" title="Relatórios" ng-href="/backoffice/relatoriosAssociado">
                            <i class="icon-signal"></i>
                        </a>
                    </div>

                    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                        <span class="btn btn-danger"></span>
                        <span class="btn btn-success"></span>
                        <span class="btn btn-info"></span>
                        <span class="btn btn-warning"></span>
                    </div>
                </div>
                <!-- #sidebar-shortcuts -->

                <navigation menu="menu"></navigation>
            
                <div class="sidebar-collapse" id="sidebar-collapse">
                    <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
                </div>
                
            </div>

            <div class="main-content">
                <div class="navigation" style="padding-top: 90px; margin-left: 5px;">
                    <div class="">
                        
                    </div>
                </div>


                <%--<div class="breadcrumbs" id="breadcrumbs">

                    <ul class="breadcrumb">
                        <li class="active">
                            <i class="icon-home home-icon"></i>
                            <a href="#">Home</a>
                        </li>
                    </ul>
                    <!-- .breadcrumb -->
                </div>--%>

                <div class="page-content" ng-view=""></div>
            </div>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="icon-double-angle-up icon-only bigger-110"></i>
            </a>
        </div>

        <ng-include src="'/views/backoffice/typeaheads.html'"></ng-include>

        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!--[if lt IE 9]>
            <script type="text/javascript" src="bower_components/es5-shim/es5-shim.js"></script>
            <script type="text/javascript" src="bower_components/json3/lib/json3.min.js"></script>
        <![endif]-->
        <%--<%= JSNLog.JavascriptLogging.Configure() %>--%>

        <script type="text/javascript" src="/bower_components/angular-i18n/angular-locale_pt-br.js"></script>
        <script type="text/javascript" src="/bower_components/angular-bootstrap/ui-bootstrap.js"></script>
        <script type="text/javascript" src="/scripts/lib/ui-bootstrap-tpls-0.11.0.min.js" ></script>

        <!-- Angular - Google Maps API -->
        <script type="text/javascript" src="/bower_components/lodash/lodash.min.js" ></script>
        <script type="text/javascript" src="/bower_components/angular-simple-logger/dist/angular-simple-logger.min.js" ></script>
        <script type="text/javascript" src="/bower_components/angular-google-maps/dist/angular-google-maps.js" ></script>

        <script type="text/javascript" src="/bower_components/ng-table/ng-table.js"></script>
        <script type="text/javascript" src="/bower_components/angular-resource/angular-resource.js"></script>
        <script type="text/javascript" src="/bower_components/angular-cookies/angular-cookies.js"></script>
        <script type="text/javascript" src="/bower_components/angular-sanitize/angular-sanitize.js"></script>
        <script type="text/javascript" src="/bower_components/angular-route/angular-route.js"></script>
        <script type="text/javascript" src="/bower_components/angular-animate/angular-animate.js"></script>
        <script type="text/javascript" src="/bower_components/ngScrollTo/ng-scrollto.js"></script>
        <script type="text/javascript" src="/bower_components/angular-treeview/angular.treeview.js"></script>

        <script type="text/javascript" src="/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="/bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js"></script>


        <script type="text/javascript" src="/bower_components/es5-shim/es5-shim.min.js"></script>
        <script type="text/javascript" src="/bower_components/angular-file-upload/angular-file-upload.min.js"></script>

        <script src="/bower_components/angular-multiselecttree/ng-multiselecttree.js" type="text/javascript"></script>
        <link href="/bower_components/angular-multiselecttree/ng-multiselecttree.css" rel="stylesheet" type="text/css" />

        <script src="/scripts/lib/canvasjs.min.js" type="text/javascript"></script>
        <script src="/scripts/lib/jquery.canvasjs.min.js" type="text/javascript"></script>

        <script src="/scripts/lib/FileSaver.js" type="text/javascript"></script>

        <script src="/Scripts/lib/jquery-plugin-circliful/js/jquery.circliful.js" type="text/javascript"></script>
        <script src="/Scripts/lib/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="/Scripts/lib/flot/jquery.flot.pie.min.js" type="text/javascript"></script>

        <script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script>
        <script src="/bower_components/angular-recaptcha/release/angular-recaptcha.js" type="text/javascript"></script>

        <script type="text/javascript" src="/scripts/lib/mask.js"></script>
        <%--<script type="text/javascript" src="/assets/js/angular-input-masks/masks.js"></script>--%>
        <script src="/bower_components/string-mask/src/string-mask.js" type="text/javascript"></script>
        <script src="/bower_components/angular-input-masks/angular-input-masks-standalone.js" type="text/javascript"></script>
        <script src="/bower_components/angular-input-masks/angular-input-masks-dependencies.js" type="text/javascript"></script>
        <script src="/bower_components/angular-input-masks/angular-input-masks.br.js" type="text/javascript"></script>

        <%--<script type="text/javascript" src="/scripts/lib/ace-extra.min.js"></script>--%>
        <script src="/Scripts/lib/uncompressed/ace-extra.js" type="text/javascript"></script>

        <script type="text/javascript" src="/scripts/lib/ace-elements.min.js"></script>
        <script type="text/javascript" src="/scripts/lib/ace.min.js"></script>
        <script type="text/javascript" src="/scripts/lib/jquery.gritter.min.js"></script>
        <script type="text/javascript" src="/scripts/lib/jquery-ui-1.10.3.custom.min.js"></script>
        <script type="text/javascript" src="/scripts/lib/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="/bower_components/angular-block-ui/dist/angular-block-ui.min.js"></script>
        <script type="text/javascript" src="/bower_components/angular-images/flow.js"></script>
        <script type="text/javascript" src="/bower_components/angular-images/fusty-flow.js"></script>
        <script type="text/javascript" src="/bower_components/angular-images/fusty-flow-factory.js"></script>
        <script type="text/javascript" src="/bower_components/angular-images/ng-flow.js"></script>
        <script type="text/javascript" src="/bower_components/angular-colorpicker/js/bootstrap-colorpicker-module.js"></script>
        <script type="text/javascript" src="/bower_components/angular-colorpicker/js/jquery.minicolors.js"></script>
        <script src="/bower_components/angular-x2js/dist/x2js.min.js" type="text/javascript"></script>
        <script src="/bower_components/x2js/xml2json.min.js" type="text/javascript"></script>
        <script src="/bower_components/angular-wysiwyg/dist/angular-wysiwyg.js" type="text/javascript"></script>

        <script src="/Scripts/lib/fuelux/fuelux.wizard.min.js" type="text/javascript"></script>
        <script src="/bower_components/ngstorage/ngStorage.min.js" type="text/javascript"></script>
        <script src="/bower_components/angular-translate/angular-translate.min.js" type="text/javascript"></script>
        <script src="/bower_components/angular-translate-storage-local/angular-translate-storage-local.min.js" type="text/javascript"></script>
        <script src="/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js" type="text/javascript"></script>
        <script src="/bower_components/angular-translate-handler-log/angular-translate-handler-log.min.js" type="text/javascript"></script>
        <%--<script src="/bower_components/logToServer.js/logToServer.js" type="text/javascript"></script>--%>

        <script type="text/javascript" src="/scripts/backoffice/app.js"></script>
        <script type="text/javascript" src="/scripts/backoffice/core/app.directives.js"></script>
        <script type="text/javascript" src="/scripts/backoffice/core/app.filters.js"></script>
        <script type="text/javascript" src="/scripts/backoffice/core/app.factory.js"></script>
        <script type="text/javascript" src="/scripts/backoffice/core/services/genericservice.js"></script>

        <script src="/Scripts/lib/angular.trackedtable.js" type="text/javascript"></script>

        <script type="text/javascript" src="/scripts/backoffice/core/controllers/indexctrl.js"></script>
        <script type="text/javascript" src="/scripts/backoffice/core/services/indexservice.js"></script>

        <script src="/scripts/backoffice/perfil/perfilctrl.js" type="text/javascript"></script>

        <script src="/scripts/backoffice/usuario/tipousuarioService.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/usuario/usuarioservice.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/usuario/trocasenhactrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/usuario/trocasenhaservice.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/usuario/usuarioctrl.js" type="text/javascript"></script>

        <script src="/scripts/backoffice/agenciareguladora/agenciasReguladorasCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/cartasCertificadas/cartasCertificadasCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/dadosDoAssociado/dadosAssociadoCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/gestaoPerfil/gestaoPerfilCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/meusDados/meusDadosCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/papelgln/papelCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/parametrosSistema/parametrosSistemaCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/usuarioCallcenter/UsuarioCallcenterCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/produtoTipo/produtoTipoCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/geracaoEtiquetas/geracaoEtiquetasCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/uploadCartas/uploadCartasCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/localizacoesFisicas/localizacoesFisicasCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/termosAdesao/termosAdesaoCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/aceiteTermoAdesao/aceiteTermoAdesaoCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/notificacoes/notificacoesCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/gerirChaves/gerirChavesCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/geracaoEpcRfid/geracaoEpcRfidCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/idioma/idiomactrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/ajuda/ajudactrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/cadastroFAQ/cadastrofaqctrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/FAQ/faqctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/pesquisaSatisfacao/pesquisaSatisfacaoCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/cadastroAjudaPassoAPasso/cadastroajudapassoapassoctrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/ajudaPassoAPasso/ajudapassoapassoctrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/cartasCertificadasGs1/cartasCertificadasGs1Ctrl.js" type="text/javascript"></script>

        <script src="/scripts/backoffice/importacaoProdutos/importacaoProdutosCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/cadastroProdutos/cadastroProdutosCtrl.js" type="text/javascript"></script>
		
        <script src="/scripts/backoffice/produto/produtoctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/produto/produtoservice.js" type="text/javascript"></script>

        <script src="/Scripts/backoffice/importarGPC/importarGPCCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/importarProdutos/importarProdutosCtrl.js" type="text/javascript"></script>

        <script src="/Scripts/backoffice/CertificacaoProdutos/certificacaoProdutoCtrl.js" type="text/javascript"></script>
        <%--<script src="/Scripts/backoffice/CertificacaoProdutosPesosMedidas/certificacaoProdutoPesosMedidasCtrl.js" type="text/javascript"></script>--%>
        <script src="/Scripts/backoffice/lotes/lotesCtrl.js" type="text/javascript"></script>

        <script src="/Scripts/backoffice/banner/espacobannerctrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/banner/bannerservice.js" type="text/javascript"></script>

        <script src="/Scripts/backoffice/integracao/integracaoCtrl.js" type="text/javascript"></script>

        <!--GEPIR-->
        <script src="/Scripts/backoffice/estatisticas/estatisticasCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/configuracoesGepir/configuracaoGepirCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/gepirCodigoBarras/gepircodigobarrasctrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/codigoSerieUnidadeLogistica/codigoserieunidadelogisticactrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/localizacaoGlobal/localizacaoglobalctrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/gepirNomeEmpresa/gepirNomeEmpresaCtrl.js" type="text/javascript"></script>

        <script src="/scripts/lib/ng-tags-input.min.js" type="text/javascript"></script>
        <script src="/scripts/lib/ngDraggable.js" type="text/javascript"></script>
        <script src="/scripts/lib/ng-infinite-scroll.min.js" type="text/javascript"></script>

        <script src="/scripts/backoffice/relatorios/relatorioAssociadoGS1Ctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioHistoricoStatusProdutoAssociadoCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioHistoricoStatusProdutoGS1Ctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioImpressaoGS1Ctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioLocalizacaoFisicaAssociadoCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioLocalizacaoFisicaGS1Ctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioLogErros.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioLoginAcessoAssociadoCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioLoginAcessoGS1Ctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioLogMonitoramentoctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioProdutoAssociadoCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatorioProdutoGS1Ctrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatoriosAssociadoCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/relatorios/relatoriosGS1Ctrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/relatorios/relatorioMensalCNPCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/relatorios/relatorioHistoricoUsoAssociadoCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/relatorios/relatorioHistoricoUsoAssociadoGS1Ctrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/relatorios/relatorioAssociadosTermosAdesaoCtrl.js" type="text/javascript"></script>

        <script src="/scripts/backoffice/dashboard/DashboardCtrl.js" type="text/javascript"></script>
        <script src="/scripts/backoffice/aprovaImportacaoProdutos/aprovaImportacaoProdutosCtrl.js" type="text/javascript"></script>
        <script src="/Scripts/backoffice/relatorios/relatorioCertificadoCtrl.js" type="text/javascript"></script>

        <script type="text/javascript" src="/scripts/lib/select2/select2.min.js"></script>
        <script src="/bower_components/jquery-file-download/src/Scripts/jquery.fileDownload.js" type="text/javascript"></script>
        <script src="/scripts/lib/ajaxfiledownload.js" type="text/javascript"></script>
        <script src="/scripts/lib/download.js" type="text/javascript"></script>
        
        <script src="/scripts/backoffice/mensagem/mensagemctrl.js" type="text/javascript"></script>

        <form id="PDFHandlerRelatorio" action='/PDFHandlerRelatorio.ashx' method='POST'>
            <input type='hidden' name='pdfdata' id='pdfdata' value='' />
        </form>

        <form id="CSVHandler" action='/CSVHandler.ashx' method='POST'>
            <input type='hidden' name='csvdata' id='csvdata' value='' />
        </form>

        <form id="WordHandler" action='/WordHandler.ashx' method='POST'>
            <input type='hidden' name='worddata' id='worddata' value='' />
        </form>
        
        <iframe id="iWordHandlerCartaProdutos" name="iWordHandlerCartaProdutos" src="javascript:false" ng-show="false" width="0px"></iframe> 
        
        <%-- usado para gerar carta unificada gs1 --%>
        <form id="WordHandlerCartaProdutos" action="/WordHandler.ashx" method='POST' target="iWordHandlerCartaProdutos">
            <input type='hidden' name='worddatacartaprodutos' id='worddatacartaprodutos' value='' />
        </form>         

        <form id="VcardHandler" action='/VcardHandler.ashx' method='POST'>
            <input type='hidden' name='codigolocalizacaofisica' id='codigolocalizacaofisica' value='' />
        </form>

        <form id="XmlHandler" action='/XmlHandler.ashx' method='POST'>
            <input type='hidden' name='xmldata' id='xmldata' value='' />
        </form>

        <form id="ModeloHandler" action='/DownloadTemplateHandler.ashx' method='POST'>
            <input type='hidden' name='codigomodelo' id='codigomodelo' value='' />
            <input type='hidden' name='tipoModelo' id='tipoModelo' value='' />
        </form>

        <form id="EPCRFIDHandler" action='/EPCRFIDHandler.ashx' method='POST'>
            <input type='hidden' name='epcdata' id='epcdata' value='' />
        </form>
        <!-- endbuild -->

        <%--<script>
            $('document').ready(function () {
                $(".sidebar").addClass("menu-min");

                $('.navbar-nav').on('click', 'li', function () {
                    $('.navbar-nav li.active').removeClass('active');
                    $(this).addClass('active');
                });

                //                $('.navbar-nav').on('mouseover', 'li', function () {
                //                    $(this).addClass('active');
                //                });

                //                $('.navbar-nav').on('mouseout', 'li', function () {
                //                    $(this).removeClass('active');
                //                });

                $(".dropdown").on("mouseover", function () {
                    $(this).addClass("open");
                    $(this).addClass("active");
                });

                $(".dropdown").on("mouseout", function () {
                    $(this).removeClass("open");
                    $(this).removeClass("active");
                });

                $("#select").select2({ width: '100%' });
            });
        </script>--%>

        <style>
            body {
                overflow-y: scroll;
            }

            body.modal-open {
                overflow-y: scroll;
                margin: 0;
            }

            .modal {
                overflow: auto;
            }
        </style>

        <script>
            $('document').ready(function () {
                $('#menu-lateral li').on('click', 'li', function () {
                    $('#menu-lateral li li.active').removeClass('active');
                    $(this).addClass('active');
                });

                //$("#select").select2({ width: '100%' });
                
            });
        </script>

        <script>
            $("#sidebar").slimScroll({
                height: window.innerHeight - 50,
                width: '195px',
                disableFadeOut: true
            });

            $("#sidebar").parent(".slimScrollDiv").css("position", "fixed");
        </script>
</body>

</html>
