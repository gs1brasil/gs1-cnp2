﻿using System;
using System.Web;
//using System.Web.UI.WebControls;
using System.Web.Services;
using GS1.CNP.BLL.Model;

namespace GS1.CNP.WEBUI
{
    public partial class interna : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["USUARIO_LOGADO"] == null)
            {
                Response.Redirect("/login?url=" + Request.QueryString["url"]);
            }
            else
            {
                Login user = Session["USUARIO_LOGADO"] as Login;

                if (user == null || user.id_tipousuario == 3)
                {
                    Response.Redirect("/");
                }
            }
        }

        [WebMethod]
        public static void Logout()
        {
            HttpContext.Current.Session.Abandon();
        }
    }
}