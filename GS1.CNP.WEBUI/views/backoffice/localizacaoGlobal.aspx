﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="localizacaoGlobal.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.localizacaoGlobal" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .wysiwyg-menu{
        text-align: center !important;
    }
</style>

<div ng-controller="LocalizacaoGlobalCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false;" style="height:1050px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container style="width: 100%;">
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label ng-bind="titulo"></label>
            </div>
            <div class="pull-right" style="margin-top: -34px;">
                <a class="btn btn-danger btn-xs" ng-show="searchMode" title="{{ '68.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="99"></ajuda>
            </div>
            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <form name="form" novalidate class="form" role="form" >
                            <div class="tab-content" style="min-height:400px;">
                                <div id="telas" class="tab-pane active">
                                    <div class="row">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.gln.$invalid && form.submitted}">
                                            <label for="gtin" translate="68.LBL_GLN">GLN </label>
                                            <input id="gln" name="gln" type="text" class="form-control input-sm" ng-model="itemForm.gln" required integer maxlength="13"/>
                                        </div>
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.tipoconsulta.$invalid && form.submitted}">
                                            <label for="tipoconsulta" translate="68.LBL_TipoConsulta">Tipo de Consulta </label>
                                            <select class="form-control input-sm" id="tipoconsulta" name="tipoconsulta" ng-model="itemForm.tipoconsulta"
                                                ng-options="tipo.codigo as tipo.nome for tipo in tiposConsulta" ng-change="alteraTipoConsulta();" required>
                                                <option value=""></option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4" style="margin-top: 29px;">
                                            <button name="consultar" class="btn btn-info btn-xs" type="button" ng-click="onSearch(itemForm)">
                                                <i class="icon-search bigger-110"></i> <span translate="68.BTN_Consultar">Consultar</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="table-responsive" ng-show="itemForm.tipoconsulta == 2  && consultaefetuada" style="margin-top: 20px;">
                                        <table ng-table="tableCampos" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th ng-click="tableCampos.sorting('gln', tableCampos.isSortBy('gln', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_GLN">Número de Localização Global </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('gln', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('gln', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('codigoretorno', tableCampos.isSortBy('codigoretorno', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_CodigoRetorno"> Código de Retorno </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('codigoretorno', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('codigoretorno', 'desc')
                                                      }"></i>
                                                    </th>

                                                    <th ng-click="tableCampos.sorting('gcp', tableCampos.isSortBy('gcp', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_GCP"> GCP </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('gcp', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('gcp', 'desc')
                                                      }"></i>
                                                    </th>                                               
                                           

                                                     <th ng-click="tableCampos.sorting('nomefantasia', tableCampos.isSortBy('nomefantasia', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="69.GRID_NomeFantasia"> Nome Fantasia </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('nomefantasia', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('nomefantasia', 'desc')
                                                      }"></i>
                                                    </th>

                                                    <th ng-click="tableCampos.sorting('postalcode', tableCampos.isSortBy('postalcode', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_postalcode"> Código Postal </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('postalcode', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('postalcode', 'desc')
                                                      }"></i>
                                                    </th>

                                                    <th ng-click="tableCampos.sorting('city', tableCampos.isSortBy('city', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_city"> Cidade </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('city', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('city', 'desc')
                                                      }"></i>
                                                    </th>

                                                    <th ng-click="tableCampos.sorting('countrycode', tableCampos.isSortBy('countrycode', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_countrycode"> Código ISO do País </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('countrycode', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('countrycode', 'desc')
                                                      }"></i>
                                                    </th>

                                                    <th ng-click="tableCampos.sorting('partyrole', tableCampos.isSortBy('partyrole', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_partyrole"> Regra de Acesso da Empresa </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('partyrole', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('partyrole', 'desc')
                                                      }"></i>
                                                    </th>

                                                    <th ng-click="tableCampos.sorting('additionalpartyidentification', tableCampos.isSortBy('additionalpartyidentification', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_additionalpartyidentification"> additionalpartyid </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('additionalpartyidentification', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('additionalpartyidentification', 'desc')
                                                      }"></i>
                                                    </th>
                                             
                                                    <th ng-click="tableCampos.sorting('streetaddressone', tableCampos.isSortBy('streetaddressone', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_streetaddressone"> Rua </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('streetaddressone', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('streetaddressone', 'desc')
                                                      }"></i>
                                                    </th>

                                                    <th ng-click="tableCampos.sorting('contato', tableCampos.isSortBy('contato', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_contato"> Contato </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('contato', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('contato', 'desc')
                                                      }"></i>
                                                    </th>

                                                                                                                                      

                                                    <th ng-click="tableCampos.sorting('provedorgln', tableCampos.isSortBy('provedorgln', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="67.GRID_provedorgln"> Provedor de Informações GLN </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('provedorgln', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('provedorgln', 'desc')
                                                      }"></i>
                                                    </th>

                                             
                               
                                                </tr>
                                            
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="campo in $data" style="cursor:pointer;word-wrap: break-word;">                                                
                                                    <td ng-style="{ 'width': '20%' }" sortable="'gln'">{{campo.gln}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'codigoretorno'">{{campo.returnCode}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'gcp'">{{campo.gcp}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'nomefantasia'">{{campo.partyName}}</td>                                                
                                                    <td ng-style="{ 'width': '5%' }" sortable="'postalcode'">{{campo.postalCode}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'city'">{{campo.city}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'city'">{{campo.countryISOCode}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'city'">{{campo.partyRole[0].Value}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'additionalpartyid'">{{campo.additionalPartyIdentification[0].Value}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'endereco'">{{campo.streetAddress[0]}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'contato'">{{campo.contact}}</td>
                                                    <td ng-style="{ 'width': '5%' }" sortable="'gln'">{{campo.informationProviderGln}}</td>


                                                </tr>
                                                <tr ng-show="consultaefetuada && $data.length == 0">
                                                    <td colspan="13" class="text-center"><span translate="67.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</span></td>
                                                </tr>
                                            </tbody>
                                            {{$data.length}}
                                        </table>
                                    </div>

                                    <div class="table-responsive" ng-show="itemForm.tipoconsulta == 1  && consultaefetuada" style="margin-top: 20px;">
                                        <table ng-table="tableCamposDono" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th ng-click="tableCampos.sorting('gtin', tableCampos.isSortBy('gln', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_GLN"> GLN </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('gln', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('gln', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('codigoretorno', tableCampos.isSortBy('codigoretorno', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_CodigoRetorno"> Código de Retorno </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('codigoretorno', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('codigoretorno', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('nomefantasia', tableCampos.isSortBy('nomefantasia', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_NomeFantasia"> Nome Fantasia </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('nomefantasia', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('nomefantasia', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('additionalpartyid', tableCampos.isSortBy('additionalpartyid', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_AdditionalPartyID"> AdditionalPartyID </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('additionalpartyid', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('additionalpartyid', 'desc')
                                                      }"></i>
                                                    </th>
                                                    <th ng-click="tableCampos.sorting('endereco', tableCampos.isSortBy('endereco', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                      <span translate="66.GRID_Endereco"> Endereço </span>&nbsp;&nbsp;
                                                      <i class="text-center icon-sort" ng-class="{
                                                        'icon-sort-up': tableCampos.isSortBy('endereco', 'asc'),
                                                        'icon-sort-down': tableCampos.isSortBy('endereco', 'desc')
                                                      }"></i>
                                                    </th>
                                                </tr>
                                            
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="campo in $data" style="cursor:pointer;">
                                                    <td ng-style="{ 'width': '20%' }" sortable="'gln'">{{campo.gepirRequestedKey.requestedKeyValue}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'codigoretorno'">{{campo.returnCode.Value}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'nomefantasia'">{{campo.gS1KeyLicensee != null ? campo.gS1KeyLicensee.partyName[0] : ''}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'additionalpartyid'">{{campo.gS1KeyLicensee != null ? campo.gS1KeyLicensee.additionalPartyIdentification[0].Value : ''}}</td>
                                                    <td ng-style="{ 'width': '20%' }" sortable="'endereco'">{{campo.address != null ? campo.address.streetAddressOne : ''}}</td>
                                                </tr>                                               
                                                <tr ng-show="consultaefetuada && $data.length == 0">
                                                    <td colspan="5" class="text-center"><span translate="66.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="17.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="17.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>

