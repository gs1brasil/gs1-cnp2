﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="localizacoesFisicas.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.localizacoesFisicas" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="LocalizacoesFisicasCtrl" class="page-container" ng-init="onInit(); editMode = false; searchMode = false; exibeVoltar = false; exibeMensagem = false; entity = 'localizacaofisica';" style="height: 970px;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <watch-fetch ids="item.codigo"></watch-fetch>
        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label ng-bind="titulo"></label>
            </div>

            <div class="widget-toolbar my-tab" style="margin-top: -36px;">
                <ul id="tabs-localizacao" class="nav nav-tabs">
                    <li id="liTabBase" class="active">
                        <a data-toogle="tab" href="#base" translate="14.ABA_InformacoesBasicas">Informações Básicas</a>
                    </li>
                    <li id="liTabMap" ng-show="!searchMode" ng-click="showMap()">
                        <a data-toogle="tab" href="#map" translate="14.ABA_Mapa">Mapa</a>
                    </li>
                </ul>
            </div>

            <div class="pull-right" style="margin-top: -32px; margin-right: 235px;" ng-show="!searchMode">
                <ajuda codigo="37" ng-show="item != undefined && item.codigo == undefined" tooltip-ajuda="'14.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="38" ng-show="item != undefined && item.codigo != undefined" tooltip-ajuda="'14.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            </div>
            <div class="pull-right" style="margin-top: -32px; margin-right: 185px;" ng-show="searchMode">
                <a class="btn btn-danger btn-xs" title="{{ '14.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="36" ng-cloak tooltip-ajuda="'14.TOOLTIP_Ajuda'"></ajuda>
            </div>
           
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" id="form" novalidate class="form" role="form">
                        <div class="tab-content">
                            <div id="base" class="tab-pane active">
                                <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row" ng-show="!searchMode">
                                        <div class="form-group col-md-12" style="font-size: 90%;">
                                            (*) - <span translate="14.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                            <label for="nome"><span translate="14.LBL_NomeLocalizacaoContrato">Nome da Localização/Contato</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm" maxlength="100" id="nome" name="nome" ng-model="item.nome" required="required" 
                                                ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_NomeLocalizacaoContrato' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.papel.$invalid && form.submitted}">
                                            <label for="papel"><span translate="14.LBL_PapelFuncao">Papel/Função</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <select class="form-control input-sm" id="papel" name="papel" ng-model="item.codigopapelgln" required ng-options="item1.codigo as item1.nome for item1 in papeis" 
                                                ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_PapelFuncao' | translate}}"/>                                                
                                        </div>
                                        <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                            <label for="status"><span translate="14.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <select class="form-control input-sm" id="status" name="status" ng-model="item.status" required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Status' | translate}}">
                                                <option value="1" ng-selected="item.status == 1">Ativo</option>
                                                <option value="0" ng-selected="item.status == 0">Inativo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.gln.$invalid && form.submitted}">
                                            <label for="gln"><span translate="14.GRID_NumeroIdentificacaoGLN">Número de Identificação GLN</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm" maxlength="13" id="gln" name="gln" ng-model="item.gln" ng-readonly="!searchMode"  
                                                ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                                onkeydown="return (event.ctrlKey || event.altKey || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) || (95<event.keyCode && event.keyCode<106) || (event.keyCode==8) || (event.keyCode==9) || (event.keyCode>34 && event.keyCode<40) || (event.keyCode==46) )" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.GRID_NumeroIdentificacaoGLN' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-3 col-xs-12" ng-show="!searchMode && item.codigo != undefined && item.codigo != ''">
                                            
                                        </div>
                                        <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.dataalteracao.$invalid && form.submitted}" ng-show="!searchMode && item.codigo != undefined && item.codigo != ''">
                                            <label for="dataalteracao"><span translate="14.LBL_DataHoraInsercaoAlteracao">Data/Hora de Inclusão/Alteração</span> </label>
                                            <input type="text" class="form-control input-sm" id="Text3" name="dataalteracao" ng-model="item.dataalteracao" ng-readonly="true"  
                                                ng-disabled="true" data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_DataHoraInsercaoAlteracao' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                       
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.telefone.$invalid && form.submitted}" ng-if="!searchMode">
                                            <label for="telefone" translate="14.LBL_Telefone">Telefone </label>
                                            <input type="text" class="form-control input-sm" maxlength="50" id="telefone" name="telefone" ng-model="item.telefone" ui-mask="(99)99999999?9" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Telefone' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.email.$invalid && form.submitted}">
                                            <label for="email" translate="14.LBL_Email">E-mail </label>
                                            <input type="text" class="form-control input-sm" maxlength="255" id="email" name="email" ng-model="item.email" ng-pattern="/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Email' | translate}}"/>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.pais.$invalid && form.submitted}">
                                            <label for="pais"><span translate="14.LBL_Pais">País</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <select class="form-control input-sm" id="pais" name="pais" ng-model="item.pais" required ng-change="limpaEndereco();" 
                                                ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Pais' | translate}}">
                                                <option ng-repeat="item1 in Paises" value="{{item1.pais}}" ng-selected="{{item1.pais == item.pais}}">{{item1.pais}}</option>                                                
                                            </select>
                                            <%--<select class="form-control input-sm" id="pais" name="pais" ng-model="item.pais" required ng-change="limpaEndereco();" 
                                                ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Pais' | translate}}"
                                                ng-options="item1.pais as item1.pais for item1 in Paises">                                             
                                            </select>--%>
                                            
                                        </div>
                                         <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.cep.$invalid && form.submitted}">                                         
                                            <span ng-if="item.pais == 'Brazil'">
                                                <label for="cep"><span translate="14.LBL_CEP">CEP</span> <span ng-show='!searchMode'>(*)</span></label>
                                                <input type="text" class="form-control input-sm" sbx-maxlength="10" id="cep" name="cep" ng-model="item.cep" ui-mask="99.999-999" 
                                                    ng-blur="buscarWebServiceCep(item.cep)" required ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_CEP' | translate}}"/>
                                            </span>
                                            <span ng-if="item.pais != 'Brazil'">
                                                <label for="cep"><span translate="14.LBL_CodigoPostal">Código Postal</span> <span ng-show='!searchMode'>(*)</span></label>
                                                <input type="text" class="form-control input-sm" maxlength="20" id="Text1" name="cep" ng-model="item.cep" required 
                                                ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_CodigoPostal' | translate}}"/>
                                            </span>
                                        </div>
                                     </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.endereco.$invalid && form.submitted}">
                                            <label for="endereco"><span translate="14.LBL_Endereco">Endereço</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm" ng-disabled="desabilitaCampos" ng-readonly="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                            sbx-maxlength="100" id="endereco" name="endereco" ng-model="item.endereco" required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Endereco' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.numero.$invalid && form.submitted}">
                                            <label for="numero"><span translate="14.LBL_Numero">Número</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm" maxlength="50" id="numero" name="numero" ng-model="item.numero" required 
                                            ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Numero' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.bairro.$invalid && form.submitted}">
                                            <label for="bairro"><span translate="14.LBL_Bairro">Bairro</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm" sbx-maxlength="50" id="bairro" name="bairro" ng-model="item.bairro" required 
                                            ng-disabled="desabilitaCampos" ng-readonly="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Bairro' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.complemento.$invalid && form.submitted}">
                                            <label for="complemento" translate="14.LBL_Complemento">Complemento </label>
                                            <input type="text" class="form-control input-sm" maxlength="100" id="complemento" name="complemento" ng-model="item.complemento" 
                                            ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Complemento' | translate}}"/>
                                        </div>
                                        <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.estado.$invalid && form.submitted}">
                                        
                                            <label for="estado"><span translate="14.LBL_Estado">Estado</span> <span ng-show='!searchMode'>(*)</span></label> 
                                            <select class="form-control input-sm" id="estado" name="estado" ng-model="item.estado" required 
                                                ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Estado' | translate}}" ng-show="item.codigo == undefined || item.codigo == ''">
                                                <option ng-repeat="item1 in UFS" value="{{item1.uf}}" ng-selected="item1.uf == item.estado">{{item1.uf}}</option>                                                
                                            </select>
                                            <input type="text" class="form-control input-sm" name="nomeestado" ng-model="item.estado" 
                                                ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Estado' | translate}}" ng-show="item.codigo != undefined && item.codigo != ''"/>
                                        </div>
                                        <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.cidade.$invalid && form.submitted}">
                                            <label for="cidade"><span translate="14.LBL_Cidade">Cidade</span> <span ng-show='!searchMode'>(*)</span></label>
                                            <input type="text" class="form-control input-sm" sbx-maxlength="50" id="cidade" name="cidade" ng-model="item.cidade" required 
                                            ng-disabled="desabilitaCampos" ng-class="{'fieldDisabled': desabilitaCampos}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Cidade' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.observacao.$invalid && form.submitted}" ng-if="!searchMode">
                                            <label for="observacao" translate="14.LBL_Observacoes">Observações </label>
                                            <textarea class="form-control input-sm" id="observacao" sbx-maxlength="500" name="observacao" rows="3" ng-model="item.observacao" style="resize: vertical;"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_Observacoes' | translate}}"></textarea>
                                            <span style="font-size: 80%;">(<span translate="14.LBL_TamanhoMaximo">Tamanho máximo</span>: 500)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span translate="14.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.observacao.$viewValue.length || 0 }}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-8" ng-if="!searchMode">
                                            <label for="imagem" translate="14.LBL_EnviarFoto">Enviar Foto </label>
                                            <div class="widget-main">
                                                <input type="file" name="nomeimagem" id="imagemLocalizacao" fileread="item.nomeimagem" onchange="angular.element(this).scope().alterImage()" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.14.LBL_EnviarFoto' | translate}}"/>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4" ng-show="nomeimagem != undefined">
                                            <ul class="draglist ace-thumbnails" ng-show="item.nomeimagem != undefined">
                                                <li>
                                                    <img height="100px" width="120px" src="{{item.nomeimagem}}"/></span>
                                                    <div class="tools tools-bottom" ng-show="item.nomeimagem != 'images/no-photo.jpg' && item.nomeimagem != 'images/no-video.jpg'">
                                                        <a href="" ng-click="ExcluirImagem(item)">
                                                            <i class="fa fa-remove" style="color: red"></i>
                                                        </a>
                                                    </div>
                                                </li>                                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="map" class="tab-pane">
                                <div class="row">                                        
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group col-md-10" ng-class="">
                                            <label for="endereco"><span translate="14.LBL_Mapa">Mapa</span> <span style="font-size:90%;">(<span translate="14.LBL_DigiteEnderecoMapa">Para a localização física, basta digitar o endereço na caixa de texto do mapa</span>)</span></label> 
                                            <a class="pull-right" href=""><i class="icon-eraser" title="{{ '14.TOOLTIP_LimparDadosMapa' | translate }}" ng-click="limparMarkers()" style="text-decoration: none;"></i></a>
                                        </div>                                      
                                        <div class="col-md-2">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-xs-12">
                                        <div class="form-group col-md-10" ng-class="">                                                
                                            <div class="ng-binding" style="height: 300px !important;padding-left: 0px;margin-top: -15px;"> 
                                                <script type="text/ng-template" id="searchbox.tpl.html">                                                        
                                                    <input type="text" id="mapsAddress" placeholder="{{ '23.PLACEHOLDER_BuscarEndereco' | translate }}" style="width:80%;margin-top:5px" id="searchBoxMapa">
                                                </script>                                                    
                                                <div style="width:100%;height:300px;" ng-show="displayed">
                                                    <ui-gmap-google-map center="map.center" zoom="map.zoom">
                                                        <ui-gmap-search-box template="searchbox.template" position="searchbox.position"  events="searchbox.events"></ui-gmap-search-box>                                                        
                                                        <ui-gmap-markers idkey="map.idkey" models="map.markers" coords="'self'" icon="'icon'" click="'onClicked'"></ui-gmap-markers>                                                                
                                                    </ui-gmap-google-map>                                                        
                                                </div>
                                            </div>                                                                                                                                                                                     
                                        </div>                                      
                                        <div class="col-md-2">                                                
                                        </div>
                                    </div>                                        
                                </div>
                            </div>
                            <div class="form-actions center" ng-show="editMode">
                                <button class="btn btn-info btn-sm" scroll-to="" ng-cloak ng-show="isPesquisa" ng-click="onSearch(item)" type="button">
                                    <i class="icon-search bigger-110"></i><span translate="14.BTN_Pesquisar">Pesquisar</span>
                                </button>
                                <button class="btn btn-info btn-sm" scroll-to="" ng-cloak ng-show="!isPesquisa" ng-click="onSave(item)" type="button">
                                    <i class="icon-save bigger-110"></i><span translate="14.BTN_Salvar">Salvar</span>
                                </button>
                                <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                    <i class="icon-remove bigger-110"></i><span translate="14.BTN_Cancelar">Cancelar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label ng-bind="titulo"></label>
            </div>

            <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                <a class="btn btn-info btn-xs" title="{{ '14.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
                <a class="btn btn-success btn-xs" title="{{ '14.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
                <ajuda codigo="35"></ajuda>
            </div>

            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                        <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                    </a>
                    <div class="widget-main" style="min-height: 768px;">
                        <div class="row">
                            <div class="form-group col-md-12" style="font-size: 90%;" translate="14.LBL_CliqueRegistroEditar">
                                Clique em um registro para editar
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table ng-table="tableLocalizacoes" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <thead>
                                    <tr>
                                        <th ng-click="tableLocalizacoes.sorting('codigo', tableLocalizacoes.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                          <span>#</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLocalizacoes.isSortBy('codigo', 'asc'),
                                            'icon-sort-down': tableLocalizacoes.isSortBy('codigo', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLocalizacoes.sorting('nome', tableLocalizacoes.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                          <span translate="14.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLocalizacoes.isSortBy('nome', 'asc'),
                                            'icon-sort-down': tableLocalizacoes.isSortBy('nome', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLocalizacoes.sorting('gln', tableLocalizacoes.isSortBy('gln', 'asc') ? 'desc' : 'asc')">
                                          <span translate="14.GRID_NumeroIdentificacaoGLN">Número de Identificação GLN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLocalizacoes.isSortBy('gln', 'asc'),
                                            'icon-sort-down': tableLocalizacoes.isSortBy('gln', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLocalizacoes.sorting('status', tableLocalizacoes.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                          <span translate="14.GRID_Status">Status</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLocalizacoes.isSortBy('status', 'asc'),
                                            'icon-sort-down': tableLocalizacoes.isSortBy('status', 'desc')
                                          }"></i>
                                        </th>
                                        <th><span translate="14.GRID_Vcard">VCard</span></th>
                                        <th><span translate="14.GRID_Acao">Ação</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                        <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                        <td ng-style="{ 'width': '40%' }" sortable="'nome'">{{item1.nome}}</td>
                                        <%--<td ng-style="{ 'width': '15%' }" data-title="'Prefixo GS1'" sortable="'prefixo'">{{item1.prefixo}}</td>--%>
                                        <td ng-style="{ 'width': '20%' }" sortable="'gln'">{{item1.gln}}</td>
                                        <td ng-style="{ 'width': '17%'}" sortable="'status'">{{item1.status | status}}</td>
                                        <td ng-style="{ 'width': '3%' }" class="action-buttons center">
                                            <a class="blue" href="" title="{{ '14.TOOLTIP_GerarVCard' | translate }}" ng-click="onGenerateVCard(item1); $event.stopPropagation();"><i class="icon-download bigger-130"></i></a>
                                        </td>
                                        <td ng-style="{ 'width': '3%' }" class="action-buttons center">
                                            <a class="red" href="" title="{{ '14.TOOLTIP_Excluir' | translate }}" ng-click="onDelete(item1); $event.stopPropagation();"><i class="icon-trash bigger-130"></i></a>
                                        </td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="7" class="text-center" translate="14.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="14.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="14.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
</div>

