﻿using System;
using System.Web;

namespace GS1.CNP.WEBUI.views.backoffice
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
            Response.Redirect("/");
        }
    }
}