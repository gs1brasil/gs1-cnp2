﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lotes.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.lotes" %>
<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/lotes";
    }

</script>

<style type="text/css">
    .input-group-btn>button{
        padding: 2px;
    }

    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak],
    .ng-cloak, .x-ng-cloak,
    .ng-hide {
        display: none !important;
    }

    .has-error-label
    {
        color: #f09784;
    }

    .btn-gray {
        color: Gray;
    }

</style>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="LotesCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = ''; gerar = false;">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <div class="form-container" form-container id="formcontainer"> <%--id usado para corrigir bug do ie no layout, no momento de mudar do grid para o form--%>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label>{{ titulo }}</label>
            </div>

            <div class="pull-right" style="margin-top:-35px; margin-right:2px;">
                <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" title="{{ '47.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
                <ajuda codigo="86" ng-show="searchMode" tooltip-ajuda="'47.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="87" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'47.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                <ajuda codigo="88" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'47.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            </div>

            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div class="tab-content">
                        <div id="geracaoEpc" class="tab-pane active" ng-show="!isPesquisa">
                            <form name="form" id="form" novalidate class="form" role="form">
                                <div class="row" ng-show="!searchMode">
                                    <%--<div class="form-group col-md-12" style="font-size: 90%;">
                                        <h4 translate="47.LBL_Lotes">Lotes</h4>
                                        <label translate="47.LBL_SelecioneItemGerarLote">Busque e selecione o item para gerar o lote</label>
                                    </div>--%>
                                    <div class="form-group col-md-12 col-xs-12" style="font-size: 90%;">
                                        (*) - <span translate="47.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                    </div>
                                </div>
                                <div>
                                    <%--<h3 class="header smaller lighter blue">
                                        <span translate="47.LBL_ProdutoSelecionado">Produto <span class="hidden-xs">Selecionado</span></span>
                                        <span class="span5 pull-right" ng-if="initMode || item.statuslote == 1">
                                            <label class="pull-right inline">
                                                <div class="form-group">
                                                    <a class="btn btn-success btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto != undefined && item.codigoproduto != null"
                                                        ng-click="onProduto(item)" title="{{ '47.TOOLTIP_PesquisarProduto' | translate }}">
                                                        <i class="fa fa-pencil"></i> <span class="hidden-xs" translate="47.BTN_AlterarProduto">Alterar Produto</span>
                                                    </a>
                                                    <a class="btn btn-info btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto == undefined || item.codigoproduto == null"
                                                        ng-click="onProduto(item)" title="{{ '47.TOOLTIP_PesquisarProduto' | translate }}">
                                                        <i class="fa fa-search"></i> <span class="hidden-xs" translate="47.BTN_SelecionarProduto">Selecionar Produto</span>
                                                    </a>
                                                </div>
                                            </label>
                                        </span>
                                    </h3>--%>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="widget-box">
                                                <div class="widget-header">
                                                    <h4 class="smaller">
                                                        <span translate="47.LBL_ProdutoSelecionado">Produto <span class="hidden-xs">Selecionado</span></span>
                                                    </h4>
                                                </div>
                                                <div class="widget-body">
                                                    <div class="widget-main">
                                                        <p class="muted">
                                                            <div ng-show="item.codigoproduto != undefined && item.codigoproduto != null">
                                                                <label>Produto: {{item.productdescription}}</label><br />
                                                                <label ng-show="item.globaltradeitemnumber != undefined && item.globaltradeitemnumber != null">GTIN: {{item.globaltradeitemnumber}}</label>
                                                            </div>
                                                            <div id="nenhumProdutoSelecionado" ng-show="item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == ''"
                                                                ng-class="{'has-error-label': (item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == '') && form.submitted}">
                                                                <label translate="47.LBL_NenhumProdutoSelecionado">Nenhum produto selecionado.</label>
                                                            </div>
                                                        </p>
                                                        <hr>
                                                        <p ng-if="initMode || item.statuslote == 1">
                                                            <a class="btn btn-success btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto != undefined && item.codigoproduto != null"
                                                                ng-click="onProduto(item)" title="{{ '47.TOOLTIP_PesquisarProduto' | translate }}">
                                                                <i class="fa fa-pencil"></i> <span class="hidden-xs" translate="47.BTN_AlterarProduto">Alterar Produto</span>
                                                            </a>
                                                            <a class="btn btn-info btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto == undefined || item.codigoproduto == null"
                                                                ng-click="onProduto(item)" title="{{ '47.TOOLTIP_PesquisarProduto' | translate }}">
                                                                <i class="fa fa-search"></i> <span class="hidden-xs" translate="47.BTN_SelecionarProduto">Selecionar Produto</span>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div ng-show="item.codigoproduto != undefined && item.codigoproduto != null">
                                        <label>Produto: {{item.productdescription}}</label><br />
                                        <label ng-show="item.globaltradeitemnumber != undefined && item.globaltradeitemnumber != null">GTIN: {{item.globaltradeitemnumber}}</label>
                                    </div>
                                    <div id="nenhumProdutoSelecionado" ng-show="item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == ''"
                                        ng-class="{'has-error-label': (item.codigoproduto == undefined || item.codigoproduto == null || item.codigoproduto == '') && form.submitted}">
                                        <label translate="47.LBL_NenhumProdutoSelecionado">Nenhum produto selecionado.</label>
                                    </div>
                                    <span class="span5 pull-right" ng-if="initMode || item.statuslote == 1">
                                        <label class="pull-right inline">
                                            <div class="form-group">
                                                <a class="btn btn-success btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto != undefined && item.codigoproduto != null"
                                                    ng-click="onProduto(item)" title="{{ '47.TOOLTIP_PesquisarProduto' | translate }}">
                                                    <i class="fa fa-pencil"></i> <span class="hidden-xs" translate="47.BTN_AlterarProduto">Alterar Produto</span>
                                                </a>
                                                <a class="btn btn-info btn-sm btn-margin-top" style="border: medium none;" ng-cloak ng-if="item.codigoproduto == undefined || item.codigoproduto == null"
                                                    ng-click="onProduto(item)" title="{{ '47.TOOLTIP_PesquisarProduto' | translate }}">
                                                    <i class="fa fa-search"></i> <span class="hidden-xs" translate="47.BTN_SelecionarProduto">Selecionar Produto</span>
                                                </a>
                                            </div>
                                        </label>
                                    </span>--%>
                                </div>
                                <%--<hr/>--%>
                                <div class="row" style="padding-top: 15px;">
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.statuslote.$invalid && form.submitted}">
                                        <label for="status" translate="47.LBL_StatusLote" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_StatusLote' | translate}}">
                                            Status do Lote <span ng-show='!searchMode'>(*)</span>
                                        </label>
                                        <select class="form-control input-sm" id="Select2" name="statuslote" ng-model="item.statuslote" required 
                                            ng-disabled="item.statuslote" ng-readonly="item.statuslote"  ng-class="{'fieldDisabled': item.statuslote}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_StatusLote' | translate}}">
                                            <option ng-repeat="sl in statuslote" value="{{sl.codigo}}" ng-selected="item.statuslote == sl.codigo" >{{sl.nome}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group col-xs-12 " ng-class="{'has-error': form.globaltradeitemnumber.$invalid && form.submitted}">
                                        <label for="globaltradeitemnumber" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_GTIN' | translate}}"><span translate="47.LBL_GTIN">GTIN</span> <span ng-show="!searchMode">(*)</span></label><br />
                                        <input type="text" name="globaltradeitemnumber" ng-model="item.globaltradeitemnumber" integer maxlength="14" class="form-control input-sm fieldDisabled" required ng-disabled="true" ng-readonly="true" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_GTIN' | translate}}"/>
                                    </div>
                                    <div class="col-md-6 form-group col-xs-12 " ng-class="{'has-error': form.gln.$invalid && form.submitted}">
                                        <label for="gln" translate="47.LBL_GLN" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_GLN' | translate}}">GLN <span ng-show="!searchMode">(*)</span></label><br />
                                        <input type="text" name="gln" ng-model="item.gln" maxlength="14" class="form-control input-sm fieldDisabled" required ng-disabled="true" ng-readonly="true"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_GLN' | translate}}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group col-xs-12 " ng-class="{'has-error': form.nrlote.$invalid && form.submitted}">
                                        <label for="nrlote" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_NumeroLote' | translate}}">
                                            <span translate="47.LBL_NumeroLote">Número do Lote</span> <span ng-show="!searchMode">(*)</span>
                                        </label><br />
                                        <input type="text" name="nrlote" ng-model="item.nrlote" maxlength="20" class="form-control input-sm" required
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2"  ng-class="{'fieldDisabled': item.statuslote == 2}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_NumeroLote' | translate}}"/>
                                    </div>
                                    <div class="col-md-3 form-group col-xs-12 " ng-class="{'has-error': form.quantidadeitens.$invalid && form.submitted}">
                                        <label for="quantidadeitens" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.GRID_QuantidadeItens' | translate}}">
                                            <span translate="47.GRID_QuantidadeItens">Quantidade de Itens</span> <span ng-show="!searchMode">(*)</span>
                                        </label><br />
                                        <input type="text" name="quantidadeitens" ng-model="item.quantidadeitens" maxlength="8" class="form-control input-sm" required
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}" integer 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.GRID_QuantidadeItens' | translate}}"/>
                                    </div>
                                    <div class="col-md-3 form-inline" ng-class="{'has-error': form.dataprocessamento.$invalid && form.submitted}">
                                        <label for="dataprocessamento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataProcessamento' | translate}}">
                                            <span translate="47.LBL_DataProcessamento">Data de Processamento</span> <span ng-show="!searchMode">(*)</span>
                                        </label><br />
                                        <input type="text" id="processamento" name="dataprocessamento" ng-model="item.dataprocessamento" maxlength="10" class="form-control input-sm datepicker"
                                            sbx-datepicker data-date-format="dd/mm/yyyy" required 
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataProcessamento' | translate}}"/>

                                        <%--<input type="text" name="horaprocessamento" value=" " ng-model="item.horaprocessamento" ng-pattern="/^([0-2]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/" ui-time-mask="short" />--%>
                                        <input type="text"  ng-model="item.horaprocessamento" ng-blur="validaHora('horaprocessamento')" placeholder="HH:mm" min="00:00:00" max="23:59:59" ng-pattern="/^([0-2]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/" ui-time-mask="short" ng-disabled="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}"/>

                                    </div>
                                    <div class="col-md-3 form-inline col-xs-12 " ng-class="{'has-error': form.datavencimento.$invalid && form.submitted}">
                                        <label for="datavencimento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataVencimento' | translate}}">
                                            <span translate="47.LBL_DataVencimento">Data de Vencimento</span> <span ng-show="!searchMode">(*)</span>
                                        </label><br />
                                        <input type="text" name="datavencimento" id="vencimento" ng-model="item.datavencimento" maxlength="10" class="form-control input-sm datepicker"
                                            sbx-datepicker data-date-format="dd/mm/yyyy" required 
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataVencimento' | translate}}"/>
                                        <input type="text"  ng-model="item.horavencimento" ng-blur="validaHora('horavencimento')" placeholder="HH:mm" min="00:00:00" max="23:59:59" ng-pattern="/^([0-2]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/" ui-time-mask="short" ng-disabled="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}"/>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 15px;">
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.codigobizstep.$invalid && form.submitted}">
                                        <label for="codigobizstep" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_TipoProcessamento' | translate}}"><span translate="47.LBL_TipoProcessamento">Tipo de Processamento</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" name="codigobizstep" ng-model="item.codigobizstep" required ng-change="onSelectProcessamento(item.codigobizstep)" ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_TipoProcessamento' | translate}}">
                                            <option ng-repeat="tipoproc in bizsteps" value="{{tipoproc.codigo}}" ng-selected="item.codigobizstep == tipoproc.codigo">{{tipoproc.nome}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.codigodispositions.$invalid && form.submitted}">
                                        <label for="codigodispositions" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_StatusProduto' | translate}}"><span translate="47.LBL_StatusProduto">Status do Produto</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" name="codigodispositions" ng-model="item.codigodispositions" required
                                            ng-options="tipostatus.codigo as tipostatus.nome for tipostatus in dispositions" 
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_StatusProduto' | translate}}">
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.informacoesadicionais.$invalid && form.submitted}">
                                        <label for="productdescription" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_InformacoesAdicionais' | translate}}"><span translate="47.LBL_InformacoesAdicionais">Informações Adicionais</span> <%--<span ng-show='!searchMode'>(*)</span>--%></label>
                                        <textarea class="form-control input-sm" id="Textarea1" sbx-maxlength="500" name="informacoesadicionais" rows="3" ng-model="item.informacoesadicionais"
                                            style="resize: vertical;" ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_InformacoesAdicionais' | translate}}"></textarea>
                                        <span style="font-size: 80%;">(<span translate="47.LBL_TamanhoMaximo">Tamanho máximo:</span> 500)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span translate="47.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.informacoesadicionais.$viewValue.length || 0 }}</span>
                                    </div>
                                </div>

                                 <div class="row">                                   
                                    <div class="col-md-3 form-group" ng-class="{'has-error': form.dataprocessamento.$invalid && form.submitted}">
                                        <label for="datacadastro" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataInclusao' | translate}}">
                                            <span translate="47.LBL_DataInclusao">Data de Inclusão</span>                                       
                                        </label><br />
                                        <input type="text" id="txtdatacadastro" name="datacadastro" ng-model="item.datacadastro" maxlength="10" class="form-control input-sm"
                                             ng-readonly="true"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataInclusao' | translate}}"/>
                                    </div>
                                    <div class="col-md-3 form-group col-xs-12 " ng-class="{'has-error': form.datavencimento.$invalid && form.submitted}">
                                        <label for="dataalteracao" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataAlteracao' | translate}}">
                                            <span translate="47.LBL_DataAlteracao">Data de Alteração</span>
                                        </label><br />
                                        <input type="text" name="dataalteracao" id="txtdataalteracao" ng-model="item.dataalteracao" maxlength="10" class="form-control input-sm"
                                             ng-readonly="true"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DataAlteracao' | translate}}"/>
                                    </div>
                                </div>

                            </form>
                            <form name="form.midia" id="midia" novalidate class="form" role="form">
                                <h3 class="header smaller lighter purple" translate="47.LBL_IncluirURLCertificacao">
                                    Incluir URL - Certificação
                                </h3>
                                <div class="row">
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12 " ng-class="{'has-error': form.midia.nome.$invalid && form.midia.nome.submitted}">
                                        <label for="nome" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_NomeURL' | translate}}" translate="47.LBL_NomeURL">Nome </label>
                                        <input type="text" class="form-control input-sm" maxlength="255" id="Text10" name="nome" ng-model="itemUrl.nome" ng-required="isRequiredURL" autocomplete="off"
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_NomeURL' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12 " ng-class="{'has-error': form.midia.url.$invalid && form.midia.url.submitted}">
                                        <label for="nome" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_URL' | translate}}" translate="47.LBL_URL">URL </label>
                                        <input type="text" class="form-control input-sm" maxlength="255" id="Text6" name="url" ng-model="itemUrl.url" ng-required="isRequiredURL" autocomplete="off"
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_URL' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-3 col-sm-6 col-xs-12 " style="margin-top: 28px;">
                                        <button ng-cloak ng-show="insertURLMode" name="salvar" ng-click="event.preventDefault(); onSaveURL(itemUrl);" title="{{ '47.TOOLTIP_SalvarURL' | translate }}" class="btn btn-info btn-xs" type="button"
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}">
                                            <i class="icon-save"></i>
                                        </button>
                                        <button ng-cloak ng-show="!insertURLMode" ng-click="event.preventDefault(); atualizarURL(itemUrl);" name="atualizar" title="{{ '47.TOOLTIP_AtualizarURL' | translate }}" class="btn btn-info btn-xs" type="button"
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}">
                                            <i class="icon-refresh"></i>
                                        </button>
                                        <button ng-click="event.preventDefault(); cancelarURL();" name="atualizar" class="btn btn-danger btn-xs" type="button"
                                            ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}" title="{{ '47.TOOLTIP_CancelarURL' | translate }}">
                                            <i class="icon-remove"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table ng-table="tableURL" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                        <thead>
                                            <tr>
                                                <th ng-click="tableURL.sorting('codigo', tableURL.isSortBy('codigo', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span># &nbsp;&nbsp;
                                                  </span>
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableURL.isSortBy('codigo', 'asc'),
                                                    'icon-sort-down': tableURL.isSortBy('codigo', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableURL.sorting('nome', tableURL.isSortBy('nome', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="47.GRID_Nome">Nome &nbsp;&nbsp;
                                                  </span>
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableURL.isSortBy('nome', 'asc'),
                                                    'icon-sort-down': tableURL.isSortBy('nome', 'desc')
                                                  }"></i>
                                                </th>
                                                <th ng-click="tableURL.sorting('url', tableURL.isSortBy('url', 'asc') ? 'desc' : 'asc')" class="text-center">
                                                  <span translate="47.GRID_URL">Url &nbsp;&nbsp;
                                                  </span>
                                                  <i class="text-center icon-sort" ng-class="{
                                                    'icon-sort-up': tableURL.isSortBy('url', 'asc'),
                                                    'icon-sort-down': tableURL.isSortBy('url', 'desc')
                                                  }"></i>
                                                </th>
                                                <th><span translate="47.GRID_Acao">Ação</span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item1 in listURL" ng-click="onAlterURL(item1)" ng-show="item1.status != 'excluido' && item1.status != 'excluido2'" 
                                                ng-disabled="{{item.statuslote == 2}}" ng-class="{'fieldDisabled': item.statuslote == 2}">
                                                <td ng-style="{ 'width': '10%' }" sortable="codigo">{{item1.codigo}}</td>
                                                <td ng-style="{ 'width': '40%' }" sortable="nome">{{item1.nome}}</td>
                                                <td ng-style="{ 'width': '40%' }" sortable="url">{{item1.url}}</td>
                                                <td ng-style="{ 'width': '10%' }" class="action-buttons center">
                                                    <a class="red" href="" ng-class="{'disabled': item.statuslote == 2}" ng-click="$event.stopPropagation(); removeURL(item1);" 
                                                        ng-disabled="{{item.statuslote == 2}}" ng-class="{'fieldDisabled': item.statuslote == 2}">
                                                    <i class="icon-trash bigger-130" ng-class="{'btn-gray': item.statuslote == 2}" title="{{ '47.TOOLTIP_Excluir' | translate }}" style="cursor:pointer; text-decoration:none;"></i></a>&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr ng-show="listURL.length == 0">
                                                <td colspan="7" class="text-center" translate="47.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                            <div class="form-actions center">
                                <button ng-if="!searchMode" id="Button3" name="gerar"
                                    ng-click="$event.stopPropagation(); onPublish(item)" class="btn btn-warning btn-sm" type="button"
                                    ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}">
                                    <i class="icon-gear bigger-110"></i><span class="hidden-xs" translate="47.BTN_Publicar">Publicar</span>
                                </button>
                                <button class="btn btn-info btn-sm" ng-click="onSave(item)" ng-show="!isPesquisa"
                                    ng-disabled="item.statuslote == 2" ng-readonly="item.statuslote == 2" ng-class="{'fieldDisabled': item.statuslote == 2}">
                                    <i class="icon-save bigger-110"></i><span class="hidden-xs" translate="47.BTN_Salvar">Salvar</span>
                                </button>
                                <button id="Button1" name="cancel" ng-click="onCancel();" class="btn btn-danger btn-sm" type="button">
                                    <i class="icon-remove bigger-110"></i><span class="hidden-xs" translate="47.BTN_Cancelar">Cancelar</span>
                                </button>
                            </div>
                        </div>
                        <div id="Div1" class="tab-pane active" ng-show="isPesquisa">
                            <div class="row" style="padding-top: 15px;">
                                <div class="col-md-3 col-xs-12 form-inline" ng-class="{'has-error': form.dataprocessamento.$invalid && form.submitted}">
                                    <label for="dataprocessamento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_DataProcessamento' | translate}}" translate="47.LBL_DataProcessamento">Data de Processamento </label><br />
                                    <span translate="47.LBL_De">De</span><br> 
                                    <input type="text" id="Text1" name="datacertificado" ng-model="item.de_dataprocessamento" maxlength="10" class="form-control input-sm datepicker"
                                        sbx-datepicker data-date-format="dd/mm/yyyy"
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_De' | translate}}"/>

                                    <input type="text"  ng-model="item.de_horaprocessamento" ng-blur="validaHora('de_horaprocessamento')" placeholder="HH:mm" min="00:00:00" max="23:59:59" ng-pattern="/^([0-2]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/" ui-time-mask="short" />

                                </div>
                                <div class="col-md-3 col-xs-12 form-inline" ng-class="{'has-error': form.datavencimento.$invalid && form.submitted}" style="margin-top: 29px;">
                                    <span translate="47.LBL_Ate">Até</span><br> 
                                    <input type="text" name="datavencimento" id="Text2" ng-model="item.ate_dataprocessamento" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_Ate' | translate}}"/>

                                    <input type="text"  ng-model="item.ate_horaprocessamento" ng-blur="validaHora('ate_horaprocessamento')" placeholder="HH:mm" min="00:00:00" max="23:59:59" ng-pattern="/^([0-2]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/" ui-time-mask="short"/>

                                </div>

                                <div class="col-md-3 col-xs-12 form-inline" ng-class="{'has-error': form.dataprocessamento.$invalid && form.submitted}">
                                    <label for="datavencimento" data-toggle="popover" data-trigger="hover" data-content="{{'t.47.GRID_DataVencimento' | translate}}" translate="47.GRID_DataVencimento">Data de Vencimento </label><br />
                                    <span translate="47.LBL_De">De</span><br>
                                    <input type="text" id="Text7" name="datacertificado" ng-model="item.de_datavencimento" maxlength="10" class="form-control input-sm datepicker"
                                        sbx-datepicker data-date-format="dd/mm/yyyy" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_De' | translate}}"/>

                                    <input type="text"  ng-model="item.de_horavencimento" ng-blur="validaHora('de_horavencimento')" placeholder="HH:mm" min="00:00:00" max="23:59:59" ng-pattern="/^([0-2]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/" ui-time-mask="short"/>
                                </div>
                                <div class="col-md-3 col-xs-12 form-inline" ng-class="{'has-error': form.datavencimento.$invalid && form.submitted}" style="margin-top: 29px;">
                                    <span translate="47.LBL_Ate">Até</span><br /> 
                                    <input type="text" name="datavencimento" id="Text8" ng-model="item.ate_datavencimento" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy"
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_Ate' | translate}}"/>

                                    <input type="text"  ng-model="item.ate_horavencimento" ng-blur="validaHora('ate_horaprde_horavencimentoocessamento')" placeholder="HH:mm" min="00:00:00" max="23:59:59" ng-pattern="/^([0-2]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/" ui-time-mask="short"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12 form-group" ng-class="{'has-error': form.produto.$invalid && form.submitted}">
                                    <label for="produto" translate="47.LBL_DescricaoProdutoLote">Descrição do Produto </label><br />
                                    <input type="text" id="Text3" name="produto" ng-model="item.produto" class="form-control input-sm" 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_DescricaoProdutoLote' | translate}}"/>
                                </div>
                                <div class="col-md-6 col-xs-12  form-group" ng-class="{'has-error': form.lote.$invalid && form.submitted}">
                                    <label for="produto" translate="47.LBL_NumeroLote">Número do Lote </label><br />
                                    <input type="text" id="lote" name="lote" ng-model="item.lote" class="form-control input-sm" 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_NumeroLote' | translate}}"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.codigostatuslote.$invalid && form.submitted}">
                                    <label for="codigostatuslote" translate="47.LBL_StatusLote">
                                        Status do Lote <span ng-show='!searchMode'>(*)</span>
                                    </label>
                                    <select class="form-control input-sm" id="Select3" name="codigostatuslote" ng-model="item.codigostatuslote" 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_StatusLote' | translate}}">
                                        <option ng-repeat="sl in statuslote" value="{{sl.codigo}}" ng-selected="item.statuslote == sl.codigo" >{{sl.nome}}</option>
                                    </select>
                                </div>
                                <div class="col-md-6 col-xs-12 form-group" ng-class="{'has-error': form.gtin.$invalid && form.submitted}">
                                    <label for="gtin" translate="47.LBL_GTINLote">GTIN </label><br />
                                    <input type="text" name="gtin" ng-model="item.gtin" integer class="form-control input-sm" maxlength="14" 
                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.47.LBL_GTINLote' | translate}}"/>
                                </div>
                            </div>
                            <div class="form-actions center">
                                <button class="btn btn-info btn-sm" name="find" type="button" ng-click="onSearch(item)" ng-show="isPesquisa">
                                    <i class="icon-search bigger-110"></i><span translate="47.BTN_Pesquisar">Pesquisar</span>
                                </button>
                                <button id="Button2" name="fechar" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                    <i class="icon-remove bigger-110"></i><span translate="47.BTN_Fechar">Fechar</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label translate="47.LBL_TitleGestaoLotes">{{ titulo }}</label>
            </div>
            <div class="widget-box">
                <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                    <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '47.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
                    <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '47.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
                    <ajuda codigo="85" tooltip-ajuda="'47.TOOLTIP_Ajuda'"></ajuda>
                </div>

                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                        <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                    </a>
                    <div class="widget-main" style="min-height: 1250px;">
                        <div class="table-responsive">
                            <table ng-table="tableLotes" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <thead>
                                    <tr>
                                        <th ng-click="tableLotes.sorting('globaltradeitemnumber', tableLotes.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="47.GRID_GTIN">GTIN &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('globaltradeitemnumber', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('globaltradeitemnumber', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLotes.sorting('gln', tableLotes.isSortBy('gln', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="47.GRID_GLN">GLN &nbsp;&nbsp;
                                          </span>
                                            <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('gln', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('gln', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLotes.sorting('nrlote', tableLotes.isSortBy('nrlote', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="47.GRID_Lote">Lote &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('nrlote', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('nrlote', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLotes.sorting('dataprocessamento', tableLotes.isSortBy('dataprocessamento', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="47.GRID_DataProcessamento">Data de Processamento &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('nrlote', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('nrlote', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLotes.sorting('bizstep', tableLotes.isSortBy('bizstep', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="47.GRID_TipoProcessamento">Tipo de Processamento &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('bizstep', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('bizstep', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLotes.sorting('dispositions', tableLotes.isSortBy('dispositions', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="47.GRID_StatusProduto">Status do Produto &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('dispositions', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('dispositions', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableLotes.sorting('datavencimento', tableLotes.isSortBy('datavencimento', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="47.GRID_DataVencimento">Data de Vencimento &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                           'icon-sort-up': tableLotes.isSortBy('datavencimento', 'asc'),
                                           'icon-sort-down': tableLotes.isSortBy('datavencimento', 'desc')
                                         }"></i>
                                        </th>
                                        <th ng-click="tableLotes.sorting('statuslote', tableLotes.isSortBy('statuslote', 'asc') ? 'desc' : 'asc')" class="text-center">
                                          <span translate="47.GRID_StatusLote">Status do Lote &nbsp;&nbsp;
                                          </span>
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableLotes.isSortBy('statuslote', 'asc'),
                                            'icon-sort-down': tableLotes.isSortBy('statuslote', 'desc')
                                          }"></i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)">
                                        <td ng-style="{ 'width': '14%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                        <td ng-style="{ 'width': '10%' }" sortable="'gln'">{{item1.gln}}</td>
                                        <td ng-style="{ 'width': '14%' }" sortable="'nrlote'">{{item1.nrlote}}</td>
                                        <td ng-style="{ 'width': '14%' }" sortable="'dataprocessamento'">{{item1.dataprocessamento | date : 'dd/MM/yyyy'}}</td>                                        
                                        <td ng-style="{ 'width': '12%' }" sortable="'tipoprocessamento'">{{item1.bizstep}}</td>
                                        <td ng-style="{ 'width': '12%' }" sortable="'statusproduto'">{{item1.dispositions}}</td>
                                        <td ng-style="{ 'width': '14%' }" sortable="'datavencimento'">{{item1.datavencimento | date : 'dd/MM/yyyy'}}</td>                                        
                                        <td ng-style="{ 'width': '10%' }" sortable="'statuslote'">{{item1.statuslote | statuslote}}</td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="8" class="text-center" translate="47.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="47.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="47.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>

    <div>
        <script type="text/ng-template" id="modalProduto">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content" on-enter="onFilter(itemForm)">
                <div class="modal-header">
                    <button type="button" class="bootbox-close-button close" ng-click="cancel()">x</button>
                    <h3 class="modal-title"><span translate="47.LBL_SelecionarProduto">Selecionar Produto</span> <popup-ajuda codigo="50" tooltip-ajuda="'47.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form id="form" name="form" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue" translate="47.LBL_PreenchaAoMenosUmFiltro">Preencha ao menos um filtro abaixo:</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <span translate="47.LBL_Filtros">Filtros</span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="pesquisaGTIN" translate="47.LBL_GTIN">GTIN</label>
                                <input type="text" id="pesquisaGTIN" class="form-control input-sm" ng-model="itemForm.gtin" maxlength="14" integer 
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_GTIN' | translate}}"/>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="pesquisaProduto" translate="47.LBL_DescricaoProduto">Descrição do Produto</label>
                                <input id="pesquisaProduto" class="form-control input-sm" type="text" ng-model="itemForm.descricao" maxlength="255" 
                                    data-toggle="popover" data-trigger="hover" data-html="true" data-content="{{'t.47.LBL_DescricaoProduto' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="tipogtin" translate="47.LBL_TipoGTIN">Tipo de GTIN</label>
                                <select class="form-control input-sm" id="tipogtin" name="tipogtin" ng-model="itemForm.codigotipogtin" ng-options="licenca.codigo as licenca.nome for licenca in licencas"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.47.LBL_TipoGTIN' | translate}}">
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="statusgtin" translate="47.LBL_StatusGTIN">Status do GTIN</label>
                                <select class="form-control input-sm" id="statusgtin" name="statusgtin" ng-model="itemForm.codigostatusgtin"
                                data-toggle="popover" data-trigger="hover" data-html="true" data-content="{{'t.47.LBL_StatusGTIN' | translate}}"> <!--ng-options="status.codigo as status.nome for status in statusgtin"-->
                                    <option value=""> Selecione</option>
                                    <option ng-repeat="status in statusgtin" value="{{status.codigo}}" ng-if="status.codigo == 1 || status.codigo == 3 || status.codigo == 6">{{status.nome}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="center">
                            <button name="find" class="btn btn-info btn-sm" type="button" ng-click="onFilter(itemForm)">
                                <i class="icon-ok bigger-110"></i><span translate="47.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <div class="table-responsive" style="margin-bottom: 20px;">
                                <table ng-table="tableProdutos" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableProdutos.sorting('productdescription', tableProdutos.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')" class="text-center">
                                              <span translate="47.GRID_Produto">Produto &nbsp;&nbsp;
                                              </span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('productdescription', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('productdescription', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableProdutos.sorting('globaltradeitemnumber', tableProdutos.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')" class="text-center">
                                              <span translate="47.GRID_GTIN">GTIN &nbsp;&nbsp;
                                              </span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('globaltradeitemnumber', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('globaltradeitemnumber', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableProdutos.sorting('nometipogtin', tableProdutos.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')" class="text-center">
                                              <span translate="47.GRID_TipoGTIN">Tipo de GTIN &nbsp;&nbsp;
                                              </span>
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableProdutos.isSortBy('nometipogtin', 'asc'),
                                                'icon-sort-down': tableProdutos.isSortBy('nometipogtin', 'desc')
                                              }"></i>
                                            </th>
                                            <th><span translate="47.GRID_Selecionar">Selecionar</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data">
                                            <td ng-style="{ 'width': '35%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                            <td ng-style="{ 'width': '30%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                            <td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Selecionar'">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedProduto(item1)">
                                                    <i class="icon-ok bigger-110"></i><span translate="47.BTN_Selecionar">Selecionar</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="4" class="text-center" translate="47.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" translate="47.BTN_Cancelar">Cancelar</button>
                </div>
            </div>
        </script>
    </div>
</div>
