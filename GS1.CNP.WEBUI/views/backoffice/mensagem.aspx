﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mensagem.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.mensagem" %>

<div ng-controller="MensagemCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = ''; itemFormEspec = { statusregistro: 'novo' }; gs1_128 = false;">
    <%--<div ng-cloak ng-show="flagAssociadoSelecionado">--%>

        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='formMode = !formMode'></i>
                <label ng-bind="titulo" translate="59.LBL_Mensagens"></label>
            </div>
            <div class="pull-right action-buttons-header" style="margin-right: 0px;">              
                <ajuda codigo="25" tooltip-ajuda="'59.TOOLTIP_Ajuda'"></ajuda>
            </div>
            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                     <div class="widget-body" style="height: 655px;" ng-class="{'exibeVoltar': exibeVoltar}">
                        <div class="widget-main no-padding">
                            <div class="row" ng-show="!searchMode">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group col-md-12" style="font-size: 90%;">&nbsp;</span></div>
                                </div>
                            </div>  
                                                      
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 col-xs-12">
                                        <i class="icon-time" ng-show="itemForm.datasincronizacao != null"></i>
                                        <label for="nome">{{itemForm.datasincronizacao}}</label>                                       
                                    </div>
                                </div>                                
                            </div>

                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group col-md-12 col-xs-12">                                        
                                        <label for="nome">{{itemForm.assunto}}</label>                                       
                                    </div>
                                </div>                                
                            </div>

                            <div class="hr hr-double" style="width:98%"></div>

                             <div class="row" style="height: 382px;overflow-Y:scroll;width:100%">                                
                                <div class="col-md-10">                                    
                                    <div class="form-group col-md-12 col-xs-12" >                                        
                                       <p ng-bind-html="itemForm.mensagem"></p>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions center" ng-show="formMode" style="width:98%">                               
                                <button id="cancel" name="cancel" ng-click="onCancel(itemForm)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                    <i class="icon-remove bigger-110"></i><span translate="10.BTN_Cancelar">Cancelar</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        

        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": formMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='formMode = !formMode'></i>
                <label ng-bind="titulo" translate="59.LBL_Mensagens"></label>
            </div>
            <div class="pull-right action-buttons-header" style="margin-right: 0px;">              
                <ajuda codigo="25" tooltip-ajuda="'59.TOOLTIP_Ajuda'"></ajuda>
            </div>
            <div class="widget-box">
                <div class="content-module-bar widget-box bg-sky"></div>
                    <div class="widget-body" style="height: 655px;" ng-class="{'exibeVoltar': exibeVoltar}">
                       <a href="" class="btn btn-sm btn-inverse" ng-click="formMode = !formMode;" ng-show="exibeVoltar">
                            <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && formMode"></i>
                            <i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !formMode"></i>
                        </a>

                        <div class="widget-main" style="min-height: 300px; width: 100%">
                           
                            <div class="table-responsive" style="height: 560px; overflow-y: auto; ">
                                <table ng-table="tableMensagens" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableMensagens.sorting('codigo', tableMensagens.isSortBy('codigo', 'asc') ? 'desc' : 'asc')" class="text-center">
                                              <span>#</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableMensagens.isSortBy('codigo', 'asc'),
                                                'icon-sort-down': tableMensagens.isSortBy('codigo', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableMensagens.sorting('datasincronizacao', tableMensagens.isSortBy('datasincronizacao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                              <span translate="59.GRID_Data">Data da Mensagem</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableMensagens.isSortBy('datasincronizacao', 'asc'),
                                                'icon-sort-down': tableMensagens.isSortBy('datasincronizacao', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableMensagens.sorting('assunto', tableMensagens.isSortBy('assunto', 'asc') ? 'desc' : 'asc')" class="text-center">
                                              <span translate="59.GRID_Assunto">Assunto</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableMensagens.isSortBy('assunto', 'asc'),
                                                'icon-sort-down': tableMensagens.isSortBy('assunto', 'desc')
                                              }"></i>
                                            </th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="" ng-class="{bolder : item1.dataleitura == ''}">
                                            <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                            <td ng-style="{ 'width': '10%' }" sortable="'datasincronizacao'">{{item1.datasincronizacao}}</td>
                                            <td ng-style="{ 'width': '80%' }" sortable="'assunto'">{{item1.assunto}}</td>                  
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="7" class="text-center" translate="10.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>

    <%--</div>--%>
</div>


<%--<div ng-cloak ng-show="!flagAssociadoSelecionado">
    <h1 translate="17.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
    <h4 translate="17.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
</div>--%>