﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="meusDados.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.meusDados" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />
<script src="../../scripts/lib/jstree.min.js" type="text/javascript"></script>

<style type="text/css">
    
    .form-actions{
        margin-bottom: 0px;
        margin-top: 0px;
    }
    
    .tab-content{
        padding-top: 10px;    
    }
    
    .table-responsive{
        overflow: auto;
        height: 150px;    
    }
    
    tfoot{
        display: none;    
    }
    
</style>

<div ng-controller="MeusDados" class="page-container" ng-init="entity = 'usuario'; " style="min-height: 820px;">
    <div class="" form-container> <%--form-container--%>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" translate="6.LBL_MeusDados"></label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <ajuda codigo="15" tooltip-ajuda="'6.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div class="row">
                            <div class="form-group col-md-12" style="font-size: 90%;">
                                (*) - <span translate="6.LBL_CamposObrigatorios">Campos obrigatórios</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" ng-class="{'has-error': form.nomeusuario.$invalid && form.submitted}">
                                <label for="nomeusuario"><span translate="6.LBL_Nome">Nome</span> (*)</label>
                                <input type="text" class="form-control input-sm" maxlength="255" id="nomeusuario" name="nomeusuario" ng-model="usuario.nomeusuario" required
                                data-toggle="popover" data-trigger="hover" data-container="body" data-placement="bottom" data-content="{{'t.6.LBL_Nome' | translate}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="perfilusuario"><span translate="6.LBL_PerfilAcesso">Perfil de Acesso</span> (*)</label>   
                                <input type="text" class="form-control input-sm" maxlength="50" id="perfilusuario" name="perfilusuario" ng-model="usuario.perfilusuario" disabled="disabled" required
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.6.LBL_PerfilAcesso' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="statususuario"><span translate="6.LBL_StatusUsuario">Status Usuário</span> (*)</label>
                                <input type="text" class="form-control input-sm" maxlength="50" id="statususuario" name="statususuario" ng-model="usuario.statususuario" disabled="disabled" required
                                data-toggle="popover" data-trigger="hover" data-container="body" data-placement="left" data-content="{{'t.6.LBL_StatusUsuario' | translate}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" ng-class="{'has-error': form.cpfcnpjusuario.$invalid && form.submitted}">
                                <label for="cpfcnpjusuario"><span translate="6.LBL_CPFCNPJ">CPF/CNPJ</span> (*)</label>
                                <input type="text" class="form-control input-sm" maxlength="50" id="cpfcnpjusuario" name="cpfcnpjusuario" ng-model="usuario.cpfcnpjusuario" ui-br-cpfcnpj-mask required
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.6.LBL_CPFCNPJ' | translate}}"/><%-- ng-pattern="/(^d{3}.d{3}.d{3}-d{2}$)|(^d{2}.d{3}.d{3}/d{4}-d{2}$)/"--%>
                            </div>
                            <div class="form-group col-md-6" ng-class="{'has-error': form.email.$invalid && form.submitted}">
                                <label for="email"><span translate="6.LBL_Email">Email</span> (*)</label>
                                <input type="text" class="form-control input-sm" maxlength="255" id="email" name="email" ng-pattern="/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/" ng-model="usuario.email" required
                                data-toggle="popover" data-trigger="hover" data-container="body" data-placement="left" data-content="{{'t.6.LBL_Email' | translate}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" ng-class="{'has-error': form.telefonecelular.$invalid && form.submitted}">
                                <label for="telefonecelular"><span translate="6.LBL_TelefoneCelular">Tel. Celular</span> (*)</label>
                                <input type="text" class="form-control input-sm" maxlength="50" id="telefonecelular" name="telefonecelular" ng-model="usuario.telefonecelular" ui-mask="(99)99999999?9" required
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.6.LBL_TelefoneCelular' | translate}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" ng-class="{'has-error': form.telefonecomercial.$invalid && form.submitted}">
                                <label for="telefonecomercial"><span translate="6.LBL_TelefoneComercial">Tel. Comercial</span> (*)</label>
                                <input type="text" class="form-control input-sm" maxlength="50" id="telefonecomercial" name="telefonecomercial" ng-model="usuario.telefonecomercial" ui-mask="(99)99999999" required
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.6.LBL_TelefoneComercial' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ramal" translate="6.LBL_Ramal">Ramal</label>
                                <input type="text" class="form-control input-sm" maxlength="50" id="ramal" name="ramal" ng-model="usuario.ramal"  
                                data-toggle="popover" data-trigger="hover" data-container="body" data-placement="left" data-content="{{'t.6.LBL_Ramal' | translate}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" ng-class="{'has-error': form.departamento.$invalid && form.submitted}">
                                <label for="departamento"><span translate="6.LBL_Departamento">Departamento</span> (*)</label>
                                <input type="text" class="form-control input-sm" maxlength="255" id="departamento" name="departamento" ng-model="usuario.departamento" required 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.6.LBL_Departamento' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6" ng-class="{'has-error': form.cargo.$invalid && form.submitted}">
                                <label for="cargo"><span translate="6.LBL_Cargo">Cargo</span> (*)</label>
                                <input type="text" class="form-control input-sm" maxlength="255" id="cargo" name="cargo" ng-model="usuario.cargo" required 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-placement="left" data-content="{{'t.6.LBL_Cargo' | translate}}"/>
                            </div>
                        </div>
                        <div class="row" ng-show="usuarioLogado.id_tipousuario != 1">
                            <div class="form-group col-md-12">
                                <label translate="6.LBL_EmpresasAssociadas">Empresa(s) Associada(s)</label>
                                <div class="table-responsive">
                                    <table ng-table="tableEmpresasAssociadas" class="table table-bordered table-striped table-hover" >
                                        <thead>
                                            <tr>
                                                <th><span translate="6.GRID_RazaoSocial">Razão Social</span></th>
                                                <th><span translate="6.GRID_CNPJ">CNPJ</span></th>                                 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item in $data">
                                                <td ng-style="{ 'width': '50%' }" data-title="'Razão Social'" sortable="'nome'">{{item.nome}}</td>
                                                <td ng-style="{ 'width': '50%' }" data-title="'CNPJ'" sortable="'cpfcnpj'">{{item.cpfcnpj}}</td>
                                            </tr>
                                            <tr ng-show="$data.length == 0">
                                                <td colspan="2" class="text-center" translate="6.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <button class="btn btn-info btn-sm" ng-click="onSave()">
                                <i class="icon-save bigger-110"></i><span translate="6.BTN_Salvar">Salvar</span>
                            </button>
                            <button id="cancel" name="cancel" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                                <i class="icon-remove bigger-110"></i><span translate="6.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
