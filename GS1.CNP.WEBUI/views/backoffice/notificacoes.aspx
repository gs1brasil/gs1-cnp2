<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="notificacoes.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.notificacoes" %>

<style type="text/css">
    .new-notification{
        font-weight: bold;
        color: #6a9cba;
    }
</style>

<div ng-controller="NotificacoesCtrl" class="page-container" ng-init="visualizeMode = false; searchMode = false;" style="height: 820px;">
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo"></label>
        </div>
        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" ng-show="searchMode" title="Limpar" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
        </div>
        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="notificacoes" class="tab-pane active">
                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                <div class="message-content" style="border: none;">
                                    <div class="message-header clearfix">
                                        <div class="pull-left">
                                            <span class="blue bigger-125 new-notification">{{item.assunto}}</span>
                                            <div class="space-4"></div>
                                            <span class="blue">De: {{item.remetente}}</span>&nbsp;&nbsp;
                                            <i class="icon-time bigger-110 orange middle"></i>
                                            <span class="time blue">{{item.vigenciaInicio}}</span>   
                                        </div>
                                        <div class="action-buttons pull-right">
											<a href="#" ng-click="onBack();"><i class="icon-reply blue icon-only bigger-130"></i></a>
										</div>
                                    </div>  
                                    <div class="hr hr-double"></div>  
                                    <div class="message-body">
                                        {{item.mensagem}}    
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </form>
            </div>
        </div>
    </div> 
    
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": visualizeMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='visualizeMode = !visualizeMode'></i>
            <label ng-bind="titulo"></label>
        </div>
        <div class="pull-right action-buttons-header">
            <a class="btn btn-success btn-xs" title="Pesquisar" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <a class="btn btn-warning btn-xs" title="Ajuda" href="/views/backoffice/ajuda/notificacoes.html" style="margin-right:10px" target="_blank"><i class="icon-question-sign" title="Ajuda" ></i></a>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <div class="widget-main" style="min-height: 300px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;">
                            (*)Clique em um registro para visualizar a mensagem completa.
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableNotificacoes" class="table table-bordered table-striped table-hover">
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onVisualize(item1)" scroll-to="" ng-class="{'new-notification': item1.status == 1}" style="cursor:pointer;">
                                    <td ng-style="{ 'width': '30%' }" data-title="'Assunto'" sortable="'assunto'">{{item1.assunto}}</td>
                                    <td ng-style="{ 'width': '60%' }" data-title="'Mensagem'" sortable="'mensagem'">{{item1.mensagem}}</td>
                                    <td ng-style="{ 'width': '10%' }" data-title="'Data'" sortable="'vigenciaInicio'">{{item1.vigenciaInicio}}</td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="3" class="text-center">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>