﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="papelGln.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.papelGln" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="PapelCtrl" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; exibeMensagem = false; entity = 'papel'; itemFormEspec = { statusregistro: 'novo' };" style="height: 820px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" translate="4.LBL_CadastroPapelGLN"></label>
        </div>

        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" ng-click="onClean(item)" title="{{ '4.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="9" ng-show="searchMode" tooltip-ajuda="'4.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="10" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'4.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="11" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'4.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>

        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="papel" class="tab-pane active">
                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                <div class="row" ng-show="!searchMode">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        (*) - <span translate="4.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                        <label for="nome"><span translate="4.LBL_Nome">Nome</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="255" id="nome" name="nome" ng-model="item.nome" required="required" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.4.LBL_Nome' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                        <label for="status"><span translate="4.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="item.status" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.4.LBL_Status' | translate}}">
                                            <option value="1" ng-selected="item.status == 1">Ativo</option>
                                            <option value="0" ng-selected="item.status == 0">Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                        <label for="descricao"><span translate="4.LBL_Descricao">Descrição</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <textarea class="form-control input-sm" id="descricao" sbx-maxlength="400" name="descricao" rows="3" ng-model="item.descricao" style="resize: vertical;" required
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.4.LBL_Descricao' | translate}}"></textarea>
                                        <span style="font-size: 80%;">(<span translate="4.LBL_TamanhoMaximo">Tamanho máximo</span>: 400)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span translate="4.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.descricao.$viewValue.length || 0 }}</span>
                                    </div>
                                </div>
                                <div class="form-actions center" ng-show="editMode">
                                    <button class="btn btn-info btn-sm" name="find" scroll-to="" ng-cloak ng-show="isPesquisa" ng-click="onSearch(item)" type="button">
                                        <i class="icon-search bigger-110"></i><span translate="4.BTN_Pesquisar">Pesquisar</span>
                                    </button>
                                    <button class="btn btn-info btn-sm" scroll-to="" ng-cloak ng-show="!isPesquisa" ng-click="onSave(item)" type="button">
                                        <i class="icon-save bigger-110"></i><span translate="4.BTN_Salvar">Salvar</span>
                                    </button>
                                    <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="4.BTN_Cancelar">Cancelar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" translate="4.LBL_CadastroPapelGLN"></label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '4.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
            <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '4.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="8" tooltip-ajuda="'4.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 600px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="4.LBL_CliqueRegistroEditar">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tablePapel" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
                                <tr>
                                    <th ng-click="tablePapel.sorting('codigo', tablePapel.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                      <span>#</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePapel.isSortBy('codigo', 'asc'),
                                        'icon-sort-down': tablePapel.isSortBy('codigo', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tablePapel.sorting('nome', tablePapel.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                      <span translate="4.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePapel.isSortBy('nome', 'asc'),
                                        'icon-sort-down': tablePapel.isSortBy('nome', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tablePapel.sorting('status', tablePapel.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                      <span translate="4.GRID_Status">Status</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePapel.isSortBy('status', 'asc'),
                                        'icon-sort-down': tablePapel.isSortBy('status', 'desc')
                                      }"></i>
                                    </th>
                                    <th>
                                      <span translate="4.GRID_Acao">Ação</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '55%' }" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '35%'}" sortable="'status'">{{item1.status | status}}</td>
                                    <td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Ação'">
                                        <a class="red" href="" ng-click="onDelete(item1); $event.stopPropagation();" title="{{ '4.TOOLTIP_Excluir' | translate }}"><i class="icon-trash bigger-130"></i></a>
                                    </td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="7" class="text-center" translate="4.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
