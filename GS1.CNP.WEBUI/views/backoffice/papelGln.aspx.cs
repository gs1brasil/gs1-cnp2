﻿using GS1.CNP.WEBUI.Core;

namespace GS1.Trade.WEBUI.views.backoffice
{
    public partial class papelGln : BaseWeb
    {
        public override string PermissaoPagina { get { return "PesquisarPapelGLN"; } }
    }
}