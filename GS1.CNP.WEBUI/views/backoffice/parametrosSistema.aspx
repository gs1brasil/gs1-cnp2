﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="parametrosSistema.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.parametrosSistema" %>

<script type="text/javascript">
    
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/parametros";
    }

</script>
<style type="text/css">
    .form-group{ margin-bottom: 10px;}
    .form-actions{ margin-bottom: 0px;}
</style>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="ParametroSistema" class="page-container" ng-init="entity = 'parametrosSistema';" style="height: 800px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label>{{ titulo }}</label>
        </div>

        <div class="widget-toolbar my-tab" style="margin-top: -36px;">
            <ul id="tabs-parametros" class="nav nav-tabs">
                <li id="liTabGeral" class="active">
                    <a data-toogle="tab" href="#geral" translate="2.ABA_Geral">Geral (Layout)</a>
                </li>
                <li id="liTabProdutos">
                    <a data-toogle="tab" href="#produtos" ng-clock translate="2.ABA_Produto">Produtos</a>
                </li>
                <li id="liTabSeguranca">
                    <a data-toogle="tab" href="#seguranca" ng-clock translate="2.ABA_Seguranca">Segurança</a>
                </li>
                <li id="liTabCertificacao">
                    <a data-toogle="tab" href="#certificacao" ng-clock translate="2.ABA_Certificacao">Certificação</a>
                </li>
                <li id="liTabSistema">
                    <a data-toogle="tab" href="#sistema" ng-clock translate="2.ABA_Sistema">Sistema</a>
                </li>
            </ul>
        </div>

        <div class="pull-right" style="margin-top: -32px; margin-right: 500px;">
            <ajuda codigo="3" tooltip-ajuda="'2.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="widget-box">
            <%--<div class="widget-header">
                <h4><span>Cadastro</span></h4>
                <div class="widget-toolbar" ng-if="searchMode">
                    <a href=""><i class="icon-eraser" title="Limpar" ng-click="onClean(item)"></i></a>
                    <a href="/views/backoffice/ajuda/produtoPesquisa.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
                <div class="widget-toolbar" ng-if="!searchMode">
                    <a href="/views/backoffice/ajuda/produtoInclusao.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
                <div class="widget-toolbar  my-tab">
                    <ul id="tabs-parametros" class="nav nav-tabs">
                        <li id="liTabGeral" class="active">
                            <a data-toogle="tab" href="#geral">Geral (Layout)</a>
                        </li>
                        <li id="liTabProdutos">
                            <a data-toogle="tab" href="#produtos" ng-clock>Produtos</a>
                        </li>
                        <li id="liTabSeguranca">
                            <a data-toogle="tab" href="#seguranca" ng-clock>Segurança</a>
                        </li>
                        <li id="liTabSistema">
                            <a data-toogle="tab" href="#sistema" ng-clock >Sistema</a>
                        </li>
                    </ul>
                </div>
            </div>--%>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" id="form" novalidate class="form" role="form">
                        <div class="tab-content"><%--style="height: 550px;"--%>
                            <div id="geral" class="tab-pane active">
                                <div style="min-height:553px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <h3 class="header smaller lighter purple" translate="2.ABA_Sistema">
                                        01 - <span translate="2.LBL_GRID">GRID</span>
                                    </h3>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.paginacao.$invalid && form.submitted}">
                                            <div class="form-group col-md-8">
                                                <label translate="2.LBL_QuantidadePorPagina">Quantidade de resultados por página (Paginação)</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control input-sm" maxlength="3" id="paginacao" name="paginacao" ng-model="itemParam.resultadosporpagina" required integer 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_QuantidadePorPagina' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                  <%--  <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4">
                                            <label>Pesquisar colunas dinamicamente</label>
                                        </div>
                                        <div class="form-group col-md-8" ng-class="{'has-error': form.form-field-checkbox.$invalid && form.submitted}">
                                            <input type="checkbox" class="ace" name="form-field-checkbox" ng-checked="parametros.geral.grid.pesquisarColunasDinamicamente == 1" ng-model="parametros.geral.grid.pesquisarColunasDinamicamente">
                                            <span class="lbl"> Permitir</span>
                                        </div>
                                    </div>
                                    <h3 class="header smaller lighter purple">
                                        02 - Classificação (Ordenação)
                                    </h3>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.ordenacaoPrimaria.$invalid && form.submitted}">
                                            <label>Ordenação Primária</label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <select class="form-control input-sm" id="ordenacaoPrimaria" name="ordenacaoPrimaria" ng-model="parametros.geral.ordenacao.primaria">
                                                <option value="{{parametros.geral.ordenacao.primaria}}" ng-selected="{{parametros.geral.ordenacao.primaria}}">{{parametros.geral.ordenacao.primaria}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.ordenacaoSecundaria.$invalid && form.submitted}">
                                            <label>Ordenação Secundária</label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <select class="form-control input-sm" id="ordenacaoSecundaria" name="ordenacaoSecundaria" ng-model="parametros.geral.ordenacao.secundaria">
                                                <option value="{{parametros.geral.ordenacao.secundaria}}">{{parametros.geral.ordenacao.secundaria}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.ordenacaoTerciaria.$invalid && form.submitted}">
                                            <label>Ordenação Terciária</label>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <select class="form-control input-sm" id="ordenacaoTerciaria" name="ordenacaoTerciaria" ng-model="parametros.geral.ordenacao.terciaria">
                                                <option value="{{parametros.geral.ordenacao.terciaria}}">{{parametros.geral.ordenacao.terciaria}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <h3 class="header smaller lighter purple">
                                        03 - Mensagens
                                    </h3>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-12" ng-class="{'has-error': form.mensagemDefault.$invalid && form.submitted}">
                                            <label>Mensagem default da página inicial (quando o sistema não conseguir resgatar mensagem do CRM)</label>
                                            <textarea class="form-control input-sm" rows="3" id="mensagemDefault" name="mensagemDefault" sbx-maxlength="500" ng-model="parametros.geral.mensagens.padrao" style="width: 100%;resize: vertical;"/>
                                            <span style="font-size: 80%;">(Tamanho máximo: 400)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Quantidade
                                            de caracteres: {{ form.mensagemDefault.$viewValue.length || 0 }}</span>
                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                            <div id="produtos" class="tab-pane">
                                <div style="min-height:553px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <h3 class="header smaller lighter purple">
                                        01 - <span translate="2.LBL_Fotos">Fotos</span>
                                    </h3>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.nufoto.$invalid && form.submitted}">
                                            <div class="form-group col-md-8">
                                                <label translate="2.LBL_QuantidadeMaximaFotos">Quantidade máxima de fotos</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control input-sm" maxlength="2" name="nufoto" ng-model="itemParam.nufoto" required integer 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_QuantidadeMaximaFotos' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="header smaller lighter purple">
                                        02 - <span translate="2.LBL_Links">Links</span>
                                    </h3>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.nuurlfoto.$invalid && form.submitted}">
                                            <div class="form-group col-md-8">
                                                <label translate="2.LBL_QuantidadeLinksProduto">Quantidade máxima de links para foto de produto</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control input-sm" maxlength="2" name="nuurlfoto" ng-model="itemParam.nuurlfoto" required integer 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_QuantidadeLinksProduto' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.nuurlyoutube.$invalid && form.submitted}">
                                            <div class="form-group col-md-8">
                                                <label translate="2.LBL_QuantidadeLinksYoutube">Quantidade máxima de links do Youtube</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control input-sm" maxlength="2" name="nuurlyoutube" ng-model="itemParam.nuurlyoutube" required integer 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_QuantidadeLinksYoutube' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.nuurllinkeddata.$invalid && form.submitted}">
                                            <div class="form-group col-md-8">
                                                <label translate="2.LBL_QuantidadeLinksLinkeddata">Quantidade máxima de links linkeddata</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control input-sm" maxlength="2" name="nuurllinkeddata" ng-model="itemParam.nuurllinkeddata" required integer 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_QuantidadeLinksLinkeddata' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.nuurlreserva.$invalid && form.submitted}">
                                            <div class="form-group col-md-8">
                                                <label translate="2.LBL_QuantidadeLinksReserva">Quantidade máxima de links reserva</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control input-sm" maxlength="2" name="nuurlreserva" ng-model="itemParam.nuurlreserva" required integer 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_QuantidadeLinksReserva' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-6" ng-class="{'has-error': form.nuurlproduto.$invalid && form.submitted}">
                                            <div class="form-group col-md-8">
                                                <label translate="2.LBL_QuantidadeLinksSite">Quantidade máxima de links de site do produto</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control input-sm" maxlength="2" name="nuurlproduto" ng-model="itemParam.nuurlproduto" required integer 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_QuantidadeLinksSite' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="certificacao" class="tab-pane">
                                <div style="min-height:553px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <h3 class="header smaller lighter purple">
                                        01 - <span translate="2.LBL_CodigoBarras">Código de Barras</span>
                                    </h3>
                                    <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.validadecertificacaocodigobarras.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_ValidadeCodigoBarras">Validade de Certificação de Código de Barras (meses)</span>
                                            <input class="col-md-5 col-xs-12" type="text" maxlength="2" ng-model="itemParam.validadecertificacaocodigobarras" name="validadecertificacaocodigobarras" 
                                                integer required data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_ValidadeCodigoBarras' | translate}}"></input>
                                        </div>
                                    </div>
                                    <h3 class="header smaller lighter purple">
                                        02 - <span translate="2.LBL_PesosMedidas">Pesos e Medidas</span>
                                    </h3>
                                    <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.validadecertificacaopesosmedidas.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_PesosMedidas">Validade de Certificação de Pesos e Medidas (meses)</span>
                                            <input class="col-md-5 col-xs-12" type="text" maxlength="2" ng-model="itemParam.validadecertificacaopesosmedidas" name="validadecertificacaopesosmedidas" integer required
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_PesosMedidas' | translate}}"></input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="seguranca" class="tab-pane">
                                <div style="min-height:553px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <h3 class="header smaller lighter purple">
                                        01 - <span translate="2.LBL_Senha">Senha</span>
                                    </h3>
                                    <%--<div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Repetição de senha</label>
                                        </div>
                                        <div class="form-group form-inline col-md-8">
                                            <input type="text" class="form-control input-sm" maxlength="3" id="repetirSenha" name="repetirSenha" ng-model="parametros.seguranca.senha.naoRepetirSenha" 
                                                integer required data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_Senha' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Tamanho mínimo da senha</label>
                                        </div>
                                        <div class="form-group form-inline col-md-8">
                                            <input type="text" class="form-control input-sm" maxlength="3" id="tamanhoMinimo" name="tamanhoMinimo" ng-model="parametros.seguranca.senha.tamanhoMinimo" 
                                                integer required data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.lbl_tamanhoMinimo' | translate}}"/>
                                            <span class="lbl"> Caracteres</span>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Armazenamento de senha</label>
                                        </div>
                                        <div class="form-group form-inline col-md-8">
                                            <input type="text" class="form-control input-sm" maxlength="3" id="armazenarUltimas" name="armazenarUltimas" ng-model="parametros.seguranca.senha.armazenarUltimas" 
                                                integer required data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.lbl_armazenamento' | translate}}"/>
                                            <span class="lbl"> Senhas salvas</span>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Expiração de senha</label>
                                        </div>
                                        <div class="form-group form-inline col-md-8">
                                            <input type="text" class="form-control input-sm" maxlength="3" id="mesesParaExpirar" name="mesesParaExpirar" ng-model="parametros.seguranca.senha.expirarSenha" 
                                                integer required data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.lbl_expiracaoSenha' | translate}}"/>
                                            <span class="lbl"> Meses</span>
                                        </div>
                                    </div>--%>
                                    <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.senhadiferenteultimas.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_NumeroSenhasRepetidas">Número de senhas que não podem ser repetidas</span>
                                            <input class="col-md-5 col-xs-12" type="text" maxlength="2" ng-model="itemParam.senhadiferenteultimas" name="senhadiferenteultimas" 
                                                integer required data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_NumeroSenhasRepetidas' | translate}}"></input>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.diasavisoexpiracao.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_NumeroDiasAvisoExpiracao">Número de dias para aviso de expiração de senha</span>
                                            <input class="col-md-5 col-xs-12" type="text" maxlength="2" ng-model="itemParam.diasavisoexpiracao" name="diasavisoexpiracao" integer required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_NumeroDiasAvisoExpiracao' | translate}}"></input>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.diasvalidadesenha.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_NumeroDiasValidadeSenha">Número de dias de validade de senha</span>
                                            <input class="col-md-5 col-xs-12" type="text" maxlength="2" ng-model="itemParam.diasvalidadesenha" name="diasvalidadesenha" integer required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_NumeroDiasValidadeSenha' | translate}}"></input>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.tentativassenha.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_NumeroTentativasSenha">Número de tentativas de senha</span>
                                            <input class="col-md-5 col-xs-12" type="text" maxlength="2" ng-model="itemParam.tentativassenha" name="tentativassenha" integer required
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_NumeroTentativasSenha' | translate}}"></input>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.tentativassenhasemrecaptcha.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_NumeroTentativasSenhaSemRecaptcha">Número de tentativas de senha sem RECAPTCHA</span>
                                            <input class="col-md-5 col-xs-12" type="text" maxlength="2" ng-model="itemParam.tentativassenhasemrecaptcha" name="tentativassenhasemrecaptcha" integer required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_NumeroTentativasSenhaSemRecaptcha' | translate}}"></input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="sistema" class="tab-pane">
                                <div style="max-height:553px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <h3 class="header smaller lighter purple">
                                        01 - <span translate="2.LBL_Integracoes">Integrações</span>
                                    </h3>
                                    <%--<div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Caminho do WebServices portal</label>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control input-sm" maxlength="250" id="wsUsuario" name="wsUsuario" ng-model="parametros.sistema.integracoes.wsPortalUsuario" />
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Sincronizar dados do CRM / RGC?</label>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <input type="radio" name="sincronizarCrmRgc" class="ace" value="parametros.sistema.integracoes.sincronizarCrmRgc" ng-model="parametros.sistema.integracoes.sincronizarCrmRgc" ng-checked="parametros.sistema.integracoes.sincronizarCrmRgc == 1">
                                            <span class="lbl"> Sim &nbsp; &nbsp;</span>
                                            <input type="radio" name="sincronizarCrmRgc" class="ace" value="parametros.sistema.integracoes.sincronizarCrmRgc" ng-model="parametros.sistema.integracoes.sincronizarCrmRgc" ng-checked="parametros.sistema.integracoes.sincronizarCrmRgc == 0">
                                            <span class="lbl"> Não &nbsp; &nbsp;</span>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Período de sincronização do CRM / RGC</label>
                                        </div>
                                        <div class="form-group form-inline col-md-4">
                                            <input type="text" class="form-control input-sm" maxlength="250" id="diasParaSincronizar" name="diasParaSincronizar" ng-model="parametros.sistema.integracoes.diasSincronizarCrmRgc" />
                                            <span class="lbl">&nbsp; &nbsp; dia(s) e iniciar às &nbsp; &nbsp;</span>
                                            <input type="text" class="form-control input-sm" maxlength="250" id="periodoSincronizacao" name="periodoSincronizacao" ng-model="parametros.sistema.integracoes.periodoSincronizarCrmRgc" />
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Horário de sincronização do CRM / RGC</label>
                                        </div>
                                        <div class="form-group form-inline col-md-4">
                                            <input type="text" class="form-control input-sm" maxlength="250" id="Text1" name="diasParaSincronizar" ng-model="parametros.sistema.integracoes.diasSincronizarCrmRgc" />
                                            <span class="lbl">&nbsp; &nbsp; dia(s) e iniciar às &nbsp; &nbsp;</span>
                                            <input type="text" class="form-control input-sm" maxlength="250" id="horaInicioSincronizacao" name="horaInicioSincronizacao" ng-model="parametros.sistema.integracoes.horasSincronizarCrmRgc" />
                                        </div>
                                    </div>--%>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label translate="2.LBL_UltimaSincronizacaoCRM">Última Sincronização com CRM / RGC</label>
                                        </div>
                                        <div class="form-group form-inline col-md-8">
                                            <input type="text" class="form-control input-sm fieldDisabled" maxlength="250" id="ultimaSincronizacaoCrmRgc" name="ultimaSincronizacaoCrmRgc" ng-model="itemParam.ultimaSincronizacaoCrmRgc" ng-disabled="true"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_UltimaSincronizacaoCRM' | translate}}"/>
                                            <%--<button class="btn btn-default btn-sm" scroll-to="" style="padding-top: 0px; padding-bottom: 0px;">
                                                <i class="bigger-110"></i>Sincronizar Agora
                                            </button>--%>
                                        </div>
                                    </div>
                                     <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label translate="2.LBL_sessoesAtivas">Sessões Ativas</label>
                                        </div>
                                        <div class="form-group form-inline col-md-8">
                                            <input type="text" class="form-control input-sm" maxlength="250" id="sessoesAtivas" name="sessoesAtivas" ng-model="itemParam.sessoesativas" ng-disabled="true"
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_sessoesAtivas' | translate}}"/>
                                            <%--<button class="btn btn-default btn-sm" scroll-to="" style="padding-top: 0px; padding-bottom: 0px;">
                                                <i class="bigger-110"></i>Sincronizar Agora
                                            </button>--%>
                                        </div>
                                    </div>
                                    <%--<div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Espelhar base de dados para BI?</label>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <input type="radio" name="espelharBaseBI" class="ace" value="parametros.sistema.integracoes.espelharBaseBi" ng-model="parametros.sistema.integracoes.espelharBaseBi" ng-checked="parametros.sistema.integracoes.espelharBaseBi == 1">
                                            <span class="lbl"> Sim &nbsp; &nbsp;</span>
                                            <input type="radio" name="espelharBaseBI" class="ace" value="parametros.sistema.integracoes.espelharBaseBi" ng-model="parametros.sistema.integracoes.espelharBaseBi" ng-checked="parametros.sistema.integracoes.espelharBaseBi == 0">
                                            <span class="lbl"> Não &nbsp; &nbsp;</span>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Espelhar a base a cada</label>
                                        </div>
                                        <div class="form-group form-inline col-md-4">
                                            <input type="text" class="form-control input-sm" maxlength="250" id="diasParaEspelharBase" name="diasParaEspelharBase" ng-model="parametros.sistema.integracoes.diasEspelharBaseBi" />
                                            <span class="lbl">&nbsp; &nbsp; dia(s) e iniciar às &nbsp; &nbsp;</span>
                                            <input type="text" class="form-control input-sm" maxlength="250" id="horaInicioEspelharBase" name="horaInicioEspelharBase" ng-model="parametros.sistema.integracoes.horasEspelharBaseBi" />
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Ultima Sincronização para base espelho (Relatórios BI)</label>
                                        </div>
                                        <div class="form-group form-inline col-md-8">
                                            <input type="text" class="form-control input-sm fieldDisabled" maxlength="250" id="ultimaSincronizacaoEspelho" name="ultimaSincronizacaoEspelho" ng-model="parametros.sistema.integracoes.ultimaSincronizacaoBaseEspelho" ng-disabled="true"/>
                                            <button class="btn btn-default btn-sm" scroll-to="" style="padding-top: 0px; padding-bottom: 0px;">
                                                <i class="bigger-110"></i>Sincronizar Agora
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12" style="font-size: 100%;">
                                            <b>02 - WebServices CNP</b>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Permitir inserção de produtos e GLN pelo associado</label>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <input type="radio" name="insercaoProdutosGln" class="ace" value="parametros.sistema.webServiceCnp.permitirInsercaoProduto" ng-model="parametros.sistema.webServiceCnp.permitirInsercaoProduto" ng-checked="parametros.sistema.webServiceCnp.permitirInsercaoProduto == 1">
                                            <span class="lbl"> Sim &nbsp; &nbsp;</span>
                                            <input type="radio" name="insercaoProdutosGln" class="ace" value="parametros.sistema.webServiceCnp.permitirInsercaoProduto" ng-model="parametros.sistema.webServiceCnp.permitirInsercaoProduto" ng-checked="parametros.sistema.webServiceCnp.permitirInsercaoProduto == 0">
                                            <span class="lbl"> Não &nbsp; &nbsp;</span>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Armazenar as informações vindas via Web Service</label>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <select class="form-control input-sm" id="armazenarInformacoesWS" name="armazenarInformacoesWS" ng-model="parametros.sistema.webServiceCnp.armazenarInformacoes" >
                                                <option value="{{parametros.sistema.webServiceCnp.armazenarInformacoes}}">{{parametros.sistema.webServiceCnp.armazenarInformacoes}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12" style="font-size: 100%;">
                                            <b>03 - Expurgo</b>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Expurgar dados?</label>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <input type="radio" name="expurgarDados" class="ace" value="parametros.sistema.expurgo.expurgarDados" ng-model="parametros.sistema.expurgo.expurgarDados" ng-checked="parametros.sistema.expurgo.expurgarDados == 1">
                                            <span class="lbl"> Sim &nbsp; &nbsp;</span>
                                            <input type="radio" name="expurgarDados" class="ace" value="parametros.sistema.expurgo.expurgarDados" ng-model="parametros.sistema.expurgo.expurgarDados" ng-checked="parametros.sistema.expurgo.expurgarDados == 0">
                                            <span class="lbl"> Não &nbsp; &nbsp;</span>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Expurgo de dados a cada</label>
                                        </div>
                                        <div class="form-group form-inline col-md-4">
                                            <input type="text" class="form-control input-sm" maxlength="250" id="diasParaExpurgoDados" name="diasParaExpurgoDados" ng-model="parametros.sistema.expurgo.diasExpurgar" />
                                            <span class="lbl">&nbsp; &nbsp; dia(s) e iniciar às &nbsp; &nbsp;</span>
                                            <input type="text" class="form-control input-sm" maxlength="250" id="horaInicioExpurgoDados" name="horaInicioExpurgoDados" ng-model="parametros.sistema.expurgo.horasExpurgar" />
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 10px;">
                                        <div class="form-group col-md-4" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                            <label>Ultimo expurgo de dados</label>
                                        </div>
                                        <div class="form-group form-inline col-md-8">
                                            <input type="text" class="form-control input-sm fieldDisabled" maxlength="250" id="ultimExpurgoDados" name="ultimExpurgoDados" ng-model="parametros.sistema.expurgo.ultimoExpurgo" ng-disabled="true"/>
                                            <button class="btn btn-default btn-sm" scroll-to="" style="padding-top: 0px; padding-bottom: 0px;">
                                                <i class="bigger-110"></i>Expurgar Agora
                                            </button>
                                        </div>
                                    </div>--%>
                                    <h3 class="header smaller lighter purple">
                                        02 - <span translate="2.LBL_InformacoesGerais">Informações Gerais</span>
                                    </h3>
                                    <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.numerotelefone.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_NumeroTelefone">Número de telefone</span>
                                            <input class="col-md-5 col-xs-12" type="text" ng-model="itemParam.numerotelefone" name="numerotelefone" ui-mask="(99)99999999?9" required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_NumeroTelefone' | translate}}"></input>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-10 col-xs-12" ng-class="{'has-error': form.numerotelefone.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_URLAtendimentoOnline">Url Atendimento Online</span>
                                            <input class="col-md-5 col-xs-12" type="text" ng-model="itemParam.urlchat" name="urlchat" required 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_URLAtendimentoOnline' | translate}}"></input>
                                        </div>
                                    </div>
                                   
                                    <h3 class="header smaller lighter purple">
                                        04 - <span translate="2.LBL_Gepir">Gepir</span>
                                    </h3>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.perfilgepir.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_Gepir">Perfil padrão de consulta no GEPIR</span>                                           
                                            <select class="col-md-5 col-xs-12" id="perfilpadrao" name="perfilpadrao" ng-model="itemParam.perfilpadrao" ng-options="profile.id as profile.name for profile in  configurationprofile">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.diarenovacaofranquia.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_DiaRenovacaoFranquia">Dia para renovação da franquia de consultas</span>
                                            <select class="col-md-5 col-xs-12" id="diarenovacaofranquia" name="diarenovacaofranquia" ng-model="itemParam.diarenovacaofranquia"
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.LBL_DiaRenovacaoFranquia' | translate}}">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="13">13</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                                <option value="18">18</option>
                                                <option value="19">19</option>
                                                <option value="20">20</option>
                                                <option value="21">21</option>
                                                <option value="22">22</option>
                                                <option value="23">23</option>
                                                <option value="24">24</option>
                                                <option value="25">25</option>
                                                <option value="26">26</option>
                                                <option value="27">27</option>
                                                <option value="28">28</option>
                                                <option value="29">29</option>
                                                <option value="30">30</option>
                                                <option value="31">31</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.glnconsultapadrao.$invalid && form.submitted}">
                                            <span class="col-md-7 col-xs-12 span-without-padding" translate="2.LBL_GLNPadrao">GLN de consulta padrão</span>
                                            <input class="col-md-5 col-xs-12" type="text" ng-model="itemParam.glnconsultapadrao" id="glnconsultapadrao" name="glnconsultapadrao" required 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.2.glnconsultapadrao' | translate}}">
                                            </input>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-actions center">
                        <button class="btn btn-info btn-sm" ng-click="onSave(itemParam)">
                            <i class="icon-save bigger-110"></i><span translate="2.BTN_Salvar">Salvar</span>
                        </button>
                        <button id="Button1" name="cancel" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button">
                            <i class="icon-remove bigger-110"></i><span translate="2.BTN_Cancelar">Cancelar</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
