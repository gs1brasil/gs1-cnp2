﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="perfil.aspx.cs" Inherits="GS1.CNP.views.cadastros.perfil" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/perfil";
    }
</script>
<script src="../../scripts/lib/jstree.min.js" type="text/javascript"></script>
<div ng-controller="PerfilCtrl" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; isPesquisa = false; importMode = false; entity = 'usuario';"
    style="height: 745px;">
    <watch-fetch ids="item.id_perfil"></watch-fetch>
    <div class="form-container" form-container id="formcontainer"> <%--id usado para corrigir bug do ie no layout, no momento de mudar do grid para o form--%>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" translate="1.LBL_CadastroPerfil"></label>
        </div>
        <!--Ajuda de Pesquisa-->
        <div class="pull-right" style="margin-top: -32px; margin-right: 80px;" ng-show="searchMode" ng-cloak>
            <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" title="{{ '1.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="2" tooltip-ajuda="'1.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <!--Ajuda de Edição-->
        <div class="pull-right" style="margin-top: -32px; margin-right: 200px;" ng-show="!searchMode && itemForm != undefined && itemForm.codigo != undefined" ng-cloak>
            <ajuda codigo="62" tooltip-ajuda="'1.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <!--Ajuda de Inserção-->
        <div class="pull-right" style="margin-top: -32px; margin-right: 200px;" ng-show="!searchMode && itemForm != undefined && itemForm.codigo == undefined" ng-cloak>
            <ajuda codigo="61" tooltip-ajuda="'1.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <div class="widget-toolbar my-tab" style="margin-top: -36px;">
            <ul id="tabs-perfil" class="nav nav-tabs">
                <li id="liTabPerfil" class="active"><a data-toogle="tab" ng-click="carregaPerfil()" href="#perfil" translate="1.ABA_Perfil">Perfil</a></li>
                <li id="liTabPermissao"><a data-toogle="tab" href="#tab_permissoes" ng-click="carregaArvoreAba()" ng-show="!searchMode" translate="1.ABA_Permissoes">Permissões</a></li>
            </ul>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky">
            </div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="perfil" class="tab-pane active">
                            <fieldset>
                                <div class="row" ng-show="!searchMode">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        (*) - <span translate="1.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                        <label for="nome">
                                            <span translate="1.LBL_Nome">Nome</span> <span ng-show='!searchMode'>(*)</span>
                                        </label>
                                        <input type="text" class="form-control input-sm" id="nome" maxlength="50" name="nome" ng-model="itemForm.nome" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.1.LBL_Nome' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-3" ng-class="{'has-error': form.codigotipousuario.$invalid && form.submitted}">
                                        <label for="user">
                                            <span translate="1.LBL_TipoUsuario">Tipo de Usuário</span> <span ng-show='!searchMode'>(*)</span>
                                        </label>
                                        <select class="form-control input-sm" id="codigotipousuario" ng-change="change()"
                                            ng-class="{'fieldDisabled': itemForm.codigo}"
                                            ng-disabled="itemForm.codigo" ng-model="itemForm.codigotipousuario" name="codigotipousuario"
                                            required ng-options="tipo.codigo as tipo.nome for tipo in tiposUsuario" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.1.LBL_TipoUsuario' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-3" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                        <label for="login">
                                            <span translate="1.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span>
                                        </label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="itemForm.status" required
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.1.LBL_Status' | translate}}">
                                            <option value="1" ng-selected="itemForm.status == 1">Ativo</option>
                                            <option value="0" ng-selected="itemForm.status == 0">Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                        <label for="descricao">
                                            <span translate="1.LBL_Descricao">Descrição</span> <span ng-show='!searchMode'>(*)</span>
                                        </label>
                                        <textarea class="form-control input-sm" id="descricao" sbx-maxlength="255" name="descricao"
                                            rows="3" ng-model="itemForm.descricao" style="resize: vertical;" required
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.1.LBL_Descricao' | translate}}"></textarea>
                                        <span style="font-size: 80%;">(<span translate="1.LBL_TamanhoMaximo">Tamanho máximo</span>: 255) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span translate="1.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.descricao.$viewValue.length || 0 }}</span>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div id="tab_permissoes" class="tab-pane">
                            <fieldset style="height: 433px; overflow-y: auto; overflow-x: auto">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <h4 translate="1.LBL_Permissoes">Permissões</h4>
                                        <div ng-show="itemForm.codigotipousuario == 3" class="form-group col-md-12" style="margin-top: 1em;">
                                            Não há permissões disponíveis para usuário Externo.
                                        </div>
                                        <div id="arvore-permissoes" class="demo" style="margin-top: 1em;">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="form-actions center" ng-show="editMode">
                            <button class="btn btn-info btn-sm" name="find" scroll-to="" ng-cloak ng-show="searchMode" ng-click="onSearch(itemForm)" type="button">
                                <i class="icon-search bigger-110"></i><span translate="1.BTN_Pesquisar">Pesquisar</span>
                            </button>
                            <button class="btn btn-info btn-sm" scroll-to="" ng-cloak ng-show="!searchMode" ng-click="onSave(itemForm)" type="button">
                                <i class="icon-save bigger-110"></i><span translate="1.BTN_Salvar">Salvar</span>
                            </button>
                            <button name="cancel" ng-click="onCancel(itemForm)" ng-keydown="focusFirstInput($event)"
                                class="btn btn-danger btn-sm" type="button">
                                <i class="icon-remove bigger-110"></i><span translate="1.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" translate="1.LBL_CadastroPerfil"></label>
        </div>
        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '1.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
            <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '1.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="1" tooltip-ajuda="'1.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <div class="widget-box">
            <%--<div class="widget-header">
                <h4>Registros</h4>

                <div class="widget-toolbar">
                    <a href=""><i class="icon-plus" title="Novo registro" ng-click="onFormMode()"></i></a>
                    <a href=""><i class="icon-search" title="Pesquisar" ng-click="onSearchMode(item)"></i></a>
                    <a  href="/views/backoffice/ajuda/perfilListagem.html" target="_blank"><i class="icon-question-sign" title="Ajuda" ></i></a>
                </div>
            </div>--%>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" style="height: 631px;" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i>
                    <i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 300px;">
                    <div class="row">
                        <div class="form-group col-md-12 information" translate="1.LBL_CliqueRegistroEditar">Clique em um registro para editar</div>
                    </div>
                    <div class="table-responsive" style="height: 555px; overflow-y: auto; overflow-x: auto">
                        <table ng-table="tablePerfil" class="table table-bordered table-striped table-hover"
                            ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}"
                            style="display: inline-table;">
                            <thead>
                                <tr>
                                    <th ng-click="tablePerfil.sorting('codigo', tablePerfil.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                      <span># &nbsp;&nbsp; </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tablePerfil.isSortBy('codigo', 'asc'),
                                          'icon-sort-down': tablePerfil.isSortBy('codigo', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tablePerfil.sorting('nome', tablePerfil.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                      <span translate="1.GRID_Nome">Nome &nbsp;&nbsp; </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tablePerfil.isSortBy('nome', 'asc'),
                                          'icon-sort-down': tablePerfil.isSortBy('nome', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tablePerfil.sorting('descricao', tablePerfil.isSortBy('descricao', 'asc') ? 'desc' : 'asc')">
                                      <span translate="1.GRID_Descricao">Descrição &nbsp;&nbsp; </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tablePerfil.isSortBy('descricao', 'asc'),
                                          'icon-sort-down': tablePerfil.isSortBy('descricao', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tablePerfil.sorting('nometipousuario', tablePerfil.isSortBy('nometipousuario', 'asc') ? 'desc' : 'asc')">
                                      <span translate="1.GRID_TipoUsuario">Tipo de Usuário &nbsp;&nbsp; </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tablePerfil.isSortBy('nometipousuario', 'asc'),
                                          'icon-sort-down': tablePerfil.isSortBy('nometipousuario', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tablePerfil.sorting('totalusuarios', tablePerfil.isSortBy('totalusuarios', 'asc') ? 'desc' : 'asc')">
                                      <span translate="1.GRID_QuantidadeUsuarios">Quantidade Usuários &nbsp;&nbsp; </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tablePerfil.isSortBy('totalusuarios', 'asc'),
                                          'icon-sort-down': tablePerfil.isSortBy('totalusuarios', 'desc')
                                        }"></i>
                                    </th>
                                    <th ng-click="tablePerfil.sorting('status', tablePerfil.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                      <span translate="1.GRID_Status">Status &nbsp;&nbsp; </span>
                                      <i class="text-center icon-sort" ng-class="{
                                          'icon-sort-up': tablePerfil.isSortBy('status', 'asc'),
                                          'icon-sort-down': tablePerfil.isSortBy('status', 'desc')
                                        }"></i>
                                    </th>
                                    <th><span translate="1.GRID_Acao">Ação</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '25%' }" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '30%' }" sortable="'descricao'" >{{item1.descricao}}</td>
                                    <td ng-style="{ 'width': '13%' }" sortable="'nometipousuario'">{{item1.nometipousuario}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'totalusuarios'">{{item1.totalusuarios}}</td>
                                    <td ng-style="{ 'width': '7%' }" sortable="'status'">{{item1.status | status}}</td>
                                    <td ng-style="{ 'width': '5%' }" class="action-buttons center">
                                        <a class="red" href="" title="{{ '1.TOOLTIP_Excluir' | translate }}" ng-click="onDelete(item1); $event.stopPropagation();">
                                            <i class="icon-trash bigger-130"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="7" class="text-center" translate="1.LBL_NenhumRegistroEncontrado">
                                        Nenhum registro encontrado
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
