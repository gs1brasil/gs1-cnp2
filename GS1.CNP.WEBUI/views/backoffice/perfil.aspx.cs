﻿using GS1.CNP.WEBUI.Core;

namespace GS1.CNP.views.cadastros
{
    public partial class perfil : BaseWeb
    {
        public override string PermissaoPagina { get { return "PesquisarPerfil"; } }
    }
}