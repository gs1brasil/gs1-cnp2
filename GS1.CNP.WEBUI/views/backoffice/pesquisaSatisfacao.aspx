﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pesquisaSatisfacao.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.pesquisaSatisfacao" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/pesquisaSatisfacao";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="PesquisaSatisfacao" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; exibeMensagem = false; entity = 'papel'; itemFormEspec = { statusregistro: 'novo' };" style="height: 820px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" translate="42.LBL_TitleCadastroPesquisaSatisfacao"></label>
        </div>

        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" title="{{ '42.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="73" ng-show="searchMode" tooltip-ajuda="'50.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="74" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'50.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="75" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'50.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>

        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="tipo" class="tab-pane active">
                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                <div class="row" ng-show="!searchMode">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        (*) - <span translate="42.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                        <label for="nome"><span translate="42.lbl_titulo">Título</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="255" id="nome" name="nome" ng-model="item.nome" required="required" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.42.lbl_titulo' | translate}}"/>
                                    </div>
                                </div>
                                <div class="row" ng-show="!isPesquisa">
                                    <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                        <label for="descricao"><span translate="42.LBL_Descricao">Descrição</span> <span ng-show="!searchMode">(*)</span></label>
                                        <textarea class="form-control input-sm" id="descricao" sbx-maxlength="400" placeholder="{{ '42.PLACEHOLDER_InsiraDescricaoPesquisaSatisfacao' | translate }}" name="descricao" rows="3" ng-model="item.descricao" style="resize: vertical;" required
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.42.LBL_Descricao' | translate}}"></textarea>

                                        <span style="font-size: 80%;">(<span translate="42.LBL_TamanhoMaximo">Tamanho máximo</span>: 400)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span translate="42.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.descricao.$viewValue.length || 0 }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" ng-class="{'has-error': form.url.$invalid && form.submitted}">
                                        <label><span translate="42.LBL_LinkPesquisa">Link para a Pesquisa</span> <span ng-show="!searchMode">(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="255" id="Text1" name="url" ng-model="item.url" required="required" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.42.LBL_LinkPesquisa' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.codigopublicoalvo.$invalid && form.submitted}">
                                        <label for="status"><span translate="42.LBL_PublicoAlvo">Público Alvo</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" id="Select1" name="codigopublicoalvo" ng-model="item.codigopublicoalvo" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.42.LBL_PublicoAlvo' | translate}}">
                                            <option ng-repeat="publico in publicosalvo" value="{{publico.codigo}}" ng-selected="publico.codigo == item.codigopublicoalvo">{{publico.nome}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                        <label for="status"><span translate="42.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="item.status" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.42.LBL_Status' | translate}}">
                                            <option value="1" ng-selected="item.status == 1">Ativo</option>
                                            <option value="0" ng-selected="item.status == 0">Inativo</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                        <label for="inicio"><span translate="42.LBL_InicioPesquisa">Início da Pesquisa</span> <span ng-show="!searchMode">(*)</span></label>
                                        <input type="text" class="form-control input-sm datepicker" id="inicio" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="10" name="inicio" ng-model="item.inicio" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.42.LBL_InicioPesquisa' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-3 col-xs-12" ng-class="{'has-error': form.fim.$invalid && form.submitted}">
                                        <label for="fim"><span translate="42.LBL_FimPesquisa">Fim da Pesquisa</span> <span ng-show="!searchMode">(*)</span></label>
                                        <input type="text" class="form-control input-sm datepicker" id="fim" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="10" name="fim" ng-model="item.fim" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.42.LBL_FimPesquisa' | translate}}"/>
                                    </div>
                                </div>
                                <div class="form-actions center" ng-show="editMode">
                                    <button class="btn btn-info btn-sm" name="find" scroll-to="" ng-cloak ng-show="isPesquisa" ng-click="onSearch(item)">
                                        <i class="icon-search bigger-110"></i><span translate="42.BTN_Pesquisar">Pesquisar</span>
                                    </button>
                                    <button class="btn btn-info btn-sm" scroll-to="" ng-show="!isPesquisa" ng-click="onSave(item)">
                                        <i class="icon-save bigger-110"></i><span translate="42.BTN_Salvar">Salvar</span>
                                    </button>
                                    <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="42.BTN_Cancelar">Cancelar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label translate="42.LBL_TitleCadastroPesquisaSatisfacao">Cadastro de Pesquisa de Satisfação</label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '42.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
            <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '42.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="72" tooltip-ajuda="'50.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 480px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="42.LBL_CliqueRegistroEditar">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tablePesquisaSatisfacao" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
	                            <tr>
                                    <th ng-click="tablePesquisaSatisfacao.sorting('codigo', tablePesquisaSatisfacao.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                      <span>#</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePesquisaSatisfacao.isSortBy('codigo', 'asc'),
                                        'icon-sort-down': tablePesquisaSatisfacao.isSortBy('codigo', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tablePesquisaSatisfacao.sorting('nome', tablePesquisaSatisfacao.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                      <span translate="42.GRID_Titulo">Título</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePesquisaSatisfacao.isSortBy('nome', 'asc'),
                                        'icon-sort-down': tablePesquisaSatisfacao.isSortBy('nome', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tablePesquisaSatisfacao.sorting('inicio', tablePesquisaSatisfacao.isSortBy('inicio', 'asc') ? 'desc' : 'asc')">
                                      <span translate="42.GRID_Inicio">Início</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePesquisaSatisfacao.isSortBy('inicio', 'asc'),
                                        'icon-sort-down': tablePesquisaSatisfacao.isSortBy('inicio', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tablePesquisaSatisfacao.sorting('fim', tablePesquisaSatisfacao.isSortBy('fim', 'asc') ? 'desc' : 'asc')">
                                      <span translate="42.GRID_Fim">Fim</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePesquisaSatisfacao.isSortBy('fim', 'asc'),
                                        'icon-sort-down': tablePesquisaSatisfacao.isSortBy('fim', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tablePesquisaSatisfacao.sorting('usuario', tablePesquisaSatisfacao.isSortBy('usuario', 'asc') ? 'desc' : 'asc')">
                                      <span translate="42.GRID_CriadoPor">Criado Por</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePesquisaSatisfacao.isSortBy('usuario', 'asc'),
                                        'icon-sort-down': tablePesquisaSatisfacao.isSortBy('usuario', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tablePesquisaSatisfacao.sorting('dataalteracao', tablePesquisaSatisfacao.isSortBy('dataalteracao', 'asc') ? 'desc' : 'asc')">
                                      <span translate="42.GRID_UltimaAtualizacao">Última Atualização</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePesquisaSatisfacao.isSortBy('dataalteracao', 'asc'),
                                        'icon-sort-down': tablePesquisaSatisfacao.isSortBy('dataalteracao', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tablePesquisaSatisfacao.sorting('status', tablePesquisaSatisfacao.isSortBy('status', 'asc') ? 'desc' : 'asc')">
                                      <span translate="42.GRID_Status">Status</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tablePesquisaSatisfacao.isSortBy('status', 'asc'),
                                        'icon-sort-down': tablePesquisaSatisfacao.isSortBy('status', 'desc')
                                      }"></i>
                                    </th>
                                    <th>
                                      <span translate="42.GRID_Acao">Ação</span>
                                    </th>                            
	                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '30%' }" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'inicio'">{{item1.inicio | date : 'dd/MM/yyyy HH:mm'}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'fim'">{{item1.fim | date : 'dd/MM/yyyy HH:mm'}}</td>
                                    <td ng-style="{ 'width': '15%'}" sortable="'usuario'">{{item1.usuario}}</td>
                                    <td ng-style="{ 'width': '15%'}" sortable="'dataalteracao'">{{item1.dataalteracao | date : 'dd/MM/yyyy HH:mm'}}</td>
                                    <td ng-style="{ 'width': '15%'}" sortable="'status'">{{item1.status | status}}</td>
                                    <td ng-style="{ 'width': '10%' }" class="action-buttons center">
                                        <a class="red" href="" ng-click="$event.stopPropagation(); removePesquisa(item1);"><i class="icon-trash bigger-130" title="{{ '15.TOOLTIP_Excluir' | translate }}" style="cursor:pointer; text-decoration:none;"></i></a>&nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="7" class="text-center" translate="42.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
