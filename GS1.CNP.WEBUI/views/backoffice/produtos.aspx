﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="produtos.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.produtos" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    * {
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    [ng-drag] {
        -moz-user-select: -moz-none;
        -khtml-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    [ng-drag] {
        color: white;
        text-align: center;
        display: block;
        cursor: move;
    }

        [ng-drag].dragging {
            opacity: 0.5;
        }

    [ng-drop] {
        text-align: center;
        display: block;
        position: relative;
        width: 120px;
        height: 100px;
        float: left;
    }

        [ng-drop] div {
            position: relative;
            z-index: 2;
        }

    .draglist {
        display: inline-block;
        margin: 0 auto;
        padding-left: 40px;
    }

    .modal-dialog {
        margin: 30px auto;
        width: 700px;
        margin-top: 5%;
    }

    .hasError > div > div {
        border-color: #f09784;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    }

    disabled {
        pointer-events: none;
        cursor: default;
    }

    .has-error .form-control {
        border-color: #f09784;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    }
</style>

<div ng-controller="ProdutoCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false;"
    style="height: 2800px; overflow-y: scroll">
    <div ng-cloak ng-show="flagAssociadoSelecionado">
        <watch-fetch ids="itemForm.id_produto"></watch-fetch>
        <div class="form-container" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label ng-bind="titulo" translate="15.LBL_CadastroProduto"></label>
            </div>

            <div class="row-fluid">
				<div class="span12">
					<div class="widget-box">
						<div class="widget-header widget-header-blue widget-header-flat">
							<h4 class="lighter" translate="15.LBL_Produto">Produto</h4>

							<div class="pull-right" style="margin-top:2px; margin-right:2px;">
                                <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" title="{{ '15.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
                                <ajuda codigo="40" ng-show="searchMode" tooltip-ajuda="'15.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                                <ajuda codigo="41" ng-show="!searchMode && itemForm != undefined && itemForm.codigoproduto == undefined" tooltip-ajuda="'15.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                                <ajuda codigo="42" ng-show="!searchMode && itemForm != undefined && itemForm.codigoproduto != undefined" tooltip-ajuda="'15.TOOLTIP_Ajuda'" ng-cloak></ajuda>
                            </div>
						</div>

						<div class="widget-body">
							<div class="widget-main" ng-show="!isPesquisa">
								<div id="fuelux-wizard" class="row-fluid" data-target="#step-container">
									<ul class="wizard-steps">
                                        <li ng-repeat="step in steps track by $index"
                                            ng-click="selectStep(($index + 1))" ng-mouseout="removeClassComplete(($index + 1))" ng-mouseover="setClassComplete(($index + 1))"
                                            ng-class="{active: stepCalculation == ($index + 1), complete: stepCalculation > ($index + 1) || (showComplete && queroSelecionar >= ($index + 1))}">
                                            <span class="step produtos">{{($index + 1)}}</span>
                                            <span class="title hidden-xs">{{step}}</span>
                                        </li>
									</ul>
								</div>
								<hr />
								<div class="step-content row-fluid position-relative" id="step-container">
									<div class="step-pane" id="step1" ng-class="{'active': stepCalculation == 1}">
										<form name="form.base" id="form4" novalidate class="form" role="form">
                                            <div style="min-height:400px; overflow-x: hidden; overflow-y: auto; clear: both;">
                                                <div class="row" ng-show="!searchMode">
                                                    <div class="form-group col-md-12 col-xs-12" style="font-size: 90%;">
                                                        (*) - <span translate="15.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                                    </div>
                                                </div>
                                                <h3 class="header smaller lighter purple" translate="15.LBL_Identificacao">
                                                    Identificação
                                                </h3>
                                                <div class="row" id="identificacao_row1">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.codigotipogtin.$invalid && form.submitted}">
                                                        <label>
                                                            <span translate="15.LBL_TipoNumeroGTIN">Tipo do Número de Identificação GTIN</span> <span ng-show='!searchMode && isRequiredAlways'>(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="codigotipogtin" name="codigotipogtin" ng-model="itemForm.codigotipogtin"
                                                            ng-class="{'fieldDisabled': (editMode && itemForm.globaltradeitemnumber != undefined) || bloquearGTIN}"
                                                            ng-disabled="(editMode && itemForm.globaltradeitemnumber != undefined) || bloquearGTIN" ng-required="isRequiredAlways" ng-change="obterTipoGtin(itemForm.codigotipogtin)"
                                                            ng-options="licenca.codigo as licenca.nome for licenca in  licencas | filter:filtraGTIN8" ng-show="itemForm.globaltradeitemnumber || licenca.codigo !== 1"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_TipoNumeroGTIN' | translate}}">
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.base.codigostatusgtin.$invalid && form.submitted}">
                                                        <label for="codigostatusgtin" data-viewport="#identificacao_row1" translate="15.LBL_StatusGTIN">Status do GTIN </label>
                                                        <select class="form-control input-sm" id="Select1" name="codigostatusgtin" ng-model="itemForm.codigostatusgtin"
                                                            ng-class="{'fieldDisabled': itemCompare.codigostatusgtin == 5 || itemCompare.codigostatusgtin == 7 || initMode || itemForm.globaltradeitemnumber == undefined || itemForm.globaltradeitemnumber == ''}"
                                                            ng-disabled="itemCompare.codigostatusgtin == 5 || itemCompare.codigostatusgtin == 7 || initMode || itemForm.globaltradeitemnumber == undefined || itemForm.globaltradeitemnumber == ''"
                                                            ng-change="showMessageStatus(itemForm.codigostatusgtin, '{{itemForm.codigostatusgtin}}')"
                                                            ng-options="state.codigo as state.nome for state in statusgtin | filter:shouldShow"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_StatusGTIN' | translate}}">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row" ng-show="itemForm.codigotipogtin == 4">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.codigoprodutoorigem.$invalid && form.submitted}">
                                                        <div class="form-group col-md-12 col-xs-12" style="margin-left: -15px;">
                                                            <label for="hierarquia">
                                                                <span translate="15.LBL_GTINOrigem">GTIN Origem</span> <span ng-show="!searchMode && itemForm.codigotipogtin == 4">(*)</span>
                                                            </label>
                                                        </div>
                                                        <div class="form-group col-md-4 col-xs-4" style="margin-left: -15px; margin-top: -15px;">
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="codigoprodutoorigem" ng-model="itemForm.codigogtinorigem" maxlength="18" ng-disabled="true" ng-readonly="true"
                                                                ng-required="itemForm.codigotipogtin == 4"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_GTINOrigem' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-7 col-xs-7" ng-if="!searchMode" style="margin-left: -15px; margin-top: -15px;">
                                                            <input type="text" class="form-control input-sm fieldDisabled" name="nomeprodutoorigem" ng-model="itemForm.nomeprodutoorigem" ng-disabled="true"
                                                                ng-readonly="true" ng-required="itemForm.codigotipogtin == 4"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_GTINOrigem' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-1 col-xs-1" ng-if="!searchMode" style="margin-left: -15px; margin-top: -15px;">
                                                            <a class="btn btn-info btn-xs" ng-click="onGtinOrigem(item)" title="{{ '15.TOOLTIP_PesquisarGTINOrigem' | translate }}"
                                                                ng-class="{'fieldDisabled': editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''}"
                                                                ng-disabled="editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''">
                                                                <i class="fa fa-search"></i>&nbsp;<span translate="15.BTN_Procurar">Procurar</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.totalquantityofnextlowerleveltradeitem.$invalid && form.submitted}">
                                                        <label for="totalquantityofnextlowerleveltradeitem">
                                                            <span translate="15.LBL_QuantidadeItensCaixa">Quantidade de itens por caixa</span> <span ng-show='!searchMode && itemForm.codigotipogtin == 4'>(*)</span>
                                                        </label>
                                                        <input type="text" class="form-control input-sm" maxlength="9" id="totalquantityofnextlowerleveltradeitem" name="totalquantityofnextlowerleveltradeitem"
                                                            ng-model="itemForm.totalquantityofnextlowerleveltradeitem" integer ng-required="itemForm.codigotipogtin == 4"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_QuantidadeItensCaixa' | translate}}"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.globaltradeitemnumber.$invalid && form.submitted}">
                                                        <label for="globaltradeitemnumber">
                                                            GTIN - <span translate="15.LBL_NumeroGlobalItemComercial">Número Global do Item Comercial </span>
                                                        </label>
                                                        <input type="text" class="form-control input-sm" maxlength="14" id="Text9" name="globaltradeitemnumber" ng-model="itemForm.globaltradeitemnumber" ng-readonly="true" integer
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_NumeroGlobalItemComercial' | translate}}"/>
                                                    </div>
                                                </div>
                                                <div class="row" ng-hide="escondeCamposGTIN14">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.brandname.$invalid && form.submitted}">
                                                        <label for="brandname" translate="15.LBL_Marca">
                                                            Marca
                                                        </label>
                                                        <input type="text" class="form-control input-sm" id="brandname" maxlength="50" name="brandname"
                                                            ng-model="itemForm.brandname" data-toggle="popover" data-container="body" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Marca' | translate}}"/> <%--ng-required="!escondeCamposGTIN14 && (isRequiredGDSN || isRequiredCNP)"--%>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.modelnumber.$invalid && form.base.submitted}">
                                                        <label for="modelnumber">
                                                            <span translate="15.LBL_NumeroModelo">Número do Modelo</span>
                                                        </label>
                                                        <input type="text" class="form-control input-sm" maxlength="70" id="modelnumber" name="modelnumber" ng-model="itemForm.modelnumber" 
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_NumeroModelo' | translate}}"/>
                                                    </div>
                                                </div>
                                                <div class="row" ng-hide="escondeCamposGTIN14">
                                                    <div class="form-group col-md-3" ng-class="{'has-error': form.base.codesegment.$invalid && form.submitted}">
                                                        <label for="codesegment">
                                                            <span translate="15.LBL_Segmento">Segmento do Produto</span> <span ng-show='!searchMode'>(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="codesegment" name="codesegment" ng-model="itemForm.codesegment"
                                                            ng-change="onSelectSegment(itemForm.codesegment)" ng-required="!escondeCamposGTIN14"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Segmento' | translate}}">
                                                            <option ng-repeat="segmento in segmentos" value="{{segmento.codigo}}" ng-selected="itemForm.codesegment == segmento.codigo">{{segmento.nome}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3" ng-class="{'has-error': form.base.codefamily.$invalid && form.submitted}">
                                                        <label for="codefamily">
                                                            <span translate="15.LBL_Familia">Família do Produto</span> <span ng-show='!searchMode'>(*)</span>
                                                        </label> 
                                                        <select class="form-control input-sm" id="codefamily" name="codefamily" ng-model="itemForm.codefamily"
                                                            ng-class="{'fieldDisabled': !searchMode && (itemForm.codesegment == undefined || itemForm.codesegment == null || itemForm.codesegment == '')}"
                                                            ng-disabled="!searchMode && (itemForm.codesegment == undefined || itemForm.codesegment == null || itemForm.codesegment == '')"
                                                            ng-required="!escondeCamposGTIN14" ng-change="onSelectFamily(itemForm.codefamily)"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Familia' | translate}}">
                                                            <option ng-repeat="familia in familias" value="{{familia.codigo}}" ng-selected="{{itemForm.codefamily == familia.codigo}}">{{familia.nome}}</option>
                                                        </select> 
                                                    </div>
                                                    <div class="form-group col-md-3" ng-class="{'has-error': form.base.codeclass.$invalid && form.submitted}">
                                                        <label for="codeclass">
                                                            <span translate="15.LBL_Classe">Classe do Produto</span> <span ng-show='!searchMode'>(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="codeclass" name="codeclass" ng-model="itemForm.codeclass"
                                                            ng-class="{'fieldDisabled': !searchMode && (itemForm.codefamily == undefined || itemForm.codefamily == null || itemForm.codefamily == '')}"
                                                            ng-disabled="!searchMode && (itemForm.codefamily == undefined || itemForm.codefamily == null || itemForm.codefamily == '')"
                                                            ng-required="!escondeCamposGTIN14" ng-change="onSelectClass(itemForm.codeclass)"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Classe' | translate}}">
                                                            <option ng-repeat="classe in classes" value="{{classe.codigo}}" ng-selected="{{itemForm.codeclass == classe.codigo}}">{{classe.nome}}</option>
                                                        </select> 
                                                    </div>
                                                    <div class="form-group col-md-3" ng-class="{'has-error': form.base.codebrick.$invalid && form.submitted}">
                                                        <label for="codebrick">
                                                            <span translate="15.LBL_Brick">Sub Classe do Produto</span> <span ng-show='!searchMode'>(*)</span>
                                                        </label> 
                                                        <select class="form-control input-sm" id="codebrick" name="codebrick" ng-model="itemForm.codebrick"
                                                            ng-class="{'fieldDisabled': !searchMode && (itemForm.codeclass == undefined || itemForm.codeclass == null || itemForm.codeclass == '')}"
                                                            ng-disabled="!searchMode && (itemForm.codeclass == undefined || itemForm.codeclass == null || itemForm.codeclass == '')"
                                                            ng-required="!escondeCamposGTIN14" ng-change="onSelectBrick(); updateProductDescription(itemForm.codebrick);"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Brick' | translate}}">
                                                            <option ng-repeat="brick in bricks" value="{{brick.codigo}}" ng-selected="{{itemForm.codebrick == brick.codigo}}">{{brick.nome}}</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row" id="identificacao_row_descricao">
                                                    <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.base.productdescription.$invalid && form.submitted}">
                                                        <label for="productdescription">
                                                            <span translate="15.LBL_DescricaoProduto">Descrição do Produto</span> <span ng-show='!searchMode && isRequiredAlways'>(*)</span>
                                                        </label>
                                                        <textarea class="form-control input-sm" id="productdescription" sbx-maxlength="300" name="productdescription" rows="3"
                                                            ng-model="itemForm.productdescription" placeholder="{{ '15.PLACEHOLDER_ExemploSuco' | translate }}"
                                                            style="resize: vertical;" ng-required="isRequiredAlways" ng-disabled="itemForm.codigotipogtin==1"
                                                            ng-class="{'fieldDisabled': itemForm.codigotipogtin==1}"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DescricaoProduto' | translate}}"></textarea>
                                                        <span style="font-size: 80%;">(<span translate="15.LBL_TamanhoMaximo">Tamanho máximo</span>: 300)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <span translate="15.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.base.productdescription.$viewValue.length || 0 }}</span>
                                                    </div>
                                                </div>

                                                <cnp-informacoes-nutricionais item="informacoesNutricionais" ng-show="showInformacoesNutricionais"></cnp-informacoes-nutricionais>
                                                <cnp-alergenos list="alergenos" ng-show="showAlergenos"></cnp-alergenos>

                                                <h3 class="header smaller lighter purple" ng-show="showInformacoesNutricionais || showAlergenos"></h3>

                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.informationprovider.$invalid && form.submitted}">
                                                        <label for="informationprovider" translate="15.LBL_GLNProvedor">
                                                            GLN do Provedor da Informação
                                                        </label>  
                                                        <input type="text" class="form-control input-sm fieldDisabled" id="informationprovider" name="informationprovider" ng-model="itemForm.informationprovider" ng-disabled="true" ng-readonly="true" 
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_GLNProvedor' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-3" ng-class="{'has-error': form.base.startavailabilitydatetime.$invalid && form.submitted}">
                                                        <label for="startavailabilitydatetime">
                                                            <span translate="15.LBL_DataLancamento">Data de lançamento</span>&nbsp;<span translate="15.LBL_ParaOMercado">para o mercado</span> </label> 
                                                        <input type="text" class="form-control input-sm datepicker" id="startavailabilitydatetime" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="10" name="startavailabilitydatetime"
                                                            ng-model="itemForm.startavailabilitydatetime"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DataLancamento' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-3" ng-class="{'has-error': form.base.endavailabilitydatetime.$invalid && form.submitted}" ng-show="isRequiredGDSN">
                                                        <label for="endavailabilitydatetime"><span translate="15.LBL_DataFinalDisponibilidade">Data Final de Disponibilidade</span> <span ng-show='!searchMode && isRequiredGDSN'>(*)</span></label>
                                                        <input type="text" class="form-control input-sm datepicker" id="endavailabilitydatetime" sbx-datepicker data-date-format="dd/mm/yyyy"
                                                            ng-click="datepicker()" maxlength="10" name="endavailabilitydatetime" ng-model="itemForm.endavailabilitydatetime" ng-required="isRequiredGDSN"
                                                            data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DataFinalDisponibilidade' | translate}}"/>
                                                    </div>
                                                </div>
                                                <div class="row" ng-hide="escondeCamposGTIN14">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.importclassificationvalue.$invalid && form.submitted}">
                                                        <div class="form-group col-md-12 col-xs-12" style="margin-left: -15px;">
                                                            <label for="importclassificationvalue">
                                                                <span translate="15.LBL_Numero">Número</span> NCM/TIPI <span ng-show="!searchMode &amp;&amp; !escondeCamposGTIN14" class="">(*)</span>
                                                            </label>
                                                        </div>
                                                        <div class="form-group col-md-3 col-xs-3" style="margin-left: -15px; margin-top: -15px;">
                                                            <input type="text" class="form-control input-sm col-md-2" maxlength="100" id="importclassificationvalue" name="importclassificationvalue"
                                                                ng-model="itemForm.importclassificationvalue" ng-required="!escondeCamposGTIN14" ng-readonly="true"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Numero' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-8 col-xs-8" ng-if="!searchMode" style="margin-left: -15px; margin-top: -15px;">
                                                            <input type="text" class="form-control input-sm" id="ncmdescricao" name="ncmdescricao" ng-model="itemForm.ncmdescricao" ng-readonly="true"/>
                                                        </div>
                                                        <div class="form-group col-md-1 col-xs-1" ng-if="!searchMode" style="margin-left: -15px; margin-top: -15px;">
                                                            <a class="btn btn-info btn-xs" ng-click="onNCM(item)" title="{{ '15.TOOLTIP_PesquisarNCM' | translate }}">
                                                                <i class="fa fa-search"></i> <span translate="15.BTN_Procurar">Procurar</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.cestNumber.$invalid && form.submitted}">
                                                        <div class="form-group col-md-12 col-xs-12" style="margin-left: -15px;">
                                                            <label for="cestNumber"><span translate="15.LBL_NumeroCest">Número CEST</span></label>
                                                        </div>
                                                        <div class="form-group col-md-3 col-xs-3" style="margin-left: -15px; margin-top: -15px;">
                                                            <input type="text" class="form-control input-sm col-md-2" maxlength="100" id="cestNumber" name="cestNumber"
                                                                ng-model="itemForm.cestnumber" ng-readonly="true"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_NumeroCest' | translate}}"/>
                                                        </div>
                                                        <div class="form-group col-md-8 col-xs-8" ng-if="!searchMode" style="margin-left: -15px; margin-top: -15px;">
                                                            <input type="text" class="form-control input-sm" id="cestdescricao" name="cestdescricao" ng-model="itemForm.cestdescricao" ng-readonly="true"/>
                                                        </div>
                                                        <div class="form-group col-md-1 col-xs-1" ng-if="!searchMode" style="margin-left: -15px; margin-top: -15px;">
                                                            <a class="btn btn-info btn-xs" ng-click="onCEST(item)" title="{{ '15.TOOLTIP_Pesquisar' | translate }}">
                                                                <i class="fa fa-search"></i> <span translate="15.BTN_Procurar">Procurar</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div ng-show="isRequiredGDSN && !escondeCamposGTIN14">
                                                    <label data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_AtributoBrick' | translate}}">
                                                        <span translate="15.LBL_AtributoBrick">Atributo do Brick</span> <span ng-show='!searchMode && isRequiredGDSN'>(*)</span>
                                                    </label>
                                                    <div class="table-responsive">
                                                        <table ng-table="tableAtributoBrick" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                                            <thead>
                                                                <tr>
                                                                    <th ng-click="tableAtributoBrick.sorting('codetype', tableAtributoBrick.isSortBy('codetype', 'asc') ? 'desc' : 'asc')">
                                                                      <span>#</span>&nbsp;&nbsp;
                                                                      <i class="text-center icon-sort" ng-class="{
                                                                        'icon-sort-up': tableAtributoBrick.isSortBy('codetype', 'asc'),
                                                                        'icon-sort-down': tableAtributoBrick.isSortBy('codetype', 'desc')
                                                                      }"></i>
                                                                    </th>
                                                                    <th ng-click="tableAtributoBrick.sorting('text', tableAtributoBrick.isSortBy('text', 'asc') ? 'desc' : 'asc')">
                                                                      <span translate="15.GRID_Texto">Texto</span>&nbsp;&nbsp;
                                                                      <i class="text-center icon-sort" ng-class="{
                                                                        'icon-sort-up': tableAtributoBrick.isSortBy('text', 'asc'),
                                                                        'icon-sort-down': tableAtributoBrick.isSortBy('text', 'desc')
                                                                      }"></i>
                                                                    </th>
                                                                    <th>
                                                                      <span translate="15.GRID_Selecione">Selecione</span>&nbsp;&nbsp;
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr ng-repeat="item1 in $data">
                                                                    <td ng-style="{ 'width': '10%' }" sortable="'codetype'">{{item1.codetype}}</td>
                                                                    <td ng-style="{ 'width': '30%' }" sortable="'text'">{{item1.text}}</td>
                                                                    <td ng-style="{ 'width': '60%' }" data-title="'Selecione'">
                                                                        <div ng-class="{'has-error': form.base.codevalue.$invalid && form.submitted}">
                                                                            <select class="form-control input-sm" id="codevalue" name="codevalue" ng-model="item1.codevalue"
                                                                                ng-change="alterarStatusAtributoBrick(item1)"
                                                                                ng-options="attribute.codevalue as attribute.text for attribute in attributeValues"
                                                                                ng-required="isRequiredGDSN && !escondeCamposGTIN14">
                                                                                <option value=""></option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr ng-show="$data.length == 0">
                                                                    <td colspan="7" class="text-center" translate="15.LBL_NenhumRegistroEncontrado">Nenhum registro selecionado</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <%--<label data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_AgenciaReguladoraCodigoInterno' | translate}}">
                                                            <span translate="15.LBL_AgenciaReguladoraCodigoInterno">Agência Reguladora / Código Interno</span>
                                                        </label>--%>
                                                        <div ng-show="!showFieldsAgency">
                                                            <button ng-cloak name="NovoRegistro" ng-click="event.preventDefault(); newAgency();" class="btn btn-info btn-xs pull-right" type="button"
                                                                style="margin-right: 0px; margin-top: -31px; margin-bottom: 5px;" title="{{'15.TOOLTIP_NovoRegistro' | translate }}">
                                                                <i class="icon-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-5 col-sm-5" ng-class="{'has-error': form.base.alternateitemidentificationid.$invalid && form.base.alternateitemidentificationid.submitted}" ng-show="showFieldsAgency">
                                                        <label for="alternateitemidentificationid" translate="15.LBL_NumeroIdentificacaoAlternativa">Código da Agência Reguladora / Código Interno </label>
                                                        <input type="text" class="form-control input-sm" maxlength="13" id="alternateitemidentificationid" name="alternateitemidentificationid"
                                                            ng-model="itemAgencia.alternateitemidentificationid" ng-required="isRequiredAgencia"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_NumeroIdentificacaoAlternativa' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-5 col-sm-5" ng-class="{'has-error': form.base.alternateitemidentificationagency.$invalid && form.base.alternateitemidentificationid.submitted}" ng-show="showFieldsAgency">
                                                        <label for="alternateitemidentificationagency" translate="15.LBL_CodAgenciaControla">Agência Reguladora / Código Interno </label>
                                                        <select class="form-control input-sm" id="alternateitemidentificationagency" name="alternateitemidentificationagency" ng-required="isRequiredAgencia" 
                                                            ng-model="itemAgencia.alternateitemidentificationagency" ng-options="agencia.codigo as agencia.nome for agencia in agencias" 
                                                            data-toggle="popover" data-container="body" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_CodAgenciaControla' | translate}}">
                                                        </select>    
                                                    </div>
                                                    <div class="form-group col-md-2 col-sm-2" style="margin-top: 28px;" ng-show="showFieldsAgency">
                                                        <button ng-cloak name="salvar" ng-click="event.preventDefault(); onSaveAgenciaReguladora(itemAgencia);" class="btn btn-info btn-xs" type="button">
                                                            <i class="icon-save"></i>
                                                        </button>
                                                        <button ng-click="event.preventDefault(); cancelarAgencia();" name="atualizar" class="btn btn-danger btn-xs" type="button">
                                                            <i class="icon-remove"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="table-agencias" class="table-responsive">
                                                            <table ng-table="tableAgencias" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                                                <thead>
                                                                    <tr>
                                                                        <th ng-click="tableAgencias.sorting('alternateItemIdentificationId', tableAgencias.isSortBy('alternateItemIdentificationId', 'asc') ? 'desc' : 'asc')">
                                                                                <span translate="15.GRID_NumeroIdentificacaoAlternativa">Cód. Id. Alternativa / Cód. Interno</span>&nbsp;&nbsp;
                                                                                <i class="text-center icon-sort" ng-class="{
                                                                                    'icon-sort-up': tableAgencias.isSortBy('alternateItemIdentificationId', 'asc'),
                                                                                    'icon-sort-down': tableAgencias.isSortBy('alternateItemIdentificationId', 'desc')
                                                                                }"></i>
                                                                        </th>
                                                                        <th ng-click="tableAgencias.sorting('nome', tableAgencias.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                                                                <span translate="15.GRID_AgenciaRegistro">Agência Reguladora / Cód. Interno</span>&nbsp;&nbsp;
                                                                                <i class="text-center icon-sort" ng-class="{
                                                                                    'icon-sort-up': tableAgencias.isSortBy('nome', 'asc'),
                                                                                    'icon-sort-down': tableAgencias.isSortBy('nome', 'desc')
                                                                                }"></i>
                                                                        </th>
                                                                        <th>
                                                                            <span translate="15.GRID_Selecione">Selecione</span>&nbsp;&nbsp;
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="item1 in listAgencias" ng-click="onEditAgencia(item1)">
                                                                        <td ng-style="{ 'width': '40%' }" sortable="'alternateItemIdentificationId'">{{item1.alternateitemidentificationid}}</td>
                                                                        <td ng-style="{ 'width': '50%' }" sortable="'nome'">{{item1.nomeagencia}}</td>
                                                                        <td ng-style="{ 'width': '10%' }" class="action-buttons center">
                                                                            <a class="red" href="" ng-click="$event.stopPropagation(); removeAgencia(item1);"><i class="icon-trash bigger-130" title="{{ '15.TOOLTIP_Excluir' | translate }}" style="cursor:pointer; text-decoration:none;"></i></a>&nbsp;&nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr ng-show="listAgencias.length == 0">
                                                                        <td colspan="4" class="text-center" translate="15.LBL_NenhumRegistroEncontradoAgencia">Nenhum Número de Identificação Alternativa inserido</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                     </div>
                                                </div>
                                                <!--Fim Agências Reguladoras-->
                                                <div class="row" ng-hide="escondeCamposGTIN14">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.codigolingua.$invalid && form.submitted}">
                                                        <label for="lingua" translate="15.LBL_Lingua">Língua </label>
                                                        <select class="form-control input-sm" id="codigolingua" name="codigolingua" ng-model="itemForm.codigolingua" ng-options="lingua.codigo as lingua.nome for lingua in linguas"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Lingua' | translate}}">
                                                             <option value=""></option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.countrycode.$invalid && form.submitted}">
                                                        <label for="countrycode" translate="15.LBL_PaisMercadoDestino">País/Mercado de Destino</label> (*) 
                                                        <select class="form-control input-sm" id="countrycode" name="countrycode" ng-model="itemForm.countrycode"
                                                            ng-required="!escondeCamposGTIN14"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_PaisMercadoDestino' | translate}}"> 
                                                            
                                                            <option ng-repeat="mercado in paises" value="{{mercado.codigopais}}" ng-selected="itemForm.countrycode == mercado.codigopais">{{mercado.pais}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row" ng-hide="escondeCamposGTIN14">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.tradeitemcountryoforigin.$invalid && form.submitted}">
                                                        <label for="tradeitemcountryoforigin" translate="15.LBL_PaisOrigem">País de Origem </label>
                                                        <select class="form-control input-sm" id="tradeitemcountryoforigin" name="tradeitemcountryoforigin" ng-model="itemForm.tradeitemcountryoforigin"
                                                            ng-change="onSelectedCountry(itemForm.tradeitemcountryoforigin)" ng-required="!escondeCamposGTIN14"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_PaisOrigem' | translate}}">
                                                            
                                                            <option ng-repeat="pais in paises" value="{{pais.codigopais}}" ng-selected="itemForm.tradeitemcountryoforigin == pais.codigopais">{{pais.pais}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.estado.$invalid && form.submitted}">
                                                        <label for="estado" translate="15.LBL_Estado">Estado</label>
                                                        <select class="form-control input-sm" id="estado" name="estado" ng-model="itemForm.estado" ng-disabled="!searchMode && (estados == undefined || estados == '')"
                                                            ng-class="{'fieldDisabled': !searchMode && (estados == undefined || estados == '')}"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Estado' | translate}}">
                                                            <option value=""></option>
                                                            <option ng-repeat="estado in estados" value="{{estado.uf}}" ng-selected="itemForm.estado == estado.uf">{{estado.uf}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.base.observacoes.$invalid && form.submitted}">
                                                        <label for="observacoes" translate="15.LBL_Observacao">Observação</label>
                                                        <textarea class="form-control input-sm" id="observacoes" sbx-maxlength="1000" name="observacoes"rows="3" ng-model="itemForm.observacoes" style="resize: vertical;"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Observacao' | translate}}"></textarea>
                                                        <span style="font-size: 80%;">(<span translate="15.LBL_TamanhoMaximo">Tamanho máximo</span>: 1000)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <span translate="15.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.base.observacoes.$viewValue.length || 0 }}</span>
                                                    </div>
                                                </div>
                                                <h3 class="header smaller lighter purple" translate="15.LBL_Impostos">
                                                    Impostos
                                                </h3>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.ipiperc.$invalid && form.submitted}">
                                                        <label for="ipiperc" data-toggle="popover" translate="15.LBL_AliquotaIPI">Alíquota de Impostos IPI </label>
                                                        <input type="text" class="form-control input-sm" maxlength="6" id="ipiperc" name="ipiperc" ng-model="itemForm.ipiperc" ui-number-mask="2"
                                                        data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_AliquotaIPI' | translate}}"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.indicadorcompartilhadados.$invalid && form.submitted}">
                                                        <input type="checkbox" class="ace" name="indicadorcompartilhadados" ng-checked="initMode || itemForm.indicadorcompartilhadados == 1" ng-model="itemForm.indicadorcompartilhadados" ng-click="alterarIndicador(itemForm.indicadorcompartilhadados || false)" data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_CompartilhaDados' | translate}}">
                                                        <label class="lbl">&nbsp;
                                                            <span translate="15.LBL_CompartilhaDados">Compartilha dados</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions center" ng-show="editMode">
                                                <button name="voltar" class="btn btn-info btn-sm fieldDisabled" type="button" ng-disabled="true">
                                                    <i class="icon-backward bigger-110"></i><span translate="15.BTN_Voltar">Voltar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" name="avancar" ng-click="irParaAba(2)" type="button">
                                                    <i class="icon-forward bigger-110"></i><span translate="15.BTN_Avancar">Avançar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" ng-click="onSave(itemForm, 'Salvar')" type="button" ng-disabled="itemCompare.codigostatusgtin == 7"
                                                    ng-class="{'fieldDisabled': itemCompare.codigostatusgtin == 7}">
                                                    <i class="icon-save bigger-110"></i><span translate="15.BTN_Salvar">Salvar</span>
                                                </button>
                                                <button ng-if="!searchMode" id="Button2" name="gerar" ng-click="$event.stopPropagation(); gerarGTIN(itemForm.codigotipogtin)" class="btn btn-warning btn-sm" type="button"
                                                    ng-class="{'fieldDisabled': editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''}"
                                                    ng-disabled="editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''">
                                                    <i class="icon-gear bigger-110"></i><span translate="15.BTN_GerarGTIN">Gerar GTIN</span>
                                                </button>
                                                <button id="Button1" name="cancel" ng-click="onCancel(itemForm)" class="btn btn-danger btn-sm" type="button">
                                                    <i class="icon-remove bigger-110"></i><span translate="15.BTN_Cancelar">Cancelar</span>
                                                </button>
                                            </div>
                                        </form>
									</div>

                                    <div class="step-pane" id="step2" ng-class="{'active': stepCalculation == 2}">
										<form name="form.hierarquia" id="hierarquia" novalidate class="form" role="form">
                                            <div style="min-height:554px; overflow-x: hidden; overflow-y: auto; clear: both;">

                                                <h3 class="header smaller lighter purple" translate="15.LBL_Hierarquia">
                                                    Informações Logísticas
                                                </h3>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group" ng-class="{'has-error': form.complementar.codigotipoproduto.$invalid && form.submitted}">
                                                            <label for="codigotipoproduto">Tipo de Produto <span ng-show='!searchMode && isRequiredGDSN'>(*)</span></label>
                                                            <select class="form-control input-sm" id="Select12" name="codigotipoproduto" ng-model="itemForm.codigotipoproduto" ng-required="isRequiredGDSN" ng-change="showHideTipoProduto(itemForm.codigotipoproduto)"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="Selecionar o tipo de produto.">
                                                                <option value=""></option>
                                                                <option ng-repeat="tipoProduto in tiposProdutos" value="{{tipoProduto.codigo}}" ng-selected="itemForm.codigotipoproduto == tipoProduto.codigo">{{tipoProduto.nome}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h3 class="header smaller lighter purple" translate="15.LBL_Medidas">
                                                            Medidas 
                                                        </h3>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.height.$invalid && form.submitted}">

                                                            <label for="height" data-toggle="popover" data-trigger="hover" data-content="{{'t.15.LBL_Altura' | translate}}"><span translate="15.LBL_Altura">Altura</span> <span ng-show='!searchMode && ((tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4)'>(*)</span></label> <%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            <input type="text" class="form-control input-sm" maxlength="6" id="Text1" name="height" ng-model="itemForm.height" ui-number-mask="2" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"/> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.heightmeasurementunitcode.$invalid && form.submitted}">
                                                            <label for="heightmeasurementunitcode">
                                                                <span translate="15.LBL_Altura">Altura</span> - <span translate="15.LBL_UnidadeMedida">Unidade de Medida </span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span><%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <select class="form-control input-sm" id="Select3" name="heightmeasurementunitcode" ng-model="itemForm.heightmeasurementunitcode" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                                <option ng-repeat="unidadecomprimento in unidadescomprimento" value="{{unidadecomprimento.commoncode}}" ng-selected="itemForm.heightmeasurementunitcode == unidadecomprimento.commoncode">{{unidadecomprimento.sigla}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.width.$invalid && form.submitted}">
                                                            <label for="width">
                                                                <span translate="15.LBL_Largura">Largura</span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span><%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <input type="text" class="form-control input-sm" maxlength="6" id="Text6" name="width" ng-model="itemForm.width" ui-number-mask="2" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"/> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.widthmeasurementunitcode.$invalid && form.submitted}">
                                                            <label for="widthmeasurementunitcode">
                                                                <span translate="15.LBL_Largura">Largura</span> - <span translate="15.LBL_UnidadeMedida">Unidade de Medida </span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span><%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <select class="form-control input-sm" id="Select6" name="widthmeasurementunitcode" ng-model="itemForm.widthmeasurementunitcode" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4">
                                                                <option ng-repeat="unidadecomprimento in unidadescomprimento" value="{{unidadecomprimento.commoncode}}" ng-selected="itemForm.widthmeasurementunitcode == unidadecomprimento.commoncode">{{unidadecomprimento.sigla}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.depth.$invalid && form.submitted}">
                                                            <label for="depth">
                                                                <span translate="15.LBL_Profundidade">Profundidade</span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span><%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <input type="text" class="form-control input-sm" maxlength="6" id="Text4" name="depth" ng-model="itemForm.depth" ui-number-mask="2" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"/> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.depthmeasurementunitcode.$invalid && form.submitted}">
                                                            <label for="depthmeasurementunitcode">
                                                                <span translate="15.LBL_Profundidade">Profundidade</span> - <span translate="15.LBL_UnidadeMedida">Unidade de Medida </span> 
                                                                <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span>
                                                            </label>

                                                            <select class="form-control input-sm" id="Select5" name="depthmeasurementunitcode" ng-model="itemForm.depthmeasurementunitcode" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                                <option ng-repeat="unidadecomprimento in unidadescomprimento" value="{{unidadecomprimento.commoncode}}" ng-selected="itemForm.depthmeasurementunitcode == unidadecomprimento.commoncode">{{unidadecomprimento.sigla}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.netcontent.$invalid && form.submitted}">
                                                            <label for="netcontent"  >
                                                                <span translate="15.LBL_ConteudoLiquido">Conteúdo Líquido</span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span><%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <input type="text" class="form-control input-sm" maxlength="6" id="Text3" name="netcontent" ng-model="itemForm.netcontent" ui-number-mask="2" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"/> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.netcontentmeasurementunitcode.$invalid && form.submitted}">
                                                            <label for="netcontentmeasurementunitcode">
                                                                <span translate="15.LBL_ConteudoLiquido">Conteúdo Líquido</span> - <span translate="15.LBL_UnidadeMedida">Unidade de Medida</span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span><%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>
                                                            <select class="form-control input-sm" id="Select4" name="netcontentmeasurementunitcode"
                                                                ng-model="itemForm.netcontentmeasurementunitcode" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_UnidadeMedida' | translate}}">
                                                                
                                                                <option ng-repeat="unidadevolume in unidadesvolume" value="{{unidadevolume.commoncode}}" ng-selected="itemForm.netcontentmeasurementunitcode == unidadevolume.commoncode">{{unidadevolume.sigla}}</option>
                                                            </select> 
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.grossweight.$invalid && form.submitted}">
                                                            <label for="grossweight">
                                                                <span translate="15.LBL_PesoBruto">Peso Bruto</span> (*)<%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <input type="text" class="form-control input-sm" maxlength="6" id="Text7" name="grossweight" ng-model="itemForm.grossweight" ui-number-mask="2" ng-required="true"/> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.grossweightmeasurementunitcode.$invalid && form.submitted}">
                                                            <label for="grossweightmeasurementunitcode">
                                                                <span translate="15.LBL_PesoBruto">Peso Bruto</span> - <span translate="15.LBL_UnidadeMedida">Unidade de Medida</span> (*)<%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <select class="form-control input-sm" id="Select7" name="grossweightmeasurementunitcode" ng-model="itemForm.grossweightmeasurementunitcode" ng-required="true"> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                                <option ng-repeat="unidadepeso in unidadespeso" value="{{unidadepeso.commoncode}}" ng-selected="itemForm.grossweightmeasurementunitcode == unidadepeso.commoncode">{{unidadepeso.sigla}}</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.netweight.$invalid && form.submitted}">
                                                            <label for="netweight">
                                                                <span translate="15.LBL_PesoLiquido">Peso Líquido </span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span><%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <input type="text" class="form-control input-sm" maxlength="6" id="Text8" name="netweight" ng-model="itemForm.netweight" ui-number-mask="2" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"/> <%--ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.netweightmeasurementunitcode.$invalid && form.submitted}">
                                                            <label for="netweightmeasurementunitcode">
                                                                <span translate="15.LBL_PesoLiquido">Peso Líquido</span> - <span translate="15.LBL_UnidadeMedida">Unidade de Medida</span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span><%--<span ng-show='!searchMode && (isRequiredGDSN || isRequiredCNP)'>(*)</span>--%>
                                                            </label>

                                                            <select class="form-control input-sm" id="Select8" name="netweightmeasurementunitcode" ng-model="itemForm.netweightmeasurementunitcode" ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4">
                                                                <option ng-repeat="unidadepeso in unidadespeso" value="{{unidadepeso.commoncode}}" ng-selected="itemForm.netweightmeasurementunitcode == unidadepeso.commoncode">{{unidadepeso.sigla}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.minimumtradeitemlifespanfromtimeofproduction.$invalid && form.submitted}">
                                                            <label for="minimumtradeitemlifespanfromtimeofproduction">
                                                                <span translate="15.LBL_TempoMinimoUtilProdProducao">Tempo mínimo (dias) de vida útil do produto após produção</span> <span ng-show='!searchMode && (tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4'>(*)</span>
                                                            </label>
                                                            
                                                            <input type="text" class="form-control input-sm" maxlength="9" id="minimumtradeitemlifespanfromtimeofproduction" name="minimumtradeitemlifespanfromtimeofproduction" integer
                                                                ng-model="itemForm.minimumtradeitemlifespanfromtimeofproduction"
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_TempoMinimoUtilProdProducao' | translate}}"
                                                                ng-required="(tipoprodutoselecionado[0].campoobrigatorio == 1 || tipoprodutoselecionado[0].campoobrigatorio == true) && itemForm.codigotipogtin != 4"/><%-- ng-required="isRequiredGDSN || isRequiredCNP"--%>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.complementar.pallettypecode.$invalid && form.submitted}" ng-show="showPalletDependency">
                                                            <label for="pallettypecode">Tipo de Pallet </label>
                                                            <select class="form-control input-sm" id="Select13" name="pallettypecode" ng-model="itemForm.pallettypecode"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="Selecionar o tipo de pallet.">
                                                                <option ng-repeat="pallet in pallettypecodes" value="{{pallet.code}}" ng-selected="itemForm.pallettypecode == pallet.code">{{pallet.codedescription}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.stackingfactor.$invalid && form.submitted}" ng-show="showPalletDependency">
                                                            <label for="stackingfactor">Fator de Empilhamento </label>
                                                            <input type="text" class="form-control input-sm" maxlength="9" id="Text13" name="stackingfactor" ng-model="itemForm.stackingfactor" integer
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="Informar o fator de empilhamento. Determina o máximo de empilhamento do produto quando em armazenamento. Indica o número de níveis que o produto pode ser empilhado."/>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.quantityoflayersperpallet.$invalid && form.submitted}" ng-show="showPalletDependency">
                                                            <label for="quantityoflayersperpallet">Quantidade de Camadas por Pallet <span ng-show='!searchMode && isRequiredGDSN'>(*)</span></label>
                                                            <input type="text" class="form-control input-sm" maxlength="9" id="Text14" name="quantityoflayersperpallet" ng-model="itemForm.quantityoflayersperpallet" 
                                                                ng-required="isRequiredGDSN" integer
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="Infomar a quantidade de camadas por pallet, caso a hierarquia NÃO conter um número GTIN em nível de pallet."/>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.quantityoftradeitemsperpalletlayer.$invalid && form.submitted}" ng-show="showPalletDependency">
                                                            <label for="quantityoftradeitemsperpalletlayer">Quantidade de Itens Comerciais em uma Única Camada <span ng-show='!searchMode && isRequiredGDSN'>(*)</span></label>
                                                            <input type="text" class="form-control input-sm" maxlength="9" id="Text15" name="quantityoftradeitemsperpalletlayer" ng-model="itemForm.quantityoftradeitemsperpalletlayer" 
                                                                ng-required="isRequiredGDSN" integer
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="Informar o número de itens comerciais contidos em uma única camada de um palete. Somente usado se o palete não tiver GTIN. Indica o número de itens comerciais colocados sobre uma camada de paletes de acordo com as preferências do fornecedor ou varejista."/>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.quantityoftradeitemcontainedinacompletelayer.$invalid && form.submitted}" ng-show="showPalletDependency">
                                                            <label for="quantityoftradeitemcontainedinacompletelayer">Quantidade de Itens Comerciais uma Camada Completa <span ng-show='!searchMode && isRequiredGDSN'>(*)</span></label>
                                                            <input type="text" class="form-control input-sm" maxlength="9" id="Text16" name="quantityoftradeitemcontainedinacompletelayer" ng-model="itemForm.quantityoftradeitemcontainedinacompletelayer" 
                                                                ng-required="isRequiredGDSN" integer 
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="Informar a quantidade de itens comerciais em uma camada completa. Necessário se uma hierarquia contém um GTIN representando um pallet."/>
                                                        </div>
                                                        <div class="form-group" ng-class="{'has-error': form.hierarquia.quantityofcompletelayerscontainedinatradeitem.$invalid && form.submitted}" ng-show="showPalletDependency">
                                                            <label for="quantityofcompletelayerscontainedinatradeitem">Quantidade de Camadas Completas em Item Comercial <span ng-show='!searchMode && isRequiredGDSN'>(*)</span></label>
                                                            <input type="text" class="form-control input-sm" maxlength="9" id="Text17" name="quantityofcompletelayerscontainedinatradeitem" 
                                                                ng-required="isRequiredGDSN" ng-model="itemForm.quantityofcompletelayerscontainedinatradeitem" integer
                                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="Informar a quantidade de camadas completas em item comercial. Necessário se uma hierarquia contém um GTIN representando um pallet."/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-offset-1 col-md-4">
                                                        <div id="dvContainerImgProduct" class="ImgProduct" ng-show="itemForm.codigotipoproduto != undefined && itemForm.codigotipoproduto != ''">
                                                            <img src="{{tipoprodutoselecionado[0].nomeimagem}}" ng-show="tipoprodutoselecionado[0].nomeimagem != undefined && tipoprodutoselecionado[0].nomeimagem != ''" style="margin-top: 15px;"/>
                                                            <img src="/images/no-photo.jpg" ng-show="tipoprodutoselecionado[0].nomeimagem == undefined || tipoprodutoselecionado[0].nomeimagem == ''" style="margin-top: 15px;"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" ng-show="itemForm.codigotipogtin == 3">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.hierarquia.gtininferior.$invalid && form.submitted}">
                                                        <div class="form-group col-md-12 col-xs-12" style="margin-left: -15px;">
                                                            <label for="" data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_QtdeCamadaCompletaItemComercial' | translate}}" translate="15.LBL_GTINInferior">GTIN Inferior</label>
                                                        </div>
                                                        <div class="form-group col-md-1 col-xs-1" style="margin-top: -15px; margin-left: -15px;" ng-if="!searchMode" style="margin-left: -15px;">
                                                            <button class="btn btn-info btn-xs" ng-click="onGtinInferior(itemForm)" title="{{ '15.TOOLTIP_PesquisarGTINInferior' | translate }}"
                                                                ng-class="{'fieldDisabled': itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != '' && itemForm.globaltradeitemnumber != null}"
                                                                ng-disabled="itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != '' && itemForm.globaltradeitemnumber != null">
                                                                <i class="icon-cogs"></i> <span translate="15.LBL_GTINInferior">GTIN Inferior</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div ng-show="itemForm.codigotipogtin == 3">
                                                    <div class="table-responsive">
                                                        <table ng-table="tableGTINInferior" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && listGTINInferior.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || listGTINInferior.length == 0}">
                                                            <thead>
                                                                <tr>
                                                                    <th ng-click="tableGTINInferior.sorting('globaltradeitemnumber', tableGTINInferior.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                                                      <span translate="15.GRID_GTIN">GTIN</span>&nbsp;&nbsp;
                                                                      <i class="text-center icon-sort" ng-class="{
                                                                        'icon-sort-up': tableGTINInferior.isSortBy('globaltradeitemnumber', 'asc'),
                                                                        'icon-sort-down': tableGTINInferior.isSortBy('globaltradeitemnumber', 'desc')
                                                                      }"></i>
                                                                    </th>
                                                                    <th ng-click="tableGTINInferior.sorting('productdescription', tableGTINInferior.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                                                      <span translate="15.GRID_DescricaoProduto">Descrição do Produto</span>&nbsp;&nbsp;
                                                                      <i class="text-center icon-sort" ng-class="{
                                                                        'icon-sort-up': tableGTINInferior.isSortBy('productdescription', 'asc'),
                                                                        'icon-sort-down': tableGTINInferior.isSortBy('productdescription', 'desc')
                                                                      }"></i>
                                                                    </th>
                                                                    <th ng-click="tableGTINInferior.sorting('quantidade', tableGTINInferior.isSortBy('quantidade', 'asc') ? 'desc' : 'asc')">
                                                                      <span translate="15.LBL_Quantidade">Quantidade</span>&nbsp;&nbsp;
                                                                      <i class="text-center icon-sort" ng-class="{
                                                                        'icon-sort-up': tableGTINInferior.isSortBy('quantidade', 'asc'),
                                                                        'icon-sort-down': tableGTINInferior.isSortBy('quantidade', 'desc')
                                                                      }"></i>
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr ng-repeat="item1 in listGTINInferior | filter:item" ng-show="item1.statusinferior != 'excluido' && item1.statusinferir != 'novo-e-excluido'">
                                                                    <td ng-style="{ 'width': '7%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                                                    <td ng-style="{ 'width': '25%' }" sortable="'nomeprodutoinferior'">{{item1.productdescription}}</td>
                                                                    <td ng-style="{ 'width': '23%' }" sortable="'quantidade'">{{item1.quantidade}}</td>
                                                                </tr>
                                                                <tr ng-show="listGTINInferior.length == 0">
                                                                    <td colspan="7" class="text-center" translate="15.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions center">
                                                <button name="voltar" class="btn btn-info btn-sm" type="button" ng-click="voltarParaAba(1)">
                                                    <i class="icon-backward bigger-110"></i><span translate="15.BTN_Voltar">Voltar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" name="avancar" ng-click="irParaAba(3)" type="button">
                                                    <i class="icon-forward bigger-110"></i><span translate="15.BTN_Avancar">Avançar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" ng-click="onSave(itemForm, 'Salvar')" type="button" ng-disabled="itemCompare.codigostatusgtin == 7"
                                                    ng-class="{'fieldDisabled': itemCompare.codigostatusgtin == 7}">
                                                    <i class="icon-save bigger-110"></i><span translate="15.BTN_Salvar">Salvar</span>
                                                </button>
                                                <button ng-if="!searchMode" id="Button5" name="gerar" ng-click="$event.stopPropagation(); gerarGTIN(itemForm.codigotipogtin)" class="btn btn-warning btn-sm" type="button"
                                                    ng-class="{'fieldDisabled': editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''}"
                                                    ng-disabled="editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''">
                                                    <i class="icon-gear bigger-110"></i><span translate="15.BTN_GerarGTIN">Gerar GTIN</span>
                                                </button>
                                                <button name="cancel" ng-click="onCancel(itemForm)" class="btn btn-danger btn-sm" type="button">
                                                    <i class="icon-remove bigger-110"></i><span translate="15.BTN_Cancelar">Cancelar</span>
                                                </button>
                                            </div>
                                        </form>
									</div>

									<div class="step-pane" id="step3" ng-class="{'active': stepCalculation == 3}">
										<form name="form.midia" id="midia" novalidate class="form" role="form">
                                            <div style="min-height:410px; overflow-x: hidden; overflow-y: auto; clear: both;">

                                                <h3 class="header smaller lighter purple" translate="15.LBL_URL">
                                                    URL
                                                </h3>
                                                <div class="row">
                                                    <div class="form-group col-md-3 col-sm-6" ng-class="{'has-error': form.midia.nome.$invalid && form.midia.nome.submitted}">
                                                        <label for="nome" translate="15.LBL_Nome">Nome </label>
                                                        <input type="text" class="form-control input-sm" maxlength="255" id="Text10" name="nome" ng-model="itemUrl.nome" ng-required="isRequiredURL" autocomplete="off" 
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Nome' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-3 col-sm-6" ng-class="{'has-error': form.midia.nomeURL.$invalid && form.midia.nomeURL.submitted}">
                                                        <label for="nome" translate="15.LBL_URL">URL </label>
                                                        <input type="text" class="form-control input-sm" maxlength="255" id="nomeURL" name="nomeURL" ng-model="itemUrl.url" ng-required="isRequiredURL" autocomplete="off" 
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_URL' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-3 col-sm-6" ng-class="{'has-error': form.midia.tipoURL.$invalid && form.midia.tipoURL.submitted}">
                                                        <label for="tiposurl" translate="15.LBL_TipoUrl">Tipo de URL </label>
                                                        <select class="form-control input-sm" id="tipoURL" name="tipoURL" ng-model="itemUrl.codigotipourl" ng-required="isRequiredURL" 
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_TipoUrl' | translate}}">
                                                            <option ng-repeat="tipourl in tiposurl" value="{{tipourl.codigo}}" ng-selected="tipourl.codigo == itemUrl.codigotipourl">{{tipourl.nome}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3 col-sm-6" style="margin-top: 28px;">
                                                        <button ng-cloak ng-show="insertURLMode" name="salvar" ng-click="event.preventDefault(); onSaveURL(itemUrl);" class="btn btn-info btn-xs" type="button" 
                                                            ng-disabled="itemCompare.codigostatusgtin == 7"
                                                            ng-class="{'fieldDisabled': itemCompare.codigostatusgtin == 7}">
                                                            <i class="icon-save"></i> <%--Salvar URL--%>
                                                        </button>
                                                        <button ng-cloak ng-show="!insertURLMode" ng-click="event.preventDefault(); atualizarURL(itemUrl);" name="atualizar" class="btn btn-info btn-xs" type="button" 
                                                        ng-disabled="itemCompare.codigostatusgtin == 7" ng-class="{'fieldDisabled': itemCompare.codigostatusgtin == 7}">
                                                            <i class="icon-refresh"></i><%--Atualizar URL--%>
                                                        </button>
                                                        <button ng-click="event.preventDefault(); cancelarURL();" name="atualizar" class="btn btn-danger btn-xs" type="button">
                                                            <i class="icon-remove"></i><%--Cancelar URL--%>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="table-responsive">
                                                    <table ng-table="tableProdutoURL" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                                        <thead>
                                                            <tr>
                                                                <th ng-click="tableProdutoURL.sorting('codigo', tableProdutoURL.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                                                  <span>#</span>&nbsp;&nbsp;
                                                                  <i class="text-center icon-sort" ng-class="{
                                                                    'icon-sort-up': tableProdutoURL.isSortBy('codigo', 'asc'),
                                                                    'icon-sort-down': tableProdutoURL.isSortBy('codigo', 'desc')
                                                                  }"></i>
                                                                </th>
                                                                <th ng-click="tableProdutoURL.sorting('nome', tableProdutoURL.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                                                  <span translate="15.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                                                  <i class="text-center icon-sort" ng-class="{
                                                                    'icon-sort-up': tableProdutoURL.isSortBy('nome', 'asc'),
                                                                    'icon-sort-down': tableProdutoURL.isSortBy('nome', 'desc')
                                                                  }"></i>
                                                                </th>
                                                                <th ng-click="tableProdutoURL.sorting('url', tableProdutoURL.isSortBy('url', 'asc') ? 'desc' : 'asc')">
                                                                  <span translate="15.GRID_URL">Url</span>&nbsp;&nbsp;
                                                                  <i class="text-center icon-sort" ng-class="{
                                                                    'icon-sort-up': tableProdutoURL.isSortBy('url', 'asc'),
                                                                    'icon-sort-down': tableProdutoURL.isSortBy('url', 'desc')
                                                                  }"></i>
                                                                </th>
                                                                <th ng-click="tableProdutoURL.sorting('codigotipourl', tableProdutoURL.isSortBy('codigotipourl', 'asc') ? 'desc' : 'asc')">
                                                                  <span translate="15.GRID_TipoURL">Tipo de URL</span>&nbsp;&nbsp;
                                                                  <i class="text-center icon-sort" ng-class="{
                                                                    'icon-sort-up': tableProdutoURL.isSortBy('codigotipourl', 'asc'),
                                                                    'icon-sort-down': tableProdutoURL.isSortBy('codigotipourl', 'desc')
                                                                  }"></i>
                                                                </th>
                                                                <th><span translate="15.GRID_Acao">Ação</span></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr ng-repeat="item1 in listURL" ng-click="onAlterURL(item1)" ng-show="item1.status != 'excluido' && item1.status != 'excluido2'">
                                                                <td ng-style="{ 'width': '6%' }">{{item1.codigo}}</td>
                                                                <td ng-style="{ 'width': '25%' }">{{item1.nome}}</td>
                                                                <td ng-style="{ 'width': '30%' }">{{item1.url}}</td>
                                                                <td ng-style="{ 'width': '25%' }">{{item1.codigotipourl | tipourl}}</td>
                                                                <td ng-style="{ 'width': '10%' }" class="action-buttons center">
                                                                    <a class="red" href="" ng-click="$event.stopPropagation(); removeURL(item1);"><i class="icon-trash bigger-130" title="{{ '15.TOOLTIP_Excluir' | translate }}" style="cursor:pointer; text-decoration:none;"></i></a>&nbsp;&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr ng-show="listURL.length == 0">
                                                                <td colspan="7" class="text-center" translate="15.LBL_NenhumRegistroEncontradoURL">Nenhuma URL inserida</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <h3 class="header smaller lighter purple">
                                                    <span translate="15.LBL_Fotos">Fotos</span> (*)
                                                </h3>
                                                <div class="row">
                                                    <div class="form-group col-md-12 col-xs-12">
                                                        <div class="widget-main" style="width: 420px;">
                                                            <input multiple="" type="file" name="fileToUpload" id="inputImagemProduto" fileread="nomeimagem" onchange="angular.element(this).scope().alterImage()"/>
                                                            <i class="icon-remove pull-right" ng-show="imagensSelecionadas" ng-click="limparUploadFiles()" style="cursor: pointer; margin-top: -5.5%;" title="{{ '15.TOOLTIP_RemoverSelecao' | translate }}"></i>
                                                            <span class="red" ng-show="!imagensSelecionadas && form.submitted">Campo Obrigatório</span>
                                                        </div>
                                                        <div ng-show="true && permiteVideo>=1">
                                                            <label for="login" translate="15.LBL_Video">
                                                                Vídeo
                                                            </label>
                                                            <div ng-class="{'has-error': form.imagens.video.$viewValue != '' && form.imagens.video.$viewValue.indexOf('youtube') < 0 && formEnviarFotos.submitted}">
                                                                <span class="input-icon col-md-12 col-xs-12">
                                                                    <input type="text" class="form-control input-sm" id="video" maxlength="100" name="video"
                                                                        placeholder="{{ '15.PLACEHOLDER_InformeURL' | translate }}" ng-model="video.src" />
                                                                    <i class="icon-youtube red bigger-120" style="padding-left: 15px; font-size: 21px;"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <br />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="row text-center">
                                                        <ul class="draglist ace-thumbnails">
                                                            <li ng-show="imagem.url != undefined && imagem.url != null && imagem.url != ''" ng-repeat="imagem in imagens">
                                                                <img height="100px" width="120px" ng-src="{{imagem.url}}"/></span>
                                                                <div class="tools tools-bottom" ng-show="imagem.url != 'images/no-photo.jpg' && imagem.url != 'images/no-video.jpg'">
                                                                    <a href="" ng-click="ExcluirImagem(imagem)">
                                                                        <i class="icon-remove red"></i>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions center">
                                                <button name="voltar" class="btn btn-info btn-sm" type="button" ng-click="voltarParaAba(2)">
                                                    <i class="icon-backward bigger-110"></i><span translate="15.BTN_Voltar">Voltar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" name="avancar" ng-click="irParaAba(4)" type="button">
                                                    <i class="icon-forward bigger-110"></i><span translate="15.BTN_Avancar">Avançar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" ng-click="onSave(itemForm, 'Salvar')" type="button" ng-disabled="itemCompare.codigostatusgtin == 7" ng-class="{'fieldDisabled': itemCompare.codigostatusgtin == 7}">
                                                    <i class="icon-save bigger-110"></i><span translate="15.BTN_Salvar">Salvar</span>
                                                </button>
                                                <button ng-if="!searchMode" id="Button4" name="gerar" ng-click="$event.stopPropagation(); gerarGTIN(itemForm.codigotipogtin)" class="btn btn-warning btn-sm" type="button"
                                                    ng-class="{'fieldDisabled': editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''}"
                                                    ng-disabled="editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''">
                                                    <i class="icon-gear bigger-110"></i><span translate="15.BTN_GerarGTIN">Gerar GTIN</span>
                                                </button>
                                                <button name="cancel" ng-click="onCancel(itemForm)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                                    <i class="icon-remove bigger-110"></i><span translate="15.BTN_Cancelar">Cancelar</span>
                                                </button>
                                            </div>
                                        </form>
									</div>



                                    <div class="step-pane" id="step4" ng-class="{'active': stepCalculation == 4}">
                                        <form name="form.complementar" id="complementar" novalidate class="form" role="form">
                                            <div style="min-height:215px; overflow-x: hidden; overflow-y: auto; clear: both;">

                                                <h3 class="header smaller lighter purple" translate="15.LBL_Compra">
                                                    Compra
                                                </h3>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.istradeitemanorderableunit.$invalid && form.submitted}" ng-show="isRequiredGDSN">
                                                        <label for="istradeitemanorderableunit">
                                                            <span translate="15.LBL_IndicadorLiberadoCompra">Indicador de Item Comercial Liberado para Compra</span> <span ng-show='!searchMode && isRequiredGDSN'>(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="istradeitemanorderableunit" name="istradeitemanorderableunit" ng-model="itemForm.istradeitemanorderableunit" ng-required="isRequiredGDSN"
                                                            ng-options="item.codigo as item.nome for item in listaBoleana"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_IndicadorLiberadoCompra' | translate}}">
                                                            <option value=""></option>
                                                            </select>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.istradeitemadespatchunit.$invalid && form.submitted}" ng-show="isRequiredGDSN">
                                                        <label for="istradeitemadespatchunit">
                                                            <span translate="15.LBL_IndicadorUnidadeDespacho">Indicador de Unidade de Despacho</span> <span ng-show='!searchMode && isRequiredGDSN'>(*)
                                                        </label>
                                                        <select class="form-control input-sm" id="istradeitemadespatchunit" name="istradeitemadespatchunit" ng-model="itemForm.istradeitemadespatchunit" ng-required="isRequiredGDSN"
                                                            ng-options="item.codigo as item.nome for item in listaBoleana"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_IndicadorUnidadeDespacho' | translate}}">
                                                            <option value=""></option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.istradeitemabaseunit.$invalid && form.submitted}" ng-show="isRequiredGDSN">
                                                        <label for="istradeitemabaseunit">
                                                            <span translate="15.LBL_IndicadorBasicaVenda">Indicador de Unidade Básica de Venda</span> <span ng-show='!searchMode && isRequiredGDSN'>(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="istradeitemabaseunit" name="istradeitemabaseunit" ng-model="itemForm.istradeitemabaseunit" ng-required="isRequiredGDSN"
                                                            ng-options="item.codigo as item.nome for item in listaBoleana"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_IndicadorBasicaVenda' | translate}}">
                                                            <option value=""></option>
                                                            </select>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.istradeitemaninvoiceunit.$invalid && form.submitted}" ng-show="isRequiredGDSN">
                                                        <label for="istradeitemaninvoiceunit">
                                                            <span translate="15.LBL_IndicadorFaturamento">Indicador de Unidade de Faturamento</span> <span ng-show='!searchMode && isRequiredGDSN'>(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="istradeitemaninvoiceunit" name="istradeitemaninvoiceunit" ng-model="itemForm.istradeitemaninvoiceunit" ng-required="isRequiredGDSN"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_IndicadorFaturamento' | translate}}">
                                                            <option value=""></option>
                                                            <option value="1" ng-selected="itemForm.istradeitemaninvoiceunit == 1">Sim</option>
                                                            <option value="2" ng-selected="itemForm.istradeitemaninvoiceunit == 2">Não</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.istradeitemaconsumerunit.$invalid && form.submitted}" ng-show="isRequiredGDSN">
                                                        <label for="istradeitemaconsumerunit">
                                                            <span translate="15.LBL_IndicadorUnidadeConsumo">Indicador se Item Comercial é Unidade de Consumo</span> <span ng-show='!searchMode && isRequiredGDSN'>(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="istradeitemaconsumerunit" name="istradeitemaconsumerunit" ng-model="itemForm.istradeitemaconsumerunit" ng-required="isRequiredGDSN"
                                                            ng-options="item.codigo as item.nome for item in listaBoleana"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_IndicadorUnidadeConsumo' | translate}}">
                                                            <option value=""></option>
                                                            </select>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.istradeitemamodel.$invalid && form.submitted}">
                                                        <label for="istradeitemamodel" translate="15.LBL_IndicadorComercialModelo">Item Comercial é um modelo</label>
                                                        <select class="form-control input-sm" id="istradeitemamodel" name="istradeitemamodel" ng-model="itemForm.istradeitemamodel"
                                                            ng-options="item.codigo as item.nome for item in listaBoleana" 
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_IndicadorComercialModelo' | translate}}">
                                                            <option value=""></option>
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.ordersizingfactor.$invalid && form.submitted}">
                                                        <label for="ordersizingfactor" translate="15.LBL_UnidadeMedidaPedido">Unidade de Medida do Pedido </label>
                                                        <select class="form-control input-sm" id="ordersizingfactor" name="ordersizingfactor" ng-model="itemForm.ordersizingfactor"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-viewport="#complementar" data-content="{{'t.15.LBL_UnidadeMedidaPedido' | translate}}">
                                                            <option value=""></option>
                                                            <option ng-repeat="order in orders" value="{{order.codevalue}}" ng-selected="itemForm.ordersizingfactor == order.codevalue">{{order.name}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.orderquantitymultiple.$invalid && form.submitted}">
                                                        <label for="orderquantitymultiple" translate="15.LBL_MultiploQuantidadePedido">Múltiplo da Quantidade para Pedido </label>
                                                        <input type="text" class="form-control input-sm" maxlength="9" id="orderquantitymultiple" name="orderquantitymultiple" ng-model="itemForm.orderquantitymultiple" integer
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_MultiploQuantidadePedido' | translate}}"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.orderquantityminimum.$invalid && form.submitted}">
                                                        <label for="orderquantityminimum" translate="15.LBL_QuantidadeMinimaPedido">Quantidade Mínima para Pedido </label>
                                                        <input type="text" class="form-control input-sm" maxlength="9" id="orderquantityminimum" name="orderquantityminimum" ng-model="itemForm.orderquantityminimum" integer
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_QuantidadeMinimaPedido' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.packagingtypecode.$invalid && form.submitted}">
                                                        <label for="packagingtypecode" translate="15.LBL_TipoEmbalagem">Tipo da Embalagem </label>
                                                        <select class="form-control input-sm" id="packagingtypecode" name="packagingtypecode" ng-model="itemForm.packagingtypecode"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_TipoEmbalagem' | translate}}">
                                                            <option value=""></option>
                                                            <option ng-repeat="packaging in packagingtypecodes" value="{{packaging.codevalue}}" ng-selected="itemForm.packagingtypecode == packaging.codevalue">{{packaging.name}}</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <h3 class="header smaller lighter purple">
                                                    <span translate="15.LBL_Armazenamento">Armazenamento</span> / <span translate="15.LBL_Manuseio">Manuseio</span>
                                                </h3>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.deliverytodistributioncentertemperatureminimum.$invalid && form.submitted}">
                                                        <label for="deliverytodistributioncentertemperatureminimum">
                                                            <span translate="15.LBL_TemperaturaMinimaArmazenamento">Temperatura Mínima</span>
                                                            <span ng-show="!searchMode && itemForm.storagehandlingtempminimumuom != undefined && itemForm.storagehandlingtempminimumuom != null && itemForm.storagehandlingtempminimumuom != ''">(*)</span>
                                                        </label>
                                                        <input type="text" class="form-control input-sm" id="deliverytodistributioncentertemperatureminimum" name="deliverytodistributioncentertemperatureminimum"
                                                            ng-required="itemForm.storagehandlingtempminimumuom != undefined && itemForm.storagehandlingtempminimumuom != null && itemForm.storagehandlingtempminimumuom != ''"
                                                            ng-model="itemForm.deliverytodistributioncentertemperatureminimum" ui-number-mask="2" maxlength="{{tamanhoMinimum}}"
                                                            ng-keydown="avaliaMinimum(itemForm.deliverytodistributioncentertemperatureminimum)" ui-negative-number
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_TemperaturaMinimaArmazenamento' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.storagehandlingtempminimumuom.$invalid && form.submitted}">
                                                        <label for="storagehandlingtempminimumuom">
                                                            <span translate="15.LBL_UnidadeTemperaturaMinimaArmazenamento">Unidade de Medida da Temperatura Mínima</span>
                                                            <span ng-show="!searchMode && itemForm.deliverytodistributioncentertemperatureminimum != undefined && itemForm.deliverytodistributioncentertemperatureminimum != null && itemForm.deliverytodistributioncentertemperatureminimum != ''">(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="storagehandlingtempminimumuom" name="storagehandlingtempminimumuom" ng-model="itemForm.storagehandlingtempminimumuom"
                                                            ng-required="itemForm.deliverytodistributioncentertemperatureminimum != undefined && itemForm.deliverytodistributioncentertemperatureminimum != null && itemForm.deliverytodistributioncentertemperatureminimum != ''"
                                                            ng-change="selectSameMeasurementMinimum(itemForm.storagehandlingtempminimumuom, itemForm.storagehandlingtemperaturemaximum)"
                                                            data-toggle="popover" data-container="body" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_UnidadeTemperaturaMinimaArmazenamento' | translate}}">
                                                            <option value=""></option>
                                                            <option ng-repeat="unidadetemperatura in unidadestemperatura" value="{{unidadetemperatura.commoncode}}" ng-selected="itemForm.storagehandlingtempminimumuom == unidadetemperatura.commoncode">{{unidadetemperatura.sigla}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.storagehandlingtemperaturemaximum.$invalid && form.submitted}">
                                                        <label for="storagehandlingtemperaturemaximum">
                                                            <span translate="15.LBL_TemperaturaMaximaArmazenamento">Temperatura Máxima</span>
                                                            <span ng-show="!searchMode && itemForm.storagehandlingtemperaturemaximumunitofmeasure != undefined && itemForm.storagehandlingtemperaturemaximumunitofmeasure != null && itemForm.storagehandlingtemperaturemaximumunitofmeasure != ''">(*)</span>
                                                        </label>
                                                        <input type="text" class="form-control input-sm" id="storagehandlingtemperaturemaximum" name="storagehandlingtemperaturemaximum"
                                                            ng-required="itemForm.storagehandlingtemperaturemaximumunitofmeasure != undefined && itemForm.storagehandlingtemperaturemaximumunitofmeasure != null && itemForm.storagehandlingtemperaturemaximumunitofmeasure != ''"
                                                            ng-model="itemForm.storagehandlingtemperaturemaximum" ui-number-mask="2" maxlength="{{tamanhoMaximum}}"
                                                            ng-keydown="avaliaMaximum(itemForm.storagehandlingtemperaturemaximum)" ui-negative-number
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_TemperaturaMaximaArmazenamento' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.storagehandlingtemperaturemaximumunitofmeasure.$invalid && form.submitted}">
                                                        <label for="storagehandlingtemperaturemaximumunitofmeasure">
                                                            <span translate="15.LBL_UnidadeTemperaturaMaximaArmazenamento">Unidade de Medida da Temperatura Máxima</span>
                                                            <span ng-show="!searchMode && itemForm.storagehandlingtemperaturemaximum != undefined && itemForm.storagehandlingtemperaturemaximum != null && itemForm.storagehandlingtemperaturemaximum != ''">(*)</span>
                                                        </label>
                                                        <select class="form-control input-sm" id="Select9" name="storagehandlingtemperaturemaximumunitofmeasure" ng-model="itemForm.storagehandlingtemperaturemaximumunitofmeasure"
                                                            ng-required="itemForm.storagehandlingtemperaturemaximum != undefined && itemForm.storagehandlingtemperaturemaximum != null && itemForm.storagehandlingtemperaturemaximum != ''"
                                                            ng-change="selectSameMeasurementMaximum(itemForm.storagehandlingtemperaturemaximumunitofmeasure, itemForm.deliverytodistributioncentertemperatureminimum)"
                                                            data-toggle="popover" data-container="body" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_UnidadeTemperaturaMaximaArmazenamento' | translate}}">
                                                            <option value=""></option>
                                                            <option ng-repeat="unidadetemperatura in unidadestemperatura" value="{{unidadetemperatura.commoncode}}" ng-selected="itemForm.storagehandlingtemperaturemaximumunitofmeasure == unidadetemperatura.commoncode">{{unidadetemperatura.sigla}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6" ng-class="{'has-error': form.complementar.isdangeroussubstanceindicated.$invalid && form.submitted}">
                                                        <label for="isdangeroussubstanceindicated" translate="15.LBL_IndicadorMercadoriaPerigosa">Indicador de Mercadorias Perigosas </label>
                                                        <select class="form-control input-sm" id="Select10" name="isdangeroussubstanceindicated" ng-model="itemForm.isdangeroussubstanceindicated"
                                                            ng-options="item.codigo as item.nome for item in listaBoleana"
                                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_IndicadorMercadoriaPerigosa' | translate}}">
                                                            <option value=""></option>
                                                            </select>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="form-actions center" ng-show="editMode">
                                                <button name="voltar" class="btn btn-info btn-sm" type="button" ng-click="voltarParaAba(3)">
                                                    <i class="icon-backward bigger-110"></i><span translate="15.BTN_Voltar">Voltar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" name="avancar" ng-click="irParaAba(5)" type="button" ng-show="itemForm.codigoproduto != undefined && itemForm.codigoproduto != null">
                                                    <i class="icon-forward bigger-110"></i><span translate="15.BTN_Avancar">Avançar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" ng-click="onSave(itemForm, 'Salvar')" type="button" ng-disabled="itemCompare.codigostatusgtin == 7" ng-class="{'fieldDisabled': itemCompare.codigostatusgtin == 7}">
                                                    <i class="icon-save bigger-110"></i><span translate="15.BTN_Salvar">Salvar</span>
                                                </button>
                                                <button ng-if="!searchMode" id="Button6" name="gerar" ng-click="$event.stopPropagation(); gerarGTIN(itemForm.codigotipogtin)" class="btn btn-warning btn-sm" type="button"
                                                    ng-class="{'fieldDisabled': editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''}"
                                                    ng-disabled="editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''">
                                                    <i class="icon-gear bigger-110"></i><span translate="15.BTN_GerarGTIN">Gerar GTIN</span>
                                                </button>
                                                <button name="cancel" ng-click="onCancel(itemForm)" class="btn btn-danger btn-sm" type="button">
                                                    <i class="icon-remove bigger-110"></i><span translate="15.BTN_Cancelar">Cancelar</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>

									<div class="step-pane" id="step5" ng-class="{'active': stepCalculation == 5}" ng-show="itemForm.codigoproduto != undefined && itemForm.codigoproduto != null">
                                        <form name="form.outros" id="outros" novalidate class="form" role="form">

                                            <div class="row" ng-if="itemForm.codigoproduto != undefined && itemForm.codigoproduto != null">
                                                <div class="form-group col-md-6">
                                                    <label for="datainclusao" translate="15.LBL_DataInclusao">Data de Inclusão </label>
                                                    <input type="text" class="form-control input-sm" id="datainclusao" name="datainclusao" ng-model="itemForm.datainclusao" ng-readonly="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DataInclusao' | translate}}"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="datasuspensao" translate="15.LBL_DataSuspensao">Data de Suspensão </label>
                                                    <input type="text" class="form-control input-sm" id="datasuspensao" name="datasuspensao" ng-model="itemForm.datasuspensao" ng-readonly="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DataSuspensao' | translate}}"/>
                                                </div>
                                            </div>
                                            <div class="row" ng-if="itemForm.codigoproduto != undefined && itemForm.codigoproduto != null">
                                                <div class="form-group col-md-6">
                                                    <label for="datareativacao" translate="15.LBL_DataReativacao">Data de Reativação </label>
                                                    <input type="text" class="form-control input-sm" id="datareativacao" name="datareativacao" ng-model="itemForm.datareativacao" ng-readonly="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DataReativacao' | translate}}"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="datareutilizacao" translate="15.LBL_DataReutilizacao">Data de Reutilização </label>
                                                    <input type="text" class="form-control input-sm" id="datareutilizacao" name="datareutilizacao" ng-model="itemForm.datareutilizacao" ng-readonly="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DataReutilizacao' | translate}}"/>
                                                </div>
                                            </div>
                                            <div class="row" ng-if="itemForm.codigoproduto != undefined && itemForm.codigoproduto != null">
                                                <div class="form-group col-md-6">
                                                    <label for="datacancelamento" translate="15.LBL_DataCancelamento">Data de Cancelamento </label>
                                                    <input type="text" class="form-control input-sm" id="datacancelamento" name="datacancelamento" ng-model="itemForm.datacancelamento" ng-readonly="true"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DataCancelamento' | translate}}"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="dataalteracao" translate="15.LBL_DataAlteracao">Data de Alteração </label>
                                                    <input type="text" class="form-control input-sm" id="Text5" name="dataalteracao" ng-model="itemForm.dataalteracao" ng-readonly="true" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DataAlteracao' | translate}}"/>
                                                </div>
                                            </div>
                                            <div class="row" ng-if="itemForm.codigoproduto != undefined && itemForm.codigoproduto != null">
                                                <div class="form-group col-md-6">
                                                    <label for="nomeusuariocriacao" translate="15.LBL_UsuarioCriacao">Usuário de Criação </label>
                                                    <input type="text" class="form-control input-sm" id="Text11" name="nomeusuariocriacao" ng-model="itemForm.nomeusuariocriacao" ng-readonly="true" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_UsuarioCriacao' | translate}}"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="nomeusuarioalteracao" translate="15.LBL_UsuarioAlteracao">Usuário de Alteração </label>
                                                    <input type="text" class="form-control input-sm" id="Text12" name="nomeusuarioalteracao" ng-model="itemForm.nomeusuarioalteracao" ng-readonly="true" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_UsuarioAlteracao' | translate}}"/>
                                                </div>
                                            </div>

                                            <div class="form-actions center" ng-show="editMode">
                                                <button name="voltar" class="btn btn-info btn-sm" type="button" ng-click="voltarParaAba(4)">
                                                    <i class="icon-backward bigger-110"></i><span translate="15.BTN_Voltar">Voltar</span>
                                                </button>
                                                <button class="btn btn-info btn-sm" ng-click="onSave(itemForm, 'Salvar')" type="button" ng-disabled="itemCompare.codigostatusgtin == 7" ng-class="{'fieldDisabled': itemCompare.codigostatusgtin == 7}">
                                                    <i class="icon-save bigger-110"></i><span translate="15.BTN_Salvar">Salvar</span>
                                                </button>
                                                <button ng-if="!searchMode" id="Button7" name="gerar" ng-click="$event.stopPropagation(); gerarGTIN(itemForm.codigotipogtin)" class="btn btn-warning btn-sm" type="button"
                                                    ng-class="{'fieldDisabled': editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''}"
                                                    ng-disabled="editMode && itemForm.globaltradeitemnumber != undefined && itemForm.globaltradeitemnumber != null && itemForm.globaltradeitemnumber != ''">
                                                    <i class="icon-gear bigger-110"></i><span translate="15.BTN_GerarGTIN">Gerar GTIN</span>
                                                </button>
                                                <button name="cancel" ng-click="onCancel(itemForm)" class="btn btn-danger btn-sm" type="button">
                                                    <i class="icon-remove bigger-110"></i><span translate="15.BTN_Cancelar">Cancelar</span>
                                                </button>
                                            </div>
                                        </form>
									</div>
								</div>
							</div><!-- /widget-main -->
                            <div class="widget-main no-padding" ng-show="isPesquisa">
                                <form name="form.pesquisa" id="form" novalidate class="form" role="form">
                                    <div class="tab-content">
                                        <div id="papel" class="tab-pane active">
                                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="globaltradeitemnumber">GTIN - <span translate="15.LBL_NumeroGlobalItemComercial">Número Global do Item Comercial</span> </label>
                                                        <input type="text" class="form-control input-sm" maxlength="14" id="globaltradeitemnumber" name="globaltradeitemnumber" ng-model="itemForm.globaltradeitemnumber" integer
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_NumeroGlobalItemComercial' | translate}}"/>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.codigostatusgtin.$invalid && form.submitted}">
                                                        <label for="codigostatusgtin"><span translate="15.LBL_StatusGTIN">Status do GTIN</span> <span ng-show='!searchMode'>(*)</span></label>
                                                        <select class="form-control input-sm" id="codigostatusgtin" name="codigostatusgtin" ng-model="itemForm.codigostatusgtin"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_StatusGTIN' | translate}}">
                                                            <option ng-repeat="state in statusgtin" ng-if="usuarioLogado.id_tipousuario != 2 || (usuarioLogado.id_tipousuario == 2 && state.codigo != 7)" value="{{state.codigo}}">{{state.nome}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="tradeitemunitdescriptor" translate="15.LBL_TipoProduto">Tipo de Produto </label>
                                                        <select class="form-control input-sm" id="tradeitemunitdescriptor" name="tradeitemunitdescriptor" ng-model="itemForm.tradeitemunitdescriptor"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_TipoProduto' | translate}}">
                                                            <option ng-repeat="tipoProduto in tiposProdutos" value="{{tipoProduto.codigo}}">{{tipoProduto.nome}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="brandname" translate="15.LBL_Marca">Marca </label>
                                                        <input type="text" class="form-control input-sm" id="Text2" maxlength="50" name="brandname" ng-model="itemForm.brandname" 
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_Marca' | translate}}"/>
                                                    </div>
                                                </div>
                                                 <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label for="tradeitemunitdescriptor" translate="15.LBL_TipoGTIN">Tipo de GTIN </label>
                                                        <select class="form-control input-sm" id="Select11" name="codigotipogtin" ng-model="itemForm.codigotipogtin"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_TipoGTIN' | translate}}">
                                                            <option ng-repeat="licenca in licencas" value="{{licenca.codigo}}">{{licenca.nome}}</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-6">

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-12 col-xs-12">
                                                        <label for="productdescription" translate="15.LBL_DescricaoProduto">Descrição do Produto </label>
                                                        <textarea class="form-control input-sm" id="Textarea1" sbx-maxlength="300" name="productdescription" rows="3" ng-model="itemForm.productdescription"
                                                            style="resize: vertical;" required data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.15.LBL_DescricaoProduto' | translate}}"></textarea>
                                                        <span style="font-size: 80%;">(<span translate="15.LBL_TamanhoMaximo">Tamanho máximo</span>: 300)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <span translate="15.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.pesquisa.productdescription.$viewValue.length || 0 }}</span>
                                                    </div>
                                                </div>
                                                <div class="form-actions center" ng-show="editMode">
                                                    <button class="btn btn-info btn-sm" name="find" ng-cloak ng-show="isPesquisa" ng-click="onSearch(itemForm)" type="button">
                                                        <i class="icon-search bigger-110"></i><span translate="15.BTN_Pesquisar">Pesquisar</span>
                                                    </button>
                                                    <button id="cancel" name="cancel" ng-click="onCancel(item)" class="btn btn-danger btn-sm" type="button">
                                                        <i class="icon-remove bigger-110"></i><span translate="15.BTN_Cancelar">Cancelar</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
						</div><!-- /widget-body -->
					</div>
				</div>
			</div>
        </div>
        <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
            <div class="page-header">
                <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
                <label ng-bind="titulo"></label>
            </div>
            <div class="widget-box">

                <div class="pull-right action-buttons-header" style="margin-right: 0px;">
                    <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '15.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
                    <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '15.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
                    <a class="btn btn-primary btn-xs" name="NovaExportacaoXLS" title="{{ '15.TOOLTIP_ExportarXLS' | translate }}" ng-click="onExport('xlsx')"><i class="fa fa-file-excel-o"></i></a>
                    <a class="btn btn-xs" name="NovoExportacaoCSV" title="{{ '15.TOOLTIP_ExportarCSV' | translate }}" ng-click="onExport('csv')" style="background-color: #892e65 !important; border-color: #892e65"><i class="fa fa-file-text-o"></i></a>
                    <ajuda codigo="39" tooltip-ajuda="'15.TOOLTIP_Ajuda'"></ajuda>
                </div>

                <div class="content-module-bar widget-box bg-sky"></div>
                <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                    <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                        <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                    </a>
                    <div class="widget-main" style="min-height: 2000px;">
                        <div class="row">
                            <div class="form-group col-md-12" style="font-size: 90%;" translate="15.LBL_CliqueRegistroEditar">
                                Clique em um registro para editar
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table ng-table="tableProdutos" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <thead>
                                    <tr>
                                        <th ng-click="tableProdutos.sorting('codigoproduto', tableProdutos.isSortBy('codigoproduto', 'asc') ? 'desc' : 'asc')">
                                          <span>#</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('codigoproduto', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('codigoproduto', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableProdutos.sorting('productdescription', tableProdutos.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                          <span translate="15.GRID_DescricaoProduto">Descrição do Produto</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('productdescription', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('productdescription', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableProdutos.sorting('nometipogtin', tableProdutos.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')">
                                          <span translate="15.GRID_TipoGTIN">Tipo de GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('nometipogtin', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('nometipogtin', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableProdutos.sorting('globaltradeitemnumber', tableProdutos.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                          <span translate="15.GRID_GTIN">GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('globaltradeitemnumber', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('globaltradeitemnumber', 'desc')
                                          }"></i>
                                        </th>
                                        <th ng-click="tableProdutos.sorting('codigostatusgtin', tableProdutos.isSortBy('codigostatusgtin', 'asc') ? 'desc' : 'asc')">
                                          <span translate="15.GRID_StatusPublicacao">Status do GTIN</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableProdutos.isSortBy('codigostatusgtin', 'asc'),
                                            'icon-sort-down': tableProdutos.isSortBy('codigostatusgtin', 'desc')
                                          }"></i>
                                        </th>
                                        <th><span translate="15.GRID_XML">XML</span></th>
                                        <th><span translate="15.GRID_PreviewInbar">InBar</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in $data | filter:item" ng-click="onEdit(item1)">
                                        <td ng-style="{ 'width': '7%' }" sortable="'codigoproduto'">{{item1.codigoproduto}}</td>
                                        <td ng-style="{ 'width': '25%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                        <td ng-style="{ 'width': '23%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                        <td ng-style="{ 'width': '20%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                        <td ng-style="{ 'width': '20%' }" sortable="'codigostatusgtin'">{{item1.codigostatusgtin | statusgtin}}</td>
                                        <td ng-style="{ 'width': '5%' }" class="action-buttons center" title="{{ '15.TOOLTIP_GerarXML' | translate }}">
                                            <a class="blue" href="" ng-click="onXMLGenerator(item1); $event.stopPropagation();"><i class="icon-download-alt bigger-130"></i></a>
                                        </td>
                                        <td ng-style="{ 'width': '5%' }" class="action-buttons center" title="{{ '15.TOOLTIP_PreviewInbar' | translate }}">
                                            <a class="blue" href="" ng-click="onPreviewInbar(item1); $event.stopPropagation();"><i class="icon-barcode bigger-130"></i></a>
                                        </td>
                                    </tr>
                                    <tr ng-show="$data.length == 0 && $data != undefined">
                                        <td colspan="8" class="text-center" translate="15.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-cloak ng-show="!flagAssociadoSelecionado">
        <h1 translate="15.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="15.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>

    <div id="modal">
        <script type="text/ng-template" id="modalImage">
            <style>
                .modal-dialog {
                    margin: 5% auto 30px;
                    width: 450px;
                }

                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }

                span.color-blue {
                    font-weight: bold;
                }

                div + h4 {
                    border-top: 1px solid #ccc;
                    padding-top: 10px;
                }

                .modal-body {
                    padding: 0 20px;
                }
            </style>
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"><span translate="15.LBL_PreviewInbar">Preview InBar</span> <popup-ajuda codigo="63" tooltip-ajuda="'15.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form name="form.banner" novalidate class="form" role="form">

                        <h4 class="color-blue" translate="15.LBL_DadosProduto">Dados do Produto</h4>
                        <div class="row" style="margin-left: 0px; margin-right: 0px;">
                            <div class="form-group col-md-5 col-xs-12">

                                <div id="myCarousel" class="carousel slide" data-ride="carousel" ng-if="selected.imagens != undefined && selected.imagens != null && selected.imagens.length > 0">

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <div class="item" ng-class="{'active': $index == 0}" ng-repeat="imagem in selected.imagens">
                                            <img src="{{imagem.url}}" alt="Imagem 1" style="width:200px; height:150px;">
                                        </div>
                                    </div>

                                    <!-- Indicators -->
                                    <ol class="carousel-indicators" style="margin-bottom: -10%;">
                                        <li data-target="#myCarousel" ng-repeat="imagem in selected.imagens" data-slide-to="{{$index}}" ng-class="{'active': $index == 0 }"></li>
                                    </ol>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="/##myCarousel" target="_self" role="button" ng-show="selected.imagens.length > 1" data-slide="prev" data-target="#myCarousel" style="padding-top:40px;">
                                        <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="/##myCarousel" target="_self" role="button" ng-show="selected.imagens.length > 1" data-slide="next" data-target="#myCarousel" style="padding-top:40px;">
                                        <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>

                                <img ng-if="selected.imagens == undefined || selected.imagens == null || selected.imagens.length == 0" src="/images/no-photo.jpg"
                                    width="115px" height="100px" alt="Imagem do produto"/>

                            </div>
                            <div class="form-group col-md-7 col-xs-12">
                                <div class="form-group col-md-12">{{selected.itemForm.productdescription}}</div>
                            </div>
                        </div>

                        <h4 class="color-blue" translate="15.LBL_OutrasInformacoes">Outras informações</h4>
                        <div class="row">
                            <div class="form-group col-md-12 no-margin-bottom" ng-if="selected.itemForm.globaltradeitemnumber != undefined && selected.itemForm.globaltradeitemnumber != ''">
                                <span class="color-blue"><span translate="15.LBL_GTIN">GTIN</span>: </span>{{selected.itemForm.globaltradeitemnumber}}
                            </div>
                            <div class="form-group col-md-12 no-margin-bottom" ng-if="selected.itemForm.nometipogtin != undefined && selected.itemForm.nometipogtin != ''">
                                <span class="color-blue"><span translate="15.LBL_TipoGTIN">Tipo do GTIN</span>: </span> {{selected.itemForm.nometipogtin}}
                            </div>
                            <div class="form-group col-md-12 no-margin-bottom" ng-if="selected.itemForm.brandname != undefined && selected.itemForm.brandname != ''">
                                <span class="color-blue"><span translate="15.LBL_Marca">Marca</span>: </span> {{selected.itemForm.brandname}}
                            </div>
                            <div class="form-group col-md-12 no-margin-bottom" ng-if="selected.itemForm.status != undefined && selected.itemForm.status != ''">
                                <span class="color-blue"><span translate="15.LBL_Status">Status</span>: </span> {{selected.itemForm.status}}
                            </div>
                            <div class="form-group col-md-12 no-margin-bottom" ng-if="selected.itemForm.statusgtin != undefined && selected.itemForm.statusgtin != ''">
                                <span class="color-blue"><span translate="15.LBL_StatusGTIN">Status do GTIN</span>: </span> {{selected.itemForm.statusgtin}}
                            </div>
                            <div class="form-group col-md-12 no-margin-bottom" ng-if="selected.itemForm.importclassificationvalue != undefined && selected.itemForm.importclassificationvalue != ''">
                                <span class="color-blue"><span>NCM</span>: </span> {{selected.itemForm.importclassificationvalue}}
                            </div>
                        </div>

                        <h4 class="color-blue" translate="15.LBL_DadosEmpresa">Dados da Empresa</h4>
                        <div class="row">
                            <div class="form-group col-md-12 no-margin-bottom" ng-if="selected.itemForm.razaosocial != undefined && selected.itemForm.razaosocial != ''">
                                <span class="color-blue"><span translate="15.LBL_RazaoSocial">Razão Social</span>: </span>{{selected.itemForm.razaosocial}}
                            </div>
                            <div class="form-group col-md-12 no-margin-bottom" ng-if="selected.itemForm.cpfcnpj != undefined && selected.itemForm.cpfcnpj != ''">
                                <span class="color-blue"><span translate="15.LBL_CPFCNPJ">CPF/CNPJ</span>: </span>{{selected.itemForm.cpfcnpj}}
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div style="margin-bottom: 5px;">
                    <div class="form-group col-md-12" style="font-size: 90%;">Powered by CNP <img src="/images/cnp-without-title.png" style="width: 25px; height: 25px;"/></div>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" translate="15.BTN_Fechar">Fechar</button>
                </div>
            </div>
        </script>
    </div>
    
    <div id="modal">
        <script type="text/ng-template" id="modalGtinOrigem">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"><span translate="15.LBL_GTINOrigem">GTIN Origem</span> <popup-ajuda codigo="43" tooltip-ajuda="'15.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form name="form.gtin" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue"><span translate="15.LBL_PreenchaUmAbaixo">Preencha um dos campos abaixo</span>:</div>
                        </div>
                        <!--<div class="row">
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="startavailabilitydatetime" translate="15.LBL_DataInicial">Data Inicial </label>
                                <input type="text" class="form-control input-sm datepicker" id="startavailabilitydatetime" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="10" name="startavailabilitydatetime" ng-model="itemGTIN.startavailabilitydatetime"/>
                            </div>
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="endavailabilitydatetime" translate="15.LBL_DataFinal">Data Final </label>
                                <input type="text" class="form-control input-sm datepicker" id="endavailabilitydatetime" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="10" name="endavailabilitydatetime" ng-model="itemGTIN.endavailabilitydatetime"/>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="globaltradeitemnumber" translate="15.LBL_NumeroGTIN">Número de Identificação GTIN </label>
                                <input type="text" class="form-control input-sm" maxlength="13" id="globaltradeitemnumber" name="globaltradeitemnumber" ng-model="itemGTIN.globaltradeitemnumber" integer
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.15.LBL_NumeroGTIN' | translate}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 col-xs-12">
                                <label for="productdescription" translate="15.LBL_DadosProduto">Descrição do Produto </label>
                                <textarea class="form-control input-sm" sbx-maxlength="300" placeholder="{{ '15.PLACEHOLDER_ExemploSuco' | translate }}" name="productdescription" rows="3" ng-model="itemGTIN.productdescription" style="resize: vertical;"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.15.LBL_DadosProduto' | translate}}"></textarea>
                                <span style="font-size: 80%;">(<span translate="15.LBL_TamanhoMaximo">Tamanho máximo</span>: 300)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span translate="15.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.gtin.productdescription.$viewValue.length || 0 }}</span>
                            </div>
                        </div>
                        <div class="center">
                            <button name="pesquisar" class="btn btn-info btn-sm" name="find" type="button" ng-click="onSearchGTINOrigem(itemGTIN)">
                                <i class="icon-ok bigger-110"></i><span translate="15.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <div class="table-responsive">
                                <table ng-table="tableGTINPesquisados" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableGTINPesquisados.sorting('globaltradeitemnumber', tableGTINPesquisados.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                              <span translate="15.GRID_GTIN">GTIN</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGTINPesquisados.isSortBy('globaltradeitemnumber', 'asc'),
                                                'icon-sort-down': tableGTINPesquisados.isSortBy('globaltradeitemnumber', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableGTINPesquisados.sorting('productdescription', tableGTINPesquisados.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                              <span translate="15.GRID_DescricaoProduto">Descrição do Produto</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGTINPesquisados.isSortBy('productdescription', 'asc'),
                                                'icon-sort-down': tableGTINPesquisados.isSortBy('productdescription', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableGTINPesquisados.sorting('nometipogtin', tableGTINPesquisados.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')">
                                              <span translate="15.GRID_TipoGTIN">Tipo de GTIN</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGTINPesquisados.isSortBy('nometipogtin', 'asc'),
                                                'icon-sort-down': tableGTINPesquisados.isSortBy('nometipogtin', 'desc')
                                              }"></i>
                                            </th>
                                            <th><span translate="15.GRID_Selecionar">Selecionar</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data | filter:item" ng-click="onEdit(item1)">
                                            <td ng-style="{ 'width': '7%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                            <td ng-style="{ 'width': '25%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                            <td ng-style="{ 'width': '23%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                            <td ng-style="{ 'width': '5%' }">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedGTINOrigem(item1); $event.stopPropagation();">
                                                    <i class="icon-ok bigger-110"></i><span translate="15.BTN_Selecionar">Selecionar</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="7" class="text-center" translate="15.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" translate="15.BTN_Cancelar">Cancelar</button>
                </div>
            </div>
        </script>
    </div>

    <div id="Div1">
        <script type="text/ng-template" id="modalGtinInferior">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content" on-enter="onSearchGTINInferior(itemGTIN)">
                <div class="modal-header">
                    <h3 class="modal-title"><span translate="15.LBL_GTINInferior">GTIN Inferior</span> <popup-ajuda codigo="44" tooltip-ajuda="'15.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form name="form.gtin" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue"><span translate="15.LBL_PreenchaUmAbaixo">Preencha um dos campos abaixo</span>:</div>
                        </div>
                        <!--<div class="row">
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="startavailabilitydatetime" translate="15.LBL_DataInicial">Data Inicial </label>
                                <input type="text" class="form-control input-sm datepicker" id="startavailabilitydatetime" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="10" name="startavailabilitydatetime" ng-model="itemGTIN.startavailabilitydatetime"/>
                            </div>
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="endavailabilitydatetime" translate="15.LBL_DataFinal">Data Final </label>
                                <input type="text" class="form-control input-sm datepicker" id="endavailabilitydatetime" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="10" name="endavailabilitydatetime" ng-model="itemGTIN.endavailabilitydatetime"/>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="globaltradeitemnumber" translate="15.LBL_NumeroGTIN">Número de Identificação GTIN </label>
                                <input type="text" class="form-control input-sm" maxlength="13" id="globaltradeitemnumber" name="globaltradeitemnumber" ng-model="itemGTIN.globaltradeitemnumber"
                                integer data-toggle="popover" data-trigger="hover" data-content="{{'t.15.LBL_NumeroGTIN' | translate}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 col-xs-12">
                                <label for="productdescription" translate="15.LBL_DescricaoProduto">Descrição do Produto </label>
                                <textarea class="form-control input-sm" placeholder="{{ '15.PLACEHOLDER_ExemploSuco' | translate }}" sbx-maxlength="300" name="productdescription" rows="3" ng-model="itemGTIN.productdescription" style="resize: vertical;"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.15.LBL_DescricaoProduto' | translate}}"></textarea>
                                <span style="font-size: 80%;">(<span translate="15.LBL_TamanhoMaximo">Tamanho máximo</span>: 300)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span translate="15.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.gtin.productdescription.$viewValue.length || 0 }}</span>
                            </div>
                        </div>
                        <div class="center">
                            <button name="pesquisar" class="btn btn-info btn-sm" name="find" type="button" ng-click="onSearchGTINInferior(itemGTIN)">
                                <i class="icon-ok bigger-110"></i><span translate="15.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <h4 translate="15.LBL_ProdutosPesquisados">Produtos pesquisados</h4>
                            <div class="table-responsive">
                                <table ng-table="tableGTINPesquisados" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                    <thead>
                                        <tr>
                                            <th ng-click="tableGTINPesquisados.sorting('globaltradeitemnumber', tableGTINPesquisados.isSortBy('globaltradeitemnumber', 'asc') ? 'desc' : 'asc')">
                                              <span translate="15.GRID_GTIN">GTIN</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGTINPesquisados.isSortBy('globaltradeitemnumber', 'asc'),
                                                'icon-sort-down': tableGTINPesquisados.isSortBy('globaltradeitemnumber', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableGTINPesquisados.sorting('productdescription', tableGTINPesquisados.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                              <span translate="15.GRID_DescricaoTipoProduto">Descrição do Produto</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGTINPesquisados.isSortBy('productdescription', 'asc'),
                                                'icon-sort-down': tableGTINPesquisados.isSortBy('productdescription', 'desc')
                                              }"></i>
                                            </th>
                                            <th ng-click="tableGTINPesquisados.sorting('nometipogtin', tableGTINPesquisados.isSortBy('nometipogtin', 'asc') ? 'desc' : 'asc')">
                                              <span translate="15.GRID_TipoGTIN">Tipo de GTIN</span>&nbsp;&nbsp;
                                              <i class="text-center icon-sort" ng-class="{
                                                'icon-sort-up': tableGTINPesquisados.isSortBy('nometipogtin', 'asc'),
                                                'icon-sort-down': tableGTINPesquisados.isSortBy('nometipogtin', 'desc')
                                              }"></i>
                                            </th>
                                            <th><span translate="15.GRID_Selecionar">Selecionar</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data | filter:item" ng-click="onEdit(item1)">
                                            <td ng-style="{ 'width': '20%' }" sortable="'globaltradeitemnumber'">{{item1.globaltradeitemnumber}}</td>
                                            <td ng-style="{ 'width': '55%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                            <td ng-style="{ 'width': '20%' }" sortable="'nometipogtin'">{{item1.nometipogtin}}</td>
                                            <td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Selecionar'">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedGTINInferior(item1); $event.stopPropagation();"
                                                    ng-disabled="GTIN.globaltradeitemnumber != undefined && GTIN.globaltradeitemnumber != ''"
                                                    ng-class="{'fieldDisabled' : GTIN.globaltradeitemnumber != undefined && GTIN.globaltradeitemnumber != ''}">
                                                    <i class="icon-ok bigger-110"></i><span translate="15.BTN_Selecionar">Selecionar</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="7" class="text-center" translate="15.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            <h4 translate="15.LBL_ProdutosSelecionados">Produtos selecionados</h4>
                            <div class="table-responsive">
                                <table ng-table="tableGTINSelecionados" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                    <thead>
	                                    <tr>
		                                    <th ng-click="tableGTINSelecionados.sorting('productdescription', tableGTINSelecionados.isSortBy('productdescription', 'asc') ? 'desc' : 'asc')">
                                          <span translate="15.GRID_DescricaoTipoProduto">Descrição do Tipo de Produto</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableGTINSelecionados.isSortBy('productdescription', 'asc'),
                                            'icon-sort-down': tableGTINSelecionados.isSortBy('productdescription', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableGTINSelecionados.sorting('quantidade', tableGTINSelecionados.isSortBy('quantidade', 'asc') ? 'desc' : 'asc')">
                                          <span translate="15.GRID_Quantidade">Quantidade</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableGTINSelecionados.isSortBy('quantidade', 'asc'),
                                            'icon-sort-down': tableGTINSelecionados.isSortBy('quantidade', 'desc')
                                          }"></i>
                                        </th>
		                                    <th><span translate="15.GRID_Remover">Remover</span></th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in listGTINInferior | filter:item" ng-click="onEdit(item1)" ng-show="item1.statusinferior != 'excluido' && item1.statusinferior != 'novo-e-excluido'">
                                            <td ng-style="{ 'width': '25%' }" sortable="'productdescription'">{{item1.productdescription}}</td>
                                            <td ng-style="{ 'width': '23%' }" sortable="'quantidade'">
                                                <input class="form-control input-sm" placeholder="{{ '15.PLACEHOLDER_Quantidade' | translate }}" ng-model="item1.quantidade" ng-disabled="GTIN.globaltradeitemnumber != undefined && GTIN.globaltradeitemnumber != '' && GTIN.globaltradeitemnumber != null"
                                                    ng-class="{'fieldDisabled' : GTIN.globaltradeitemnumber != undefined && GTIN.globaltradeitemnumber != ''}" integer>
                                                </input>
                                            </td>
                                            <td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Remover'">
                                                <a href="" class="red" ng-click="onDeleteGTINInferior(item1); $event.stopPropagation();"
                                                    ng-class="{'fieldDisabled' : GTIN.globaltradeitemnumber != undefined && GTIN.globaltradeitemnumber != '' && GTIN.globaltradeitemnumber != null}"
                                                    ng-disabled="GTIN.globaltradeitemnumber != undefined && GTIN.globaltradeitemnumber != '' && GTIN.globaltradeitemnumber != null"><i class="icon-trash bigger-130"></i></a>
                                            </td>
                                        </tr>
                                        <tr ng-show="listGTINInferior.length == 0">
                                            <td colspan="7" class="text-center" translate="15.LBL_NenhumRegistroEncontrado">Nenhum registro selecionado </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-info" ng-click="save()" ng-if="GTIN.globaltradeitemnumber == undefined || GTIN.globaltradeitemnumber == '' || GTIN.globaltradeitemnumber == null" translate="15.BTN_Salvar">Salvar</button>
                    <button class="btn btn-danger" ng-click="cancel()" ng-if="GTIN.globaltradeitemnumber != undefined && GTIN.globaltradeitemnumber != '' && GTIN.globaltradeitemnumber != null" translate="15.BTN_Fechar">Fechar</button>
                </div>
            </div>
        </script>
    </div>

    <div id="Div2">
        <script type="text/ng-template" id="modalGtin8">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }

                .carousel-control.right {
                    background-image: none;
                }

                .carousel-control.left {
                    background-image: none;
                }
            </style>
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Selecione um GTIN-8 <popup-ajuda codigo="45" tooltip-ajuda="'15.TOOLTIP_Ajuda'"></popup-ajuda></a></h3>
                </div>
                <div class="modal-body">
                    <form name="form.gtin" novalidate class="form" role="form">
                        <div class="table-responsive">
                            <table ng-table="tableGTIN8" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                <tbody>
                                    <tr ng-repeat="item1 in $data | filter:item" ng-click="onEdit(item1)" ng-show="item1.statusinferior != 'excluido' && item1.statusinferior != 'novo-e-excluido'">
                                        <td ng-style="{ 'width': '10%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                        <td ng-style="{ 'width': '45%' }">GTIN-8</td>
                                        <td ng-style="{ 'width': '25%' }" sortable="'numeroprefixo'">{{item1.numeroprefixo}}</td>
                                        <td ng-style="{ 'width': '20%' }" class="action-buttons center" data-title="'Selecionar'">
                                            <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelected8(item1)">
                                                <i class="icon-ok bigger-110"></i>Selecionar
                                            </button>
                                        </td>
                                    </tr>
                                    <tr ng-show="$data.length == 0">
                                        <td colspan="7" class="text-center">Nenhum GTIN-8 disponível </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" ng-if="GTIN.globaltradeitemnumber != undefined && GTIN.globaltradeitemnumber != '' && GTIN.globaltradeitemnumber != null">Fechar</button>
                </div>
            </div>
        </script>
    </div>

    <div id="modal">
        <script type="text/ng-template" id="modalNcm">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }
            </style>
            <div class="modal-content" on-enter="onSearchNCM(itemNCM)">
                <div class="modal-header">
                    <h3 class="modal-title">NCM <popup-ajuda codigo="45" tooltip-ajuda="'15.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form name="form.gtin" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue"><span translate="15.LBL_PreenchaUmAbaixo">Preencha um dos campos abaixo</span>:</div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="ncm">NCM </label>
                                <input type="text" class="form-control input-sm" id="ncm" name="ncm" ng-model="itemNCM.ncm" ui-mask="9999.99.99"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.15.LBL_Ncm' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="descricao" translate="15.LBL_Descricao">Descrição </label>
                                <input type="text" class="form-control input-sm" id="descricao" name="descricao" ng-model="itemNCM.descricao" autocomplete="off"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.15.LBL_Descricao' | translate}}"/>
                            </div>
                        </div>
                        <div class="center">
                            <button name="pesquisar" class="btn btn-info btn-sm" name="find" type="button" ng-click="onSearchNCM(itemNCM)">
                                <i class="icon-ok bigger-110"></i><span translate="15.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <div class="table-responsive">
                                <table ng-table="tableNCMPesquisados" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                    <thead>
	                                    <tr>
		                                    <th ng-click="tableNCMPesquisados.sorting('codigo', tableNCMPesquisados.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                          <span>#</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableNCMPesquisados.isSortBy('codigo', 'asc'),
                                            'icon-sort-down': tableNCMPesquisados.isSortBy('codigo', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableNCMPesquisados.sorting('ncm', tableNCMPesquisados.isSortBy('ncm', 'asc') ? 'desc' : 'asc')">
                                          <span>NCM</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableNCMPesquisados.isSortBy('ncm', 'asc'),
                                            'icon-sort-down': tableNCMPesquisados.isSortBy('ncm', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableNCMPesquisados.sorting('descricao', tableNCMPesquisados.isSortBy('descricao', 'asc') ? 'desc' : 'asc')">
                                          <span translate="15.GRID_Descricao">Descrição</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableNCMPesquisados.isSortBy('descricao', 'asc'),
                                            'icon-sort-down': tableNCMPesquisados.isSortBy('descricao', 'desc')
                                          }"></i>
                                        </th>
		                                    <th><span translate="15.GRID_Selecionar">Selecionar</span></th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data | filter:item">
                                            <td ng-style="{ 'width': '10%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                            <td ng-style="{ 'width': '25%' }" sortable="'ncm'">{{item1.ncm}}</td>
                                            <td ng-style="{ 'width': '45%' }" sortable="'descricao'">{{item1.descricao}}</td>
                                            <td ng-style="{ 'width': '20%' }" class="action-buttons center" data-title="'Selecionar'">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedNCM(item1)">
                                                    <i class="icon-ok bigger-110"></i>Selecionar
                                                </button>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="7" class="text-center" translate="15.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" translate="15.BTN_Cancelar">Cancelar</button>
                </div>
            </div>
        </script>
    </div>

    <div id="modal">
        <script type="text/ng-template" id="modalCest">
            <style>
                .color-blue {
                    color: #172C6C;
                }

                .no-margin-bottom {
                    margin-bottom: 0px;
                }
            </style>
            <div class="modal-content" on-enter="onSearchCEST(itemCEST)">
                <div class="modal-header">
                    <h3 class="modal-title">CEST <popup-ajuda codigo="45" tooltip-ajuda="'15.TOOLTIP_Ajuda'"></popup-ajuda></h3>
                </div>
                <div class="modal-body">
                    <form name="form.gtin" novalidate class="form" role="form">
                        <div class="row">
                            <div class="form-group col-md-12 color-blue"><span translate="15.LBL_PreenchaUmAbaixo">Preencha um dos campos abaixo</span>:</div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="cest">CEST </label>
                                <input type="text" class="form-control input-sm" id="cest" name="cest" ng-model="itemCEST.cest" ui-mask="99.999.99"
                                data-toggle="popover" data-trigger="hover" data-content="{{'15.LBL_NumeroCest' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6 col-xs-12">
                                <label for="descricao_cest" translate="15.LBL_Descricao">Descrição </label>
                                <input type="text" class="form-control input-sm" id="descricao_cest" name="descricao_cest" ng-model="itemCEST.descricao_cest" autocomplete="off"
                                data-toggle="popover" data-trigger="hover" data-content="{{'t.15.LBL_Descricao' | translate}}"/>
                            </div>
                        </div>
                        <div class="center">
                            <button name="pesquisar" class="btn btn-info btn-sm" name="find" type="button" ng-click="onSearchCEST(itemCEST)">
                                <i class="icon-ok bigger-110"></i><span translate="15.BTN_Pesquisar">Pesquisar</span>
                            </button>
                        </div>
                        <br>
                        <div>
                            <div class="table-responsive">
                                <table ng-table="tableCESTPesquisados" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                    <thead>
	                                    <tr>
		                                    <th ng-click="tableCESTPesquisados.sorting('codigocest', tableCESTPesquisados.isSortBy('codigocest', 'asc') ? 'desc' : 'asc')">
                                          <span>#</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCESTPesquisados.isSortBy('codigocest', 'asc'),
                                            'icon-sort-down': tableCESTPesquisados.isSortBy('codigocest', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableCESTPesquisados.sorting('cest', tableCESTPesquisados.isSortBy('cest', 'asc') ? 'desc' : 'asc')">
                                          <span>CEST</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCESTPesquisados.isSortBy('cest', 'asc'),
                                            'icon-sort-down': tableCESTPesquisados.isSortBy('cest', 'desc')
                                          }"></i>
                                        </th>
		                                    <th ng-click="tableCESTPesquisados.sorting('descricao_cest', tableCESTPesquisados.isSortBy('descricao_cest', 'asc') ? 'desc' : 'asc')">
                                          <span translate="15.GRID_Descricao">Descrição</span>&nbsp;&nbsp;
                                          <i class="text-center icon-sort" ng-class="{
                                            'icon-sort-up': tableCESTPesquisados.isSortBy('descricao_cest', 'asc'),
                                            'icon-sort-down': tableCESTPesquisados.isSortBy('descricao_cest', 'desc')
                                          }"></i>
                                        </th>
		                                    <th><span translate="15.GRID_Selecionar">Selecionar</span></th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in $data | filter:item">
                                            <td ng-style="{ 'width': '10%' }" sortable="'codigoCest'">{{item1.codigocest}}</td>
                                            <td ng-style="{ 'width': '25%' }" sortable="'cest'">{{item1.cest}}</td>
                                            <td ng-style="{ 'width': '45%' }" sortable="'descricao_cest'">{{item1.descricao_cest}}</td>
                                            <td ng-style="{ 'width': '20%' }" class="action-buttons center" data-title="'Selecionar'">
                                                <button name="selecionar" class="btn btn-info btn-xs" type="button" ng-click="onSelectedCEST(item1)">
                                                    <i class="icon-ok bigger-110"></i>Selecionar
                                                </button>
                                            </td>
                                        </tr>
                                        <tr ng-show="$data.length == 0">
                                            <td colspan="7" class="text-center" translate="15.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div class="modal-footer center">
                    <button class="btn btn-danger" ng-click="cancel()" translate="15.BTN_Cancelar">Cancelar</button>
                </div>
            </div>
        </script>
    </div>
</div>
