using GS1.CNP.WEBUI.Core;

namespace GS1.CNP.WEBUI.views.backoffice
{
    public partial class relatorioAcessoAssociado : BaseWeb
    {
        public override string PermissaoPagina { get { return "AcessarRelatorioAcessoAssociado"; } }
    }
}