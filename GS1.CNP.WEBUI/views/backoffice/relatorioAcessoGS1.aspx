﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioAcessoGS1.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioAcessoGS1" %>
<div ng-controller="RelatorioLoginAcessoGS1Ctrl">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="34.LBL_RelatorioLoginAcesso">Relatório de Login de Acesso</label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode" translate="34.LBL_Filtro">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="{{ '34.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="34.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.cpfcnpj.$invalid && form.submitted}">
                                                <label for="cpfcnpj" translate="34.LBL_CPFCNPJUsuario">CPF/CNPJ do Usuário</label>
                                                <input type="text" id="cpfcnpj" name="cpfcnpj" class="form-control input-sm" ng-model="model.cpfcnpj" maxlength="18" ui-br-cpfcnpj-mask 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.34.LBL_CPFCNPJUsuario' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.nomerazaosocial.$invalid && form.submitted}">
                                                <label for="nomerazaosocial" translate="34.LBL_NomeRazaoSocial">Nome/Razão Social da Empresa</label>
                                                <input id="nomerazaosocial" name="nomerazaosocial" type="text" maxlength="255" class="form-control input-sm" ng-model="model.nomerazaosocial" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.34.LBL_NomeRazaoSocial' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.nomeusuario.$invalid && form.submitted}">
                                                <label for="nomeusuario" translate="34.LBL_NomeUsuario">Nome do Usuário</label>
                                                <input id="nomeusuario" name="nomeusuario" type="text" maxlength="255" class="form-control input-sm" ng-model="model.nomeusuario" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.34.LBL_NomeUsuario' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.login.$invalid && form.submitted}">
                                                <label for="login" translate="34.LBL_Email">E-mail</label>
                                                <input id="login" name="login" type="text" maxlength="255" class="form-control input-sm" ng-model="model.login" ng-pattern="/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.34.LBL_Email' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                                <label for="inicio" translate="34.LBL_DataInicioUltimoAcesso">Data de Início do Último Acesso</label>
                                                <input id="inicio" name="inicio" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.inicio" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.34.LBL_DataInicioUltimoAcesso' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.fim.$invalid && form.submitted}">
                                                <label for="fim" translate="34.LBL_DataFimUltimoAcesso">Data de Fim do Último Acesso</label>
                                                <input id="fim" name="fim" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.fim" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.34.LBL_DataFimUltimoAcesso' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center botoes">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="34.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="widget-box" ng-show="(dadosRelatorio != null && dadosRelatorio.length == 0)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4 translate="34.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio.length > 0">
                        <div id="divRelatorio" class="table-responsive">
                            <meta charset="utf-8">
                            <div>
                                <table id='exportPDF01' class='table table-striped' style="width: 98%; margin-left: 7px">
                                    <colgroup>
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                    </colgroup>
                                    <thead>
                                        <tr class='warning'>
                                            <th translate="34.GRID_CPFCNPJUsuario">CPF/CNPJ do Usuário</th>
                                            <th translate="34.GRID_NomeRazaoSocialEmpresa">Razão Social da Empresa</th>
                                            <th translate="34.GRID_NomeUsuario">Nome do Usuário</th>
                                            <th translate="34.GRID_Email">E-mail</th>
                                            <th translate="34.GRID_Perfil">Perfil</th>
                                            <th translate="34.GRID_UltimoAcesso">Último Acesso</th>
                                            <th translate="34.GRID_DataInclusao">Data de Inclusão</th>
                                            <th translate="34.GRID_DataExpiracaoSenha">Data de Expiração da Senha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in dadosRelatorio" >
                                            <td>{{item1.cpf_usuario}}</td>
                                            <td>{{item1.razao_social}}</td>
                                            <td>{{item1.nome_usuario}}</td>
                                            <td>{{item1.login_usuario}}</td>
                                            <td>{{item1.perfil}}</td>
                                            <td>{{item1.data_ultimo_acesso | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            <td>{{item1.data_criacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            <td>{{item1.data_expiracao_senha | date:'dd/MM/yyyy'}}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="8"><span translate="34.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório de Login de Acesso')">
                    <i class="icon-paste bigger-110"></i>
                    <span translate="34.BTN_ExportarPDF">Exportar PDF</span>
                </button>
                <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                    <i class="icon-table bigger-110"></i>
                    <span translate="34.BTN_ExportarCSV">Exportar CSV</span>
                </button>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 340px;"></div>
        </div>
    </div>
</div>