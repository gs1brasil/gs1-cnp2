﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioAssociadoGS1.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioAssociadoGS1" %>
<div ng-controller="RelatorioAssociadoGS1Ctrl">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="28.LBL_RelatorioAssociado">Relatório de Associado</label>

        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode" translate="28.LBL_Filtros">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="{{ '28.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="28.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <%--<div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.cpfcnpj.$invalid && form.submitted}">
                                                <label for="cpfcnpj">CPF/CNPJ <span>(*)</span></label>
                                                <input type="text" id="cpfcnpj" name="cpfcnpj" class="form-control input-sm" ng-model="model.cpfcnpj" maxlength="18" ui-br-cpfcnpj-mask required="required" ng-pattern="/(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)|(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)/" data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.28.lbl_CpfCnpj' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>--%>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                                <label for="nome" translate="28.LBL_NomeRazaoSocial">Nome/Razão Social </label>
                                                <input id="nome" name="Nome" type="text"  class="form-control input-sm" ng-model="model.nome" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.28.LBL_NomeRazaoSocial' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.licenca.$invalid && form.submitted}">
                                                <label for="licenca" translate="28.LBL_Licenca">Licença </label>
                                                <select id="licenca" name="licenca" class="form-control" ng-model="model.licenca"  ng-options="obj.key as obj.value for obj in listalicenca" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.28.LBL_Licenca' | translate}}">
                                                    <option value="" selected></option>
                                                </select>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                                <label for="status" translate="28.LBL_Status">Status </label>
                                                <select id="status" name="status" class="form-control" ng-model="model.status" ng-options="obj.key as obj.value for obj in listaStatusAssociado" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.28.LBL_Status' | translate}}">
                                                    <option value="" selected ></option>
                                                </select>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center botoes">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="28.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="widget-box" ng-show="(dadosRelatorio != null && dadosRelatorio.length == 0)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4 translate="28.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio.length > 0">
                        <div id="divRelatorio" class="table-responsive">
                            <meta charset="utf-8">
                            <div>
                                <table id='exportPDF01' class='table table-striped' style="width: 98%; margin-left: 7px">
                                    <colgroup>
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                    </colgroup>
                                    <thead>
                                        <tr class='warning'>
                                            <th translate="28.GRID_CPFCNPJ">CPF/CNPJ</th>
                                            <th translate="28.GRID_RazaoSocialNome">Nome/Razão Social</th>
                                            <th translate="28.GRID_Status">Status</th>
                                            <th translate="28.GRID_ContatoMaster">Contato Master</th>
                                            <th translate="28.GRID_EmailContato">E-mail do Contato</th>
                                            <th translate="28.GRID_DataVencimentoTaxa">Data de Vencimento da Taxa</th>
                                            <th translate="28.GRID_CEP">CEP</th>
                                            <th translate="28.GRID_Endereco">Endereço</th>
                                            <th translate="28.GRID_Complemento">Complemento</th>
                                            <th translate="28.GRID_Bairro">Bairro</th>
                                            <th translate="28.GRID_Cidade">Cidade</th>
                                            <th translate="28.GRID_UF">UF</th>
                                            <th translate="28.GRID_UltimaSincronizacao">Última Sincronização</th>
                                            <th translate="28.GRID_InscricaoEstadual">Inscrição Estadual</th>
                                            <th translate="28.GRID_Licenca">Licença</th>
                                            <th translate="28.GRID_PrefixoLicenca">Prefixo da Licença</th>
                                            <th translate="28.GRID_StatusLicenca">Status da Licença</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in dadosRelatorio" >
                                            <td>{{item1.cpf_cnpj}}</td>
                                            <td>{{item1.nome_razao_social}}</td>
                                            <td>{{item1.status}}</td>
                                            <td>{{item1.contato_master}}</td>
                                            <td>{{item1.email_contato}}</td>
                                            <td>{{item1.data_vencimento_taxa | date:'dd/MM/yyyy'}}</td>
                                            <td>{{item1.cep}}</td>
                                            <td>{{item1.endereco}}</td>
                                            <td>{{item1.complemento}}</td>
                                            <td>{{item1.bairro}}</td>
                                            <td ng-style="{ 'white-space': 'pre' }">{{item1.cidade | pipequebralinha}}</td>
                                            <td>{{item1.uf}}</td>
                                            <td>{{item1.ultima_sincronizacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            <td>{{item1.inscricao_estadual}}</td>
                                            <td>{{item1.licenca}}</td>
                                            <td>{{item1.prefixo_licenca}}</td>
                                            <td>{{item1.status_licenca | status}}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="17"><span translate="28.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório de Associado', 'L')">
                    <i class="icon-paste bigger-110"></i>
                    <span translate="28.BTN_ExportarPDF">Exportar PDF</span>
                </button>
                <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                    <i class="icon-table bigger-110"></i>
                    <span translate="28.BTN_ExportarCSV">Exportar CSV</span>
                </button>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 340px;"></div>
        </div>
    </div>
</div>