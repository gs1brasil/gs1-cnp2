﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioAssociadosTermosAdesaoHistorico.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioAssociadosTermosAdesaoHistorico" %>
<div ng-controller="relatorioAssociadosTermosAdesaoCtrl">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="57.LBL_TitleRelatorioAssociadosTermosAdesaoHistorico">Relatório de Associados x Termos de Adesão - Histórico</label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode" translate="57.LBL_Filtros">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px;" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="{{ '57.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="57.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12">
                                                <div class="col-md-12" style="padding-left:0px;">
                                                    <label for="razaosocial"><span translate="57.LBL_RazaoSocial">Nome/Razão Social</span></label><br />
                                                    <input id="razaosocial" name="razaosocial" type="text" maxlength="255" class="form-control input-sm" ng-model="model.razaosocial"
                                                        ng-blur="verificaAutoCompleteRazaoSocial()" typeahead-on-select="alteraAssociado(associadoatual);" placeholder="{{ '57.PLACEHOLDER_DigiteRazaoSocial' | translate }}" 
                                                        typeahead="razoessociais as razoessociais.nome for razoessociais in BuscarRazaoSocial($viewValue)" typeahead-min-length="3"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12">
                                                <div class="col-md-12" style="padding-left:0px;">
                                                    <label for="versao"><span translate="57.LBL_Versao">Versão Termo de Aceite</span></label><br />
                                                    <select id="versao" name="versao" class="form-control input-sm" ng-model="model.versao">
                                                        <option value="">Todas</option>
                                                        <option ng-repeat="versao in listaVersoes" value="{{versao.codigo}}">{{versao.nome}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12">
                                                <div class="col-md-3" style="padding-left:0px;" ng-class="{'has-error': form.periodoinicial.$invalid && form.submitted}">
                                                    <label for="periodoinicial"><span translate="57.LBL_PeriodoInicial">Período Inicial</span> (*)</label><br />
                                                    <input id="periodoinicial" name="periodoinicial" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker sbx-options='{ "format":"dd/mm/yyyy" }' ng-model="model.periodoinicial" required/>
                                                </div>
                                                <div class="col-md-3" style="padding-left:0px;" ng-class="{'has-error': form.periodofinal.$invalid && form.submitted}">
                                                    <label for="periodofinal"><span translate="57.LBL_PeriodoFinal">Período Final</span> (*)</label><br />
                                                    <input id="periodofinal" name="periodofinal" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker sbx-options='{ "format":"dd/mm/yyyy" }' ng-model="model.periodofinal" required/>
                                                </div>
                                                <div class="col-md-6"></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center botoes">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorioHistorico()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="57.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="widget-box" ng-show="(generating == true && dadosRelatorio == null)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4 translate="57.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio != null">
                        <div id="divRelatorio" class="table-responsive">
                            <meta charset="utf-8">
                            <div>
                                <table id='exportPDF01' class='table table-striped mensal' style="width: 100%; margin-left: auto; margin-right:auto">
                                    <colgroup>
                                        <col />
                                        <col />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <td translate="57.LBL_RazaoSocial">Nome/Razão Social</td>
                                            <td translate="57.LBL_CPFCNPJ" style="width:150px">CPF/CNPJ</td>
                                            <td translate="57.LBL_CAD">CAD</td>
                                            <td translate="57.LBL_Versao">Versão Termo Aceite</td>
                                            <td translate="57.LBL_DataHora" style="width:150px">Data/Hora</td>
                                            <td translate="57.LBL_Usuario">Usuário</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="data in dadosRelatorio">
                                            <td>{{data.razaosocial}}</td>
                                            <td>{{data.cpfcnpj | cpfcnpj}}</td>
                                            <td>{{data.cad}}</td>
                                            <td>{{data.versao}}</td>
                                            <td>{{data.dataaceite | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            <td>{{data.nomeusuario}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null">
                <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório de Associados x  Termos de Adesão - Histórico','L')">
                    <i class="icon-paste bigger-110"></i>
                    <span translate="57.BTN_ExportarPDF">Exportar PDF</span>
                </button>
                <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                    <i class="icon-table bigger-110"></i>
                    <span translate="57.BTN_ExportarCSV">Exportar CSV</span>
                </button>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 340px;"></div>
        </div>
    </div>
</div>