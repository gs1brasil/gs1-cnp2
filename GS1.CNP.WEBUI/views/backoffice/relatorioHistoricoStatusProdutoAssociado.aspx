﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioHistoricoStatusProdutoAssociado.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioHistoricoStatusProdutoAssociado" %>
<div ng-controller="RelatorioHistoricoStatusProdutoAssociadoCtrl">
    <div ng-cloak ng-show="flagAssociadoSelecionado === false">
        <h1 translate="21.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="21.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
    <div ng-cloak ng-show="flagAssociadoSelecionado === true">
        <div class="form-container form-relatorios" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label translate="21.LBL_TitleRelatorioHistoricoStatusProduto">Relatório de Histórico de Status do Produto</label>
            </div>

            <div class="widget-box">
                <div class="widget-header">
                    <h4><span ng-show="!searchMode" translate="21.LBL_Filtros">Filtros </span></h4>
                    <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                        <i ng-click="ocultarFiltros()" style="margin-right: 5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                    </div>
                    <div class="widget-toolbar" ng-show="!collapseFiltro">
                        <a href=""><i class="icon-eraser" title="{{ '21.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                    </div>

                </div>
                <div class="widget-body">
                    <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                        <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                            <div class="tab-content">
                                <div id="usuario" class="tab-pane active">
                                    <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="21.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                                    <label for="inicio" translate="21.LBL_DataInicialCadastro">Data Inicial de Cadastro</label> (*)
                                                    <input id="inicio" name="inicio" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.inicio" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.21.LBL_DataInicialCadastro' | translate}}" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group" ng-class="{'has-error': form.fim.$invalid && form.submitted}">
                                                    <label for="fim" translate="21.LBL_DataFimCadastro">Data Fim de Cadastro</label> (*)
                                                    <input id="fim" name="fim" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.fim" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.21.LBL_DataFimCadastro' | translate}}" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group" ng-class="{'has-error': (form.gtin.$invalid || form.gtin.$invalidLength) && form.submitted }">
                                                    <label for="gtin" translate="21.LBL_GTIN">GTIN</label>
                                                    <input type="text" id="gtin" name="gtin" class="form-control input-sm" ng-model="model.gtin" integer gtin tipo-gtin="model.tipogtin" maxlength="{{model.tipogtin ? maxlengthGtin[model.tipogtin] : 14}}"  
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.21.LBL_GTIN' | translate}}"/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                                    <label for="descricao" translate="21.LBL_DescricaoProduto">Descrição do Produto </label>
                                                    <input type="text" id="descricao" name="descricao" class="form-control col-sm-12" ng-model="model.descricao" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body"  data-html="true" data-content="{{'t.21.LBL_DescricaoProduto' | translate}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group" ng-class="{'has-error': form.statusgtin.$invalid && form.submitted}">
                                                    <label for="statusgtin" translate="21.LBL_StatusGTIN">Status GTIN </label>
                                                    <select id="statusgtin" name="statusgtin" class="form-control " ng-model="model.statusgtin"  ng-options="obj.key as obj.value for obj in listastatusGtin track by obj.key"
                                                    data-toggle="popover" data-trigger="hover" data-container="body"  data-html="true" data-content="{{'t.21.LBL_StatusGTIN' | translate}}">
                                                        <option value="" translate="21.LBL_Selecione">Selecione</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-xs-12">
                                                <div class="form-group" ng-class="{'has-error': form.tipogtin.$invalid && form.submitted}">
                                                    <label for="tipogtin" translate="21.LBL_TipoGTIN">Tipo de GTIN </label>
                                                    <select id="tipogtin" name="tipogtin"  class="form-control" ng-model="model.tipogtin"  ng-options="obj.key as obj.value for obj in listatipoGtin track by obj.key"
                                                        ng-change="changeTipoGtin();"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.21.LBL_TipoGTIN' | translate}}">
                                                        <option value="" translate="21.LBL_Selecione">Selecione</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-actions center botoes">
                                        <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                            <i class="icon-file bigger-110"></i>
                                            <span translate="21.BTN_GerarRelatorio">Gerar Relatório</span>
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="widget-box" ng-show="(dadosRelatorio != null && dadosRelatorio.length == 0)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
                <h4 translate="21.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
            </div>
            <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                <div class="widget-body" style="height: 660px;">
                    <div class="widget-main" style="min-height: 300px; width: 100%">
                        <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio.length > 0">
                            <div id="divRelatorio" class="table-responsive">
                                <meta charset="utf-8">
                                <div>
                                    <table id='exportPDF01' class='table table-striped' style="width: 98%; margin-left: 7px">
                                        <colgroup>
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                        </colgroup>
                                        <thead>
                                            <tr class='warning'>
                                                <th translate="21.GRID_GTIN">GTIN</th>
                                                <th translate="21.GRID_TipoGTIN">Tipo de GTIN</th>
                                                <th translate="21.GRID_Marca">Marca do Produto</th>
                                                <th translate="21.GRID_StatusAtual">Status Atual</th>
                                                <th translate="21.GRID_CodeBrick">Code Brick</th>
                                                <th translate="21.GRID_AgenciaRegistro">Agência Reguladora</th>
                                                <th translate="21.GRID_CodigoRegistro">Código Interno</th>
                                                <th translate="21.GRID_Usuario">Usuário</th>
                                                <th translate="21.GRID_DescricaoProduto">Descrição do Produto</th>
                                                <th translate="21.GRID_DataCriacao">Data de Criação</th>
                                                <th translate="21.GRID_DataSuspensao">Suspenção</th>
                                                <th translate="21.GRID_DataReativacao">Reativação</th>
                                                <th translate="21.GRID_DataCancelamento">Cancelamento</th>
                                                <th translate="21.GRID_DataReutilizacao">Reutilização</th>
                                                <th translate="21.GRID_PaisOrigem">País de Origem</th>
                                                <th translate="21.GRID_PaisDestino">País de Destino</th>
                                                <th translate="21.GRID_TipoProduto">Tipo do Produto</th>
                                                <th translate="21.GRID_Lingua">Língua</th>
                                                <th translate="21.GRID_Estado">Estado</th>
                                                <th translate="21.GRID_Largura">Largura</th>
                                                <th translate="21.GRID_Profundidade">Profundidade</th>
                                                <th translate="21.GRID_Altura">Altura</th>
                                                <th translate="21.GRID_PesoLiquido">Peso Líquido</th>
                                                <th translate="21.GRID_PesoBruto">Peso Bruto</th>
                                                <th translate="21.GRID_AliquotaIPI">Alíquota de IPI</th>
                                                <th translate="21.GRID_TipoURL">Tipo de URL</th>
                                                <th translate="21.GRID_URL">URL</th>
                                                <th translate="21.GRID_CompartilhaDados">Compartilha Dados</th>
                                                <th translate="21.GRID_Observacoes">Observações</th>
                                                <th translate="21.GRID_NCM">NCM</th>
                                                <th translate="21.GRID_GTINInferior">GTIN Inferior</th>
                                                <th translate="21.GRID_DescricaoGTINInferior">Descrição do GTIN Inferior</th>
                                                <th translate="21.GRID_QuantidadeItens">Quantidade de Itens</th>
                                                <th translate="21.GRID_StatusHistorico">Status de Histórico</th>
                                                <th translate="21.GRID_DataHistorico">Data do Histórico</th>
                                                <th translate="21.GRID_UsuarioAlteracaoHistorico">Usuário de Alteração</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item1 in dadosRelatorio" >
                                                <td>{{item1.gtin}}</td>
                                                <td>{{item1.tipo_gtin}}</td>
                                                <td>{{item1.marca_produto}}</td>
                                                <td>{{item1.status_atual}}</td>
                                                <td>{{item1.code_brick}}</td>
                                                <td>{{item1.agencia_registro}}</td>
                                                <td>{{item1.codigo_interno}}</td>
                                                <td>{{item1.usuario}}</td>
                                                <td>{{item1.descricao_produto}}</td>
                                                <td>{{item1.data_criacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.suspencao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.reativacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.cancelamento | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.reutilizacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.pais_origem}}</td>
                                                <td>{{item1.pais_destino}}</td>
                                                <td>{{item1.tipo_produto}}</td>
                                                <td>{{item1.lingua}}</td>
                                                <td>{{item1.estado}}</td>
                                                <td>{{item1.largura}}</td>
                                                <td>{{item1.profundidade}}</td>
                                                <td>{{item1.altura}}</td>
                                                <td>{{item1.peso_liquido}}</td>
                                                <td>{{item1.peso_bruto}}</td>
                                                <td>{{item1.ipi}}</td>
                                                <td>{{item1.tipo_url}}</td>
                                                <td>{{item1.url}}</td>
                                                <td>{{item1.compartilha_dados}}</td>
                                                <td>{{item1.observacoes}}</td>
                                                <td>{{item1.ncm}}</td>
                                                <td>{{item1.gtin_inferior}}</td>
                                                <td>{{item1.descricao_inferior}}</td>
                                                <td>{{item1.qtd_itens_inferior}}</td>
                                                <td>{{item1.status_historico}}</td>
                                                <td>{{item1.data_status_historico | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.usuario_alteracao}}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="36"><span translate="21.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                    <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatorio de Histórico de Status do Produto', 'L')">
                        <i class="icon-paste bigger-110"></i>
                        <span translate="21.BTN_ExportarPDF">Exportar PDF</span>
                    </button>
                    <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                        <i class="icon-table bigger-110"></i>
                        <span translate="21.BTN_ExportarCSV">Exportar CSV</span>
                    </button>
                </div>
            </div>
            <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
                <div id="chartContainer" style="width: 100%; height: 340px;"></div>
            </div>
        </div>
    </div>
    
</div>