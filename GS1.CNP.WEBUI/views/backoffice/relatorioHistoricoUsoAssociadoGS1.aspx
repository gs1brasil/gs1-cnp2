﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioHistoricoUsoAssociadoGS1.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioHistoricoUsoAssociadoGS1" %>
<div ng-controller="relatorioHistoricoUsoAssociadoGS1Ctrl">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="54.LBL_TitleRelatorioHistoricoUsoAssociadoGS1">Relatório de Histórico de Uso</label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode" translate="54.LBL_Filtros">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px;" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="{{ '54.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="54.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12">
                                                <div class="col-md-3" style="padding-left:0px;">
                                                    <label for="nomeusuario"><span translate="54.LBL_NomeUsuario">Nome do Usuário</span></label><br />
                                                    <input id="nomeusuario" name="nomeusuario" type="text" maxlength="255" class="form-control input-sm" ng-model="model.nomeusuario" 
                                                        ng-blur="verificaAutoCompleteUsuario()" placeholder="{{ '54.PLACEHOLDER_DigiteNomeUsuario' | translate }}"
                                                        typeahead="usuarios as usuarios.nome for usuarios in BuscarUsuario($viewValue)" typeahead-min-length="3"/>
                                                </div>
                                                <div class="col-md-3" style="padding-left:0px;">
                                                    <label for="razaosocial"><span translate="54.LBL_Nome">Nome/Razão Social</span></label><br />
                                                    <input id="razaosocial" name="razaosocial" type="text" maxlength="255" class="form-control input-sm" ng-model="model.razaosocial"
                                                        ng-blur="verificaAutoCompleteRazaoSocial()" typeahead-on-select="alteraAssociado(associadoatual);" placeholder="{{ '54.PLACEHOLDER_DigiteRazaoSocial' | translate }}" 
                                                        typeahead="razoessociais as razoessociais.nome for razoessociais in BuscarRazaoSocial($viewValue)" typeahead-min-length="3"/>
                                                </div>
                                                <div class="col-md-3" style="padding-left:0px;">
                                                    <label for="login"><span translate="54.LBL_Login">Login</span></label><br />
                                                    <input id="login" name="login" type="text" maxlength="255" class="form-control input-sm" ng-model="model.login" 
                                                        ng-blur="verificaAutoCompleteLogin()" typeahead="logins as logins.email for logins in BuscarLogin($viewValue)" 
                                                        placeholder="{{ '54.PLACEHOLDER_DigiteLogin' | translate }}" typeahead-min-length="3"/>
                                                </div>
                                                <div class="col-md-3" style="padding-left:0px;">
                                                    <label for="funcionalidade"><span translate="54.LBL_Funcionalidade">Funcionalidade</span></label><br />
                                                    <select id="funcionalidade" name="funcionalidade" class="form-control input-sm" ng-model="model.funcionalidade">
                                                        <option value="0">Todas</option>
                                                        <option ng-repeat="funcionalidade in dadosFuncionalidade" value="{{funcionalidade.codigo}}">{{funcionalidade.nome}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12">
                                                <div class="col-md-3" style="padding-left:0px;" ng-class="{'has-error': form.periodoinicial.$invalid && form.submitted}">
                                                    <label for="periodoinicial"><span translate="54.LBL_PeriodoInicial">Período Inicial</span> (*)</label><br />
                                                    <input id="periodoinicial" name="periodoinicial" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker sbx-options='{ "format":"dd/mm/yyyy" }' ng-model="model.periodoinicial" required/>
                                                </div>
                                                <div class="col-md-3" style="padding-left:0px;" ng-class="{'has-error': form.periodofinal.$invalid && form.submitted}">
                                                    <label for="periodofinal"><span translate="54.LBL_PeriodoFinal">Período Final</span> (*)</label><br />
                                                    <input id="periodofinal" name="periodofinal" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker sbx-options='{ "format":"dd/mm/yyyy" }' ng-model="model.periodofinal" required/>
                                                </div>
                                                <div class="col-md-3" style="padding-left:0px;">
                                                    <label for="cpfcnpj"><span translate="54.LBL_CPFCNPJ">CPF/CNPJ</span> </label><br />
                                                    <input id="cpfcnpj" name="cpfcnpj" type="text" maxlength="14" class="form-control input-sm" ng-model="model.cpfcnpj" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center botoes">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="54.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="widget-box" ng-show="(generating == true && dadosRelatorio == null)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4 translate="54.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio != null">
                        <div id="divRelatorio" class="table-responsive">
                            <meta charset="utf-8">
                            <div>
                                <table id='exportPDF01' class='table table-striped mensal' style="width: 100%; margin-left: auto; margin-right:auto">
                                    <colgroup>
                                        <col />
                                        <col />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <td translate="54.LBL_CPFCNPJ" style="width:150px">CPF/CNPJ</td>
                                            <td translate="54.LBL_Nome">Nome/Razão Social</td>
                                            <td translate="54.LBL_NomeUsuario">Nome do usuário</td>
                                            <td translate="54.LBL_Login">Login</td>
                                            <td translate="54.LBL_NomeAcao">Nome da ação</td>
                                            <td translate="54.LBL_DataAcao" style="width:150px">Data da ação</td>
                                            <td translate="54.LBL_Descricao">Descrição</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="data in dadosRelatorio">
                                            <td>{{data.cpfcnpj | cpfcnpj}}</td>
                                            <td>{{data.nomeassociado}}</td>
                                            <td>{{data.nomeusuario}}</td>
                                            <td>{{data.emailusuario}}</td>
                                            <td>{{data.nomeacao}}</td>
                                            <td>{{data.dataacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            <td ng-bind-html="data.descricao"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null">
                <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório de Histórico de Uso GS1','L')">
                    <i class="icon-paste bigger-110"></i>
                    <span translate="54.BTN_ExportarPDF">Exportar PDF</span>
                </button>
                <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                    <i class="icon-table bigger-110"></i>
                    <span translate="54.BTN_ExportarCSV">Exportar CSV</span>
                </button>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 340px;"></div>
        </div>
    </div>
</div>