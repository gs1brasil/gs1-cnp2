using GS1.CNP.WEBUI.Core;

namespace GS1.CNP.WEBUI.views.backoffice
{
    public partial class relatorioHistoricoUsoAssociadoGS1 : BaseWeb
    {
        public override string PermissaoPagina { get { return "AcessarRelatorioHistoricoUsoAssociadoGS1"; } }
    }
}