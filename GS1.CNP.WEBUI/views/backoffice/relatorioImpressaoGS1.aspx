﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioImpressaoGS1.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioImpressaoGS1" %>
<div ng-controller="RelatorioImpressaoGS1Ctrl">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="32.GRID_TitleRelatorioImpressao">Relatório de Impressão</label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode" translate="32.LBL_Filtros">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="{{ '32.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="32.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                       <%-- <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.cpfcnpj.$invalid && form.submitted}">
                                                <label for="cpfcnpj">CPF/CNPJ <span>(*)</span></label>
                                                <input id="cpfcnpj" name="cpfcnpj" type="text" maxlength="18" class="form-control input-sm" ng-model="model.cpfcnpj" required="required" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>--%>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.associado.$invalid && form.submitted}">
                                                <label for="associado" translate="32.LBL_NomeRazaoSocial">Nome/Razão Social</label>
                                                <input id="associado" name="associado" type="text" maxlength="255" class="form-control input-sm" ng-model="model.nome" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.32.LBL_NomeRazaoSocial' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': (form.gtin.$invalid || form.gtin.$invalidLength) && form.submitted }">
                                                <label for="gtin" translate="32.LBL_GTIN">GTIN</label>
                                                <input id="gtin" name="gtin" type="text" class="form-control input-sm" integer gtin maxlength="14" ng-model="model.gtin" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.32.LBL_GTIN' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.produto.$invalid && form.submitted}">
                                                <label for="produto" translate="32.LBL_DescricaoProduto">Descrição do Produto</label>
                                                <input id="produto" name="produto" type="text" maxlength="255" class="form-control input-sm" ng-model="model.produto" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.32.LBL_DescricaoProduto' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                                <label for="inicio" translate="32.LBL_PeriodoInicial">Período Inicial</label>
                                                <input id="inicio" name="inicio" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.inicio" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.32.LBL_PeriodoInicial' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.fim.$invalid && form.submitted}">
                                                <label for="fim" translate="32.LBL_PeriodoFinal">Período Final</label>
                                                <input id="fim" name="fim" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.fim" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.32.LBL_PeriodoFinal' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center botoes">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="32.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="widget-box" ng-show="(dadosRelatorio != null && dadosRelatorio.length == 0)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4 translate="32.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio.length > 0">
                        <div id="divRelatorio" class="table-responsive">
                            <meta charset="utf-8">
                            <div>
                                <table id='exportPDF01' class='table table-striped' style="width: 98%; margin-left: 7px">
                                    <colgroup>
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                    </colgroup>
                                    <thead>
                                        <tr class='warning'>
                                            <th translate="32.GRID_CPFCNPJ">CPF/CNPJ</th>
                                            <th translate="32.LBL_NomeRazaoSocial">Nome/Razão Social</th>
                                            <th translate="32.GRID_GTIN">GTIN</th>
                                            <th translate="32.GRID_DescricaoProduto">Descrição do Produto</th>
                                            <th translate="32.GRID_TipoImpressao">Tipo de Impressão</th>
                                            <th translate="32.GRID_DataImpressao">Data da Impressão</th>
                                            <th translate="32.GRID_TextoLivre">Texto Livre</th>
                                            <th translate="32.GRID_Fabricante">Fabricante</th>
                                            <th translate="32.GRID_FormatoEtiqueta">Formato da Etiqueta</th>
                                            <th translate="32.GRID_QuantidadeEtiquetas">Quantidade de Etiquetas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in dadosRelatorio" >
                                            <td>{{item1.cpfcnpj}}</td>
                                            <td>{{item1.nome}}</td>
                                            <td>{{item1.gtin}}</td>
                                            <td>{{item1.descricao}}</td>
                                            <td>{{item1.tipo_impressao}}</td>
                                            <td>{{item1.dataimpressao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            <td>{{item1.texto_livre}}</td>
                                            <td>{{item1.fabricante}}</td>
                                            <td>{{item1.formato_etiqueta}}</td>
                                            <td>{{item1.quantidade_etiquetas}}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <span translate="32.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatorio de Impressão')">
                    <i class="icon-paste bigger-110"></i>
                    <span translate="25.BTN_ExportarPDF">Exportar PDF</span>
                </button>
                <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                    <i class="icon-table bigger-110"></i>
                    <span translate="25.BTN_ExportarCSV">Exportar CSV</span>
                </button>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 340px;"></div>
        </div>
    </div>
</div>