﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioLocalizacaoFisicaAssociado.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioLocalizacaoFisicaAssociado" %>
<div ng-controller="RelatorioLocalizacaoFisicaAssociadoCtrl">
    <div ng-cloak ng-show="flagAssociadoSelecionado === false">
        <h1 translate="23.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="23.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
    <div ng-cloak ng-show="flagAssociadoSelecionado === true">
        <div class="form-container form-relatorios" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label translate="23.LBL_RelatorioLocalizacaoFisica">Relatório de Localização Física</label>
            </div>

            <div class="widget-box">
                <div class="widget-header">
                    <h4><span ng-show="!searchMode" translate="23.LBL_Filtros">Filtros</span></h4>
                    <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                        <i ng-click="ocultarFiltros()" style="margin-right: 5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                    </div>
                    <div class="widget-toolbar" ng-show="!collapseFiltro">
                        <a href=""><i class="icon-eraser" title="{{ '23.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                    </div>

                </div>
                <div class="widget-body">
                    <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                        <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                            <div class="tab-content">
                                <div id="usuario" class="tab-pane active">
                                    <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="23.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': (form.gln.$invalid || form.gln.invalidLength ) && form.submitted}">
                                                    <label for="gln" translate="23.LBL_GLN">GLN</label>
                                                    <input type="text" id="gln" name="gln" class="form-control input-sm" integer maxlength="13" ng-model="model.gln" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.23.LBL_GLN' | translate}}"/>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                                    <label for="nome" translate="23.LBL_NomeLocalizacaoContato">Nome da Localização/Contato</label>
                                                    <input type="text" id="nome" name="nome" class="form-control input-sm" maxlength="500" ng-model="model.nome" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.23.LBL_NomeLocalizacaoContato' | translate}}"/>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                        </div>
                                
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                                    <label for="inicio" translate="23.LBL_DataInicioAlteracao">Data de Inicio de Alteração</label>
                                                    <input id="inicio" name="inicio" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.inicio" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.23.LBL_DataInicioAlteracao' | translate}}"/>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.fim.$invalid && form.submitted}">
                                                    <label for="fim" translate="23.LBL_DataFimAlteracao">Data de Fim de Alteração</label>
                                                    <input id="fim" name="fim" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.fim" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.23.LBL_DataFimAlteracao' | translate}}"/>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-actions center botoes">
                                        <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                            <i class="icon-file bigger-110"></i>
                                            <span translate="23.BTN_GerarRelatorio">Gerar Relatório</span>
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="widget-box" ng-show="(dadosRelatorio != null && dadosRelatorio.length == 0)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
                <h4 translate="23.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
            </div>
            <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                <div class="widget-body" style="height: 660px;">
                    <div class="widget-main" style="min-height: 300px; width: 100%">
                        <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio.length > 0">
                            <div id="divRelatorio" class="table-responsive">
                                <meta charset="utf-8">
                                <div>
                                    <table id='exportPDF01' class='table table-striped' style="width: 98%; margin-left: 7px">
                                        <colgroup>
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                        </colgroup>
                                        <thead>
                                            <tr class='warning'>
                                                <th translate="23.GRID_GLN">GLN</th>
                                                <th translate="23.GRID_NomeLocalizacaoContato">Nome da Localização/Contato</th>
                                                <th translate="23.LBL_PapelFuncao">Papel/Função</th>
                                                <th translate="23.GRID_Status">Status</th>
                                                <th translate="23.GRID_RepresentanteComercial">Representante Comercial</th>
                                                <th translate="23.GRID_CEP">CEP</th>
                                                <th translate="23.GRID_Endereco">Endereço</th>
                                                <th translate="23.GRID_Numero">Número</th>
                                                <th translate="23.GRID_Complemento">Complemento</th>
                                                <th translate="23.GRID_Bairro">Bairro</th>
                                                <th translate="23.GRID_Cidade">Cidade</th>
                                                <th translate="23.GRID_Estado">Estado</th>
                                                <th translate="23.GRID_Telefone">Telefone</th>
                                                <th translate="23.GRID_Observacao">Observação</th>
                                                <th translate="23.GRID_DataAlteracao">Data de Alteração</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item1 in dadosRelatorio" >
                                                <td>{{item1.gln}}</td>
                                                <td>{{item1.nome}}</td>
                                                <td>{{item1.papel}}</td>
                                                <td>{{item1.status | status}}</td>
                                                <td>{{item1.representante}}</td>
                                                <td>{{item1.cep}}</td>
                                                <td>{{item1.endereco}}</td>
                                                <td>{{item1.numero}}</td>
                                                <td>{{item1.complemento}}</td>
                                                <td>{{item1.bairro}}</td>
                                                <td>{{item1.cidade}}</td>
                                                <td>{{item1.estado}}</td>
                                                <td>{{item1.telefone | telefone}}</td>
                                                <td>{{item1.observacao}}</td>
                                                <td>{{item1.alteracao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="15"><span translate="23.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                    <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório de Localização Física', 'L')">
                        <i class="icon-paste bigger-110"></i>
                        <span translate="23.BTN_ExportarPDF">Exportar PDF</span>
                    </button>
                    <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                        <i class="icon-table bigger-110"></i>
                        <span translate="23.BTN_ExportarCSV">Exportar CSV</span>
                    </button>
                </div>
            </div>
            <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
                <div id="chartContainer" style="width: 100%; height: 340px;"></div>
            </div>
        </div>
    </div>
</div>