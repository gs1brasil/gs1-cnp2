﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioLocalizacaoFisicaGS1.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioLocalizacaoFisicaGS1" %>
<div ng-controller="RelatorioLocalizacaoFisicaGS1Ctrl">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="33.LBL_RelatorioLocalizacaoFisica">Relatório de Localização Física</label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode" translate="33.LBL_Filtros">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="{{ '33.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="33.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <%--<div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': (form.cpfcnpj.$invalid || form.cpfcnpj.invalido) && form.submitted}">
                                                <label for="cpf_cnpj">CPF/CNPJ <span>(*)</span></label>
                                                <input id="cpfcnpj" name="cpfcnpj" type="text" class="form-control input-sm" ng-model="model.cpfcnpj" maxlength="18" ui-br-cpfcnpj-mask required="required" ng-pattern="/(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)|(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)/" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>--%>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                                <label for="nome" translate="33.LBL_NomeRazaoSocial">Nome/Razão Social</label>
                                                <input id="nome" name="nome" type="text" maxlength="255" class="form-control input-sm" ng-model="model.nome" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.33.LBL_NomeRazaoSocial' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': (form.gln.$invalid || form.gln.invalidLength ) && form.submitted}">
                                                <label for="gln" translate="33.LBL_GLN">GLN</label>
                                                <input id="gln" name="gln" type="text" class="form-control input-sm" integer maxlength="13" ng-model="model.gln" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.33.LBL_GLN' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                                <label for="nomeLocalizacao" translate="33.LBL_NomeLocalizacaoContato">Nome da Localização/Contato</label>
                                                <input type="text" id="nomeLocalizacao" name="nomeLocalizacao" class="form-control input-sm" maxlength="500" ng-model="model.nomeLocalizacao" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.33.LBL_NomeLocalizacaoContato' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                                <label for="inicio" translate="33.LBL_DataInicioAlteracao">Data de Inicio de Alteração</label>
                                                <input id="inicio" name="inicio" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.inicio" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.33.LBL_DataInicioAlteracao' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.fim.$invalid && form.submitted}">
                                                <label for="fim" translate="33.LBL_DataFimAlteracao">Data de Fim de Alteração</label>
                                                <input id="fim" name="fim" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.fim" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.33.LBL_DataFimAlteracao' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center botoes">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="33.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="widget-box" ng-show="(dadosRelatorio != null && dadosRelatorio.length == 0)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4 translate="33.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio.length > 0">
                        <div id="divRelatorio" class="table-responsive">
                            <meta charset="utf-8">
                            <div>
                                <table id='exportPDF01' class='table table-striped' style="width: 98%; margin-left: 7px">
                                    <colgroup>
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                    </colgroup>
                                    <thead>
                                        <tr class='warning'>
                                            <th translate="33.GRID_CPFCNPJ">CPF/CNPJ</th>
                                            <th translate="33.LBL_NomeRazaoSocial">Nome/Razão Social</th>
                                            <th translate="33.GRID_GLN">GLN</th>
                                            <th translate="33.GRID_NomeLocalizacaoContato">Nome da Localização/Contato</th>
                                            <th translate="33.LBL_PapelFuncao">Papel/Função</th>
                                            <th translate="33.GRID_Status">Status</th>
                                            <th translate="33.GRID_CEP">CEP</th>
                                            <th translate="33.GRID_Endereco">Endereço</th>
                                            <th translate="33.GRID_Numero">Número</th>
                                            <th translate="33.GRID_Complemento">Complemento</th>
                                            <th translate="33.GRID_Bairro">Bairro</th>
                                            <th translate="33.GRID_Cidade">Cidade</th>
                                            <th translate="33.GRID_Estado">Estado</th>
                                            <th translate="33.GRID_Pais">País</th>
                                            <th translate="33.GRID_Telefone">Telefone</th>
                                            <th translate="33.GRID_Observacao">Observação</th>
                                            <th translate="33.GRID_DataAlteracao">Data de Alteração</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in dadosRelatorio" >
                                            <td>{{item1.cpfcnpj}}</td>
                                            <td>{{item1.nome_associado}}</td>
                                            <td>{{item1.gln}}</td>
                                            <td>{{item1.nome_localizacao}}</td>
                                            <td>{{item1.papel}}</td>
                                            <td>{{item1.status | status}}</td>
                                            <td>{{item1.cep}}</td>
                                            <td>{{item1.endereco}}</td>
                                            <td>{{item1.numero}}</td>
                                            <td>{{item1.complemento}}</td>
                                            <td>{{item1.bairro}}</td>
                                            <td>{{item1.cidade}}</td>
                                            <td>{{item1.estado}}</td>
                                            <td>{{item1.pais}}</td>
                                            <td>{{item1.telefone | telefone}}</td>
                                            <td>{{item1.observacao}}</td>
                                            <td>{{item1.alteracao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="17"><span translate="33.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório de Localização Física', 'L')">
                    <i class="icon-paste bigger-110"></i>
                    <span translate="33.BTN_ExportarPDF">Exportar PDF</span>
                </button>
                <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                    <i class="icon-table bigger-110"></i>
                    <span translate="33.BTN_ExportarCSV">Exportar CSV</span>
                </button>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 340px;"></div>
        </div>
    </div>
</div>