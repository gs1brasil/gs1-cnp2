﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioLogErros.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioLogErros" %>

<div ng-controller="RelatorioLogErrosCtrl" ng-init="formMode = false; searchMode = false; exibeVoltar = false; isPesquisa = false; validaEMail = false; entity = 'usuario'; typeUser = 'Usuário GS1'; listaContatos = [];">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="31.LBL_TitleRelatorioLogErros">Relatório de Log de Erros</label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode" translate="31.LBL_Filtros">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor:pointer">    
                    <i ng-click="ocultarFiltros()" style="margin-right:5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>                 
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">                    
                    <a href=""><i class="icon-eraser" title="{{ '31.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active" id="filtros">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both; height:250px">
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="31.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8"  ng-class="{'has-error': form.datainicial.$invalid && form.submitted}">
                                                 <label for="datainicial" translate="31.LBL_DataInicial">Data Inicial (*)</label>
                                                <input type="text" id="datainicial" name="datainicial" class="form-control input-sm datepicker" ng-model="datainicial" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="10"  required  
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.31.LBL_DataInicial' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.datafinal.$invalid && form.submitted}">
                                                <label for="datafinal" translate="31.LBL_DataFinal">Data Final (*)</label>
                                                <input type="text" id="datafinal" name="datafinal" class="form-control input-sm datepicker" ng-model="datafinal" sbx-datepicker data-date-format="dd/mm/yyyy" maxlength="10" ng-click="datepicker()" required 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.31.LBL_DataFinal' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div> 
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8">
                                                <label for="usuario" style="display: block" translate="31.LBL_Usuario">Usuário</label>
                                                <input type="text" ng-blur="verificaAutoCompleteUsuario()" ng-model="usuario" maxlength="255" id="usuario" placeholder="{{ '31.PLACEHOLDER_DigiteNomeUsuario' | translate }}" typeahead="usuarios as usuarios.nome for usuarios in BuscarUsuario($viewValue)" typeahead-min-length="3" class="form-control" name="usuario"
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.31.LBL_Usuario' | translate}}">
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8">
                                                <label for="usuario" style="display: block" translate="31.LBL_Email">Email</label>
                                                <input type="text" ng-model="email" maxlength="255" id="Text1" class="form-control" name="email" data-toggle="popover" data-trigger="hover" 
                                                    data-container="body" data-content="{{'t.31.LBL_Email' | translate}}">
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>                                   
                                </fieldset>

                                <div class="form-actions center">                                    
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="31.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>                                                                       
                                </div>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length == 0" style="height: 50px;text-align: center;margin-top: 30px;border-bottom: none;" >
            <h4 translate="31.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <%--<div class="widget-box" ng-cloak ng-if="dadosRelatorio != null && dadosRelatorio.length > 0 && (usuario == '' || usuario == undefined || usuario == null)" style="height: 50px;margin-left: 20px;margin-top: 20px;border-bottom: none;" >
            <h4>Relatório de Log de Erros</h4>
            <span style="font-weight: bold">Período: {{datainicial | date:'dd/MM/yyyy'}} à {{datafinal | date:'dd/MM/yyyy'}} </span><br />
            <span style="font-weight: bold">Usuário: Todos</span>
        </div>
        <div class="widget-box" ng-cloak ng-if="dadosRelatorio != null && dadosRelatorio.length > 0 && (usuario != '' && usuario != undefined && usuario != null && usuario.nome != undefined)" style="height: 50px;margin-left: 20px;margin-top: 20px;border-bottom: none;" >
            <h4>Relatório de Log de Erros</h4>
            <span style="font-weight: bold">Período: {{datainicial | date:'dd/MM/yyyy'}} à {{datafinal | date:'dd/MM/yyyy'}} </span><br />
            <span style="font-weight: bold">Usuário: {{usuario.nome}}</span>
        </div>--%>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
            <div class="widget-body" style="height: 660px;" >                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                <div class="widget-main" style="min-height: 300px; width: 100%; margin-top: 35px;">                   
                    <div style="height: 536px; overflow-y: scroll; overflow-x: scroll;" id="relatorioPDF" ng-if="dadosRelatorio.length > 0"> 
                    
                    <div id="divRelatorio" class="table-responsive">
                        <meta charset="utf-8">               
                        <table id='exportPDF01' class='table table-striped' style="width:98%;margin-left: 7px">
                            <colgroup>
                                <col width='8%'>
                                <col width='5%'>
                                <col width='18%'>
                                <col width='10%'>
                                <col width='10%'>
                                <col width='10%'>
                                <col width='10%'>  
                                <col width='10%'>
                                <col width='10%'>        
                                <col width='7%'>       
                            </colgroup>
                            <thead>
                                <tr class='warning'>
                                    <th translate="31.GRID_Data">Data</th>
                                    <th translate="31.GRID_Hora">Hora</th>
                                    <th translate="31.GRID_Mensagem">Mensagem</th>
                                    <th translate="31.GRID_Detalhe" class="removerRelatorio">Detalhe</th>
                                    <th translate="31.GRID_Nome">Nome</th>
                                    <th translate="31.GRID_Email">E-mail</th>
                                    <th translate="31.GRID_URL">URL</th>
                                    <th translate="31.GRID_IP">IP</th>
                                    <th translate="31.GRID_Navegador">Navegador</th>
                                    <th translate="31.GRID_VersaoNavegador">Versão do Navegador</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in dadosRelatorio">
                                    <td>{{item1.datalog | date:'dd/MM/yyyy'}}</td>
                                    <td>{{item1.datalog | date:'HH:mm:ss'}}</td>
                                    <td style="white-space:pre;" class="removequebralinha">{{item1.mensagem | formatQuebraLinha: 70}}</td>
                                    <td class="removerRelatorio" style="text-align: center"><a style="text-decoration: none" href="" ng-click="geraDetalhes(item1.descricao)"><i class="icon-eye-open" title="{{ '31.TOOLTIP_DetalhesLogErro' | translate }}"></i></a></td>
                                    <td>{{item1.nome}}</td>
                                    <td>{{item1.email}}</td>
                                    <td>{{item1.url}}</td>
                                    <td>{{item1.ip}}</td>
                                    <td>{{item1.navegador}}</td>
                                    <td>{{item1.versaonavegador}}</td>
                                </tr>  
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="10"><span translate="31.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>                                
                                </tr>
                            </tfoot>
                        </table>
                    </div>                                                                  
                </div>
                                
                    <div class="form-actions center" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">                                    
                    <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório de Log de Erros','L')">
                        <i class="icon-paste bigger-110"></i>
                        <span translate="31.BTN_ExportarPDF">Exportar PDF</span>
                    </button> 
                    <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                        <i class="icon-table bigger-110"></i>
                        <span translate="31.BTN_ExportarCSV">Exportar CSV</span>
                    </button>
                                                                       
                </div>
                </div>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">            
            <div id="chartContainer" style="width:100%; height:300px;"></div>            
        </div>        
    </div>
</div>
