﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioLogMonitoramento.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioLogMonitoramento" %>

<div ng-controller="RelatorioLogMonitoramentoCtrl" ng-init="formMode = false; searchMode = false; exibeVoltar = false; isPesquisa = false; validaEMail = false; entity = 'usuario'; typeUser = 'Usuário GS1'; listaContatos = [];">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="30.LBL_TitleRelatorioLogMonitoramento">Relatório de Log de Monitoramento de Ação</label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                 <h4><span ng-show="!searchMode" translate="30.LBL_Filtros">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px; cursor: pointer;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                 <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="{{ '30.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both; height: 300px">
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="30.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-4">
                                            <div class="form-group col-md-12" ng-class="{'has-error': form.datainicial.$invalid && form.submitted}">
                                                <label for="data" translate="30.LBL_Data">Data</label> (*)
                                                <input type="text" class="form-control input-sm datepicker" id="data" sbx-datepicker data-date-format="dd/mm/yyyy" ng-click="datepicker()" maxlength="255" name="datainicial" ng-model="datainicial" required 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.30.LBL_Data' | translate}}"/>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group col-md-12">
                                                <label for="url" translate="30.LBL_URL">Url</label>
                                                <input type="text" ng-model="tela" id="url" maxlength="255" placeholder="{{ '30.PLACEHOLDER_InformeURL' | translate }}" class="form-control"
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.30.LBL_URL' | translate}}">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group col-md-12">
                                                <label for="pk" translate="30.LBL_CodigoEntidade">Código da Entidade</label>
                                                <input type="text" id="pk" ng-model="pk" maxlength="255" placeholder="{{ '30.PLACEHOLDER_DigiteCodigoEntidade' | translate }}" class="form-control" integer 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.30.LBL_CodigoEntidade' | translate}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group col-md-12" ng-class="{'has-error': form.razaosocial.$invalid && form.submitted}">

                                                <label for="razaosocial"><span translate="54.LBL_NomeRazaoSocial">Nome/Razão Social</span> (*)</label><br />
                                                <input id="razaosocial" name="razaosocial" type="text" maxlength="255" class="form-control input-sm" ng-model="razaosocial"
                                                    ng-blur="verificaAutoCompleteRazaoSocial()" typeahead-on-select="selecionaAssociado(razaosocial);" placeholder="Digite a Razão Social" 
                                                    typeahead="razoessociais as razoessociais.nome for razoessociais in BuscarRazaoSocial($viewValue)" typeahead-min-length="3" required
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.30.LBL_NomeRazaoSocial' | translate}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group col-md-12">
                                                <label for="usuario" translate="30.LBL_Usuario">Usuário</label>
                                                <select class="form-control input-sm" id="nomeusuario" 
                                                    ng-class="{'fieldDisabled': model.razaosocial}"
                                                    ng-disabled="razaosocial.nome == undefined || razaosocial.nome == ''" ng-model="usuario" name="usuario"
                                                    ng-options="tipo.codigo as tipo.nome for tipo in Usuarios"
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.30.LBL_Usuario' | translate}}" />
                                                <%--<input type="text" id="usuario" ng-blur="verificaAutoCompleteUsuario()" ng-model="usuario"  maxlength="255" placeholder="{{ '30.PLACEHOLDER_DigiteNomeUsuario' | translate }}" 
                                                    typeahead="usuarios as usuarios.nome for usuarios in BuscarUsuario($viewValue)" typeahead-min-length="3" class="form-control" disa
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.30.LBL_Usuario' | translate}}">--%>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                <div class="form-actions center">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="30.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length == 0" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4 translate="30.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: scroll; overflow-x: scroll;" ng-if="dadosRelatorio.length > 0">
                        <div id="divRelatorio" class="table-responsive" style="overflow: visible">
                            <meta charset="utf-8">
                            <table id='exportPDF01' class='table table-striped' style="width: 96%; margin-left: 7px;font-size: 14px;">
                                <colgroup>
                                    <col width='10%'>
                                    <col width='10%'>
                                    <col width='10%'>
                                    <col width='10%'>
                                    <col width='10%'>
                                    <col width='10%'>
                                    <col width='10%'>
                                    <col width='11%'>
                                    <col width='11%'>
                                </colgroup>
                                <thead>
                                    <tr class='warning'>
                                        <th translate="30.GRID_DataAlteracao">Data de Alteração</th>
                                        <th translate="30.GRID_Nome">Nome</th>
                                        <th translate="30.GRID_URL">URL</th>
                                        <th translate="30.GRID_Classe">Classe</th>
                                        <th translate="30.GRID_Metodo">Método</th>
                                        <th translate="30.GRID_Requisicao">Requisição</th>
                                        <th translate="30.GRID_CodigoEntidade">Codigo da Entidade</th>
                                        <th translate="30.GRID_TipoUsuario">Tipo de Usuário</th>
                                        <th translate="30.GRID_Perfil">Perfil</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item1 in dadosRelatorio" >
                                        <td>{{item1.dataalteracao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                        <td>{{item1.nome}}</td>
                                        <td>{{item1.url}}</td>
                                        <td>{{item1.classe}}</td>
                                        <td>{{item1.metodo}}</td>
                                        <td style="white-space:pre;" class="removequebralinha">{{item1.requisicao | formatQuebraLinha: 70}}</td>
                                        <td>{{item1.codigoentidade}}</td>
                                        <td>{{item1.tipousuario}}</td>
                                        <td>{{item1.perfil}}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="9"><span translate="30.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>                                
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="form-actions center" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                        <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório de Log de Monitoramento de Ação', 'L')">
                            <i class="icon-paste bigger-110"></i>
                            <span translate="30.BTN_ExportarPDF">Exportar PDF</span>
                        </button>
                        <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                            <i class="icon-table bigger-110"></i>
                            <span translate="30.BTN_ExportarCSV">Exportar CSV</span>
                        </button>

                    </div>
                </div>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 300px;"></div>
        </div>
    </div>
</div