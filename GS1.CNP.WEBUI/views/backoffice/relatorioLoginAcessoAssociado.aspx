<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioLoginAcessoAssociado.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioLoginAcessoAssociado" %>
<div ng-controller="RelatorioLoginAcessoAssociadoCtrl">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label>Relatório de </label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="Limpar" ng-click="onClean(item)"></i></a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - Campos obrigatórios</div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.nomeusuario.$invalid && form.submitted}">
                                                <label for="nomeusuario">Nome do Usuário </label>
                                                <input id="nomeusuario" name="nomeusuario" type="text" maxlength="255" class="form-control input-sm" ng-model="model.nomeusuario" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.login.$invalid && form.submitted}">
                                                <label for="login">Login </label>
                                                <input id="login" name="login" type="text" maxlength="255" class="form-control input-sm" ng-model="model.login" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                                <label for="inicio">Período Inicial </label>
                                                <input id="inicio" name="inicio" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.inicio" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                
                                        <div class="col-md-6">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.fim.$invalid && form.submitted}">
                                                <label for="fim">Período Final </label>
                                                <input id="fim" name="fim" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.fim" />
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center botoes">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        Gerar Relatório
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="widget-box" ng-show="(dadosRelatorio != null && dadosRelatorio.length == 0)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4>Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio.length > 0">
                        <div id="divRelatorio table-responsive">
                            <meta charset="utf-8">
                            <div>
                                <table id='exportPDF01' class='table table-striped' style="width: 98%; margin-left: 7px">
                                    <colgroup>
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                        <col />
                                    </colgroup>
                                    <thead>
                                        <tr class='warning'>
                                            <th>Nome do Usuário</th>
                                            <th>Login</th>
                                            <th>Perfil</th>
                                            <th>Último Acesso</th>
                                            <th>Data de Inclusão</th>
                                            <th>Data de Expiração da Senha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="item1 in dadosRelatorio" >
                                            <td>{{item1.nome_usuario}}</td>
                                            <td>{{item1.login_usuario}}</td>
                                            <td>{{item1.perfil}}</td>
                                            <td>{{item1.data_ultimo_acesso | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            <td>{{item1.data_criacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                            <td>{{item1.data_expiracao_senha | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6">Total: {{tipoGS1.length}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions center" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatorio de ')">
                    <i class="icon-paste bigger-110"></i>
                    Exportar PDF
                </button>
                <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                    <i class="icon-table bigger-110"></i>
                    Exportar CSV
                </button>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 340px;"></div>
        </div>
    </div>
</div>