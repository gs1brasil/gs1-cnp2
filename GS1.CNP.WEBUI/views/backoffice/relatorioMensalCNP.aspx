﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioMensalCNP.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioMensalCNP" %>
<div ng-controller="RelatorioMensalCNPCtrl">
    <div class="form-container form-relatorios" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label translate="43.LBL_TitleRelatorioMensal">Relatório Totalizador - CNP 2.0</label>
        </div>

        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode" translate="43.LBL_Filtros">Filtros </span></h4>
                <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                    <i ng-click="ocultarFiltros()" style="margin-right: 5px;" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                </div>
                <div class="widget-toolbar" ng-show="!collapseFiltro">
                    <a href=""><i class="icon-eraser" title="{{ '43.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                </div>

            </div>
            <div class="widget-body">
                <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                    <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                        <div class="tab-content">
                            <div id="usuario" class="tab-pane active">
                                <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="43.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                                <label for="tipoconsulta" translate="43.LBL_TipoConsulta">Tipo de Consulta </label><br />
                                                <input type="radio" name="tipo" value="0" ng-model="model.tipo" id="total" /> <label for="total" translate="43.LBL_Total">Total</label>&nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="tipo" value="1" ng-model="model.tipo" id="periodo" checked="checked" /> <label for="periodo" translate="43.LBL_Mensal">Mensal</label>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <div class="col-md-6" ng-if="model.tipo == 1">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.mes.$invalid && form.submitted}">
                                                <div class="col-md-6" style="padding-left:0px;">
                                                    <label for="mes"><span translate="43.LBL_Mes">Mês</span> (*)</label><br />
                                                    <input id="mes" name="mes" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker sbx-options='{ "format": "mm/yyyy", "startView":"months", "minViewMode":"months" }' ng-model="model.mes" required
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.43.LBL_Mes' | translate}}"/>
                                                </div>
                                                <div class="col-md-6"></div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group col-md-8" ng-class="{'has-error': form.tipoetiqueta.$invalid && form.submitted}">
                                                <div class="col-md-6" style="padding-left:0px;">
                                                    <label for="mes"><span translate="43.LBL_TipoEtiqueta">Tipo de Etiqueta</span></label><br />
                                                    <select class="form-control input-sm" id="tipoetiqueta" name="tipoetiqueta" ng-model="model.tipoetiqueta" required
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.43.LBL_TipoEtiqueta' | translate}}">
                                                        <option value="0" ng-selected="model.tipoetiqueta == 0">Todos</option>
                                                        <option value="-1">Nenhum</option>
                                                        <option value="1">EAN-8</option>
                                                        <option value="2">EAN-12 (UPC-A)</option>
                                                        <option value="3">EAN-13</option>
                                                        <option value="4">ITF-14</option>
                                                        <option value="5">GS1-128</option>
                                                        <option value="6">GS1 Datamatrix</option>
                                                        <option value="7">GS1 Databar</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6"></div>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-actions center botoes">
                                    <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                        <i class="icon-file bigger-110"></i>
                                        <span translate="43.BTN_GerarRelatorio">Gerar Relatório</span>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="widget-box" ng-show="(generating == true && dadosRelatorio == null && model.mes != undefined && model.mes != '')" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
            <h4 translate="43.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
        </div>
        <div class="widget-box" ng-show="dadosRelatorio != null">
            <div class="widget-body" style="height: 660px;">
                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio != null">
                        <div id="divRelatorio" class="table-responsive">
                            <meta charset="utf-8">
                            <div>
                                <table id='exportPDF01' class='table table-striped mensal' style="width: 60%; margin-left: auto; margin-right:auto">
                                    <colgroup>
                                        <col />
                                        <col />
                                    </colgroup>
                                    <tbody>
                                        <tr>
                                            <td translate="43.LBL_TotalEmpresas">Total de empresas</td>
                                            <td>{{dadosRelatorio.totalempresas || 0}}</td>
                                        </tr>
                                        <tr>
                                            <td translate="43.LBL_TotalProdutos">Total de produtos</td>
                                            <td>{{dadosRelatorio.totalprodutos || 0}}</td>
                                        </tr>
                                        <tr>
                                            <td translate="43.LBL_TotalAcessoDia">Total de acessos por dia</td>
                                            <td>{{dadosRelatorio.totalacessosdia || 0}}</td>
                                        </tr>
                                        <tr>
                                            <td translate="43.LBL_TotalAcessoMes">Total de acessos por mês</td>
                                            <td>{{dadosRelatorio.totalacessosmes || 0}}</td>
                                        </tr>
                                        <tr>
                                            <td translate="43.LBL_TotalUsuarios">Total de usuários</td>
                                            <td>{{dadosRelatorio.totalusuarios || 0}}</td>
                                        </tr>
                                        <tr>
                                            <td translate="43.LBL_QuantidadeOperacoesImpressaoEtiqueta">Quantidade de operações de impressão de etiquetas</td>
                                            <td>{{dadosRelatorio.qntoperacoesimpetiquetas}}</td>
                                        </tr>
                                        <tr>
                                            <td translate="43.LBL_EmpresasImprimemEtiquetas">Empresas que imprimem etiquetas</td>
                                            <td>{{dadosRelatorio.empresasimpetiquetas}}</td>
                                        </tr>
                                        <tr ng-if="tipoetiqueta == 0 || tipoetiqueta == 1">
                                            <td translate="43.LBL_TOTALETIQUETASEAN8">Total de etiquetas EAN-8</td>
                                            <td>{{dadosRelatorio.totaletiquetasean8}}</td>
                                        </tr>
                                        <tr ng-if="tipoetiqueta == 0 || tipoetiqueta == 2">
                                            <td translate="43.LBL_TOTALETIQUETASEAN12">Total de etiquetas EAN-12 (UPC-A)</td>
                                            <td>{{dadosRelatorio.totaletiquetasean12}}</td>
                                        </tr>
                                        <tr ng-if="tipoetiqueta == 0 || tipoetiqueta == 3">
                                            <td translate="43.LBL_TOTALETIQUETASEAN13">Total de etiquetas EAN-13</td>
                                            <td>{{dadosRelatorio.totaletiquetasean13}}</td>
                                        </tr>
                                        <tr ng-if="tipoetiqueta == 0 || tipoetiqueta == 4">
                                            <td translate="43.LBL_TOTALETIQUETASITF14">Total de etiquetas ITF-14</td>
                                            <td>{{dadosRelatorio.totaletiquetasitf14}}</td>
                                        </tr>
                                        <tr ng-if="tipoetiqueta == 0 || tipoetiqueta == 5">
                                            <td translate="43.LBL_TOTALETIQUETASGS1128">Total de etiquetas GS1-128</td>
                                            <td>{{dadosRelatorio.totaletiquetasgs1128}}</td>
                                        </tr>
                                        <tr ng-if="tipoetiqueta == 0 || tipoetiqueta == 6">
                                            <td translate="43.LBL_TOTALETIQUETASDATABAR">Total de etiquetas GS1 Databar</td>
                                            <td>{{dadosRelatorio.totaletiquetasdatabar}}</td>
                                        </tr>
                                        <tr ng-if="tipoetiqueta == 0 || tipoetiqueta == 7">
                                            <td translate="43.LBL_TOTALETIQUETASDATAMATRIX">Total de etiquetas GS1 Datamatrix</td>
                                            <td>{{dadosRelatorio.totaletiquetasdatamatrix}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null">
                <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatório Totalizador - CNP 2.0')">
                    <i class="icon-paste bigger-110"></i>
                    <span translate="43.BTN_ExportarPDF">Exportar PDF</span>
                </button>
                <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                    <i class="icon-table bigger-110"></i>
                    <span translate="43.BTN_ExportarCSV">Exportar CSV</span>
                </button>
            </div>
        </div>
        <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
            <div id="chartContainer" style="width: 100%; height: 340px;"></div>
        </div>
    </div>
</div>