﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatorioProdutoAssociado.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatorioProdutoAssociado" %>
<div ng-controller="RelatorioProdutoAssociadoCtrl">
    <div ng-cloak ng-show="flagAssociadoSelecionado === false">
        <h1 translate="25.LBL_AssociadoNaoSelecionado">Empresa Associada não foi selecionada</h1>
        <h4 translate="25.LBL_PesquisaNomeCPFCNPJCAD">Pesquise pelo nome, CPF/CNPJ ou CAD do associado no canto superior direito do sistema.</h4>
    </div>
    <div ng-cloak ng-show="flagAssociadoSelecionado === true">
        <div class="form-container form-relatorios" form-container>
            <div class="page-header">
                <i class="icon-align-justify"></i>
                <label translate="25.LBL_TitleRelatorioProduto">Relatório de Produto</label>
            </div>

            <div class="widget-box">
                <div class="widget-header">
                    <h4><span ng-show="!searchMode" translate="25.LBL_Filtros">Filtros </span></h4>
                    <div class="widget-toolbar" ng-show="dadosRelatorioGrafico !=null || dadosRelatorio != null" style="cursor: pointer">
                        <i ng-click="ocultarFiltros()" style="margin-right: 5px;" title="{{collapseFiltro ? 'Exibir Filtros' : 'Esconder Filtros'}}" ng-class="{'icon-chevron-sign-up': !collapseFiltro, 'icon-chevron-sign-down': collapseFiltro}"></i>
                    </div>
                    <div class="widget-toolbar" ng-show="!collapseFiltro">
                        <a href=""><i class="icon-eraser" title="{{ '25.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"></i></a>
                    </div>

                </div>
                <div class="widget-body">
                    <div class="widget-main no-padding" collapse="collapseFiltro" id="filtros">
                        <form name="form" id="form" novalidate class="form" role="form" ng-submit="gerarRelatorio()">
                            <div class="tab-content">
                                <div id="usuario" class="tab-pane active">
                                    <fieldset style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="25.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.gtin.$invalid && form.submitted}">
                                                    <label for="gtin" translate="25.LBL_GTIN">GTIN</label>
                                                    <input type="text" id="gtin" name="gtin" class="form-control input-sm" ng-model="model.gtin" integer gtin tipo-gtin="model.tipogtin" maxlength="{{model.tipogtin ? maxlengthGtin[model.tipogtin] : 14}}" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.25.LBL_GTIN' | translate}}"/>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.produto.$invalid && form.submitted}">
                                                    <label for="produto" translate="25.LBL_DescricaoProduto">Descrição do Produto</label>
                                                    <input id="produto" name="produto" type="text" maxlength="255" class="form-control input-sm" ng-model="model.produto" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.25.LBL_DescricaoProduto' | translate}}"/>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                        </div>
                                
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.inicio.$invalid && form.submitted}">
                                                    <label for="inicio" translate="25.LBL_DataInicialCadastro">Data Inicial de Cadastro</label>
                                                    <input id="inicio" name="inicio" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.inicio" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.25.LBL_DataInicialCadastro' | translate}}"/>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.fim.$invalid && form.submitted}">
                                                    <label for="fim" translate="25.LBL_DataFimCadastro">Data Fim de Cadastro</label>
                                                    <input id="fim" name="fim" type="text" maxlength="10" class="form-control input-sm datepicker" sbx-datepicker data-date-format="dd/mm/yyyy" ng-model="model.fim" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.25.LBL_DataFimCadastro' | translate}}"/>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                        </div>
                                
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.statusgtin.$invalid && form.submitted}">
                                                    <label for="statusgtin" translate="25.LBL_StatusGTIN">Status do GTIN</label>
                                                    <select id="statusgtin" name="statusgtin" class="form-control " ng-model="model.statusgtin"  ng-options="obj.key as obj.value for obj in listastatusgtin track by obj.key" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.25.LBL_StatusGTIN' | translate}}">
                                                        <option value="" translate="25.LBL_Selecione">Selecione</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group col-md-8" ng-class="{'has-error': form.tipogtin.$invalid && form.submitted}">
                                                    <label for="tipogtin" translate="25.LBL_TipoGTIN">Tipo de GTIN</label>
                                                    <select id="tipogtin" name="tipogtin" class="form-control " ng-model="model.tipogtin"  ng-options="obj.key as obj.value for obj in listatipogtin track by obj.key"
                                                        ng-change="changeTipoGtin();"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.25.LBL_TipoGTIN' | translate}}">
                                                            <option value="" translate="25.LBL_Selecione">Selecione</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-actions center botoes">
                                        <button id="submit" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="gerarRelatorio()">
                                            <i class="icon-file bigger-110"></i>
                                            <span translate="25.BTN_GerarRelatorio">Gerar Relatório</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="widget-box" ng-show="(dadosRelatorio != null && dadosRelatorio.length == 0)" style="height: 50px; text-align: center; margin-top: 30px; border-bottom: none;">
                <h4 translate="25.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</h4>
            </div>
            <div class="widget-box" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                <div class="widget-body" style="height: 660px;">
                    <div class="widget-main" style="min-height: 300px; width: 100%">
                        <div style="height: 536px; overflow-y: auto; overflow-x: auto;" ng-if="dadosRelatorio.length > 0">
                            <div id="divRelatorio" class="table-responsive">
                                <meta charset="utf-8">
                                <div>
                                    <table id='exportPDF01' class='table table-striped' style="width: 98%; margin-left: 7px">
                                        <colgroup>
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                            <col />
                                        </colgroup>
                                        <thead>
                                            <tr class='warning'>
                                                <th translate="25.GRID_GTIN">GTIN</th>
                                                <th translate="25.GRID_TipoGTIN">Tipo GTIN</th>
                                                <th translate="25.GRID_Marca">Marca</th>
                                                <th translate="25.GRID_StatusProduto">Status Produto</th>
                                                <th translate="25.GRID_AgenciaRegistro">Agência Reguladora</th>
                                                <th translate="25.GRID_CodigoRegistro">Código Reguladora</th>
                                                <th translate="25.GRID_UsuarioCriacao">Usuário Criação</th>
                                                <th translate="25.GRID_DescricaoProduto">Descrição Produto</th>
                                                <th translate="25.GRID_DataCriacao">Data Criação</th>
                                                <th translate="25.GRID_DataSuspensao">Data Suspensão</th>
                                                <th translate="25.GRID_DataReativacao">Data Reativação</th>
                                                <th translate="25.GRID_DataCancelamento">Data Cancelamento</th>
                                                <th translate="25.GRID_DataReutilizacao">Data Reutilização</th>
                                                <th translate="25.GRID_PaisDestino">País Destino</th>
                                                <th translate="25.GRID_PaisOrigem">País Origem</th>
                                                <th translate="25.GRID_TipoProduto">Tipo Produto</th>
                                                <th translate="25.GRID_Lingua">Língua</th>
                                                <th translate="25.GRID_Estado">Estado</th>
                                                <th translate="25.GRID_Largura">Largura</th>
                                                <th translate="25.GRID_Profundidade">Profundidade</th>
                                                <th translate="25.GRID_Altura">Altura</th>
                                                <th translate="25.GRID_PesoLiquido">Peso Líquido</th>
                                                <th translate="25.GRID_PesoBruto">Peso Bruto</th>
                                                <th translate="25.GRID_AliquotaIPI">Alíquota IPI</th>
                                                <th translate="25.GRID_TipoURL">Tipo URL</th>
                                                <th translate="25.GRID_URL">URL</th>
                                                <th translate="25.GRID_URLImagem">URL Imagem</th>
                                                <th translate="25.GRID_CompartilhaDados">Compartilha Dados</th>
                                                <th translate="25.GRID_Observacoes">Observações</th>
                                                <th translate="25.GRID_NCM">NCM</th>
                                                <th translate="25.GRID_GTINInferior">GTIN Inferior</th>
                                                <th translate="25.GRID_DescricaoGTINInferior">Descrição GTIN Inferior</th>
                                                <th translate="25.GRID_QuantidadeItens">Quantidade Itens</th>
                                                <th translate="25.GRID_Segmento">Segmento</th>
                                                <th translate="25.GRID_Familia">Família</th>
                                                <th translate="25.GRID_Classe">Classe</th>
                                                <th translate="25.GRID_SubClasse">Sub Classe</th>
                                                <th translate="25.GRID_AtributoBrick">Atributo Brick</th>
                                                <th translate="25.GRID_ValorAtributo">Valor Atributo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="item1 in dadosRelatorio" >
                                                <td>{{item1.globaltradeitemnumber}}</td>
                                                <td>{{item1.tipo_gtin}}</td>
                                                <td>{{item1.brandname}}</td>
                                                <td>{{item1.statusproduto}}</td>
                                                <td>{{item1.agenciaregistro}}</td>
                                                <td>{{item1.alternateitemidentificationid}}</td>
                                                <td>{{item1.usuariocriacao}}</td>
                                                <td>{{item1.productdescription}}</td>
                                                <td>{{item1.datainclusao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.datasuspensao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.datareativacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.datacancelamento | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.datareutilizacao | date:'dd/MM/yyyy HH:mm:ss'}}</td>
                                                <td>{{item1.paisdestino}}</td>
                                                <td>{{item1.paisorigem}}</td>
                                                <td>{{item1.tipoproduto}}</td>
                                                <td>{{item1.lingua}}</td>
                                                <td>{{item1.estado}}</td>
                                                <td>{{item1.largura}}</td>
                                                <td>{{item1.profundidade}}</td>
                                                <td>{{item1.altura}}</td>
                                                <td>{{item1.peso_liquido}}</td>
                                                <td>{{item1.peso_bruto}}</td>
                                                <td>{{item1.ipiperc}}</td>
                                                <td>{{item1.tipourl}}</td>
                                                <td>{{item1.url}}</td>
                                                <td>{{item1.url_imagem}}</td>
                                                <td>{{item1.compartilhadados}}</td>
                                                <td>{{item1.observacoes}}</td>
                                                <td>{{item1.ncm}}</td>
                                                <td>{{item1.gtin_inferior}}</td>
                                                <td>{{item1.descricao_inferior}}</td>
                                                <td>{{item1.quantidade}}</td>
                                                <td>{{item1.segmento}}</td>
                                                <td>{{item1.familia}}</td>
                                                <td>{{item1.classe}}</td>
                                                <td>{{item1.brick}}</td>
                                                <td>{{item1.atributobrick}}</td>
                                                <td>{{item1.valoratributobrick}}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="41"><span translate="25.LBL_Total">Total</span>: {{dadosRelatorio.length}}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions center" ng-controller="RelatorioCtrl" ng-show="dadosRelatorio != null && dadosRelatorio.length > 0">
                    <button id="Button2" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarPDF('Relatorio de Produto')">
                        <i class="icon-paste bigger-110"></i>
                        <span translate="25.BTN_ExportarPDF">Exportar PDF</span>
                    </button>
                    <button id="Button3" name="submit" class="btn btn-info btn-sm" scroll-to="" type="button" ng-click="exportarCSV()">
                        <i class="icon-table bigger-110"></i>
                        <span translate="25.BTN_ExportarCSV">Exportar CSV</span>
                    </button>
                </div>
            </div>
            <div class="widget-box" id="corpoRelatorio" style="margin-top: 60px;">
                <div id="chartContainer" style="width: 100%; height: 340px;"></div>
            </div>
        </div>
    </div>
</div>