using GS1.CNP.WEBUI.Core;

namespace GS1.CNP.WEBUI.views.backoffice
{
    public partial class relatorioProdutoGS1 : BaseWeb
    {
        public override string PermissaoPagina { get { return "AcessarRelatorioProdutoGS1"; } }
    }
}