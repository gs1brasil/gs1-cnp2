﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="relatoriosAssociado.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.relatoriosAssociado" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#views/backoffice/relatoriosAssociado";
    }
</script>

<div class="form-relatorios" form-container> <%-- form-container --%>
    <div class="page-header">
        <i class="icon-align-justify"></i>
        <label translate="19.LBL_GerarRelatorio">Gerar Relatório</label>
    </div>

    <div class="widget-box">
        <div class="widget-header">
            <h4><span translate="19.LBL_Relatorios">Relatórios</span></h4>
            <div class="pull-right" style="margin-top: 2px; margin-right: 6px;">
                <ajuda codigo="58" tooltip-ajuda="'19.TOOLTIP_Ajuda'"></ajuda>
            </div>
        </div>

        <div class="widget-box" style="margin: 0px;">

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <div ng-controller="RelatoriosAssociadoCtrl" class="margin-left-style" style="height: 600px;"> <%--page-container--%>
                        <br />
                        <div class="row">
                            <div class="form-group col-md-12" style="font-size: 90%;" translate="19.LBL_CliqueRelatorioVisualizar">
                                Clique em um relatório para visualizá-lo.
                            </div>
                        </div>
                        <ul>
                            <li ng-repeat="relatorio in relatorios">
                                <a href="{{relatorio.url}}">{{relatorio.nome}}</a><br />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>