﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="termosAdesao.aspx.cs" Inherits="GS1.CNP.WEBUI.views.backoffice.termosAdesao" %>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<style type="text/css">

    .wysiwyg-menu{
        text-align: center !important;
    }
</style>

<div ng-controller="TermosAdesao" class="page-container" ng-init="entity = 'usuario'; " style="min-height: 720px;">
    <div class="form-container" form-container id="formcontainer"> <%--id usado para corrigir bug do ie no layout, no momento de mudar do grid para o form--%>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label>{{ titulo }}</label>
        </div>
        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" ng-show="searchMode" ng-click="onClean(item)" name="limpar" title="{{ '9.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="22" ng-show="searchMode" tooltip-ajuda="'9.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="23" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'9.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="24" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'9.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>
        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div class="row" ng-if="!isPesquisa">
                            <div class="form-group col-md-12" ng-class="{'has-error': form.html.$invalid && form.submitted}">
                                <wysiwyg textarea-id="html"
                                      textarea-class="form-control"
                                      textarea-height="400px"
                                      textarea-name="html"
                                      textarea-required
                                      ng-model="item.html"
                                      enable-bootstrap-title="true"
                                      textarea-menu="menuWysiwyg"
                                      disabled="isVisualize"
                                      required
                                      data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.9.LBL_termoAdesao' | translate}}">
                                </wysiwyg>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6" ng-class="{'has-error': form.codigotipoaceite.$invalid && form.submitted}">
                                <label for="tipoaceite"><span translate="9.LBL_QuemAceitaTermo">Quem deve aceitar esse termo</span>? <span ng-show='!searchMode'>(*)</span></label>
                                <select class="form-control input-sm" id="codigotipoaceite" ng-model="item.codigotipoaceite" name="codigotipoaceite" ng-disabled="isVisualize" ng-class="{'fieldDisabled': isVisualize}" required ng-options="tipoAceite.codigo as tipoAceite.nome for tipoAceite in tiposAceite" 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.9.LBL_QuemAceitaTermo' | translate}}"/>
                            </div>
                            <div class="form-group col-md-6" ng-class="{'has-error': form.codigoidioma.$invalid && form.submitted}">
                                <label for="codigoidioma"><span translate="9.LBL_Idioma">Idioma</span> <span ng-show='!searchMode'>(*)</span></label>
                                <select class="form-control input-sm" id="codigoidioma" ng-model="item.codigoidioma" name="codigoidioma" ng-disabled="isVisualize" ng-class="{'fieldDisabled': isVisualize}" required ng-options="idioma.codigo as idioma.nome for idioma in idiomas" 
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.9.LBL_Idioma' | translate}}"/>
                            </div>
                        </div>
                        <div class="row" ng-if="!isPesquisa">
                            <div class="form-group col-md-6"></div>
                            <div class="form-group col-md-6">
                                <label translate="9.LBL_StatusTermo">Status do termo</label><br />
                                <button scroll-to="" class="btn btn-info btn-sm" type="button" ng-show="item.codigostatustermoadesao == 1" ng-click="ativarTermo(item);">
                                    <i class="icon-power-off bigger-110"></i>&nbsp;<span translate="9.BTN_AtivarTermo">Ativar Termo</span>
                                </button>
                                <button scroll-to="" class="btn btn-success btn-sm fieldDisabled" type="button" ng-disabled="true" ng-show="item.codigostatustermoadesao == 2">
                                    <i class="icon-ok bigger-110"></i>&nbsp;<span translate="9.BTN_Ativo">Ativo</span>
                                </button>
                                <button scroll-to="" class="btn btn-sm fieldDisabled" type="button" ng-disabled="true" ng-show="isNovoRegistro && (item.codigostatustermoadesao == '' || item.codigostatustermoadesao == undefined)">
                                    <i class="icon-cogs bigger-110"></i>&nbsp;<span translate="9.BTN_EmElaboracao">Em Elaboração</span>
                                </button>
                                <button scroll-to="" class="btn btn-sm fieldDisabled" type="button" ng-disabled="true" ng-show="item.codigostatustermoadesao == 3">
                                    <i class="fa fa-clock-o bigger-110"></i>&nbsp;<span translate="9.BTN_Historico">Histórico</span>
                                </button>
                            </div>
                        </div>
                        <div class="row" ng-if="isPesquisa">
                            <div class="form-group col-md-6">
                                <label translate="9.LBL_StatusTermo">Status do Termo</label>
                                <select class="form-control input-sm" id="codigostatustermoadesao" ng-model="item.codigostatustermoadesao" name="statustermoadesao"
                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.9.LBL_StatusTermo' | translate}}">
                                    <option ng-repeat="status in statustermoadesao" value="{{status.codigo}}">{{status.nome}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" ng-show="item.autor">
                            <div class="form-group col-md-12">
                                <span><strong translate="9.LBL_CriadoEm">Criado em: </strong>{{item.dataCriacao}} <strong translate="9.LBL_Por">por</strong>: {{item.autor}}</span>
                            </div>
                        </div>
                        <div class="form-actions center" ng-show="editMode" style="margin-bottom: 0px; margin-top: 0px;">
                            <button class="btn btn-info btn-sm" scroll-to="" name="salvar" ng-click="onSave(item)" ng-show="!isVisualize && !searchMode">
                                <i class="icon-save bigger-110"></i><span translate="9.BTN_Salvar">Salvar</span>
                            </button>
                            <button ng-click="onCancel(item)" class="btn btn-info btn-sm" name="concluir" type="button" ng-show="isVisualize && item.codigostatustermoadesao != 3">
                                <i class="icon-ok bigger-110"></i><span translate="9.BTN_Concluir">Concluir</span>
                            </button>
                            <button ng-click="onSearch(item)" class="btn btn-info btn-sm" name="find" type="button" ng-show="!isVisualize && searchMode">
                                <i class="icon-search bigger-110"></i><span translate="9.BTN_Pesquisar">Pesquisar</span>
                            </button>
                            <button ng-click="onCancel(item)" class="btn btn-danger btn-sm" name="cancel" type="button" ng-show="!isVisualize">
                                <i class="icon-remove bigger-110"></i><span translate="9.BTN_Cancelar">Cancelar</span>
                            </button>
                            <button ng-click="onCancel(item)" class="btn btn-danger btn-sm" name="voltar" type="button" ng-show="isVisualize && item.codigostatustermoadesao == 3">
                                <i class="icon-remove bigger-110"></i><span translate="9.BTN_Voltar">Voltar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" translate="9.LBL_TermosAdesao"></label>
        </div>
        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '9.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
            <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '9.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="21" tooltip-ajuda="'9.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 720px;">
                    <div class="table-responsive">
                        <table ng-table="tableHistorico" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
                                <tr>
                                    <th ng-click="tableHistorico.sorting('versao', tableHistorico.isSortBy('versao', 'asc') ? 'desc' : 'asc')">
                                      <span translate="9.GRID_Versao">Versão</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableHistorico.isSortBy('versao', 'asc'),
                                        'icon-sort-down': tableHistorico.isSortBy('versao', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableHistorico.sorting('datacriacao', tableHistorico.isSortBy('datacriacao', 'asc') ? 'desc' : 'asc')">
                                      <span translate="9.GRID_DataCriacao">Data Criação</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableHistorico.isSortBy('datacriacao', 'asc'),
                                        'icon-sort-down': tableHistorico.isSortBy('datacriacao', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableHistorico.sorting('datavencimento', tableHistorico.isSortBy('datavencimento', 'asc') ? 'desc' : 'asc')">
                                      <span translate="9.GRID_Vencimento">Vencimento</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableHistorico.isSortBy('datavencimento', 'asc'),
                                        'icon-sort-down': tableHistorico.isSortBy('datavencimento', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableHistorico.sorting('nomestatustermoadesao', tableHistorico.isSortBy('nomestatustermoadesao', 'asc') ? 'desc' : 'asc')">
                                      <span translate="9.GRID_Status">Status</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableHistorico.isSortBy('nomestatustermoadesao', 'asc'),
                                        'icon-sort-down': tableHistorico.isSortBy('nomestatustermoadesao', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableHistorico.sorting('usuariocriacao', tableHistorico.isSortBy('usuariocriacao', 'asc') ? 'desc' : 'asc')">
                                      <span translate="9.GRID_CriadoPor">Criado Por</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableHistorico.isSortBy('usuariocriacao', 'asc'),
                                        'icon-sort-down': tableHistorico.isSortBy('usuariocriacao', 'desc')
                                      }"></i>
                                    </th>
                                    <th><span translate="9.GRID_Acao">Ação</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" scroll-to="">
                                    <td ng-style="{ 'width': '8%' }" sortable="'versao'">{{item1.versao}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'datacriacao'">{{item1.datacriacao | date: 'dd/MM/yyyy'}}</td>
                                    <td ng-style="{ 'width': '12%' }" sortable="'datavencimento'">{{item1.datavencimento | date: 'dd/MM/yyyy'}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'nomestatustermoadesao'">{{item1.nomestatustermoadesao}}</td>
                                    <td ng-style="{ 'width': '40%' }" sortable="'usuariocriacao'">{{item1.usuariocriacao}}</td>
                                    <td ng-style="{ 'width': '10%' }" class="action-buttons center" data-title="'Ação'">
                                        <a href="" ng-click="onEdit(item1);" ng-show="item1.codigostatustermoadesao == 2 || item1.codigostatustermoadesao == 1"><i class="icon-edit bigger-130" title="{{ '9.TOOLTIP_Editar' | translate }}"></i></a>
                                        <a href="" ng-click="onVisualize(item1);" ng-show="item1.codigostatustermoadesao != 2 && item1.codigostatustermoadesao != 1"><i class="icon-search bigger-130" title="{{ '9.TOOLTIP_Visualizar' | translate }}"></i></a>
                                    </td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="6" class="text-center" translate="9.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
