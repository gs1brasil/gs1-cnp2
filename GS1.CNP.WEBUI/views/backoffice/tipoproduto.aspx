﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tipoproduto.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.produtoTipo" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="ProdutoTipo" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; exibeMensagem = false; entity = 'papel'; itemFormEspec = { statusregistro: 'novo' };" style="height: 820px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label> {{ titulo }}</label>
        </div>

        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" ng-show="searchMode" name="limpar" ng-click="onClean(item)" title="{{ '7.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="17" ng-show="searchMode" tooltip-ajuda="'7.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="18" ng-show="!searchMode && item != undefined && item.codigo == undefined" tooltip-ajuda="'7.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="19" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'7.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>

       <%--<div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode">Cadastro</span><span ng-show="searchMode">Pesquisa</span></h4>
                <div class="widget-toolbar" ng-if="searchMode">
                    <a href=""><i class="icon-eraser" title="Limpar" ng-click="onClean(item)"></i></a>
                    <a ng-show="searchMode" href="/views/backoffice/ajuda/tipoProduto.html" target="_blank"><i class="icon-question-sign" title="Limpar"></i></a>
                </div>
                <div class="widget-toolbar" ng-if="!searchMode">
                    <a ng-show="!searchMode" href="/views/backoffice/ajuda/tipoProduto.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
                <div class="widget-toolbar  my-tab">
                    <ul id="tabs-tipo" class="nav nav-tabs">
                        <li id="liTabTipo" class="active">
                            <a data-toogle="tab" href="#tipo">Tipo de Produto</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>--%>
        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="tipo" class="tab-pane active">
                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                <div class="row" ng-show="!searchMode">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        (*) - <span translate="7.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                        <label for="nome"><span translate="7.LBL_Nome">Nome</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="nome" name="nome" ng-model="item.nome" required="required" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.7.LBL_Nome' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                        <label for="status"><span translate="7.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="item.status" required ng-options="itemLista.codigo as itemLista.descricao for itemLista in [ { codigo: 1, descricao: 'Ativo' }, { codigo: 0, descricao: 'Inativo' } ]" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.7.LBL_Status' | translate}}">
                                            <option value=""></option>
                                        </select>

                                        <%--<select class="form-control input-sm" id="status" name="status" ng-model="item.status" required>
                                            <option value="1" ng-selected="item.status == 1">Ativo</option>
                                            <option value="0" ng-selected="item.status == 0">Inativo</option>
                                        </select>--%>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" ng-class="{'has-error': form.codigotradeitemunitdescriptorcodes.$invalid && form.submitted}">
                                        <label><span translate="7.GRID_TradeItemUnitDescriptorCodes">Descrição da Unidade Comercial</span> <span ng-show="!searchMode">(*)</span></label>
                                        <select class="form-control input-sm" id="codigotradeitemunitdescriptorcodes" name="codigotradeitemunitdescriptorcodes" ng-model="item.codigotradeitemunitdescriptorcodes" required ng-options="tradeitem.codigo as tradeitem.codigo + ' - ' + tradeitem.descricao for tradeitem in tradeitens" data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.7.GRID_TradeItemUnitDescriptorCodes' | translate}}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                        <label for="descricao" translate="7.GRID_TradeItemUnitDescriptorCodes">Descrição </label>
                                        <textarea class="form-control input-sm" id="descricao" sbx-maxlength="255" name="descricao" rows="3" ng-model="item.descricao" style="resize: vertical;" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.7.GRID_TradeItemUnitDescriptorCodes' | translate}}"></textarea>

                                        <span style="font-size: 80%;">(<span translate="7.LBL_TamanhoMaximo">Tamanho máximo</span>: 255)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span translate="7.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ form.descricao.$viewValue.length || 0 }}</span>
                                    </div>
                                </div>
                                <div class="row" ng-show ="!isPesquisa">
                                    <div class="form-group col-md-6">
                                        <label for="imagem" translate="7.LBL_Imagem">Imagem</label>
                                        <div class="widget-main">
                                            <input type="file" name="imagem" id="imagemTipoProduto" fileread="item.nomeimagem" onchange="angular.element(this).scope().alterImage()" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.7.LBL_Imagem' | translate}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6" ng-show="nomeimagem != undefined">
                                        <ul class="draglist ace-thumbnails" ng-show="item.nomeimagem != undefined">
                                            <li>
                                                <img height="100px" width="120px" src="{{item.nomeimagem}}"/></span>
                                                <div class="tools tools-bottom" ng-show="item.nomeimagem != 'images/no-photo.jpg' && item.nomeimagem != 'images/no-video.jpg'">
                                                    <a href="" ng-click="ExcluirImagem(item)">
                                                        <i class="fa fa-remove" style="color: red" title="{{ '7.TOOLTIP_Excluir' | translate }}"></i>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row" ng-show="!isPesquisa">
                                    <div class="form-group col-md-6" ng-class="{'has-error': form.base.campoobrigatorio.$invalid && form.submitted}">
                                        <input type="checkbox" class="ace" name="campoobrigatorio" ng-checked="initMode || item.campoobrigatorio == 1" ng-model="item.campoobrigatorio" ng-click="alterarIndicador(item.campoobrigatorio || false)">
                                        <label class="lbl">&nbsp;
                                            <span translate="7.LBL_CampoObrigatorioTipoProduto">Exigir obrigatoriedade dos campos de medidas no cadastro de produtos</span><span>?</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-actions center" ng-show="editMode">
                                    <button class="btn btn-info btn-sm" scroll-to="" name="find" ng-cloak ng-show="isPesquisa" ng-click="onSearch(item)">
                                        <i class="icon-search bigger-110"></i><span translate="7.BTN_Pesquisar">Pesquisar</span>
                                    </button>
                                    <button class="btn btn-info btn-sm" scroll-to="" ng-show="!isPesquisa" ng-click="onSave(item)">
                                        <i class="icon-save bigger-110"></i><span translate="7.BTN_Salvar">Salvar</span>
                                    </button>
                                    <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="7.BTN_Cancelar">Cancelar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" translate="7.LBL_CadastroTipoProduto"></label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '7.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode()"><i class="fa fa-plus"></i></a>
            <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '7.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="16" tooltip-ajuda="'7.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="widget-box">
            <%--<div class="widget-header">
                <h4>Registros</h4>
                <div class="widget-toolbar">
                    <a href=""><i class="icon-plus" title="Novo registro" ng-click="onFormMode()"></i>
                    </a><a href=""><i class="icon-search" title="Pesquisar" ng-click="onSearchMode(item)"></i></a>
                    <a href="/views/backoffice/ajuda/tipoProduto.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
            </div>--%>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 508px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="7.LBL_CliqueRegistroEditar">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableTipos" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
                                <tr>
                                    <th ng-click="tableTipos.sorting('codigo', tableTipos.isSortBy('codigo', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span>#</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableTipos.isSortBy('codigo', 'asc'),
                                        'icon-sort-down': tableTipos.isSortBy('codigo', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableTipos.sorting('nome', tableTipos.isSortBy('nome', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="7.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableTipos.isSortBy('nome', 'asc'),
                                        'icon-sort-down': tableTipos.isSortBy('nome', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableTipos.sorting('codigotradeitemunitdescriptorcodes', tableTipos.isSortBy('codigotradeitemunitdescriptorcodes', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="7.GRID_TradeItemUnitDescriptorCodes">Descrição da Unidade Comercial</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableTipos.isSortBy('codigotradeitemunitdescriptorcodes', 'asc'),
                                        'icon-sort-down': tableTipos.isSortBy('codigotradeitemunitdescriptorcodes', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableTipos.sorting('status', tableTipos.isSortBy('status', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="7.GRID_Status">Status</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableTipos.isSortBy('status', 'asc'),
                                        'icon-sort-down': tableTipos.isSortBy('status', 'desc')
                                      }"></i>
                                    </th>
                                    <th>
                                      <span translate="7.GRID_Acao">Ação</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '50%' }" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '20%' }" sortable="'codigotradeitemunitdescriptorcodes'">{{item1.codigotradeitemunitdescriptorcodes}} - {{item1.descricaotradeitemunitdescriptorcodes}}</td>
                                    <td ng-style="{ 'width': '20%'}"  sortable="'status'">{{item1.status | status}}</td>
                                    <td ng-style="{ 'width': '5%' }" class="action-buttons center" data-title="'Ação'">
                                        <a class="red" href="" ng-click="onDelete(item1); $event.stopPropagation();" title="{{ '7.TOOLTIP_Excluir' | translate }}"><i class="icon-trash bigger-130"></i></a>
                                    </td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="5" class="text-center" translate="7.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
