﻿using GS1.CNP.WEBUI.Core;

namespace GS1.Trade.WEBUI.views.backoffice
{
    public partial class produtoTipo : BaseWeb
    {
        public override string PermissaoPagina { get { return "PesquisarTipoProduto"; } }
    }
}