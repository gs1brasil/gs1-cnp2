﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="troca_senha.aspx.cs" Inherits="GS1.CNP.WEBUI.views.troca_senha" %>
<style>
    #passwordStrength    {	    height:10px;	    display:block;	    float:left;    }
    .strength0    {	    width:250px;	    background:#cccccc;    }
    .strength1    {	    width:50px;	    background:#ff0000;    }
    .strength2    {	    width:100px;		    background:#ff5f5f;    }
    .strength3    {	    width:150px;	    background:#56e500;    }
    .strength4    {	    background:#4dcd00;	    width:200px;    }
    .strength5    {	    background:#399800;	    width:250px;    }
</style>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/troca_senha";
    }
</script>

<div ng-controller="TrocaSenhaCtrl" class="page-container" style="height: 675px;">
    <div class="form-mobile" form-container> <%--form-container --%>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" translate="37.LBL_TrocaSenha"></label>
        </div>
        
        <div class="pull-right" style="margin-top: -40px;">
            <ajuda codigo="60"></ajuda>
        </div>

        <div class="widget-box">
            <%--<div class="widget-header">
                <h4>{{ titulo }}</h4>
            </div>--%>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" novalidate class="form" role="form">
                        <fieldset>
                            <div class="row" ng-show="usuarioLogado.avisosenha == 2">
                                <div class="form-group col-md-4 col-xs-12" ng-class="{'has-error': form.senha_atual.$invalid && form.submitted}">
                                    <label style="color: red; font-size: 14px;">*<span translate="37.LBL_SenhaExpirou">Sua senha expirou e deve ser alterada</span>.</label>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <span translate="37.LBL_RegrasSenhas">Regras para senhas</span><br />
                                - <span translate="37.LBL_SenhaCaracteres">A senha deve ter entre 7 e 15 caracteres</span>.<br />
                                - <span translate="37.LBL_SenhaMaiusculaMinuscula">Deve conter letras maiúscula e minúsculas, números e caracteres especiais (ex.: !, @, #...)</span>.<br />
                                - <span translate="37.LBL_SenhaAnterior60Porcento">Não pode repetir a senha anterior em mais de 60% dos caracteres</span>.<br />
                                - <span translate="37.LBL_SenhaDiferenteNUltimas">Deve ser diferente das</span> {{qtdesenhas}} <span translate="37.LBL_UltimasSenhas">últimas senhas</span>.<br /><br />
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4 col-xs-12" ng-class="{'has-error': form.senha_atual.$invalid && form.submitted}">
                                    <label for="nome"><span translate="37.LBL_SenhaAtual">Senha Atual</span> (*)</label>
                                    <span class="input-icon input-icon-right" style="width: 100%; cursor: pointer;">
                                        <input type="password" class="form-control input-sm" id="senha_atual" name="senha_atual" maxlength="15" ng-model="item.senha_atual" required
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.37.LBL_SenhaAtual' | translate}}"/>
                                        <span ng-bind="15-item.senha_atual.length" ng-show="item.senha_atual.length > 0"></span>
                                        <i class="icon-eye-open blue" ng-click="showHidePass('senha_atual')"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4 col-xs-12" ng-class="{'has-error': form.nova_senha.$invalid && form.submitted}">
                                    <label for="descricao"><span translate="37.LBL_NovaSenha">Nova Senha</span> (*)</label>
                                    <span class="input-icon input-icon-right" style="width: 100%; cursor: pointer;">
                                        <input type="password" class="form-control input-sm" id="nova_senha" ng-keyup="passwordStrength(item.nova_senha)" name="nova_senha" maxlength="15" ng-model="item.nova_senha" required/
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.37.LBL_NovaSenha' | translate}}">
                                        <span ng-bind="15-item.nova_senha.length" ng-show="item.nova_senha.length > 0"></span>
                                        <i class="icon-eye-open blue" ng-click="showHidePass('nova_senha')"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4 col-xs-12" ng-class="{'has-error': form.confirmar_senha.$invalid && form.submitted}">
                                    <label for="login"><span translate="37.LBL_ConfirmarNovaSenha">Confirmar Nova Senha</span> (*)</label>
                                    <span class="input-icon input-icon-right" style="width: 100%; cursor: pointer;">
                                        <input type="password" class="form-control input-sm" id="confirmar_senha" name="confirmar_senha" maxlength="15" ng-model="item.confirmar_senha" required
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.37.LBL_ConfirmarNovaSenha' | translate}}"/>
                                        <span ng-bind="15-item.confirmar_senha.length" ng-show="item.confirmar_senha.length > 0"></span>
                                        <i class="icon-eye-open blue" ng-click="showHidePass('confirmar_senha')"></i>
                                    </span>
                                </div>
                                <p>
			                        <div id="passwordDescription" translate="37.LBL_SenhaNaoInserida">Senha não inserida</div>
			                        <div id="passwordStrength" class="strength0"></div>
		                        </p>
                            </div>
                        </fieldset>

                        <div class="form-actions center">
                            <button id="submit" name="submit" class="btn btn-info btn-sm" type="button" ng-click="onSave(item)">
                                <i class="icon-ok bigger-110"></i><span translate="37.BTN_Salvar">Salvar</span>
                            </button>
                            <button id="cancel" name="cancel" ng-click="onCancel()" class="btn btn-danger btn-sm" type="button" ng-show="usuarioLogado.id_statususuario != 5 && usuarioLogado.avisosenha != 2">
                                <i class="icon-remove bigger-110"></i><span translate="37.BTN_Cancelar">Cancelar</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

