﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uploadCartas.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.uploadCartas" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />

<div ng-controller="UploadCartas" class="page-container" ng-init="editMode = false; searchMode = false; exibeVoltar = false; exibeMensagem = false; entity = 'papel'; itemFormEspec = { statusregistro: 'novo' };" style="height: 820px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo"></label>
        </div>

        <div class="pull-right" style="margin-top: -34px;">
            <a class="btn btn-danger btn-xs" name="limpar" ng-show="searchMode" ng-click="onClean(item)" title="{{ '5.TOOLTIP_Limpar' | translate }}"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="13" ng-show="searchMode" tooltip-ajuda="'5.TOOLTIP_Ajuda'" ng-cloak></ajuda>
            <ajuda codigo="14" ng-show="!searchMode && item != undefined && item.codigo != undefined" tooltip-ajuda="'5.TOOLTIP_Ajuda'" ng-cloak></ajuda>
        </div>

       <%--<div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode">Cadastro</span><span ng-show="searchMode">Pesquisa</span></h4>
                <div class="widget-toolbar" ng-if="searchMode">
                    <a href=""><i class="icon-eraser" title="Limpar" ng-click="onClean(item)"></i></a>
                    <a ng-show="searchMode" href="/views/backoffice/ajuda/tipoProduto.html" target="_blank"><i class="icon-question-sign" title="Limpar"></i></a>
                </div>
                <div class="widget-toolbar" ng-if="!searchMode">
                    <a ng-show="!searchMode" href="/views/backoffice/ajuda/tipoProduto.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
                <div class="widget-toolbar  my-tab">
                    <ul id="tabs-tipo" class="nav nav-tabs">
                        <li id="liTabTipo" class="active">
                            <a data-toogle="tab" href="#tipo">Tipo de Produto</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>--%>
        <div class="content-module-bar widget-box bg-sky"></div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="tipo" class="tab-pane active">
                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                <div class="row" ng-show="!searchMode">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        (*) - <span translate="5.LBL_CamposObrigatorios">Campos obrigatórios</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                        <label for="nome" translate="5.LBL_TipoCartaTemplate">Tipo de Carta (template) <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm fieldDisabled" maxlength="50" id="nome" name="nome" ng-model="item.nome" required="required" ng-disabled="true" 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.5.LBL_TipoCartaTemplate' | translate}}"/>
                                    </div>
                                    <div class="form-group col-md-6 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                        <label for="status" translate="5.LBL_Status">Status <span ng-show='!searchMode'>(*)</span></label>
                                        <select class="form-control input-sm" id="status" name="status" ng-model="item.status" required 
                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.5.LBL_Status' | translate}}">
                                            <option value="1" ng-selected="item.status == 1">Ativo</option>
                                            <option value="0" ng-selected="item.status == 0">Inativo</option>
                                        </select>
                                    </div>
                                </div>
                                <%--<div class="row">
                                    <div class="form-group col-md-12" ng-class="{'has-error': form.descricao.$invalid && form.submitted}">
                                        <label for="nome">
                                            Descrição <span ng-show='!searchMode'>(*)</span>
                                        </label>
                                        <textarea class="form-control input-sm" id="descricao" sbx-maxlength="2000" name="descricao"
                                            rows="3" ng-model="item.descricao" style="resize: vertical;" required></textarea>
                                        <span style="font-size: 80%;">(Tamanho máximo: 2000)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Quantidade
                                        de caracteres: {{ form.descricao.$viewValue.length || 0 }}</span>
                                    </div>
                                </div>--%>
                                <div class="row" ng-show ="!isPesquisa">
                                    <div class="form-group col-md-6" ng-class="{'has-error': uploadObrigatorio}">
                                        <label for="imagem" translate="5.LBL_ArquivoUpload">Arquivo para upload <span ng-show='!searchMode'>(*)</span></label>
                                        <div class="widget-main">
                                            <input type="file" name="carta" id="carta" fileread="carta" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.5.LBL_ArquivoUpload' | translate}}"/>
                                        </div>
                                    </div>
                                    <%--<div class="form-group col-md-6" ng-show="item.carta != undefined">
                                        <ul class="draglist ace-thumbnails" ng-show="item.carta != undefined">
                                            <li>
                                                <img height="100px" width="120px" src="{{item.carta}}"/></span>
                                            </li>
                                        </ul>
                                    </div>--%>
                                </div>
                                <div class="form-actions center" ng-show="editMode">
                                    <button class="btn btn-info btn-sm" scroll-to="" name="find" ng-cloak ng-show="isPesquisa" ng-click="onSearch(item)" type="button">
                                        <i class="icon-search bigger-110"></i><span translate="5.BTN_Pesquisar">Pesquisar</span>
                                    </button>
                                    <button class="btn btn-info btn-sm" scroll-to="" ng-show="!isPesquisa" ng-click="onSave(item, carta)" type="button">
                                        <i class="icon-save bigger-110"></i><span translate="5.BTN_Salvar">Salvar</span>
                                    </button>
                                    <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i><span translate="5.BTN_Cancelar">Cancelar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label ng-bind="titulo" translate="5.LBL_UploadCartas"></label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-success btn-xs" title="{{ '5.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="12" tooltip-ajuda="'5.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="widget-box">
            <%--<div class="widget-header">
                <h4>Registros</h4>
                <div class="widget-toolbar">
                    <a href=""><i class="icon-plus" title="Novo registro" ng-click="onFormMode()"></i>
                    </a><a href=""><i class="icon-search" title="Pesquisar" ng-click="onSearchMode(item)"></i></a>
                    <a href="/views/backoffice/ajuda/tipoProduto.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
            </div>--%>
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 380px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;" translate="5.LBL_CliqueRegistroEditar">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableCartas" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
                                <tr>
                                    <th ng-click="tableCartas.sorting('codigo', tableCartas.isSortBy('codigo', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span>#</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableCartas.isSortBy('codigo', 'asc'),
                                        'icon-sort-down': tableCartas.isSortBy('codigo', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableCartas.sorting('nome', tableCartas.isSortBy('nome', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="5.GRID_TipoLicenca">Tipo de Licença</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableCartas.isSortBy('nome', 'asc'),
                                        'icon-sort-down': tableCartas.isSortBy('nome', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableCartas.sorting('usuario', tableCartas.isSortBy('usuario', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="5.GRID_Usuario">Usuário</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableCartas.isSortBy('usuario', 'asc'),
                                        'icon-sort-down': tableCartas.isSortBy('usuario', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableCartas.sorting('dataalteracao', tableCartas.isSortBy('dataalteracao', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="5.GRID_DataAlteracao">Data de Alteração</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableCartas.isSortBy('dataalteracao', 'asc'),
                                        'icon-sort-down': tableCartas.isSortBy('dataalteracao', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableCartas.sorting('status', tableCartas.isSortBy('status', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="5.GRID_Status">Status</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableCartas.isSortBy('status', 'asc'),
                                        'icon-sort-down': tableCartas.isSortBy('status', 'desc')
                                      }"></i>
                                    </th>
                                    <th>
                                      <span translate="5.GRID_Modelo">Modelo</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '25%' }" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'usuario'">{{item1.usuario}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'dataalteracao'">{{item1.dataalteracao | date:'dd/MM/yyyy'}}</td>
                                    <td ng-style="{ 'width': '15%'}" sortable="'status'">{{item1.status | status}}</td>
                                    <td ng-style="{ 'width': '15%' }" class="text-center">
                                        <a href="{{item1.caminhotemplate}}" ng-show="item1.caminhotemplate != undefined && item1.caminhotemplate != null && item1.caminhotemplate != ''" ng-click="$event.stopPropagation();">
                                            <button class="btn btn-info btn-xs">
                                                <i class="fa fa-download" title="{{ '5.TOOLTIP_Download' | translate }}"></i>
                                            </button>
                                        </a>
                                    </td>
                                    <%--<td ng-style="{ 'width': '5%' }" class="action-buttons center">
                                        <a class="red" href="" ng-click="onDelete(item1); $event.stopPropagation();"><i class="icon-trash bigger-130"></i></a>
                                    </td>--%>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="7" class="text-center" translate="5.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
