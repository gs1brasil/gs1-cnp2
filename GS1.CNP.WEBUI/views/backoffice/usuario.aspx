﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usuario.aspx.cs" Inherits="GS1.CNP.WEBUI.views.cadastros.usuario" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/usuario";
    }

    $(".widget-header a").attr("tabindex", "-1");
</script>

<style>
    .has-error-cpfcnpj input
    {
        border-color:#f09784 !important;
        color:#d68273 !important;
        -webkit-box-shadow:none;
        box-shadow:none;
    }
</style>

<div ng-controller="UsuarioCtrl" class="page-container" ng-init="formMode = false; searchMode = false; exibeVoltar = false; isPesquisa = false; validaEMail = false; entity = 'usuario'; typeUser = 'Usuário GS1'; listaContatos = [];"
    style="height: 825px;">
    <watch-fetch ids="item.id_perfil"></watch-fetch>

    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label ng-bind="titulo" translate="10.LBL_CadastroUsuario"></label>
        </div>

        <div class="widget-toolbar my-tab" style="margin-top: -36px;">
            <ul id="tabs-user" class="nav nav-tabs">
                <li id="liTabUser" class="active">
                    <a data-toogle="tab" href="#usuario" translate="10.ABA_Usuario">Usuário</a>
                </li>
                <li id="liTabAssociados" ng-show="!searchMode || !formMode">
                    <a data-toogle="tab" href="#associados" translate="10.ABA_AssociarEmpresa">Associar Empresa</a>
                </li>
            </ul>
        </div>
        <div class="pull-right" style="margin-top: -32px; margin-right: 100px;" ng-show="searchMode" ng-cloak>
            <a class="btn btn-danger btn-xs" name="limpar" title="{{ '10.TOOLTIP_Limpar' | translate }}" ng-click="onClean(item)"><i class="fa fa-eraser"></i></a>
            <ajuda codigo="26" tooltip-ajuda="'10.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <div class="pull-right" style="margin-top: -32px; margin-right: 235px;" ng-show="!searchMode && itemForm != undefined && itemForm.codigo == undefined" ng-cloak>
            <ajuda codigo="27" tooltip-ajuda="'10.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <div class="pull-right" style="margin-top: -32px; margin-right: 235px;" ng-show="!searchMode && itemForm != undefined && itemForm.codigo != undefined" ng-cloak>
            <ajuda codigo="28" tooltip-ajuda="'10.TOOLTIP_Ajuda'"></ajuda>
        </div>

        <div class="widget-box">
            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body">
                <div class="widget-main no-padding">
                    <form name="form" id="form" novalidate class="form" role="form">
                        <div class="tab-content" style="min-height:400px;">
                            <div id="usuario" class="tab-pane active">
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-12 col-xs-12">
                                            <div class="form-group col-md-12" style="font-size: 90%;">(*) - <span translate="10.LBL_CamposObrigatorios">Campos obrigatórios</span></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.nome.$invalid && form.submitted}">
                                                <label for="nome"><span translate="10.LBL_Nome">Nome</span> <span ng-show='!searchMode'>(*)</span></label>
                                                <input type="text" class="form-control input-sm" id="nome" name="nome" maxlength="255" ng-model="itemForm.nome" ng-required="(formMode && !searchMode) || (searchMode && !validaEmail && form.email.$invalid)" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Nome' | translate}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.email.$invalid && form.submitted}">
                                                <label for="email"><span translate="10.LBL_Email">E-mail</span> <span ng-show='!searchMode'>(*)</span></label>
                                                <input type="text" class="form-control input-sm" id="email" name="email" maxlength="255" ng-pattern="/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/" 
                                                    ng-disabled="formMode && !searchMode && itemForm.codigo > 0" ng-class="{'fieldDisabled': formMode && !searchMode && itemForm.codigo > 0}" 
                                                    ng-model="itemForm.email" validity-bind-fix ng-required="formMode || (searchMode && form.nome.$invalid)" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Email' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error-cpfcnpj': errorCPFCNPJ }">
                                                <label for="cpfcnpj" translate="10.LBL_CPFCNPJ">CPF/CNPJ</label>
                                                <input type="text" class="form-control input-sm" id="cpfcnpj" name="cpfcnpj" maxlength="18" ng-model="itemForm.cpfcnpj" ui-br-cpfcnpj-mask ng-pattern="/(^(\d{2}.\d{3}.\d{3}/\d{4}-\d{2})|(\d{14})$)|(^(\d{3}.\d{3}.\d{3}-\d{2})|(\d{11})$)/" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_CPFCNPJ' | translate}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-xs-12">
                                                <label for="telefone" translate="10.LBL_TelefoneResidencial">Telefone Residencial</label>
                                                <input type="text" class="form-control input-sm" id="telefone" name="telefone" sbx-maxlength="15" ng-model="itemForm.telefone" ui-mask="(99)99999999" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_TelefoneResidencial' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-xs-12">
                                                <label for="celular" translate="10.LBL_TelefoneCelular">Telefone Celular</label>
                                                <input type="text" class="form-control input-sm" id="celular" name="celular" sbx-maxlength="16" ng-model="itemForm.celular" ui-mask="(99)99999999?9" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_TelefoneCelular' | translate}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-xs-12">
                                                <label for="comercial" translate="10.LBL_TelefoneComercial">Telefone Comercial</label>
                                                <input type="text" class="form-control input-sm" id="comercial" name="comercial" sbx-maxlength="15" ng-model="itemForm.comercial" ui-mask="(99)99999999" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_TelefoneComercial' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" ng-show="!searchMode">
                                            <div class="form-group col-md-12 col-xs-12">
                                                <label for="ramal" translate="10.LBL_Ramal">Ramal</label>
                                                <input type="text" class="form-control input-sm" id="ramal" name="ramal" maxlength="15" ng-model="itemForm.ramal" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Ramal' | translate}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6" ng-show="!searchMode">
                                            <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                                <label for="login" style="display: block"><span translate="10.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>

                                                <select class="form-control input-sm" id="status" name="status" ng-model="itemForm.codigostatususuario" ng-required="" ng-options="status.codigo as status.nome for status in statusUsuario" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Status' | translate}}"/>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-xs-12">
                                                <label for="tipo_usuario"><span translate="10.LBL_TipoUsuario">Tipo de Usuário</span> <span ng-show='!searchMode'>(*)</span></label>
                                                <select id="tipo_usuario" name="codigotipousuario" class="form-control input-sm" ng-model="itemForm.codigotipousuario"
                                                    ng-disabled="(usuarioLogado.id_tipousuario != 1) || itemForm.codigo" ng-class="{'fieldDisabled': (usuarioLogado.id_tipousuario != 1) || itemForm.codigo}" 
                                                    ng-options="tipoUsuario.codigo as tipoUsuario.nome for tipoUsuario in tiposUsuario" ng-change="onSelectTipoUsuario(itemForm.codigotipousuario,searchMode)" ng-required="!searchMode" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_TipoUsuario' | translate}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.perfil.$invalid && form.submitted}">
                                                <label for="login"><span translate="10.LBL_Perfil">Perfil</span> <span ng-show='!searchMode'>(*)</span></label>

                                                <select class="form-control input-sm" id="perfil" name="perfil" ng-model="itemForm.codigoperfil" required=""
                                                    ng-readonly="(usuarioLogado.id == itemForm.codigo)" ng-disabled="(usuarioLogado.id == itemForm.codigo)" ng-class="{'fieldDisabled': (usuarioLogado.id == itemForm.codigo)}"
                                                    ng-options="perfilUsuario.codigo as perfilUsuario.nome for perfilUsuario in perfisUsuario" 
                                                    data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Perfil' | translate}}">
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6" ng-show="searchMode">
                                            <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.status.$invalid && form.submitted}">
                                                <label for="login" style="display: block"><span translate="10.LBL_Status">Status</span> <span ng-show='!searchMode'>(*)</span></label>

                                                <select class="form-control input-sm" id="Select1" name="status" ng-model="itemForm.codigostatususuario" ng-required="" ng-options="status.codigo as status.nome for status in statusUsuario" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Status' | translate}}"/>

                                            </div>
                                        </div>
                                        <div class="col-md-6" ng-show="searchMode">
                                            <div class="form-group col-md-12 col-xs-12">
                                                <label for="login" style="display: block" translate="10.LBL_EmpresaAssociada">Empresa Associada </label>
                                                <div ng-if="usuarioLogado.id_tipousuario == 1">
                                                    <input id="typea" type="text" ng-model="itemForm.empresaassociada" maxlength="255"
                                                        placeholder="{{ '10.PLACEHOLDER_PesquisarAssociado' | translate }}" typeahead="associadosDoUsuario as associadosDoUsuario.cad+'-'+associadosDoUsuario.nome for associadosDoUsuario in BuscarAssociadosAutoComplete($viewValue)"
                                                        typeahead-min-length="3" class="typeahead form-control input-sm"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_EmpresaAssociada' | translate}}"/>
                                                </div>
                                                <div ng-cloak ng-if="usuarioLogado.id_tipousuario != 1">
                                                    <select id="select" class="input-sm" style="padding: 0; width: 100%" name="produto" ng-model="itemForm.empresaassociada"
                                                        ng-options="associado.codigo as associado.nome for associado in associadosDoUsuario"
                                                        data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_EmpresaAssociada' | translate}}">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" ng-show="!searchMode">
                                            <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.departamento.$invalid && form.submitted}">
                                                <label for="departamento" translate="10.LBL_Departamento">Departamento</label>
                                                <input type="text" class="form-control input-sm" id="departamento" name="departamento" maxlength="255" ng-model="itemForm.departamento" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Departamento' | translate}}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6" ng-show="!searchMode">
                                            <div class="form-group col-md-12 col-xs-12" ng-class="{'has-error': form.cargo.$invalid && form.submitted}">
                                                <label for="cargo" translate="10.LBL_Cargo">Cargo</label>
                                                <input type="text" class="form-control input-sm" id="cargo" name="cargo" maxlength="255" ng-model="itemForm.cargo" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Cargo' | translate}}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-show="!searchMode">
                                        <div class="col-md-12 col-xs-12 col-xs-12">
                                            <div class="form-group col-md-12 col-xs-12">
                                                <label for="mensagemobservacao" translate="10.LBL_Observacao">Observação</label>
                                                <textarea class="form-control input-sm" rows="2" id="mensagemobservacao" name="mensagemobservacao" sbx-maxlength="500" ng-model="itemForm.mensagemobservacao" style="width: 100%;resize: vertical;" 
                                                data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.LBL_Observacao' | translate}}"/>
                                                <span style="font-size: 80%;">(<span translate="10.LBL_TamanhoMaximo">Tamanho máximo</span>: 500)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span translate="10.LBL_QuantidadeCaracteres">Quantidade de caracteres</span>: {{ itemForm.mensagemobservacao.length || 0 }}</span>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div id="associados" class="tab-pane" ng-show="!searchMode">
                                <div class="" ng-cloak ng-if="usuarioLogado.id_tipousuario == 1">
                                    <div class="input-group col-md-5 pull-left" style="margin-top: 7px; margin-bottom: 7px;">
                                        <input id="typea2" type="text" ng-model="associado" maxlength="255" placeholder="{{ '10.PLACEHOLDER_PesquisarAssociado' | translate }}"
                                            typeahead="associadosDoUsuario as associadosDoUsuario.cad+'-'+associadosDoUsuario.nome for associadosDoUsuario in BuscarAssociadosAutoComplete($viewValue)"
                                            typeahead-min-length="3" typeahead-on-select="insereAssociadoLista(associado);" class="typeahead form-control input-sm" 
                                            data-toggle="popover" data-trigger="hover" data-container="body" data-content="{{'t.10.PLACEHOLDER_PesquisarAssociado' | translate}}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="table-responsive">
                                            <table ng-table="tableAssociados" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <label ng-if="usuarioLogado.id != itemForm.codigo">
                                                               <input ng-model="item.confirmar"
                                                                    ng-change="marcarDesmarcarAssociados(item.confirmar)"
                                                                    type="checkbox"
                                                                    class="ace">

                                                                <span class="lbl"></span>
                                                            </label>
                                                        </th>
                                                        <th ng-click="tableAssociados.sorting('codigo', tableAssociados.isSortBy('codigo', 'asc') ? 'desc' : 'asc')">
                                                          <span>#</span>&nbsp;&nbsp;
                                                          <i class="text-center icon-sort" ng-class="{
                                                            'icon-sort-up': tableAssociados.isSortBy('codigo', 'asc'),
                                                            'icon-sort-down': tableAssociados.isSortBy('codigo', 'desc')
                                                          }"></i>
                                                        </th>
                                                        <th ng-click="tableAssociados.sorting('nome', tableAssociados.isSortBy('nome', 'asc') ? 'desc' : 'asc')">
                                                          <span translate="10.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                                          <i class="text-center icon-sort" ng-class="{
                                                            'icon-sort-up': tableAssociados.isSortBy('nome', 'asc'),
                                                            'icon-sort-down': tableAssociados.isSortBy('nome', 'desc')
                                                          }"></i>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="item1 in $data">
                                                        <td ng-style="{ 'width': '4%' }" class="center" data-header="'checkboxheader.html'">
                                                            <div class="" stop-event='click'>
                                                                <label ng-if="usuarioLogado.id != itemForm.codigo">
                                                                    <input ng-model="item1.checked" ng-checked="{{item1.checked == 1}}"
                                                                           ng-click="flagMarcarAssociado(item1)"
                                                                           type="checkbox"
                                                                           class="ace">

                                                                        <span class="lbl" ng-show="{{usuarioLogado.id != itemForm.codigo}}"></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td ng-style="{ 'width': '5%' }" data-title="'#'" sortable="'codigo'">{{item1.codigo}}</td>
                                                        <td ng-style="{ 'width': '55%' }" data-title="'Nome'" sortable="'nome'">{{item1.nome}}</td>
                                                    </tr>
                                                    <tr ng-show="$data.length == 0">
                                                        <td colspan="7" class="text-center" translate="10.LBL_NenhumRegistroEncontrado">Nenhum associado selecionado</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions center" ng-show="formMode">
                                <button class="btn btn-info btn-sm" name="find" scroll-to="" ng-cloak ng-show="isPesquisa" ng-click="onSearch(itemForm)" type="button">
                                    <i class="icon-search bigger-110"></i><span translate="10.BTN_Pesquisar">Pesquisar</span>
                                </button>
                                <button class="btn btn-info btn-sm" scroll-to="" ng-cloak ng-show="!isPesquisa" ng-click="onSave(itemForm)" type="button">
                                    <i class="icon-save bigger-110"></i><span translate="10.BTN_Salvar">Salvar</span>
                                </button>
                                <button id="cancel" name="cancel" ng-click="onCancel(itemForm)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                    <i class="icon-remove bigger-110"></i><span translate="10.BTN_Cancelar">Cancelar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": formMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='formMode = !formMode'></i>
            <label ng-bind="titulo" translate="10.LBL_CadastroUsuario"></label>
        </div>

        <div class="pull-right action-buttons-header" style="margin-right: 0px;">
            <a class="btn btn-info btn-xs" name="NovoRegistro" title="{{ '10.TOOLTIP_NovoRegistro' | translate }}" ng-click="onFormMode(item)"><i class="fa fa-plus"></i></a>
            <a class="btn btn-success btn-xs" name="NovaPesquisa" title="{{ '10.TOOLTIP_Pesquisar' | translate }}" ng-click="onSearchMode(item)"><i class="fa fa-search"></i></a>
            <ajuda codigo="25" tooltip-ajuda="'10.TOOLTIP_Ajuda'"></ajuda>
        </div>
        <div class="widget-box">

            <div class="content-module-bar widget-box bg-sky"></div>
            <div class="widget-body" style="height: 655px;" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="formMode = !formMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && formMode"></i>
                    <i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !formMode"></i>
                </a>

                <div class="widget-main" style="min-height: 300px; width: 100%">
                    <div class="row">
                        <div class="form-group col-md-12 col-xs-12 information" translate="10.LBL_CliqueRegistroEditar">Clique em um registro para editar</div>
                    </div>

                    <div class="table-responsive" style="height: 560px; overflow-y: auto; ">
                        <table ng-table="tableParams" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <thead>
                                <tr>
                                    <th ng-click="tableParams.sorting('codigo', tableParams.isSortBy('codigo', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span>#</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableParams.isSortBy('codigo', 'asc'),
                                        'icon-sort-down': tableParams.isSortBy('codigo', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableParams.sorting('nome', tableParams.isSortBy('nome', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="10.GRID_Nome">Nome</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableParams.isSortBy('nome', 'asc'),
                                        'icon-sort-down': tableParams.isSortBy('nome', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableParams.sorting('email', tableParams.isSortBy('email', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="10.GRID_Email">E-mail</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableParams.isSortBy('email', 'asc'),
                                        'icon-sort-down': tableParams.isSortBy('email', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableParams.sorting('nometipousuario', tableParams.isSortBy('nometipousuario', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="10.GRID_TipoUsuario">Tipo de Usuário</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableParams.isSortBy('nometipousuario', 'asc'),
                                        'icon-sort-down': tableParams.isSortBy('nometipousuario', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableParams.sorting('nomeperfil', tableParams.isSortBy('nomeperfil', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="10.GRID_Perfil">Perfil</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableParams.isSortBy('nomeperfil', 'asc'),
                                        'icon-sort-down': tableParams.isSortBy('nomeperfil', 'desc')
                                      }"></i>
                                    </th>
                                    <th ng-click="tableParams.sorting('nomestatususuario', tableParams.isSortBy('nomestatususuario', 'asc') ? 'desc' : 'asc')" class="text-center">
                                      <span translate="10.GRID_Status">Status</span>&nbsp;&nbsp;
                                      <i class="text-center icon-sort" ng-class="{
                                        'icon-sort-up': tableParams.isSortBy('nomestatususuario', 'asc'),
                                        'icon-sort-down': tableParams.isSortBy('nomestatususuario', 'desc')
                                      }"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item1 in $data" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '5%' }" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '30%' }" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '15%' }" sortable="'email'">{{item1.email}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'nometipousuario'">{{item1.nometipousuario}}</td>
                                    <td ng-style="{ 'width': '20%' }" sortable="'nomeperfil'">{{item1.nomeperfil}}</td>
                                    <td ng-style="{ 'width': '10%' }" sortable="'nomestatususuario'">{{item1.nomestatususuario}}</td>
                                    <td ng-style="{ 'width': '10%' }" ng-show="false" sortable="'setor'">{{item1.setor}}</td>
                                </tr>
                                <tr ng-show="$data.length == 0">
                                    <td colspan="7" class="text-center" translate="10.LBL_NenhumRegistroEncontrado">Nenhum registro encontrado</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/ng-template" id="checkboxheader.html">
       <label ng-if="usuarioLogado.id != itemForm.codigo">
           <input ng-model="item.confirmar"
                ng-change="marcarDesmarcarAssociados(item.confirmar)"
                type="checkbox"
                class="ace">

            <span class="lbl"></span>
        </label>
    </script>
</div>
