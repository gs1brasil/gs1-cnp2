using GS1.CNP.WEBUI.Core;


namespace GS1.CNP.WEBUI.views.cadastros
{
    public partial class usuario : BaseWeb
    {
        public override string PermissaoPagina { get { return "PesquisarUsuario"; } }
    }
}