﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usuariosCallcenter.aspx.cs" Inherits="GS1.Trade.WEBUI.views.backoffice.usuariosCallcenter" %>

<script type="text/javascript">
    if (typeof angular == 'undefined') {
        window.location.href = "/#page/cadastros/produtos";
    }

</script>

<link href="../../styles/style.min.css" rel="stylesheet" type="text/css" />
<script src="../../scripts/lib/jstree.min.js" type="text/javascript"></script>

<div ng-controller="UsuarioCallcenterCtrl" class="page-container" ng-init="editMode = false; searchMode = true; exibeVoltar = false; exibeMensagem = false; entity = 'agencia'; itemFormEspec = { statusregistro: 'novo' };" style="height: 820px;">
    <watch-fetch ids="item.codigo"></watch-fetch>
    <div class="form-container" form-container>
        <div class="page-header">
            <i class="icon-align-justify"></i>
            <label>Administração Usuário Call-Center</label>
        </div>
        <div class="widget-box">
            <div class="widget-header">
                <h4><span ng-show="!searchMode">Cadastro</span><span ng-show="searchMode">Pesquisa</span></h4>
                <div class="widget-toolbar" ng-if="searchMode">
                    <a href=""><i class="icon-eraser" title="Limpar" ng-click="onClean(item)"></i></a>
                    <a ng-show="searchMode" href="/views/backoffice/ajuda/gestãoPerfil.html" target="_blank"><i class="icon-question-sign" title="Limpar"></i></a>
                </div>
                <div class="widget-toolbar" ng-if="!searchMode">
                    <a ng-show="!searchMode" href="/views/backoffice/ajuda/gestãoPerfil.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
                <div class="widget-toolbar  my-tab">
                    <ul id="tabs-dados" class="nav nav-tabs">
                        <li id="liTabDados" class="active">
                            <a data-toogle="tab" href="#dados">Dados do Usuário</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main no-padding">
                <form name="form" id="form" novalidate class="form" role="form">
                    <div class="tab-content">
                        <div id="dados" class="tab-pane active">
                            <div style="overflow-x: hidden; overflow-y: auto; clear: both;">
                                <div class="row" ng-show="!searchMode">
                                    <div class="form-group col-md-12" style="font-size: 90%;">
                                        (*) - Campos obrigatórios
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="nome">Nome Completo <span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="nome" name="nome" ng-model="item.nome" required="required" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cpf">CPF<span ng-show='!searchMode'>(*)</span></label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="cpf" name="cpf" ng-model="item.cpf" required="required" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="email" name="email" ng-model="item.email" required="required" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="perfil">Nome do perfil</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="perfil" name="perfil" ng-model="item.perfil" required="required" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="razao">Razão social</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="razao" name="razao" ng-model="item.razao" required="required" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="cnpj">CNPJ</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="cnpj" name="cnpj" ng-model="item.cnpj" required="required" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="status">Status</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="status" name="status" ng-model="item.status" required="required" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="telResidencial">Telefone residencial</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="telResidencial" name="telResidencial" ng-model="item.telResidencial" required="required" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="telCelular">Telefone celular</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="telCelular" name="telCelular" ng-model="item.telCelular" required="required" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="telComercial">Telefone comercial</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="telComercial" name="telComercial" ng-model="item.telComercial" required="required" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="ramal">Ramal</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="ramal" name="ramal" ng-model="item.ramal" required="required" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="departamento">Departamento</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="departamento" name="departamento" ng-model="item.departamento" required="required" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="cargo">Cargo</label>
                                        <input type="text" class="form-control input-sm" maxlength="50" id="cargo" name="cargo" ng-model="item.cargo" required="required" />
                                    </div>
                                </div>
                                <div class="form-actions center" ng-show="editMode">
                                    <button class="btn btn-info btn-sm" scroll-to="" ng-click="">
                                        <i class="icon-ok bigger-110"></i>Salvar
                                    </button>
                                    <button id="cancel" name="cancel" ng-click="onCancel(item)" scroll-to="" class="btn btn-danger btn-sm" type="button">
                                        <i class="icon-remove bigger-110"></i>Cancelar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="grid-container" grid-container ng-swipe-left="gridHidden = false" ng-class='{"gridHidden": editMode}'>
        <div class="page-header">
            <i class="icon-align-justify" ng-click='editMode = !editMode'></i>
            <label>Administração Usuário Call-Center</label>
        </div>
        <div class="widget-box">
            <div class="widget-header">
                <h4>Registros</h4>
                <div class="widget-toolbar">
                    </a><a href=""><i class="icon-search" title="Pesquisar" ng-click="onSearchMode(item)"></i></a>
                    <a href="/views/backoffice/ajuda/cadastroUsuario.html" target="_blank"><i class="icon-question-sign" title="Ajuda"></i></a>
                </div>
            </div>
            <div class="widget-body" ng-class="{'exibeVoltar': exibeVoltar}">
                <a href="" class="btn btn-sm btn-inverse" ng-click="editMode = !editMode;" ng-show="exibeVoltar">
                    <i class="icon-double-angle-left icon-only bigger-110" ng-show="exibeVoltar && editMode"></i><i class="icon-double-angle-right icon-only bigger-110" ng-show="exibeVoltar && !editMode"></i>
                </a>
                <div class="widget-main" style="min-height: 600px;">
                    <div class="row">
                        <div class="form-group col-md-12" style="font-size: 90%;">
                            Clique em um registro para editar
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table ng-table="tableUsuarios" class="table table-bordered table-striped table-hover" ng-class="{'tableVoltar': exibeVoltar && $data.length > 0, 'tableNaoExibeVoltar': !exibeVoltar || $data.length == 0}">
                            <tbody>
                                <tr ng-repeat="item1 in $data | filter:itemProduto" ng-click="onEdit(item1)" scroll-to="">
                                    <td ng-style="{ 'width': '5%' }" data-title="'#'" sortable="'codigo'">{{item1.codigo}}</td>
                                    <td ng-style="{ 'width': '35%' }" data-title="'Razão Social'" sortable="'razao'">{{item1.razao}}</td>
                                    <td ng-style="{ 'width': '15%' }" data-title="'CNPJ'" sortable="'cnpj'">{{item1.cnpj}}</td>
                                    <td ng-style="{ 'width': '15%' }" data-title="'Nome'" sortable="'nome'">{{item1.nome}}</td>
                                    <td ng-style="{ 'width': '15%' }" data-title="'CPF'" sortable="'cpf'">{{item1.cpf}}</td>
                                    <td ng-style="{ 'width': '15%' }" data-title="'Email'" sortable="'email'">{{item1.email}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
