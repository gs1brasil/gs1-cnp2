﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Configuration;
using GS1.CNP.UTIL;

namespace GS1.CNP.WSBARCODE
{
    /// <summary>
    /// Summary description for WSBarcode
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    //[System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WSBarcode : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public string GerarCodigoBarras(dynamic parametro)
        {
            try
            {
                string dominios = ConfigurationManager.AppSettings["DominioPermitido"].ToString();
                string[] split = dominios.Split(';');
                bool dominiopermitido = false;

                if (dominios != string.Empty)
                {
                    foreach (string s in split)
                    {
                        if (System.Web.HttpContext.Current.Request.UserHostAddress.Equals(s.Trim()))
                            dominiopermitido = true;
                    }
                }
                else
                {
                    dominiopermitido = true;
                }

                if (dominiopermitido)
                {
                    dynamic param = new ExpandoObject();
                    CodigoBarras util = new CodigoBarras();
                    byte[] img = util.GeraEtiqueta(JsonConvert.DeserializeObject(parametro));
                    if (img != null)
                        param.imagedata = Convert.ToBase64String(img);

                    return JsonConvert.SerializeObject(param);
                }

                return JsonConvert.SerializeObject("Domínio não permitido: " + System.Web.HttpContext.Current.Request.UserHostAddress);
                               
            }
            catch (Exception ex)
            {
                return null;
            }
           
        }
    }
}
